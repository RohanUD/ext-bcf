<%@ tag body-content="empty" trimDirectiveWhitespaces="true"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ attribute name="journeyRefNumber" required="true" type="java.lang.Integer"%>
<c:set var="bcfTravellerData" value="" />
<c:forEach items="${travellersGroupedByJourney.travellerDataPerJourney}" var="bcfTravellerDataPerJourney">
	<c:if test="${bcfTravellerDataPerJourney.journeyReferenceNumber == journeyRefNumber}">
		<c:set var="bcfTravellerData" value="${bcfTravellerDataPerJourney}" />
	</c:if>
</c:forEach>
<div>
	<h3 class="panel-title title-collapse">
		<a role="button" data-toggle="collapse" aria-expanded="true" aria-controls="panel-AccessibilityDeclaration" data-target="#panel-AccessibilityDeclaration" class="panel-heading panel-header-link collapsable">
			<spring:theme code="text.accessbilitydeclaration.category.name" />
		</a>
	</h3>
</div> 
<div class="panel-body">
	<div class="row in collapse show" id="panel-AccessibilityDeclaration" aria-expanded="true" style="">
			<c:if test="${not empty bcfTravellerData}">
				<c:forEach items="${bcfTravellerData.outboundTravellerData}" var="traveller" varStatus="travellerIndex">
					<c:if test="${traveller.travellerType eq 'PASSENGER'}">
						<div class="col-xs-12">
							<div class="col-xs-12">
								<c:set var="travellerCodeMap" value="${bcfTravellerData.namesMap[traveller.travellerInfo.passengerType.code]}" />
								<label>${fn:escapeXml(travellerCodeMap[traveller.label])}</label>
								<input type="hidden" class="y_accessibilityDeclaration_travellerCode" value="${fn:escapeXml(traveller.label)}">
							</div>
						</div>
						<c:set var="selectedSpecialRequestsForTraveller" value="" />
						<c:if test="${not empty traveller and not empty traveller.specialRequestDetail}">
							<c:set var="selectedSpecialRequestsForTraveller" value="${traveller.specialRequestDetail.specialServiceRequests}" />
						</c:if>
						<c:forEach items="${specialServiceRequests}" var="masterSpecialServiceRequest" varStatus="status">
							<c:set var="checked" value="" />
							<c:set var="combinedKey" value="${bcfTravellerData.journeyReferenceNumber}-${traveller.uid}-${masterSpecialServiceRequest.code}" />
							<c:if test="${not empty selectedServicesPerLegPerTraveller && fn:contains(selectedServicesPerLegPerTraveller, combinedKey) }">
								<c:set var="checked" value='checked="checked"' />
							</c:if>
							<div id="accessibilityDeclaration${bcfTravellerData.journeyReferenceNumber}-traveller${travellerIndex.index}-declaration${status.index}" class="form-inline full-width marL15">
								${masterSpecialServiceRequest.name} <input class="accessibilityDeclaration_checkbox_js" id="accessibilityDeclaration_${traveller.label}_${bcfTravellerData.journeyReferenceNumber}" type="checkbox" data-specialServiceCode="${masterSpecialServiceRequest.code}"
									data-journeyRefNumber="${bcfTravellerData.journeyReferenceNumber}" data-travellerUid="${traveller.uid}" ${checked} />
							</div>
						</c:forEach>
					</c:if>
				</c:forEach>
				</c:if>
	</div>
</div>
