<%@ tag body-content="empty" trimDirectiveWhitespaces="true"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="booking" tagdir="/WEB-INF/tags/addons/bcfvoyageaddon/responsive/bookingdetails"%>
<%@ attribute name="reservationItem" required="true" type="de.hybris.platform.commercefacades.travel.reservation.data.ReservationItemData"%>
<%@ taglib prefix="reservation" tagdir="/WEB-INF/tags/addons/bcfvoyageaddon/responsive/reservation"%>
<%@ attribute name="cssClass" required="true" type="java.lang.String"%>
<%@ taglib prefix="format" tagdir="/WEB-INF/tags/addons/bcfstorefrontaddon/responsive/format"%>
<spring:htmlEscape defaultHtmlEscape="false" />
<div class="${fn:escapeXml(cssClass)}">
	<%-- display route information --%>
	<div class="panel-heading">
		<c:set var="transportOfferings" value="${reservationItem.reservationItinerary.originDestinationOptions[0].transportOfferings}" />
		<c:set var="originSector" value="${transportOfferings[0].sector}" />
		<c:set var="item" value="${reservationItem}" />
		<c:set var="finalSector" value="${transportOfferings[fn:length(transportOfferings) - 1].sector}" />
		<h3 class="panel-title title-collapse">${fn:escapeXml(originSector.origin.name)}
			(${fn:escapeXml(originSector.origin.code)}) <span aria-hidden="true" class="glyphicon glyphicon-arrow-right"></span> ${fn:escapeXml(finalSector.destination.name)} (${fn:escapeXml(finalSector.destination.code)}) -
			${fn:escapeXml(reservationItem.reservationPricingInfo.itineraryPricingInfo.bundleTypeName)}
		</h3>
	</div>
	<%-- display flight information --%>
	<div class="panel-body collapse in" id="section-${fn:escapeXml(reservationItem.originDestinationRefNumber)}">
		<booking:journeyDetails itinerary="${item.reservationItinerary}" transportOfferings="${transportOfferings}" journeyReferenceNumber="${reservation.journeyReferenceNumber}" originDestinationRefNumber="${item.originDestinationRefNumber}"/>
		<div>
               Duration: <strong>${item.reservationItinerary.duration['transport.offering.status.result.hours'] }</strong> hr
                         <strong>${item.reservationItinerary.duration['transport.offering.status.result.minutes'] }</strong> min
		</div>
		<div class="col-sm-12">
			<div class="col-sm-2">
				<c:if test="${not empty item.reservationPricingInfo.totalFare.totalBaseExtrasPrice}">
					<p class="blue-color mt-2">
						<strong><u>Fare$</u></strong>
					</p>
				</c:if>
			</div>
			<div class="col-sm-4">
				<h4>
					<strong><format:price priceData="${item.reservationPricingInfo.totalFare.totalBaseExtrasPrice}" /></strong>
				</h4>
			</div>
			<div class="col-sm-6">
				<c:if test="${not empty item.reservationPricingInfo.itineraryPricingInfo.bundleTypeName}">
					<h5>
						FareType: <b>${fn:escapeXml(item.reservationPricingInfo.itineraryPricingInfo.bundleTypeName)}</b>
					</h5>
				</c:if>
			</div>
				<ul class="col-sm-12 list-unstyled">
					<li><p>
						<spring:theme code="reservation.carrying.dangerous.goods" text="Carrying Dangerous Goods" />
						&nbsp;&nbsp;<Strong>${reservationItem.carryingDangerousGoods}</Strong>
				</p></li>
				</ul>
			<ul class="col-sm-12 list-unstyled">
				<reservation:ptcBreakdown ptcBreakdownList="${item.reservationPricingInfo.itineraryPricingInfo.ptcFareBreakdownDatas}" />
			</ul>
			<ul class="col-sm-12 list-unstyled">
				<reservation:vehicleBreakdown vehicleBreakdownList="${item.reservationPricingInfo.itineraryPricingInfo.vehicleFareBreakdownDatas}" />
			</ul>
			<ul class="col-sm-12 list-unstyled">
				<reservation:ancillaries offerBreakdowns="${item.reservationPricingInfo.offerBreakdowns}" />
			</ul>
		</div>
		<c:if test="${item.reservationItinerary.route.routeType eq 'LONG'}">
			<c:forEach items="${item.reservationItinerary.travellers}" var="traveller" varStatus="travellerCount">
				<c:if test="${traveller.travellerType eq 'PASSENGER' }">
					<label>${traveller.travellerInfo.passengerType.name} ${travellerCount.index + 1}</label>
					<c:forEach items="${traveller.specialRequestDetail.specialServiceRequests}" var="specialServiceRequests">
						<br />${specialServiceRequests.name}
	               </c:forEach>
				</c:if>
			</c:forEach>
		</c:if>
	</div>
</div>
