<%@ tag body-content="empty" trimDirectiveWhitespaces="true"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ attribute name="bookingActionDatas" required="true" type="java.util.List"%>
<%@ attribute name="actionType" required="true" type="java.lang.String"%>
<%@ attribute name="travellerUid" type="java.lang.String"%>
<%@ attribute name="leg" type="java.lang.String"%>
<%@ attribute name="actionName" type="java.lang.String"%>
<%@ attribute name="isGlobal" required="true" type="java.lang.Boolean"%>
<%@ attribute name="waitlistedBooking" type="java.lang.Boolean"%>
<spring:htmlEscape defaultHtmlEscape="false" />
<c:forEach items="${bookingActionDatas}" var="bookingActionData">
	<c:if test="${actionType == bookingActionData.actionType}">
		<c:set var="actionData" value="${bookingActionData}" />
	</c:if>
</c:forEach>
<c:set var="waitlistedBooking" value="${(empty waitlistedBooking) ? 'false' : waitlistedBooking}" />
<c:choose>
	<c:when test="${actionData.enabled}">
		<c:if test="${isGlobal}">
			<c:if test="${actionType == 'CANCEL_BOOKING'}">
				<c:set var="y_class" value="y_cancelBookingButton" />
				<c:set var="disabled" value="true" />
			</c:if>
		</c:if>
		<c:if test="${actionType == 'CANCEL_TRANSPORT_BOOKING'}">
			<c:set var="y_class" value="y_cancelTransportBookingButton" />
			<c:set var="disabled" value="true" />
		</c:if>
		<c:if test="${actionType == 'CANCEL_SAILING'}">
            <c:set var="y_class" value="y_cancelSailingButton" />
        </c:if>
        <c:if test="${not waitlistedBooking}">
		<c:if test="${actionType == 'REMOVE_TRAVELLER'}">
			<c:set var="y_class" value="y_removeTraveller" />
		</c:if>
		<c:if test="${actionType == 'ACCEPT_BOOKING'}">
			<c:set var="y_class" value="y_acceptBookingButton" />
		</c:if>
		<c:if test="${actionType == 'REJECT_BOOKING'}">
			<c:set var="y_class" value="y_cancelBookingButton" />
		</c:if>
		<c:if test="${actionType == 'AMEND_PASSENGER_VEHICLE'}">
			<c:set var="y_class" value="y_amendTravellerButton" />
		</c:if>
		<c:if test="${actionType == 'REPLAN_JOURNEY'}">
			<c:set var="y_class" value="y_replanJourneyButton" />
		</c:if>
		<c:if test="${actionType == 'AMEND_ANCILLARY'}">
			<c:set var="y_class" value="y_amendAncillaryButton" />
		</c:if>
		<c:if test="${actionType == 'AMEND_PERSONAL_DETAILS' }">
			<c:set var="y_class" value="y_amendPersonalDetails"></c:set>
		</c:if>
		</c:if>
		<c:if test="${not empty y_class}">
			<c:url value="${actionData.actionUrl}" var="actionUrl" />
			<a class="btn ${actionType == 'CHECK_IN_ALL'? 'btn-secondary' : 'btn-primary'} btn-block ${fn:escapeXml(y_class)}" href="${actionUrl}" ${disabled ? 'disabled' : ''} <c:if test="${actionType == 'REMOVE_TRAVELLER'}">
			style="pointer-events: none;"
		</c:if>>
				<c:choose>
					<c:when test="${not empty actionName}">
						<c:out value="${actionName}" />
					</c:when>
					<c:otherwise>
						<spring:theme code="button.page.managemybooking.bookingaction.${actionType}" text="${fn:escapeXml(fn:toLowerCase(actionType))}" />
					</c:otherwise>
				</c:choose>
			</a>
		</c:if>
	</c:when>
	<c:otherwise>
		<c:if test="${not empty actionData.alternativeMessages && actionType == 'CHECK_IN'}">
			<c:forEach items="${actionData.alternativeMessages}" var="message">
				<c:set var="messageText">
					<spring:theme code="${message}" />
				</c:set>
				<c:if test="${not empty messageText}">
					<button class="btn btn-default btn-block" disabled="disabled">${fn:escapeXml(messageText)}</button>
				</c:if>
			</c:forEach>
		</c:if>
	</c:otherwise>
</c:choose>
