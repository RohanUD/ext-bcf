<%@ tag body-content="empty" trimDirectiveWhitespaces="true"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="booking" tagdir="/WEB-INF/tags/addons/bcfvoyageaddon/responsive/bookingdetails"%>
<%@ attribute name="reservationItem" required="true" type="de.hybris.platform.commercefacades.travel.reservation.data.ReservationItemData"%>
<%@ attribute name="reservationData" required="true" type="de.hybris.platform.commercefacades.travel.reservation.data.ReservationData"%>
<%@ attribute name="cssClass" required="true" type="java.lang.String"%>
<booking:itineraryitem reservationItem="${reservationItem}" cssClass="${cssClass}" />
<c:set var = "key" scope = "session" value = "${reservationData.journeyReferenceNumber}${reservationItem.originDestinationRefNumber}"/>
<img  src="data:image/png;base64,<c:out value="${QRImageMap[key]}"/> " alt="QR CODE"height="200" width="200" >
<c:if test="${reservationItem.reservationItinerary.route.routeType=='LONG'}">
	<booking:passengersummary reservationData="${reservationData}" reservationItem="${reservationItem}" />
</c:if>
<booking:ancillarySharedSummary reservationItem="${reservationItem}" />
