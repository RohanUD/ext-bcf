<%@ tag body-content="empty" trimDirectiveWhitespaces="true"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ attribute name="tabId" required="true" type="java.lang.String"%>
<%@ attribute name="tabName" required="true" type="java.lang.String"%>
<%@ attribute name="passengerType" required="true" type="java.lang.String"%>
<%@ attribute name="checked" required="true" type="Boolean"%>
<%@ attribute name="disabled" required="true" type="Boolean"%>
<spring:htmlEscape defaultHtmlEscape="false" />
<li class="list-group-item passenger-seat">
	<div class="row input-row">
		<div class="col-xs-12">
			<label for="${fn:escapeXml(tabId)}" class="col-xs-10 passenger-group">
				<span class="passenger-name">${fn:escapeXml(passengerType)}</span>
				<span class="passenger-seat-info">
					<span class="passenger-seat-number"><spring:theme code="text.ancillary.seatmap.seat" text="Seat" /></span>
					<span class="passenger-selected-seat">__</span>
				</span>
			</label>
			<input id="${fn:escapeXml(tabId)}" type="radio" class="y_ancillaryPassengerSeatRadio passenger-seat" name="${fn:escapeXml(tabName)}" ${disabled ? 'disabled ' : '' } <c:if test="${checked}">checked="checked"</c:if> />
		</div>
	</div>
</li>
