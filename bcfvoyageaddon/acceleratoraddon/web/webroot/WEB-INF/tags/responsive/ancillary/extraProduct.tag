<%@ tag body-content="empty" trimDirectiveWhitespaces="true"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ attribute name="extraProductPerVessel" required="true" type="com.bcf.facades.extra.product.ExtraProductPerVessel"%>
<%@ attribute name="pathPrefix" required="true" type="String"%>
<%@ attribute name="journeyRef" required="true" type="Integer"%>
<%@ attribute name="odRef" required="true" type="Integer"%>
<%@ attribute name="routeCode" required="true" type="String"%>
<%@ attribute name="transportOfferingCode" required="true" type="String"%>
<%@ attribute name="paxCountPerVessel" required="true" type="Integer"%>
<%@ attribute name="location" required="true" type="String"%>
<%@ attribute name="dateTime" required="true" type="String"%>
<%@ taglib prefix="fareselection" tagdir="/WEB-INF/tags/addons/bcfvoyageaddon/responsive/fareselection"%>
<c:choose>
<c:when test="${odRef eq 0}">
<p class="selacted-date-text mb-1"><span><spring:theme code="reservation.journey.depart" text="Depart:"/></span></p>
</c:when>
<c:otherwise>
    <p class="selacted-date-text mb-1"><span><spring:theme code="reservation.journey.return" text="Return:"/></span></p>
</c:otherwise>
</c:choose>
    <fmt:parseDate value="${dateTime}" var="parsedDepartDate" pattern="MM/dd/yyyy" />
    <fmt:formatDate value="${parsedDepartDate}" var="formattedParsedDepartDate" pattern="E, MMM dd yyyy" />${formattedParsedDepartDate}
<c:choose>
    <c:when test="${fn:contains(location, '-')}">
        <c:set var = "departureLocationName" value = "${fn:substringAfter(location,'-')}"/>
    </c:when>
    <c:otherwise>
        <c:set var = "departureLocationName" value = "${location}"/>
    </c:otherwise>
</c:choose>

<c:choose>
    <c:when test="${fn:contains(departureLocationName, '(')}">
        <c:set var = "departureTerminal" value ="${fn:substringAfter(fn:substringBefore(departureLocationName,')'), '(')}"/>
        <c:set var = "departureCity" value ="${fn:substringBefore(departureLocationName, '(')}"/>
        <p><strong>${departureCity}</strong> (${departureTerminal})</p>
    </c:when>
    <c:otherwise>
        <p class="font-weight-bold fnt-18 m-0">${departureLocationName}</p>
    </c:otherwise>
</c:choose>

<p class="font-italic mb-3">
	<spring:theme code="text.ancillary.vessel.name.prefix" text="Ferry" />: ${extraProductPerVessel.vehicleName}
</p>
<div class="row mb-4 bc-accordion">
	<div class="col-md-6 mb-3">
		<p>
		    <spring:message code="label.ancillary.add.cabin.info.text" var="cabinInfoText" />
			<strong><spring:theme code="label.ancillary.cabin.product.dropdown.header" text="Add cabin" /></strong>
				<a href="javascript:;" data-container="body" data-toggle="popover"
				data-placement="bottom" data-html="true" title="" class="popoverThis" data-content="${fn:escapeXml(cabinInfoText)}">
                   <span class="bcf bcf-icon-info-solid"></span>
                </a>
			<c:if test="${not empty extraProductPerVessel.cabins}">
				<span class="pull-right icon-blue">	
					<i class="bcf bcf-icon-gallery ml-2" aria-hidden="true" data-toggle="modal" data-target="#cabinMedia-${extraProductPerVessel.vehicleCode}-${journeyRef}-${odRef}">
					<span><spring:theme code="label.ancillary.extra.product.cabin.photos" text="Cabin photos"/></span></i>
				</span>
			</c:if>
		</p>
		<div class="modal fade" id="cabinMedia-${extraProductPerVessel.vehicleCode}-${journeyRef}-${odRef}" tabindex="-1" role="dialog" aria-labelledby="cabinModalCenterTitle" aria-hidden="true">
			<div class="modal-dialog modal-dialog-centered" role="document">
				<div class="modal-content">
					<div class="modal-header p-3">
						<h4 class="modal-title mt-0" id="cabinModalLongTitle">
							<strong><spring:theme code="label.ancillary.extra.lounge.cabins" text="Cabins" /></strong>
						</h4>
						<button type="button" class="close p-3" data-dismiss="modal" aria-label="Close">
							<span aria-hidden="true"><i class="fas fa-times-circle"></i></span>
						</button>
					</div>
					<div class="modal-body">
						<div id="cabin-carousel-modal" class="carousel slide" data-ride="carousel">
							<div class="carousel-inner">
								<c:forEach items="${extraProductPerVessel.cabins}" var="cabin" varStatus="cabinIdx">
									<div class="item ${cabinIdx.index eq 0 ? 'active' : ''}">
										<c:if test="${not empty cabin.cabinMedia}">
											<img class='img-fluid js-responsive-image' alt='${altText}' title='${altText}' data-media='${cabin.cabinMedia}' />
										</c:if>
										<h2 class="mt-3">${cabin.cabinName}</h2>
										<p>${cabin.cabinInfo}</p>
									</div>
								</c:forEach>
							</div>
							<a class="left carousel-control" href="#cabin-carousel-modal" data-slide="prev">
								<span class="glyphicon glyphicon-chevron-left"></span>
							</a>
							<a class="right carousel-control" href="#cabin-carousel-modal" data-slide="next">
								<span class="glyphicon glyphicon-chevron-right"></span>
							</a>
						</div>
					</div>
				</div>
			</div>
		</div>
		<div id="accordion-extras-page-1">
			<h3>
				<c:choose>
					<c:when test="${empty extraProductPerVessel.cabinProductList}">
						<spring:theme code="label.ancillary.extra.product.dropdown.cabinsoldout.heading" text="Cabin Sold Out" />
					</c:when>
					<c:otherwise>
						<spring:theme code="label.ancillary.extra.product.cabin.dropdown.heading" text="Select cabin" />
					</c:otherwise>
				</c:choose>
				<span class="custom-arrow"></span>
			</h3>
			<div class="js-extra-product-group-by-type">
				<c:forEach items="${extraProductPerVessel.cabinProductList}" var="cabinProduct" varStatus="k">
					<c:url var="addToCartUrl" value="/cart/add" />
					<form:form method="post" id="addToCartForm" commandName="addToCartForm" action="${addToCartUrl}">
						<form:input path="productCode" type="hidden" readonly="true" value="${fn:escapeXml(cabinProduct.code)}" />
						<form:input path="journeyRefNumber" type="hidden" readonly="true" value="${journeyRef}" />
						<form:input path="originDestinationRefNumber" type="hidden" readonly="true" value="${odRef}" />
						<form:input path="travelRouteCode" type="hidden" readonly="true" value="${routeCode}" />
						<form:input path="transportOfferingCodes[${0}]" type="hidden" readonly="true" value="${transportOfferingCode}" />
						<div class="pass-fluid">
							<div class="pass-fluid-left fluid-text">
								<label for="y_${fn:escapeXml(cabinProduct.code)}"> ${fn:escapeXml(cabinProduct.name)} </label>
							</div>
							<div class="pass-fluid-right">
								<div class="input-group y_extraProductQuantitySelector">
									<span class="input-group-btn">
										<button class="btn btn-minus y_extraProductSelectorMinus" type="button" data-type="minus" data-field="">
											<span class="bcf bcf-icon-remove"></span>
										</button>
									</span>
									<c:set var="cabinProductJourneyRef">${cabinProduct.code}-${journeyRef}-${odRef}</c:set>
									<c:choose>
										<c:when test="${ not empty ancillaryProductsEntryQtyMap[cabinProductJourneyRef]}">
											<form:input type="text" class="form-control js-product-qty-input" id="y_extraProductQuantity" min="0" max="${paxCountPerVessel}" value="${ancillaryProductsEntryQtyMap[cabinProductJourneyRef]}" path="qty" readonly="true" />
										</c:when>
										<c:otherwise>
											<form:input type="text" class="form-control js-product-qty-input" id="y_extraProductQuantity" min="0" max="${paxCountPerVessel}" path="qty" value="${cabinProduct.count}" readonly="true" />
										</c:otherwise>
									</c:choose>
									<span class="input-group-btn">
										<button class="btn btn-plus y_extraProductSelectorPlus" type="button" data-type="plus" data-field="">
											<span class="bcf bcf-icon-add"></span>
										</button>
									</span>
								</div>
							</div>
						</div>
					</form:form>
				</c:forEach>
			</div>
		</div>
	</div>
	<div class="col-md-6 mb-3">
		<p>
			<strong><spring:theme code="label.ancillary.meal.product.dropdown.header" text="Pre-order meal" /></strong>
		</p>
		<div id="accordion-extras-page-2">
			<h3>
				<c:choose>
					<c:when test="${empty extraProductPerVessel.mealProductList}">
						<spring:theme code="label.ancillary.extra.product.dropdown.mealsoldout.heading" text="Meal Sold Out" />
					</c:when>
					<c:otherwise>
						<spring:theme code="label.ancillary.extra.product.buffet.dropdown.heading" text="Select buffet" />
					</c:otherwise>
				</c:choose>
				<span class="custom-arrow"></span>
			</h3>
			<div class="js-extra-product-group-by-type">
				<c:forEach items="${extraProductPerVessel.mealProductList}" var="mealProduct" varStatus="k">
					<c:url var="addToCartUrl" value="/cart/add" />
					<form:form method="post" id="addToCartForm" commandName="addToCartForm" action="${addToCartUrl}">
						<form:input path="productCode" type="hidden" readonly="true" value="${fn:escapeXml(mealProduct.code)}" />
						<form:input path="journeyRefNumber" type="hidden" readonly="true" value="${journeyRef}" />
						<form:input path="originDestinationRefNumber" type="hidden" readonly="true" value="${odRef}" />
						<form:input path="travelRouteCode" type="hidden" readonly="true" value="${routeCode}" />
						<form:input path="transportOfferingCodes[${0}]" type="hidden" readonly="true" value="${transportOfferingCode}" />
						<div class="pass-fluid">
							<div class="pass-fluid-left fluid-text">
								<label for="y_${fn:escapeXml(mealProduct.code)}"> ${fn:escapeXml(mealProduct.name)} </label>
							</div>
							<div class="pass-fluid-right">
								<div class="input-group y_extraProductQuantitySelector">
									<span class="input-group-btn">
										<button class="btn btn-minus y_extraProductSelectorMinus" type="button" data-type="minus" data-field="">
											<span class="bcf bcf-icon-remove"></span>
										</button>
									</span>
									<c:set var="mealProductJourneyRef">${mealProduct.code}-${journeyRef}-${odRef}</c:set>
									<c:choose>
										<c:when test="${not empty ancillaryProductsEntryQtyMap[mealProductJourneyRef]}">
											<form:input type="text" class="form-control js-product-qty-input" id="y_extraProductQuantity" min="0" max="${paxCountPerVessel}" value="${ancillaryProductsEntryQtyMap[mealProductJourneyRef]}" path="qty" readonly="true" />
										</c:when>
										<c:otherwise>
											<form:input type="text" class="form-control js-product-qty-input" id="y_extraProductQuantity" min="0" max="${paxCountPerVessel}" path="qty" value="${mealProduct.count}" readonly="true" />
										</c:otherwise>
									</c:choose>
									<span class="input-group-btn">
										<button class="btn btn-plus y_extraProductSelectorPlus" type="button" data-type="plus" data-field="">
											<span class="bcf bcf-icon-add"></span>
										</button>
									</span>
								</div>
							</div>
						</div>
					</form:form>
				</c:forEach>
			</div>
		</div>
	</div>
	<div class="col-md-6 mb-3">
		<p>
			<strong><spring:theme code="label.ancillary.lounge.product.dropdown.header" text="Lounge access" /></strong>
			<c:if test="${extraProductPerVessel.seatMapMedia ne ''}">
				<span class="pull-right icon-blue">
					<i class="bcf bcf-icon-gallery ml-2" aria-hidden="true" data-toggle="modal" data-target="#seatMap-${extraProductPerVessel.vehicleCode}-${journeyRef}-${odRef}">
					<span><spring:theme code="label.ancillary.extra.lounge.seating.chart" text="Seating chart" /></span></i>
				</span>
			</c:if>
		</p>
		<div class="modal fade" id="seatMap-${extraProductPerVessel.vehicleCode}-${journeyRef}-${odRef}" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
			<div class="modal-dialog modal-dialog-centered" role="document">
				<div class="modal-content">
					<div class="modal-header p-3">
						<h4 class="modal-title mt-0" id="exampleModalLongTitle">
							<strong>${extraProductPerVessel.seatMapName}</strong>
						</h4>
						<button type="button" class="close p-3" data-dismiss="modal" aria-label="Close">
							<span aria-hidden="true"><i class="fas fa-times-circle"></i></span>
						</button>
					</div>
					<div class="modal-body">
						<c:if test="${not empty extraProductPerVessel.seatMapMedia}">
							<img class='img-fluid js-responsive-image' alt='${altText}' title='${altText}' data-media='${extraProductPerVessel.seatMapMedia}' />
						</c:if>
						<h2 class="mt-3">
							<spring:theme code="label.ancillary.extra.lounge.seating.chart" text="Seating chart" />
						</h2>
						<p>${extraProductPerVessel.seatMapInfo}</p>
					</div>
				</div>
			</div>
		</div>
		<div id="accordion-extras-page-3">
			<h3>
				<c:choose>
					<c:when test="${empty extraProductPerVessel.loungeProductList}">
						<spring:theme code="label.ancillary.extra.product.dropdown.loungesoldout.heading" text="Lounge Sold Out" />
					</c:when>
					<c:otherwise>
						<spring:theme code="label.ancillary.extra.product.lounge.dropdown.heading" text="Select lounge" />
					</c:otherwise>
				</c:choose>
				<span class="custom-arrow"></span>
			</h3>
			<div class="js-extra-product-group-by-type">
				<c:forEach items="${extraProductPerVessel.loungeProductList}" var="loungeProduct" varStatus="k">
					<c:url var="addToCartUrl" value="/cart/add" />
					<form:form method="post" id="addToCartForm" commandName="addToCartForm" action="${addToCartUrl}">
						<form:input path="productCode" type="hidden" readonly="true" value="${fn:escapeXml(loungeProduct.code)}" />
						<form:input path="journeyRefNumber" type="hidden" readonly="true" value="${journeyRef}" />
						<form:input path="originDestinationRefNumber" type="hidden" readonly="true" value="${odRef}" />
						<form:input path="travelRouteCode" type="hidden" readonly="true" value="${routeCode}" />
						<form:input path="transportOfferingCodes[${0}]" type="hidden" readonly="true" value="${transportOfferingCode}" />
						<div class="pass-fluid">
							<div class="pass-fluid-left fluid-text">
								<label for="y_${fn:escapeXml(loungeProduct.code)}"> ${fn:escapeXml(loungeProduct.name)} </label>
							</div>
							<div class="pass-fluid-right">
								<div class="input-group y_extraProductQuantitySelector">
									<span class="input-group-btn">
										<button class="btn btn-minus y_extraProductSelectorMinus" type="button" data-type="minus" data-field="">
											<span class="bcf bcf-icon-remove"></span>
										</button>
									</span>
								    <c:set var="loungeProductJourneyRef">${loungeProduct.code}-${journeyRef}-${odRef}</c:set>
									<c:choose>
										<c:when test="${ not empty ancillaryProductsEntryQtyMap[loungeProductJourneyRef]}">
											<form:input type="text" class="form-control js-product-qty-input" id="y_extraProductQuantity" min="0" max="${paxCountPerVessel}" value="${ancillaryProductsEntryQtyMap[loungeProductJourneyRef]}" path="qty" readonly="true" />
										</c:when>
										<c:otherwise>
											<form:input type="text" class="form-control js-product-qty-input" id="y_extraProductQuantity" min="0" max="${paxCountPerVessel}" path="qty" value="${loungeProduct.count}" readonly="true" />
										</c:otherwise>
									</c:choose>
									<span class="input-group-btn">
										<button class="btn btn-plus y_extraProductSelectorPlus" type="button" data-type="plus" data-field="">
											<span class="bcf bcf-icon-add"></span>
										</button>
									</span>
								</div>
							</div>
						</div>
					</form:form>
				</c:forEach>
			</div>
		</div>
	</div>
</div>
