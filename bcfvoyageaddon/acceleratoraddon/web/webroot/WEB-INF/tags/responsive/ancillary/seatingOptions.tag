<%@ tag body-content="empty" trimDirectiveWhitespaces="true"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ attribute name="itineraries" required="true" type="java.util.List"%>
<%@ taglib prefix="ancillary" tagdir="/WEB-INF/tags/addons/bcfvoyageaddon/responsive/ancillary"%>
<spring:htmlEscape defaultHtmlEscape="false" />
<div id="y_itineraryContainer" class="hidden">${fn:escapeXml(itinerariesJson)}</div>
<div class="panel-heading">
	<h3 class="title">
		<spring:theme code="text.ancillary.seatmap.seating.options" />
	</h3>
</div>
<div class="panel-body">
	<div class="row">
		<div class="col-sm-2 col-xs-4 icon-big">
			<div class="icon-seats"></div>
		</div>
		<div class="col-sm-6 col-xs-8">
			<span><spring:theme code="text.ancillary.seatmap.seating.options.description" text="Select a seat to enjoy additional features or to make sure you stay close to your friends and family." /></span>
		</div>
		<div class="col-xs-12 col-sm-4">
            <a id="y_selectSeat" href="#y_ancillaryPanelSeatingOptions" data-toggle="collapse" data-target="#y_ancillaryPanelSeatingOptions" aria-expanded="false" aria-controls="y_ancillaryPanelSeatingOptions" class="btn btn-primary btn-block" data-amend="${fn:escapeXml(amend)}">
                <span id="y_selectSeatSpan">
                    <spring:theme code="text.ancillary.seatmap.seating.options.button.selectSeat" text="Select Seat" />
                </span>
				<span id="y_hideSeatSpan" hidden="true">
                    <spring:theme code="text.ancillary.seatmap.seating.options.button.hideSelectSeat" text="Hide Seat Selection" />
                </span>

			</a>
		</div>
	</div>
	<div class="panel-collapse collapse" id="y_ancillaryPanelSeatingOptions">
		<div class="tab-wrapper left-tabs">
			<ul class="nav nav-tabs" role="tablist">
				<c:set var="firstTransportOfferingTab" value="true"></c:set>
				<c:set var="tabId" value="0"></c:set>
				<c:if test="${fn:length(itineraries) > 0}">
					<c:forEach var="itinerary" items="${itineraries}">
						<c:forEach var="originDestinationOption" items="${itinerary.originDestinationOptions}">
							<c:if test="${originDestinationOption.active}">
								<c:forEach var="transportOffering" items="${originDestinationOption.transportOfferings}">
									<li role="presentation" <c:if test="${fn:escapeXml(firstTransportOfferingTab)}">
	                                                    class="active"
	                                                </c:if>>
										<a href="#seating-tab-${fn:escapeXml(tabId)}" aria-controls="seating-tab-${fn:escapeXml(tabId)}" role="tab" data-toggle="tab">
											<span class="tab-line-1">${fn:escapeXml(transportOffering.sector.origin.code)}-${fn:escapeXml(transportOffering.sector.destination.code)}</span>
											<span class="tab-line-2">${fn:escapeXml(transportOffering.travelProvider.code)}${fn:escapeXml(transportOffering.number)}</span>
										</a>
									</li>
									<c:set var="firstTransportOfferingTab" value="false"></c:set>
									<c:set var="tabId" value="${tabId+1}"></c:set>
								</c:forEach>
							</c:if>
						</c:forEach>
					</c:forEach>
				</c:if>
			</ul>
			<div class="tab-content">
				<c:if test="${fn:length(itineraries) > 0}">
					<c:set var="passengerTabId" value="0"></c:set>
					<c:set var="firstItinerary" value="true"></c:set>
					<c:forEach var="itinerary" items="${itineraries}">
						<c:forEach var="originDestinationOption" items="${itinerary.originDestinationOptions}">
							<c:if test="${originDestinationOption.active}">
								<c:forEach var="transportOffering" items="${originDestinationOption.transportOfferings}">
									<c:set var="adultNum" value="1" />
									<c:set var="childNum" value="1" />
									<c:set var="infantNum" value="1" />
									<c:set var="checked" value="false" />
									<div role="tabpanel" class='tab-pane fade in <c:if test="${fn:escapeXml(firstItinerary)}"> active</c:if>' id="seating-tab-${fn:escapeXml(passengerTabId)}">
										<ul class="list-group col-xs-12 col-sm-3 radio-group">
											<c:if test="${fn:length(itinerary.travellers) > 0}">
												<c:forEach var="traveller" items="${itinerary.travellers}">
												<c:set var="travellerCodeMap" value="${travellersNamesMap[traveller.travellerInfo.passengerType.code]}" />
													<c:set var="disabled" value="${traveller.travellerStatusInfo[transportOffering.code] == 'CHECKED_IN'}" />
													<c:if test="${traveller.travellerInfo.passengerType.code == 'adult'}">
														<c:if test="${adultNum==1 && not disabled}">
															<c:set var="checked" value="true" />
														</c:if>
														<ancillary:seatingTravellerDetails tabId="tab-${passengerTabId}-radio-adult-${adultNum}-seat-${traveller.label}" tabName="tab-${passengerTabId}-passenger-seat" passengerType="${travellerCodeMap[traveller.label]}" checked="${checked}" disabled="${disabled}" />
														<c:if test="${not disabled}">
															<c:set var="adultNum" value="${adultNum+1}" />
														</c:if>
														<c:set var="checked" value="false" />
													</c:if>
													<c:if test="${traveller.travellerInfo.passengerType.code == 'child'}">
														<ancillary:seatingTravellerDetails tabId="tab-${passengerTabId}-radio-child-${childNum}-seat-${traveller.label}" tabName="tab-${passengerTabId}-passenger-seat" passengerType="${travellerCodeMap[traveller.label]}" checked="${checked}" disabled="${disabled}" />
														<c:set var="childNum" value="${childNum+1}" />
													</c:if>
													<c:if test="${traveller.travellerInfo.passengerType.code == 'infant'}">
														<ancillary:seatingTravellerDetails tabId="tab-${passengerTabId}-radio-infant-${infantNum}-seat-${traveller.label}" tabName="tab-${passengerTabId}-passenger-seat" passengerType="${travellerCodeMap[traveller.label]}" checked="${checked}" disabled="${disabled}" />
														<c:set var="infantNum" value="${infantNum+1}" />
													</c:if>
												</c:forEach>
											</c:if>
										</ul>
										<%-- Seat Picker --%>
										<div class="seat-map-wrapper col-xs-12 col-sm-9">
											<div class="seatCharts-scroll-panel">
												<div id="y_ancillarySeatMap${fn:escapeXml(passengerTabId)}">
													<div class="front-indicator"><spring:theme code="text.ancillary.seatmap.seating.front" /></div>
												</div>
											</div>
											<div class="booking-details">
												<div id="y_ancillarySeatMapLegend${fn:escapeXml(passengerTabId)}">
													<h3 class="legend-title">
														<spring:theme code="text.ancillary.seatmap.seating.legend" />
													</h3>
												</div>
											</div>
										</div>
										<%-- / Seat Picker --%>
									</div>
									<c:set var="passengerTabId" value="${passengerTabId+1}"></c:set>
									<c:set var="firstItinerary" value="false"></c:set>
								</c:forEach>
							</c:if>
						</c:forEach>
					</c:forEach>
				</c:if>
			</div>
		</div>
	</div>
</div>
