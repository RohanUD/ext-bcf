<%@ tag body-content="empty" trimDirectiveWhitespaces="true"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>

<%@ taglib prefix="ancillary" tagdir="/WEB-INF/tags/addons/bcfvoyageaddon/responsive/ancillary"%>
<div id="wrapper">


<div id="sidebar-wrapper">
          
         <div class="row">
             <div class="col-sm-9 offset-2 margin-top-100"	>
          <h4 class="mb-0"><strong>Accessibility Assistance</strong></h4>
          
           <div class="row mt-3">
            <div class="col-sm-6">
				<p>Require WheelChair</p>
			   </div>
             <div class="col-sm-5">
             <div class="input-group plus-minus-btn">
          <span class="input-group-btn">
              <button type="button" class="btn btn-default btn-number" disabled="disabled" data-type="minus" data-field="quant[1]">
                
<i class="fa fa-minus" aria-hidden="true"></i>
              </button>
          </span>
          <input type="text" name="quant[1]" class="form-control input-number" value="1">
          <span class="input-group-btn">
              <button type="button" class="btn btn-default btn-number" data-type="plus" data-field="quant[1]">
                 <i class="fa fa-plus" aria-hidden="true"></i>
              </button>
          </span>
    </div>
			   </div>
			   
			   
				 </div>
				 <div class="row mt-3">
            <div class="col-sm-6">
				<p>WheelChair Pusher</p>
			   </div>
             <div class="col-sm-5">
             <div class="input-group plus-minus-btn">
          <span class="input-group-btn">
              <button type="button" class="btn btn-default btn-number" disabled="disabled" data-type="minus" data-field="quant[1]">
                
<i class="fa fa-minus" aria-hidden="true"></i>
              </button>
          </span>
          <input type="text" name="quant[1]" class="form-control input-number" value="1">
          <span class="input-group-btn">
              <button type="button" class="btn btn-default btn-number" data-type="plus" data-field="quant[1]">
                 <i class="fa fa-plus" aria-hidden="true"></i>
              </button>
          </span>
    </div>
			   </div>
			   
			   
				 </div>
				  <div class="row mt-3">
            <div class="col-sm-6">
				<p>Traveling with my own wheelchair</p>
			   </div>
             <div class="col-sm-4">
<button type="button" class="btn btn-sm btn-toggle" data-toggle="button" aria-pressed="false" autocomplete="off">
        <div class="handle"></div>
      </button>
			   </div>
			   
			   
				 </div>
           <div class="row mt-3">
            <div class="col-sm-6">
				<p>Hearing Impaired</p>
			   </div>
             <div class="col-sm-4">
<button type="button" class="btn btn-sm btn-toggle" data-toggle="button" aria-pressed="false" autocomplete="off">
        <div class="handle"></div>
      </button>
			   </div>
			   
			   
				 </div>
           <div class="row mt-3">
            <div class="col-sm-6">
				<p>Visually Impaired</p>
			   </div>
             <div class="col-sm-4">
<button type="button" class="btn btn-sm btn-toggle" data-toggle="button" aria-pressed="false" autocomplete="off">
        <div class="handle"></div>
      </button>
			   </div>
			   
			   
				 </div>
          
          
         <div class="col-sm-12 mb-4">
         <button type="button" class="btn btn-outline-light col-sm-5" data-dismiss="modal" id="close_sidebar1">Cancel</button>
        <button type="button" class="btn find-button col-sm-5">Apply</button>
				 </div>
			 </div>
	 </div>
           
           <!-- <ul class="sidebar-nav">
                <li class="sidebar-brand">
                      <a href="#" class="btn text-left" id="sidebar-close">
                       <i class="fa fa-times fa-2x" aria-hidden="true"></i>
                    </a>
                </li>
                
                
                <li>
                    <a href="#">Dashboard</a>
                </li>
                <li>
                    <a href="#">Shortcuts</a>
                </li>
                
            </ul>-->
        </div>
</div>
        
        <div id="wrapper2">

<div id="sidebar-wrapper">
           <div class="row">
             <div class="col-sm-12">
             <img
									src="../bcfstorefront/_ui/responsive/theme-bcf/images/side-bar-image.jpg">
			   </div>
	 </div>
         <div class="row">
             <div class="col-sm-9 offset-2">
          <h4 class="mb-0"><strong>Pacific Buffet</strong></h4>
          
           <div class="row">
            <div class="col-sm-6">
				<p>Adult (XX.xx)</p>
			   </div>
             <div class="col-sm-5">
             <div class="input-group plus-minus-btn">
          <span class="input-group-btn">
              <button type="button" class="btn btn-default btn-number" disabled="disabled" data-type="minus" data-field="quant[1]">
                
<i class="fa fa-minus" aria-hidden="true"></i>
              </button>
          </span>
          <input type="text" name="quant[1]" class="form-control input-number" value="1">
          <span class="input-group-btn">
              <button type="button" class="btn btn-default btn-number" data-type="plus" data-field="quant[1]">
                 <i class="fa fa-plus" aria-hidden="true"></i>
              </button>
          </span>
    </div>
			   </div>
			   
			   
				 </div>
          
          <hr>
           <div class="row">
            <div class="col-sm-6">
				<p>Child (XX.xx)</p>
			   </div>
             <div class="col-sm-5">
             <div class="input-group plus-minus-btn">
          <span class="input-group-btn">
              <button type="button" class="btn btn-default btn-number" disabled="disabled" data-type="minus" data-field="quant[1]">
                
<i class="fa fa-minus" aria-hidden="true"></i>
              </button>
          </span>
          <input type="text" name="quant[1]" class="form-control input-number" value="1">
          <span class="input-group-btn">
              <button type="button" class="btn btn-default btn-number" data-type="plus" data-field="quant[1]">
                 <i class="fa fa-plus" aria-hidden="true"></i>
              </button>
          </span>
    </div>
			   </div>
			   
			   
				 </div>
			  
			   	
			   <hr>
        <div class="col-sm-12 mb-4">
         <button type="button" class="btn btn-outline-light col-sm-5" data-dismiss="modal" id="close_sidebar2">Cancel</button>
        <button type="button" class="btn find-button col-sm-5">Apply</button>
				 </div>
			 </div>
	 </div>
           <div class="bg-light-blue">
             <div class="col-sm-9 offset-2 pt-4">
           
          <p>The Pacific Buffet offers elegant ocean-view dining on select sailings between Vancouver(Tsawwassen) and Victoria (Swartz Bay) </p>
          <p>The buffet features a wide selection of hot and cold items, a salad bar, a decadent dessert bar and more - for one all-inclusive price</p>
				 
				 <h4><strong>Regular menu item</strong></h4>
				 <ul class="list-unstyled">
					 <li>Grilled ham</li>
					  <li>Grilled ham</li>
					   <li>Grilled ham</li>
					    <li>Grilled ham</li>
					     <li>Grilled ham</li>
					      <li>Grilled ham</li>
			   </ul>
				 
				 
				 </div>
				 
				 
				 
	 </div>
           <!-- <ul class="sidebar-nav">
                <li class="sidebar-brand">
                      <a href="#" class="btn text-left" id="sidebar-close">
                       <i class="fa fa-times fa-2x" aria-hidden="true"></i>
                    </a>
                </li>
                
                
                <li>
                    <a href="#">Dashboard</a>
                </li>
                <li>
                    <a href="#">Shortcuts</a>
                </li>
                
            </ul>-->
        </div>
</div>
        