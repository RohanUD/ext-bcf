<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="cms" uri="http://hybris.com/tld/cmstags"%>
<%@ taglib prefix="ycommerce" uri="http://hybris.com/tld/ycommercetags"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ attribute name="formPrefix" required="true" type="java.lang.String"%>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags"%>
<%@ attribute name="bcfFareFinderForm" required="true" type="com.bcf.bcfvoyageaddon.forms.cms.BCFFareFinderForm"%>
<%@ attribute name="vehicleInfoForm" required="true" type="com.bcf.bcfvoyageaddon.forms.cms.VehicleInfoForm"%>
<%@ attribute name="idPrefix" required="true" type="java.lang.String"%>
<%@ attribute name="index" required="true" type="java.lang.String"%>
<spring:htmlEscape defaultHtmlEscape="false" />

<c:choose>
	<c:when test="${index=='1'}">
		<c:set var="allowMotorcycles">${vehicleInfoForm.vehicleInfo[index].allowMotorcycle}</c:set>
	</c:when>
	<c:otherwise>
		<c:set var="allowMotorcycles">${vehicleInfoForm.vehicleInfo[0].allowMotorcycle}</c:set>
	</c:otherwise>
</c:choose>

<c:choose>
    <c:when test="${not empty vehicleInfoForm.vehicleInfo[index].vehicleType.category}">
    <c:set var="vehicleType">${vehicleInfoForm.vehicleInfo[index].vehicleType.code}_${fn:toLowerCase(vehicleInfoForm.vehicleInfo[index].vehicleType.category)}</c:set>
    </c:when>
    <c:otherwise>
        <c:set var="vehicleType">${vehicleInfoForm.vehicleInfo[index].vehicleType.code}</c:set>
    </c:otherwise>
</c:choose>
<c:set var="vehicleLength">${vehicleInfoForm.vehicleInfo[index].length}</c:set>

<div class="motorCycleSelectionDiv collapsible_${index}" style="display:${allowMotorcycles ? 'block' : 'none'}">
    <i class="bcf bcf-icon-motorcycle bcf-2x dir-right"></i>
    <spring:theme code="label.ferry.farefinder.vehicleInfo.motorcycle" text="Motorcycle" />
</div>
<div class="content motor-c motorCycleSelectionDiv" style="display:${allowMotorcycles ? 'block' : 'none'}">
    <div class="row">
        <div class="col-lg-12 col-md-12">
            <div class="">
                <div class="row">
                    <div class="col-xs-12">
                        <div class="vehicle-radio-box">
                            <label for="under7HeightRadbtn" class="custom-radio-input">
                                <spring:theme code="label.ferry.farefinder.vehicleInfo.standard" text="Standard" />
                                <form:radiobutton id="standardMotorcycle_${index}" path="vehicleInfoForm.vehicleInfo[${index}].vehicleType.code" value="MC_nonstandard" checked="${vehicleType == 'MC_nonstandard' ? 'checked' : '' }" class="y_fareFinderVehicleTypeBtn radioCode" />
                                <span class="checkmark"></span>
                            </label>
                        </div>
                    </div>
                    <div class="col-xs-12">
                        <div class="vehicle-radio-box">
                            <label for="over7HeightRadbtn" class="custom-radio-input">
                                <spring:theme code="label.ferry.farefinder.vehicleInfo.withTrailer" text="With trailer" />
                                <c:choose>
                                    <c:when test="${vehicleType == 'MST_nonstandard' and vehicleLength gt 1}">
                                        <form:radiobutton id="withTrailerMotorcycle_${index}" path="vehicleInfoForm.vehicleInfo[${index}].vehicleType.code" value="MST_trailer_nonstandard" checked='checked' class="y_fareFinderVehicleTypeBtn radioCode" />
                                    </c:when>
                                     <c:otherwise>
                                        <form:radiobutton id="withTrailerMotorcycle_${index}" path="vehicleInfoForm.vehicleInfo[${index}].vehicleType.code" value="MST_trailer_nonstandard"   class="y_fareFinderVehicleTypeBtn radioCode" />
                                     </c:otherwise>
                                </c:choose>
                                <span class="checkmark"></span>
                            </label>
                        </div>

                        <div class="unit-converter unit-converter--alt d-none" id="motorCycleLength_${index}">
                        <div class="col-xs-4 ">
                            <p id="lengthft_${index}" class="label vehicle-motorcyle-trailer-text" aria-hidden="true">
                                <spring:theme code="label.ferry.farefinder.vehicleInfo.totalLength" text="Total Length" />
                            </p>
                            </div>
                            <div class="col-xs-8 padding-0">
                            <div class="unit-converter__input-container mt-3" id="vehicle_length_inputs">
                                <div class="unit-converter__inputs">
                                    <input id="Motorcyclelengthboxinft_${index}" value="${vehicleLength}" class="js-convert-units non-spinner farelengthInput" step="any" type="number" />
                                    <label id="lengthf_${index}" for="lengthInput">
                                        <span aria-hidden="true"><spring:theme code="label.ferry.farefinder.vehicleInfo.feet" text="ft" /> </span>
                                    </label>
                                </div>
                                <div class="unit-converter__inputs">
                                    <input id="lengthboxinm_${index}" class="js-convert-units non-spinner farelengthInputInMtrs" value="0" step="any" type="number" />
                                    <label id="lengthm_${index}" for="lengthInput">
                                        <span aria-hidden="true"><spring:theme code="label.ferry.farefinder.vehicleInfo.meter" text="m" /></span>
                                    </label>
                                </div>

                            </div>
                            <div class="full-wdth fnt-14">
                              <c:set var="motorcycleLengthError">
                                    <form:errors path="${fn:escapeXml(formPrefix)}vehicleInfoForm.vehicleInfo[${fn:escapeXml(index)}].motorcycleLengthError" />
                                </c:set>
                                <c:if test="${not empty motorcycleLengthError}">
                                    <div class="motorcycleError_${fn:escapeXml(index)}">${motorcycleLengthError}</div>
                                </c:if>
                            </div>
                        </div>
                        </div>
                    </div>
                    <div class="col-xs-12">
                        <div class="vehicle-radio-box">
                            <label for="over7HeightRadbtn" class="custom-radio-input">
                                <spring:theme code="label.ferry.farefinder.vehicleInfo.trikeOrSideCar" text="Trike or sidecar" />
                                      <c:choose>
                                        <c:when test="${vehicleType == 'MST_nonstandard' and vehicleLength eq 0}">
                                        <form:radiobutton id="sidecarMotorcycle_${index}" path="vehicleInfoForm.vehicleInfo[${index}].vehicleType.code" value="MST_sidecar_nonstandard" checked='checked' class="y_fareFinderVehicleTypeBtn radioCode" />
                                         </c:when>
                                        <c:otherwise>
                                            <form:radiobutton id="sidecarMotorcycle_${index}" path="vehicleInfoForm.vehicleInfo[${index}].vehicleType.code" value="MST_sidecar_nonstandard"  class="y_fareFinderVehicleTypeBtn radioCode" />
                                        </c:otherwise>
                                        </c:choose>
                                <span class="checkmark"></span>
                            </label>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

