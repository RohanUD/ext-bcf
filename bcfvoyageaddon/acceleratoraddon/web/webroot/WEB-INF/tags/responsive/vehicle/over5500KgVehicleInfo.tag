<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="cms" uri="http://hybris.com/tld/cmstags"%>
<%@ taglib prefix="ycommerce" uri="http://hybris.com/tld/ycommercetags"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ attribute name="formPrefix" required="true" type="java.lang.String"%>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags"%>
<%@ attribute name="bcfFareFinderForm" required="true" type="com.bcf.bcfvoyageaddon.forms.cms.BCFFareFinderForm"%>
<%@ attribute name="vehicleInfoForm" required="true" type="com.bcf.bcfvoyageaddon.forms.cms.VehicleInfoForm"%>
<%@ attribute name="idPrefix" required="true" type="java.lang.String"%>
<%@ attribute name="index" required="true" type="java.lang.String"%>
<spring:htmlEscape defaultHtmlEscape="false" />
<div class="collapsible_${index}">
	<i class="bcf bcf-icon-oversize-vehicle dir-right bcf-2x bcf-bule-icon"></i>
	<spring:theme code="label.ferry.farefinder.vehicleInfo.Over5500kgGVW" text="Over 5,500 kg GVW" />
</div>
<div class="content">
	<p><strong><spring:theme code="label.ferry.farefinder.vehicleInfo.OverSizeDetail" text="Oversize details" /></strong>
	<a href="javascript:void(0)" data-container="body" data-toggle="popover"
		data-placement="bottom" data-html="true" title="" class="popoverThis" data-content="Depending on your vehicle size, sailing options may be limited.">
		<span class="bcf bcf-icon-info-solid icon-blue"></span>
	</a>
	</p>
	<div class="">
		<c:choose>
			<c:when test="${not empty vehicleInfoForm.vehicleInfo[index].vehicleType.category}">
				<c:set var="vehicleType" value="${vehicleInfoForm.vehicleInfo[index].vehicleType.code}_${fn:toLowerCase(vehicleInfoForm.vehicleInfo[index].vehicleType.category)}"></c:set>
			</c:when>
			<c:otherwise>
				<c:set var="vehicleType" value="${vehicleInfoForm.vehicleInfo[index].vehicleType.code}"></c:set>
			</c:otherwise>
		</c:choose>
		<c:set var="vehicleWidth" value="${vehicleInfoForm.vehicleInfo[index].width}"></c:set>
		<c:set var="vehicleHeight" value="${vehicleInfoForm.vehicleInfo[index].height}"></c:set>
		<c:set var="vehicleLength" value="${vehicleInfoForm.vehicleInfo[index].length}"></c:set>
		<c:set var="vehicleWeight" value="${vehicleInfoForm.vehicleInfo[index].weight}"></c:set>
		<c:set var="vehicleAxles" value="${vehicleInfoForm.vehicleInfo[index].numberOfAxis}"></c:set>
		<c:set var="groundClearance" value="${vehicleInfoForm.vehicleInfo[index].groundClearance}"></c:set>
		<c:set var="adjustable" value="${vehicleInfoForm.vehicleInfo[index].adjustable}"></c:set>
		<c:set var="over9FtWide" value="${vehicleInfoForm.vehicleInfo[index].over9FtWide}"></c:set>
		<sec:authorize access="hasAnyRole('ROLE_ANONYMOUS')">
			<input type="hidden" id="userType" value="guestUser" />
		</sec:authorize>
		<sec:authorize access="!hasAnyRole('ROLE_ANONYMOUS')">
			<input type="hidden" id="userType" value="loginUser" />
		</sec:authorize>
		<div class="row">
			<div class="col-lg-12 col-xs-12">
				<div class="vehicle-radio-box">
					<label class="custom-radio-input" for="under7HeightRadbtn">
						<spring:theme code="label.ferry.farefinder.vehicleInfo.semiTrailer" text="Semi-trailer" />
						<form:radiobutton id="semiTrailor_${index}" path="vehicleInfoForm.vehicleInfo[${index}].vehicleType.code" value="SEMI_oversize" checked="${vehicleType == 'SEMI_oversize' ? 'checked' : '' }" class="y_fareFinderVehicleTypeBtn radioCode" />
						<span class="checkmark"></span>
					</label>
				</div>
			</div>
			<div class="col-lg-12 col-xs-12">
				<div class="vehicle-radio-box">
					<label class="custom-radio-input" for="over7HeightRadbtn">
						<spring:theme code="label.ferry.farefinder.vehicleInfo.semiA/Btrain" text="Semi A/B train" />
						<form:radiobutton id="semiABTrain_${index}" path="vehicleInfoForm.vehicleInfo[${index}].vehicleType.code" value="SBT_oversize" checked="${vehicleType == 'SBT_oversize' ? 'checked' : '' }" class="y_fareFinderVehicleTypeBtn radioCode" />
						<span class="checkmark"></span>
					</label>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-lg-12 col-xs-12">
				<div class="vehicle-radio-box">
					<label class="custom-radio-input" for="under7HeightRadbtn">
						<spring:theme code="label.ferry.farefinder.vehicleInfo.straightTruck/commercialVehicle" text="Straight truck/Commercial Vehicle" />
						<form:radiobutton id="straightTruck_${index}" path="vehicleInfoForm.vehicleInfo[${index}].vehicleType.code" value="CV_oversize" checked="${vehicleType == 'CV_oversize' ? 'checked' : '' }" class="y_fareFinderVehicleTypeBtn radioCode" />
						<span class="checkmark"></span>
					</label>
				</div>
			</div>
			<div class="col-lg-12 col-xs-12">
				<div class="vehicle-radio-box">
					<label class="custom-radio-input" for="otherOversize">
						<spring:theme code="label.ferry.farefinder.vehicleInfo.other" text="Other" />
						<form:radiobutton id="otherOversize_${index}" path="vehicleInfoForm.vehicleInfo[${index}].vehicleType.code" value="OTH_oversize" checked="${vehicleType == 'OTH_oversize' ? 'checked' : '' }" class="y_fareFinderVehicleTypeBtn radioCode" />
						<span class="checkmark"></span>
					</label>
				</div>
				<div id="other-input_${index}" class="hidden">
					<i class="bcf bcf-icon-notice-outline bcf-2x bcf-vertical-middle" aria-hidden="true"></i>
					<span class="info-text-sm"><spring:theme code="label.ferry.farefinder.vehicleInfo.other.vehicle.message" text="Please call 1-888-BC FERRY (1-888-223-3779) to book this type of vehicle." /></span>
				</div>
			</div>
			<div class="col-lg-6 col-md-6 col-sm-12 col-xs-12"></div>
		</div>
		<div id="vehiclewide_${index}">
		<div class="js-over5500kg-width-selector_${index}">
		<hr>
		<div class="row">
			<div class="col-lg-12 col-xs-12">
				<div class="vehicle-radio-box">
					<label class="custom-radio-input" for="under9FtWideRadbtn">
						<spring:theme code="label.ferry.farefinder.vehicleInfo.under9ftWide" text="Under 9ft wide" />
						<form:radiobutton id="under9Ft_${index}" path="vehicleInfoForm.vehicleInfo[${index}].over9FtWide" value="${false}" checked="${over9FtWide == false ? 'checked' : '' }" class="y_fareFinderVehicleTypeBtn radioCode" />
						<span class="checkmark"></span>
					</label>
				</div>
			</div>
			<div class="col-lg-12 col-xs-12">
				<div class="vehicle-radio-box vehicle-over9ft-selection">
					<label class="custom-radio-input" for="over9FtWideRadbtn">
						<spring:theme code="label.ferry.farefinder.vehicleInfo.over9ftWide" text="Over 9ft wide" />
						<form:radiobutton id="over9Ft_${index}" path="vehicleInfoForm.vehicleInfo[${index}].over9FtWide" value="${true}" checked="${over9FtWide == true ? 'checked' : '' }" class="y_fareFinderVehicleTypeBtn radioCode" />
						<span class="checkmark"></span>
					</label>
				</div>
			</div>
			<div class="col-lg-12 col-xs-12">
			<div class="vehicle-over9ft-selection-error_${index}">
			</div>
			</div>
		</div>
		</div>
		<div class="js-over5500kg-all-dimensions_${index} ${over9FtWide eq true or over9FtWide eq false ? 'd-block' : 'd-none'}">
		<hr>
			<div class="row width-length-height">
			<div class="col-lg-12 col-xs-12 js-over5500kg-limited-sailing-warning_${index} hidden">
				<p class="col-sm-12 warning-msg mt-0 bcf-icon-text" id="limitedSilings">
					<i class="bcf bcf-icon-notice-outline bcf-2x bcf-vertical-middle" aria-hidden="true"></i>
					<spring:theme code="text.ferry.limited.sailings.message" text="Your sailings may be limited based on your input criteria." />
				</p>
			</div>
			<div class="col-lg-12 col-xs-12 margin-bottom-30 js-over5500kb-width-input_${index} ${vehicleWidth > 9.0 ? '' : 'hidden'}">
					<div class="unit-converter unit-converter--alt">
						<p id="widthft_${index}" class="label" aria-hidden="true">
							<spring:theme code="label.ferry.farefinder.vehicleInfo.totalWidth" text="Total Width" />
						</p>
						<div class="unit-converter__input-container mt-3" id="vehicle_width_inputs">
							<div class="unit-converter__inputs">
								<c:choose>
									<c:when test="${fn:contains(vehicleType, 'oversize')  and vehicleWidth gt 0}">
										<input id="OversizeWidthboxinft_${index}" value="${vehicleWidth}" name="vehicleInfoForm.vehicleInfo[${index}].width" class="fareWidthInput js-convert-units non-spinner" step="any" type="number" />
									</c:when>
									<c:otherwise>
										<input id="OversizeWidthboxinft_${index}" value="0" class="fareWidthInput js-convert-units non-spinner" step="any" type="number" />
									</c:otherwise>
								</c:choose>
								<label id="widthf_${index}" for="widthInput">
									<span aria-hidden="true"> <spring:theme code="label.ferry.farefinder.vehicleInfo.feet" text="ft" /></span>
								</label>
							</div>
							<div class="unit-converter__inputs">
								<input id="widthinm_${index}" value="0" class="js-convert-units non-spinner fareWidthInputInMtrs" step="any" type="number" />
								<label id="widthm_${index}" for="widthInput">
									<span aria-hidden="true"><spring:theme code="label.ferry.farefinder.vehicleInfo.meter" text="m" /></span>
								</label>
							</div>
						</div>
						<div class="full-wdth fnt-14">
							<c:set var="oversizeWidthError">
								<form:errors path="vehicleInfoForm.vehicleInfo[${index}].width" />
							</c:set>
							<c:if test="${not empty oversizeWidthError}">
								<p class="oversizeError_${fn:escapeXml(index)}">${oversizeWidthError}</p>
							</c:if>
						</div>
					</div>
				</div>

				<div class="col-lg-12 col-xs-12 margin-bottom-30">
					<div class="unit-converter unit-converter--alt">
						<p id="lengthft_${index}" class="label" aria-hidden="true">
							<spring:theme code="label.ferry.farefinder.vehicleInfo.totalLength" text="Total length" />
						</p>
						<div class="unit-converter__input-container mt-3" id="vehicle_length_inputs">
							<div class="unit-converter__inputs">
								<c:choose>
									<c:when test="${fn:contains(vehicleType, 'oversize')  and vehicleLength gt 0}">
										<input id="Oversizelengthboxinft_${index}" value="${vehicleLength}" name="vehicleInfoForm.vehicleInfo[${index}].length" class="js-convert-units non-spinner farelengthInput" step="any" type="number" />
									</c:when>
									<c:otherwise>
										<input id="Oversizelengthboxinft_${index}" value="0" class="js-convert-units non-spinner farelengthInput" step="any" type="number" />
									</c:otherwise>
								</c:choose>
								<label id="lengthf_${index}" for="lengthInput">
									<span aria-hidden="true"> <spring:theme code="label.ferry.farefinder.vehicleInfo.feet" text="ft" /></span>
								</label>
							</div>
							<div class="unit-converter__inputs">
								<input id="lengthboxinm_${index}" value="0" class="js-convert-units non-spinner farelengthInputInMtrs" step="any" type="number" />
								<label id="lengthm_${index}" for="lengthInput">
									<span aria-hidden="true"><spring:theme code="label.ferry.farefinder.vehicleInfo.meter" text="m" /></span>
								</label>
							</div>
						</div>
						<div class="full-wdth fnt-14">
							<c:set var="oversizeLengthError">
								<form:errors path="${fn:escapeXml(formPrefix)}vehicleInfoForm.vehicleInfo[${fn:escapeXml(index)}].oversizevehicleLengthError" />
							</c:set>
							<c:if test="${not empty oversizeLengthError}">
								<div class="oversizeError_${fn:escapeXml(index)}">${oversizeLengthError}</div>
							</c:if>
						</div>
					</div>
				</div>
				<div class="col-lg-12 col-xs-12 margin-bottom-30">
					<div class="unit-converter unit-converter--alt">
						<p id="heightft_${index}" class="label" aria-hidden="true">
							<spring:theme code="label.ferry.farefinder.vehicleInfo.totalHeight" text="Total Height" />
						</p>
						<div class="unit-converter__input-container mt-3" id="vehicle_height_inputs">
							<div class="unit-converter__inputs">
								<c:choose>
									<c:when test="${fn:contains(vehicleType, 'oversize')  and vehicleHeight gt 0}">
										<input id="Oversizeheightboxinft_${index}" value="${vehicleHeight}" name="vehicleInfoForm.vehicleInfo[${index}].height" class="fareheightInput js-convert-units non-spinner" step="any" type="number" />
									</c:when>
									<c:otherwise>
										<input id="Oversizeheightboxinft_${index}" value="0" class="fareheightInput js-convert-units non-spinner" step="any" type="number" />
									</c:otherwise>
								</c:choose>
								<label id="heightf_${index}" for="heightInput">
									<span aria-hidden="true"> <spring:theme code="label.ferry.farefinder.vehicleInfo.feet" text="ft" /></span>
								</label>
							</div>
							<div class="unit-converter__inputs">
								<input id="heightinm_${index}" value="0" class="js-convert-units non-spinner  fareheightInputInMtrs" step="any" type="number" />
								<label id="heightm_${index}" for="lengthInput">
									<span aria-hidden="true"><spring:theme code="label.ferry.farefinder.vehicleInfo.meter" text="m" /></span>
								</label>
							</div>
						</div>
						<div class="full-wdth fnt-14">
							<c:set var="oversizeHeightError">
								<form:errors path="${fn:escapeXml(formPrefix)}vehicleInfoForm.vehicleInfo[${fn:escapeXml(index)}].oversizeVehicleHeightError" />
							</c:set>
							<c:if test="${not empty oversizeHeightError}">
								<p class="oversizeError_${fn:escapeXml(index)}">${oversizeHeightError}</p>
							</c:if>
						</div>
					</div>
				</div>
			</div>
			<div class="row js-over5500kg-conditional-dimensions_${index} ${(vehicleWidth > 9.0 || vehicleAxles > 0) ? '' : 'hidden'}">
				<div class="col-lg-12 col-xs-12 margin-bottom-30">
					<div class="unit-converter unit-converter--alt">
						<p class="label" aria-hidden="true">
							<spring:theme code="label.ferry.farefinder.vehicleInfo.totalNumberOfAxles" text="Total number of axles" />
						</p>
						<div class="unit-converter__input-container mt-3">
							<div class="unit-converter__inputs">
								<input id="vehicleInfoForm.vehicleInfo${index}.numberOfAxis" name="vehicleInfoForm.vehicleInfo[${index}].numberOfAxis" value="${vehicleAxles}" class="js-convert-units non-spinner" step="any" type="number" />
							</div>
						</div>
						<div class="full-wdth fnt-14">
							<c:set var="oversizeAxleError">
								<form:errors path="vehicleInfoForm.vehicleInfo[${index}].numberOfAxis" />
							</c:set>
							<c:if test="${not empty oversizeAxleError}">
								<p class="oversizeError_${fn:escapeXml(index)}">${oversizeAxleError}</p>
							</c:if>
						</div>
					</div>
				</div>
				<div class="col-lg-12 col-xs-12 margin-bottom-20">
					<div class="unit-converter unit-converter--alt">
						<p class="label" aria-hidden="true">
							<spring:theme code="label.ferry.farefinder.vehicleInfo.totalWeight" text="Total weight" />
						</p>
						<div class="unit-converter__input-container mt-3">
							<div class="unit-converter__inputs">
								<c:choose>
									<c:when test="${fn:contains(vehicleType, 'oversize') and vehicleWeight gt 0}">
										<input id="vehicleInfoForm.vehicleInfo${index}.weight" name="vehicleInfoForm.vehicleInfo[${index}].weight" value="${vehicleWeight}" class="js-convert-units non-spinner" step="any" type="number" />
									</c:when>
									<c:otherwise>
										<input id="vehicleInfoForm.vehicleInfo${index}.weight" name="vehicleInfoForm.vehicleInfo[${index}].weight" value="0" class="js-convert-units non-spinner" step="any" type="number" />
									</c:otherwise>
								</c:choose>
								<label for="feet-78880b">
									<span aria-hidden="true"><spring:theme code="label.ferry.farefinder.vehicleInfo.kg" text="kg" /></span>
								</label>
							</div>
						</div>
						<div class="full-wdth fnt-14">
							<c:set var="oversizeWeightError">
								<form:errors path="${fn:escapeXml(formPrefix)}vehicleInfoForm.vehicleInfo[${fn:escapeXml(index)}].weight" />
							</c:set>
							<c:if test="${not empty oversizeWeightError}">
								<p class="oversizeError_${fn:escapeXml(index)}">${oversizeWeightError}</p>
							</c:if>
						</div>
					</div>
				</div>
				<div class="col-lg-12 col-xs-12 margin-bottom-20">
					<div class="unit-converter unit-converter--alt">
						<p class="label" aria-hidden="true">
							<spring:theme code="label.ferry.farefinder.vehicleInfo.groundClearance" text="Ground clearance" />
						</p>
						<div class="unit-converter__input-container mt-3">
							<div class="unit-converter__inputs">
								<input id="vehicleInfoForm.vehicleInfo${index}.groundClearance" name="vehicleInfoForm.vehicleInfo[${index}].groundClearance" value="${groundClearance}" class="js-convert-units non-spinner" step="any" type="number" />
								<label for="feet-78880b">
									<span aria-hidden="true"><spring:theme code="label.ferry.farefinder.vehicleInfo.inches" text="in" /></span>
								</label>
							</div>
						</div>
						<div class="full-wdth fnt-14">
							<c:set var="oversizeGroundClearanceError">
								<form:errors path="${fn:escapeXml(formPrefix)}vehicleInfoForm.vehicleInfo[${fn:escapeXml(index)}].groundClearance" />
							</c:set>
							<c:if test="${not empty oversizeGroundClearanceError}">
								<p class="oversizeError_${fn:escapeXml(index)}">${oversizeGroundClearanceError}</p>
							</c:if>
						</div>
					</div>
				</div>
				<div class="col-lg-12 col-xs-12 margin-bottom-20">
					<div class="unit-converter unit-converter--alt">
						<p class="label" aria-hidden="true">
							<spring:theme code="label.ferry.farefinder.vehicleInfo.adjustable" text="Adjustable" />
						</p>
						<div class="unit-converter__input-container mt-3">
							<div class="unit-converter__inputs">
						<label class="custom-radio-input">
							<form:radiobutton id="adjustable_yes_${index}" path="vehicleInfoForm.vehicleInfo[${index}].adjustable" value="${true}" checked="${adjustable == true ? 'checked' : '' }" class="y_fareFinderVehicleTypeBtn radioCode" />
							<span class="checkmark"></span>
							&emsp;&emsp;<spring:theme code="label.ferry.farefinder.vehicleInfo.adjustable.yes" text="Yes" />
						</label>
						&emsp;&emsp;
						<label class="custom-radio-input">
							<form:radiobutton id="adjustable_no_${index}" path="vehicleInfoForm.vehicleInfo[${index}].adjustable" value="${false}" checked="${adjustable == false ? 'checked' : '' }" class="y_fareFinderVehicleTypeBtn radioCode" />
							<span class="checkmark"></span>
							&emsp;&emsp;<spring:theme code="label.ferry.farefinder.vehicleInfo.adjustable.no" text="No" />
						</label>
						</div></div>
					</div>
				</div>
				</div>
		</div>
		<div id="showCallToBookDiv_${index}" class="row d-none">
			<div class="col-lg-12 col-md-12">
				<div>
					<p>
						<spring:theme code="label.ferry.farefinder.vehicleInfo.callToBook" text="Please call to book" />
					</p>
					<h4>
						<spring:theme code="label.ferry.farefinder.vehicleInfo.phoneNumber" text="1-888-BC FERRY (223-3779)" />
					</h4>
				</div>
			</div>
		</div>
		<div id="showCallToBookDivForGuestUser_${index}" class="row d-none">
			<div class="col-lg-12  col-md-12">
				<div>
					<p>
						<spring:theme code="label.ferry.farefinder.vehicleInfo.callToBook" text="Please call to book" />
					</p>
					<h4>
						<spring:theme code="label.ferry.farefinder.vehicleInfo.phoneNumber" text="1-888-BC FERRY (223-3779)" />
					</h4>
				</div>
			</div>
		</div>
		</div>
	</div>
</div>
