<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="cms" uri="http://hybris.com/tld/cmstags"%>
<%@ taglib prefix="ycommerce" uri="http://hybris.com/tld/ycommercetags"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ attribute name="formPrefix" required="true" type="java.lang.String"%>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags"%>
<%@ attribute name="bcfFareFinderForm" required="true" type="com.bcf.bcfvoyageaddon.forms.cms.BCFFareFinderForm"%>
<%@ attribute name="vehicleInfoForm" required="true" type="com.bcf.bcfvoyageaddon.forms.cms.VehicleInfoForm"%>
<%@ attribute name="idPrefix" required="true" type="java.lang.String"%>
<%@ attribute name="index" required="true" type="java.lang.String"%>
<%@ taglib prefix="additionalInfo" uri="additionalInfoTag"%>
<spring:htmlEscape defaultHtmlEscape="false" />
<div class="collapsible_${index}">
	<i class="bcf bcf-icon-std-car dir-right bcf-2x bcf-bule-icon"></i>
	<spring:theme code="text.ferry.farefinder.vehicletype.standard" text="Standard and Oversized" />
</div>
<div class="content">
	<p class="small-text mt-3 mb-5">
		<spring:theme code="text.ferry.farefinder.vehicletype.standard.details" text="Standard Vehicle under 5,500 kg including cars, trucks, SUVs, vans, motorhomes and vehicles pulling trailers." />
	</p>
	<p class="margin-top-30" for="standardVehicleHeight">
		<strong> <spring:theme code="text.ferry.farefinder.vehicletype.height" text="Vehicle Height" /> </strong>
		<a href="javascript:;" class="popoverThis" data-container="body" data-toggle="popover" data-placement="bottom" data-html="true"
        	data-content="<additionalInfo:additionalInfo code="text.farefinder.additionalinfo.vehicleheight" />">
        <span class="bcf bcf-icon-info-solid"></span>
        </a>
	</p>

    <c:choose>
        <c:when test="${not empty vehicleInfoForm.vehicleInfo[index].vehicleType.category}">
        <c:set var="vehicleType" value="${vehicleInfoForm.vehicleInfo[index].vehicleType.code}_${fn:toLowerCase(vehicleInfoForm.vehicleInfo[index].vehicleType.category)}"></c:set>
        </c:when>
        <c:otherwise>
            <c:set var="vehicleType" value="${vehicleInfoForm.vehicleInfo[index].vehicleType.code}"></c:set>
        </c:otherwise>
    </c:choose>
    <c:set var="vehicleHeight" value="${vehicleInfoForm.vehicleInfo[index].height}"></c:set>
    <c:set var="vehicleLength" value="${vehicleInfoForm.vehicleInfo[index].length}"></c:set>

    <div class="row">
		<div class="col-xs-12">
			<div class="vehicle-radio-box">
				<label for="under7HeightRadbtn" class="custom-radio-input">
					<spring:theme code="label.ferry.farefinder.vehicleInfo.under7" text="Under7ft(2.13m)" />
					<form:radiobutton id="under7Height_${index}" path="vehicleInfoForm.vehicleInfo[${index}].vehicleType.code" value="UH_standard" class="y_fareFinderVehicleTypeBtn radioCode" name="radio" checked="${((fn:contains(vehicleType, 'UH') or fn:contains(vehicleType, 'OS')) and vehicleHeight <= 7) ? 'checked' : '' }"/>
					<span class="checkmark"></span>
				</label>
				<ul class="list-inline mb-0">
					<li>
						<i class="bcf bcf-icon-std-car bcf-2x"></i>
					</li>
					<li>
						<i class="bcf bcf-icon-truck bcf-2x"></i>
					</li>
					<li>
						<i class="bcf bcf-icon-suv bcf-2x"></i>
					</li>
					<li>
						<i class="bcf bcf-icon-van bcf-2x"></i>
					</li>
				</ul>
			</div>
		</div>
		<div class="col-xs-12">
			<div class="vehicle-radio-box">
				<label for="over7HeightRadbtn" class="custom-radio-input">
					<spring:theme code="label.ferry.farefinder.vehicleInfo.over7" text="Over 7ft(2.13m)" />
						<c:choose>
                            <c:when test="${(fn:contains(vehicleType, 'UH') or fn:contains(vehicleType, 'OS')) and vehicleHeight gt 7}">
                            	<form:radiobutton id="over7Height_${index}" path="vehicleInfoForm.vehicleInfo[${index}].vehicleType.code" value="OS_standard" name="radio" class="y_fareFinderVehicleTypeBtn radioCode" checked="${vehicleType == 'OS_standard' ? 'checked' : '' }" />
                            </c:when>
                            <c:otherwise>
                                <form:radiobutton id="over7Height_${index}" path="vehicleInfoForm.vehicleInfo[${index}].vehicleType.code" value="OS_standard" name="radio" class="y_fareFinderVehicleTypeBtn radioCode"  />
                            </c:otherwise>
                        </c:choose>

					<span class="checkmark"></span>
					<form:hidden path="vehicleInfoForm.travellingWithVehicle" value="true" />
				</label>
				<ul class="list-inline mb-0">
					<li>
                        <i class="bcf bcf-icon-van-cargo bcf-2x"></i>
                    </li>
                    <li>
                        <i class="bcf bcf-icon-suv-cargo bcf-2x"></i>
                    </li>
                    <li>
                        <i class="bcf bcf-icon-camper bcf-2x"></i>
                    </li>
                    <li>
                        <i class="bcf bcf-icon-rv bcf-2x"></i>
                    </li>
				</ul>
			</div>
			<div class="row mt-2 pl-5 unit-converter unit-converter--alt d-none" id="Over7heightDiv_${index}">
				<div class="col-md-11 col-md-offset-1" id="vehicle_height_inputs">
				    <p id="heightft_${index}" for="heightInput" class="label" aria-hidden="true">
                        <spring:theme code="label.ferry.farefinder.vehicleInfo.totalHeight" text="Total Height" />
                    </p>
                     <div class="unit-converter__input-container">
					<div class="unit-converter__inputs">
						<input id="Over7heightboxinft_${index}" value="${vehicleHeight}" class="fareheightInput js-convert-units non-spinner" step="any" type="number" />
						<label id="heightf_${index}" for="heightInput">
							<span aria-hidden="true"><spring:theme code="label.ferry.farefinder.vehicleInfo.feet" text="ft" /> </span>
						</label>
					</div>
					<div class="unit-converter__inputs">
						<input id="heightinm_${index}" value="0" class="js-convert-units non-spinner  fareheightInputInMtrs" step="any" type="number" />
						<label id="heightm_${index}" for="lengthInput">
							<span aria-hidden="true"><spring:theme code="label.ferry.farefinder.vehicleInfo.meter" text="m" /></span>
						</label>
					</div>
					</div>
					<c:set var="standardHeightError">
						<form:errors path="${fn:escapeXml(formPrefix)}vehicleInfoForm.vehicleInfo[${fn:escapeXml(index)}].standardVehicleHeightError" />
					</c:set>
					<c:if test="${not empty standardHeightError}">
						<p class="standardError_${fn:escapeXml(index)}">${standardHeightError}</p>
					</c:if>
				</div>
				<div class="col-lg-12 col-xs-12 js-standard-height-limited-sailing-warning_${index} hidden">
					<p class="col-sm-12 warning-msg bcf-icon-text" id="limitedSilings">
						<i class="bcf bcf-icon-notice-outline bcf-2x bcf-vertical-middle" aria-hidden="true"></i>
						<spring:theme code="text.ferry.limited.sailings.message" text="Your sailings may be limited based on your input criteria." />
					</p>
				</div>
			</div>
		</div>
	</div>
	<hr>
	<p for="standardVehicleLength">
		<strong><spring:theme code="text.ferry.farefinder.vehicletype.length" text="Vehicle Length" /> </strong>
		 <a href="javascript:;" data-container="body" class="popoverThis" data-toggle="popover" data-placement="bottom" data-html="true"
                 	data-content="<additionalInfo:additionalInfo code="text.farefinder.additionalinfo.vehiclelength" />">
                 <span class="bcf bcf-icon-info-solid"></span>
                 </a>
	</p>
	<div class="row">
		<div class="input-required-wrap">
				<div class="col-xs-12">
					<div class="vehicle-radio-box">
						<label for="under20LengthRadbtn" class="custom-radio-input">
							<spring:theme code="label.ferry.farefinder.vehicleInfo.under20" text="Under 20ft(6.10 m)" />
							 	<c:choose>
                                	<c:when test="${(fn:contains(vehicleType, 'UH') or fn:contains(vehicleType, 'OS')) and vehicleLength <= 20}">
                                    	<input type="radio" id="under20Length_${index}" value="underLength" name="vehicleInfoForm.vehicleInfo[${index}].standardOversizeLengthOpted" class="y_fareFinderVehicleTypeBtn"  checked='checked'/> <span class="checkmark"></span>
                                    </c:when>
                                    <c:otherwise>
                                        <input type="radio" id="under20Length_${index}" value="underLength" name="vehicleInfoForm.vehicleInfo[${index}].standardOversizeLengthOpted" class="y_fareFinderVehicleTypeBtn"  /> <span class="checkmark"></span>
                                    </c:otherwise>
                                </c:choose>
						</label>
						<ul class="list-inline mb-0">
					<li>
						<i class="bcf bcf-icon-std-car bcf-2x"></i>
					</li>
					<li>
						<i class="bcf bcf-icon-truck bcf-2x"></i>
					</li>
					<li>
						<i class="bcf bcf-icon-suv bcf-2x"></i>
					</li>
					<li>
						<i class="bcf bcf-icon-van bcf-2x"></i>
						</li>
						</ul>
					</div>
				</div>
				<div class="col-xs-12">
					<div class="vehicle-radio-box">
						<label for="over20LengthRadbtn" class="custom-radio-input">
							<spring:theme code="label.ferry.farefinder.vehicleInfo.over20" text="Over 20 ft(6.10 m)" />
							<c:choose>
								<c:when test="${(fn:contains(vehicleType, 'UH') or fn:contains(vehicleType, 'OS')) and vehicleLength gt 20}">
                                	<input type="radio" id="over20Length_${index}" value="overLength" name="vehicleInfoForm.vehicleInfo[${index}].standardOversizeLengthOpted" class="y_fareFinderVehicleTypeBtn"  checked='checked'/> <span class="checkmark"></span>
                                </c:when>
                                <c:otherwise>
                                    <input type="radio" id="over20Length_${index}" value="overLength" name="vehicleInfoForm.vehicleInfo[${index}].standardOversizeLengthOpted" class="y_fareFinderVehicleTypeBtn"/> <span class="checkmark"></span>
                                </c:otherwise>
                            </c:choose>

						</label>
						<ul class="list-inline">
					<li>
                        <i class="bcf bcf-icon-truck-trailer bcf-2x"></i>
                    </li>
                    <li>
                        <i class="bcf bcf-icon-truck-boat bcf-2x"></i>
                    </li>
                    <li>
                        <i class="bcf bcf-icon-large-rv bcf-2x"></i>
                    </li>
						</ul>
					</div>
					<div class="row mt-2 pl-5  unit-converter unit-converter--alt d-none" id="Over20lengthDiv_${index}">
						<div class="col-md-11 col-md-offset-1" id="vehicle_length_inputs">
						    <p id="lengthft_${index}" class="label block-text" aria-hidden="true">
                                <spring:theme code="label.ferry.farefinder.vehicleInfo.totalLength" text="Total Length" />
                            </p>
                            <div class="unit-converter__input-container">
							<div class="unit-converter__inputs">
								<input id="Over20lengthboxinft_${index}" value="${vehicleLength}" class="js-convert-units non-spinner farelengthInput" step="any" type="number" />
								<label id="lengthf_${index}" for="lengthInput">
									<span class="sr-only">feet</span><span aria-hidden="true"><spring:theme code="label.ferry.farefinder.vehicleInfo.feet" text="ft" /> </span>
								</label>
							</div>
							<div class="unit-converter__inputs mt-2">
								<input id="lengthboxinm_${index}" value="0" class="js-convert-units non-spinner farelengthInputInMtrs" step="any" type="number" />
								<label id="lengthm_${index}" for="lengthInput">
									<span aria-hidden="true"><spring:theme code="label.ferry.farefinder.vehicleInfo.meter" text="m" /></span>
								</label>
							</div>
							</div>
							<c:set var="standardLengthError">
								<form:errors path="${fn:escapeXml(formPrefix)}vehicleInfoForm.vehicleInfo[${fn:escapeXml(index)}].standardVehicleLengthError" />
							</c:set>
							<c:if test="${not empty standardLengthError}">
								<p class="standardError_${fn:escapeXml(index)}">${standardLengthError}</p>
							</c:if>
						</div>
						<div class="col-lg-12 col-xs-12 js-standard-length-limited-sailing-warning_${index} hidden">
							<p class="col-sm-12 warning-msg mt-0 bcf-icon-text" id="limitedSilings">
								<i class="bcf bcf-icon-notice-outline bcf-2x bcf-vertical-middle" aria-hidden="true"></i>
								<spring:theme code="text.ferry.limited.sailings.message" text="Your sailings may be limited based on your input criteria." />
							</p>
						</div>
					</div>
				</div>
		</div>
	</div>
</div>
