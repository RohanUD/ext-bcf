<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="ycommerce" uri="http://hybris.com/tld/ycommercetags"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ attribute name="vehicleTypeQuantityList" required="true" type="java.util.List"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ attribute name="formPrefix" required="true" type="java.lang.String"%>
<%@ attribute name="idPrefix" required="true" type="java.lang.String"%>
<%@ attribute name="returnWithDifferentVehicle" type="java.lang.Boolean"%>
<spring:htmlEscape defaultHtmlEscape="false" />
<input type="hidden" id="${idPrefix }skipVehicle" value="${vehicleToSkip}">
<c:forEach var="entry" items="${vehicleTypeQuantityList}" varStatus="i">
	<div class="row ${idPrefix }vehicle_${i.index} ${i.index==1 && returnWithDifferentVehicle==false?'hidden':''}">
		<div class="row radio-button-row-fr mt-3">
			<div class="col-md-12">
				<form:radiobutton id="${idPrefix }y_standardRadbtn_${i.index}" path="${fn:escapeXml(formPrefix)}vehicleInfo[${i.index}].vehicleType.category" value="standard" class="${idPrefix }y_fareFinderVehicleTypeBtn" />
				<label for="standardVehicleRadbtn"> <spring:theme code="text.cms.farefinder.vehicletype.standard" text="Standard vehicle" />
				</label>
				<form:radiobutton id="${idPrefix }y_commercialRadbtn_${i.index}" path="${fn:escapeXml(formPrefix)}vehicleInfo[${i.index}].vehicleType.category" value="nonstandard" class="${idPrefix }y_fareFinderVehicleTypeBtn" />
				<label for="commercialVehicleRadbtn"> <spring:theme code="text.cms.farefinder.vehicletype.nonstandard" text="NonStandard vehicle" />
				</label>
				<form:radiobutton id="${idPrefix }y_overSizeRadbtn_${i.index}" path="${fn:escapeXml(formPrefix)}vehicleInfo[${i.index}].vehicleType.category" value="oversize" class="${idPrefix }y_fareFinderVehicleTypeBtn" />
				<label for="motorcycleVehicleRadbtn"> <spring:theme code="text.cms.farefinder.vehicletype.oversized" text="Oversize vehicle" />
				</label> <input type="hidden" value="${entry.vehicleType.category}" id="${idPrefix }y_category_${i.index}" />
			</div>
			<div class="${idPrefix }y_vehicleInfo[${i.index}].vehicleType.categoryError col-md-12 vehicle-error text-left"></div>
		</div>
		<div class="row">
			<div class="col-xs-12 col-sm-3">
				<label for="lengthInput"> &nbsp;</label>
				<form:select class="form-control ${idPrefix }y_subVehicle y_vehicleSubTypeSelect_${i.index}" id="${idPrefix }y_vehicleSubTypeSelect_${i.index}" path="${fn:escapeXml(formPrefix)}vehicleInfo[${i.index}].vehicleType.code" cssErrorClass="fieldError">
					<form:option value="-1" disabled="true" selected="selected"> please select </form:option>
					<c:choose>
						<c:when test="${not empty vehicles}">
							<c:forEach var="vehicleEntry" items="${vehicles}" varStatus="ii">
								<form:option class="${fn:escapeXml(vehicleEntry.category)} ${entry.vehicleType.category == vehicleEntry.category ?'':'hidden'} " value="${fn:escapeXml(vehicleEntry.code)}"> ${fn:escapeXml(vehicleEntry.name)} </form:option>
							</c:forEach>
						</c:when>
						<c:otherwise>
							<form:options items="${defaultList}" htmlEscape="true" />
						</c:otherwise>
					</c:choose>
				</form:select>
				<div class="y_vehicleInfo[${i.index}].vehicleType.codeError col-md-12 vehicle-error text-left"></div>
				<form:input class="form-control" path="${fn:escapeXml(formPrefix)}vehicleInfo[${i.index}].qty" type="hidden" value="1" autocomplete="off" />
				<div class="container p-0">
					<div class="livestockCheckBox">
						<label id="${idPrefix }livestockinbound"> <form:checkbox id="${idPrefix }livestockinbound" path="${fn:escapeXml(formPrefix)}vehicleInfo[${i.index}].carryingLivestock" class="y_fareFinderVehicleTypeBtn" /> <spring:theme code="text.cms.farefinder.livestock" text="Carrying Livestock" />
						</label>
					</div>
					<div class="${idPrefix }sidecarCheckBox_${i.index}  ${entry.vehicleType.code == 'MC' ?'':'hidden'}">
						<label id="${idPrefix }sideCarLabel"> <form:checkbox id="${idPrefix }sidecarCheckBox_${i.index}" path="${fn:escapeXml(formPrefix)}vehicleInfo[${i.index}].vehicleWithSidecarOrTrailer" class="" /> <spring:theme code="text.cms.farefinder.sidecar" text="Vehicle With Sidecar/Trailer" />
						</label>
					</div>
				</div>
			</div>
			<c:set var="skipVehicle" value="false" />
			<c:forEach var="vehicleCode" items="${vehicleToSkip}">
				<c:if test="${vehicleCode eq entry.vehicleType.code}">
					<c:set var="skipVehicle" value="true" />
				</c:if>
			</c:forEach>
			<div class="${idPrefix }vehicleDimensions_${i.index} ${skipVehicle ?'hidden':''}">
				<div class="input-required-wrap col-xs-12 col-sm-2">
					<label class="col-md-12 p-0" for="lengthInput"> Length</label>
					<form:input id="${idPrefix }lengthInput_${i.index}" class="form-control col-md-6 ${idPrefix }lengthInput" path="${fn:escapeXml(formPrefix)}vehicleInfo[${i.index}].length" type="number" autocomplete="off" />
					<div class="col-md-6 p-0 mt-4 blue-color">Ft</div>
					<p class="col-md-12 p-0" id="${idPrefix }lengthInMtrs_${i.index}"></p>
					<div class="y_vehicleInfo[${i.index}].lengthError col-md-12 vehicle-error"></div>
				</div>
				<div class="input-required-wrap col-xs-12 col-sm-2">
					<label class="col-md-12 p-0" for="heightInput"> Height</label>
					<form:input id="${idPrefix }heightInput_${i.index}" class="form-control col-md-6 ${idPrefix }heightInput" path="${fn:escapeXml(formPrefix)}vehicleInfo[${i.index}].height" type="number" autocomplete="off" />
					<div class="col-md-6 p-0 mt-4 blue-color">Ft</div>
					<p class="col-md-12 p-0" id="${idPrefix }heightInMtrs_${i.index}"></p>
					<div class="y_vehicleInfo[${i.index}].heightError col-md-12 vehicle-error"></div>
				</div>
				<div class="input-required-wrap col-xs-12 col-sm-2">
					<label class="col-md-12 p-0" for="widthInput"> Width</label>
					<form:input id="${idPrefix }widthInput_${i.index}" class="form-control col-md-6 ${idPrefix }widthInput" path="${fn:escapeXml(formPrefix)}vehicleInfo[${i.index}].width" type="number" autocomplete="off" />
					<div class="col-md-6 p-0 mt-4 blue-color">Ft</div>
					<p class="col-md-12 p-0" id="${idPrefix }widthInMtrs_${i.index}"></p>
					<div class="y_vehicleInfo[${i.index}].widthError col-md-12 vehicle-error"></div>
				</div>
				<div>Please round vehicle dimensions to the nearest foot.</div>
			</div>
			<div class="row input-row"></div>
			<label id="${idPrefix }vehicledangerous" class="hidden"> <input type="checkbox" id="${idPrefix }y_dangerous_${i.index}" class="y_fareFinderVehicleTypeBtn" /> <spring:theme code="text.cms.farefinder.vehicletype.Dangerousgoods" text="Dangerous goods" />
			</label>
			<form:hidden id="${idPrefix }vehicleTravelRouteCode_${i.index}" class="form-control" path="${fn:escapeXml(formPrefix)}vehicleInfo[${i.index}].travelRouteCode" />
		</div>
	</div>
</c:forEach>
