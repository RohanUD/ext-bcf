<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="cms" uri="http://hybris.com/tld/cmstags"%>
<%@ taglib prefix="ycommerce" uri="http://hybris.com/tld/ycommercetags"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ attribute name="formPrefix" required="true" type="java.lang.String"%>
<%@ taglib prefix="vehicle" tagdir="/WEB-INF/tags/addons/bcfvoyageaddon/responsive/vehicle"%>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags"%>
<%@ taglib prefix="popupmodals" tagdir="/WEB-INF/tags/addons/bcfvoyageaddon/responsive/popupmodals"%>
<%@ taglib prefix="modal" tagdir="/WEB-INF/tags/addons/bcfvoyageaddon/responsive/passenger"%>
<%@ taglib prefix="additionalInfo" uri="additionalInfoTag"%>
<%@ attribute name="bcfFareFinderForm" required="true" type="com.bcf.bcfvoyageaddon.forms.cms.BCFFareFinderForm"%>
<%@ attribute name="vehicleInfoForm" required="true" type="com.bcf.bcfvoyageaddon.forms.cms.VehicleInfoForm"%>
<%@ attribute name="idPrefix" required="true" type="java.lang.String"%>
<%@ attribute name="isFareFinderComponent" type="java.lang.Boolean"%>
<spring:htmlEscape defaultHtmlEscape="false" />


<div id="vehicleSelectionDiv" style="display:${bcfFareFinderForm.passengerInfoForm.travellingAsWalkOn ? 'none' : 'block'}">
    <a data-toggle="modal" href="#journeydetailseditModal">
        <span class="edit-icon"></span>
    </a>
    <c:forEach var="entry" items="${vehicleInfoForm.vehicleInfo}" varStatus="i">
        <div class="margin-top-30 farevehicle_${i.index} ${i.index==1 && vehicleInfoForm.returnWithDifferentVehicle==false?'hidden':''}">
            <div class="container">
            <div class="row">
                <div class="col-lg-12 col-xs-12 mt-3">
                    <form:errors path="vehicleInfoForm.vehicleInfo[${i.index}].vehicleType.code" cssClass="alert alert-danger" element="div"/>
                    <form:errors path="vehicleInfoForm.travellingWithVehicle" cssClass="alert alert-danger" element="div"/>
                </div>
            </div>
            </div>
            <div class="container">
              <div class="row">
              <div class="col-lg-8 col-md-8 col-xs-12">
                <h4 class="mb-3">
                        <strong><spring:theme code="label.ferry.farefinder.vehicle.heading" text="Vehicle" /></strong>
                    </h4>
                    <p>
                        <spring:theme code="label.ferry.farefinder.vehicle.details" text="Specific measurements based on your car type are required to provide accurate deck space availability." />
                    </p>
              </div>
                <div class="col-md-5 col-lg-5 col-xs-12">
                    <div class="alert alert-danger hidden form-error-messages error-message-${i.index}"></div>
                    <div id="accordion_${i.index}" class="vehicle-custom-accordion">
                        <vehicle:standardVehicleInfo bcfFareFinderForm="${bcfFareFinderForm}" vehicleInfoForm="${bcfFareFinderForm.vehicleInfoForm}" formPrefix="${formPrefix}" index="${i.index}" idPrefix="fare" />
                        <vehicle:fareCalculatorMotorcycleVehicleInfo bcfFareFinderForm="${bcfFareFinderForm}" vehicleInfoForm="${bcfFareFinderForm.vehicleInfoForm}" formPrefix="${formPrefix}" index="${i.index}" idPrefix="fare" />
                        <vehicle:fareCalculatorOver5500KgVehicleInfo bcfFareFinderForm="${bcfFareFinderForm}" vehicleInfoForm="${bcfFareFinderForm.vehicleInfoForm}" formPrefix="${formPrefix}" index="${i.index}" idPrefix="fare" />
                        <div class="collapsible ">
                            <i class="bcf bcf-icon-bus bcf-2x icon-bus-sm bcf-bule-icon"></i>
                            <spring:theme code="label.ferry.farefinder.vehicleInfo.bus" text="Bus" />
                        </div>
                        <div class="content">
                            <p>
                                <spring:theme code="text.ferries.bus.label.fareCalculator" text="Sailing options may be limited based on your input requirements." />
                            </p>
                        </div>
                    </div>
                </div>
            </div>
            </div>
        </div>
        <form:input class="form-control" path="vehicleInfoForm.vehicleInfo[${i.index}].qty" type="hidden" value="1" autocomplete="off" />
        <form:input id="height_${i.index}" class=" fareheightInput js-convert-units non-spinner" step="any" type="hidden" path="${fn:escapeXml(formPrefix)}vehicleInfoForm.vehicleInfo[${fn:escapeXml(i.index)}].height" />
        <form:input id="length_${i.index}" class="js-convert-units non-spinner farelengthInput" step="any" type="hidden" path="${fn:escapeXml(formPrefix)}vehicleInfoForm.vehicleInfo[${fn:escapeXml(i.index)}].length" />
        <c:if test="${vehicleInfoForm.vehicleInfo[1] ne null && i.index == 0 && !isFareFinderComponent}">
        <div id="different-vehicle-inbound-btn-div" class="container">
        <div class="row">
            <div class="col-md-12 col-xs-12">
                <hr>
                <div class="faredifferentVehicleCheckBox returnVehicleCheckbox_${i.index} ${bcfFareFinderForm.routeInfoForm.tripType=='RETURN' ?'':'hidden'}">
                    <label class="custom-checkbox-input" id="vehicleinbound">
                        <form:checkbox id="farevehicleinboundcheckbox" path="${fn:escapeXml(formPrefix)}vehicleInfoForm.returnWithDifferentVehicle" class="y_fareFinderVehicleTypeBtn" />
                        <spring:theme code="text.cms.farefinder.vehicletype.returnvehicle" text="Different vehicle for return journey" />
                        <span class="checkmark-checkbox"></span>
                    </label>
                </div>
            </div>
            </div>
        </div>
        </c:if>
    </c:forEach>
    <div>
    <modal:limitedferrymodal/>
    <modal:nonBookableVehicleError/>
    <popupmodals:dangerousGoodsModal />
    <popupmodals:vehicleDescriptionModal />
    <popupmodals:standardVehicleHeightModal />
    <popupmodals:standardVehicleLengthModal />
    <popupmodals:oversizeVehicleDetailsModal />
    <popupmodals:liveStockModal />
    </div>
    <c:set var="isAnonymousUser" value="${false}"/>
    <sec:authorize access="hasAnyRole('ROLE_ANONYMOUS')">
        <c:set var="isAnonymousUser" value="${true}"/>
    </sec:authorize>
    <input type="hidden" id="isAnonymousUser" value="${isAnonymousUser}">
    <input type="hidden" id="standardVehicleLengthPortLimit" value="${bcfFareFinderForm.vehicleInfoForm.standardVehicleLengthPortLimit}">
    <input type="hidden" id="standardVehicleHeightPortLimit" value="${bcfFareFinderForm.vehicleInfoForm.standardVehicleHeightPortLimit}">
    <input type="hidden" id="over5500VehicleLengthPortLimit" value="${bcfFareFinderForm.vehicleInfoForm.over5500VehicleLengthPortLimit}">
    <input type="hidden" id="over5500VehicleHeightPortLimit" value="${bcfFareFinderForm.vehicleInfoForm.over5500VehicleHeightPortLimit}">
    <input type="hidden" id="over5500VehicleLengthWarningLimit" value="${bcfFareFinderForm.vehicleInfoForm.over5500VehicleLengthWarningLimit}">
    <input type="hidden" id="over5500VehicleHeightWarningLimit" value="${bcfFareFinderForm.vehicleInfoForm.over5500VehicleHeightWarningLimit}">
    <input type="hidden" id="vehicleWidthPortLimit" value="${bcfFareFinderForm.vehicleInfoForm.vehicleWidthPortLimit}">

</div>
