<%@ tag language="java" pageEncoding="UTF-8"%>
<%@ attribute name="reservationDataList" required="true" type="com.bcf.facades.reservation.data.ReservationDataList"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="bookingDetails" tagdir="/WEB-INF/tags/addons/bcfstorefrontaddon/responsive/bookingdetails"%>
<div class="booking-confirm-box text-center margin-bottom-40"> 
	<div class="booking-confirm-text mb-3">
        <span class="bcf bcf-icon-checkmark confirm-check"></span>
		<h4>
			<spring:theme code="text.booking.confirmation.detailed.text" text="Your booking has been confirmed." />
		</h4>
		<p>
			<spring:theme code="text.booking.confirmation.detailed.paragraph" text="A confirmation has been sent to your email address." />
		</p>
		<c:if test="${showAdditionalMessages}">
			<c:if test="${not empty reservationDataList.operatorDisplayMessage}">
			<p>
				<spring:theme code="text.booking.confirmation.operator.display.msg" text="Operator display message" /> : ${reservationDataList.operatorDisplayMessage}
			</p>
			</c:if>
			<c:if test="${not empty reservationDataList.approvalOrDeclineMessage}">
			<p>
                <spring:theme code="text.booking.confirmation.approval.or.decline.msg" text="Approval or decline message" /> : ${reservationDataList.approvalOrDeclineMessage}
            </p>
            </c:if>
			<c:if test="${reservationDataList.paymentProcessingMethod eq 'PINPAD'}">
				<div class="row">
					<div class="col-md-offset-4 col-md-4">
						<a href="${contextPath}/payment/receipt/order/${bookingReference}" class="btn btn-primary btn-block" id="print-receipt-link">
							<spring:theme code="text.booking.confirmation.print.receipt" text="Print receipt" />
						</a>
					</div>
				</div>
			</c:if>
		</c:if>
	</div>
</div>

<c:if test="${reservationDataList != null}">
	<c:forEach var="reservation" items="${reservationDataList.reservationDatas}">
		<c:if test="${reservation.bookingStatusCode ne 'CANCELLED' && reservation != null}">
			<c:forEach var="reservationItem" items="${reservation.reservationItems}">
				<div class="journey-wrapper margin-bottom-none">
					<div class="panel-heading departure-heading margin-bottom-20">
						<c:set var="transportOfferings" value="${reservationItem.reservationItinerary.originDestinationOptions[0].transportOfferings}" />
						<c:set var="originSector" value="${transportOfferings[0].sector}" />
						<c:set var="item" value="${reservationItem}" />
						<c:set var="finalSector" value="${transportOfferings[fn:length(transportOfferings) - 1].sector}" />
						<h5 class="departure-heading text-center my-3">
							<c:choose>
								<c:when test="${reservationItem.reservationItinerary.originDestinationOptions[0].originDestinationRefNumber == 0}">
									<fmt:formatDate var="departureDate" pattern="EEE, MMM dd" value="${item.reservationItinerary.originDestinationOptions[0].transportOfferings[0].departureTime}" />
									<strong><spring:theme code="label.payment.reservation.transport.departure.sailing.time.prefix" /></strong>: <span class="departure-date">${departureDate}</span>
									</c:when>
								<c:when test="${reservationItem.reservationItinerary.originDestinationOptions[0].originDestinationRefNumber == 1}">
									<fmt:formatDate var="returnDate" pattern="EEE, MMM dd" value="${item.reservationItinerary.originDestinationOptions[0].transportOfferings[fn:length(transportOfferings) - 1].departureTime}" />
									<strong><spring:theme code="label.payment.reservation.transport.return.sailing.time.prefix" /></strong>: <span class="departure-date">${returnDate}</span>
									</c:when>
							</c:choose>
						</h5>
					</div>

					

                  <div class="row">
                     <div class="col-md-offset-3 col-md-6">
                        <div class="row">
                           <div class="clearfix"></div>
                           <div class="col-lg-5 col-xs-5 text-center">
                              <fmt:formatDate var="departureTime" pattern="HH:mm a" value="${item.reservationItinerary.originDestinationOptions[0].transportOfferings[0].departureTime}" />
                              <p class="fnt-14 mb-0">Depart: <span class="time-format">${departureTime}<span></p>
                              <h5 class="font-weight-bold mb-1">${fn:escapeXml(originSector.origin.location.name)}</h5>
                              <p>${fn:escapeXml(originSector.origin.name)}</p>
                           </div>
                           <div class="col-lg-2 col-xs-2 text-center">
							  <span class="bcf bcf-icon-single-arrows-right booking-tripType-icon icon-blue"></span>
                           </div>
                           <div class="col-lg-5 col-xs-5 text-center">
                              <fmt:formatDate var="arrivalTime" pattern="HH:mm a" value="${item.reservationItinerary.originDestinationOptions[0].transportOfferings[fn:length(transportOfferings) - 1].arrivalTime}" />
                              <p class="fnt-14 mb-0">Arrival: <span class="time-format">${arrivalTime}</span></p>
                              <h5 class="font-weight-bold mb-1">${fn:escapeXml(finalSector.destination.location.name)}</h5>
                              <p>(${fn:escapeXml(finalSector.destination.name)})</p>
                           </div>
                        </div>
                     </div>
                  </div>
				<div>
					<p class="text-center text-center margin-top-20 margin-bottom-30">
						<spring:theme code="label.booking.confirmation.booking.reference.prefix" text="Confirmation" />
						#: <strong>${reservationItem.bcfBookingReference}</strong>
					</p>
				</div>

				<hr/>
					
				</div>
			</c:forEach>
				<div class="row">
					<div class="col-md-offset-2 col-md-8 col-xs-12">
						<bookingDetails:termsAndConditions/>
					</div>
				</div>
				<c:if test="${not empty reservation.reservationItems[0].reservationItinerary.route.travelAgentWaitListLink || not empty reservation.reservationItems[0].reservationItinerary.route.waitListLink}">
				<div>
					<div class="container waitlist-wrapper bg-gray text-center padding-top-30 padding-bottom-30 margin-bottom-40">
						<div class="row">
							<div class="col-md-offset-2 col-md-8 col-xs-12">
                    <c:if test="${not empty reservation.reservationItems[0].reservationItinerary.route.waitListLink}">
                     <h4><strong><spring:theme code="text.booking.confirmation.waitlist.header" /></strong></h4>
                     <p class="sailing-info-text text-sm"><spring:theme code="text.booking.confirmation.waitlist.title"/></p>
                     <a href="${reservation.reservationItems[0].reservationItinerary.route.waitListLink}" target="_blank" class="btn btn-secondary btn-block margin-bottom-20">
                         
                            <c:choose>
                            <c:when test="${reservation.reservationItems[0].reservationItinerary.route.routeType eq 'LONG'}">
                            <spring:theme code="text.booking.confirmation.north.coast.waitlist.title" />
                            </c:when>
                            <c:otherwise>
                            <spring:theme code="text.booking.confirmation.southern.gulf.waitlist.title" />
                            </c:otherwise>
                            </c:choose>
                         
                     </a>
                     </c:if>

                    <c:if test="${not empty reservation.reservationItems[0].reservationItinerary.route.travelAgentWaitListLink}">
                        <a href="${reservation.reservationItems[0].reservationItinerary.route.travelAgentWaitListLink}" target="_blank" class="agent-link"><spring:theme code="label.listsailing.travel.agent.waitlist.link"/> </a>
                    </c:if>
					</div>

					</div>
					</div>
				</div>
				</c:if>
		</c:if>
	</c:forEach>
</c:if>


