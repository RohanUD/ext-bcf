<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ attribute name="formPrefix" required="true" type="java.lang.String"%>
<%@ taglib prefix="popupmodals" tagdir="/WEB-INF/tags/addons/bcfvoyageaddon/responsive/popupmodals"%>
<%@ taglib prefix="passenger" tagdir="/WEB-INF/tags/addons/bcfvoyageaddon/responsive/passenger"%>
<%@ taglib prefix="modal" tagdir="/WEB-INF/tags/addons/bcfvoyageaddon/responsive/passenger"%>
<%@ taglib prefix="additionalInfo" uri="additionalInfoTag"%>
<spring:htmlEscape defaultHtmlEscape="false" />
<div class="row mb-3">
	<c:forEach var="entry" items="${bcfFareFinderForm.passengerInfoForm.inboundPassengerTypeQuantityList}" varStatus="j">
		<c:if test="${fn:contains(bcfFareFinderForm.passengerInfoForm.basePassengers, entry.passengerType.code)}">
			<div class="col-xs-6 col-md-2 pr-4 y_passengerWrapper ${entry.passengerType.routeType}">
			<spring:theme code="label.ferry.farefinder.passengerinfo.passenger.${entry.passengerType.code}"/><br/>
				<label for="y_${fn:escapeXml(entry.passengerType.code)}"> ${fn:escapeXml(entry.passengerType.name)} </label>
				<c:if test="${entry.passengerType.infoRequired}">
                    <a href="javascript:;" class="popoverThis" data-container="body" data-toggle="popover" data-placement="bottom" data-html="true"
                    	data-content="<additionalInfo:additionalInfo code="text.farefinder.additionalinfo.ageinformation.${entry.passengerType.code}" />">
                    	<span class="bcf bcf-icon-info-solid"></span>
                    </a>
				</c:if>
				<div class="input-group y_inboundPassengerQtySelector text-center">
					<span class="input-group-btn">
						<button class="btn btn-minus y_inboundPassengerQtySelectorMinus" type="button" data-type="minus" data-field="">
							<span class="bcf bcf-icon-remove"></span>
						</button>
					</span>
					<form:input type="text" readonly="true" id="y_inboundPassengerQuantity" class="form-control passenger-quantity-return yr_${fn:escapeXml(entry.passengerType.code)}" value="${entry.quantity}" min="0" max="${bcfFareFinderForm.passengerInfoForm.maxPassengersAllowed}" path="${fn:escapeXml(formPrefix)}passengerInfoForm.inboundPassengerTypeQuantityList[${fn:escapeXml(j.index)}].quantity" />
					<spring:theme code="text.farefinder.additionalinfo.senior.id.info" var="tooltip"/>
					<span class="input-group-btn">
						<button class="${entry.passengerType.infoRequired ? 'popoverThis':''}  btn btn-plus y_inboundPassengerQtySelectorPlus y_${fn:escapeXml(entry.passengerType.code)}" type="button" data-type="plus" data-field="" data-container="body" data-placement="top" data-html="true" data-content="${fn:escapeXml(tooltip)}">
							<span class="bcf bcf-icon-add"></span>
						</button>
					</span>
					<form:input path="${fn:escapeXml(formPrefix)}passengerInfoForm.inboundPassengerTypeQuantityList[${fn:escapeXml(j.index)}].passengerType.code" id="y_passengerCode" type="hidden" readonly="true" />
					<form:input path="${fn:escapeXml(formPrefix)}passengerInfoForm.inboundPassengerTypeQuantityList[${fn:escapeXml(j.index)}].passengerType.name" id="y_passengerTypeName" type="hidden" readonly="true" />
					<form:input path="${fn:escapeXml(formPrefix)}passengerInfoForm.inboundPassengerTypeQuantityList[${fn:escapeXml(j.index)}].passengerType.minAge" type="hidden" readonly="true" />
					<form:input path="${fn:escapeXml(formPrefix)}passengerInfoForm.inboundPassengerTypeQuantityList[${fn:escapeXml(j.index)}].passengerType.maxAge" type="hidden" readonly="true" />
				</div>
			</div>
		</c:if>
	</c:forEach>
</div>
