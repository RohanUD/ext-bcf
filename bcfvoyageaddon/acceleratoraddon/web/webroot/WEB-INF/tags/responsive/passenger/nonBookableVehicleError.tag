<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<div class="modal fade" id="non-bookabale-veh-error-modal" tabindex="-1" role="dialog" aria-labelledby="non-bookabale-veh-error-modal">
	<div class="modal-dialog" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true"><i class="bcf bcf-icon-cancel-solid"></i></span>
				</button>
				<h3 class="modal-title" id="limitedSailingsLabel">
					<spring:theme code="fareselection.validation.incorrect" text="Incorrect selection" />
				</h3>
			</div>
			<div class="modal-body">
				<p class="col-sm-12 warning-msg mt-0 bcf-icon-text">
					<i class="bcf bcf-icon-alert bcf-2x bcf-vertical-middle" aria-hidden="true"></i>
					<spring:theme code="error.ferry.farefinder.vehicleInfo.non.bookable.vehicle.message" text="Please call 1-888-BC FERRY (1-888-223-3779) to book this type of vehicle." />
				</p>
			</div>
			<div class="modal-footer">
				<div class="row">
				<div class="col-xs-12 col-sm-6">
				</div>
					<div class="col-xs-12 col-sm-6">
						<button class="btn btn-secondary btn-block" data-dismiss="modal">
							<spring:theme code="popup.close" text="Close" />
						</button>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
