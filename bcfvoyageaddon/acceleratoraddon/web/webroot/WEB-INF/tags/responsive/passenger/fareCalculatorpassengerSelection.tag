<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="popupmodals" tagdir="/WEB-INF/tags/addons/bcfvoyageaddon/responsive/popupmodals"%>
<%@ taglib prefix="passenger" tagdir="/WEB-INF/tags/addons/bcfvoyageaddon/responsive/passenger"%>
<%@ taglib prefix="modal" tagdir="/WEB-INF/tags/addons/bcfvoyageaddon/responsive/passenger"%>
<%@ attribute name="formPrefix" required="true" type="java.lang.String"%>
<%@ taglib prefix="additionalInfo" uri="additionalInfoTag"%>
<%@ attribute name="isFareFinderComponent" type="java.lang.Boolean"%>
<spring:htmlEscape defaultHtmlEscape="false" />
	<c:set var="qtyMinus" value="0" />
	<c:choose>
		<c:when test="${not empty bcfFareFinderForm.passengerInfoForm.maxPassengersAllowed}">
			<c:set var="maxPassengerQuantity" value="${bcfFareFinderForm.passengerInfoForm.maxPassengersAllowed}" scope="session"/>
		</c:when>
		<c:otherwise>
			<c:set var="maxPassengerQuantity" value="${guestQuantity}" scope="session"/>
		</c:otherwise>
	</c:choose>
	<div id="maxPassengerCount" class="hidden">${maxPassengerQuantity}</div>
	<div id="contextPath" class="hidden">${contextPath}</div>
	<form:errors path="${fn:escapeXml(formPrefix)}passengerInfoForm.passengerTypeQuantityList" cssClass="alert alert-danger d-block" />


    <!-- outbound passenger -->
    <div>
    <div class="row passenger-selection-accordion">
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
            <div class="js-accordion js-accordion-first-active custom-contentpage-accordion">
                <h4>
                    <spring:theme code="label.ferry.farefinder.passengerinfo.base.passengers.heading" text="General Passenger(s)" />
                </h4>
                <div>
                    <div class="row margin-bottom-30">
                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                            <spring:theme code="label.ferry.farefinder.passengerinfo.passengers.description" text="Book up to nine passengers online. To book 10 or more passengers, please complete a" />
                            &nbsp;<a href="${pageContext.request.contextPath}/book-sailings/group-bookings" target="_blank" class="group-booking"><spring:theme code="label.ferry.farefinder.passengerinfo.passengers.description.group.booking" text="group booking form." /></a>
                        </div>
                    </div>
                    <passenger:fareCalculatorOutboundbasepassengers formPrefix="${formPrefix}" />
                </div>
            </div>
        </div>
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
            <div class="js-accordion js-accordion-default custom-contentpage-accordion">
              <passenger:fareCalculatorOutboundadditionalpassengers formPrefix="${formPrefix}"/>
            </div>
        </div>
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
            <div class="js-accordion js-accordion-default custom-contentpage-accordion">
                <passenger:fareCalculatorOutboundnorthernpassengers formPrefix="${formPrefix}" />
            </div>
        </div>
    </div>
	<h4 class="m-0">
    	<div id="PassengerCountalert" class="hidden"><spring:message code="text.passenger.maxallowed.reached"/></div>
    </h4>
	<div class="row">
		<div id="y_accessibilityPlaceholderParent" class="sr-only">
			<div id="y_accessibilityPlaceholder"></div>
			    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
				<p>
					<spring:theme code="text.ferry.farefinder.passengerinfo.accesibility.terms.line.1" />
				</p>
				<p>
					<spring:theme code="text.ferry.farefinder.passengerinfo.accesibility.terms.line.2" />
				</p>
				<p>
					<spring:theme code="text.ferry.farefinder.passengerinfo.accesibility.terms.line.3" />
				</p>
			</div>
		</div>
	</div>

	<div id="fareCalculatorOutboundWalkOnOptions" style="display:none">
        <passenger:fareCalculatorOutboundWalkOnOptions/>
	</div>

	<!-- travel mode selection -->

	<passenger:fareCalculatorTravelmodeselection formPrefix="${formPrefix}" />

	<!-- inbound Passenger start-->
	<c:if test="${bcfFareFinderForm.routeInfoForm.tripType == 'RETURN' && !isFareFinderComponent}">
		<div class="input-group mb-3 mt-4 different-passenger" id="different-passenger-inbound-btn-div">
			<label class="custom-checkbox-input" for="y_differentPassengerInReturn">
				<form:checkbox id="y_differentPassengerInReturn" path="${fn:escapeXml(formPrefix)}passengerInfoForm.returnWithDifferentPassenger" class="y_passengerReturn" />
				<spring:theme code="label.ferry.farefinder.passengerinfo.different.passenger.inbound.check" text="Different passengers on return trip" />
				<span class="checkmark-checkbox"></span>
			</label>
		</div>
	</c:if>
	<div id="y_returnPassenger" class="hidden mb-4 mt-4">
	<div class="margin-bottom-2">
	<h4><spring:theme code="label.ferry.farefinder.passengerinfo.inbound.passenger" text="Return Passengers" /></h4>
	</div>
	<div class="row passenger-selection-accordion">
		<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
            <div class="js-accordion js-accordion-first-active custom-contentpage-accordion">
                <h4>
                    <spring:theme code="label.ferry.farefinder.passengerinfo.base.passengers.heading" text="General Passenger(s)" />
                </h4>
                <div>
                    <div class="row margin-bottom-30">
                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                            <spring:theme code="label.ferry.farefinder.passengerinfo.passengers.description" text="Book up to nine passengers online. To book 10 or more passengers, please complete a" />
                            &nbsp;<a href="${pageContext.request.contextPath}/book-sailings/group-bookings" target="_blank" class="group-booking"><spring:theme code="label.ferry.farefinder.passengerinfo.passengers.description.group.booking" text="group booking form." /></a>
                        </div>
                    </div>
                    <passenger:fareCalculatorInboundbasepassengers formPrefix="${formPrefix}" />
                </div>
            </div>
        </div>



	<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
            <div class="js-accordion js-accordion-default custom-contentpage-accordion">
              <passenger:fareCalculatorInboundadditionalpassengers formPrefix="${formPrefix}" />
            </div>
        </div>

			<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
            <div class="js-accordion js-accordion-default custom-contentpage-accordion">
				<passenger:fareCalculatorInboundnorthernpassengers formPrefix="${formPrefix}" />
			</div>
        </div>
		</div>
		<div class="row">
			<div class=" col-lg-12">
				<div class="input-group">
					<label class="custom-checkbox-input show" for="y_checkReturnAccessibilityNeed">
						<form:checkbox id="y_checkReturnAccessibilityNeed" path="${fn:escapeXml(formPrefix)}passengerInfoForm.accessibilityNeedsCheckInbound" />
						<spring:theme code="label.ferry.farefinder.passengerinfo.accessibility.consent.checkbox.label" text="I have accessibility requirements" />
						<span class="checkmark-checkbox"></span>
					</label>
				</div>
			</div>
			<div id="y_accessibilityReturnPlaceholderParent" class="sr-only">
				<div id="y_accessibilityReturnPlaceholder"></div>
				<p>
					<spring:theme code="text.ferry.farefinder.passengerinfo.accesibility.terms.line.1" />
				</p>
				<p>
					<spring:theme code="text.ferry.farefinder.passengerinfo.accesibility.terms.line.2" />
				</p>
				<p>
					<spring:theme code="text.ferry.farefinder.passengerinfo.accesibility.terms.line.3" />
				</p>
			</div>
		</div>
		<c:if test="${bcfFareFinderForm.routeInfoForm.walkOnRoute}">
			<passenger:fareCalculatorInboundWalkOnOptions/>
		</c:if>
	</div>
	<!-- inbound Passenger end-->

	<!-- hidden attributes to support java-script -->
	<div id="js-farefinder-routeType" class="hidden">${bcfFareFinderForm.routeInfoForm.routeType}</div>
	<div id="js-farefinder-isWalkOnRoute" class="hidden">${bcfFareFinderForm.routeInfoForm.walkOnRoute}</div>
	<div id="js-farefinder-allowsWalkOnOptions" class="hidden">${bcfFareFinderForm.routeInfoForm.allowsWalkOnOptions}</div>

	<popupmodals:bcResidentModal/>
	<popupmodals:ageInformationModal />
	<popupmodals:dangerousGoodsModal />
	<popupmodals:largeItemsWalkOnModal />
	<popupmodals:northernBCResidentModal />
	<modal:limitedferrymodal/>
