<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="cms" uri="http://hybris.com/tld/cmstags"%>
<%@ taglib prefix="ycommerce" uri="http://hybris.com/tld/ycommercetags"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ attribute name="passengerTypeQuantityList" required="true"
	type="java.util.List"%>
<%@ attribute name="formPrefix" required="true" type="java.lang.String"%>
<spring:htmlEscape defaultHtmlEscape="false" />
<c:forEach var="entry" items="${passengerTypeQuantityList}" varStatus="i">
	<c:if test="${entry.passengerType.code == 'adult' || entry.passengerType.code == 'child' || entry.passengerType.code == 'infant' || entry.passengerType.code == 'senior'}">
		<div class="col-xs-3 col-sm-4 ${entry.passengerType.routeType == 'LONG' ? 'sr-only' : ''} y_passengerWrapper ${entry.passengerType.routeType}">
			<label for="y_${fn:escapeXml(entry.passengerType.code)}"> ${fn:escapeXml(entry.passengerType.name)} </label>
			<form:select class="form-control ${fn:escapeXml(entry.passengerType.code)} y_${fn:escapeXml(entry.passengerType.code)}Select" id="y_${fn:escapeXml(entry.passengerType.code)}" path="${fn:escapeXml(formPrefix)}passengerTypeQuantityList[${fn:escapeXml(i.index)}].quantity" cssErrorClass="fieldError">
				<form:option value="-1" disabled="true"> ${fn:escapeXml(entry.passengerType.name)} </form:option>
				<c:choose>
					<c:when test="${not empty passengerTypeMaxQuantityMap}">
						<c:forEach begin="0"
							end="${passengerTypeMaxQuantityMap[entry.passengerType.code]}"
							varStatus="loop">
							<form:option value="${fn:escapeXml(loop.index)}"> ${fn:escapeXml(loop.index)} </form:option>
						</c:forEach>
					</c:when>
					<c:otherwise>
						<form:options items="${guestQuantity}" htmlEscape="true" />
					</c:otherwise>
				</c:choose>
			</form:select>
			<form:input
				path="${fn:escapeXml(formPrefix)}passengerTypeQuantityList[${fn:escapeXml(i.index)}].passengerType.code"
				class="y_passengerTypeCode" type="hidden" readonly="true" />
			<form:input
				path="${fn:escapeXml(formPrefix)}passengerTypeQuantityList[${fn:escapeXml(i.index)}].passengerType.name"
				type="hidden" readonly="true" />
			<form:input
				path="${fn:escapeXml(formPrefix)}passengerTypeQuantityList[${fn:escapeXml(i.index)}].passengerType.minAge"
				type="hidden" readonly="true" />
			<form:input
				path="${fn:escapeXml(formPrefix)}passengerTypeQuantityList[${fn:escapeXml(i.index)}].passengerType.maxAge"
				type="hidden" readonly="true" />
			
		</div>
	</c:if>
</c:forEach>
<div class="col-sm-6">
	<p id="morePassenger" class="passenger-link btn btn-primary">More Passenger Types</p>
</div>
<div class="col-xs-12 col-sm-6 age-info text-right">
						<c:set var="infoText">
							<spring:theme code="text.cms.farefinder.age.info" text="Age refers to the age at the time of arrival date of the last leg of the journey: Adult[16+], Child[2-15], Infant[0-1]" />
						</c:set>
						<a tabindex="0" class="link-trigger" role="button" data-toggle="popover" data-trigger="focus" data-placement="bottom" data-container="body" data-content="${fn:escapeXml(infoText)}">
							<spring:theme code="text.cms.farefinder.ageinfo" text="Age Information" />
							<i class="glyphicon glyphicon-info-sign"></i>
						</a>
					</div>

<div id="hiddenDiv" class="morePassengersDiv sr-only" >
	<c:forEach var="entry" items="${passengerTypeQuantityList}" varStatus="i">
	<c:if test="${entry.passengerType.code != 'senior' && entry.passengerType.code != 'adult' && entry.passengerType.code != 'child' && entry.passengerType.code != 'infant' }">
			<div class="col-xs-3 col-sm-3 ${entry.passengerType.routeType == 'LONG' || entry.passengerType.code == 'teen' ? 'sr-only' : ''} y_passengerWrapper ${entry.passengerType.routeType}">
				<label for="y_${fn:escapeXml(entry.passengerType.code)}">
					${fn:escapeXml(entry.passengerType.name)} </label>
				<form:select
					class="form-control ${fn:escapeXml(entry.passengerType.code)} y_${fn:escapeXml(entry.passengerType.code)}Select"
					id="y_${fn:escapeXml(entry.passengerType.code)}"
					path="${fn:escapeXml(formPrefix)}passengerTypeQuantityList[${fn:escapeXml(i.index)}].quantity"
					cssErrorClass="fieldError">
					<form:option value="-1" disabled="true"> ${fn:escapeXml(entry.passengerType.name)} </form:option>
					<c:choose>
						<c:when test="${not empty passengerTypeMaxQuantityMap}">
							<c:forEach begin="0"
								end="${passengerTypeMaxQuantityMap[entry.passengerType.code]}"
								varStatus="loop">
								<form:option value="${fn:escapeXml(loop.index)}"> ${fn:escapeXml(loop.index)} </form:option>
							</c:forEach>
						</c:when>
						<c:otherwise>
							<form:options items="${guestQuantity}" htmlEscape="true" />
						</c:otherwise>
					</c:choose>
				</form:select>
				<form:input
					path="${fn:escapeXml(formPrefix)}passengerTypeQuantityList[${fn:escapeXml(i.index)}].passengerType.code"
					class="y_passengerTypeCode" type="hidden" readonly="true" />
				<form:input
					path="${fn:escapeXml(formPrefix)}passengerTypeQuantityList[${fn:escapeXml(i.index)}].passengerType.name"
					type="hidden" readonly="true" />
				<form:input
					path="${fn:escapeXml(formPrefix)}passengerTypeQuantityList[${fn:escapeXml(i.index)}].passengerType.minAge"
					type="hidden" readonly="true" />
				<form:input
					path="${fn:escapeXml(formPrefix)}passengerTypeQuantityList[${fn:escapeXml(i.index)}].passengerType.maxAge"
					type="hidden" readonly="true" />
				
			</div>
		</c:if>
	</c:forEach>
</div>
