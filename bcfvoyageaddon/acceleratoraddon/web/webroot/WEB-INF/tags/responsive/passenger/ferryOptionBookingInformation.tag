<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<div class="modal fade" id="ferry-option-booking-info-modal" tabindex="-1" role="dialog" aria-labelledby="ferry-option-booking-info-modal">
	<div class="modal-dialog" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true"><i class="bcf bcf-icon-cancel-solid"></i></span>
				</button>
				<h3 class="modal-title" id="limitedSailingsLabel">
					<spring:theme code="text.ferry.farefinder.vehicleInfo.tentative.booking.modal.title" />
				</h3>
			</div>
			<div class="modal-body">
				<p class="col-sm-12 warning-msg mt-0 bcf-icon-text">
					<i class="bcf bcf-icon-alert bcf-2x bcf-vertical-middle" aria-hidden="true"></i>
					<spring:theme code="text.ferry.farefinder.vehicleInfo.tentative.booking.modal.info" />
				</p>
			</div>
			<div class="modal-footer">
				<div class="row">
					<div class="col-xs-12 col-sm-6">
						<button id="y_reset_page" class="btn btn-secondary btn-block">
							<spring:theme code="text.ferry.farefinder.vehicleInfo.tentative.booking.cancel" />
						</button>
					</div>
					<div class="col-xs-12 col-sm-6">
						<button id="y_freshBooking" class="btn btn-primary btn-block">
							<spring:theme code="text.ferry.farefinder.vehicleInfo.tentative.booking.confirm" />
						</button>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
