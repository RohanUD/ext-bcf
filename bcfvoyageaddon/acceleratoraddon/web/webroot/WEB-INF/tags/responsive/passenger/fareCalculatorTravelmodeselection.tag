<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ attribute name="formPrefix" required="true" type="java.lang.String"%>
<spring:htmlEscape defaultHtmlEscape="false" />
<div class="row mb-6">
	<div class="col-md-12">
		<h4 class="md-5">
			<strong><spring:theme code="text.ferry.farefinder.passengerinfo.travelling.selection" text="Travelling" /></strong>
		</h4>
	</div>
	<div class="col-md-12">
		<div class="travelling-radio-sec margin-bottom-20">
			<label class="custom-radio-input">
				<form:radiobutton id="proceed_to_vehicle_selection_btn" path="${fn:escapeXml(formPrefix)}passengerInfoForm.travellingAsWalkOn" checked="${bcfFareFinderForm.passengerInfoForm.travellingAsWalkOn == 'false' ? 'checked' : '' }" value="false" />
				<spring:theme code="text.ferry.farefinder.passengerinfo.travelling.with.vehicle" text="With a vehicle" />
				<span class="checkmark"></span>
			</label>
			<label class="custom-radio-input">
				<form:radiobutton id="proceed_to_sailing_selection_btn" path="${fn:escapeXml(formPrefix)}passengerInfoForm.travellingAsWalkOn" checked="${bcfFareFinderForm.passengerInfoForm.travellingAsWalkOn == 'true' ? 'checked' : '' }" value="true" />
				<spring:theme code="text.ferry.farefinder.passengerinfo.travelling.without.vehicle" text="Without vehicle" />
				<span class="checkmark"></span>
			</label>
		</div>
	</div>
</div>
