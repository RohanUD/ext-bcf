<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ attribute name="formPrefix" required="true" type="java.lang.String"%>
<%@ taglib prefix="popupmodals" tagdir="/WEB-INF/tags/addons/bcfvoyageaddon/responsive/popupmodals"%>
<%@ taglib prefix="passenger" tagdir="/WEB-INF/tags/addons/bcfvoyageaddon/responsive/passenger"%>
<%@ taglib prefix="modal" tagdir="/WEB-INF/tags/addons/bcfvoyageaddon/responsive/passenger"%>
<%@ taglib prefix="additionalInfo" uri="additionalInfoTag"%>
<spring:htmlEscape defaultHtmlEscape="false" />
<div class="row mb-3 passenger-block-text">
	<c:forEach var="entry" items="${bcfFareFinderForm.passengerInfoForm.passengerTypeQuantityList}" varStatus="i">
		<c:if test="${fn:contains(bcfFareFinderForm.passengerInfoForm.basePassengers, entry.passengerType.code)}">
			<div class="col-lg-2 col-md-2 col-xs-6 pr-4 y_passengerWrapper ${entry.passengerType.routeType}">
			<spring:theme code="label.ferry.farefinder.passengerinfo.passenger.${entry.passengerType.code}"/><br/>
				<label for="y_${fn:escapeXml(entry.passengerType.code)}" class="passenger-lable fnt-16"> ${fn:escapeXml(entry.passengerType.name)} </label>
				<c:if test="${entry.passengerType.infoRequired}">
                    <a href="javascript:;" class="popoverThis" data-container="body" data-toggle="popover" data-placement="bottom" data-html="true"
                    	data-content="<additionalInfo:additionalInfo code="text.farefinder.additionalinfo.ageinformation.${entry.passengerType.code}" />">
                    	<span class="bcf bcf-icon-info-solid"></span>
                    </a>
				</c:if>
				<div class="input-group y_outboundPassengerQtySelector text-center mb-3">
					<span class="input-group-btn">
						<button class="btn btn-minus y_outboundPassengerQtySelectorMinus" type="button" data-type="minus" data-field="">
							<span class="bcf bcf-icon-remove"></span>
						</button>
					</span>
					<form:input type="text" id="y_outboundPassengerQuantity" class="form-control passenger-quantity y_${fn:escapeXml(entry.passengerType.code)}" value="${entry.quantity}" min="0" max="${bcfFareFinderForm.passengerInfoForm.maxPassengersAllowed}" path="${fn:escapeXml(formPrefix)}passengerInfoForm.passengerTypeQuantityList[${fn:escapeXml(i.index)}].quantity" readonly="true" />
					<span class="input-group-btn">
						<button class="btn btn-plus y_outboundPassengerQtySelectorPlus" type="button" data-type="plus" data-field="">
							<span class="bcf bcf-icon-add"></span>
						</button>
					</span>
					<form:input path="${fn:escapeXml(formPrefix)}passengerInfoForm.passengerTypeQuantityList[${fn:escapeXml(i.index)}].passengerType.code" id="y_passengerCode" type="hidden" readonly="true" />
					<form:input path="${fn:escapeXml(formPrefix)}passengerInfoForm.passengerTypeQuantityList[${fn:escapeXml(i.index)}].passengerType.name" id="y_passengerTypeName" type="hidden" readonly="true" />
					<form:input path="${fn:escapeXml(formPrefix)}passengerInfoForm.passengerTypeQuantityList[${fn:escapeXml(i.index)}].passengerType.minAge" type="hidden" readonly="true" />
					<form:input path="${fn:escapeXml(formPrefix)}passengerInfoForm.passengerTypeQuantityList[${fn:escapeXml(i.index)}].passengerType.maxAge" type="hidden" readonly="true" />
				</div>
			</div>
		</c:if>
	</c:forEach>
</div>
