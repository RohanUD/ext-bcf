<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="additionalInfo" uri="additionalInfoTag"%>
<spring:htmlEscape defaultHtmlEscape="false" />
<div id="y_walkOnOptionsInbound" class="hidden">
	<div class="row mb-3">
		<div class="col-lg-12">
			<div class="input-group ">
				<label class="custom-checkbox-input show" for="y_caryingDangerousGoodsInbound">
					<form:checkbox id="y_caryingDangerousGoodsInbound" path="${fn:escapeXml(formPrefix)}passengerInfoForm.carryingDangerousGoodsInReturn" checked="${bcfFareFinderForm.passengerInfoForm.carryingDangerousGoodsInReturn == 'true' ? 'checked' : '' }" />
					<spring:theme code="label.ferry.farefinder.passengerinfo.dangerousgoods.consent.checkbox" text="Carrying dangerous goods" />
					<span class="checkmark-checkbox"></span>
					<a href="javascript:;" data-container="body" class="popoverThis" data-toggle="popover" data-placement="bottom" data-html="true" data-content="<additionalInfo:additionalInfo code="text.farefinder.additionalinfo.dangerousgoods" />">
						<span class="bcf bcf-icon-info-solid"></span>
					</a>
				</label>
			</div>
		</div>
	</div>
	<div id="dangerousGoodsInboundInfo" class="warning-msg mb-3 hidden">
		<spring:theme code="text.ferry.farefinder.passengerinfo.carryingdangerousgoods.part1" text="Please be sure to review the" />
		<a href="${pageContext.request.contextPath}/travel-boarding/dangerous-goods" target="_blank">
			<spring:theme code="text.ferry.farefinder.passengerinfo.carryingdangerousgoods.part2" text="dangerous goods policies" />
		</a>
		<spring:theme code="text.ferry.farefinder.passengerinfo.carryingdangerousgoods.part3" text=" prior to sailing" />
	</div>
	<div class="row">
		<div class="col-xs-12 mb-3">
			<div class="input-group ">
				<label class="custom-checkbox-input show" for="y_caryingLargeItemsInbound">
					<form:checkbox id="y_caryingLargeItemsInbound" path="${fn:escapeXml(formPrefix)}passengerInfoForm.largeItemsCheckInbound" checked="${bcfFareFinderForm.passengerInfoForm.largeItemsCheckInbound == 'true' ? 'checked' : '' }" />
					<spring:theme code="label.ferry.farefinder.passengerinfo.largeItems.consent.checkbox" text="Carrying large items" />
					<a href="javascript:;" data-container="body" class="popoverThis" data-toggle="popover" data-placement="bottom" data-html="true" data-content="<additionalInfo:additionalInfo code="text.farefinder.additionalinfo.carryinglargeitems" />">
						<span class="bcf bcf-icon-info-solid"></span>
					</a>
					<span class="checkmark-checkbox"></span>
				</label>
			</div>
		</div>
	</div>
	<div id="y_largeItemInbound" class="row mb-4 large-items ${bcfFareFinderForm.passengerInfoForm.largeItemsCheckInbound ? '' : 'hidden' }">
		<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
			<div class="row mb-5">
				<c:forEach var="entry" items="${bcfFareFinderForm.passengerInfoForm.largeItemsInbound}" varStatus="i">
					<div class="col-lg-2 col-md-2 col-sm-4 col-xs-6">
						<label>${entry.name}</label>
						<div class="input-group y_inboundLargeItemQtySelector">
							<span class="input-group-btn">
								<button class="btn btn-minus y_inboundLargeItemQtySelectorMinus" type="button" data-type="minus" data-field="">
									<span class="bcf bcf-icon-remove"></span>
								</button>
							</span>
							<form:input type="text" id="y_inboundLargeItemQuantity" class="form-control carrying-large-items" readonly="true" value="${entry.quantity}" min="0" max="9" path="${fn:escapeXml(formPrefix)}passengerInfoForm.largeItemsInbound[${fn:escapeXml(i.index)}].quantity" />
							<span class="input-group-btn">
								<button class="btn btn-plus y_inboundLargeItemQtySelectorPlus" type="button" data-type="plus" data-field="">
									<span class="bcf bcf-icon-add"></span>
								</button>
							</span>
						</div>
					</div>
				</c:forEach>
			</div>
		</div>
		<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
			<label>
				<spring:theme code="label.ferry.farefinder.passengerinfo.extra.baggage.consent.checkbox" text="Hand baggage/luggage" />
				(
				<spring:theme code="text.ferry.farefinder.passengerinfo.extra.baggage.allowance.info" text="Free for bags up to 23 kgs or 50 lbs" />
				)
			</label>
			<div class="row mb-5">
				<div class="col-lg-2 col-md-2 col-sm-4 col-xs-6">
					<div class="input-group y_inboundLargeItemQtySelector">
						<span class="input-group-btn">
							<button class="btn btn-minus y_inboundLargeItemQtySelectorMinus" type="button" data-type="minus" data-field="">
								<span class="bcf bcf-icon-remove"></span>
							</button>
						</span>
						<form:input type="text" id="y_inboundLargeItemQuantity" class="form-control carrying-large-items" readonly="true" value="${passengerInfoForm.extraBaggageInbound.quantity}" min="0" max="9" path="${fn:escapeXml(formPrefix)}passengerInfoForm.extraBaggageInbound.quantity" />
						<span class="input-group-btn">
							<button class="btn btn-plus y_inboundLargeItemQtySelectorPlus" type="button" data-type="plus" data-field="">
								<span class="bcf bcf-icon-add"></span>
							</button>
						</span>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
