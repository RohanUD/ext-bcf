<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ attribute name="formPrefix" required="true" type="java.lang.String"%>
<spring:htmlEscape defaultHtmlEscape="false" />
	<h4>
		<i><spring:theme code="text.ferry.farefinder.passengerinfo.northern.bc.resident.header" text="Northern BC
		Resident" /></i>
	</h4>
    <div>
    <div class="row margin-bottom-20">
           <div class="col-lg-8 col-md-8 col-sm-12 col-xs-12">
              <spring:theme code="label.ferry.farefinder.passengerinfo.passengers.northern.description" />
           </div>
        </div>
        <p class="margin-bottom-30">
		<spring:theme code="label.ferry.farefinder.passengerinfo.passengers.northernlink.description"/>
        <spring:theme code="label.ferry.farefinder.passengerinfo.passengers.northern.info" var="inboundNorthernPassengerInfo"/>
		<a href="javascript:void(0);" data-container="body" class="popoverThis" data-toggle="popover" data-placement="bottom" data-html="true"
			data-content="${fn:escapeXml(inboundNorthernPassengerInfo)}">
			<span class="bcf bcf-icon-info-solid"></span>
		</a>
		</p>
        <div class="row">
			<c:forEach var="entry" items="${bcfFareFinderForm.passengerInfoForm.inboundPassengerTypeQuantityList}" varStatus="j">
				<c:if test="${fn:contains(bcfFareFinderForm.passengerInfoForm.northernPassengers, entry.passengerType.code)}">
					 <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6 mb-5 y_passengerWrapper">
							<label for="y_${fn:escapeXml(entry.passengerType.code)}"> ${fn:escapeXml(entry.passengerType.name)} </label>

							<div class="input-group y_inboundPassengerQtySelector">
								<span class="input-group-btn">
									<button class="btn btn-minus y_inboundPassengerQtySelectorMinus" type="button" data-type="minus" data-field="">
										<span class="bcf bcf-icon-remove"></span>
									</button>
								</span>
								<form:input type="text" readonly="true" id="y_inboundPassengerQuantity" class="form-control yr_${fn:escapeXml(entry.passengerType.code)}" value="${entry.quantity}" min="0" max="${bcfFareFinderForm.passengerInfoForm.maxPassengersAllowed}" path="${fn:escapeXml(formPrefix)}passengerInfoForm.inboundPassengerTypeQuantityList[${fn:escapeXml(j.index)}].quantity" />
								<span class="input-group-btn">
									<button class="btn btn-plus y_inboundPassengerQtySelectorPlus" type="button" data-type="plus" data-field="">
										<span class="bcf bcf-icon-add"></span>
									</button>
								</span>
								<form:input path="${fn:escapeXml(formPrefix)}passengerInfoForm.inboundPassengerTypeQuantityList[${fn:escapeXml(j.index)}].passengerType.code" id="y_passengerCode" type="hidden" readonly="true" />
								<form:input path="${fn:escapeXml(formPrefix)}passengerInfoForm.inboundPassengerTypeQuantityList[${fn:escapeXml(j.index)}].passengerType.name" id="y_passengerTypeName" type="hidden" readonly="true" />
								<form:input path="${fn:escapeXml(formPrefix)}passengerInfoForm.inboundPassengerTypeQuantityList[${fn:escapeXml(j.index)}].passengerType.minAge" type="hidden" readonly="true" />
								<form:input path="${fn:escapeXml(formPrefix)}passengerInfoForm.inboundPassengerTypeQuantityList[${fn:escapeXml(j.index)}].passengerType.maxAge" type="hidden" readonly="true" />
							</div>

					</div>
				</c:if>
			</c:forEach>
		</div>
     </div>
