<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ attribute name="formPrefix" required="true" type="java.lang.String"%>
<spring:htmlEscape defaultHtmlEscape="false" />

<div class="northernPassengersTypesDiv" style="display:${allowAdditionalPassengerTypes ? 'block' : 'none'}">
	<h4>
		<i><spring:theme code="text.ferry.farefinder.passengerinfo.northern.bc.resident.header" text="Northern BC
		Resident" /></i>
	</h4>
    <div>
    <div class="row margin-bottom-20">
           <div class="col-lg-8 col-md-8 col-sm-12 col-xs-12">
              <spring:theme code="label.ferry.farefinder.passengerinfo.passengers.northern.description" />
           </div>
        </div>
        <p class="margin-bottom-30">
        <spring:theme code="label.ferry.farefinder.passengerinfo.passengers.northernlink.description"/>
        <spring:theme code="label.ferry.farefinder.passengerinfo.passengers.northern.info" var="outboundNorthernPassengerInfo"/>
        <a href="javascript:void(0);" data-container="body" class="popoverThis" data-toggle="popover" data-placement="bottom" data-html="true"
			data-content="${fn:escapeXml(outboundNorthernPassengerInfo)}">
			<span class="bcf bcf-icon-info-solid"></span>
		</a>
        
        </p>
        <div class="row">
        <c:forEach var="entry" items="${bcfFareFinderForm.passengerInfoForm.passengerTypeQuantityList}" varStatus="i">
            <c:if test="${fn:contains(bcfFareFinderForm.passengerInfoForm.northernPassengers, entry.passengerType.code)}">
                <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6 mb-5 y_passengerWrapper">
                        <label class="fnt-16" for="y_${fn:escapeXml(entry.passengerType.code)}"> ${fn:escapeXml(entry.passengerType.name)} </label>
                        <div class="input-group y_outboundPassengerQtySelector">
                            <span class="input-group-btn">
                                <button class="btn btn-minus y_outboundPassengerQtySelectorMinus" type="button" data-type="minus" data-field="">
                                    <span class="bcf bcf-icon-remove"></span>
                                </button>
                            </span>
                            <form:input type="text" readonly="true" id="y_outboundPassengerQuantity" class="form-control passenger-quantity y_${fn:escapeXml(entry.passengerType.code)}" value="${entry.quantity}" min="0" max="${bcfFareFinderForm.passengerInfoForm.maxPassengersAllowed}" path="${fn:escapeXml(formPrefix)}passengerInfoForm.passengerTypeQuantityList[${fn:escapeXml(i.index)}].quantity" />
                            <span class="input-group-btn">
                                <button class="btn btn-plus y_outboundPassengerQtySelectorPlus" type="button" data-type="plus" data-field="">
                                    <span class="bcf bcf-icon-add"></span>
                                </button>
                            </span>
                            <form:input path="${fn:escapeXml(formPrefix)}passengerInfoForm.passengerTypeQuantityList[${fn:escapeXml(i.index)}].passengerType.code" id="y_passengerCode" type="hidden" readonly="true" />
                            <form:input path="${fn:escapeXml(formPrefix)}passengerInfoForm.passengerTypeQuantityList[${fn:escapeXml(i.index)}].passengerType.name" id="y_passengerTypeName" type="hidden" readonly="true" />
                            <form:input path="${fn:escapeXml(formPrefix)}passengerInfoForm.passengerTypeQuantityList[${fn:escapeXml(i.index)}].passengerType.minAge" type="hidden" readonly="true" />
                            <form:input path="${fn:escapeXml(formPrefix)}passengerInfoForm.passengerTypeQuantityList[${fn:escapeXml(i.index)}].passengerType.maxAge" type="hidden" readonly="true" />
                        </div>
                </div>
            </c:if>
        </c:forEach>
        </div>
    </div>
</div>
