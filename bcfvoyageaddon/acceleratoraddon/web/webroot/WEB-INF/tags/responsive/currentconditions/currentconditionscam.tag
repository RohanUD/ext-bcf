<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="cms" uri="http://hybris.com/tld/cmstags"%>
<%@ taglib prefix="ycommerce" uri="http://hybris.com/tld/ycommercetags"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ attribute name="routeCamData" required="false" type="com.bcf.integration.currentconditionsResponse.data.RouteCamData"%>

<%@ attribute name="displaySourceMap" required="false" type="java.lang.Boolean" %>
<%@ attribute name="sourceLocation" required="true" type="java.lang.String"%>
<%@ attribute name="sourceTerminalName" required="true" type="java.lang.String"%>

<%@ attribute name="displayDestniationMap" required="false" type="java.lang.Boolean" %>
<%@ attribute name="destinationLocation" required="true" type="java.lang.String"%>
<%@ attribute name="destinationTerminalName" required="true" type="java.lang.String" %>

<c:url var="depTerminalDetailsUrl"
	   value="/travel-boarding/terminal-directions-parking-food/${sourceTerminalName}/${sourceLocation}"/>

<c:url var="destTerminalDetailsUrl"
	   value="/travel-boarding/terminal-directions-parking-food/${destinationTerminalName}/${destinationLocation}"/>


<spring:htmlEscape defaultHtmlEscape="false" />
<div class="row">
	<%-- From Terminal (outSide Terminal) --%>
	<c:if test="${displaySourceMap and not empty routeCamData.trafficOutsideTerminalCam}">
		<div class="col-lg-4 col-md-4 col-sm-4 col-xs-12 text-center col-md-offset-2 col-sm-offset-2">
			<img src="${routeCamData.trafficOutsideTerminalCam}"/>
			<p class="text-center margin-top-10 margin-bottom-20">
				<a href="${depTerminalDetailsUrl}" target="_blank">
					<spring:theme code="text.cc.atAglance.webcam.tarffic.outSide.label" text="Traffic outside terminal"/>
				</a>
			</p>
		</div>
	</c:if>
	
	<%-- To Terminal (to Terminal) --%>
	<c:if test="${displayDestniationMap and not empty routeCamData.trafficToDestCam}">
		<div class="col-lg-4 col-md-4 col-sm-4 col-xs-12 text-center">
			<img src="${routeCamData.trafficToDestCam}">
			<c:if test="${not empty destinationTerminalName}">
				<p class="text-center margin-top-10 margin-bottom-20">
					<a href="${destTerminalDetailsUrl}" target="_blank">
						<spring:theme code="text.cc.atAglance.webcam.tarffic.to.label" text="Traffic to"
									  arguments="${destinationTerminalName}"/>
					</a>
				</p>
			</c:if>
		</div>
	</c:if>
</div>
