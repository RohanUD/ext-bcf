<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ attribute name="ferryTrackingLink" required="true" type="java.lang.String"%>
<%@ attribute name="sourceLocationName" required="false" type="java.lang.String"%>
<%@ attribute name="sourceTerminalName" required="false" type="java.lang.String"%>
<%@ attribute name="sourceLocation" required="false" type="java.lang.String" %>
<%@ attribute name="sourceTerminal" required="false" type="java.lang.String" %>
<%@ attribute name="destinationTerminal" required="false" type="java.lang.String" %>
<%@ attribute name="destinationLocationName" required="false" type="java.lang.String"%>
<%@ attribute name="destinationTerminalName" required="false" type="java.lang.String"%>
<%@ attribute name="destinationLocation" required="false" type="java.lang.String" %>
<%@ attribute name="colClass" required="false" type="java.lang.String" %>

<c:url var="depTerminalDetailsUrl"
   value="/travel-boarding/terminal-directions-parking-food/${sourceTerminalName}/${sourceLocation}"/>
<c:url var="destTerminalDetailsUrl"
   value="/travel-boarding/terminal-directions-parking-food/${destinationTerminalName}/${destinationLocation}"/>
<spring:htmlEscape defaultHtmlEscape="false" />
<div class="row">
   <div class="${not empty colClass ? 'colClass' : 'col-lg-12 col-md-12' } col-sm-12 col-xs-12">
      <c:if test="${not empty ferryTrackingLink}">
         <div class="embed-responsive embed-responsive-16by9" style="height: 250px;">
            <iframe class="embed-responsive-item current-conditions-ferry-iframe" src="${ferryTrackingLink}"  style="z-index: -999;" scrolling ="no"  name="test">You need a Frames Capable browser to view this content.</iframe>
        </div>
        <c:if test="${not empty sourceLocationName and not empty sourceTerminalName and not empty destinationLocationName and not empty destinationTerminalName}">
            <a href="#" class="text-center margin-top-10 mb-5 fnt-14">
                <span>${sourceTerminal}</span>
                <span class="no-anchor"><spring:theme code="text.to.label" text="to"/></span>
                <span>${destinationTerminal}</span>
            </a>
        </c:if>
      </c:if>
   </div>
</div>
