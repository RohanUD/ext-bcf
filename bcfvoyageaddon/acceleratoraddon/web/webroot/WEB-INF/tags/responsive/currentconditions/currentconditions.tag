<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="cms" uri="http://hybris.com/tld/cmstags" %>
<%@ taglib prefix="ycommerce" uri="http://hybris.com/tld/ycommercetags" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ attribute name="formPrefix" required="true" type="java.lang.String" %>
<%@ attribute name="currentConditionsForm" required="true" type="com.bcf.bcfvoyageaddon.forms.cms.CurrentConditionsForm" %>
<spring:htmlEscape defaultHtmlEscape="false"/>
<%-- From / To input fields --%>
<div class="custom-error-4 alert alert-danger alert-dismissable custom-error-msg hidden">
    <span class="icon-alert"></span> <span></span>
</div>
<div class="bc-accordion ferry-drop current-condition accordion-content-w-9">
    <label for="fromLocation" class="home-label-mobile">
        <spring:theme code="label.currentconditions.depart.from" text="FROM"/>
    </label>

    <div id="y_currentConditionsDepartureLocationTravelroute" class="autocomplete-suggestions-wrapper hidden"></div>
    <div id="y_currentConditionsOriginLocationSuggestions" class="autocomplete-suggestions-wrapper hidden"></div>
    <div id="currentcondition-accordion1" class="accordion-34 accordion-bg-white component-to-top">
        <h3>
          <span class="dropdown-text">
  					<strong class="drop-text mt-0 destination-text"><spring:theme code="label.farefinder.vacation.destination.dropdown.placeholder" text="Select a destination (city)" /></strong>
  					<span class="custom-arrow"></span>
  				</span>
        </h3>
        <div>
          <div class="vacation-range-box">
            <div class="form-height-fix style-3">


            </div>
          </div>
        </div>
    </div>
</div>

<div class="bc-accordion ferry-drop current-condition accordion-content-w-9">
    <label for="toLocation" class="home-label-mobile">
        <spring:theme code="label.currentconditions.return.to" text="TO"/>
    </label>

    <div id="y_currentConditionsArrivalLocationTravelroute" class="autocomplete-suggestions-wrapper hidden"></div>
    <div id="y_currentConditionsDestinationLocationSuggestions" class="autocomplete-suggestions-wrapper hidden"></div>
    <div id="currentcondition-accordion2" class="accordion-34 accordion-bg-white component-to-top">
        <h3>
          <span class="dropdown-text">
  			<strong class="drop-text destination-text">City</strong>
            <span>Terminal</span>
  					<span class="custom-arrow"></span>
  				</span>
        </h3>
        <div>
          <div class="vacation-range-box">
            <div class="form-height-fix style-3">

            </div>
          </div>
        </div>
    </div>
</div>
