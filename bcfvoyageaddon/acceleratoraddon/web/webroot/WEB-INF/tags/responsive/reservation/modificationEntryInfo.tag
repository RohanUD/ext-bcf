<%@ tag body-content="empty" trimDirectiveWhitespaces="true"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ attribute name="orderEntry" required="true" type="com.bcf.facades.reservation.entry.data.BcfOrderEntryData"%>
<c:choose>
	<c:when test="${orderEntry.product.productType=='FARE_PRODUCT'}">
		<c:set var="traveller" value="${orderEntry.traveller}" />
		<c:choose>
			<c:when test="${traveller.travellerType=='PASSENGER'}">
				<div class="col-sm-6">${orderEntry.quantity}&nbsp;${traveller.travellerInfo.passengerType.name}</div>
				<div class="col-sm-3">${orderEntry.basePrice}</div>
			</c:when>
			<c:when test="${traveller.travellerType=='VEHICLE'}">
				<c:set var="vehicleInfo" value="${traveller.travellerInfo}" />
				<div class="col-sm-6">${orderEntry.quantity}&nbsp;${vehicleInfo.vehicleType.name}</div>
				<div class="col-sm-3">${orderEntry.basePrice}</div>
				<div class="col-sm-6">Carrying Livestock : ${vehicleInfo.carryingLivestock}</div>
				<div class="col-sm-12">${vehicleInfo.vehicleWithSidecarOrTrailer==true?'Vehicle with Sidecar/Trailer':'' }</div>
			</c:when>
		</c:choose>
	</c:when>
	<c:when test="${orderEntry.product.productType=='ANCILLARY'}">
		<div class="col-sm-6">${orderEntry.quantity}&nbsp;${orderEntry.product.name}</div>
		<div class="col-sm-3">${orderEntry.basePrice}</div>
	</c:when>
</c:choose>