<%@ tag body-content="empty" trimDirectiveWhitespaces="true"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ attribute name="ptcBreakdownList" required="true" type="java.util.List"%>
<%@ attribute name="travellers" required="false" type="java.util.List"%>
<%@ attribute name="routeType" required="false" type="String"%>

<spring:htmlEscape defaultHtmlEscape="false" />
<c:forEach items="${ptcBreakdownList}" var="ptcFareBreakdown" varStatus="ptcIdx">
	<li>
		<div class="row">
			<ul class="col-xs-9 col-md-9">
				<li>${fn:escapeXml(ptcFareBreakdown.passengerTypeQuantity.quantity)}&nbsp;x&nbsp;${fn:escapeXml(ptcFareBreakdown.passengerTypeQuantity.passengerType.name)}</li>
			</ul>
			<ul class="col-xs-3 col-md-3 text-right">
				<li>${fn:escapeXml(ptcFareBreakdown.passengerFare.baseFare.formattedValue)}</li>
			</ul>
		</div>
		<c:forEach items="${ptcFareBreakdown.passengerFare.taxes}" var="taxes" varStatus="taxesIdx">
			<div>
				<Strong>${taxes.price.formattedValue}</Strong>
			</div>
			<div>
				<Strong>${taxes.code}</Strong>
			</div>
		</c:forEach>
		<c:forEach items="${ptcFareBreakdown.passengerFare.discounts}" var="discounts" varStatus="discountsIdx">
			<div>
				<Strong>${discounts.price.formattedValue}</Strong>
			</div>
			<div>
				<Strong>${discounts.purpose}</Strong>
			</div>
		</c:forEach>
		<c:if test="${routeType eq 'LONG'}">
            <c:set var="paxCount" value="1" />
            <c:forEach items="${travellers}" var="traveller" varStatus="travellerIdx">
                <c:if test="${traveller.travellerType eq 'PASSENGER' && traveller.travellerInfo.passengerType.code eq ptcFareBreakdown.passengerTypeQuantity.passengerType.code}">
                    <div class="row">
                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                            Passenger ${paxCount} : ${traveller.travellerInfo.firstName}&nbsp;${traveller.travellerInfo.surname}
                        </div>
                    </div>
                    <c:set var="paxCount" value="${paxCount+1}" />
                </c:if>
            </c:forEach>
        </c:if>
	</li>
</c:forEach>
