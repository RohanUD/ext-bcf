<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="transportreservation" tagdir="/WEB-INF/tags/addons/bcfvoyageaddon/responsive/reservation"%>
<%@ taglib prefix="format" tagdir="/WEB-INF/tags/addons/bcfstorefrontaddon/responsive/format"%>
<%@ taglib prefix="formElement"	tagdir="/WEB-INF/tags/responsive/formElement"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="bookingDetails" tagdir="/WEB-INF/tags/addons/bcfstorefrontaddon/responsive/bookingdetails"%>
<%@ attribute name="alacarteBooking" required="false" type="java.lang.Boolean"%>
<%@ attribute name="reservationData" required="true" type="de.hybris.platform.commercefacades.travel.reservation.data.ReservationData"%>
<div class="y_transportReservationComponent booking-details-accordion margin-top-20 margin-top-20">

	<c:if test="${reservation.bookingStatusCode ne 'CANCELLED' && not empty reservationData.reservationItems}">
       <c:if test="${asmLoggedIn && alacarteBooking}">
            <bookingDetails:asmCommonBookingDetails userRoles="${userRoles}" asmOrderData="${asmOrderData}"/>
       </c:if>

       <c:forEach items="${reservationData.reservationItems}" var="item" varStatus="itemIdx">
        <div class="js-accordion js-accordion-default payment-accordion">
        <h4 class="booking-detail-title">
            <c:choose>
                <c:when test="${item.reservationItinerary.originDestinationOptions[0].originDestinationRefNumber == 0}">
                    <c:set var="labelPrefix" value="label.payment.reservation.transport.departure.sailing.time.prefix"></c:set>
                </c:when>
                <c:when test="${item.reservationItinerary.originDestinationOptions[0].originDestinationRefNumber == 1}">
                    <c:set var="labelPrefix" value="label.payment.reservation.transport.return.sailing.time.prefix"></c:set>
                </c:when>
            </c:choose>
            <fmt:formatDate var="travelDate" pattern="EEEEE, MMM dd" value="${item.reservationItinerary.originDestinationOptions[0].transportOfferings[0].departureTime}" />
            <strong><spring:theme code="${labelPrefix}" /></strong>&nbsp;:&nbsp;${travelDate}
        </h4>

          <div class="add-on-box">
                <c:choose>
                    <c:when test="${item.reservationItinerary.originDestinationOptions[0].originDestinationRefNumber == 0}">
                        <c:set var="labelPrefix" value="label.payment.reservation.transport.departure.sailing.total"></c:set>
                    </c:when>
                    <c:when test="${item.reservationItinerary.originDestinationOptions[0].originDestinationRefNumber == 1}">
                        <c:set var="labelPrefix" value="label.payment.reservation.transport.return.sailing.total"></c:set>
                    </c:when>
                </c:choose>
                <fmt:formatDate var="departureDate" pattern="EEE, MMM dd" value="${itinerary.originDestinationOptions[0].transportOfferings[0].departureTime}" />
                 <transportreservation:sailinginformation itinerary="${item.reservationItinerary}" isOpenTicket="${item.openTicketSelected}"  />
                 <transportreservation:travellercount vehicleBreakdownList="${item.reservationPricingInfo.itineraryPricingInfo.vehicleFareBreakdownDatas}" ptcBreakdownList="${item.reservationPricingInfo.itineraryPricingInfo.ptcFareBreakdownDatas}" />
                <c:if test="${item.openTicketSelected ne 'true'}">
                    <div class="col-md-12 margin-bottom-17">
                        <p class="text-center my-3 fnt-14">
                        <c:set var="vessel" value="${item.reservationItinerary.originDestinationOptions[0].transportOfferings[0].transportVehicle.vehicleInfo}" />
                            <a href="/ship-info/${vessel.code}">${vessel.name}
                            </a>
                        </p>
                    </div>
                    </c:if>
                    <div class="col-md-10 col-md-offset-1">
                            <div class="dash-dvider mt-3 mb-3">

                            </div>
                            <h5 class="my-3">
                                <strong><spring:theme code="text.page.managemybooking.bookingdetails.bundle.name.${item.reservationPricingInfo.itineraryPricingInfo.bundleType}" /></strong>
                            </h5>
                            <p>
                                <spring:theme code="text.page.managemybooking.bookingdetails.bundle.description.${item.reservationPricingInfo.itineraryPricingInfo.bundleType}" />
                            </p>
                            <div class="vp-box">
                           <h5 class="my-3">
                                <strong><spring:theme code="label.payment.reservation.traveller.header" text="Vehicles & passengers" /></strong>
								<c:if test="${item.carryingDangerousGoods}">
									<spring:theme code="label.payment.reservation.traveller.details.carrying.dangerous.goods" />
								</c:if>
							</h5>
								<div class="col-md-12">
									<ul class="col-md-12 col-xs-12 list-unstyled payment-vp">
										<transportreservation:vehicleBreakdown vehicleBreakdownList="${item.reservationPricingInfo.itineraryPricingInfo.vehicleFareBreakdownDatas}" travellers="${item.reservationItinerary.travellers}" routeType="${item.reservationItinerary.route.routeType}" />
										<transportreservation:ptcBreakdown ptcBreakdownList="${item.reservationPricingInfo.itineraryPricingInfo.ptcFareBreakdownDatas}" travellers="${item.reservationItinerary.travellers}" routeType="${item.reservationItinerary.route.routeType}" />
										<transportreservation:accessibilityBreakdown accessibilityBreakdownDataList="${item.reservationPricingInfo.itineraryPricingInfo.accessibilityBreakdownDataList}" />
										<transportreservation:largeItems largeItems="${item.reservationPricingInfo.itineraryPricingInfo.largeItemsBreakdownDataList}" />
									</ul>
								</div>
								<c:if test="${not empty item.reservationPricingInfo.itineraryPricingInfo.accessibilities}">
									<div class="col-md-12">
										<ul class="col-md-12 col-xs-12 list-unstyled payment-vp">
											<c:forEach items="${item.reservationPricingInfo.itineraryPricingInfo.accessibilities}" var="accessibiltyRequest" varStatus="k">
												<li>
													<strong><spring:theme code="text.ferry.farefinder.passengerinfo.passenger.accesibility"/> ${k.index + 1}:&nbsp;${fn:escapeXml(accessibiltyRequest.passengerType.name)}</strong>
												</li>
												<c:forEach items="${accessibiltyRequest.specialServiceRequestDataList}" var="specialServiceRequest" varStatus="l">
													<li>${specialServiceRequest.name}</li>
												</c:forEach>
											</c:forEach>
										</ul>
									</div>
								</c:if>
								<c:if test="${not empty item.reservationPricingInfo.ancilliaryDatas}">
									<h5 class="my-3">
										<strong><spring:theme code="reservation.journey.extras" text="Options and Extras" /></strong>
									</h5>
									<div class="col-md-12">
										<ul class="col-md-12 col-xs-12 list-unstyled payment-vp">
											<transportreservation:ancillaries offerBreakdowns="${item.reservationPricingInfo.ancilliaryDatas}" />
										</ul>
									</div>
								</c:if>
								<c:set var="showTaxes" value="${false}"/>
								<c:forEach var="tax" items="${item.reservationPricingInfo.totalFare.taxes}">
									<c:if test="${tax.price.value > 0.0}">
									<c:set var="showTaxes" value="${true}"/>
									</c:if>
								</c:forEach>
								<c:forEach var="fee" items="${item.reservationPricingInfo.totalFare.fees}">
									<c:if test="${fee.price.value > 0.0}">
									<c:set var="showTaxes" value="${true}"/>
									</c:if>
								</c:forEach>
								<c:forEach var="discount" items="${item.reservationPricingInfo.totalFare.discounts}">
									<c:if test="${discount.price.value < 0.0}">
									<c:set var="showTaxes" value="${true}"/>
									</c:if>
								</c:forEach>
								<c:if test="${showTaxes}">
								<h5 class="my-3">
									<strong><spring:theme code="label.payment.reservation.sailing.price.breakup.header" text="Taxes fees & discounts" /></strong>
								</h5>
								<div class="col-md-12">
									<ul class="col-md-12 col-xs-12 list-unstyled payment-vp">
										<c:forEach var="tax" items="${item.reservationPricingInfo.totalFare.taxes}">
											<c:if test="${tax.price.value > 0.0}">
												<li>
													<div class="row">
														<ul class="col-xs-9 col-md-9">
															<li>
																<spring:theme code="${tax.code}" />
															</li>
														</ul>
														<ul class="col-xs-3 col-md-3 text-right">
															<li>
																<format:price priceData="${tax.price}" />
															</li>
														</ul>
													</div>
												</li>
											</c:if>
										</c:forEach>
										<c:forEach var="fee" items="${item.reservationPricingInfo.totalFare.fees}">
											<c:if test="${fee.price.value > 0.0}">
												<li>
													<div class="row">
														<ul class="col-xs-9 col-md-9">
															<li>
																<spring:theme code="${fee.name}" />
															</li>
														</ul>
														<ul class="col-xs-3 col-md-3 text-right">
															<li>
																<format:price priceData="${fee.price}" />
															</li>
														</ul>
													</div>
												</li>
											</c:if>
										</c:forEach>
										<c:forEach var="discount" items="${item.reservationPricingInfo.totalFare.discounts}">
											<c:if test="${discount.price.value < 0.0}">
												<li>
													<div class="row">
														<ul class="col-xs-9 col-md-9">
															<li>
																<spring:theme code="${discount.purpose}" />
															</li>
														</ul>
														<ul class="col-xs-3 col-md-3 text-right">
															<li>
																<format:price priceData="${discount.price}" />
															</li>
														</ul>
													</div>
												</li>
											</c:if>
										</c:forEach>
									</ul>
								</div>
								</c:if>
								<div class="clearfix"></div>




                            <div class="departure-bx payment-total">
                                <h5>
                                    <spring:theme code="${labelPrefix}" text="Sailing total" />
                                    <span><strong><format:price priceData="${item.reservationPricingInfo.totalFare.totalPrice}" /></strong></span>
                                </h5>
                            </div>
                        </div>
                    </div>
                    <div class="row margin-bottom-40 margin-top-40">
                        <div class="col-lg-4 col-lg-offset-4">
                           <div class="text-center">


                           <c:choose>
                           		<c:when test="${alacarteBooking}">
                           		  <div class="col-xs-4 col-md-2 wapper-center margin-top-10">
                                       <button class="btn btn-primary removeSailingButton" type="button" id="removeSailingButton_${reservationData.journeyReferenceNumber}_${item.reservationItinerary.originDestinationOptions[0].originDestinationRefNumber}">
                                             <input id="removeSailingURL" type="hidden" value="${fn:escapeXml(request.contextPath)}/fare-selection/remove-selection/${reservationData.journeyReferenceNumber}/${item.reservationItinerary.originDestinationOptions[0].originDestinationRefNumber}"/>
                                                             <spring:theme code="label.packageferryreview.reservation.remove" text="Remove" />
                                        </button>
                                        <c:set var="changeURL" value="/fare-selection/change-selection/${reservationData.journeyReferenceNumber}/${item.reservationItinerary.originDestinationOptions[0].originDestinationRefNumber}"></c:set>
                                        <a href="${contextPath}${fn:escapeXml(changeURL)}" class="btn btn-primary"><spring:theme code="label.packageferryreview.reservation.change" text="Change" /></a>
                                    </div>
                          		</c:when>
                                <c:otherwise>
                                     <button type="button" data-toggle="modal" data-target="#editModal" class="btn btn-primary btn-block margin-bottom-20 js-open-edit-modal" data-id="${contextPath}/edit-entry/${reservationData.journeyReferenceNumber}/${item.reservationItinerary.originDestinationOptions[0].originDestinationRefNumber}">
                                        <spring:theme code="label.payment.reservation.sailing.entry.edit" text="Edit Booking" />
                                     </button>
                                     <button type="button" data-toggle="modal" data-target="#removeModal" class="btn btn-transparent btn-block js-open-cancel-modal" data-id="${contextPath}/cart/remove-entry/${reservationData.journeyReferenceNumber}/${item.reservationItinerary.originDestinationOptions[0].originDestinationRefNumber}">
                                        <spring:theme code="label.payment.reservation.sailing.entry.remove" text="Remove Booking" />
                                      </button>
                                </c:otherwise>
                            </c:choose>
                          </div>
                        </div>
                    </div>
                </div>
            </div>

           <c:if test="${asmLoggedIn && alacarteBooking}">
               <c:choose>
                   <c:when test="${northernRoute}">
                       <li><bookingDetails:asmNorthernTransportBookingDetails bookingRef="${reservationData.reservationItems[0].bcfBookingReference}" asmOrderData="${asmOrderData}"/></li>
                   </c:when>
                   <c:otherwise>
                       <li><bookingDetails:asmTransportBookingDetails bookingRef="${reservationData.reservationItems[0].bcfBookingReference}" asmOrderData="${asmOrderData}"/></li>
                   </c:otherwise>
               </c:choose>
           </c:if>

        </c:forEach>
	</c:if>
</div>

<div class="modal" tabindex="-1" role="dialog" id="editModal">
   <div class="modal-dialog" role="document">
      <div class="modal-content">
         <div class="modal-header">
         <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                     <span aria-hidden="true"><i class="bcf bcf-icon-cancel-solid"></i></span>
                     </button>
            <h4 class="modal-title">
               <spring:theme code="label.payment.reservation.sailing.modal.entry.edit" text="Edit Sailing" />
            </h4>
         </div>
         <div class="modal-body">
            <p>
               <spring:theme code="label.payment.reservation.sailing.modal.body.entry.edit" text="Are you sure you want to edit this booking?" />
            </p>
         </div>
         <div class="modal-footer">
            <a href="#" class="btn btn-transparent" id="edit-sailing-url">
               <spring:theme code="text.company.disable.confirm.yes" text="Yes" />
            </a>
            <button type="button" class="btn btn-transparent" data-dismiss="modal">
               <spring:theme code="text.company.disable.confirm.no" text="No" />
            </button>
         </div>
      </div>
   </div>
</div>
<div class="modal" tabindex="-1" role="dialog" id="removeModal">
   <div class="modal-dialog" role="document">
      <div class="modal-content">
         <div class="modal-header">
         <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                     <span aria-hidden="true"><i class="bcf bcf-icon-cancel-solid"></i></span>
                     </button>
            <h4 class="modal-title">
               <spring:theme code="label.payment.reservation.sailing.modal.entry.remove" text="Remove Sailing" />
            </h4>
         </div>
         <div class="modal-body">
            <p>
               <spring:theme code="label.payment.reservation.sailing.modal.body.entry.remove" text="Are you sure you want to remove this booking? This action cannot be undone." />
            </p>
         </div>
         <div class="modal-footer">
            <a href="#" class="btn btn-transparent" id="remove-sailing-url">
               <spring:theme code="text.company.disable.confirm.yes" text="Yes" />
            </a>
            <button type="button" class="btn btn-transparent" data-dismiss="modal">
               <spring:theme code="text.company.disable.confirm.no" text="No" />
            </button>
         </div>
      </div>
   </div>
</div>
