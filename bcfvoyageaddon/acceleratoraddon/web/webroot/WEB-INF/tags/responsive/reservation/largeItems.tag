<%@ tag body-content="empty" trimDirectiveWhitespaces="true"%>
<%@ attribute name="largeItems" required="true" type="java.util.List"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="format" tagdir="/WEB-INF/tags/addons/bcfstorefrontaddon/responsive/format"%>
<spring:htmlEscape defaultHtmlEscape="false" />
<c:if test="${not empty largeItems}">
	<c:forEach items="${largeItems}" var="largeItem" varStatus="idx">
		<li>
			<div class="row">
				<ul class="col-xs-9 col-md-9">
					<li>${fn:escapeXml(largeItem.quantity)}&nbsp;x&nbsp;${fn:escapeXml(largeItem.name)}</li>
				</ul>
				<ul class="col-xs-3 col-md-3 text-right">
					<li>
						<format:price priceData="${largeItem.price}" />
					</li>
				</ul>
			</div>
		</li>
	</c:forEach>
</c:if>
