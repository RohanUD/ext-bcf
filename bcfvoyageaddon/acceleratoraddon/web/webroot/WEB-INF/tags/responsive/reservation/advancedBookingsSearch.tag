<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ attribute name="bookingType" required="true" type="String"%>

<div class="custom-error-2 alert alert-danger alert-dismissable advanced-search-error hidden"><span class="icon-alert">Select From Terminal</span> <span>Please Select</span></div>
<div class=" mt-4 tab-my-booking margin-bottom-30 advanced-search">
      <div class="row">
            <div class="col-lg-12 col-xs-12">
            <p class="fnt-14"><a href="#" class="advance-serch-link"><spring:theme code="text.bookings.search.advance" text="Advanced Search"/> <span class="bcf bcf-icon-up-arrow"></span></a><p>
            <div class="selected-advanced-search hidden">
               <p><strong>Departure: </strong> <span id="advSearchDeparture"></span></p>
               <p><strong>Arrival: </strong> <span id="advSearchArrival"></span></p>
               <p><strong>From: </strong><span id="advSearchDepartDate"></span></p>
               <p><strong>To: </strong><span id="advSearchReturnDate"></span></p>
               <c:choose>
                   <c:when test="${bookingType eq 'business'}">
                        <p class="text-center "><a href="/my-business-account/my-business-bookings"><spring:theme code="text.bookings.search.clear" text="Clear Search"/>
                        <span class="bcf bcf-icon-cancel-solid"></span></a></p>
                   </c:when>
                   <c:otherwise>
                        <p class="text-center "><a href="/my-account/my-bookings"><spring:theme code="text.bookings.search.clear" text="Clear Search"/>
                        <span class="bcf bcf-icon-cancel-solid"></span></a></p>
                   </c:otherwise>
               </c:choose>
            </div>
         </div>
      </div>
      
      <div class="advanced-search-wrapper">
          <div class="row">
            <div class="col-lg-12 col-xs-12">
               <p class="fnt-14 margin-top-20"><spring:theme code="text.bookings.search.advance.message" text="Fill in at least one of the fields below to search your bookings"/></p>
            </div>
         </div>
         <div class="row">
            <div class="col-lg-6 col-xs-12">
               <input type="hidden" id="bookingType" value="${bookingType}"/>
               <input type="hidden" id="pageNumber" value="1"/>
               <label><spring:theme code="text.bookings.search.booking.reference" text="Booking Reference"/></label>
               <div><input type="text" id="bookingReference" class="form-control"/></div>
            </div>
         </div>

         <div class="row margin-top-20">
            <div class="col-lg-4 col-sm-12 col-xs-12 padding-right-0">
               <div class="bc-accordion">
                  <label>
                     <spring:theme code="text.bookings.search.departure.port.code" text="Departure"/>
                  </label>
                  <div class="accordion-34 component-to-top" id="fromLocationDropDown"
                     data-inputselector="fromLocation"
                     data-routeinfoselector="y_fareFinderOriginLocationCode"
                     data-suggestionselector="y_fareFinderDestinationLocationSuggestionType">
                     <h3>
                        <span class="dropdown-text">
                           <strong>
                              <spring:theme
                                 code="label.ferry.farefinder.dropdown.default.heading" text="Location" />
                           </strong>
                           <br />
                           <span>
                              <spring:theme
                                 code="label.ferry.farefinder.dropdown.default.subheading" text="Location" />
                           </span>
                           <span class="custom-arrow"></span>
                        </span>
                     </h3>
                  </div>
               </div>
            </div>

            <div class="col-lg-4 col-sm-12 col-xs-12 padding-right-0">
               <div class="bc-accordion">
                  <label>
                     <spring:theme code="text.bookings.search.arrival.port.code" text="Arrival"/>
                  </label>
                  <div class="accordion-34 component-to-top" id="toLocationDropDown"
                     data-inputselector="toLocation"
                     data-routeinfoselector="y_fareFinderDestinationLocationCode"
                     data-suggestionselector="y_fareFinderOriginLocationSuggestionType">
                     <h3>
                        <span class="dropdown-text">
                           <strong>
                              <spring:theme
                                 code="label.ferry.farefinder.dropdown.default.heading" text="Location" />
                           </strong>
                           <br />
                           <span>
                              <spring:theme
                                 code="label.ferry.farefinder.dropdown.default.subheading" text="Location" />
                           </span>
                           <span class="custom-arrow"></span>
                        </span>
                        <span class="custom-arrow"></span>
                     </h3>
                     <input type="text" class="dropdown-hiddenInput hidden" />
                  </div>
               </div>
            </div>

            <div class="col-lg-4 col-sm-12 col-xs-12 padding-right-0">
               <div class="bc-accordion">
                     <div id="js-roundtrip">
                        <ul class="nav nav-tabs" role="tablist">
                           <li class="nav-item ui-depart">
                              <label for="departingDateTime" class="home-label-mobile">
                                 <spring:theme
                                    code="label.ferry.farefinder.routeinfo.depart.date" text="DEPART" />
                              </label>

                              <a class="nav-link p-0 component-to-top" data-toggle="tab" href="#home">
                                 <div class="vacation-calen-box">
                                    <span class="vacation-calen-date-txt">Date</span>
                                    <span class="vacation-calen-year-txt current-year-custom advSelectedDepartDate">${routeInfoForm.departingDateTime}</span>
                                    <i class="bcf bcf-icon-calendar bcf-2x bc-icon bc-icon--big"
                                       aria-hidden="true"></i>
                                 </div>
                              </a>
                           </li>
                           <li class="nav-item ui-return disabled-nopointer">
                              <label for="returnDateTime"
                                 class="home-label-mobile font-weight-light">
                                 <spring:theme
                                    code="label.ferry.farefinder.routeinfo.return.date" text="RETURN" />
                              </label>
                              <a class="nav-link p-0 component-to-top" data-toggle="tab" href="#menu1">
                                 <div class="vacation-calen-box">
                                    <span class="vacation-calen-date-txt">Date</span>
                                    <span class="vacation-calen-year-txt current-year-custom advSelectedReturnDate ">${routeInfoForm.returnDateTime}</span>
                                    <i class="bcf bcf-icon-calendar bcf-2x bc-icon bc-icon--big"
                                       aria-hidden="true"></i>
                                 </div>
                              </a>
                           </li>
                        </ul>

                        <!-- Tab panes -->
                        <div class="tab-content">
                           <div id="home"
                              class=" tab-pane input-required-wrap  depart-calendar">
                              <div class="bc-dropdown calender-box" id="js-return">
                                 <label><spring:theme
                                    code="label.ferry.farefinder.routeinfo.date.format" text="Search date: mmm dd yyyy" /></label>
                                 <input type="text" class="adv-depart-date datepicker-input input-grid form-control fnt-14" placeholder="Depart">
                                 
                                 <div class="datepicker bc-dropdown--big  y_${fn:escapeXml( idPrefix )}FinderDatePickerDeparting y_transportDepartDate"
                                    id="check-in-datepicker" aria-describedby="check-in-date"></div>
                              </div>
                           </div>
                           
                           <div id="menu1"
                              class=" tab-pane fade y_fareFinderReturnField input-required-wrap return-calendar">
                              <div class="bc-dropdown calender-box">
                                 <label><spring:theme
                                    code="label.ferry.farefinder.routeinfo.date.format" text="Search date: mmm dd yyyy" /></label>
                                 <input type="text" class="adv-return-date datepicker-input input-grid form-control fnt-14" placeholder="Return">
                                 
                                 <div
                                    class="returnOption bc-dropdown--big  datepicker  y_${fn:escapeXml( idPrefix )}FinderDatePickerReturning y_transportReturnDate"
                                    id="check-out-datepicker" aria-describedby="check-out-date"></div>
                              </div>
                           </div>
                        </div>

                     </div>
                  </div>
               </div>
               <div class="col-md-4 col-md-offset-0 col-sm-6 col-sm-offset-0 col-xs-12 col-xs-12">
               <a href="/my-account/my-bookings/advanceSearch" id="search-advance-bookings" class="btn btn-primary btn-block">
                     <spring:theme code="text.bookings.search.advance.button" text="Search" />
                  </a>          
               </div>
            </div>
         </div>
   </div>
</div>

</div>