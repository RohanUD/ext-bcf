<%@ tag body-content="empty" trimDirectiveWhitespaces="true"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ attribute name="itinerary" required="true" type="de.hybris.platform.commercefacades.travel.ItineraryData"%>
<%@ attribute name="isOpenTicket" required="false" type="java.lang.Boolean"%>
<spring:htmlEscape defaultHtmlEscape="false" />
<div class="pb-3">
	<div class="row">
		<div class="col-md-offset-1 col-md-10">
			<div class="row">
				<div class="col-lg-4 col-xs-4 padding-0-mobile pr-0 text-center">
					<c:set var="transportOfferings" value="${itinerary.originDestinationOptions[0].transportOfferings}" />
					<c:if test="${isOpenTicket ne 'true'}">
						<fmt:formatDate var="departureTime" pattern="h:mm a" value="${itinerary.originDestinationOptions[0].transportOfferings[0].departureTime}" />
						<p class="fnt-14 mb-0">Depart: ${departureTime}</p>
					</c:if>
					<p class="font-weight-bold fnt-18 mb-0">${itinerary.route.origin.location.name}</p>
					<p class=" fnt-14 m-0">(${itinerary.route.origin.name})</p>
				</div>
				<div class="col-lg-4 col-xs-4 text-center">
					<c:if test="${isOpenTicket ne 'true'}">
						<h5 class="mt-0 mb-0 fnt-14 medium-grey-text">
							<c:set var="durationHour" value="${itinerary.duration['transport.offering.status.result.hours']}" /> 
							<c:set var="durationMinutes" value="${itinerary.duration['transport.offering.status.result.minutes']}" /> 
							${durationHour}h${durationMinutes}m
						</h5>
					</c:if>
					<span class="bcf bcf-icon-single-arrows-right icon-blue fnt-28"></span>
				</div>
				<div class="col-lg-4 col-xs-4 padding-0-mobile pl-0 text-center">
					<c:if test="${isOpenTicket ne 'true'}">
						<fmt:formatDate var="arrivalTime" pattern="h:mm a" value="${itinerary.originDestinationOptions[0].transportOfferings[fn:length(transportOfferings) - 1].arrivalTime}" />
						<p class="fnt-14 mb-0">Arrive: ${arrivalTime}</p>
					</c:if>
					<p class="font-weight-bold fnt-18 mb-0">${itinerary.route.destination.location.name}</p>
					<p class="m-0 fnt-14">(${itinerary.route.destination.name})</p>
				</div>
			</div>
		</div>
	</div>
</div>
