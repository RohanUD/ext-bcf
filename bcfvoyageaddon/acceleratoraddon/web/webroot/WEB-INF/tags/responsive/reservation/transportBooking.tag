<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="transportreservation" tagdir="/WEB-INF/tags/addons/bcfvoyageaddon/responsive/reservation"%>
<%@ taglib prefix="bookingDetails" tagdir="/WEB-INF/tags/addons/bcfstorefrontaddon/responsive/bookingdetails"%>
<%@ taglib prefix="format" tagdir="/WEB-INF/tags/addons/bcfstorefrontaddon/responsive/format"%>
<%@ attribute name="transportBookings" required="true" type="com.bcf.facades.reservation.data.ReservationDataList"%>
<spring:url value="/manage-booking/personal/confirm-cancellation" var="requestMultipleBookingCancelUrl" />

<c:if test="${fn:length(transportBookings.reservationDatas) gt 0}">
    <c:set var="listIndex" value="1" />
    <spring:url value="/manage-booking/booking-details" var="viewBookingUrl" />
    <div id="my-bookings-list-div">
        <div class="margin-bottom-20">
            <form:form method="POST" action="${fn:escapeXml(requestMultipleBookingCancelUrl)}" commandName="cancelBookingRefs" i="cancelRequestForm">
                <input type="hidden" class="cancelBookingRef" name="cancelBookingRefs" value="" />
                <a class="y_multiBookingCancellationButton submitForCancelBooking fnt-14 ">
                    <spring:theme code="button.page.mybookings.multi.cancelbooking" />
                </a>
            </form:form>
        </div>
        <div id="js-bookings-list-table-body">
            <c:forEach items="${transportBookings.reservationDatas}" var="reservationData" varStatus="reservationItemIdx">
                <c:if test="${not empty reservationData.reservationItems}">
                    <c:set var="bookingReferences" value="${null}" />
                    <div id="${listIndex}" class="panel panel-default">
                        <c:forEach items="${reservationData.reservationItems}" var="item" varStatus="itemIdx">
                            <div class="row text-center mt-4 margin-bottom-30">
                                <div class="col-lg-4 col-xs-8 padding-right-0">
                                    <c:choose>
                                        <c:when test="${item.bookingStatus ne 'CANCELLED'}">
                                            <c:if test="${item.cancelAllowed}">
                                                <label class="custom-checkbox-input pull-right">
                                                    <input type="checkbox" id="cancelCheckbox${idx.index}" class="cancelBookingCheckbox checkbox-label-align" value="${item.bcfBookingReference}" /> <span class="checkmark-checkbox"></span>
                                                </label>
                                            </c:if>
                                        </c:when>
                                        <c:otherwise>
                                        </c:otherwise>
                                    </c:choose>
                                    <c:choose>
                                        <c:when test="${item.reservationItinerary.originDestinationOptions[0].originDestinationRefNumber == 0}">
                                            <c:set var="labelPrefix" value="label.payment.reservation.transport.departure.sailing.time.prefix"></c:set>
                                        </c:when>
                                        <c:when test="${item.reservationItinerary.originDestinationOptions[0].originDestinationRefNumber == 1}">
                                            <c:set var="labelPrefix" value="label.payment.reservation.transport.return.sailing.time.prefix"></c:set>
                                        </c:when>
                                    </c:choose>
                                    <fmt:formatDate var="departureDate" pattern="EEE, MMM dd, yyyy" value="${item.reservationItinerary.originDestinationOptions[0].transportOfferings[0].departureTime}" />
                                </div>
                                <div class="col-lg-4 col-xs-8 padding-left-0 padding-right-0">
                                    <span class="font-weight-bold"><spring:theme code="${labelPrefix}" text="Departs" /></span> : ${departureDate}
                                </div>
                            </div>
                            <c:choose>
                                <c:when test="${item.reservationItinerary.originDestinationOptions[0].originDestinationRefNumber == 0}">
                                    <c:set var="labelPrefix" value="label.payment.reservation.transport.departure.sailing.total"></c:set>
                                </c:when>
                                <c:when test="${item.reservationItinerary.originDestinationOptions[0].originDestinationRefNumber == 1}">
                                    <c:set var="labelPrefix" value="label.payment.reservation.transport.return.sailing.total"></c:set>
                                </c:when>
                            </c:choose>
                            <p class="text-center my-3 fnt-14 mb-0">
                                <spring:theme code="label.search.bookings.sailing.reference.number" text="Booking" />
                                #: <strong>${item.bcfBookingReference}</strong>
                            </p>
                            <p class="text-center fnt-14 my-3">
                                <spring:theme code="text.page.mybookings.bookingStatus" text="Booking Status" />
                                : <strong>${item.bookingStatus}</strong>
                            </p>
                            <transportreservation:sailinginformation itinerary="${item.reservationItinerary}" />
                            
                            <hr>
                            <c:choose>
                                <c:when test="${itemIdx.first && not itemIdx.last}">
                                    <c:set var="bookingReferences" value="${item.bcfBookingReference}${'-'}" />
                                </c:when>
                                <c:otherwise>
                                    <c:set var="bookingReferences" value="${bookingReferences}${item.bcfBookingReference}" />
                                    <div class="row text-center">
                                        <div class="col-md-6 col-lg-6 col-sm-6 col-xs-12 mb-2 mt-4 col-md-offset-3 col-sm-offset-3 margin-bottom-20">
                                        <form id="editBookingForm" method="get" action="${fn:escapeXml(viewBookingUrl)}">
                                            <input type="hidden" name="eBookingCode" value="${bookingReferences}">
                                            <button type="submit" class="btn btn-primary btn-block">
                                                <span> <spring:theme code="text.manage.account.my.bookings.view.or.edit.booking" text="View / edit booking details" />
                                                </span>
                                            </button>
                                        </form>
                                        </div>
                                    </div>
                                    <br>
                                </c:otherwise>
                            </c:choose>
                        </c:forEach>
                    </div>
                </c:if>
                <c:set var="listIndex" value="${listIndex+1}" />
            </c:forEach>
        </div>
        <div>
            <form:form method="POST" action="${fn:escapeXml(requestMultipleBookingCancelUrl)}" commandName="cancelBookingRefs" class="cancelRequestForm">
                <input type="hidden" class="cancelBookingRef" name="cancelBookingRefs" value="" />
                <a class="y_multiBookingCancellationButton submitForCancelBooking fnt-14">
                    <spring:theme code="button.page.mybookings.multi.cancelbooking" />
                </a>
            </form:form>
        </div>
        <div class="pagination-section">
            <input type="hidden" id="js-bookings-page-size" value="${transportBookings.pageSize}">
            <div class="medium-grey-text text-center">
                <span><spring:theme code="text.manage.account.my.business.bookings.pagination.showing" text="Showing" />&nbsp;</span><span id="bookings-start-end"></span><span>&nbsp;<spring:theme code="text.manage.account.my.business.bookings.pagination.of" text="of" />&nbsp;
                </span><span id="total-bookings"></span>
            </div>
            <div class="col-md-12  col-xs-12 text-center">
                <ul class="pagination pagination-xs pager" id="js-bookings-table-pager"></ul>
            </div>
        </div>
    </div>
</c:if>
