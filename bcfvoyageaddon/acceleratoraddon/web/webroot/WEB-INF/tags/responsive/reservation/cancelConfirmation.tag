<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<div class="modal fade" id="y_multipleBookingCancelModal" tabindex="-1" role="dialog" aria-labelledby="cancelBookingLabel">
	<div class="modal-dialog" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true"><i class="bcf bcf-icon-cancel-solid"></i></span>
				</button>
				<h3 class="modal-title" id="cancelBookingLabel">
					<spring:theme code="text.page.managemybooking.cancel.booking.modal.title" text="Cancel Booking" />
				</h3>
			</div>
			<div class="modal-body">
				<h3>
					<spring:theme code="text.page.confirm.multiple.booking.cancellation.title" />
				</h3>
				<spring:theme code="text.page.confirm.multiple.booking.cancellation.subtitle" />
			</div>
			<div class="modal-footer">
				<div class="row">
					<div class="col-xs-12 col-sm-6">
						<button id="y_multipleBookingCancelUrl" class="btn btn-primary btn-block">
							<spring:theme code="text.quote.yes.button.label" />
						</button>
					</div>
					<div class="col-xs-12 col-sm-6">
						<button class="btn btn-secondary btn-block" data-dismiss="modal" id="y_noMultipleBookingCancelUrl">
							<spring:theme code="text.quote.no.button.label" />
						</button>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<div class="modal fade" id="y_noCheckBoxSelectedModal" tabindex="-1" role="dialog" aria-labelledby="cancelBookingLabel">
	<div class="modal-dialog" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true"><i class="bcf bcf-icon-cancel-solid"></i></span>
				</button>
			</div>
			<div class="modal-body">
				<spring:theme code="text.page.multi.cancelbooking.no.selection" />
			</div>
		</div>
	</div>
</div>
