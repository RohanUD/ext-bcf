<%@ tag body-content="empty" trimDirectiveWhitespaces="true"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ attribute name="vehicleBreakdownList" required="true" type="java.util.List"%>
<%@ attribute name="travellers" required="false" type="java.util.List"%>
<%@ attribute name="routeType" required="false" type="String"%>
<spring:htmlEscape defaultHtmlEscape="false" />
<c:forEach items="${vehicleBreakdownList}" var="vehicleFareBreakdown" varStatus="vehicleIdx">
	<li>
		<div class="row">
			<ul class="col-xs-9 col-md-9">
			<c:choose>
				<c:when test="${vehicleFareBreakdown.vehicleTypeQuantity.carryingLivestock and not(vehicleFareBreakdown.vehicleTypeQuantity.vehicleType.code eq 'UHL' or vehicleFareBreakdown.vehicleTypeQuantity.vehicleType.code eq 'OSL')}">
					<li>${fn:escapeXml(vehicleFareBreakdown.vehicleTypeQuantity.qty)}&nbsp;x&nbsp;${fn:escapeXml(vehicleFareBreakdown.vehicleTypeQuantity.vehicleType.name)}&nbsp;<spring:theme code="label.payment.reservation.traveller.details.carrying.livestock" text="with Livestock" /></li>
				</c:when>
				<c:otherwise>
					<li>${fn:escapeXml(vehicleFareBreakdown.vehicleTypeQuantity.qty)}&nbsp;x&nbsp;${fn:escapeXml(vehicleFareBreakdown.vehicleTypeQuantity.vehicleType.name)}</li>
				</c:otherwise>
			</c:choose>
			</ul>
			<ul class="col-xs-3 col-md-3 text-right">
				<li>${fn:escapeXml(vehicleFareBreakdown.vehicleFare.baseFare.formattedValue)}</li>
			</ul>
		</div>
		<div class="col-sm-12">${fn:escapeXml(vehicleFareBreakdown.vehicleTypeQuantity.vehicleWithSidecarOrTrailer)==true?'Vehicle with Sidecar/Trailer':'' }</div>
		<c:forEach items="${vehicleFareBreakdown.vehicleFare.taxes}" var="taxes" varStatus="taxesIdx">
			<div>
				<Strong>${taxes.price.formattedValue}</Strong>
			</div>
			<div>
				<Strong>${taxes.code}</Strong>
			</div>
		</c:forEach>
		<c:forEach items="${vehicleFareBreakdown.vehicleFare.discounts}" var="discounts" varStatus="discountsIdx">
			<div>
				<Strong>${discounts.price.formattedValue}</Strong>
			</div>
			<div>
				<Strong>${discounts.purpose}</Strong>
			</div>
		</c:forEach>

		<c:if test="${routeType eq 'LONG'}">
                    <c:set var="vehicleCount" value="1" />
                    <c:forEach items="${travellers}" var="traveller" varStatus="travellerIdx">
                        <c:if test="${traveller.travellerType eq 'VEHICLE' && traveller.travellerInfo.vehicleType.code eq vehicleFareBreakdown.vehicleTypeQuantity.vehicleType.code}">
                            <div class="row">
                                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                    Vehicle ${vehicleCount} License Plate : ${traveller.travellerInfo.licensePlateNumber}
                                </div>
                            </div>
                            <c:set var="vehicleCount" value="${vehicleCount+1}" />
                        </c:if>
                    </c:forEach>
                </c:if>
	</li>
</c:forEach>
