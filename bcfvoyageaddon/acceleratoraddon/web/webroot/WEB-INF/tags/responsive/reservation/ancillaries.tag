<%@ tag body-content="empty" trimDirectiveWhitespaces="true"%>
<%@ attribute name="offerBreakdowns" required="true" type="java.util.List"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<spring:htmlEscape defaultHtmlEscape="false" />
<c:if test="${not empty offerBreakdowns}">
	<c:forEach items="${offerBreakdowns}" var="offerBreakdown" varStatus="idx">
		<c:if test="${offerBreakdown.product.productType != 'ACCOMMODATION' || offerBreakdown.totalFare.basePrice.value.intValue() != 0 }">
			<li>
				<div class="row">
					<ul class="col-xs-9 col-md-9">
						<li>${fn:escapeXml(offerBreakdown.quantity)}&nbsp;x&nbsp;${fn:escapeXml(offerBreakdown.product.name)}</li>
					</ul>
					<ul class="col-xs-3 col-md-3 text-right">
						<li>
							<c:choose>
								<c:when test="${offerBreakdown.included}">
									<c:choose>
										<c:when test="${offerBreakdown.totalFare.totalPrice.value.intValue() == 0}">
											<spring:theme code="reservation.extras.free" text="Free" />
										</c:when>
										<c:otherwise>
											<span> ${fn:escapeXml(offerBreakdown.totalFare.totalPrice.formattedValue)} ( <spring:theme code="reservation.extras.included" text="Included" /> )
											</span>
										</c:otherwise>
									</c:choose>
								</c:when>
								<c:otherwise>
                                    ${fn:escapeXml(offerBreakdown.totalFare.totalPrice.formattedValue)}
                                </c:otherwise>
							</c:choose>
						</li>
					</ul>
				</div>
			</li>
		</c:if>
	</c:forEach>
</c:if>
