<%@ tag body-content="empty" trimDirectiveWhitespaces="true"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="reservation" tagdir="/WEB-INF/tags/addons/bcfvoyageaddon/responsive/reservation"%>
<%@ attribute name="cartModifiedEntries" required="true" type="java.util.List"%>
<%@ attribute name="journeyReferenceNumber" required="true" type="java.lang.Integer"%>
<%@ attribute name="originDestinationRefNumber" required="true" type="java.lang.Integer"%>
<c:forEach items="${cartModifiedEntries}" var="cartModifiedEntry" varStatus="cartModifiedEntryLoopStatus">
	<c:if test="${cartModifiedEntry.journeyRefNumber == journeyReferenceNumber && cartModifiedEntry.originDestinationRefNumber==originDestinationRefNumber}">
		<c:if test="${not empty cartModifiedEntry.newOrderEntries}">
			<p><Strong><spring:theme code="reservation.modification.items.added" text="Items added" /></Strong></p>
			<ul class="col-sm-12 list-unstyled">
				<c:forEach items="${cartModifiedEntry.newOrderEntries}" var="newOrderEntry">
					<li>
						<reservation:modificationEntryInfo orderEntry="${newOrderEntry}" />
					</li>
				</c:forEach>
			</ul>
		</c:if>
		<c:if test="${not empty cartModifiedEntry.changedOrderEntries}">
			<p><Strong><spring:theme code="reservation.modification.items.changed" text="Items changed" /></Strong></p>
			<ul class="col-sm-12 list-unstyled">
				<c:forEach items="${cartModifiedEntry.changedOrderEntries}" var="changedOrderEntry">
					<li>
						<reservation:modificationEntryInfo orderEntry="${changedOrderEntry}" />
					</li>
				</c:forEach>
			</ul>
		</c:if>
		<c:if test="${not empty cartModifiedEntry.removedOrderEntries}">
			<p><Strong><spring:theme code="reservation.modification.items.removed" text="Items removed" /></Strong></p>
			<ul class="col-sm-12 list-unstyled">
				<c:forEach items="${cartModifiedEntry.removedOrderEntries}" var="removedOrderEntry">
					<li>
						<reservation:modificationEntryInfo orderEntry="${removedOrderEntry}" />
					</li>
				</c:forEach>
			</ul>
		</c:if>
	</c:if>
</c:forEach>