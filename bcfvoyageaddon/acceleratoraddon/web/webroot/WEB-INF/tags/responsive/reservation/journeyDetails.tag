<%@ tag body-content="empty" trimDirectiveWhitespaces="true"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="reservation" tagdir="/WEB-INF/tags/addons/bcfvoyageaddon/responsive/reservation"%>
<%@ attribute name="itinerary" required="true" type="de.hybris.platform.commercefacades.travel.ItineraryData"%>
<%@ attribute name="journeyReferenceNumber" type="java.lang.Integer"%>
<%@ attribute name="originDestinationRefNumber" type="java.lang.Integer"%>
<spring:htmlEscape defaultHtmlEscape="false" />

<div class="row">
	<div class="col-sm-6">
		${departureLabel } <b> <fmt:formatDate pattern="${dateFormat}" value="${itinerary.originDestinationOptions[0].transportOfferings[0].departureTime}" />,<fmt:formatDate pattern="${timeFormat}"
		value="${itinerary.originDestinationOptions[0].transportOfferings[0].departureTime}" />
		</b><br> <strong>${fn:escapeXml(itinerary.route.origin.name)}(${fn:escapeXml(itinerary.route.origin.code)})</strong>
	</div>
	<div class="col-sm-6">
		${arrivalLabel } <b><c:set var="length" value="${fn:length(itinerary.originDestinationOptions[0].transportOfferings)}" /> <fmt:formatDate pattern="${dateFormat}" value="${itinerary.originDestinationOptions[0].transportOfferings[length-1].arrivalTime}" />,<fmt:formatDate
		pattern="${timeFormat}" value="${itinerary.originDestinationOptions[0].transportOfferings[length-1].arrivalTime}" /></b><br> <strong>${fn:escapeXml(itinerary.route.destination.name)}(${fn:escapeXml(itinerary.route.destination.code)})</strong>
	</div>
	<div class="col-sm-12">
		<hr>
	</div>
</div><br>
<c:forEach items="${itinerary.originDestinationOptions}" var="originDestinationOption" varStatus="optionIdx">
	<c:set var="multiSector" value="${fn:length(originDestinationOption.transportOfferings) > 1}" />
	<reservation:journeyBreakDown itinerary="${itinerary}" journeyReferenceNumber="${journeyReferenceNumber}" originDestinationRefNumber="${originDestinationRefNumber}" multiSector="${multiSector }" originDestinationOption="${originDestinationOption }"/>
	</c:forEach>
	
	
	

	
	
	

