<%@ tag body-content="empty" trimDirectiveWhitespaces="true"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ attribute name="ptcBreakdownList" required="true" type="java.util.List"%>
<%@ attribute name="vehicleBreakdownList" required="true" type="java.util.List"%>
<spring:htmlEscape defaultHtmlEscape="false" />
<div class="row margin-top-10 margin-bottom-10">
<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
	<p class="text-center mb-0 payment-fa">
		<c:forEach items="${ptcBreakdownList}" var="ptcFareBreakdown" varStatus="ptcIdx">
			<span><span class="user-icon"></span> ${fn:escapeXml(ptcFareBreakdown.passengerTypeQuantity.passengerType.code)}&nbsp;${ptcFareBreakdown.passengerTypeQuantity.quantity} </span>
		</c:forEach>
		<c:set var = "ctrWHP" value ="0"/>
		<c:forEach var="accessibility" items="${bcfFareFinderForm.passengerInfoForm.accessibilityRequestDataList}" varStatus="i">
         <c:forEach var="ancillary" items="${accessibility.ancillaryRequestDataList}" varStatus="i">
             <c:if test="${ancillary.code eq accessibility.selection }">
                <c:set var = "ctrWHP" value = "${ctrWHP + 1}"/>
             </c:if>
         </c:forEach>
        </c:forEach>
        <c:if test="${ctrWHP > 0}">
          <span><span class="wheelchair-icon"></span>${ctrWHP}</span>
        </c:if>
		<c:forEach items="${vehicleBreakdownList}" var="vehicleFareBreakdown" varStatus="vehicleIdx">
			<span><i class="fa fa-car"></i> ${fn:escapeXml(vehicleFareBreakdown.vehicleTypeQuantity.qty)}</span>
		</c:forEach>
	</p>
</div>
</div>
