<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="transportreservation" tagdir="/WEB-INF/tags/addons/bcfvoyageaddon/responsive/reservation"%>
<%@ taglib prefix="bookingDetails" tagdir="/WEB-INF/tags/addons/bcfstorefrontaddon/responsive/bookingdetails"%>
<%@ taglib prefix="format" tagdir="/WEB-INF/tags/addons/bcfstorefrontaddon/responsive/format"%>
<%@ attribute name="transportBookings" required="true" type="com.bcf.facades.reservation.data.ReservationDataList"%>
<spring:url value="/manage-booking/booking-details" var="viewBookingUrl" />
<spring:url value="/manage-booking/business/confirm-cancellation" var="requestMultipleBookingCancelUrl" />
<c:set var="tableRowIndex" value="1" />
<div id="my-bookings-list-div">
	<div class="margin-bottom-20">
		<div class="row">
			<div id="footerinfomation">
				<div class="col-lg-10 col-md-10 col-sm-10 col-xs-12 inline-form">
					<form:form method="POST" action="${fn:escapeXml(requestMultipleBookingCancelUrl)}" commandName="cancelBookingRefs" i="cancelRequestForm">
						<input type="hidden" class="cancelBookingRef" name="cancelBookingRefs" value="" />
						<a class="y_multiBookingCancellationButton submitForCancelBooking fnt-14 ">
							<spring:theme code="button.page.mybookings.multi.cancelbooking" />
						</a>
					</form:form>
					<spring:theme code="text.manage.account.my.business.bookings.multi.cancellation.help" text="To make changes or to cancel more than 10 bookings, please contact Customer Care, 1-888-223-3779." />
				</div>
			</div>
			<div class="col-lg-2 col-md-2 col-sm-2 col-xs-12">
				<a href="javascript:window.print()">
					<spring:theme code="text.manage.account.my.business.bookings.book.print.this.list" text="Print this list" />
				</a>
			</div>
		</div>
	</div>
	<div>
		<table id="transport-business-bookings" class="table table-hover">
			<thead>
				<tr>
					<th id="0"><input type="checkbox" id="js-select-all-on-page" /></th>
					<th id="1"><a href="javaScript: ;">
							<span><spring:theme code="text.manage.account.my.business.bookings.table.header.booking.ref" text="BOOKING REF" />#</span>
						</a></th>
					<th id="2"><a href="javaScript: ;">
							<span><spring:theme code="text.manage.account.my.business.bookings.table.header.booking.status" text="BOOKING STATUS" />#</span>
						</a></th>
					<th id="3"><a href="javaScript: ;">
							<spring:theme code="text.manage.account.my.business.bookings.table.header.date" text="DATE" />
						</a></th>
					<th id="4"><spring:theme code="text.manage.account.my.business.bookings.table.header.time" text="TIME" /></th>
					<th id="5"><a href="javaScript: ;">
							<spring:theme code="text.manage.account.my.business.bookings.table.header.from" text="FROM" />
						</a></th>
					<th id="6"><a href="javaScript: ;">
							<spring:theme code="text.manage.account.my.business.bookings.table.header.to" text="TO" />
						</a></th>
					<th id="7"><a href="javaScript: ;">
							<spring:theme code="text.manage.account.my.business.bookings.table.header.vehicle.type" text="VEHICLE TYPE" />
						</a></th>
					<th id="8"><spring:theme code="text.manage.account.my.business.bookings.table.header.vehicle.length" text="VEHICLE LENGTH" /></th>
					<th id="9"><spring:theme code="text.manage.account.my.business.bookings.table.header.no.pax" text="NO PAX" /></th>
					<th id="10"><spring:theme code="text.manage.account.my.business.bookings.table.header.change.allowed" text="CHANGE ALLOWED" /></th>
					<th id="11"><a href="javaScript: ;">
							<spring:theme code="text.manage.account.my.business.bookings.table.header.ref.code" text="REF CODE" />
						</a></th>
					<th id="12"><a href="javaScript: ;">
							<spring:theme code="text.manage.account.my.business.bookings.table.header.pax.names" text="PAX NAMES" />
						</a></th>
					<th id="13"><a href="javaScript: ;">
							<spring:theme code="text.manage.account.my.business.bookings.table.header.booked.by" text="BOOKED BY" />
						</a></th>
					<%-- <th id="14"><spring:theme code="text.manage.account.my.business.bookings.table.header.details" text="DETAILS" /></th> --%>
				</tr>
			</thead>
			<tbody id="js-bookings-list-table-body">
				<c:forEach items="${transportBookings.reservationDatas}" var="reservationData" varStatus="reservationItemIdx">
					<c:if test="${not empty reservationData.reservationItems}">
						<c:forEach items="${reservationData.reservationItems}" var="item" varStatus="itemIdx">
							<c:set var="paxnames" value="" />
							<c:forEach items="${item.reservationItinerary.travellers}" var="traveller" varStatus="travellerIdx">
								<c:if test="${traveller.travellerType eq 'PASSENGER' and (not empty fn:trim(traveller.travellerInfo.firstName) or not empty fn:trim(traveller.travellerInfo.surname))}">
									<c:set var="fullname" value="${fn:trim(traveller.travellerInfo.firstName)}&nbsp;${fn:trim(traveller.travellerInfo.surname)}" />
									<c:if test="${not empty fn:trim(fullname)}">
										<c:set var="paxnames" value="${paxnames}<div>${fn:trim(fullname)}</div>" />
									</c:if>
								</c:if>
							</c:forEach>
							<tr id="${tableRowIndex}">
								<td><c:if test="${item.bookingStatus ne 'CANCELLED' and item.cancelAllowed}">
										<label class="custom-checkbox-input pull-right">
											<input type="checkbox" id="cancelCheckbox${idx.index}" class="cancelBookingCheckbox checkbox-label-align js-checkbox" value="${item.bcfBookingReference}" /> <span class="checkmark-checkbox"></span>
										</label>
									</c:if></td>
								<td><a href="/manage-booking/booking-details?eBookingCode=${item.bcfBookingReference}">${item.bcfBookingReference}</a></td>
								<td>${item.bookingStatus}</td>
								<td><fmt:formatDate pattern="yyyy-MM-dd" value="${item.reservationItinerary.originDestinationOptions[0].transportOfferings[0].departureTime}" /></td>
								<td><fmt:formatDate pattern="hhmm" value="${item.reservationItinerary.originDestinationOptions[0].transportOfferings[0].departureTime}" /></td>
								<td>${item.reservationItinerary.route.origin.code}</td>
								<td>${item.reservationItinerary.route.destination.code}</td>
								<td>${item.vehicleType}</td>
								<td>${item.vehicleLength}</td>
								<td>${item.passengerCount}</td>
								<td><spring:theme code="text.manage.account.my.business.bookings.change.allowed.${item.modifyAllowed}" text="Changes allowed in yes or no" /></td>
								<td>${item.routeReference}</td>
								<td>${paxnames}</td>
								<td>${item.bookerName}</td>
								<%-- <td><a href="/manage-booking/booking-details?eBookingCode=${item.bcfBookingReference}">
										<span>...</span>
									</a></td> --%>
							</tr>
							<c:set var="tableRowIndex" value="${tableRowIndex+1}" />
						</c:forEach>
					</c:if>
				</c:forEach>
			</tbody>
		</table>
	</div>
	<div class="row">
		<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
			<form:form method="POST" action="${fn:escapeXml(requestMultipleBookingCancelUrl)}" commandName="cancelBookingRefs" class="cancelRequestForm">
				<input type="hidden" class="cancelBookingRef" name="cancelBookingRefs" value="" />
				<a class="y_multiBookingCancellationButton submitForCancelBooking fnt-14">
					<spring:theme code="button.page.mybookings.multi.cancelbooking" />
				</a>
			</form:form>
		</div>
		<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
			<spring:theme code="text.manage.account.my.business.bookings.multi.cancellation.help" text="To make changes or to cancel more than 10 bookings, please contact Customer Care, 1-888-223-3779." />
		</div>
	</div>
	<div class="pagination-section">
		<input type="hidden" id="js-bookings-page-size" value="${transportBookings.pageSize}">
		<div class="medium-grey-text text-center">
			<span><spring:theme code="text.manage.account.my.business.bookings.pagination.showing" text="Showing" />&nbsp;</span><span id="bookings-start-end"></span><span>&nbsp;<spring:theme code="text.manage.account.my.business.bookings.pagination.of" text="of" />&nbsp;
			</span><span id="total-bookings"></span>
		</div>
		<div class="col-md-12 text-center">
			<ul class="pagination pagination-xs pager" id="js-bookings-table-pager"></ul>
		</div>
	</div>
</div>
