<%@ tag body-content="empty" trimDirectiveWhitespaces="true"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ attribute name="accessibilityBreakdownDataList" required="true" type="java.util.List"%>

<spring:htmlEscape defaultHtmlEscape="false" />
<c:forEach items="${accessibilityBreakdownDataList}" var="accessibilityItemData" varStatus="ptcIdx">
	<li>
		<div class="row">
			<ul class="col-xs-9 col-md-9">
				<li>${fn:escapeXml(accessibilityItemData.quantity)}&nbsp;x&nbsp;${fn:escapeXml(accessibilityItemData.name)}</li>
			</ul>
			<ul class="col-xs-3 col-md-3 text-right">
				<li>${fn:escapeXml(accessibilityItemData.price.formattedValue)}</li>
			</ul>
		</div>
	</li>
</c:forEach>
