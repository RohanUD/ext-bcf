<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="cms" uri="http://hybris.com/tld/cmstags"%>
<%@ taglib prefix="ycommerce" uri="http://hybris.com/tld/ycommercetags"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="additionalInfo" uri="additionalInfoTag"%>
<spring:htmlEscape defaultHtmlEscape="false" />
<div id="acceptableFormsModal" class="modal fade" role="dialog">
  <div class="modal-dialog">
  <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true"><i class="bcf bcf-icon-cancel-solid"></i></span></button>
          <h4 class="modal-title">Modal title</h4>
        </div>
    <div class="modal-content">
      <div class="modal-body">
          <additionalInfo:additionalInfo code="text.farefinder.additionalinfo.acceptableforms" /></div>
      </div>
      <div class="modal-footer">
      </div>
    </div>
  </div>
