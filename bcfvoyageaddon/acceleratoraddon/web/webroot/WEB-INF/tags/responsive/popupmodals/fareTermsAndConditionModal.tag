<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="cms" uri="http://hybris.com/tld/cmstags"%>
<%@ taglib prefix="ycommerce" uri="http://hybris.com/tld/ycommercetags"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<spring:htmlEscape defaultHtmlEscape="false" />
<div id="fareTermsAndConditionModal" class="modal fade" role="dialog">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
      <button align="right" type="button" class="close" data-dismiss="modal"><i class="bcf bcf-icon-cancel-solid"></i></button>
        <h1 class="modal-title"><div id="fareTermsAndConditionPageTitle" /></h1>
      </div>
      <div class="modal-body">
          <div id="fareTermsAndConditionPage" />
      </div>
      <div class="modal-footer">
      </div>
    </div>
  </div>
</div>
