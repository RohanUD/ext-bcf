<%@ page trimDirectiveWhitespaces="true"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="cms" uri="http://hybris.com/tld/cmstags"%>
<%@ taglib prefix="ycommerce" uri="http://hybris.com/tld/ycommercetags"%>
<%@ taglib prefix="common" tagdir="/WEB-INF/tags/responsive/common"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<jsp:useBean id="now" class="java.util.Date" />
<%@ page language="java" contentType="text/html;charset=UTF-8"%>
<%@ taglib prefix="format"
	tagdir="/WEB-INF/tags/addons/bcfstorefrontaddon/responsive/format"%>
<%@ taglib prefix="fareselection"
	tagdir="/WEB-INF/tags/addons/bcfvoyageaddon/responsive/fareselection"%>
<%@ taglib prefix="sailing"
	tagdir="/WEB-INF/tags/responsive/farecalculator"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>


<div class="container">


<b><spring:theme code="text.ferry.calculator.summary.fare.calculator"
		text="Fare Calculator" /></b>
<br />
</div>
<hr class="">
<c:if test="${showNonBookableInfo or showLoadingRestrictionInfo}">
<div class="seasonal-message-box margin-top-0">
               <div class="error-msg-icon">
                  <img src="${commonResourcePath}/images/bcf bcf-icon-notice-outline bcf-2x.svg">
               </div>
	<div class="error-msg-text-box">
	<c:if test="${showNonBookableInfo}">
		<spring:theme code="text.ferry.calculator.not.bookable.message" />
		<br />
	</c:if>
	<c:if test="${showLoadingRestrictionInfo}">
		<spring:theme code="text.ferry.calculator.vehicle.message" />
		<br />
	</c:if>
	</div>
</div>
</c:if>

<div class="container">
<div class="margin-bottom-30">
<b><spring:theme code="text.ferry.calculator.summary.fare.results"
		text="Fare results" /></b>
		</div>
</div>

<c:if test="${fareCalculatorForm.pricedItinerary.itinerary.tripType eq 'SINGLE'}">
	<c:set value="${fn:escapeXml(fareCalculatorForm.itineraryPricingInfo.totalFare.totalPrice.value)}" var="formattedPrice"/>
	<sailing:sailingDetails fareCalculatorForm="${fareCalculatorForm}" />
	<br />
</c:if>
<c:if test="${fareCalculatorForm.pricedItinerary.itinerary.tripType eq 'RETURN'}">
    <sailing:sailingDetails fareCalculatorForm="${outBoundForm}" />
	<c:set value="${fn:escapeXml(journeyTotal + fareCalculatorForm.itineraryPricingInfo.totalFare.totalPrice.value)}" var="formattedPrice"/>

	<div class="container">
		<div class="js-accordion js-accordion-default payment-accordion">
			<h4 class="booking-detail-title">
				<spring:theme code="text.ferry.calculate.return" />
				&nbsp;<fmt:parseDate value="${bcfFareFinderForm.routeInfoForm.returnDateTime}" var="parsedDepartDate" pattern="MM/dd/yyyy" />
            <fmt:formatDate value="${parsedDepartDate}" var="formattedParsedDepartDate" pattern="E, MMM dd" />${formattedParsedDepartDate}
			</h4>

			<div>
				<div class="pb-3">
					<div class="row">
						<div class="col-md-offset-1 col-md-10">
							<div class="row">
								<div class="col-lg-2 col-xs-4 padding-0-mobile pr-0 text-center">
									<p class="fnt-14 m-0">
										<fmt:formatDate pattern="h:mm a"
											value="${fareCalculatorForm.itineraryPricingInfo.bundleTemplates[0].transportOfferings[0].departureTime}" />
									</p>
									<fareselection:departArrivalLocationFormat location="${bcfFareFinderForm.routeInfoForm.arrivalLocationName}"/>
									<p class=" fnt-14 m-0"></p>
								</div>
								<div class="col-lg-8 col-xs-4 text-center">
									<h5 class="mt-0 mb-0 fnt-14 medium-grey-text font-weight-normal">
										${fareCalculatorForm.departureDurationHour}
										<spring:theme code="text.ferry.calculate.duration.hour" />
										&nbsp;${fareCalculatorForm.departureDurationMinute}
										<spring:theme code="text.ferry.calculate.duration.minute" />
									</h5>
									<span class="bcf bcf-icon-single-arrows-right return-tripType"></span>

								</div>
								<div class="col-lg-2 col-xs-4 padding-0-mobile pl-0 text-center">

									<p class="fnt-14 m-0">
										<fmt:formatDate pattern="h:mm a"
											value="${fareCalculatorForm.itineraryPricingInfo.bundleTemplates[0].transportOfferings[0].arrivalTime}" />
									</p>
									<fareselection:departArrivalLocationFormat location="${bcfFareFinderForm.routeInfoForm.departureLocationName}"/>
									<p class="m-0 fnt-14"></p>
								</div>
							</div>
						</div>
					</div>
				</div>

				<div class="row margin-top-10 margin-bottom-10">
					<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
						<p class="text-center mb-0 payment-fa">
							<span> <c:forEach var="passenger"
									items="${bcfFareFinderForm.passengerInfoForm.passengerTypeQuantityList}"
									varStatus="i">
									<c:if test="${passenger.quantity > 0 }">
									<span class="fa fa-user padding-0 grey-txt-i"></span>
									<span class="caps-f padding-0 font-weight-bold">${passenger.quantity}</span>
									</c:if>
								</c:forEach> <c:set var="ctrWHP" value="0" /> <c:set var="ancillaryIcon" />
								<c:forEach var="accessibility"
									items="${bcfFareFinderForm.passengerInfoForm.accessibilityRequestDataList}"
									varStatus="i">
									<c:forEach var="specialService"
										items="${accessibility.specialServiceRequestDataList}"
										varStatus="i">
										<c:if test="${specialService.selection}">
										</c:if>
									</c:forEach>
									<c:forEach var="ancillary"
										items="${accessibility.ancillaryRequestDataList}"
										varStatus="i">
										<c:if test="${ancillary.code eq accessibility.selection }">
											<c:set var="ctrWHP" value="${ctrWHP + 1}" />
											<c:set var="ancillaryIcon" value="${accessibility.selection}" />
										</c:if>
									</c:forEach>
								</c:forEach> <c:if test="${ctrWHP > 0}">
									<span><span class="${ancillaryIcon}"></span>${ctrWHP}</span>
								</c:if>
							</span> <span> <c:if
									test="${bcfFareFinderForm.passengerInfoForm.travellingAsWalkOn eq false}">
									<c:set var="vehicleInfo"
										value="${bcfFareFinderForm.vehicleInfoForm.vehicleInfo[0]}" />
									<c:set var="vehicleIcon"
										value="${vehicleInfo.vehicleType.code}" />
									<c:set var="vehicleCount" value="1" />
									<c:if
										test="${bcfFareFinderForm.routeInfoForm.tripType=='RETURN'}">
										<c:set var="vehicleCount" value="2" />
									</c:if>
									<c:set var="vehicleFareBreakdownData"
										value="${fareCalculatorForm.itineraryPricingInfo.vehicleFareBreakdownDatas[0]}" />
									
									<span><span class="${vehicleIcon} grey-bg grey-txt-i"></span><span class="padding-0 font-weight-bold">${vehicleFareBreakdownData.vehicleTypeQuantity.qty}</span></span>
								</c:if>
							</span>
						</p>
					</div>
				</div>
				<div class="row">
					<div class="col-md-12 margin-bottom-17">
						<p class="text-center my-3 fnt-14">
							<a class="font-italic" href="/ship-info/${fareCalculatorForm.shipName}">${fareCalculatorForm.shipName}</a>
						</p>
					</div>

					<div class="col-md-10 col-md-offset-1">
						
						<div class="">
							<h5 class="my-3">
								<strong><spring:theme
										code="text.ferry.calculator.summary.vehicles.and.passengers"
										text="Vehicles & passengers" /></strong>
							</h5>



							<div class="row">
								<ul class="list-unstyled payment-vp">


									<c:forEach var="vehicleFareBreakdownData"
										items="${fareCalculatorForm.itineraryPricingInfo.vehicleFareBreakdownDatas}"
										varStatus="ptcIdx">
										<c:if
											test="${vehicleFareBreakdownData.vehicleTypeQuantity.qty ne 0}">
											<li>
												<div class="list-unstyled col-xs-9">
												${vehicleFareBreakdownData.vehicleTypeQuantity.qty}&nbsp;x&nbsp;
												${vehicleFareBreakdownData.vehicleTypeQuantity.vehicleType.name}&nbsp;
												${vehicleFareBreakdownData.vehicleTypeQuantity.vehicleType.description}</div>
												<div class="list-unstyled col-xs-3 text-right">
													<c:forEach var="fareDetail"
														items="${vehicleFareBreakdownData.fareInfos.fareDetails}"
														varStatus="fareDetailIdx">
														<p>
															<format:price priceData="${fareDetail.fareProduct.price}" />
														</p>
													</c:forEach>
												</div>
											</li>
										</c:if>
									</c:forEach>


									<c:forEach var="ptcBreakDownData"
										items="${fareCalculatorForm.itineraryPricingInfo.ptcFareBreakdownDatas}"
										varStatus="ptcIdx">
										<c:if
											test="${ptcBreakDownData.passengerTypeQuantity.quantity ne 0}">
											<li>
												<div class="list-unstyled col-xs-9">${ptcBreakDownData.passengerTypeQuantity.quantity}&nbsp;x&nbsp;${ptcBreakDownData.passengerTypeQuantity.passengerType.name}</div>
												<div class="list-unstyled col-xs-3 text-right">
													<c:forEach var="fareInfo"
														items="${ptcBreakDownData.fareInfos}"
														varStatus="fareInfoIdx">
														<c:forEach var="fareDetail"
															items="${fareInfo.fareDetails}" varStatus="fareDetailIdx">
															<p>
																<format:price
																	priceData="${fareDetail.fareProduct.price}" />
															</p>
														</c:forEach>
													</c:forEach>
												</div>
											</li>
										</c:if>
									</c:forEach>

                                    <c:forEach var="otherChargesData"
                                        items="${fareCalculatorForm.itineraryPricingInfo.otherCharges}"
                                        varStatus="ptcIdx">
                                        <c:if
                                            test="${otherChargesData.quantity ne 0}">
                                            <li>
                                                <div class="list-unstyled col-xs-9"><spring:theme code="text.listsailing.price.itinerary.other.charges" text="Other charges" /></div>
                                                <div class="list-unstyled col-xs-3 text-right">
                                                <p><format:price priceData="${otherChargesData.price}" /></p>
                                                </div>
                                            </li>
                                        </c:if>
                                    </c:forEach>

								</ul>
							</div>
							<div class="departure-bx payment-total">
								<h5 class="font-weight-normal">
									<spring:theme
										code="text.ferry.calculator.summary.return.sailing.total"
										text="Return sailing total" />
									<span><strong><format:price
												priceData="${fareCalculatorForm.itineraryPricingInfo.totalFare.totalPrice}" /></strong></span>
								</h5>
							</div>

						</div>


					</div>

				</div>

			</div>


		</div>
	</div>

</c:if>

<div class="container">
<div class="departure-bx payment-total margin-top-30">
	<h5>
		<spring:theme code="text.ferry.calculate.fare.total.fare"/><span><strong><fmt:formatNumber type="currency" value="${formattedPrice}" currencySymbol="${fn:escapeXml(currentCurrency.symbol)}"/></strong></span>
	</h5>
</div>
</div>

<div class="container">

   <div class="fare-calculator-text border-bottom margin-top-30 margin-bottom-30">
   <spring:theme code="label.packageferryselection.fareselection.lastupdated"/>&nbsp;
   <fmt:formatDate value="${now}" pattern="h:mm a, EEE MMM dd, yyyy"/><br><br>
  <spring:theme code="text.ferry.calculator.summary.message" />
</div>

	<div class="col-lg-6 col-md-6 col-md-offset-3">
		<div class="row">
			<div class="col-lg-6 col-md-6 mb-3">
				<a href="${pageContext.request.contextPath}/fare-selection/clear-session-calculatefare"
					class="btn btn-primary btn-block"> <spring:theme
						code="text.ferry.calculate.fare.calculate.another.fare"
						text="Calculate another fare" />
				</a>
			</div>
			<div class="col-lg-6 col-md-6">
			    <c:choose>
                    <c:when test="${showNonBookableInfo}">
                        <c:url var="seasonalScheduleUrl" value="/seasonal-schedules">
                            <c:param name="departurePort" value="${bcfFareFinderForm.routeInfoForm.departureLocation}"/>
                            <c:param name="arrivalPort" value="${bcfFareFinderForm.routeInfoForm.arrivalLocation}"/>
                            <c:if test="${not empty bcfFareFinderForm.routeInfoForm.departingDateTime and bcfFareFinderForm.routeInfoForm.returnDateTime}">
                                <fmt:parseDate var="departureDateObj" value="${bcfFareFinderForm.routeInfoForm.departingDateTime}"
                                               pattern="mm/dd/yyy"/>
                                <fmt:formatDate value="${departureDateObj}" var="departureDate" pattern="yyyymmdd"/>

                                <fmt:parseDate var="arrivalDateObj" value="${bcfFareFinderForm.routeInfoForm.returnDateTime}"
                                               pattern="mm/dd/yyy"/>
                                <fmt:formatDate value="${arrivalDateObj}" var="arrivalDate" pattern="yyyymmdd"/>

                                <c:param name="departureDate" value="${departureDate}-${arrivalDate}"/>
                            </c:if>
                        </c:url>
                        <a href="${seasonalScheduleUrl}" class="btn btn-outline-blue btn-block">
                            <spring:theme code="text.ferry.calculate.fare.view.sailing.schedules" text="View sailing schedules" />
                        </a>
                    </c:when>
                    <c:otherwise>
                        <a href="/RouteSelectionPage" class="btn btn-outline-blue btn-block">
                            <spring:theme code="sailing.link.bookThisRoute.text" text="Book this route" />
                        </a>
                    </c:otherwise>
                </c:choose>
			</div>
		</div>
	</div>

</div>


