<%@ page trimDirectiveWhitespaces="true"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="cms" uri="http://hybris.com/tld/cmstags"%>
<%@ taglib prefix="ycommerce" uri="http://hybris.com/tld/ycommercetags"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib uri = "http://java.sun.com/jsp/jstl/core" prefix = "c" %>
<%@ page language="java" contentType="text/html;charset=UTF-8" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@ taglib prefix="mapPath" tagdir="/WEB-INF/tags/addons/bcfcommonsaddon/responsive/map"%>
<spring:htmlEscape defaultHtmlEscape="false" />
<c:url value="/routes-fares/discover-route-map" var="regionFinderFormAction" />
<c:url var="dailySchedulesUrl" value="/routes-fares/schedules/"/>
<noscript><input type="submit" value="Submit"></noscript>
<div class="container">
<div class="margin-bottom-30 p-relative">

 <div class="bg-image-map-text"><img src="../../../../_ui/responsive/common/images/bg-map.png"> <h2><spring:theme code="text.choose.a.region" text="Choose a region to start"/></h2></div>
    <c:if test="${not empty travelRoutelist}">
    <c:set var="googleApiKey" value="${google_api_key}"/>
    <c:if test="${not empty googleApiKey}">
         <c:set var="url" value="${dailySchedulesUrl}"/>
         <div class="row">
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 mb-4">
                    <div class="map-wrap y_propertyMapDiscover" id="discovermap"
                        data-schedulesurl="${url}"
                        data-googleapi="${googleApiKey}">
                    </div>
        </div>
        </div>
    </c:if>
    </c:if>
    <div class="col-md-5 discoverroutes-drop-down">
    <c:if test="${not empty regionFinderForm}">
    <form:form action="${regionFinderFormAction}" method="get" commandName="regionFinderForm">
        <form:select path="routeRegions" class="selectpicker form-control discoverroutes-custom map-dropdwon-custom" onchange='this.form.submit()'>
            <form:options items="${routeRegions}"/>
        </form:select>
    </form:form>
    </c:if>
    </div>
</div>

</div>
<mapPath:maptravelroute travelRoutelist="${travelRoutelist}" originToDestinationMapping="${originToDestinationMapping}" />
<script type="text/javascript" defer="true" src="${commonResourcePath}/js/acc.discoverOurRoute.js"></script>
