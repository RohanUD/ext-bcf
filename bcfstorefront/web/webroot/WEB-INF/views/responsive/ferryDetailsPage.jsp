<%@ page trimDirectiveWhitespaces="true"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="cms" uri="http://hybris.com/tld/cmstags"%>
<%@ taglib prefix="ycommerce" uri="http://hybris.com/tld/ycommercetags"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<div class="container px-0  white-bg-only">
		<div id="ferryDetails">
		    <c:if test="${transportVehicleInfo.shipDisplay eq true}">
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 mb-5">
                <h3 class="mt-2 mb-3 text-dark-blue">
                    <strong>${transportVehicleInfo.name}</strong>
                </h3>
                <p class="mb-5">
                    <b><spring:theme code="label.ferry.details.about.ferry"
                        text="About this ferry" /></b>
                </p>
                <p class="mt-5">${transportVehicleInfo.description}</p>
            </div>
            <div class="text-center ferry-ship-icon">
            <c:if test="${not empty transportVehicleInfo.shipIcon}">
                <img class='js-responsive-image'
                    alt='${transportVehicleInfo.shipIconAltText}' title='${transportVehicleInfo.shipIconAltText}' data-media='${transportVehicleInfo.shipIcon}'  />
            </c:if>
            </div>
		    <div class="row mb-5">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                        <c:if test="${transportVehicleInfo.otherImages ne null }">
                                <div class="js-owl-carousel js-owl-rotating-gallery owl-carousel owl-theme ferry-detail-owl">
                                    <c:forEach var="image"
                                        items="${transportVehicleInfo.otherImages}">
                                        <img class='img-fluid  img-w-h js-responsive-image'
                                            alt='${image.altText}' title='${image.altText}' data-media='${image.url}' />
                                    </c:forEach>
                            </div>
                        </c:if>
                </div>
            </div>
            <div class="row mb-5 text-center">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <button type="submit"
                        class="btn btn-outline-blue btn-custome-width">
                        <spring:theme code="button.ferry.details.track.ferry"
                            text="Track this ferry" />
                    </button>
                </div>
            </div>
            <div class="row my-5">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
			        <hr>
			    </div>
			</div>

			<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 mb-5 ferrydetails-accordion-sec">
			    <div class="row">
				<div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
					<div id="" class="js-accordion js-accordion-contentpage custom-ferrycontentpage-accordion  mb-3">
					    <div class="ferrydetails-accordion onboard-accordion">
                            <header>
                                <span class="bcf bcf-icon-food"></span>
                                <spring:theme code="label.ferry.details.onboard.amenities"
                                    text="Onboard Amenities" />
                                <span class="custom-arrow"></span>
                            </header>
                            <div>
                                <div class="tabel-ferry-build">
                                <ul class="list-inline">
                                    <c:forEach items="${transportVehicleInfo.shipFacilities}"
                                        var="shipFacility" varStatus="status">
                                        <c:set var="alignment" value="left" />
                                          <c:if test="${not empty shipFacility.logo}">
                                            <li><img class='img-rounded ferry-logo js-responsive-image' alt='${altText}'
                                            title='${altText}' data-media='${shipFacility.logo}'
                                            /></li>
                                            </c:if>
                                    </c:forEach>
                                </ul>
                                    <div class="padding-override info-text ferrydetails-heading">
                                            <spring:theme
                                            code="label.ferry.details.available.onboard"
                                            text="Also available onboard this ferry:" />
                                    </div>
                                    <c:forEach items="${transportVehicleInfo.shipFacilities}"
                                        var="shipFacility" varStatus="status">
                                        <c:if test="${empty shipFacility.logo}">
                                            <div class="ferrydetails-onboard-sec padding-override">
                                                <div class="col-lg-10 col-md-10 col-sm-10 col-xs-10">${shipFacility.shortDescription}</div>
                                                <div class="col-lg-2 col-md-2 col-sm-2 col-xs-2 text-center ferrydetails-onboard-right"><span class="bcf bcf-icon-checkmark"></span></div>
                                            </div>
                                        </c:if>
                                    </c:forEach>
                                </div>
                            </div>
						</div>


                        <div class="ferrydetails-accordion">
                            <header>
                                <span class="bcf bcf-icon-safety"></span>
                                <spring:theme code="label.ferry.details.safety" text="Safety" />
                            </header>
                            <div>
                                ${transportVehicleInfo.safetyDescription}
                                <div class="row content-pdf mt-4">
                                    <c:if test="${not empty transportVehicleInfo.safetyPdfDownloadUrl}">
                                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                            <div class="accessibility-download-text fnt-14 my-3">
                                                <span class="bcf bcf-icon-PDF mr-1"></span>
                                                <a href="${transportVehicleInfo.safetyPdfDownloadUrl}">${transportVehicleInfo.safetyPdfName}</a>
                                            </div>
                                            <p class="pdf-light-text fnt-12">
                                                <a href="${transportVehicleInfo.safetyPdfDownloadUrl}">
                                                    ${transportVehicleInfo.safetyPdfHelpInfo}
                                                </a>
                                            </p>
                                        </div>
                                    </c:if>
                                </div>
                            </div>
                        </div>
                        <div class="ferrydetails-accordion">
                            <header>
                                <span class="bcf bcf-icon-adventure-circle"></span>
                                <spring:theme code="label.ferry.details.linksMore" text="Links & more" />
                            </header>
                            <div>
                                ${transportVehicleInfo.generalDescription}
                            </div>
                        </div>

					</div>
				</div>
				<div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
					<div class="js-accordion js-accordion-contentpage custom-ferrycontentpage-accordion  mb-3">

                        <div class="ferrydetails-accordion build-accordion">
                            <header>
                                <span class="bcf bcf-icon-ferry"></span>
                                <spring:theme code="label.ferry.details.build.statistics"
                                    text="Build Statistics" />
                                <span class="custom-arrow"></span>
                            </header>
                            <div>
                                <div class="ferrydetails-build-statistics">
                                <ul class="list-group no-border">
                                  <li class="list-group-item">
                                  <span class="information-data"><spring:theme code="label.ferry.details.statistics.built" text="Built" /></span>
                                  <span class="information-value">${transportVehicleInfo.built}</span>
                                  </li>

                                  <li class="list-group-item">
                                   <span class="information-data"><spring:theme code="label.ferry.details.statistics.overall.length" text="Overall Length" /></span>
                                  <span class="information-value"> ${transportVehicleInfo.overallLength}</span>
                                  </li>

                                  <li class="list-group-item">
                                   <span class="information-data"><spring:theme code="label.ferry.details.statistics.maximum.displacement" text="Maximum Displacement" /> </span>
                                  <span class="information-value">${transportVehicleInfo.maximumDisplacement} </span>
                                  </li>
                                  <li class="list-group-item">
                                   <span class="information-data"><spring:theme
                                  code="label.ferry.details.statistics.car.capacity"
                                  text="Car Capacity" /> </span>
                                  <span class="information-value">
                                  ${transportVehicleInfo.carCapacity} </span>
                                  </li>
                                  <li class="list-group-item">
                                   <span class="information-data"><spring:theme
                                  code="label.ferry.details.statistics.passenger.crew.capacity"
                                  text="Passenger & Crew Capacity" /></span>
                                  <span class="information-value"> ${transportVehicleInfo.totalTravellerCapacity}</span>
                                  </li>
                                  <li class="list-group-item">
                                     <span class="information-data"><spring:theme
                                     code="label.ferry.details.statistics.maximum.speed"
                                     text="Maximum Speed" /></span>
                                     <span class="information-value">
                                      ${transportVehicleInfo.maximumSpeed}</span>
                                   </li>

                                    <li class="list-group-item">
                                     <span class="information-data"><spring:theme
                                        code="label.ferry.details.statistics.horsepower"
                                        text="Horsepower" /></span>
                                        <span class="information-value"> ${transportVehicleInfo.power}</span>
                                  </li>

                                </ul>

                                </div>
                            </div>
						</div>
                        <div class="ferrydetails-accordion">
                            <header>
                                <span class="bcf bcf-icon-wheelchair"></span>
                                <spring:theme code="label.ferry.details.accessibility"
                                    text="Accessibility" />
                            </header>
                            <div>${transportVehicleInfo.accessibilityDescription}
                                <c:if test="${not empty transportVehicleInfo.accessibilityPdfDownloadUrl}">
                                    <div class="row my-4">
                                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 my-3 accessibility-download-text fnt-14">
                                            <span class="bcf bcf-icon-PDF mr-1"></span>
                                            <a href="${transportVehicleInfo.accessibilityPdfDownloadUrl}">
                                                ${transportVehicleInfo.accessibilityPdfName}
                                            </a>
                                            <p class="pdf-light-text fnt-12">
                                                <a href="${transportVehicleInfo.accessibilityPdfDownloadUrl}">
                                                    ${transportVehicleInfo.accessibilityPdfHelpInfo}
                                                </a>
                                            </p>

                                        </div>
                                    </div>
                                </c:if>
                            </div>
                        </div>
					</div>
				</div>
			</div>
			</div>
			</c:if>
		    <c:if test="${transportVehicleInfo.shipDisplay eq false}">
				<spring:theme code="label.ferry.details.ferry.not.exist" text="Sorry, Ferry does not exist" />
		    </c:if>
		</div>
</div>
<div class="container-fluid bg-light-gray padding-top-20 padding-bottom-30 margin-top-20 margin-bottom-30">
	<div class="container">
		<div
			class="col-lg-12 col-md-12 col-sm-12 col-xs-12 offset-1 pt-2 pb-2">
			<h4 class="my-4 font-weight-bold">
				<spring:theme code="label.ferry.details.find.another.ferry"
					text="Find another ferry" />
			</h4>
			<div class="row">
				<div class="col-lg-8 col-md-8  col-sm-12  col-xs-12 mb-3">
					<div class="find-another-ferry">
						<Select class="form-control  selectpicker" id='transportVehiclesSelect'>
							<option value="default"><spring:theme code="label.ferry.details.make.selection" text="Please make a selection"/></option>
							<c:forEach var="entry" items="${transportVehiclesList}">

                                <c:choose>
                                    <c:when test="${not empty entry.name}">
                                        <c:set var="ferryName" value="${entry.name}"/>
                                    </c:when>
                                    <c:otherwise>
                                        <c:set var="ferryName" value="${entry.code}"/>
                                    </c:otherwise>
                                </c:choose>

                                <option value="${ferryName}/${entry.code}">
                                        ${ferryName}
								</option>
							</c:forEach>
						</Select>
						<span class="custom-arrow"></span>
					</div>
				</div>
				<div class="col-lg-4 col-md-4  col-sm-12  col-xs-12  mb-3">
						<button
							class="y_findFerryDetailResult btn btn-primary btn-block"
							type="submit" id="" value="Submit">
							<spring:theme code="button.ferry.details.find.ferry"
								text="Find ferry" />
						</button>
				</div>

			</div>


		</div>
	</div>

</section>


