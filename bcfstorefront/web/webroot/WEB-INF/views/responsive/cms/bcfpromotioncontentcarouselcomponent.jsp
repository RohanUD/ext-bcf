<%@ page trimDirectiveWhitespaces="true" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="cms" uri="http://hybris.com/tld/cmstags" %>
<%@ taglib prefix="bcfcms" tagdir="/WEB-INF/tags/addons/bcfstorefrontaddon/responsive/cms"%>

<bcfcms:displayCount displayCount="${displayCount}"/>
<section style="background:${backgroundColour}">
<div class="container">
	<div class="row flexbox-row">
		<div class="col-lg-12 col-md-12 col-xs-12 flexbox-col"><h2>${headingText}</h2></div>
		<c:forEach items="${promotionsContent}" var="promotionContent"
			varStatus="status"> 
			<div class="${colWidthClass} flexbox-col">
				<cms:component component="${promotionContent}" evaluateRestriction="true" />
			</div>
			<c:if test="${(status.index + 1) % displayCount == 0}">
                </div>
                <div class="row">
		    </c:if>
		</c:forEach>
	</div>
</div>
</section>