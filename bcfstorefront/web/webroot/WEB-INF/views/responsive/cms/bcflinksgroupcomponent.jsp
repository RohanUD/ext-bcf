<%@ page trimDirectiveWhitespaces="true" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="cms" uri="http://hybris.com/tld/cmstags" %>

${headerText}
<br>

<c:forEach items="${links}" var="link">
   <cms:component component="${link}" evaluateRestriction="true" /><br/>
</c:forEach>

