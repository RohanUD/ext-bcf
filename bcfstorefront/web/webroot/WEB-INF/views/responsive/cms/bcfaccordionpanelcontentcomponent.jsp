<%@ page trimDirectiveWhitespaces="true" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="cms" uri="http://hybris.com/tld/cmstags" %>
<div class="container">
	<div class="row">
		<div class="col-lg-12 col-md-12 col-xs-12">
			<div class="js-accordion js-accordion-contentpage custom-contentpage-accordion" style="${fontStyle}">
				<h2 class="mt-3 mb-3 ml-4">${title}</h2>
				<c:forEach items="${accordions}" var="accordion">
					<header>${accordion.title}</header>
					<div>
						<div class="component-cnrl">${accordion.content}</div>
						<div class="component-cnrl">
							<c:if test="${not empty accordion.imageCarousel}">
								<cms:component component="${accordion.imageCarousel}" evaluateRestriction="true" />
							</c:if>
						</div>
						<div class="component-cnrl">
							<c:if test="${not empty accordion.downloadLinksCarousel}">
								<cms:component component="${accordion.downloadLinksCarousel}" evaluateRestriction="true" />
							</c:if>
							<c:if test="${not empty accordion.downloadLinkGroups}">
							    <c:forEach items="${accordion.downloadLinkGroups}" var="downloadLinkCarousel">
								    <cms:component component="${downloadLinkCarousel}" evaluateRestriction="true" />
								</c:forEach>
							</c:if>
						</div>
					</div>
				</c:forEach>
			</div>
		</div>
	</div>
</div>
