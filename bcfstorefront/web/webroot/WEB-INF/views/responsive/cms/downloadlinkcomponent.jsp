<%@ page trimDirectiveWhitespaces="true" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="cms" uri="http://hybris.com/tld/cmstags" %>
<div class="container">
    ${upperText}
	<div class="row">
	<div class="col-lg-12 col-md-12 col-xs-12">
	    <div class="media">
          <a class="pull-left" href="#">
            <img class="media-object" src="${image.url}" alt="...">
          </a>
          <div class="media-body">
          <div class="media-title media-heading">
            <a href="${media.downloadURL}"><u>${title}</u></a>
          </div>
          ${helpInfo}
          </div>
        </div>
        ${lowerText}
      </div>
	</div>
</div>


