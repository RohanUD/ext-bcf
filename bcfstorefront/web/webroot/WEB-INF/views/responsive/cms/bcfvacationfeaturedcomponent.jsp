<%@ page trimDirectiveWhitespaces="true" %>
<%@ taglib prefix="cms" uri="http://hybris.com/tld/cmstags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<spring:htmlEscape defaultHtmlEscape="false"/>
<a href="${logoLinkComponent.link}">
    <c:if test="${not empty logoLinkComponent.image}">
    
        <img src="${logoLinkComponent.image.url}"/>
            
    </c:if>
    ${logoLinkComponent.linkText}
</a>

<c:if test="${not empty image}">

    <cms:component component="${image}" evaluateRestriction="true" />
   

</c:if>
<br>
