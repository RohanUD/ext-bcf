<%@ page trimDirectiveWhitespaces="true"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="breadcrumb" tagdir="/WEB-INF/tags/responsive/nav/breadcrumb"%>
<div class="container">
	<div class="row">
		<div class="col-lg-12 col-md-12  col-sm-12 col-xs-12">
			<c:if test="${not empty breadcrumbs}">
				<div class="breadcrumb-section">
					<breadcrumb:breadcrumb breadcrumbs="${breadcrumbs}" />
				</div>
			</c:if>
		</div>
	</div>
</div>
