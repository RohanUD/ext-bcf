<%@ page trimDirectiveWhitespaces="true" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="cms" uri="http://hybris.com/tld/cmstags" %>
<div class="container">
    <div class="row mb-5">
    <div class="bcf-grid-component flexbox-row">
        <c:forEach items="${columns}" var="column">
            <div class="col-lg-${column.lgColumn} col-md-${column.lgColumn} col-sm-${column.lgColumn} col-xs-12 flexbox-col content-carousel-width">
                <c:choose>
                    <c:when test="${not empty column.component}">
                        <cms:component component="${column.component}" evaluateRestriction="true" />
                    </c:when>
                </c:choose>
            </div>
        </c:forEach>
    </div>
</div>
</div>
