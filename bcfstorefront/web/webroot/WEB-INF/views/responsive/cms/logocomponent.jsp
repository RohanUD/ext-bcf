<%@ taglib prefix="cms" uri="http://hybris.com/tld/cmstags"%>
<%@ page trimDirectiveWhitespaces="true" %>

<div class="cmslogo">
	<cms:component component="${logo}" evaluateRestriction="true" element="li" />
</div>
