<%@ page trimDirectiveWhitespaces="true" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="cms" uri="http://hybris.com/tld/cmstags" %>
<div class="container">

<div class=" my-5">
<c:if test="${not empty headingText}">
<h3 class="text-dark-blue"><strong>${headingText}</strong></h3>
</c:if>
<div class="experience-map mt-5">

<c:if test="${not empty mapItineraries}">
    <c:forEach items="${mapItineraries}" var="mapItinerary">
       <cms:component component="${mapItinerary}" evaluateRestriction="true" />
    </c:forEach>
</c:if>
</div>
</div>
</div>