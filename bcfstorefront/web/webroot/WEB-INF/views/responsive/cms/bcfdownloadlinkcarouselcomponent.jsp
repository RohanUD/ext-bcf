<%@ page trimDirectiveWhitespaces="true" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="cms" uri="http://hybris.com/tld/cmstags" %>
<div class="container">
	<div class="row">
		<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">${title}</div>
	</div>
</div>

<c:if test="${not empty downloadLinks}">
    <c:forEach items="${downloadLinks}" var="downloadLink">
       <cms:component component="${downloadLink}" evaluateRestriction="true" />
    </c:forEach>
</c:if>
