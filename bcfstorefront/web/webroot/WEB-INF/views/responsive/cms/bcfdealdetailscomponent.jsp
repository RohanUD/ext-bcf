<%@ page trimDirectiveWhitespaces="true" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="cms" uri="http://hybris.com/tld/cmstags" %>

<cms:component component="${image}" evaluateRestriction="true" />

${description}

${infoContent}

<cms:component component="${link}" evaluateRestriction="true" />
