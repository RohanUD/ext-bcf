<%@ page trimDirectiveWhitespaces="true" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="cms" uri="http://hybris.com/tld/cmstags" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<footer>
    <div class="container">
    <div class="row">
            <c:if test="${not empty socialLinks}">
                <div class="col-lg-3 col-md-3 col-sm-12 col-xs-12 pull-right footer-m">
                    ${socialLinks}
                </div>
            </c:if>
            <c:forEach items="${linksGroup}" var="linkGroup">
                <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12 footer-m">
                    <div class="footer-title">${linkGroup.headerText}</div>
                    <ul class="list-unstyled">
                        <c:forEach items="${linkGroup.links}" var="link">
                            <li><cms:component component="${link}" evaluateRestriction="true" /></li>
                        </c:forEach>
                    </ul>
                </div>
            </c:forEach>
</div>



		<div class="row">
          <div class="additional-links">
                    <div class="col-lg-8 col-md-8 col-sm-6 col-xs-12">
                        <ul class="footer-bottom-links">
                            <c:forEach items="${bottomLinks}" var="link">
                                <li><cms:component component="${link}" evaluateRestriction="true" /></li>
                            </c:forEach>
                        </ul>
                    </div>
                    <div class="col-lg-4 col-md-4 col-sm-6 col-xs-12 text-center">
                        <cms:component component="${notice}" evaluateRestriction="true" />
                    </div>
                </div>
                </div>


    </div>
</footer>
