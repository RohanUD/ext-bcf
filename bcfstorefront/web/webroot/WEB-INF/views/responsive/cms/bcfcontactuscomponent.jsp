<%@ page trimDirectiveWhitespaces="true" %>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="cms" uri="http://hybris.com/tld/cmstags" %>

<c:if test="${not empty title}">
    <h3 class="mb-4 text-dark-blue min-height-title"><b>${title}</b></h3>
</c:if>
<div class="talk-box">
    <c:if test="${not empty topText}">
        <h4 class="margin-0"><b>${topText}</b></h4>
     </c:if>
    <p class="margin-top-30 margin-bottom-30 fnt-14">${description}</p>
    <p class="margin-0 learn-link">
        <c:if test="${not empty links}">
            <c:forEach items="${links}" var="link">
                <cms:component component="${link}" evaluateRestriction="true" />
            </c:forEach>
        </c:if>
    </p>
</div>

