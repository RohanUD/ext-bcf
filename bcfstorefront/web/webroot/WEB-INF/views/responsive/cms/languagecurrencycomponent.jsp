<%@ page trimDirectiveWhitespaces="true" %>
<%@ taglib prefix="header" tagdir="/WEB-INF/tags/addons/bcfstorefrontaddon/responsive/common/header"  %>

<ul class="languagecurrencycomponent">
	<li><header:currencySelector currencies="${currencies}" currentCurrency="${currentCurrency}" /></li>
</ul>

