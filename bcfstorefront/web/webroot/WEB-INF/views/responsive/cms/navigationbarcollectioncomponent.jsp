<%@ page trimDirectiveWhitespaces="true"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="cms" uri="http://hybris.com/tld/cmstags"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>

<c:if test="${component.visible}">	
		<c:forEach items="${components}" var="component">
			<cms:component component="${component}" evaluateRestriction="true" navigationType="offcanvas" />
		</c:forEach>	
</c:if>
