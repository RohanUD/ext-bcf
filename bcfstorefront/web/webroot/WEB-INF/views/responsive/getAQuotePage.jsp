<%@ page trimDirectiveWhitespaces="true"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="cms" uri="http://hybris.com/tld/cmstags"%>
<%@ taglib prefix="ycommerce" uri="http://hybris.com/tld/ycommercetags"%>
<%@ taglib prefix="common" tagdir="/WEB-INF/tags/responsive/common"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ page language="java" contentType="text/html;charset=UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@ taglib prefix="passenger"
	tagdir="/WEB-INF/tags/addons/bcfcommonsaddon/responsive/passenger"%>
<spring:htmlEscape defaultHtmlEscape="false" />
<c:set var="formPrefix" value="" />
<div class="container">
	<div class="row mb-5">
		<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
			<h2 class="text-blue font-weight-bold">
				<spring:theme code="label.getaquote.inquiry" text="BOOKING INQUIRY" />
			</h2>
			<p>
				<spring:theme code="label.getaquote.inquiry.info"
					text="If your preferred vacation option is not bookable online please call " />
				<a href="#"> <spring:theme
						code="label.getaquote.inquiry.info.link"
						text="1-888-BC FERRY (223-3779) " />
				</a>
				<spring:theme code="label.getaquote.inquiry.info1"
					text="ext.3 or complete the form below and one of our travel experts will be in contact as soon as possible during office hours." />
				<spring:theme code="label.getaquote.selected.package"
					text="Selected package" />
			</p>
		</div>
	</div>

<hr>


	<div class="booking-inquiry-form">
		<c:url var="submitQuoteURL" value="/get-quote/save-quote" />
		<form:form commandName="getAQuoteForm" method="POST"
			action="${fn:escapeXml(submitQuoteURL)}"
			class="fe-validate form-background form-booking-trip box-3"
			id="y_getAQuoteForm">
			<div class="row mb-5">
				<div class="col-xs-6">
					<label class="sr-only"
						for="y_${fn:escapeXml( idPrefix)}DatePickerCheckIn"> <spring:theme
							var="checkInDatePlaceholderText"
							code="text.cms.getaquote.checkin.date.placeholder"
							text="Check In" />
					</label>
                    <input type="hidden" name="${fn:escapeXml(formPrefix)}productName" value="${productName}"/>
					<label><spring:theme code="label.getaquote.contact.details.checkIn.date"/></label>

					<ul class="nav nav-tabs vacation-calender activity-calender">
						<li class="tab-links tab-depart">

							<a data-toggle="tab" href="#check-in" class="nav-link activity-tab component-to-top">
								<div class="vacation-calen-box vacation-ui-depart">
									<span class="vacation-calen-date-txt">Date</span>
									<span class="vacation-calen-year-txt current-year-custom"></span>
									<i class="bcf bcf-icon-calendar bcf-2x bc-icon bc-icon--big" aria-hidden="true"></i>
								</div>
							</a>
						</li>
					</ul>
					<div class="tab-content">
						<div id="check-in" class="tab-pane input-required-wrap activity-tab-content depart-calendar">
							<div class="bc-dropdown calender-box tab-content-depart" id="js-depart">
								<label><spring:theme code="label.ferry.farefinder.routeinfo.date.format"/></label>
								<input name="${fn:escapeXml(formPrefix)}checkInDateTime" type="text" class="bc-dropdown depart form-control datepicker-input valid" value="" autocomplete="off" aria-invalid="false">
								<div id="datepicker" class="bc-dropdown--big"></div>

							</div>
						</div>
					</div>

					<form:errors path="checkInDateTime" cssClass="fe-error" />

				</div>
				<div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
					<label><spring:theme code="label.getaquote.rooms.number"
							text="Number of rooms" /></label>

			<c:set var="noOfRoomsMaxValue" value="${jalosession.tenant.config.getParameter('passenger.type.number.rooms.max.count')}"/>
			 <input type="hidden" id="noOfRoomsMax" name="noOfRoomsMax" value="${noOfRoomsMaxValue}"/>
					<div class="input-group input-btn-mp text-center mb-3 roomSelector">
                       <span class="fa bcf bcf-icon-remove btn-number" disabled="disabled" data-type="minus" data-field="numberOfRooms"></span>
                       <input id="y_roomQuantity" name="numberOfRooms" min="1" max="${noOfRoomsMaxValue}" type="text" class="input-number min-input2" value="1">
                       <span class="fa bcf bcf-icon-add btn-number" data-type="plus" data-field="numberOfRooms"></span>
					</div>


				</div>
			</div>


			<%-- Begin Guests input field section --%>
			<div class="row">
			<div class="col-lg-6 col-md-6 col-xs-6">
				<c:forEach var="roomCandidatesEntry"
					items="${getAQuoteForm.roomStayCandidates}" varStatus='idx'>
					<div id="room[${fn:escapeXml(idx.index+1)}]" class="custom-room-${idx.index+1} room-widget guest-types ${ idx.index >= numberOfRooms ? 'hidden' : ''}">
						<div class="bc-accordion select-guest-custom ">
							<label class="font-weight-light"> <spring:theme
									code="text.cms.getaquote.room" arguments="${idx.index+1}" />
							</label>
							<div class="custom-accordion">
								<h3>
									<strong><spring:theme
                                    code="label.farefinder.vacation.rooms.guest"
                                    text="Select guest" /> <span class="custom-arrow"></span></strong>
								</h3>
								<div>
                                    <div class="col-lg-12 col-md-12 col-xs-12">
                                        <div class="row">
                                            <c:set var="formPrefix"
                                                value="roomStayCandidates[${idx.index}]." />
                                            <form:input
                                                path="${fn:escapeXml(formPrefix)}roomStayCandidateRefNumber"
                                                type="hidden" />
                                            <passenger:quotepassengertypequantity formPrefix="${formPrefix}"
                                                passengerTypeQuantityList="${roomCandidatesEntry.passengerTypeQuantityList}" />
                                        </div>
                                    </div>
                                    <div class="clearfix"></div>
                                    <div class="input-required-wrap border-input">
                                        <div id="childAgeDropdownPackage"></div>
                                 </div>
								</div>
							</div>

							<div class="vacation-guest-gray hidden">
		                        <div class="row">
		                          <div class="col-lg-12 col-md-12 col-sm-12">
		                            <c:forEach var="entry" items="${roomCandidatesEntry.passengerTypeQuantityList}" varStatus="i">
		                            <p><span class="${fn:escapeXml(entry.passengerType.code)}-count">0</span> x ${fn:escapeXml(entry.passengerType.name)}</p>
		                            </c:forEach>
		                          </div>
		                        </div>
		                      </div>

						</div>
					</div>
				</c:forEach>


			</div>
</div>



			<div class="col-xs-12 y_roomStayCandidatesError"></div>
			<%-- End Guests input field section --%>
			<div class="row mb-5">
				<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
					<label for="comments" class="home-label-mobile"> <spring:theme
							code="label.getaquote.comments" text="Comments" />
					</label>
					<form:input type="text" id="comments" path="comments"
						class="form-control min-height-150" />
					<form:errors path="comments" cssClass="fe-error" />
				</div>
			</div>


			<div class="row mb-5">
			<hr>
				<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
					<h2 class="font-weight-bold">
						<spring:theme code="label.getaquote.contact.details"
							text="Contact details" />
					</h2>
				</div>
			</div>
			<div class="row mb-5">
				<div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
					<label for="firstName" class="home-label-mobile"> <spring:theme
							code="label.getaquote.contact.details.first.name"
							text="First name" />
					</label>
					<form:input type="text" class="form-control" id="firstName" path="firstName" />
					<form:errors path="firstName" cssClass="fe-error" />
				</div>
				<div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
					<label for="lastName" class="home-label-mobile"> <spring:theme
							code="label.getaquote.contact.details.last.name" text="Last name" />
					</label>
					<form:input type="text" class="form-control" id="lastName" path="lastName" />
					<form:errors path="lastName" cssClass="fe-error" />
				</div>
			</div>
			<div class="row mb-5">
				<div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
					<label for="email" class="home-label-mobile"> <spring:theme
							code="label.getaquote.contact.details.email" text="Email" />
					</label>
					<form:input type="text" class="form-control" id="email" path="email" />
					<form:errors path="email" cssClass="fe-error" />
				</div>
				<div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
					<label for="confirmEmail" class="home-label-mobile"> <spring:theme
							code="label.getaquote.contact.details.confirm.email"
							text="Confirm email" />
					</label>
					<form:input type="text" class="form-control" id="confirmEmail" path="confirmEmail" />
					<form:errors path="confirmEmail" cssClass="fe-error" />
				</div>
			</div>
			<div class="row mb-5">
				<div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
					<label for="phoneNumber" class="home-label-mobile"> <spring:theme
							code="label.getaquote.contact.details.phone.number"
							text="Phone number" />
					</label>
					<form:input type="text" class="form-control" id="phoneNumber" path="phoneNumber" />
					<form:errors path="phoneNumber" cssClass="fe-error" />
				</div>
				<div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
					<label for="postalCode" class="home-label-mobile"> <spring:theme
							code="label.getaquote.contact.details.postal.code"
							text="Postal Code" />
					</label>
					<form:input type="text" class="form-control" id="postalCode" path="postalCode" />
					<form:errors path="postalCode" cssClass="fe-error" />
				</div>
			</div>
			<div class="row">
			   			<div class="col-lg-12 col-md-12 col-xs-12">
				<label class="custom-checkbox-input show" for="y_moreInfo">
					<form:checkbox id="y_moreInfo" path="moreInfo" /> <spring:theme
						code="label.getaquote.more.info.checkbox"
						text="I would like more information on activities related to my destination(s)" />
					<span class="checkmark-checkbox"></span>
				</label> <label class="home-label-mobile"> &nbsp; </label>
			</div>
			</div>

			<div class="row justify-content-md-center">
				<div class="col-md-4">
					<button class="btn btn-primary  btn-block" type="submit"
						id="inquiryFormButton" value="Submit">
						<spring:theme code="text.getaquote.submit.inquiry"
							text="Submit inquiry" />
					</button>
				</div>
			</div>
		</form:form>
	</div>
</div>
