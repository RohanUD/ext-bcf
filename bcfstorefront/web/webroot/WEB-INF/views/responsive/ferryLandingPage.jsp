<%@ page trimDirectiveWhitespaces="true"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="cms" uri="http://hybris.com/tld/cmstags"%>
<%@ taglib prefix="ycommerce" uri="http://hybris.com/tld/ycommercetags"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ page language="java" contentType="text/html;charset=UTF-8"%>
<%@ taglib prefix="ferries" tagdir="/WEB-INF/tags/addons/bcfstorefrontaddon/responsive/ferrylisting"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@ taglib prefix="pagination" tagdir="/WEB-INF/tags/responsive/pagination"%>

<spring:htmlEscape defaultHtmlEscape="false" />
<div class="container-fluid bg-dark-gray padding-top-10 margin-bottom-30">
   <div class="container">
      <h4>
            <strong>
               <spring:theme code="label.ferry.landing.find.ferry" text="Find a ferry"/>
            </strong>
      </h4>
      <div class="row">
         <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 mb-5">
           <div class="col-lg-8 col-md-8 col-sm-12 col-xs-12 pl0 mob">
           <select class="form-control selectpicker" name="shipCodeFilter" id="shipCodeFilter">
              <option value="ALL">
                 <spring:theme code="label.ferry.details.make.selection" text="Please make a selection"/>
              </option>
              <c:forEach var="transportVehicle" items="${transportVehiclesList}">
                 <option value="${transportVehicle.name}/${transportVehicle.code}" ${param.shipCodeFilter eq transportVehicle.code ? 'selected' : '' }>
                 ${transportVehicle.name}
                 </option>
              </c:forEach>
           </select>
           </div>
           <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12 pr0 mob">
              <input type="button" class="btn btn-primary btn-block y_navigateToFerryDetails" value='<spring:theme code="text.ferry.listing.find.ferry" text="Find ferry" />' />
           </div>
         </div>
      </div>
   </div>
</div>
<div class="container  mt-3 white-bg-only py-5" id="ferry_details">
   <div class="ferry_details_placeholder">
      <div class="row">
         <ferries:ferries transportVehicles="${results}"/>
      </div>
   </div>
</div>
<pagination:globalpagination/>
</div>
