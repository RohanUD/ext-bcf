<%@ page trimDirectiveWhitespaces="true"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="cms" uri="http://hybris.com/tld/cmstags"%>
<%@ taglib prefix="ycommerce" uri="http://hybris.com/tld/ycommercetags"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib uri = "http://java.sun.com/jsp/jstl/core" prefix = "c" %>
<%@ taglib prefix="getquotes" tagdir="/WEB-INF/tags/addons/bcfstorefrontaddon/responsive/getaquotelisting"%>
<%@ page language="java" contentType="text/html;charset=UTF-8" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<spring:htmlEscape defaultHtmlEscape="false" />
<div class="container-fluid px-0 pt-3 pb-4 bg-light-gray">
   <div id="contextPath" class="hidden">${contextPath}</div>
   <div class="container-fluid px-0 mt-3 white-bg-only py-5" id="quote_details">
      <div class="col-xl-10 col-sm-10 offset-1 pt-2 pb-2">
         <div class="quote_details_placeholder">
            <div class="row">
                <getquotes:getquotes ticketsList="${ticketsList}"/>
            </div>
         </div>
      </div>
   </div>
   <div id="y_quoteListingShowMore" class="hidden"></div>
   <div class="row text-center">
      <div class="col-md-12 col-xs-12">
         <div class="y_shownResultId text-center mb-4">
            <p>
               <spring:theme code="text.get.quote.listing.shown.results" arguments="${startingNumberOfResults},${totalShownResults},${totalNumberOfResults}" />
            </p>
         </div>
      </div>
   </div>
   <div class="acco-pagination col-xl-12 text-center">
      <p>
         <a href="#" class="y_showSpecificQuotePageResults prev" data-pagenumber="${pageNum-1}">
            <span class="y_showPreviousQuotePageResults" disabled>
               <spring:theme code="text.get.quote.listing.show.previous.page" text="Previous Page" />
            </span>
         </a>
         |
         <a href="">
            <strong>
         <a href="#" class="y_showSpecificQuotePageResults next" data-pagenumber="${pageNum}">
         <span class="y_showNextQuotePageResults" >
         <spring:theme code="text.get.quote.listing.show.next.page" text="Next Page" />
         </span>
         </a></strong></a>
      </p>
   </div>
</div>
