<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="user" tagdir="/WEB-INF/tags/responsive/user"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>

<c:url value="/j_spring_security_check" var="loginActionUrl" />

<div class="login-section">
	<user:login actionNameKey="login.login" action="${loginActionUrl}" />
</div>
<div class="login-payemnt-card-box margin-top-30">
<h5 class="margin-bottom-20"><spring:theme code="label.login.page.payment.cards" text="BCF PAYMENT CARDS"/></h5>
  <ul>
    <li><a href="${contextPath}/routes-fares/ferry-fares/card-login"> <spring:theme code="label.login.page.experience.card.login" text="Experience Card™ login" /><span class="pull-right"><i class="fas fa-sign-in-alt"></i></span></a></li>
    <li><a href="${contextPath}/routes-fares/ferry-fares/card-login"> <spring:theme code="label.login.page.assured.loading.card.login" text="Assured Loading Card login" /><span class="pull-right"><i class="fas fa-sign-in-alt"></i></span></a></li>
  </ul>
</div>


