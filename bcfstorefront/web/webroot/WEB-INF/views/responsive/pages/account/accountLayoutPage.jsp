<%@ page trimDirectiveWhitespaces="true"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="template" tagdir="/WEB-INF/tags/responsive/common"%>
<%@ taglib prefix="cms" uri="http://hybris.com/tld/cmstags"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="ycommerce" uri="http://hybris.com/tld/ycommercetags"%>
<%@ taglib prefix="common" tagdir="/WEB-INF/tags/responsive/common"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<spring:url value="/my-account/update-profile" var="updateProfileUrl" />
<spring:url value="/my-account/update-password" var="updatePasswordUrl" />
<spring:url value="/my-account/update-email" var="updateEmailUrl" />
<spring:url value="/my-account/address-book" var="addressBookUrl" />
<spring:url value="/my-account/payment-details" var="paymentDetailsUrl" />
<spring:url value="/my-account/orders" var="ordersUrl" />
<spring:htmlEscape defaultHtmlEscape="false" />
<template:page pageTitle="${pageTitle}">
	<div class="container account-layout-page">
	<div class="row">
		<div class="col-xs-12 col-sm-12 col-md-12">
			<c:choose>
				<c:when test="${cmsPage.uid eq 'viewBusinessAccountDetails'}">
					<h3 class="my-5 margin-bottom-30"><spring:theme code="text.business.account.title"/></h3>
					<hr>
				</c:when>
				<c:when test="${cmsPage.uid eq 'MyBusinessAccount'}">
					<h3 class="my-5 margin-top-20 margin-bottom-30"><spring:theme code="text.business.account.title"/></h3>
					<h5 class="margin-top-20 margin-bottom-20"><a href="/my-business-account/profile"> ${b2bUnitName} </a></h5>
				</c:when>
				<c:when test="${not empty accountPageTitle}">
					<h3 class="my-5 margin-bottom-30"><spring:theme code="${accountPageTitle}"/></h3>
				</c:when>
			</c:choose>
		</div>
	</div>
	</div>

<div class="container">
    <div class="row">
		<div class="col-md-9 col-md-12 col-xs-12 padding-0">
			<div class="my-business-account-links">
				<cms:pageSlot position="BodyContent" var="feature" element="div" class="accountPageBodyContent p-0">
					<cms:component component="${feature}" />
				</cms:pageSlot>
			</div>
    	</div>
	 </div>
</div>
<div class="container">
	<div class="row">
		<div class="col-md-9 col-xs-12">
			<div class="row">
				<div class="col-xs-12 margin-bottom-30">
					<cms:pageSlot position="BottomContent" var="feature" element="div" class="accountPageBodyContent">
						<cms:component component="${feature}" />
					</cms:pageSlot>
				</div>
			</div>
		</div>
	</div>
</div>


</template:page>
<c:if test="${cmsPage.uid eq 'addPaymentCardsPage'}">
	<script type="text/javascript" src="${commonResourcePath}/js/acc.tokenizer.js"></script>
</c:if>

<div class="modal fade" id="addBusinessAccountLink" tabindex="-1" role="dialog">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true"><i class="bcf bcf-icon-cancel-solid"></i></span>
                </button>
                                    <h5 class="modal-title inline-block"><spring:theme code="text.company.add.business.account"
                                                                                       text="Add business account"/></h5>
            </div>
            <div class="modal-body">
                     <spring:theme code="text.page.myaccount.addbusinessaccountlink" htmlEscape="false" />
            </div>
        </div>
    </div>
</div>
