<%@ page trimDirectiveWhitespaces="true"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="template" tagdir="/WEB-INF/tags/responsive/common"%>
<%@ taglib prefix="cms" uri="http://hybris.com/tld/cmstags"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<template:page pageTitle="${pageTitle}">
    <div class="container login-page">

        <c:if test="${not empty param.message}">
            <div id="globalMessages">
                <div class="global-alerts">
                    <div class="alert alert-danger alert-dismissable payment-alerts">
                        <button class="close" aria-hidden="true" data-dismiss="alert" type="button">&times;</button>
                        <span class="icon-alert" aria-hidden="true"></span>&nbsp;
                        <spring:theme code="${param.message}"/>
                    </div>
                </div>
            </div>
        </c:if>

        <div class="checkout-login">
			<div class="row login-form-wrap mb-5">

                <div class="col-lg-5 col-md-5 col-sm-5 col-xs-12 mb-5">
                    <cms:pageSlot position="RightContentSlot" var="feature">
                        <cms:component component="${feature}" />
                    </cms:pageSlot>
                </div>
                <div class="col-lg-2 col-md-2 col-sm-2 col-xs-12"></div>
                <div class="col-lg-5 col-md-5 col-sm-5 col-xs-12">
                    <cms:pageSlot position="LeftContentSlot" var="feature">
                        <cms:component component="${feature}" />
                    </cms:pageSlot>
                </div>
			</div>
		</div>
	</div>
</template:page>
