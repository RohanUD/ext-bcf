<%@ page trimDirectiveWhitespaces="true"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="cms" uri="http://hybris.com/tld/cmstags"%>
<%@ taglib prefix="ycommerce" uri="http://hybris.com/tld/ycommercetags"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="cities" tagdir="/WEB-INF/tags/addons/bcfstorefrontaddon/responsive/citieslisting"%>
<%@ taglib prefix="pagination" tagdir="/WEB-INF/tags/responsive/pagination"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>

<spring:htmlEscape defaultHtmlEscape="false" />
<div class="container">
   <h2>
      <spring:theme code="text.city.listing.cities" text="CITIES"/>
   </h2>
</div>
<div class="container">
   <p>
      <strong>
         <spring:theme code="text.cms.accommodationrefinement.filterby" text="Filter by:"/>
      </strong>
   </p>
   <div class="row">
      <div class="col-lg-4 col-md-4 col-sm-4 col-xs-6">
         <form action="${requestScope['javax.servlet.forward.request_uri']}">
             <Select class="select-custom  form-control selectpicker regionselectpicker" name='regionFilterValue' onChange="this.form.submit();">
                <option value="ALL">
                   <spring:theme code="label.cities.listing.select.region" text="Select a region"/>
                </option>
                <c:forEach var="region" items="${regionsList}">
                   <option value="${region.code}" ${param.regionFilterValue eq region.code ? 'selected' : '' }>
                      ${region.name}
                   </option>
                </c:forEach>
             </Select>
             <input type="hidden" name="page" value="0"/>
         </form>
      </div>
      <div class="col-lg-8 col-md-8 col-sm-8 col-xs-6 text-right">
         <strong>
            <a href="${pageContext.request.contextPath}/regions-landing">
               <spring:theme code="text.city.listing.view.regions" text="View regions"/>
            </a>
            <i class="far bcf bcf-icon-right-arrow"></i>
         </strong>
      </div>
   </div>
   <hr>
</div>
<div class="container" id="city_details">
   <div class="city_details_placeholder">
      <div class="row">
         <cities:cities cityDataList="${results}"/>
      </div>
      <div id="y_regionListingShowMore" class="hidden"></div>
   </div>
   <pagination:globalpagination/>
</div>
