<%@ page trimDirectiveWhitespaces="true"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="cms" uri="http://hybris.com/tld/cmstags"%>
<%@ taglib prefix="ycommerce" uri="http://hybris.com/tld/ycommercetags"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ page language="java" contentType="text/html;charset=UTF-8"%>
<%@ taglib prefix="regions" tagdir="/WEB-INF/tags/addons/bcfstorefrontaddon/responsive/regionslisting"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@ taglib prefix="pagination" tagdir="/WEB-INF/tags/responsive/pagination"%>

<spring:htmlEscape defaultHtmlEscape="false" />
<div class="container">
   <h2>
      <spring:theme code="text.region.listing.regions" text="REGIONS"/>
      <p class="regions-cities">
         <a href="${pageContext.request.contextPath}/cities-landing">
            <spring:theme code="text.region.listing.view.cities" text="View cities"/>
         </a>
         <i class="far bcf bcf-icon-right-arrow"></i>
      </p>
   </h2>
   <hr>
</div>
<div class="region_details_placeholder region-landing">
  <div class="row">
     <regions:regions regionDataList="${results}"/>
  </div>
  <div id="y_regionListingShowMore" class="hidden"></div>
</div>
<pagination:globalpagination/>
<div class="container" id="region_details">

</div>
