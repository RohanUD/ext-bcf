<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ attribute name="imgSource" required="false" %>
<%@ attribute name="url" required="false" %>
<%@ attribute name="label" required="false" %>
<%@ attribute name="cssStyle" required="false" %>
<%@ attribute name="target" required="false" %>


<div class="col-lg-4 col-sm-6 col-xs-12  img-icon-width">
    <div class="m-info">
        <span class="${cssStyle}"></span>
        <a href="${url}"
           <c:if test="${not empty target}">target="${target}"</c:if> >${label}<i class="far bcf bcf-icon-right-arrow"></i></a>
    </div>
</div>
