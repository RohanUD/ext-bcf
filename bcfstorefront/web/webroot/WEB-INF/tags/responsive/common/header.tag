<%@ tag body-content="empty" trimDirectiveWhitespaces="true"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="cms" uri="http://hybris.com/tld/cmstags"%>
<%@ taglib prefix="ycommerce" uri="http://hybris.com/tld/ycommercetags"%>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags"%>
<%@ taglib prefix="nav" tagdir="/WEB-INF/tags/responsive/nav"%>
<%@ taglib prefix="booking" tagdir="/WEB-INF/tags/addons/bcfstorefrontaddon/responsive/booking"%>
<%@ attribute name="hideHeaderLinks" required="false"%>
<spring:htmlEscape defaultHtmlEscape="false" />
<cms:pageSlot position="TopHeader" var="feature" element="section">
	<c:if test="${empty hideHeaderLinks}">
		<cms:component component="${feature}" />
	</c:if>
</cms:pageSlot>
<cms:pageSlot position="TravelAdvisoryContent" var="feature" element="section"> 
	<cms:component component="${feature}" />
</cms:pageSlot>
<cms:pageSlot position="BottomHeader" var="feature" element="section">
	<cms:component component="${feature}" />
</cms:pageSlot>
<cms:pageSlot position="ASMContent" var="feature" element="section">
	<cms:component component="${feature}" />
</cms:pageSlot>

<div class="container">
	<booking:confirmFreshBookingModal />
</div>
