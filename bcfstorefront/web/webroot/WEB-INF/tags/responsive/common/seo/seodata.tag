<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<%@ attribute name="canonicalUrl" required="false" %>
<%@ attribute name="pageTitle" required="false" rtexprvalue="true" %>
<%@ attribute name="metaDescription" required="false" %>
<%@ attribute name="metaType" required="false" %>

 <link rel="canonical" href="${not empty canonicalUrl ? canonicalUrl : ''}" />
 <meta name="viewport" content="width=device-width, initial-scale=1, user-scalable=no">
 <meta name="title" content="${not empty pageTitle ? pageTitle : not empty cmsPage.title ? cmsPage.title : ''}">
 <c:if test="${cmsPage.typeCode eq 'ContentPage'}">
     <c:if test="${not empty ogGraphData}">
         <meta property="og:title" content="${ogGraphData.title}"/>
         <meta property="og:description" content="${ogGraphData.description}"/>
         <meta property="og:type" content="${ogGraphData.type}">
         <meta property="og:url" content="${ogGraphData.url}">
         <meta property="og:sitename" content="${ogGraphData.sitename}">
         <c:url var="imageUrl" value="${ogGraphData.image}"/>
         <meta property="og:image" content="${imageUrl}">
     </c:if>
 </c:if>
