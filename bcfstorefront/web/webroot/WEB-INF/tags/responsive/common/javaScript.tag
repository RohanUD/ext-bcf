<%@ tag body-content="empty" trimDirectiveWhitespaces="true" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<%@ taglib prefix="theme" tagdir="/WEB-INF/tags/shared/theme" %>
<%@ taglib prefix="cms" tagdir="/WEB-INF/tags/responsive/common/cms" %>
<%@ taglib prefix="template" tagdir="/WEB-INF/tags/responsive/common" %>

<c:url value="/" var="siteRootUrl"/>

<template:javaScriptVariables/>

<c:choose>
	<c:when test="${minifyEnabled}">
      <script type="text/javascript" src="${commonResourcePath}/../js/jsLibrary.min.js"></script>
      <script type="text/javascript" src="${commonResourcePath}/../js/common.min.js"></script>
	</c:when>
	<c:otherwise>

    <script type="text/javascript" src="${commonResourcePath}/js/js-custom/tether.min.js"></script>
    <script type="text/javascript" src="${commonResourcePath}/js/js-custom/geoxml3.js"></script>
    <script type="text/javascript" src="${commonResourcePath}/js/js-custom/jquery-1.11.3.min.js"></script>
    <script type="text/javascript" src="${commonResourcePath}/js/js-custom/jquery-ui.js"></script>
    <script type="text/javascript" src="${commonResourcePath}/js/js-custom/lodash.min.js"></script>
    <script type="text/javascript" src="${commonResourcePath}/js/js-custom/popper.min.js"></script>
    <script type="text/javascript" src="${commonResourcePath}/js/js-custom/bootstrap.min.js"></script>
    <script type="text/javascript" src="${commonResourcePath}/js/js-custom/owl.carousel.min.js"></script>
	<script type="text/javascript" src="${commonResourcePath}/js/js-custom/datepicker.js"></script>
	<script type="text/javascript" src="${commonResourcePath}/js/js-custom/bootstrap.bundle.min.js"></script>
	<script type="text/javascript" src="${commonResourcePath}/js/js-custom/bootstrap-select.js"></script>
	<script type="text/javascript" src="${commonResourcePath}/js/js-custom/js-custom.js"></script>

    <%-- plugins --%>
    <script type="text/javascript" src="${commonResourcePath}/js/enquire.min.js"></script>
    <script type="text/javascript" src="${commonResourcePath}/js/Imager.min.js"></script>
    <script type="text/javascript" src="${commonResourcePath}/js/jquery.blockUI-2.66.js"></script>
    <script type="text/javascript" src="${commonResourcePath}/js/jquery.colorbox-min.js"></script>
    <script type="text/javascript" src="${commonResourcePath}/js/jquery.form.min.js"></script>
    <script type="text/javascript" src="${commonResourcePath}/js/jquery.hoverIntent.js"></script>
    <script type="text/javascript" src="${commonResourcePath}/js/jquery.pstrength.custom-1.2.0.js"></script>
    <script type="text/javascript" src="${commonResourcePath}/js/jquery.syncheight.custom.js"></script>
    <script type="text/javascript" src="${commonResourcePath}/js/jquery.tabs.custom.js"></script>
    <script type="text/javascript" src="${commonResourcePath}/js/ui-widgets.js"></script>
    <script type="text/javascript" src="${commonResourcePath}/js/jquery.zoom.custom.js"></script>
    <script type="text/javascript" src="${commonResourcePath}/js/jquery.tmpl-1.0.0pre.min.js"></script>
    <script type="text/javascript" src="${commonResourcePath}/js/jquery.currencies.min.js"></script>
    <script type="text/javascript" src="${commonResourcePath}/js/jquery.waitforimages.min.js"></script>
    <script type="text/javascript" src="${commonResourcePath}/js/jquery.slideviewer.custom.1.2.js"></script>
    <script type="text/javascript" src="${commonResourcePath}/js/datatables.min.js"/>

    <%-- Custom ACC JS --%>

    <script type="text/javascript" src="${commonResourcePath}/js/acc.address.js"></script>
    <script type="text/javascript" src="${commonResourcePath}/js/acc.autocomplete.js"></script>
    <script type="text/javascript" src="${commonResourcePath}/js/acc.carousel.js"></script>
    <script type="text/javascript" src="${commonResourcePath}/js/acc.cart.js"></script>
    <script type="text/javascript" src="${commonResourcePath}/js/acc.checkout.js"></script>
    <script type="text/javascript" src="${commonResourcePath}/js/acc.checkoutaddress.js"></script>
    <script type="text/javascript" src="${commonResourcePath}/js/acc.checkoutsteps.js"></script>
    <script type="text/javascript" src="${commonResourcePath}/js/acc.cms.js"></script>
    <script type="text/javascript" src="${commonResourcePath}/js/acc.colorbox.js"></script>
    <script type="text/javascript" src="${commonResourcePath}/js/acc.common.js"></script>
    <script type="text/javascript" src="${commonResourcePath}/js/acc.forgottenpassword.js"></script>
    <script type="text/javascript" src="${commonResourcePath}/js/acc.global.js"></script>
    <script type="text/javascript" src="${commonResourcePath}/js/acc.hopdebug.js"></script>
    <script type="text/javascript" src="${commonResourcePath}/js/acc.imagegallery.js"></script>
    <script type="text/javascript" src="${commonResourcePath}/js/acc.langcurrencyselector.js"></script>
    <script type="text/javascript" src="${commonResourcePath}/js/acc.minicart.js"></script>
    <script type="text/javascript" src="${commonResourcePath}/js/acc.navigation.js"></script>
    <script type="text/javascript" src="${commonResourcePath}/js/acc.order.js"></script>
    <script type="text/javascript" src="${commonResourcePath}/js/acc.paginationsort.js"></script>
    <script type="text/javascript" src="${commonResourcePath}/js/acc.payment.js"></script>
    <script type="text/javascript" src="${commonResourcePath}/js/acc.paymentDetails.js"></script>
    <script type="text/javascript" src="${commonResourcePath}/js/acc.pickupinstore.js"></script>
    <script type="text/javascript" src="${commonResourcePath}/js/acc.product.js"></script>
    <script type="text/javascript" src="${commonResourcePath}/js/acc.productDetail.js"></script>
    <script type="text/javascript" src="${commonResourcePath}/js/acc.quickview.js"></script>
    <script type="text/javascript" src="${commonResourcePath}/js/acc.ratingstars.js"></script>
    <script type="text/javascript" src="${commonResourcePath}/js/acc.refinements.js"></script>
    <script type="text/javascript" src="${commonResourcePath}/js/acc.silentorderpost.js"></script>
    <script type="text/javascript" src="${commonResourcePath}/js/acc.tabs.js"></script>
    <script type="text/javascript" src="${commonResourcePath}/js/acc.termsandconditions.js"></script>
    <script type="text/javascript" src="${commonResourcePath}/js/acc.track.js"></script>
    <script type="text/javascript" src="${commonResourcePath}/js/acc.storefinder.js"></script>
    <script type="text/javascript" src="${commonResourcePath}/js/acc.futurelink.js"></script>
    <script type="text/javascript" src="${commonResourcePath}/js/acc.productorderform.js"></script>
    <script type="text/javascript" src="${commonResourcePath}/js/acc.savedcarts.js"></script>
    <script type="text/javascript" src="${commonResourcePath}/js/acc.multidgrid.js"></script>
    <script type="text/javascript" src="${commonResourcePath}/js/acc.quickorder.js"></script>
    <script type="text/javascript" src="${commonResourcePath}/js/acc.quote.js"></script>
    <script type="text/javascript" src="${commonResourcePath}/js/acc.terminalListing.js"></script>
    <script type="text/javascript" src="${commonResourcePath}/js/acc.travelBookingListing.js"></script>
    <script type="text/javascript" src="${commonResourcePath}/js/bcfMultimediaTextComponent.js"></script>

    <script type="text/javascript" src="${commonResourcePath}/js/acc.ferryDetails.js"></script>
    <script type="text/javascript" src="${commonResourcePath}/js/acc.getAQuote.js"></script>
    <script type="text/javascript" src="${commonResourcePath}/js/acc.hoteldetail.js"></script>
    <script type="text/javascript" src="${commonResourcePath}/js/acc.shipinfo.js"></script>
    <script type="text/javascript" src="${commonResourcePath}/js/acc.csv-import.js"></script>
    <script type="text/javascript" src="${commonResourcePath}/js/_autoload.js"></script>


    <script type="text/javascript" src="/_ui/addons/smarteditaddon/shared/common/js/webApplicationInjector.js"></script>
    <script type="text/javascript" src="/_ui/addons/smarteditaddon/shared/common/js/reprocessPage.js"></script>
    <script type="text/javascript" src="/_ui/addons/smarteditaddon/shared/common/js/adjustComponentRenderingToSE.js"></script>
    <script type="text/javascript" src="/_ui/addons/smarteditaddon/responsive/common/js/smarteditaddon.js"></script>
    <script type="text/javascript" src="/_ui/addons/bcfassistedservicesstorefront/responsive/common/js/bcfassistedservicesstorefront.js"></script>
    <script type="text/javascript" src="/_ui/addons/bcfassistedservicesstorefront/responsive/common/js/jquery.tablesorter.min.js"></script>
    <script type="text/javascript" src="/_ui/addons/bcfassistedservicesstorefront/responsive/common/js/Chart.min.js"></script>
    <script type="text/javascript" src="/_ui/addons/bcfassistedservicesstorefront/responsive/common/js/asm.storefinder.js"></script>
    <script type="text/javascript" src="/_ui/addons/bcfstorefrontaddon/responsive/common/js/jquery.validate.min.js"></script>
    <script type="text/javascript" src="/_ui/addons/bcfstorefrontaddon/responsive/common/js/jquery.ddslick.min.js"></script>
    <script type="text/javascript" src="/_ui/addons/bcfstorefrontaddon/responsive/common/js/jquery.autosuggest.js"></script>
    <script type="text/javascript" src="/_ui/addons/bcfstorefrontaddon/responsive/common/js/jquery.seat-charts.js"></script>
    <script type="text/javascript" src="/_ui/addons/bcfstorefrontaddon/responsive/common/js/jquery.cookie.js"></script>
    <script type="text/javascript" src="/_ui/addons/bcfstorefrontaddon/responsive/common/js/affix-custom.js"></script>
    <script type="text/javascript" src="/_ui/addons/bcfstorefrontaddon/responsive/common/js/acc.appmodel.js"></script>
    <script type="text/javascript" src="/_ui/addons/bcfstorefrontaddon/responsive/common/js/acc.services.js"></script>
    <script type="text/javascript" src="/_ui/addons/bcfstorefrontaddon/responsive/common/js/acc.basketsummary.js"></script>
    <script type="text/javascript" src="/_ui/addons/bcfstorefrontaddon/responsive/common/js/acc.pendingorders.js"></script>
    <script type="text/javascript" src="/_ui/addons/bcfstorefrontaddon/responsive/common/js/acc.travelcommon.js"></script>
    <script type="text/javascript" src="/_ui/addons/bcfstorefrontaddon/responsive/common/js/acc.formvalidation.js"></script>
    <script type="text/javascript" src="/_ui/addons/bcfstorefrontaddon/responsive/common/js/acc.myaccount.js"></script>
    <script type="text/javascript" src="/_ui/addons/bcfstorefrontaddon/responsive/common/js/acc.asmorderdetails.js"></script>
    <script type="text/javascript" src="/_ui/addons/bcfstorefrontaddon/responsive/common/js/acc.navigation.js"></script>
    <script type="text/javascript" src="/_ui/addons/bcfstorefrontaddon/responsive/common/js/acc.payment.js"></script>
    <script type="text/javascript" src="/_ui/addons/bcfstorefrontaddon/responsive/common/js/acc.registration.js"></script>
    <script type="text/javascript" src="/_ui/addons/bcfstorefrontaddon/responsive/common/js/acc.reservation.js"></script>
     <script type="text/javascript" src="/_ui/addons/bcfstorefrontaddon/responsive/common/js/acc.addressValidation.js"></script>
    <script type="text/javascript" src="/_ui/addons/bcfstorefrontaddon/responsive/common/js/acc.bookingdetails.js"></script>
    <script type="text/javascript" src="/_ui/addons/bcfstorefrontaddon/responsive/common/js/_autoloadtracc.js"></script>
    <script type="text/javascript" src="/_ui/addons/bcfvoyageaddon/responsive/common/js/acc.validationMessages.js"></script>
    <script type="text/javascript" src="/_ui/addons/bcfvoyageaddon/responsive/common/js/acc.farefinder.js"></script>
    <script type="text/javascript" src="/_ui/addons/bcfvoyageaddon/responsive/common/js/acc.custom.validation.js"></script>
    <script type="text/javascript" src="/_ui/addons/bcfvoyageaddon/responsive/common/js/acc.currentconditions.js"></script>
    <script type="text/javascript" src="/_ui/addons/bcfvoyageaddon/responsive/common/js/acc.dealfinder.js"></script>
    <script type="text/javascript" src="/_ui/addons/bcfvoyageaddon/responsive/common/js/acc.schedulefinder.js"></script>
    <script type="text/javascript" src="/_ui/addons/bcfvoyageaddon/responsive/common/js/acc.activityfinder.js"></script>
    <script type="text/javascript" src="/_ui/addons/bcfvoyageaddon/responsive/common/js/acc.packagefinder.js"></script>
    <script type="text/javascript" src="/_ui/addons/bcfvoyageaddon/responsive/common/js/acc.transportofferingstatus.js"></script>
    <script type="text/javascript" src="/_ui/addons/bcfvoyageaddon/responsive/common/js/acc.fareselection.js"></script>
    <script type="text/javascript" src="/_ui/addons/bcfvoyageaddon/responsive/common/js/acc.tripfinder.js"></script>
    <script type="text/javascript" src="/_ui/addons/bcfvoyageaddon/responsive/common/js/acc.ancillary.js"></script>
    <script type="text/javascript" src="/_ui/addons/bcfvoyageaddon/responsive/common/js/acc.travellerdetails.js"></script>
    <script type="text/javascript" src="/_ui/addons/bcfvoyageaddon/responsive/common/js/acc.managebookings.js"></script>
    <script type="text/javascript" src="/_ui/addons/bcfvoyageaddon/responsive/common/js/acc.checkin.js"></script>
    <script type="text/javascript" src="/_ui/addons/bcfvoyageaddon/responsive/common/js/acc.advancePassengerInfo.js"></script>
    <script type="text/javascript" src="/_ui/addons/bcfcheckoutaddon/responsive/common/js/acc.silentorderpost.js"></script>
    <script type="text/javascript" src="/_ui/addons/bcfcheckoutaddon/responsive/common/js/acc.paymentType.js"></script>

    <!--BCF Accommodations addon related js -->
    <script type="text/javascript" src="/_ui/addons/bcfaccommodationsaddon/responsive/common/js/acc.accommodationfinder.js"></script>
    <script type="text/javascript" src="/_ui/addons/bcfaccommodationsaddon/responsive/common/js/acc.hotelgallery.js"></script>
    <script type="text/javascript" src="/_ui/addons/bcfaccommodationsaddon/responsive/common/js/acc.accommodationselection.js"></script>
    <script type="text/javascript" src="/_ui/addons/bcfaccommodationsaddon/responsive/common/js/acc.accommodationdetails.js"></script>
    <script type="text/javascript" src="/_ui/addons/bcfaccommodationsaddon/responsive/common/js/acc.guestdetails.js"></script>
    <script type="text/javascript" src="/_ui/addons/bcfaccommodationsaddon/responsive/common/js/acc.accommodationmanagebookings.js"></script>

    <!--BCF Commons addon related js -->
    <script type="text/javascript" src="/_ui/addons/bcfcommonsaddon/responsive/common/js/acc.travelfinder.js"></script>
    <script type="text/javascript" src="/_ui/addons/bcfcommonsaddon/responsive/common/js/acc.dealselection.js"></script>
    <script type="text/javascript" src="/_ui/addons/bcfcommonsaddon/responsive/common/js/acc.packageListing.js"></script>
    <script type="text/javascript" src="/_ui/addons/bcfcommonsaddon/responsive/common/js/acc.packagedetails.js"></script>
    <script type="text/javascript" src="/_ui/addons/bcfcommonsaddon/responsive/common/js/acc.personaldetails.js"></script>
    <script type="text/javascript" src="/_ui/addons/bcfcommonsaddon/responsive/common/js/acc.ancillaryextras.js"></script>
    <script type="text/javascript" src="/_ui/addons/bcfcommonsaddon/responsive/common/js/acc.activityListing.js"></script>
    <script type="text/javascript" src="/_ui/addons/bcfcommonsaddon/responsive/common/js/acc.activitydetails.js"></script>
    <script type="text/javascript" src="/_ui/addons/bcfcommonsaddon/responsive/common/js/acc.optionbooking.js"></script>
    <script type="text/javascript" src="/_ui/addons/bcfcommonsaddon/responsive/common/js/acc.schedules.js"></script>
    <script type="text/javascript" src="/_ui/addons/bcfcommonsaddon/responsive/common/js/acc.endofdayreport.js"></script>
    <script type="text/javascript" src="/_ui/addons/bcfcommonsaddon/responsive/common/js/acc.inventoryreports.js"></script>

	</c:otherwise>
</c:choose>
<script type="text/javascript">

$( function() {
    $( "#tabs" ).tabs();
  } );

$(document).ready(function() {

	$('.js-main-box .add-on-box:nth-child(3)').addClass("accordion-arriv");
	$(function() {
		$("#accordion-review").accordion();
		$("#accordion-review").accordion({
			header : "h4",
			collapsible : true,
			active : false
		});
	});
	$(function() {
		$(".accordion-arriv #accordion-review").accordion();
		$(".accordion-arriv #accordion-review").accordion({
			header : "h4",
			collapsible : true,
			active : false
		});
	});

});

<!--initialize pending(on-request) orders datatable -->
$(document).ready(function() {
    $('#pendingOrdersContentList').DataTable({
     "order": [[ 2, "desc" ]],
     "columnDefs": [
     {
         "targets": [ 6 ],
         "searchable": false,
         "orderable": false
     },
      {
          "targets": [ 7 ],
          "searchable": false,
          "orderable": false
      }
      ],
           "lengthMenu": [[5,10, 25, 50, -1], [5,10, 25, 50, "All"]]
       });
} );


$(document).ready(function() {
			if ($(window).width() >= 980) {
						$("#js_navbar_menu .dropdown-toggle").click(function() {
							if ($(".orbeon").length > 0) {
								$(this).parent().toggleClass("open");
							}
						});
					}
			
			$(window).resize(function() {
				if ($(window).width() >= 980) {
					$("#js_navbar_menu .dropdown-toggle").click(function() {
						if ($(".orbeon").length > 0) {
							$(this).parent().toggleClass("open");
						}
						});
					}

					});

			// document ready
		});


	</script>
<cms:previewJS cmsPageRequestContextData="${cmsPageRequestContextData}" />
