<%@ tag body-content="scriptless" trimDirectiveWhitespaces="true"%>
<%@ attribute name="pageTitle" required="false" rtexprvalue="true"%>
<%@ attribute name="pageCss" required="false" fragment="true"%>
<%@ attribute name="pageScripts" required="false" fragment="true"%>
<%@ taglib prefix="template" tagdir="/WEB-INF/tags/responsive/common"%>
<%@ taglib prefix="header" tagdir="/WEB-INF/tags/responsive/common"%>
<%@ taglib prefix="footer" tagdir="/WEB-INF/tags/responsive/common"%>
<%@ taglib prefix="session" tagdir="/WEB-INF/tags/addons/bcfstorefrontaddon/responsive/common/session"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ attribute name="hideHeaderLinks" required="false"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="common" tagdir="/WEB-INF/tags/responsive/common"%>
<%@ taglib prefix="cart" tagdir="/WEB-INF/tags/addons/bcfstorefrontaddon/responsive/common/cart"%>
<template:master pageTitle="${pageTitle}">
	<jsp:attribute name="pageCss">
		<jsp:invoke fragment="pageCss" />
	</jsp:attribute>
	<jsp:attribute name="pageScripts">
		<jsp:invoke fragment="pageScripts" />
	</jsp:attribute>
	<jsp:body>
		<main class="page-margin" data-currency-iso-code="${fn:escapeXml(currentCurrency.isocode)}">
			<header:header hideHeaderLinks="${hideHeaderLinks}"/>
		        <session:sessionExpirationPreWarningModal />
		        <cart:cartTimerModal />
				<section class="margin-top--I">
		        <div class="container">
		           <div class="row">
		             <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
		               <div id="globalMessages">
							<common:globalMessages />
						</div>
		             </div>
		           </div>
		        </div>
				</section>
				<jsp:doBody />
				<c:if test="${not empty cartId}">
				<c:choose>
					<c:when
					test="${isHomePage eq true or isLoginPage}">
					<!-- Show nothing! -->
				</c:when>
				<c:otherwise>
				<c:if test="${displayOrderId eq true}">
				<div class="cartid-display">
				<spring:theme code="page.cart.id" text="Cart ID: " /> ${cartId}
				</div>
				</c:if>
				</c:otherwise>
				</c:choose>
				</c:if>
			<footer:footer />
		</main>
	</jsp:body>
</template:master>
