<%@ taglib prefix="cms" uri="http://hybris.com/tld/cmstags"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>

<cms:pageSlot position="Footer" var="feature" element="div">
   <div>
      <cms:component component="${feature}" />
   </div>
</cms:pageSlot>