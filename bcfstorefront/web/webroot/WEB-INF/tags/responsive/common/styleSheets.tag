<%@ tag body-content="empty" trimDirectiveWhitespaces="true" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<%@ taglib prefix="cms" tagdir="/WEB-INF/tags/responsive/common/cms" %>

<link rel="stylesheet" type="text/css" media="all" href="${themeResourcePath}/css/animate.css" />
<link rel="stylesheet" type="text/css" media="all" href="${themeResourcePath}/css/bootstrap.min.css"/>
<link rel="stylesheet" type="text/css" media="all" href="${themeResourcePath}/css/bootstrap-theme.css"/>
<link rel="stylesheet" type="text/css" media="all" href="${themeResourcePath}/css/custom-components-ui.css" />
<link rel="stylesheet" type="text/css" media="all" href="${themeResourcePath}/css/owl.carousel.min.css" />
<link rel="stylesheet" type="text/css" media="all" href="${themeResourcePath}/css/owl.theme.default.min.css" />
<link rel="stylesheet" type="text/css" media="all" href="${themeResourcePath}/css/fontawesome-all.css" />
<link rel="stylesheet" type="text/css" media="all" href="${themeResourcePath}/css/flaticon.css" />
<link rel="stylesheet" type="text/css" media="all" href="${themeResourcePath}/css/bootstrap-select.css" />
<link rel="stylesheet" type="text/css" media="all" href="${themeResourcePath}/css/datatables.min.css" />

<c:choose>
	<c:when test="${minifyEnabled}">
		<link rel="stylesheet" type="text/css" media="all" href="${themeResourcePath}/css/stylesheet.min.css" />
	</c:when>
	<c:otherwise>
	  <link rel="stylesheet" type="text/css" media="all" href="${themeResourcePath}/css/style.css" />
	  <link rel="stylesheet" type="text/css" media="all" href="${themeResourcePath}/css/media_query.css" />
      <%-- Theme CSS files --%>
	</c:otherwise>
</c:choose>

<link href="https://fonts.googleapis.com/css?family=Open+Sans:300,300i,400,400i,600,600i,700,700i,800,800i" rel="stylesheet">
<link rel="icon" href="${themeResourcePath}/images/favicon.ico" type="image/ico" sizes="16x16">

<%-- AddOn Common CSS files --%>
<c:forEach items="${addOnCommonCssPaths}" var="addOnCommonCss">
  <link rel="stylesheet" type="text/css" media="all" href="${addOnCommonCss}" />
</c:forEach>

<%--  AddOn Common CSS files --%>
<c:forEach items="${addOnCommonCssPaths}" var="addOnCommonCss">
    <link rel="stylesheet" type="text/css" media="all" href="${addOnCommonCss}"/>
</c:forEach>


<%--  AddOn Theme CSS files --%>
<c:forEach items="${addOnThemeCssPaths}" var="addOnThemeCss">
	<link rel="stylesheet" type="text/css" media="all" href="${addOnThemeCss}"/>
</c:forEach>

<%-- <link rel="stylesheet" href="${commonResourcePath}/blueprint/print.css" type="text/css" media="print" />
<style type="text/css" media="print">
	@IMPORT url("${commonResourcePath}/blueprint/print.css");
</style>
 --%>

<cms:previewCSS cmsPageRequestContextData="${cmsPageRequestContextData}" />
