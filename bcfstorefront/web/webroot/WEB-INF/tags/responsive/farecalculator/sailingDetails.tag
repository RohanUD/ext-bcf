<%@ tag body-content="empty" trimDirectiveWhitespaces="true"%>
<%@ attribute name="fareCalculatorForm" required="true"
	type="com.bcf.bcfvoyageaddon.forms.cms.FareCalculatorForm"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="format"
	tagdir="/WEB-INF/tags/addons/bcfstorefrontaddon/responsive/format"%>
<%@ taglib prefix="formElement"
	tagdir="/WEB-INF/tags/responsive/formElement"%>
<%@ taglib prefix="theme" tagdir="/WEB-INF/tags/shared/theme"%>
<%@ taglib prefix="ycommerce" uri="http://hybris.com/tld/ycommercetags"%>
<%@ taglib prefix="fareselection" tagdir="/WEB-INF/tags/addons/bcfvoyageaddon/responsive/fareselection"%>
<div class="container">
	<div class="js-accordion js-accordion-default payment-accordion">
		<h4 class="booking-detail-title">
			<spring:theme code="text.ferry.calculate.departure" />
			&nbsp;<fmt:parseDate value="${bcfFareFinderForm.routeInfoForm.departingDateTime}" var="parsedDepartDate" pattern="MM/dd/yyyy" />
            <fmt:formatDate value="${parsedDepartDate}" var="formattedParsedDepartDate" pattern="E, MMM dd" />${formattedParsedDepartDate}
		</h4>
		<div>
			<div class="pb-3">
				<div class="row">
					<div class="col-md-offset-1 col-md-10">
						<div class="row">
							<div class="col-lg-4 col-xs-4 padding-0-mobile pr-0 text-center">
								<p class="fnt-14 m-0">
								<fmt:formatDate value="${fareCalculatorForm.itineraryPricingInfo.bundleTemplates[0].transportOfferings[0].departureTime}" pattern="h:mm a"/>
									
								</p>
                                 <fareselection:departArrivalLocationFormat location="${bcfFareFinderForm.routeInfoForm.departureLocationName}"/>
								<p class=" fnt-14 m-0"></p>
							</div>
							<div class="col-lg-4 col-xs-4 text-center">
								<h5 class="mt-0 mb-0 fnt-14 medium-grey-text font-weight-normal">
									${fareCalculatorForm.departureDurationHour}
									<spring:theme code="text.ferry.calculate.duration.hour" />
									&nbsp;${fareCalculatorForm.departureDurationMinute}
									<spring:theme code="text.ferry.calculate.duration.minute" />
								</h5>
								<span class="bcf bcf-icon-single-arrows-right return-tripType"></span>

							</div>
							<div class="col-lg-4 col-xs-4 padding-0-mobile pl-0 text-center">

								<p class="fnt-14 m-0">
									<fmt:formatDate 
										value="${fareCalculatorForm.itineraryPricingInfo.bundleTemplates[0].transportOfferings[0].arrivalTime}" pattern="h:mm a"/>
								</p>
                                <fareselection:departArrivalLocationFormat location="${bcfFareFinderForm.routeInfoForm.arrivalLocationName}"/>
								<p class="m-0 fnt-14"></p>
							</div>
						</div>


					</div>
				</div>
			</div>
			<div class="row margin-top-10 margin-bottom-10">
				<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
					<p class="text-center mb-0 payment-fa">
						<span> <c:forEach var="passenger"
								items="${bcfFareFinderForm.passengerInfoForm.passengerTypeQuantityList}"
								varStatus="i">
								<c:if test="${passenger.quantity > 0 }">
									<span class="fa fa-user padding-0 grey-txt-i"></span>
									<span class="caps-f padding-0 font-weight-bold">${passenger.quantity}</span>
								</c:if>
							</c:forEach> <c:set var="ctrWHP" value="0" /> <c:set var="ancillaryIcon" /> <c:forEach
								var="accessibility"
								items="${bcfFareFinderForm.passengerInfoForm.accessibilityRequestDataList}"
								varStatus="i">
								<c:forEach var="specialService"
									items="${accessibility.specialServiceRequestDataList}"
									varStatus="i">
									<c:if test="${specialService.selection}">
									</c:if>
								</c:forEach>
								<c:forEach var="ancillary"
									items="${accessibility.ancillaryRequestDataList}" varStatus="i">
									<c:if test="${ancillary.code eq accessibility.selection }">
										<c:set var="ctrWHP" value="${ctrWHP + 1}" />
										<c:set var="ancillaryIcon" value="${accessibility.selection}" />
									</c:if>
								</c:forEach>
							</c:forEach> <c:if test="${ctrWHP > 0}">
								<span><span class="${ancillaryIcon}"></span>${ctrWHP}</span>
							</c:if>
						</span> <span> <c:if
								test="${bcfFareFinderForm.passengerInfoForm.travellingAsWalkOn eq false}">
								<c:set var="vehicleInfo"
									value="${bcfFareFinderForm.vehicleInfoForm.vehicleInfo[0]}" />
								<c:set var="vehicleIcon" value="${vehicleInfo.vehicleType.code}" />
								<c:set var="vehicleCount" value="1" />
								<span><span class="${vehicleIcon} grey-bg grey-txt-i"></span><span class=" padding-0 font-weight-bold">${vehicleCount}</span></span>
							</c:if>
						</span>
					</p>
				</div>
			</div>
			<div class="row">
				<div class="col-md-12 margin-bottom-17">
					<p class="text-center my-3 fnt-14">
						<a class="font-italic" href="/ship-info/${fareCalculatorForm.shipName}">${fareCalculatorForm.shipName}</a>
					</p>
				</div>
				<div class="col-md-10 col-md-offset-1">
					<div class="">
						<h5 class="my-3">
							<strong><spring:theme
									code="text.ferry.calculator.summary.vehicles.and.passengers"
									text="Vehicles & passengers" /></strong>
						</h5>

						<div class="row">
							<ul class="list-unstyled payment-vp">

								<c:forEach var="vehicleFareBreakdownData"
									items="${fareCalculatorForm.itineraryPricingInfo.vehicleFareBreakdownDatas}"
									varStatus="ptcIdx">

									<c:if
										test="${vehicleFareBreakdownData.vehicleTypeQuantity.qty ne 0}">
										<li>
											<div class="list-unstyled col-xs-9">${vehicleFareBreakdownData.vehicleTypeQuantity.qty}&nbsp;x&nbsp;
											${vehicleFareBreakdownData.vehicleTypeQuantity.vehicleType.name}&nbsp;
                                            ${vehicleFareBreakdownData.vehicleTypeQuantity.vehicleType.description}</div>
											<div class="list-unstyled col-xs-3 text-right">
												<c:forEach var="fareDetail"
													items="${vehicleFareBreakdownData.fareInfos.fareDetails}"
													varStatus="fareDetailIdx">
													<p>
														<format:price priceData="${fareDetail.fareProduct.price}" />
													</p>
												</c:forEach>
											</div>
										</li>
									</c:if>

								</c:forEach>



								<c:forEach var="ptcBreakDownData"
									items="${fareCalculatorForm.itineraryPricingInfo.ptcFareBreakdownDatas}"
									varStatus="ptcIdx">

									<c:if
										test="${ptcBreakDownData.passengerTypeQuantity.quantity ne 0}">
										<li>
											<div class="list-unstyled col-xs-9">${ptcBreakDownData.passengerTypeQuantity.quantity}&nbsp;x&nbsp;${ptcBreakDownData.passengerTypeQuantity.passengerType.name}</div>
											<div class="list-unstyled col-xs-3 text-right">
												<c:forEach var="fareInfo"
													items="${ptcBreakDownData.fareInfos}"
													varStatus="fareInfoIdx">
													<c:forEach var="fareDetail" items="${fareInfo.fareDetails}"
														varStatus="fareDetailIdx">
														<p>
															<format:price priceData="${fareDetail.fareProduct.price}" />
														</p>
													</c:forEach>
												</c:forEach>
											</div>
										</li>
									</c:if>

								</c:forEach>

								<c:forEach var="otherChargesData"
                                    items="${fareCalculatorForm.itineraryPricingInfo.otherCharges}"
                                    varStatus="ptcIdx">
                                    <c:if
                                        test="${otherChargesData.quantity ne 0}">
                                        <li>
                                            <div class="list-unstyled col-xs-9"><spring:theme code="text.listsailing.price.itinerary.other.charges" text="Other charges" /></div>
                                            <div class="list-unstyled col-xs-3 text-right">
                                            <p><format:price priceData="${otherChargesData.price}" /></p>
                                            </div>
                                        </li>
                                    </c:if>
                                </c:forEach>

							</ul>
						</div>

						<div class="departure-bx payment-total">
							<h5 class="font-weight-normal">
								<spring:theme
									code="text.ferry.calculator.summary.departure.sailing.total"
									text="Departure sailing total" />
								<span><strong><format:price
											priceData="${fareCalculatorForm.itineraryPricingInfo.totalFare.totalPrice}" /></strong></span>
							</h5>
						</div>
					</div>

				</div>
			</div>

		</div>

	</div>
</div>

<c:set var="journeyTotal" value="${fareCalculatorForm.itineraryPricingInfo.totalFare.totalPrice.value}" scope="request" />
