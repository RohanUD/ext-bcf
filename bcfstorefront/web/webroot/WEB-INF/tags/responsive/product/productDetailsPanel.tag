<%@ tag body-content="empty" trimDirectiveWhitespaces="true"%>
<%@ taglib prefix="product" tagdir="/WEB-INF/tags/responsive/product"%>
<%@ taglib prefix="cms" uri="http://hybris.com/tld/cmstags"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="ycommerce" uri="http://hybris.com/tld/ycommercetags"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
  <script src="https://code.jquery.com/jquery-1.12.4.js"></script>
  <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
<script type='text/javascript'
	src='//platform-api.sharethis.com/js/sharethis.js#property=5b112e239e5e2f00115f16d0&product=custom-share-buttons'
	async='async'>
	</script>
	 <script>
	$( function() {
    $( "#activityDate" ).datepicker({
    	 dateFormat: 'd M yy'
    });
  } );
	</script>
<div class="product-details page-title">
	<ycommerce:testId
		code="productDetails_productNamePrice_label_${product.code}">
		<div class="name">${fn:escapeXml(product.name)}<span class="sku"><spring:theme
					code="product.activity.id" /></span><span class="code">${fn:escapeXml(product.code)}</span>
		</div>
	</ycommerce:testId>
</div><input id="productCode" hidden="hidden" value="${product.code}"/>
<div class="row">
	<div class="col-xs-10 col-xs-push-1 col-sm-6 col-sm-push-0 col-lg-6">
		<product:productImagePanel galleryImages="${galleryImages}" />
	</div>
	<div class="clearfix hidden-sm hidden-md hidden-lg"></div>
	<div class="col-sm-6 col-lg-6">
		<div class="">
			<div class="row">
				<div class="col-lg-6">
					<div class="product-details">
						<product:productPromotionSection product="${product}" />
						<ycommerce:testId
							code="productDetails_productNamePrice_label_${product.code}">
							<product:productPricePanel product="${product}" />
						</ycommerce:testId>

						<div class="name">
							<spring:theme code="product.activity.type" />
							: ${product.activityType}
						</div>
						<div class="name">
							<spring:theme code="product.activity.destination" />
							: ${product.destination}
						</div>
					</div>
				</div>
			</div>

			<div class="product-details">
				<div class="description">${ycommerce:sanitizeHTML(product.description)}</div>
			</div>
		
	
		
				<div class="col-lg-12">
	<c:url var="addToCartUrl" value="/cart/addActivity${cartRequest}"/>
	<form:form method="post" id="addToCartForm" class=""
		action="${addToCartUrl}">
		

				<div class="input-required-wrap  col-xs-12 col-sm-4" id="js-return">
					<label for="departingDateTime" class="home-label-mobile">Activity
						Date <spring:theme var="departingDatePlaceholderText"
							code="text.cms.dealfinder.departure.date.placeholder"
							text="Departure Date" />
					</label> <input type="text" value="${activityDate}"  id="activityDate" 
						class="col-xs-12 datepicker input-grid form-control y_getUpdatePriceStock y_transportDepartDate"
						placeholder="${fn:escapeXml(departingDatePlaceholderText)}" 
						autocomplete="off" />
					<div class="y_departureDateError col-md-12 departureDate-error"></div>
				</div>

				<div class="row">
			<div class="col-lg-12">
				<c:if test="${product.stock.stockLevel gt 0}">
					<c:set var="productStockLevel"><p id="updateStockLevel">${product.stock.stockLevel}&nbsp;</p>
				<spring:theme code="product.variants.in.stock" />
					</c:set>
				</c:if>
				<c:if test="${product.stock.stockLevelStatus.code eq 'lowStock'}">
					<c:set var="productStockLevel">
						<spring:theme code="product.variants.only.left"
							arguments="${product.stock.stockLevel}" />
					</c:set>
				</c:if>
				<c:if test="${isForceInStock}">
					<c:set var="productStockLevel">
						<spring:theme code="product.variants.available" />
					</c:set>
				</c:if>
				<div class="stock-wrapper clearfix">${productStockLevel}</div>
				<div class="stock-wrapper clearfix">
					<c:set var="isForceInStock"
						value="${product.stock.stockLevelStatus.code eq 'inStock' and empty product.stock.stockLevel}" />
					<c:choose>
						<c:when test="${isForceInStock}">
							<c:set var="maxQty" value="FORCE_IN_STOCK" />
						</c:when>
						<c:otherwise>
							<c:set var="maxQty" value="${product.stock.stockLevel}" />
						</c:otherwise>
					</c:choose>
				</div>
				<input type="hidden" name="productCodePost"
					value="${fn:escapeXml(product.code)}" />

				<c:if test="${empty showAddToCart ? true : showAddToCart}">
					<c:set var="buttonType">button</c:set>
					<c:if
						test="${product.purchasable and product.stock.stockLevelStatus.code ne 'outOfStock' }">
						<c:set var="buttonType">submit</c:set>
					</c:if>
				</c:if>

				<!-- 		<div class="row">
			<div class="col-lg-3"> -->
				<c:set var="qtyMinus" value="1" />

<div class="addtocart-component">
		<c:if test="${empty showAddToCart ? true : showAddToCart}">
		<div class="qty-selector input-group js-qty-selector">
			<span class="input-group-btn">
				<button class="btn btn-default js-qty-selector-minus" type="button" <c:if test="${qtyMinus <= 1}"><c:out value="disabled='disabled'"/></c:if> ><span class="bcf bcf-icon-remove" aria-hidden="true" ></span></button>
			</span>
				<input type="text" maxlength="3" class="form-control js-qty-selector-input" size="1" value="${qtyMinus}" data-max="${maxQty}" data-min="1" name="pdpAddtoCartInput"  id="pdpAddtoCartInput" />
			<span class="input-group-btn">
				<button class="btn btn-default js-qty-selector-plus" type="button"><span class="bcf bcf-icon-add" aria-hidden="true"></span></button>
			</span>
		</div>
		</c:if>
		<div class="actions">
		<c:choose>
		<c:when test="${activityAvailable}">
				<ycommerce:testId code="addToCartButton">
					<button id="addToCartButton" type="${buttonType}"
						class="btn btn-primary btn-block js-add-to-cart js-enable-btn btn-icon glyphicon-shopping-cart"
						disabled="disabled">
						<spring:theme code="basket.add.to.basket" />
					</button>
				</ycommerce:testId>
				</c:when><c:otherwise>
				<ycommerce:testId code="addToCartButton">
					<button id="addToCartButton" type="button"
						class="btn btn-primary btn-block js-add-to-cart js-enable-btn btn-icon glyphicon-shopping-cart "
						disabled="disabled">
						<spring:theme code="basket.add.to.basket" />
					</button>
				</ycommerce:testId></c:otherwise>
				</c:choose>
			</div>
</div>



			</div>
			

		</div>
	</form:form></div></div>
	</div>
</div>

<div class="col-lg-7">
	<product:vacationSocialShare />
</div>

<div class="tabs js-tabs tabs-responsive col-lg-10">
	<ul class="clearfix tabs-list tabamount4">
		<li class="active first"><a href="#tab-1"> <span
				class="current-info">current tab: </span> <spring:theme
					code="product.activity.details" />
		</a></li>
		<li><a href="#tabs-2"><span class="current-info"></span> <spring:theme
					code="product.activity.termsConditions" /></a></li>
	</ul>

	<div id="tabs-1">
		<div class="tabbody">
			<div class="container-lg">
				<div class="row">
					<div class="col-md-6 col-lg-4">
						<div class="tab-container">
							<div class="fieldset-inner-wrapper">${product.description}</div>
						</div>
					</div>
				</div>
			</div>
		</div>
		<div id="tabs-2">
			<div class="tabbody">
				<div class="container-lg">
					<div class="row">
						<div class="col-md-6 col-lg-4">
							<div class="tab-container">
								<div class="fieldset-inner-wrapper">
									${product.termsAndCondition}</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>


	</div>
</div>
