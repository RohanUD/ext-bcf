<%@ tag body-content="empty" trimDirectiveWhitespaces="true"%>
<%@ attribute name="breadcrumbs" required="true" type="java.util.List"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>

  <ol class="breadcrumb">
    <c:forEach items="${breadcrumbs}" var="breadcrumb">
        <c:url value="${breadcrumb.url}" var="breadcrumbUrl" />
        <c:choose>
            <c:when test="${not empty breadcrumbUrl}">
                <li><a href="${breadcrumbUrl}">${breadcrumb.name}</a></li>
            </c:when>
            <c:otherwise>
                 <li class="active" aria-current="page">${breadcrumb.name}</li>
            </c:otherwise>
        </c:choose>
    </c:forEach>
  </ol>
