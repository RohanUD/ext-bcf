<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ attribute name="paginationCss" required="true" type="String"%>

<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 mb-3">
    <ul class="acco-pagination text-center">
        <li class="previous-page-control">
            <a href="#" class="${paginationCss}" data-pagenumber="${pageNum-1}">
                 <span>&lt;</span>
            </a>
        </li>
        <c:forEach begin="${startPageNumber}" end="${endPageNumber}" var="pageNumber">
            <li class="${pageNumber eq pageNum - 1 ? 'current-page disabled' : ''}">
                <a href="#" class="${paginationCss}" data-pagenumber="${pageNumber + 1}">
                    ${pageNumber + 1}
                </a>
            </li>
        </c:forEach>
        <li class="next-page-control">
            <a href="#" class="${paginationCss}" data-pagenumber="${pageNum + 1}">
               <span>&gt;</span>
            </a>
        </li>
    </ul>
</div>
