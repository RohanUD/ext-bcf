<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ attribute name="currentField" required="true" type="java.lang.String"%>

<c:forEach items="${paramValues}" var="requestParameter">
  <c:if test="${requestParameter.key ne currentField}">
    <c:forEach items="${requestParameter.value}" var="requestParameterValues">
        <input type="hidden" name="${requestParameter.key}" value="${requestParameterValues}"/>
    </c:forEach>
  </c:if>
</c:forEach>
