<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>

<c:set var="numberOfPages" value="0" scope="request"/>
<c:set var="currentPage" value="0" scope="request"/>
<c:if test="${not empty facetSearchPageData}">
    <c:set var="numberOfPages" value="${facetSearchPageData.pagination.numberOfPages}" scope="request"/>
    <c:set var="currentPage" value="${facetSearchPageData.pagination.currentPage}" scope="request"/>
    <c:set var="totalNumberOfResults" value="${facetSearchPageData.pagination.totalNumberOfResults}" scope="request"/>
    <c:set var="pageSize" value="${facetSearchPageData.pagination.pageSize}" scope="request"/>
</c:if>
<c:if test="${not empty paginationMetaData}">
    <c:set var="numberOfPages" value="${paginationMetaData.numberOfPages}" scope="request"/>
    <c:set var="currentPage" value="${paginationMetaData.currentPage}" scope="request"/>
    <c:set var="totalNumberOfResults" value="${paginationMetaData.totalNumberOfResults}" scope="request"/>
    <c:set var="pageSize" value="${paginationMetaData.pageSize}" scope="request"/>
</c:if>

<c:set var="startIndex" value="${(currentPage * pageSize) + 1}" scope="request"/>
<c:set var="endIndex" value="${((currentPage + 1) * pageSize)}" scope="request"/>

<c:if test="${endIndex gt totalNumberOfResults}">
    <c:set var="endIndex" value="${totalNumberOfResults}" scope="request"/>
</c:if>
<div class="text-center bottom-pagination">
    <spring:theme code="text.global.pagination.showing" arguments="${startIndex},${endIndex},${totalNumberOfResults}"/>
</div>
