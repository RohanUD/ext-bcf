<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@ taglib prefix="pagination" tagdir="/WEB-INF/tags/responsive/pagination"%>

<pagination:pageNumbers/>
<c:if test="${numberOfPages gt 1}">
    <div class="container">
       <ul class="acco-pagination text-center">
          <li>
             <form action="${requestScope['javax.servlet.forward.request_uri']}">
                <pagination:storeRequestParams currentField="page"/>
                <input type="hidden" name="page" value="${currentPage - 1}"/>
                <input type="button" value="<" ${currentPage <= 0 ? 'disabled' : ''} onClick="this.form.submit();"/>
             </form>
          </li>
          <fmt:parseNumber var="displayPageNumberCount" integerOnly="true" type="number" value="${totalDisplayPageNumbers/2}" />
          <c:set var="startPage" value="${currentPage - displayPageNumberCount}"/>
          <c:set var="endPage" value="${currentPage + displayPageNumberCount}"/>
          <c:if test="${startPage lt 0}">
             <c:set var="endPage" value="${endPage + (0 - startPage)}"/>
             <c:set var="startPage" value="0"/>
          </c:if>
          <c:if test="${endPage >= numberOfPages}">
             <c:set var="endPage" value="${numberOfPages}"/>
          </c:if>
          <c:if test="${totalDisplayPageNumbers % 2 eq 0 and endPage gt 0}">
             <c:set var="endPage" value="${endPage - 1}"/>
          </c:if>
          <c:if test="${endPage - startPage lt totalDisplayPageNumbers}">
             <c:set var="startPage" value="${endPage - (totalDisplayPageNumbers - 1)}"/>
          </c:if>
          <c:if test="${startPage lt 0}">
             <c:set var="startPage" value="0"/>
          </c:if>
          <c:forEach begin="${startPage}" end="${endPage}" var="pageNumber">
             <li>
                <form action="${requestScope['javax.servlet.forward.request_uri']}">
                   <pagination:storeRequestParams currentField="page"/>
                   <input type="hidden" name="page" value="${pageNumber}"/>
                   <input type="button" class="${currentPage eq pageNumber ? 'current-page' : ''}" value="${pageNumber + 1}" ${pageNumber eq currentPage ? 'disabled' : ''} onClick="this.form.submit();"/>
                </form>
             </li>
          </c:forEach>
          <li>
             <form action="${requestScope['javax.servlet.forward.request_uri']}">
                <pagination:storeRequestParams currentField="page"/>
                <input type="hidden" name="page" value="${currentPage + 1}"/>
                <input type="button" value=">" ${(currentPage + 1) ge (numberOfPages) ? 'disabled' : ''} onClick="this.form.submit();"/>
             </form>
          </li>
       </ul>
    </div>
    <div class="row">
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 text-center">
            <div class="back-top-btn">
                 <a href="#"><span class="icon-up-circle"><span class="bcf bcf-icon-up-arrow"></span></span>
                    <strong><spring:theme code="text.global.pagination.back.to.top" text="Back To Top" /></strong>
                </a>
            </div>
        </div>
    </div>
</c:if>
