<%@ tag body-content="empty" trimDirectiveWhitespaces="true" %>
<%@ attribute name="actionNameKey" required="true" type="java.lang.String" %>
<%@ attribute name="action" required="true" type="java.lang.String" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="formElement" tagdir="/WEB-INF/tags/responsive/formElement" %>
<%@ taglib prefix="ycommerce" uri="http://hybris.com/tld/ycommercetags" %>

<spring:htmlEscape defaultHtmlEscape="false"/>

<c:set var="hideDescription" value="checkout.login.loginAndCheckout"/>
<c:url value="/login/pw" var="forgotPasswordUrl"/>

<div class="login-page__headline">
    <h4><spring:theme code="login.title"/></h4>
</div>

<c:if test="${actionNameKey ne hideDescription}">
    <p class="login-description">
        <spring:theme code="login.description"/>
    </p>
</c:if>

<c:if test="${not empty param.message}">
		<span class="has-error">
		<c:if test="${not empty param.email}">
            <a href="/login/resendEmail?email=${param.email}">
                <spring:theme code="resend.verification.email.message" text="Resend account activation email."/>
            </a>
        </c:if>
		</span>
</c:if>

<p>
    <span class="red-text">*</span>
    <spring:theme code="fields.required.desc" text="These fields are required"/>
</p>

<form:form action="${action}" method="post" commandName="loginForm">
    <spring:theme code="forgottenPwd.email.placeholder" var="EmailPlaceholder"/>
	<spring:theme code="login.password.placeholder" var="PasswordPlaceholder" />
    <c:if test="${not empty param['updateEmail'] and not empty jalosession.getAttribute('oldEmailId')}">
        <div class="form-group">
            <label class="control-label " for="oldEmailId">
                <spring:theme code="update.email.old.email.label" text="Old email ID"/>
            </label>
            <input id="oldEmailId" name="oldEmailId" class=" form-control" type="text"
                   value="${jalosession.getAttribute("oldEmailId")}" disabled="disabled">
        </div>
    </c:if>
    <formElement:formInputBox idKey="j_username" labelKey="login.email" path="j_username" mandatory="true" placeholder="${EmailPlaceholder}"/>
    <formElement:formPasswordBox idKey="j_password" labelKey="login.password" path="j_password" inputCSS="form-control"
                                 mandatory="true" placeholder="${PasswordPlaceholder}"/>

    <div class="forgotten-password mb-5 fnt-14">
        <ycommerce:testId code="login_forgotPassword_link">
            <a href="${forgotPasswordUrl}">
                <spring:theme code="login.link.forgottenPwd"/>
            </a>
        </ycommerce:testId>
    </div>

    <div class="login-btn">
    <ycommerce:testId code="loginAndCheckoutButton">
        <button type="submit" class="btn btn-primary btn-block mb-4">
            <spring:theme code="${actionNameKey}"/>
        </button>
    </ycommerce:testId>
    </div>


    <c:if test="${expressCheckoutAllowed}">
        <button type="submit" class="btn btn-default btn-block expressCheckoutButton"><spring:theme
                code="text.expresscheckout.header"/></button>
        <input id="expressCheckoutCheckbox" name="expressCheckoutEnabled" type="checkbox"
               class="form left doExpressCheckout display-none"/>
    </c:if>

</form:form>

