<%@ tag body-content="empty" trimDirectiveWhitespaces="true" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="formElement" tagdir="/WEB-INF/tags/responsive/formElement" %>
<%@ taglib prefix="theme" tagdir="/WEB-INF/tags/shared/theme" %>
<c:url value="/login/pw/resetpassword" var="resetpasswordUrl" />
<spring:htmlEscape defaultHtmlEscape="false"/>
<div class="container">
  <div class="row">
    <div class="col-md-9 col-xs-12">
        <div class="account-section">
            <div class="account-section-header no-border fnt-14"><b><spring:theme code="text.account.profile.resetPassword"/></b></div>
            <div class="account-section-content row">
                <form:form method="post" commandName="bcfUpdatePasswordForm" action="${resetpasswordUrl}">
                    <div class="col-lg-7 col-md-7 col-sm-12 col-xs-12 p-relative col-md-offset-3">
                        <div class="form-group">
                            <formElement:formPasswordBox idKey="password" labelKey="updatePwd.pwd" path="pwd"
                                                         inputCSS="form-control password-strength" mandatory="true"/>
                        </div>
                        <div class="form-group">
                            <formElement:formPasswordBox idKey="updatePwd.checkPwd" labelKey="updatePwd.checkPwd"
                                                         path="checkPwd" inputCSS="form-control" mandatory="true"/>
                        </div>
                       
                    </div>
                     <div class="row login-form-action">
                            <div class="col-lg-5 col-md-5 col-xs-12 col-md-offset-4 margin-top-20">
                                <button type="submit" class="btn btn-primary btn-block">
                                    <spring:theme code="updatePwd.submit"/>
                                </button>
                            </div>
                            <div class="col-lg-5 col-md-5 col-xs-12 col-md-offset-4 margin-top-20">
                                <button type="button" class="btn btn-outline-primary btn-block backToHome">
                                    <spring:theme code="text.button.cancel"/>
                                </button>
                            </div>
                        </div>
                </form:form>
            </div>
        </div>
        </div>
  </div>
        </div>

