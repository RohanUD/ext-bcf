ACC.common = {
    _autoloadTracc: [
        "bindAnonymousUserSaveQuoteButton",
        "bindAnonymousUserEmail",
        "bindLoggedinUserEmail"
    ],

	currentCurrency: $("main").data('currencyIsoCode') || "USD",
	processingMessage: $("<img src='" + ACC.config.commonResourcePath + "/images/spinner.gif'/>"),


	blockFormAndShowProcessingMessage: function (submitButton)
	{
		var form = submitButton.parents('form:first');
		form.block({ message: ACC.common.processingMessage });
	},

	shallCartBeRemoved: function (journeyType)
	{
		var returnValue = false;
		$.when(ACC.services.shallCartBeRemoved(journeyType)).then(
			function(data) {
				if(data)
					{
						$.when(ACC.services.removeCurrentCart()).then(
			                function(data) {
			                    return true;
			                });
					}
			});

		if (returnValue) {
			return false;
		}
	},

	refreshScreenReaderBuffer: function ()
	{
		// changes a value in a hidden form field in order
		// to trigger a buffer update in a screen reader
		$('#accesibility_refreshScreenReaderBufferField').attr('value', new Date().getTime());
	},

	checkAuthenticationStatusBeforeAction: function (actionCallback)
	{
		$.ajax({
			url: ACC.config.authenticationStatusUrl,
			statusCode: {
				401: function () {
					location.href = ACC.config.loginUrl;
				}
			},
			success: function (data) {
				if (data == "authenticated") {
					actionCallback();
				}
			}
		});
	},

	bindAnonymousUserSaveQuoteButton: function () {
	    $(document).on('click', '#y_anonymoususer_savequote', function(event) {
            event.preventDefault();
            $(".y_anonymoususer_email_submit").removeClass("hidden");
        });
	},

    bindAnonymousUserEmail: function() {
        $(document).on('click', '#y_anonymoususer_submit', function(event) {
            event.preventDefault();
            var input = document.getElementById('y_anonymoususer_email');
            var validMail = validateEmail(input.value);
            if(validMail){
            $( ".y_anonymoususer_emailerror" ).empty();
            $.when(ACC.services.saveQuoteAnonymousUserAjax(input.value)).then(
             function(data) {
                $(".y_bcfGlobalReservationModalComponent .close").click();
                if(data.result == true) {
                    var quoteSuccess = ".y_anonymoususer_quotesuccess";
                    var htmlString = data.success;
                    $(quoteSuccess).html(htmlString);
                    $('.quotesuccess').removeClass('hidden');
                }
                else if (data.result == false) {
                    var quoteError = ".y_anonymoususer_quoteerror";
                    var htmlString = data.error;
                    $(quoteError).html(htmlString);
                    $('.quoteerror').removeClass('hidden');
                }
             });
            }else{
                 var emailerrormessage = ".y_anonymoususer_emailerror";
                 var htmlStringStart = 'EmailAddress entered is incorrect';
                 $(emailerrormessage).html(htmlStringStart);
            }
        });
    },

    bindLoggedinUserEmail: function() {
        $(document).on('click', '#y_loggedinuser_submit', function(event) {
            event.preventDefault();
            $.when(ACC.services.saveQuoteLoggedinUserAjax()).then(
             function(data) {
                $(".y_bcfGlobalReservationModalComponent .close").click();
                if(data.result == true) {
                    var quoteSuccess = ".y_anonymoususer_quotesuccess";
                    var htmlString = data.success;
                    $(quoteSuccess).html(htmlString);
                    $('.quotesuccess').removeClass('hidden');
                }
                else if (data.result == false) {
                    var quoteError = ".y_anonymoususer_quoteerror";
                    var htmlString = data.error;
                    $(quoteError).html(htmlString);
                    $('.quoteerror').removeClass('hidden');
                }
             });
        });
    }
};

/* Extend jquery with a postJSON method */
jQuery.extend({
	postJSON: function (url, data, callback)
	{
		return jQuery.post(url, data, callback, "json");
	}
});

// add a CSRF request token to POST ajax request if its not available
$.ajaxPrefilter(function (options, originalOptions, jqXHR)
{
	// Modify options, control originalOptions, store jqXHR, etc
	if (options.type === "post" || options.type === "POST")
	{
		var noData = (typeof options.data === "undefined");
		if (noData)
		{
			options.data = "CSRFToken=" + ACC.config.CSRFToken;
		}
		else
		{
			var patt1 = /application\/json/i;
			if (options.data instanceof window.FormData)
			{
				options.data.append("CSRFToken", ACC.config.CSRFToken);
			}
			// if its a json post, then append CSRF to the header. 
			else if (patt1.test(options.contentType))
			{
				jqXHR.setRequestHeader('CSRFToken', ACC.config.CSRFToken);
			}
			else if (options.data.indexOf("CSRFToken") === -1)
			{
				options.data = options.data + "&" + "CSRFToken=" + ACC.config.CSRFToken;
			}
		}
		
	}
});
//Hide tab if has no text or link 
$(document).ready(function(){
	setTimeout(function(){
		$('ul.flow-tab--list li').each(function() {
			if ($.trim($(this).text()).length === 0){
				$(this).hide();
			 }
		});
	},1000);
	// Added for price-breakdown arrow toggle
	$( "p.price-breakdown" ).click(function() {
		$( ".bcf-icon-down-arrow" ).toggleClass( "bcf-icon-up-arrow" );
	});

	$( ".toggle-arrow" ).each(function() {
		$(this).on('click', function(){
			$( this ).find(".bcf-icon-down-arrow").toggleClass( "bcf-icon-up-arrow" );
		});
	});

	$( ".view-deck" ).each(function() {
		$(this).on('click', function(){
			$( this ).find(".bcf-icon-down-arrow").toggleClass( "bcf-icon-up-arrow" );
		});
	});

	$(".importantInfo").click(function(){
		$(".info-sailing-details-show").toggle();
		$( ".bcf-icon-down-arrow" ).toggleClass( "bcf-icon-up-arrow" );
	});

	//Travel Advisories

	$('.travel-advisories-close').on('click', function() {
        $(this).parent('.alert').slideUp();
        $(".travel-advisories-reset").slideDown();
        $(".travel-advisories").addClass("bg-transparent");
        $.get("/travel-advisories/suppress-alert");
	});

	$('.travel-advisories-reset').on('click', function() {
		$('.alert').slideDown();
		$(".travel-advisories-reset").slideUp();
		$(".travel-advisories").removeClass("bg-transparent");
		$.get("/travel-advisories/restore-ribbon");
	});

	//view Travel Advisories
	$(".viewTravelAdvisories").click(function() {
		var lable = $(".viewTravelAdvisories").text();

		if(lable == "Hide") {
			$(".viewTravelAdvisories").text("View");
			$(".detailTravelAdvisories").slideUp();
		}
		else {
			$(".viewTravelAdvisories").text("Hide");
			$(".detailTravelAdvisories").slideDown();
		}
	});
	
	calTravelAdvisoriesText = function(obj){
		other = obj.clone();
		other.html('a<br>b').hide().appendTo('body');
		size = other.height() / 2;
		other.remove();
		return obj.height() /  size;
	  }
	  n = calTravelAdvisoriesText($('.travel-advisories .alert .detailTravelAdvisories .advisoryDescription'));
	  n = Math.round(n);
	  if(n >= 5){
		  $('p.advisoryDescription').removeClass("line-clamp");
		  $('a.read-more-advisory').hide();
	  } else{
		  $('p.advisoryDescription').addClass("line-clamp");
		  $('a.read-more-advisory').show();
	  }

	$('.moreless-one').click(function() {
		
		if ($('.moreless-one').text() == "More") {
			$(this).text("Less");
			$('.moretext').slideToggle();
		} else {
			$(this).text("More");
			$('.moretext').slideToggle();
		}
	});

	$('.moreless-two').click(function() {
		$('.moretext').slideToggle();
		if ($('.moreless-two').text() == "More") {
			$(this).text("Less")
		} else {
			$(this).text("More")
		}
	});

	$('.moreless-three').click(function() {
		$('.moretext').slideToggle();
		if ($('.moreless-three').text() == "More") {
			$(this).text("Less")
		} else {
			$(this).text("More")
		}
	});

	$(".advanced-search a.advance-serch-link").click(function() {
		if($(this).children(".bcf").hasClass("bcf-icon-up-arrow")){
			$(this).children(".bcf").removeClass("bcf-icon-up-arrow").addClass("bcf-icon-down-arrow");
			$('.accountPageBodyContent').find('.btn-group').show();
		}
		else{
			$(this).children(".bcf").removeClass("bcf-icon-down-arrow").addClass("bcf-icon-up-arrow");
			$('.accountPageBodyContent').find('.btn-group').hide();
		}

		$(".advanced-search .advanced-search-wrapper").slideToggle("slow");
			return false;
	});

	$('button.quote-msg-close').click(function(){
	    $('.quotesuccess').addClass('hidden');
	});

 });



