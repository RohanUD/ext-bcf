//MHT - Moneris Hosted Tokenization

ACC.tokenizer = {

	_autoload : [ "bindOnLoad","bindSavedPaymentInfo" ],
	pathSeparator : "/",
	config : null,
	sucCallback : null,
	failureCallback : null,

	bindOnLoad : function() {
		window.onload = function() {
			if (window.addEventListener) {
				window
						.addEventListener("message", ACC.tokenizer.respMsg,
								false);
			} else {
				if (window.attachEvent) {
					window.attachEvent("onmessage", ACC.tokenizer.respMsg);
				}
			}
		}

		$(window).load(function() {
			ACC.tokenizer.htInit();


			$("#ht_account_submit").on("click", function() {
				var checkValidForm=$('#silentOrderPostForm').valid();
				if(checkValidForm==true) {
					ACC.tokenizer.htSubmit();
				}
			});

			$("#ht_submit").on("click", function(event) {
			event.preventDefault();
			$("#ht_submit").prop('disabled',true);
			var checkValidForm=$('#silentOrderPostForm').valid();
			$('#terms-error').remove();
			if(checkValidForm==true)
			{
				if($('#termsCheck1').prop('checked')==false)
				{
					$('.payment-agree').removeClass('hidden');
					$('#termsCheck1').parent().append('<div id="terms-error" class="fe-error">'+ACC.addons.bcfstorefrontaddon['error.formvalidation.payment.terms_and_conditions']+'</div>');
					$("#ht_submit").prop('disabled',false);
				}
				else
				{
					$.when(ACC.services.validateCartBeforePayment()).then(
	                    function(data) {
	                        if(data.valid == false){
	                           $('#y_cartValidationWebModal').modal('show');
	                           $('#y_OriginTerminal').html(data.origin);
	                           $('#y_ArrivalTerminal').html(data.destination);
	                           $('#y_DepartureTime').html(data.departureTime);
	                           $('#y_ArrivalTime').html(data.arrivalTime);
	                           $("#ht_submit").prop('disabled',false);
	                        }
	                        else {
	                        	if ($(".new-card-and-billing-details").hasClass("hidden")) {
	                        		$('#silentOrderPostForm').submit();
	                        	}
	                        	else {
		                            ACC.tokenizer.htSubmit();
		                            $("#ht_submit").prop('disabled',false);
	                        	}
	                        }
	                    }
	             	);
	            }
			}
			else
				{
					if ($(".new-card-and-billing-details").hasClass("hidden")) {
						$(".new-card-and-billing-details").removeClass("hidden");
	            	}
					$("#ht_submit").prop('disabled',false);
				}
		  });
		});


	},
	
	bindSavedPaymentInfo: function(){
		$(".savedPaymentSubmit").click(function() {
			if(!$(this).closest('.row').find("input[id=card_cvNumber]").val()){
				$(this).closest('.row').find("#cvvError").text("This is mandatory field")
				return false;
			}
			$(this).closest('.form-group').find("input[name=card_cvNumber]").val($(this).closest('.row').find("input[id=card_cvNumber]").val())
		});
	},
	
	htInit : function() {
		// tip: append @location.host for environment-specific values
		ACC.tokenizer.config = {
			'monFrameId' : 'monerisFrame',
			'id' : $("#ht_id").val(),
			'host' : $("#ht_host").val(),
			'host_url' : $("#ht_host_tokenizerURL").val(),
			'attributes' : {
				'css_body' : 'border:none;border-radius:4px;',
				'css_label_pan':'color:rgb(134, 134, 126);display:block;margin-bottom:10px;outline:none;font-size: 14px;font-family: Open Sans,Helvetica Neue Helvetica,Arial,sans-serif;',
				'css_textbox':'border:1px solid;border-color:rgb(134, 134, 126);padding:6px 12px;outline: none;font-size: 14px;height:45px;',
				'css_textbox_pan':'width: 100%;',
				'display_labels':1
			}
		};

		// (optional) callback when $ht.init() completes
		function htReady(num, errorIfAny) {
			if (num < 0) { // ERROR!
				console.log("IFrame not initialized!!, reason: " + errorIfAny);
			} else { // READY!
				console.log("IFrame ready!!");
			}
		}
		var frame = document.getElementById(ACC.tokenizer.config.monFrameId);
		ACC.tokenizer.init(frame, htReady);
	},
	init : function(frame, callback) {
		try {
			var url = ACC.tokenizer.config.host_url;
			url += "?";

			if (ACC.tokenizer.config.id) {
				url += "id=" + ACC.tokenizer.config.id;
			} else {
				throw "mandatory attribute 'id' not present!";
			}

			$.each(ACC.tokenizer.config.attributes, function(key, value) {
				if (!key) {
					log.warn("Not a valid key:" + key);
				}

				if (!value) {
					log.warn("Not a valid value for key: " + key + ", value: "
							+ value);
				}

				if (key && value) {
					url += "&" + key + "=" + value;
				}

			});

			var iFrame = null;

			if (frame) {
				iFrame = frame;
			} else {
				iFrame = ACC.tokenizer.getFrameElement();
			}
			if (url && isFrameSrcBlank(iFrame)) {
				$(iFrame).attr("src", url);
			}

			callback(0);
		} catch (e) {
			callback(-1, e);
		}
	},
	htSubmit : function() {
		function htFailure(obj) {
			// sample: obj: {"message":"invalid data","code":["943"],"def":"card
			// data invalid","ms":136}
			// note: may still get called after timeout, dev choice to ignore

			$('.monerisFrameError').text(ACC.services.getValidationMessageForCode('error.creditcardtokenization.'+ obj.responseCode[0]).responseText);
			$('html, body').animate({
		        scrollTop: $(".monerisFrameError").offset().top
		    }, 500);
		}
		function htSuccess(obj) {
			$("#silentOrderPostForm input[id= pd-card-number]").val(obj.dataKey);
			$('#silentOrderPostForm').submit();
		}
		function htTimeout(num) {
		}
		ACC.tokenizer.submit(htSuccess, htFailure, htTimeout, 1000);
	},

	submit : function(htSuccess, htFailure, htTimeout, htTimeoutPeriod) {

		ACC.tokenizer.sucCallback = htSuccess;
		ACC.tokenizer.failureCallback = htFailure;

		var monFrameRef = ACC.tokenizer.getFrameElement().contentWindow;
		monFrameRef.postMessage('', ACC.tokenizer.config.host);

		// change link according to table above
		return false;
	},

	getFrameElement : function() {
		return document.getElementById(ACC.tokenizer.config.monFrameId);
	},

	respMsg : function(e) {
		if (e.origin == ACC.tokenizer.config.host) {

			if (!e.data) {
				return;
			}
			var respData = JSON.parse(e.data);
			if (!respData.dataKey) {
				if (ACC.tokenizer.failureCallback) {
					ACC.tokenizer.failureCallback(respData);
				}
			}

			if (respData.dataKey) {
				ACC.tokenizer.sucCallback(respData);
			}
			if (respData.errorMesmonerisFramesage) {
				ACC.tokenizer.failureCallback(respData);
			}
		}
	}
};

function isFrameSrcBlank(iFrame) {
	if (!iFrame) {
		console.log("iFrame is null!");
		return false;
	}
	return ($(iFrame).prop('src').trim().length == 0 || $(iFrame).attr("src") == "about:blank");
}
