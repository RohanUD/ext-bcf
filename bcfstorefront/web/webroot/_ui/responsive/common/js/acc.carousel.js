var dataCount = 1;
ACC.carousel = {

	_autoload: [
		["bindCarousel", $(".js-owl-carousel").length >0],
		"bindJCarousel",
		["bindAccordion", $(".js-accordion").length >0],
		"adjustSearchHeight",
		"bindJquerytab"
	],

	carouselConfig:{
		"default":{
			nav:false,
			loop:false,
			margin:40,
			dots:true,
			pagination:true,
			responsive:{
		        0:{
		            items:1
		        },
		        600:{
		            items:2
		        },
		        1000:{
		            items:dataCount
		        }
		    }
		},
		"content":{
			nav:true,
			loop:true,
			dots:true,
			navText : ["<i class='fa fa-chevron-left'></i>", "<i class='fa fa-chevron-right'></i>"],
			pagination:true,
			responsive:{
		        0:{
		            items:1
		        },
		        600:{
		            items:1
		        },
		        1000:{
		            items:dataCount
		        }
		    }
		},
		"news-carousel":{
			nav:true,
			loop:true,
			dots:true,
			margin:40,
			navText : ["<i class='fa fa-chevron-left'></i>", "<i class='fa fa-chevron-right'></i>"],
			pagination:true,
			responsive:{
		        0:{
		            items:1
		        },
		        600:{
		            items:1
		        },
		        1000:{
		        	items:dataCount
		        }
		    }
		},
		"rotating-image":{
			nav:false,
			pagination:false,
			singleItem : true,
			responsive:{
		        0:{
		            items:1
		        },
		        600:{
		            items:1
		        },
		        1000:{
		            items:dataCount
		        }
		    }

		},
		"rotating-gallery":{
			nav:true,
			pagination:false,
			loop:false,
			dots:false,
			navText : ["<i class='fa fa-chevron-left'></i>", "<i class='fa fa-chevron-right'></i>"],
			pagination:true,
			responsive:{
		        0:{
		            items:1
		        },
		        600:{
		            items:1
		        },
		        1000:{
		            items:dataCount
		        }
		    },
		    onInitialized  : showIndex,
            onTranslated : showIndex

		},
		"lazy-reference":{
			nav:true,
			navText : ["<span class='glyphicon glyphicon-chevron-left'></span>", "<span class='glyphicon glyphicon-chevron-right'></span>"],
			pagination:false,
			itemsDesktop : [5000,7],
			itemsDesktopSmall : [1200,5],
			itemsTablet: [768,4],
			itemsMobile : [480,3],
			lazyLoad:true
		}
	},

	bindCarousel: function(){
		$(".js-owl-carousel").each(function(){
			var $c = $(this)
			$.each(ACC.carousel.carouselConfig,function(key,config){
				if($c.hasClass("js-owl-"+key)){
					var $e = $(".js-owl-"+key);
					dataCount = $c.data("count");
						if (typeof dataCount !== "undefined") {config.responsive['1000'].items=dataCount; }
						$c.owlCarousel(config);
				}
			});
		});
	},
	accordionConfig:{
		"default":{
			header : "h4",
			collapsible : true,
			active : false,
			heightStyle: "content"
		},
		"contentpage":{
			header : "header",
			collapsible : true,
			active : true,
			heightStyle: "content"
		}
	},

	bindAccordion: function ()
	{
		$(".js-accordion").each(function(){
			var $c = $(this)
			$.each(ACC.carousel.accordionConfig,function(key,config){
				if($c.hasClass("js-accordion-"+key)){
					var $e = $(".js-accordion-"+key);
					$e.accordion(config);
				}
			});
		});

	},


	bindJCarousel: function ()
	{

		$(".modal").colorbox({
			onComplete: function ()
			{
				ACC.common.refreshScreenReaderBuffer();
			},
			onClosed: function ()
			{
				ACC.common.refreshScreenReaderBuffer();
			}
		});
		$('.svw').each( function(){
	          $( this).waitForImages( function(){
	               $(this).slideView({toolTip: true, ttOpacity: 0.6, autoPlay: true, autoPlayTime: 8000});
	          });
	    });
	},


	bindJquerytab: function ()
	{
		$( "#js-home-tab" ).tabs({
	    	create: function( event, ui ) {
	    		ACC.carousel.adjustSearchHeight();
	    	},
	    	activate: function( event, ui ) {
	    		ACC.carousel.adjustSearchHeight();
	    	}

	    });
	     if ($(".page-routesandFaresPage").length > 0){
           $( "#js-vacation-form" ).tabs({
            create: function( event, ui ) {
                ACC.carousel.adjustSearchHeight();
            },
            activate: function( event, ui ) {
                ACC.carousel.adjustSearchHeight();

            }

            });
         }
	},

	adjustSearchHeight: function() {
		if ($("#js-home-tab").length > 0){
			var height= $("#js-home-tab").find(".ui-tabs-panel:visible").height();
			if(!height){
				height=$(".ui-tabs").innerHeight();
				$('.bc-search-container__form-box').addClass('landing-box-shadow');
			}
			if ($(window).width() < 960) {
				height=height+60;
				}else{
					height=height-50;
				}
			$('.js-element-margin').css('margin-bottom',height+"px");
		}else if ($(".page-routesandFaresPage").length > 0) {
                var height= $("#js-vacation-form").find(".ui-tabs-panel:visible").height();
                if(!height){
                    height=$(".ui-tabs").innerHeight();
                    $('.bc-search-container__form-box').addClass('landing-box-shadow');
                }
                if ($(window).width() < 960) {
                    height=height+60;
                    }else{
                        height=height-20;
                    }
                $('.js-element-margin').css('margin-bottom',height+"px");

        }else {
			var height=$("#js-vacation-form").innerHeight();
			height=height-140;
			$('.js-element-margin').css('margin-bottom',height+"px");

		}

	}
};

function showIndex(event) {
    var element   = event.target;         // DOM element, in this example .owl-carousel
    var items     = event.item.count;     // Number of items
    var item      = event.item.index + 1;     // Position of the current item
    $(element).find(".slider-num").html(item+"/"+items)
}
