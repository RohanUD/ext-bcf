ACC.paymentDetails = {
	_autoload: [
		"showRemovePaymentDetailsConfirmation",
		"bindUserPaymentOptionSelection"
	],

	showRemovePaymentDetailsConfirmation: function ()
	{
		$(document).on("click", ".removePaymentDetailsButton", function ()
		{
			var paymentId = $(this).data("paymentId");
			var popupTitle = $(this).data("popupTitle");

			ACC.colorbox.open(popupTitle,{
				inline: true,
				href: "#popup_confirm_payment_removal_" + paymentId,
				onComplete: function ()
				{
					$(this).colorbox.resize();
				}
			});

		})
	},
	bindUserPaymentOptionSelection: function ()
    	{
    		$(document).on("click", "#y_useUserPaymentOptions", function ()
    		{
    			 $.when(ACC.services.useUserPaymentOption($("#y_selectPaymentOptionForm"))).then(function(response) {
                           var result=response;
                                     ACC.reservation.refreshReservationTotalsComponent($("#y_reservationTotalsComponentId").val());
                                    return;
                           }
                        );

    		})
    	}
}
