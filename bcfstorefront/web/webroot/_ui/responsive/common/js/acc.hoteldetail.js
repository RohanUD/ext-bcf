ACC.hoteldetail = {
	_autoloadTracc: [
		"bindHotelDetailsResult"
	],
            bindHotelDetailsResult: function() {
                    $("[href^='/hotel']").click(function(e){
                    e.preventDefault();
                    var code=$(this).attr("href").split("/").pop();
                    ACC.hoteldetail.getDetails($(this),code);
                    });
                },
           getDetails: function(ele,code) {
               $(ele).closest("body").append('<div class="y_hotelDetailModal"></div>');
               $.when(ACC.services.getHotels(code)).then(
               function(data){
               $(".y_hotelDetailModal").html(data.hotelDetailModalHtml);
               $("#y_hotelDetailsModal").modal('show');
               });
               ACC.global.initImager();
               return false;
           }
}
