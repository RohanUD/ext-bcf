ACC.checkout = {

	_autoload: [
		"bindCheckOutValidationMessages",
		"bindCheckO",
		"bindForms",
		"bindSavedPayments",
		"bindingGuestCheckoutValidation",
        "bindCallCentrePaymentModes"
	],

	bindCheckOutValidationMessages : function () {
		ACC.CheckOutValidationMessages = new validationMessages("ferry-bindCheckOutValidationMessages");
        ACC.CheckOutValidationMessages.getMessages("error");
	},
	bindForms:function(){

		$(document).on("click","#addressSubmit",function(e){
			e.preventDefault();
			$('#addressForm').submit();
		})

		$(document).on("click","#deliveryMethodSubmit",function(e){
			e.preventDefault();
			$('#selectDeliveryMethodForm').submit();
		})

	},
    bindCallCentrePaymentModes : function() {
		if ($('.asm-payment-type-selector').length > 0) {
			$("#y_agentPayNow").validate({
				rules : {
					paymentType : {
						required : true
					}
				}
			});
		}
	},
	      bindingGuestCheckoutValidation : function() {
                         $('#bcfGuestForm').validate({
                             errorElement: "span",
                             errorClass: "fe-error",
                             ignore: ".fe-dont-validate",
                             onfocusout: function(element) {
                                 $(element).valid();
                             },
                             rules: {
                                 firstName: {
                                     required: true,
                                     nameValidation: true
                                 },
                                 lastName: {
                                     required: true,
                                     nameValidation: true
                                 },
                                 phoneNo : {
                                     required: true,
                                     validatePhoneNumberPattern: true
                                 },
                                 email : {
                                      required:true,
                                      validateEmailPattern: true
                                  }

                             },
                             messages: {
                                 firstName: {
                                     required: ACC.CheckOutValidationMessages.message('error.formvalidation.firstName'),
                                     nameValidation: ACC.CheckOutValidationMessages.message('error.formvalidation.firstNameValid')
                                 },
                                 lastName: {
                                     required: ACC.CheckOutValidationMessages.message('error.formvalidation.lastName'),
                                     nameValidation: ACC.CheckOutValidationMessages.message('error.formvalidation.lastNameValid')
                                 },
                                 phoneNo: {
                                     required: ACC.CheckOutValidationMessages.message('error.formvalidation.mobileNumber'),
                                     validatePhoneNumberPattern: ACC.CheckOutValidationMessages.message('error.lead.guest.phone.invalid')
                                 },
                                  email: {
                                      validateEmailPattern: ACC.CheckOutValidationMessages.message('error.lead.guest.email.invalid')
                                   }

                             }
                         });
                     },


	bindSavedPayments:function(){
		$(document).on("click",".js-saved-payments",function(e){
			e.preventDefault();
			var title = $("#savedpaymentstitle").html();

			$.colorbox({
				href: "#savedpaymentsbody",
				inline:true,
				maxWidth:"100%",
				opacity:0.7,
				//width:"320px",
				title: title,
				close:'<span class="glyphicon glyphicon-remove"></span>',
				onComplete: function(){
				}
			});
		})
	},

	bindCheckO: function ()
	{
		var cartEntriesError = false;

		// Alternative checkout flows options
		$('.doFlowSelectedChange').change(function ()
		{
			if ('multistep-pci' == $('#selectAltCheckoutFlow').val())
			{
				$('#selectPciOption').show();
			}
			else
			{
				$('#selectPciOption').hide();

			}
		});


        $("#y_agentPayNowBtn").click(function(e) {
            e.preventDefault();
            var checkValidForm=$('#y_agentPayNow').valid();
            if(checkValidForm){
            $.when(ACC.services.validateCartBeforePayment()).then(
                function(data) {
                    if(data.valid == false){
                       $('#y_cartValidationWebModal').modal('show');
                       $('#y_OriginTerminal').html(data.origin);
                       $('#y_ArrivalTerminal').html(data.destination);
                       $('#y_DepartureTime').html(data.departureTime);
                       $('#y_ArrivalTime').html(data.arrivalTime);
                       if(data.needConfirmation){
                       $(".y_continueBooking").removeClass('hidden');
                       }
                    }
                    else {
                       $('#y_agentPayNow').submit();
                    }
                }
              );
              }
          });

            $("#y_continueBookingBtn").click(function() {
                                     $('#y_agentPayNow').submit();
                          });



		$('.js-continue-shopping-button').click(function ()
		{
			var checkoutUrl = $(this).data("continueShoppingUrl");
			window.location = checkoutUrl;
		});
		
		$('.js-create-quote-button').click(function ()
		{
			$(this).prop("disabled", true);
			var createQuoteUrl = $(this).data("createQuoteUrl");
			window.location = createQuoteUrl;
		});

		
		$('.expressCheckoutButton').click(function()
				{
					document.getElementById("expressCheckoutCheckbox").checked = true;
		});
		
		$(document).on("input",".confirmGuestEmail,.guestEmail",function(){
			  
			  var originalEmail = $(".guestEmail").val();
			  var confirmationEmail = $(".confirmGuestEmail").val();
			  
			  if(originalEmail === confirmationEmail){
			    $(".guestCheckoutBtn").removeAttr("disabled");
			  }else{
			     $(".guestCheckoutBtn").attr("disabled","disabled");
			  }
		});

         $(document).ready(function() {

              var originalEmail = $(".guestEmail").val();
              if(typeof originalEmail != 'undefined' && originalEmail.length > 0){
                    $(".confirmGuestEmail,.guestEmail").trigger("input");
              }
        });

        $(document).ready(function() {
            $("input[type='radio']").click(function(){
                var paymentType = $("input[name='paymentType']:checked").val();
                if(paymentType == 'payByCash'){
                    $('#receiptNo').show();
                }
            });
        });
		
		$('.js-continue-checkout-button').click(function ()
		{
			var checkoutUrl = $(this).data("checkoutUrl");
			
			cartEntriesError = ACC.pickupinstore.validatePickupinStoreCartEntires();
			if (!cartEntriesError)
			{
				var expressCheckoutObject = $('.express-checkout-checkbox');
				if(expressCheckoutObject.is(":checked"))
				{
					window.location = expressCheckoutObject.data("expressCheckoutUrl");
				}
				else
				{
					var flow = $('#selectAltCheckoutFlow').val();
					if ( flow == undefined || flow == '' || flow == 'select-checkout')
					{
						// No alternate flow specified, fallback to default behaviour
						window.location = checkoutUrl;
					}
					else
					{
						// Fix multistep-pci flow
						if ('multistep-pci' == flow)
						{
						flow = 'multistep';
						}
						var pci = $('#selectPciOption').val();

						// Build up the redirect URL
						var redirectUrl = checkoutUrl + '/select-flow?flow=' + flow + '&pci=' + pci;
						window.location = redirectUrl;
					}
				}
			}
			return false;
		});

	}

};
