ACC.global = {

    _autoload: [
           ["passwordStrength", $('.password-strength').length > 0],
           "bindToggleOffcanvas",
           "bindToggleXsSearch",
           "bindHoverIntentMainNavigation",
           "initImager",
           "backToHome",
           "bindSignupFormValidation",
           "bookingSelectorActive",
           "mySubscriptionsActive"
       ],

       bindSignupFormValidation: function(){
           $("#BCFSignupForm").validate({
                 errorElement: "span",
                 errorClass: "fe-error",
                 onfocusout: function(element) {
                     $(element).valid();
                 },
                 onkeyup: false,
                 onclick: false
             });

             if($("#BCFSignupForm").find(".form-email").length>0){
            	 
            	 $.validator.addMethod("customemail", 
            			    function(value, element) {
            			        return /^(?![^#]*[#])/.test(value);
            			    }, 
            			    ACC.emailValidationMsg
            			); 
            	 
               $("#BCFSignupForm").find(".form-email").rules("add", {
                   required: true,
                   email:true,
                   messages: {
                       required: ACC.emailValidationMsg
                   },
                   customemail: true
               });
             }

             if($("#BCFSignupForm").find(".form-password").length>0){

               $.validator.addMethod("passwordPatternCheck", function(value, element) {

                  var password = $(element).val();

                  var ATLEAST_ONE_NUMBER_REGEX = new RegExp(/[0-9]/);
                  var ATLEAST_ONE_UPPERCASE_LETTER_REGEX = new RegExp(/[A-Z]/);
                  var ATLEAST_ONE_LOWERCASE_LETTER_REGEX = new RegExp(/[a-z]/);
                  var ATLEAST_ONE_SPECIAL_SYMBOL_REGEX = new RegExp(/[^A-Za-z0-9]/);


                  if (password.indexOf(" ")!==-1)
                        {
                            return false;
                        }

                        var count = 0;
                        if(password.match(ATLEAST_ONE_UPPERCASE_LETTER_REGEX))
                        {
                            count++;
                        }
                        if (password.match(ATLEAST_ONE_LOWERCASE_LETTER_REGEX))
                        {
                            count++;
                        }

                        if (password.match(ATLEAST_ONE_NUMBER_REGEX))
                        {
                            count++;
                        }

                        if (password.match(ATLEAST_ONE_SPECIAL_SYMBOL_REGEX))
                        {
                            count++;
                        }

                        if (count>2)
                        {
                            return true;
                        }
                  return false;
                }, "passwordPatternCheck");



                  $("#BCFSignupForm").find(".form-password").rules("add", {
                      required:true,
                      minlength:ACC.pwdStrengthMinChar,
                      maxlength:ACC.pwdStrengthMaxChar,
                      passwordPatternCheck:true,

                      messages: {
                          required: "Password not valid",
                          minlength: ACC.pwdMinCharValidationMsg,
                          maxlength: ACC.pwdMaxCharValidationMsg,
                          passwordPatternCheck: ACC.pwdPatternValidationMsg
                      }
                  });
                }


               if($("#BCFSignupForm").find(".form-checkPwd").length>0){
                   $("#BCFSignupForm").find(".form-checkPwd").rules("add", {
                       required: true,
                       equalTo:".form-password",
                       messages: {
                           required: ACC.reconfirmPasswordValidationMsg
                       }
                   });
                 }

       },
       passwordStrength: function () {
           $('.password-strength').pstrength({
               verdicts: [ACC.pwdStrengthTooShortPwd,
                   ACC.pwdStrengthVeryWeak,
                   ACC.pwdStrengthWeak,
                   ACC.pwdStrengthMedium,
                   ACC.pwdStrengthStrong,
                   ACC.pwdStrengthVeryStrong],
               minCharText: ACC.pwdStrengthMinCharText,
               minchar: ACC.pwdStrengthMinChar,
               maxChar: ACC.pwdStrengthMaxChar
           });
       },






    bindToggleOffcanvas: function () {
        $(document).on("click", ".js-toggle-sm-navigation", function () {
            ACC.global.toggleClassState($("main"), "offcanvas");
            ACC.global.toggleClassState($("html"), "offcanvas");
            ACC.global.toggleClassState($("body"), "offcanvas");
            ACC.global.resetXsSearch();
        });
    },

    bindToggleXsSearch: function () {
        $(document).on("click", ".js-toggle-xs-search", function () {
            ACC.global.toggleClassState($(".site-search"), "active");
            ACC.global.toggleClassState($(".js-mainHeader .navigation--middle"), "search-open");
        });
    },

    resetXsSearch: function () {
        $('.site-search').removeClass('active');
        $(".js-mainHeader .navigation--middle").removeClass("search-open");
    },

    toggleClassState: function ($e, c) {
        $e.hasClass(c) ? $e.removeClass(c) : $e.addClass(c);
        return $e.hasClass(c);
    },

    bindHoverIntentMainNavigation: function () {

        enquire.register("screen and (min-width:" + screenMdMin + ")", {

            match: function () {
                // on screens larger or equal screenMdMin (1024px) calculate position for .sub-navigation
                $(".js-enquire-has-sub").hoverIntent(function () {
                    var $this = $(this),
                        itemWidth = $this.width();
                    var $subNav = $this.find('.js_sub__navigation'),
                        subNavWidth = $subNav.outerWidth();
                    var $mainNav = $('.js_navigation--bottom'),
                        mainNavWidth = $mainNav.width();

                    console.log($subNav);

                    // get the left position for sub-navigation to be centered under each <li>
                    var leftPos = $this.position().left + itemWidth / 2 - subNavWidth / 2;
                    // get the top position for sub-navigation. this is usually the height of the <li> unless there is more than one row of <li>
                    var topPos = $this.position().top + $this.height();

                    if (leftPos > 0 && leftPos + subNavWidth < mainNavWidth) {
                        // .sub-navigation is within bounds of the .main-navigation
                        $subNav.css({
                            "left": leftPos,
                            "top": topPos,
                            "right": "auto"
                        });
                    } else if (leftPos < 0) {
                        // .suv-navigation can't be centered under the <li> because it would exceed the .main-navigation on the left side
                        $subNav.css({
                            "left": 0,
                            "top": topPos,
                            "right": "auto"
                        });
                    } else if (leftPos + subNavWidth > mainNavWidth) {
                        // .suv-navigation can't be centered under the <li> because it would exceed the .main-navigation on the right side
                        $subNav.css({
                            "right": 0,
                            "top": topPos,
                            "left": "auto"
                        });
                    }
                    $this.addClass("show-sub");
                }, function () {
                    $(this).removeClass("show-sub")
                });
            },

            unmatch: function () {
                // on screens smaller than screenMdMin (1024px) remove inline styles from .sub-navigation and remove hoverIntent
                $(".js_sub__navigation").removeAttr("style");
                $(".js-enquire-has-sub").hoverIntent(function () {
                    // unbinding hover
                });
            }

        });
    },

    initImager: function (elems) {
        elems = elems || '.js-responsive-image';
        this.imgr = new Imager(elems);
    },

    reprocessImages: function (elems) {
        elems = elems || '.js-responsive-image';
        if (this.imgr == undefined) {
            this.initImager(elems);
        } else {
            this.imgr.checkImagesNeedReplacing($(elems));
        }
    },

    // usage: ACC.global.addGoogleMapsApi("callback function"); // callback function name like "ACC.global.myfunction"
    addGoogleMapsApi: function (callback) {
        if (callback != undefined && $(".js-googleMapsApi").length == 0) {
            $('head').append('<script class="js-googleMapsApi" type="text/javascript" src="//maps.googleapis.com/maps/api/js?key=' + ACC.config.googleApiKey + '&sensor=false&callback=' + callback + '"></script>');
        } else if (callback != undefined) {
            eval(callback + "()");
        }
    },

    backToHome: function () {
        $(".backToHome").on("click", function () {
            var sUrl = ACC.config.contextPath;
            window.location = sUrl;
        });
    },

    bookingSelectorActive : function(){
        $('.booking-selector  input').click(function () {
            $('.booking-selector  input:not(:checked)').parent().removeClass("label-active");
            $('.booking-selector input:checked').parent().addClass("label-active");
        });
        $('.booking-selector input:checked').parent().addClass("label-active");
    },

    mySubscriptionsActive : function(){
        $('.my-subscriptions  input').click(function () {
            $('.my-subscriptions  input:not(:checked)').parent().removeClass("label-active");
            $('.my-subscriptions input:checked').parent().addClass("label-active");
        });
        $('.my-subscriptions input:checked').parent().addClass("label-active");
    }

};
