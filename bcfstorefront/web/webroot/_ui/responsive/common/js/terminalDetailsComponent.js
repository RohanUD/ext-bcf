ACC.terminaldetails = {
	_autoloadTracc: [
		"addGoogleMapsApi",

	],

    addGoogleMapsApi: function () {

		var callback = "ACC.terminaldetails.loadGoogleMap";
		if (callback != undefined && $(".js-googleMapsApi").length == 0) {
			$('head').append('<script async defer class="js-googleMapsApi" type="text/javascript" src="//maps.googleapis.com/maps/api/js?key=' + $('#terminalmap').data('googleapi') + '&callback=' + callback + '"></script>');
		} else if (callback != undefined) {
			eval(callback + "()");

		}
	},
	loadGoogleMap: function () {
		var map = new google.maps.Map(document.getElementById('terminalmap'), {
		    zoom: 2,
			styles: ACC.schedule.mapStyle

		});
		var listOfTravelRouteCount = $('#listOfTravelRouteCount').attr('value');
		var terminalCode = $('#terminalCode').attr('value');
		var terminalName = ("<b>" + $('#terminalName').attr('value') + "<b>").fontcolor("#002966");
		var processedFile = [];
		var checkDuplicateFile = [];
		var processedTerminal = [];
        var mapKmlFileMap = {};
		for (var i = 0; i < listOfTravelRouteCount; i++) {
           var routeOriginDestinationKey ="#routeOriginDestination"+i;
           var mapkmlfile=$(routeOriginDestinationKey).data("mapkmlfile");
           var mapkmlInternalfileName=$(routeOriginDestinationKey).data("mapkmlinternalfilename");
           if(mapkmlfile != undefined)
           {
                    var mapkmlInternalfileNameSeparator = mapkmlInternalfileName.split("-");
                    var duplicateMapkmlInternalfileName = mapkmlInternalfileNameSeparator[1] + "-" + mapkmlInternalfileNameSeparator[0];
                    if(!checkDuplicateFile.includes(duplicateMapkmlInternalfileName))
                    {
                    checkDuplicateFile.push(mapkmlInternalfileName);
					var kmlFilePathName = mapkmlfile;
					var lineSymbol = {
						path: 'M 0,-0.1 0,0.5',
						strokeOpacity: 1,
						scale: 4
					};

                    // Comment the below line, if you are testing in local env
                    var absoluteFileName = mapkmlfile;

                    // And Uncomment the below line to work in local env.
                    //var absoluteFileName = mapkmlfile.substring(0, mapkmlfile.indexOf("?"));
					mapKmlFileMap[absoluteFileName] = mapkmlInternalfileName;

                    var listenerDebounce;
                    var bounds = new google.maps.LatLngBounds();
					var createMarker = function (placemark, doc) {
						var travelRoute = mapKmlFileMap[placemark.styleBaseUrl];
						var sameFileCount =1;
						if(!processedFile.includes(travelRoute)){
						    processedFile.push(travelRoute);
						}else{
						    sameFileCount =2;
						}
						var travelRouteOriginAndDestinationSeparator = travelRoute.split("-");
						var travelRouteOrigin = travelRouteOriginAndDestinationSeparator[0];
						var travelRouteDestination = travelRouteOriginAndDestinationSeparator[1];
						var isIncluded = true;
						var infoWindowContent;
						if (!processedTerminal.includes(travelRouteOrigin) && sameFileCount == 1 && travelRouteOrigin ==
						 terminalCode && !processedTerminal.includes(terminalCode)) {
							processedTerminal.push(travelRouteOrigin);
							infoWindowContent = terminalName;
							isIncluded = false;
						}else if(!processedTerminal.includes(travelRouteDestination) && sameFileCount == 2 &&
						travelRouteDestination == terminalCode && !processedTerminal.includes(terminalCode)){
						    processedTerminal.push(travelRouteDestination);
						    infoWindowContent = terminalName;
                            isIncluded = false;
						}
						var marker=null;
						if (!isIncluded) {
							var markerOptions = geoXML3.combineOptions(geoXml.options.markerOptions, {
								map: geoXml.options.map,
								position: new google.maps.LatLng(placemark.Point.coordinates[0].lat, placemark.Point.coordinates[0].lng)
							});

							// Create the marker on the map
							marker = new google.maps.Marker(markerOptions);

							// Set up and create the infowindow if it is not suppressed
							if (!geoXml.options.suppressInfoWindows && !isIncluded) {
								var infoWindowOptions = geoXML3.combineOptions(geoXml.options.infoWindowOptions, {
									content: infoWindowContent
								});
								if (geoXml.options.infoWindow) {
									marker.infoWindow = geoXml.options.infoWindow;
								} else {
									marker.infoWindow = new google.maps.InfoWindow(infoWindowOptions);
								}
								marker.infoWindowOptions = infoWindowOptions;
								bounds.extend(marker.position);
                                map.fitBounds(bounds);
                                var listener = google.maps.event.addListener(map, "idle", function () {
                                    map.setZoom(9);
                                    google.maps.event.removeListener(listener);
                                });
								// Infowindow-opening event handler
								google.maps.event.addListener(marker, 'click', function () {
									//								this.infoWindow.close();
									marker.infoWindow.setOptions(this.infoWindowOptions);
									this.infoWindow.open(this.map, this);

								});
								google.maps.event.addListener(map, 'click', function (event) {
									marker.infoWindow.close();
								});
							}
						    placemark.marker = marker;
						}
						return marker;
					};

					var geoXml = new geoXML3.parser({
                        map: map,
						singleInfoWindow: true,
						createMarker: createMarker,
						markerOptions: {
							icon: {
								url: 'https://snazzy-maps-cdn.azureedge.net/assets/marker-c5ce3bc4-d268-4b8a-9941-285dc369ed54.png',
								scaledSize: new google.maps.Size(
									24,
									24),
								size: new google.maps.Size(
									24,
									24),
								anchor: new google.maps.Point(
									12,
									24)
							}
						},
						polylineOptions: {
							strokeOpacity: 0,
							strokeColor: '#0079a6',
							icons: [{
								icon: lineSymbol,
								offset: '0',
								repeat: '10px'
                            }],
                            clickable:false,
                            strokeWeight:4
						},
					});
					geoXml.parse(kmlFilePathName);

          }
          }
        }
	}
}

    $(document).ready(function () {
		$(".y_findTerminalResults").click(function () {
			var value = $("#transportFacilitySelect").val();
			if (value != 'default') {
				var vehicleName = $("#transportFacilitySelect").find(":selected").attr("data-defaultvalue");
				window.location.href = ACC.config.contextPath + "/travel-boarding/terminal-directions-parking-food/" + vehicleName + "/" + value;
			}
        });
    });
