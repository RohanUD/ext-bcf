ACC.travelbookinglisting = {

    _autoloadTracc: [
        "bindTravelBookingListingShowMore",
        "bindTravelBookingListing"
    ],
    bindTravelBookingListingShowMore: function() {
        $(".y_showSpecificVacationsPageResults").on("click", function() {

            var pageNumber = $(this).data('pagenumber');
            if (pageNumber <= 0) {
                $(".y_showPreviousVacationsPageResults").attr("disabled", "disabled");
            } else {
                $(".y_showPreviousVacationsPageResults").removeAttr("disabled");
            }

            pageNumber = pageNumber + 1;
            ACC.travelbookinglisting.getShowMoreResults(pageNumber);
            ACC.travelcommon.bindEqualHeights();
        });
    },
    getShowMoreResults: function(pageNumber) {
        var isUpcoming = $('li.ui-state-active').find('a.y_vacationsList').data('upcoming');

        if(pageNumber > 0){
        $.when(ACC.services.getTravelBookingListing(pageNumber,isUpcoming)).then(
            function(data) {
                if (data.hasMoreResults) {
                    $(".y_showNextVacationsPageResults").removeAttr("disabled");
                }
                if($(".y_showNextVacationsPageResults").attr("disabled")!="disabled"){
                    $(".vacation_details_placeholder").html(data.htmlContent);
                    $(".y_showPageCount").html(pageNumber);
                    $(".y_shownResultId").html(data.totalShownResults);
                    $(".y_showSpecificVacationsPageResults.next").data('pagenumber', pageNumber);
                    $(".y_showSpecificVacationsPageResults.prev").data("pagenumber", (pageNumber - 2));
                   }

                if(!data.hasMoreResults) {
                    $(".y_showNextVacationsPageResults").attr("disabled", "disabled");
                }
            });}
        ACC.global.initImager();
        return false;
    },
    bindTravelBookingListing: function() {
        $(".y_vacationsList").on("click", function() {

            var isUpcoming = $(this).data('upcoming');
            ACC.travelbookinglisting.replaceResults(isUpcoming);
            ACC.travelcommon.bindEqualHeights();
        });
    },
    replaceResults: function(isUpcoming) {
               $.when(ACC.services.getTravelBookingUpcomingList(isUpcoming)).then(
                   function(data) {
                       $(".vacation_details_placeholder").html(data.htmlContent);
                   });
               return false;
           }
}
