ACC.getaquote = {
	_autoloadTracc: [
		"checkInDatePicker",
		"numericSelectorEventBinder",
		"bindGetAQuoteListingShowMore",
		"bindGetAQuoteListingChangeStatus"
	],
	  checkInDatePicker:function(){
          $("#y_checkIn").datepicker();
         },
       numericSelectorEventBinder : function() {
        $('.numericSelectorHandler').click(
            function(e) {
                e.preventDefault();
                var ele = $(this).closest(".numericSelector").find(".numericSelectorInput");
                var currentValue = parseInt(ele.val());
                var maxAllowed =  3;

                    if($(this).hasClass("numericSelectorPlus"))
                        currentValue += 1;
                     else if($(this).hasClass("numericSelectorMinus"))
                        currentValue -= 1;

                     if(currentValue<1)
                        currentValue =1;

                    if (currentValue <= maxAllowed) {
                        $(ele).val(currentValue);
                    }
            });
       	},
       	bindGetAQuoteListingShowMore: function() {
                  $(".y_showSpecificQuotePageResults").on("click", function() {

                      var pageNumber = $(this).data('pagenumber');
                      if (pageNumber == 0) {
                          $(".y_showPreviousQuotePageResults").attr("disabled", "disabled");
                      } else {
                          $(".y_showPreviousQuotePageResults").removeAttr("disabled");
                      }
                      pageNumber = pageNumber + 1;
                      ACC.getaquote.getShowMoreResults(pageNumber);
                      ACC.travelcommon.bindEqualHeights();
                  });
              },
              getShowMoreResults: function(pageNumber) {
                  $.when(ACC.services.getAQuoteListing(pageNumber)).then(
                      function(data) {
                          if (data.hasMoreResults) {
                              $(".y_showNextQuotePageResults").removeAttr("disabled");
                          }
                          if($(".y_showNextQuotePageResults").attr("disabled")!="disabled"){
                              $(".quote_details_placeholder").html(data.htmlContent);
                              $(".y_showPageCount").html(pageNumber);
                              $(".y_shownResultId").html(data.totalShownResults);
                              $(".y_showSpecificQuotePageResults.next").data('pagenumber', pageNumber);
                              $(".y_showSpecificQuotePageResults.prev").data("pagenumber", (pageNumber - 2));
                             }
                           if (!data.hasMoreResults) {
                               $(".y_showNextQuotePageResults").attr("disabled", "disabled");
                           }
                      });
                  return false;
              },
            bindGetAQuoteListingChangeStatus: function() {
                    $(".y_changeStatusResult").on("click", function() {
                        var status = $(this).data('status');
                        var code = $(this).data('code');
                        ACC.getaquote.getChangeStatusResults(status,code);
                        ACC.travelcommon.bindEqualHeights();
                    });
                },
           getChangeStatusResults: function(status,code) {
               $.when(ACC.services.getAQuoteListingStatus(status,code)).then(
               function(data){
                           location.reload();}
                   );
               return false;
           }
}
    $( ".getAQuoteMoreInfo" ).on( "click", function() {
     var ele = $(this).siblings(".dialog-Message");
      var sel = $(this).data("dialog");
      if(sel==undefined){
          var sel = ele.dialog();
          ele.removeClass("hidden");
          $(this).data("dialog",sel);
       }else{
        sel.dialog( "open" );
       }
    });
