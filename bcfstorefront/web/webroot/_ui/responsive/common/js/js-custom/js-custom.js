$( document ).ready(function() {
	/////// Function to control the active underline status of the parent nav items ////////////////////////////////////////////
	$(".navigator a").on("click", function(event){
		event.preventDefault();
		
		var target = $(this);
		var path = target.attr("href");
		alert(path);
		var parentMenu = $(this).parents('ul.dropdown-menu.mega-dropdown-menu').attr('data-panel');
		
		setInStorage("fromPage", parentMenu);
		window.location.href = path;
	});

	function checkForSpecialNavigation(){
		var fromPage = getFromStorage("fromPage");
		
		if(fromPage){
			$('li.' + fromPage).addClass('underline active');
		}
		removeFromStorage('fromPage');
	};
	
	function setInStorage(key, value){
		 if (typeof (Storage) !== 'undefined') {
			 localStorage.setItem(key, value);
		 }else{
			 return false;
		 }
	};
	
	function getFromStorage(key){
		if (typeof (Storage) !== 'undefined') {
			 return localStorage.getItem(key);
		 }else{
			 return false;
		 }
	};
	
	function removeFromStorage(key){
		if (typeof (Storage) !== 'undefined') {
			 localStorage.removeItem(key);
		 }else{
			 return false;
		 }
	};
	
	checkForSpecialNavigation();
	/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	
	
	$(".page-dealsAndOffersDetailsPage  .landing-row").addClass("landing-row-margin");
	$(".page-beachDestinationDealsPage .more-packages-modal .travelforms").removeClass("container");
	$(".page-beachDestinationDealsPage .more-packages-modal .bc-accordion").removeClass("col-md-4 col-lg-4 col-sm-4 pr-0");
	$(".page-beachDestinationDealsPage .more-packages-modal .bc-accordion").addClass("col-md-12 col-lg-12 col-sm-12");
	$(".page-beachDestinationDealsPage .more-packages-modal #tabs-2").addClass("padding-10");
	$(".page-beachDestinationDealsPage .more-packages-modal .vacation-calender-section").removeClass("col-md-4 col-lg-4 col-sm-4 pr-0");
	$(".page-beachDestinationDealsPage .more-packages-modal .vacation-calender-section").addClass("col-md-12 col-lg-12 col-sm-12");
	$(".page-beachDestinationDealsPage .more-packages-modal .rooms-wrapper").removeClass("col-md-2 col-lg-2 col-sm-2");
	$(".page-beachDestinationDealsPage .more-packages-modal .rooms-wrapper").addClass("col-md-12 col-lg-12 col-sm-12");
	$(".page-beachDestinationDealsPage .more-packages-modal .modal-header").addClass("border-none");
	$(".page-vacationsListingPage .js-list-box-width ").removeClass("col-lg-4 col-md-4 col-sm-4 ");
	$(".page-ccDeparturesPage #tabs-4").removeClass("departures-cc-component ");
	$(".page-vacationsListingPage .js-list-box-width ").addClass("col-lg-6 col-md-6 col-sm-6 ");
	$(".page-currentconditionsdetailspage .promotional-content").closest("section").addClass("bg-light-gray margin-bottom-30 margin-top-30");
	$(".destination-landing-link").closest("section").addClass("destination-landing-link-box");
	$(".bg-full").closest("section").css("background-color", "#eee");
	$(".bg-full").closest("section").css("margin-top", "20px");
	$(".pageLabel--destination-city-details").find(".bg-Packages").closest("section").addClass("bg-Packages-css");
	$( ".orbeon .row" ).wrap( "<div class='container'></div>" );

	$('.flow-tab--heading').find('li').find('a').each(function(){
		$(this).parent().addClass("text-center");
	});

	// Main Code
	$(".js-sailing-link a").click(function(e){
	    sailingNorthernWidgetState($(this));
	    if($(this).siblings("a.in").length>0){
	    $(this).siblings("a.in").each(function(){
	        sailingNorthernWidgetState($(this));
	    })
	    }
	    e.preventDefault();
	})

	window.sailingNorthernWidgetState = function(ele){
	    var href = ele.attr("href");
	    ele.closest(".js-collapse-parent").find(href).toggleClass( "in" );
	    ele.toggleClass( "in" );
	    ele.find("i").toggleClass( "fa-angle-up fa-angle-down" );
	}
	$("#addRow").click(function() {
		$(".error").addClass("hidden");
	});

	$(".travel-advisory-mainbox .js-heading-travel").click(function(){
		  var parent = $(this).closest(".travel-advisory-mainbox");
		  parent.find(".js-travel-advisory-box").slideToggle();

		  parent.find(".js-travel-advisory-show").toggleClass("hidden");
		  parent.find(".js-travel-advisory-hide").toggleClass("hidden");
	  });
	    $(".js-travel-advisory-close").click(function(){
	    $(this).closest(".travel-advisory-mainbox").addClass("hidden");

	  });

    $(".travel-advisory-mainbox.true-defaultopen").each(function(){
    	$(this).find(".js-travel-advisory-box").slideToggle();

    	$(this).find(".js-travel-advisory-show").toggleClass("hidden");
    	$(this).find(".js-travel-advisory-hide").toggleClass("hidden");
    })

});

$(document).ready(function(){
var isVisible = false;
var clickedAway = false;

$('.popoverThis').popover({
 html: true,
 trigger: 'manual',
title:"<i class='fas fa-times close' ></i>"
}).click(function (e) {
 $(this).popover('show');
 clickedAway = true
 isVisible = true
 e.preventDefault()
});

$(document).on("click", ".close", function() {
 if (isVisible && clickedAway) {
     $('.popoverThis').popover('hide')
     isVisible = clickedAway = false
 } else {
     clickedAway = true
 }
});
$('body').on('click', function (e) {
		if ($(e.target).data('toggle') !== 'popover'
            && $(e.target).parents('[data-toggle="popover"]').length === 0
            && $(e.target).parents('.popover.in').length === 0) {
            $('[data-toggle="popover"]').popover('hide');
        }
	});
});
	
////////// JS function to slide the viewport to the position of the clicked element //////////// 
  	
  	$('.component-to-top').on('click', function(e) {
  	    var target = $(this);
  	    $('html, body').animate({
  	        scrollTop: target.offset().top
  	    }, 500);
  	});
  	

  	
  	$('#y_roundTripRadbtn').on('click', function(e) {
  		$('.bc-dropdown').addClass('dual-tab');
  	});
  	$('#y_oneWayRadbtn').on('click', function(e) {
  		$('.bc-dropdown').removeClass('dual-tab');
  	});
////////////////////////////////////////////////////////////////////
$(document).ready(function() {
$( function() {
    $( ".account-profile-accordion" ).accordion({
        active: false,
        collapsible: true,
        heightStyle: "content"
    });
  } );

$('select[name=salesChannel]').on('change', function() {
     if($(this).val() == 'CallCenter'){

      $("#_asmLogin .pinPadID").hide();
       $("#_asmLogin .iceBarID").show();
		 //$("#location").hide();
    } else {
       $("#_asmLogin .pinPadID").show();
       $("#_asmLogin .iceBarID").show();
    }
  });

function toggleIconChange(ele){
	if(ele.find(".fa-angle-down").length >0){
		ele.find(".fa-angle-down").addClass("fa-angle-up");
		ele.find(".fa-angle-down").removeClass("fa-angle-down");
	}else{
		ele.find(".fa-angle-up").addClass("fa-angle-down");
		ele.find(".fa-angle-up").removeClass("fa-angle-up");

	}
}

$(".view-deck").click(function () {
	$(this).siblings(".view-deck-div").toggle();
	toggleIconChange($(this))

});
$(".toggle-arrow").click(function () {
	$(this).closest("tr").next(".toggle-div").toggle();
	toggleIconChange($(this))

});

$(".current-condition-sailing-detail").find("#SailingDetails").change(function(){
	sailingDetailUIrender();

})

function sailingDetailUIrender(){
	var ele = $(".current-condition-sailing-detail").find("#SailingDetails");
	var selOpt = ele.val();
	ele.siblings(".view-deck-wrapper").find(".view-deck-full-box").addClass("d-none");
	ele.siblings(".view-deck-wrapper").find(".box-"+selOpt).removeClass("d-none");
}
sailingDetailUIrender();

$(".webcam-h").click(function () {
	if(!$(this).closest('.webcam-main').find('.webcam-media').hasClass('d-none'))
	{
		$(this).closest('.webcam-main').find('.webcam-media').addClass('d-none');
		$(this).find('i').addClass('fa-angle-down');
		$(this).find('i').removeClass('fa-angle-up');
	}
	else
	{
		$('.webcam-media').addClass('d-none');
		$('.webcam-h').find('i').removeClass('fa-angle-up');
		$('.webcam-h').find('i').addClass('fa-angle-down');

		$(this).find('i').removeClass('fa-angle-down');
		$(this).find('i').addClass('fa-angle-up');
		$(this).closest('.webcam-main').find('.webcam-media').removeClass('d-none');
	}
});

$('.webcam-main:first').find('i').removeClass('fa-angle-down');
$('.webcam-main:first').find('i').addClass('fa-angle-up');
$('.webcam-main:first').closest('.webcam-main').find('.webcam-media').removeClass('d-none');
					$("#close_sidebar1").click(function() {
						$("#wrapper").removeClass("toggled");
					});
					$("#close_sidebar2").click(function() {
						$("#wrapper2").removeClass("toggled");
					});
					$("#menu-toggle1 ").click(function(e) {
						e.preventDefault();
						$("#wrapper").toggleClass("toggled");
						$("#wrapper2").removeClass("toggled");
					});
					$("#menu-toggle2 ").click(function(e) {
						e.preventDefault();
						$("#wrapper2").toggleClass("toggled");
						$("#wrapper").removeClass("toggled");
					});


					var index = 1;
					$("#addRow")
							.click(
									function() {
										$("#maxEmailAllowed").val()
										if (index < $("#maxEmailAllowed").val()) {
											$("#container")
													.append(
															'<div class="row"><div class="col-sm-10 offset-1""><div class="col-md-4 p-0 js_holder"><div><input name="'
																	+ 'emails'
																	+ index
																	+ '" type="email" class="form-control email-field" id="'
																	+ 'emails'
																	+ index
																	+ '"  placeholder="Enter your Email" path="emails['
																	+ index
																	+ ']" /><span class="error" id="'
																	+ 'e_emails'
																	+ index
																	+ '"  style="color:red"></span></div><br></div></div></div>');
											index++;
										} else {
											$("#addRow").attr("disabled", true);
										}
									});

					// document ready
				});

$(function() {
	$("#accordion2").accordion();
	$("#accordion2").accordion({
		header : "h4",
		collapsible : true,
		active : false
	});
	$(".accordion").accordion();
	$(".accordion").accordion({
		header : "h4",
		collapsible : true,
		active : false
	});
});

$(function() {
	$("#accordion").accordion();
	$("#accordion").accordion({
		header : "h4",
		collapsible : true,
		active : false
	});
});
$(function() {
	$("#accordion3").accordion();
	$("#accordion3").accordion({
		header : "h4",
		collapsible : true,
		active : false
	});
});
$(function() {
	$("#accordion4").accordion();
	$("#accordion4").accordion({
		header : "h4",
		collapsible : true,
		active : false
	});
});

$(function() {
	$("#booking-detail-page-accordion").accordion();
	$("#booking-detail-page-accordion").accordion({
		header : "h4",
		collapsible : true,
		heightStyle: "content"
	});
});

  $( function() {
    $( "#accordion-filter" ).accordion({
      collapsible: true
    });
  } );

$("#sidebar-close").click(function() {
	$("#wrapper").removeClass("toggled");
	$("#wrapper2").removeClass("toggled");
});


// plugin bootstrap minus and plus
// http://jsfiddle.net/laelitenetwork/puJ6G/
$(document).ready(function() {$(document).mouseup(function(e){
                      var container = $(".pageLabel-dealActivitySelection").find(".calender-box");
                      // if the target of the click isn't the container nor a descendant of the container
                      if (!container.is(e.target) && container.has(e.target).length === 0)
                      {
                          $(".activity-tab-content").removeClass("active");
                          $(".activity-calender").find(".tab-links.active").removeClass("active");
                      }
                  });
					$(document).on("click",'.btn-number',function(e) {
						e.preventDefault();
						var fieldName = $(this).attr('data-field');
						var type = $(this).attr('data-type');
						var input = $("input[name='" + fieldName + "']");
						var currentVal = parseInt(input.val());
						if (!isNaN(currentVal)) {
							if (type == 'minus') {
								var minValue = parseInt(input.attr('min'));
								if (minValue === "" || isNaN(Number(minValue)))
									minValue = 1;
								if (currentVal > minValue) {
									input.val(currentVal - 1).change();
								}
								if (parseInt(input.val()) == minValue) {
									$(this).attr('disabled', true);
								}

							} else if (type == 'plus') {
								var maxValue = parseInt(input.attr('max'));
								if (maxValue === "" || isNaN(Number(maxValue)))
									maxValue = 9999999999999;
								if (currentVal < maxValue) {
									input.val(currentVal + 1).change();
								}
								if (parseInt(input.val()) == maxValue) {
									$(this).attr('disabled', true);
								}

							}
						} else {
							input.val(0);
						}
					});
					$('.input-number').focusin(function() {
						$(this).data('oldValue', $(this).val());
					});
					$('.input-number')
							.change(
									function() {

										var minValue = parseInt($(this).attr(
												'min'));
										var maxValue = parseInt($(this).attr(
												'max'));
										if (isNaN(minValue))
											minValue = 1;
										if (isNaN(maxValue))
											maxValue = 6;
										var valueCurrent = parseInt($(this)
												.val());

										var name = $(this).attr('name');
										if (valueCurrent >= minValue) {
											$(
													".btn-number[data-type='minus'][data-field='"
															+ name + "']")
													.removeAttr('disabled')
										} else {
											console.log('Sorry, the minimum value was reached');
											$(this).val(
													$(this).data('oldValue'));
										}
										if (valueCurrent <= maxValue) {
											$(
													".btn-number[data-type='plus'][data-field='"
															+ name + "']")
													.removeAttr('disabled')
										} else {
											console.log('Sorry, the maximum value was reached');
											$(this).val(
													$(this).data('oldValue'));
										}

									});
					$(".input-number")
							.keydown(
									function(e) {
										// Allow: backspace, delete, tab,
										// escape, enter and .
										if ($.inArray(e.keyCode, [ 46, 8, 9,
												27, 13, 190 ]) !== -1
												||
												// Allow: Ctrl+A
												(e.keyCode == 65 && e.ctrlKey === true)
												||
												// Allow: home, end, left, right
												(e.keyCode >= 35 && e.keyCode <= 39)) {
											// let it happen, don't do anything
											return;
										}
										// Ensure that it is a number and stop
										// the keypress
										if ((e.shiftKey || (e.keyCode < 48 || e.keyCode > 57))
												&& (e.keyCode < 96 || e.keyCode > 105)) {
											e.preventDefault();
										}
									});
				});

function send_mail() {
	$('.error').html('');
	// var email = document.getElementById('email').value.trim()
	var regex_email = /^([\w-]+(?:\.[\w-]+)*)@((?:[\w-]+\.)*\w[\w-]{0,66})\.([a-z]{2,6}(?:\.[a-z]{2})?)$/i;
	var flag = true;

	$('input.email-field')
			.each(
					function() {
						var email = $(this).val();
						var id = 'e_' + $(this).attr('id');
						var error_id = 'e_' + $(this).attr('id');

						if (email != "" && !regex_email.test(email)) {
							$('#' + error_id)
									.html(
											ACC.addons.bcfstorefrontaddon['checkout.error.email.invalid']);
							document.getElementById(id).focus();
							flag = false;
						}
					});

	if (!flag) {
		return false;
	} else {
		return true;
	}
	return false;
}

var filterbtn = true;
 $(".filter-btn").click(function(){
    if(filterbtn){
        $(this).siblings("ul").toggleClass("show");
        $(this).find(".caret").toggleClass("up-arrow");
        filterbtn = false;
        setTimeout(function(){ filterbtn=true; },100)
    }

 });

$(document).ready(function () {
    $(".package-content").hide();
    $(".show_hide").on("click", function () {
        var txt = $(".package-content").is(':visible') ? 'More Details' : 'Less Details';
        $(this).parent().toggleClass('rev');
        $(".show_hide").text(txt);
        $(this).closest('.col-12').find('.package-content').slideToggle(200);
    });

    //carousel bottom margin adjustment

    $( window ).resize(function() {
    	ACC.carousel.adjustSearchHeight();
    	}).load(function(){
    		setTimeout(function(){
    			ACC.carousel.adjustSearchHeight();
    		},500)

    	});


});



/**********************************mega-menu**************************/
$(document).ready(function(){
    $('.navbar-toggle').click(function(){
    var hidden = $('.navbar-collapse');
    if (hidden.hasClass('visible')){
        //hidden.animate({"left":"-1000px"}, "slow").removeClass('visible');

    } else {
		hidden.animate({"left":"0px"}, "slow").addClass('visible');
		$('body').append("<div class='nav-overlay'></div>");
		$('.navbar.navbar-default').addClass('navbar-minify-tablet');
		$('.nav-overlay').on('click', function(){
			$('.close-menu').trigger("click");
		});
		$(".navbar-toggle[data-toggle='collapse']").hide();
		$(".close-menu").show();
    }
    });
	 $('.close-menu').on('click', function(){
		var hidden = $('.navbar-collapse');
		hidden.animate({"left":"-1200px"}, "slow",function(){
			hidden.removeClass("show");
			hidden.addClass("collapse");
			hidden.removeClass("in");
		}).removeClass('visible');
		$('body').find(".nav-overlay").remove();
		$('.navbar.navbar-default').removeClass('navbar-minify-tablet');
		$(".navbar-toggle[data-toggle='collapse']").show();
		$(".close-menu").hide();
	 });

	 $('.mob-navigation li a').on('click',function(){
		 if ($( window ).width()<1200){
		 	var li=$(this).closest('li');
			setTimeout(function(){
				$('.mob-navigation').find('i').each(function(){
					 if($(this).hasClass('bcf-icon-up-arrow ')){
						//$(this).closest('li').removeClass('active'); bcf-icon-up-arrow bcf-icon-down-arrow 
						$(this).removeClass('bcf-icon-up-arrow ');
						$(this).addClass('bcf-icon-down-arrow');
					 }
				 });

				if(li.hasClass('open')){
					//li.addClass('active');
					li.find('i').removeClass('bcf-icon-down-arrow ');
					li.find('i').addClass('bcf-icon-up-arrow ');
					li.find('.navigation-mob-link-n i').removeClass('bcf-icon-up-arrow ');
				}else{
					//li.removeClass('active');
					li.find('i').removeClass('bcf-icon-up-arrow ');
					li.find('i').addClass('bcf-icon-down-arrow ');
					li.find('.navigation-mob-link-n i').addClass('bcf-icon-up-arrow ');
				}
			});
		 }
		});






});
/**********************************mega-menu-end**************************/

/**********************************Accommodations Filter by start**************************/
$(document).ready(function(){

$('.package-list-checkbox').each(function(){
	if($(this).find('.y_packageListingFacetCheckbox:checkbox:checked').length)
    {
       $('.filter-clear-all-btn').removeClass('hidden');
       $(this).parent().find('.totalCheckboxesChecked').html('('+$(this).find('.y_packageListingFacetCheckbox:checkbox:checked').length+')');
    }

});
});
/**********************************Accommodations Filter by end**************************/
$('body').on('click','.select-room',function(){
	$('.nav-tabs a[href="#tab-02"]').tab('show');
	$('.tab-pane').removeClass('active');
	$('#tab-02').addClass('active in');
	$('html, body').animate({
        scrollTop: $('#tab-02').offset().top
    }, 500);

});

function datepickerInit(ele,dateFormat,callback,beforeShowFunction){
    if(dateFormat == undefined){
        dateFormat = 'dd/mm/yy';
    }
	ele.datepicker({
	minDate: new Date(),
	maxDate: '+1y',
	dateFormat:dateFormat,
  beforeShowDay: function(date){

		var pickerWidth = $(".schedules-wrapper").find(".vacation-ui-depart").outerWidth();
		$("#js-depart").css("width", pickerWidth);

    if(beforeShowFunction == undefined){
      return [true];
    }else{
      return window[beforeShowFunction](date,$(this));
    }

  },
	onSelect: function (selectedDate) {
		var ele = $(this).siblings(".datepicker-input");
		$(ele).val(selectedDate);
		setDateToTitle($(this).closest(".tab-content").parent(),selectedDate);
		var autoclose = $(this).data("autoclose");
		if(autoclose !== undefined && (autoclose == "true" || autoclose == true)){
		    $(this).closest(".tab-pane").removeClass("active");
            $(this).closest(".tab-content").siblings("ul.nav-tabs").find(".tab-links").removeClass("active");
		}
		if(callback != undefined){
            callback(selectedDate)
        }
		}
	});

	$(ele).siblings(".datepicker-input").keyup(function(){
		var dt = $(this).val();
		ele.datepicker("setDate",dt);
		setDateToTitle($(ele).closest(".tab-content").parent(),dt);
	})
}



function setDateToTitle(ele,selectedDate){
	var parentEle = $(ele).find(".vacation-calen-box");
	parentEle.find(".vacation-calen-year-txt").text(selectedDate);
	$(".vacation-calen-box").addClass("select-color-change");
}

function package_room_ui(ele,noOfRoomsMax){
	var noRoom = ele.val();

	for (var i = 1; i <= noRoom; i++) {
		$(".custom-room-"+i).removeClass("hidden");
	}
	for (var j=i; j <= noOfRoomsMax; j++) {
		$(".custom-room-"+j).addClass("hidden");
	}
}

/* Custom Panel toggle */
	$('body').on('click',".js-custom-panel-heading",function(){
			$(this).siblings(".js-custom-panel-content").slideToggle(function(){
				if($(this).is(":visible")){
					$(this).siblings(".js-custom-panel-heading").find("i").removeClass("fa-angle-right");
					$(this).siblings(".js-custom-panel-heading").find("i").addClass("fa-angle-down");
				}else{
					$(this).siblings(".js-custom-panel-heading").find("i").removeClass("fa-angle-down");
					$(this).siblings(".js-custom-panel-heading").find("i").addClass("fa-angle-right");

				}
			});
	})

  function setScheduleDate(date){
    var urlSample = $("#urlSample").val();
	  urlSample = urlSample.split("=");
	  urlSample.pop();
	  urlSample = urlSample.join("=");
	  window.location.href = urlSample + "=" + date;
  }

  function initUISechdule(){
    if($("#datepickerSchedule").length == 0){
      return;
    }
    if($("#scheduleRouteType").length ==0){
      $("body").append("<input type='hidden' id='scheduleRouteType' value='SHORT'/>");
    }
    datepickerInit($("#datepickerSchedule"),"mm/dd/yy",function(date){
      setScheduleDate(date);
    },"scheduleValidateDate");

    var url = new URL(window.location.href);
    var sDt = url.searchParams.get("scheduleDate");
    var departureLocation = url.searchParams.get("departureLocation");
    var arrivalLocation = url.searchParams.get("arrivalLocation");
    if(!sDt){
      sDt = $.datepicker.formatDate("mm/dd/yy", new Date());
		}
      $("#datepickerSchedule").datepicker("setDate",sDt)
      $("#datepickerSchedule").siblings(".datepicker-input").val(sDt);
      $(".vacation-calender").find(".vacation-calen-year-txt").html(sDt);

    if(departureLocation && arrivalLocation){
      $("body").bind("routeInfoSet",function(){
        setCalenderUiSchedule(departureLocation,arrivalLocation);
      });
    }

    $(document).mouseup(function(e)
    {
        var container = $(".schedules-calender");

        // if the target of the click isn't the container nor a descendant of the container
        if (!container.is(e.target) && container.has(e.target).length === 0)
        {
            $(".schedules-calender").find(".tab-links.active").removeClass("active");
            $(".schedules-calender").find(".tab-pane.active").removeClass("active");
        }
    });

  }

  function setCalenderUiSchedule(departureLocation,arrivalLocation){
    var routeType="";
    _.forEach(ACC.farefinder.allRouteInfo,function(val){
      if(val.code==departureLocation){
        _.forEach(val.destinationRoutes,function(dval){
          if(dval.code == arrivalLocation){
            routeType = dval.routeType;
          }
        });
      }
    })

    if(routeType!=""){
      $("#scheduleRouteType").val(routeType);
      $("#datepickerSchedule").datepicker('refresh');
    }

  }

  function scheduleValidateDate(date,ele){
    var url = new URL(window.location.href);
    var departureLocation = url.searchParams.get("departureLocation");
    var arrivalLocation = url.searchParams.get("arrivalLocation");

    if($("#scheduleRouteType").val() == "LONG"){
      return ACC.farefinder.calenderBlockDate(date,ele,departureLocation,arrivalLocation)
    }else{
      return [true];
    }
  }

  function scheduleLandingValidateDate(date,ele){
    var url = new URL(window.location.href);
    var departureLocation = $("#departureLocation").val();
    var arrivalLocation = $("#arrivalLocation").val();


    if($("#routeType").val() == "LONG"){
      return ACC.farefinder.calenderBlockDate(date,ele,departureLocation,arrivalLocation)
    }else{
      return [true];
    }
  }


	$(document).ready(function(){
    initUISechdule();
    datepickerInit($("#datepicker-schedule"),"mm/dd/yy",function(){},"scheduleLandingValidateDate");
	$("#y_fareFinderForm").find(".js-accordion-first-active").accordion({
        active:0,
        header : "h4",
        collapsible : true,
        heightStyle: "content"
	});
		$(".homepage-search-box").removeClass("hidden");
		$("#y_editPackageForm").find(".custom-accordion").accordion({
			header : "h3",
			collapsible : true,
			active : false,
			activate: function(event, ui) {
				if(ui.newHeader.length === 0){
					ACC.travelfinder.renderAccordionState($(this),"close");
				}else{
					ACC.travelfinder.renderAccordionState($(this),"open");
				}
			}

		});

		$(".yCmsContentSlot").find(".custom-accordion").accordion({
			header : "h3",
			collapsible : true,
			active : false,
			activate: function(event, ui) {
				if(ui.newHeader.length === 0){
					ACC.travelfinder.renderAccordionState($(this),"close");
				}else{
					ACC.travelfinder.renderAccordionState($(this),"open");
				}
			}

		});

		package_room_ui($("#y_editPackageForm").find("#y_roomQuantity"),$("#noOfRoomsMax").val());
		package_room_ui($(".yCmsContentSlot").find("#y_roomQuantity"),$("#noOfRoomsMax").val());
		$(".yCmsContentSlot").find("#y_roomQuantity").change(function(){
			package_room_ui($(".yCmsContentSlot").find("#y_roomQuantity"),$("#noOfRoomsMax").val());
		})
		$("#y_editPackageForm").find("#y_roomQuantity").change(function(){
			package_room_ui($("#y_editPackageForm").find("#y_roomQuantity"),$("#noOfRoomsMax").val());
		});
		datepickerInit($("#datepicker-activitydetail"),"M dd yy");

	});

	$('.gotonextslide').on('click',function(e){
		e.preventDefault();
		var nextKey=parseInt($(this).attr('data-key'))+1;
		var total=$('.gotonextslide').length;
		if(total==nextKey){
			$('.js-owl-rotating-image').trigger('to.owl.carousel', 0);
		}else{
			$('.js-owl-rotating-image').trigger('to.owl.carousel', nextKey);
		}
	});

    $('.tripAdvisorLink').on('click',function(e){
        var href;
        href=$(this).context.href;
        window.open(href, 'tripAdvisor', 'width=1000px,height=1000px,scrollbars=yes');
        return false;
		});
	$(document).ready(function() {
		//Calculation of highest box and applying that height to rest of boxes on activity pages
		var maxHeight = 0;

		$(".height-calculation").each(function(){
		   if ($(this).height() > maxHeight) 
		   	{ maxHeight = $(this).height(); }
		});

		$(".height-calculation").height(maxHeight);
		
		$(".region-landing .promo-cards").each(function(){
			   if ($(this).height() > maxHeight) 
			   	{ maxHeight = $(this).height(); }
			});

			$(".region-landing .promo-cards").height(maxHeight);
		///////////////////////////////

		//ASM - CSS classes
		var asmText = $(".asm-find-text").text().trim();
		if(asmText != "" && (asmText.search(/Travel/i) > -1 || asmText.search(/Customer/i) > -1)){
			var asmClass = asmText.toLowerCase().replace(/ /g, "-");
			$("#_asm").addClass(asmClass);
		}


		//compare fares modal section
		$("#compare-fares-accordion").find("[role='tabpanel']").on('show.bs.collapse', function () {
			$(this).siblings("[role='tab']").addClass('active');
			$(this).siblings("[role='tab']").find(".bcf").addClass( "bcf-icon-up-arrow" ).removeClass("bcf-icon-down-arrow");
		});

		$("#compare-fares-accordion").find("[role='tabpanel']").on('hide.bs.collapse', function () {
			$(this).siblings("[role='tab']").removeClass('active');
			$(this).siblings("[role='tab']").find(".bcf").removeClass( "bcf-icon-up-arrow" ).addClass("bcf-icon-down-arrow");
		});

	});





