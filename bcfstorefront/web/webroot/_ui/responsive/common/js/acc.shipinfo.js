ACC.shipinfo = {
	_autoloadTracc: [
		"bindShipInfoResult"
	],
            bindShipInfoResult: function() {
                    $("[href^='/ship-info']").click(function(e){
                    e.preventDefault();
                    var code=$(this).attr("href").split("/").pop();
                    ACC.shipinfo.getDetails($(this),code);
                    });
                },
           getDetails: function(ele,code) {
               $(ele).closest("body").append('<div class="y_shipInfoModal"></div>');
               $.when(ACC.services.getShipInfo(code)).then(
               function(data){
               $(".y_shipInfoModal").html(data.shipInfoModalHtml);
               $("#y_shipInfosModal").modal('show');
               });
               ACC.global.initImager();
               return false;
           }
}
