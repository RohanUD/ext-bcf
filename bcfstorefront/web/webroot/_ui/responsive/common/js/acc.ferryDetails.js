ACC.ferrydetails = {
	_autoloadTracc: [
		"changeVehicle",
		"navigateToFerryDetails"
	],
	changeVehicle: function (value) {
		$(".y_findFerryDetailResult").on("click", function () {
			console.log("click", $("#transportVehiclesSelect").val());
			var value = $("#transportVehiclesSelect").val();
			if (value != 'default') {
				window.location.href = ACC.config.contextPath + "/on-the-ferry/our-fleet/" + value;
			}
		})
	},
	navigateToFerryDetails: function (value) {
		$(".y_navigateToFerryDetails").on("click", function () {
			var value = $("#shipCodeFilter").val();
			if (value != 'ALL') {
				window.location.href = ACC.config.contextPath + "/on-the-ferry/our-fleet/" + value;
			}
		})
	}
}
