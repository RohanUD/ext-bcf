ACC.discoverOurRoute = {
	_autoloadTracc: [
		"bindRegionSelection"
	],
	activeWindow: null,
	displayedMap: null,

    addGoogleMapsApi: function () {

        var callback = "ACC.discoverOurRoute.loadGoogleMap";
        if (callback != undefined && $("ulle-googleMapsApi").length == 0) {
            $('head').append('<script async defer class="js-googleMapsApi" type="text/javascript" src="//maps.googleapis.com/maps/api/js?key=' + $('#discovermap').data('googleapi') + '&callback=' + callback + '"></script>');
        } else if (callback != undefined) {
            eval(callback + "()");

        }
    },


	bindRegionSelection: function () {
		$('#routeRegions').prepend('<option value="" disabled selected style="display: none;">'+ACC.MapMessages.message("text.choose.a.region.select")+'</option>');
		var urlParams = (new URL(document.location)).searchParams;
		if(urlParams.has('routeRegions') == true && urlParams.get('routeRegions') != ''){
		$('.bg-image-map-text').html('');
		ACC.discoverOurRoute.addGoogleMapsApi();
		//Fallback for IE as searchParams are not supported
		var sAgent = window.navigator.userAgent;
		var isIE = sAgent.indexOf("MSIE");
		if(isIE > 0){
		function getUrlParameter(name) {
			name = name.replace(/[\[]/, '\\[').replace(/[\]]/, '\\]');
			var regex = new RegExp('[\\?&]' + name + '=([^&#]*)');
			var results = regex.exec(location.search);
			return results === null ? '' : decodeURIComponent(results[1].replace(/\+/g, ' '));
		}; 
		var urlParams = getUrlParameter('routeRegions');
		}
		else{
			var urlParams = (new URL(document.location)).searchParams;
		}
		if(urlParams && urlParams.has('routeRegions') == true && urlParams.get('routeRegions') != ''){
			$('.bg-image-map-text').html('');
			document.getElementById('routeRegions').value=urlParams.get('routeRegions');
		}
		else{
				$('.bg-image-map-text').html('<img src="../../../../_ui/responsive/common/images/bg-map.png"> <h2>'+ACC.MapMessages.message("text.choose.a.region"));
		}
	    }
    },

	loadGoogleMap: function () {
		var map = new google.maps.Map(document.getElementById('discovermap'), {
			mapTypeControl: false,
			streetViewControl: false,
			styles: ACC.schedule.mapStyle
		});
		var listOfTravelRouteCount = $('#listOfTravelRouteCount').attr('value');
		var processedFile = [];
		var checkDuplicateFile = [];
		var processedTerminal = [];
        var infoContentMap = {};
        var mapKmlFileMap = {};
        var mapDiv = $('.y_propertyMapDiscover');
        var url = mapDiv.data('schedulesurl')
        var originToDestinationMappingCount = $('#originToDestinationMappingCount').attr('value');
        for (var j = 0; j < originToDestinationMappingCount; j++) {

            var routeOriginKey ="#routeOrigin"+j;
            var routeOriginFilteredNameDisplay = $(routeOriginKey).data("originfilteredname");
            var routeOriginFilteredCodeDisplay = $(routeOriginKey).data("originfilteredcode");
            var routeOriginNameDisplay = $(routeOriginKey).data("originname");

            var destinationCountKey ="#destinationCount"+j;
            var destinationCountDisplay = $(destinationCountKey).data("destinationcount");

			var infoContent = ACC.viewDiscoverRoutesFrom + "<br><b>" + routeOriginNameDisplay + "</b><br>" + ACC.viewScheduleTo +": " + "<br>";

            for (var k = 0; k < destinationCountDisplay; k++) {

                var routeDestinationKey ="#routeDestination"+j+k;
                var routeDestinationFilteredNameDisplay = $(routeDestinationKey).data("destinationfilteredname");
                var routeDestinationFilteredCodeDisplay = $(routeDestinationKey).data("destinationfilteredcode");
                var routeDestinationNameDisplay = $(routeDestinationKey).data("destinationname");

                var createURL = url + routeOriginFilteredNameDisplay + "-" + routeDestinationFilteredNameDisplay + "/" +
                routeOriginFilteredCodeDisplay + "-" + routeDestinationFilteredCodeDisplay;
                infoContent = infoContent + '<a href="' + createURL + '">' + routeDestinationNameDisplay + '</a>' + "<br>";

                var terminalCode = routeOriginFilteredCodeDisplay;
                infoContentMap[terminalCode] = infoContent;

            }
        }
		for (var i = 0; i < listOfTravelRouteCount; i++) {
           var routeOriginDestinationKey ="#routeOriginDestination"+i;
           var mapkmlfile=$(routeOriginDestinationKey).data("mapkmlfile");
           var mapkmlInternalfileName=$(routeOriginDestinationKey).data("mapkmlinternalfilename");
           if(mapkmlfile != undefined)
           {
                    var mapkmlInternalfileNameSeparator = mapkmlInternalfileName.split("-");
                    var duplicateMapkmlInternalfileName = mapkmlInternalfileNameSeparator[1] + "-" + mapkmlInternalfileNameSeparator[0];
                    if(!checkDuplicateFile.includes(duplicateMapkmlInternalfileName)){
                    checkDuplicateFile.push(mapkmlInternalfileName);
					var kmlFilePathName = mapkmlfile;
					var lineSymbol = {
						path: 'M 0,-0.1 0,0.5',
						strokeOpacity: 1,
						scale: 4
					};

                    // Comment the below line, if you are testing in local env
                    var absoluteFileName = mapkmlfile;

                    // And Uncomment the below line to work in local env.
                    //var absoluteFileName = mapkmlfile.substring(0, mapkmlfile.indexOf("?"));
					mapKmlFileMap[absoluteFileName] = mapkmlInternalfileName;

                    var listenerDebounce;
                    var bounds = new google.maps.LatLngBounds();
					var createMarker = function (placemark, doc) {
						var travelRoute = mapKmlFileMap[placemark.styleBaseUrl];
						var sameFileCount = 1; //Each kml file load 2 times: for each placemark. sameFileCount use to
						                       // identify is it origin or destination placemark
						if(!processedFile.includes(travelRoute)){
						    processedFile.push(travelRoute);
						}else{
						    sameFileCount = 2;
						}
						var travelRouteOriginAndDestinationSeparator = travelRoute.split("-");
						var travelRouteOrigin = travelRouteOriginAndDestinationSeparator[0];
						var travelRouteDestination = travelRouteOriginAndDestinationSeparator[1];
						var isIncluded = true;
						var infoWindowContent;
						if (!processedTerminal.includes(travelRouteOrigin) && sameFileCount == 1) {
							processedTerminal.push(travelRouteOrigin);
							infoWindowContent = infoContentMap[travelRouteOrigin];
							isIncluded = false;
						}else if(!processedTerminal.includes(travelRouteDestination) && sameFileCount == 2){
						    processedTerminal.push(travelRouteDestination);
						    infoWindowContent = infoContentMap[travelRouteDestination];
                            isIncluded = false;
						}
						var marker=null;
						if (!isIncluded) {
							var markerOptions = geoXML3.combineOptions(geoXml.options.markerOptions, {
								map: geoXml.options.map,
								position: new google.maps.LatLng(placemark.Point.coordinates[0].lat, placemark.Point.coordinates[0].lng)
							});

							// Create the marker on the map
							marker = new google.maps.Marker(markerOptions);

							// Set up and create the infowindow if it is not suppressed
							if (!geoXml.options.suppressInfoWindows && !isIncluded) {
								var infoWindowOptions = geoXML3.combineOptions(geoXml.options.infoWindowOptions, {
									content: infoWindowContent
								});
								if (geoXml.options.infoWindow) {
									marker.infoWindow = geoXml.options.infoWindow;
								} else {
									marker.infoWindow = new google.maps.InfoWindow(infoWindowOptions);
								}
								marker.infoWindowOptions = infoWindowOptions;
								bounds.extend(marker.position);
                                var listener = google.maps.event.addListener(map, "idle", function () {
                                    clearTimeout(listenerDebounce);
                                    listenerDebounce = setTimeout(map.fitBounds(bounds, 80), 250);
                                    google.maps.event.removeListener(listener);
                                });
								// Infowindow-opening event handler
								google.maps.event.addListener(marker, 'click', function () {
									//								this.infoWindow.close();
									marker.infoWindow.setOptions(this.infoWindowOptions);
									this.infoWindow.open(this.map, this);

								});
								google.maps.event.addListener(map, 'click', function (event) {
									marker.infoWindow.close();
								});
							}
						    placemark.marker = marker;
						}
						return marker;
					};

					var geoXml = new geoXML3.parser({
                        map: map,
						singleInfoWindow: true,
						createMarker: createMarker,
						markerOptions: {
							icon: {
								url: 'https://snazzy-maps-cdn.azureedge.net/assets/marker-c5ce3bc4-d268-4b8a-9941-285dc369ed54.png',
								scaledSize: new google.maps.Size(
									24,
									24),
								size: new google.maps.Size(
									24,
									24),
								anchor: new google.maps.Point(
									12,
									24)
							}
						},
						polylineOptions: {
							strokeOpacity: 0,
							strokeColor: '#0079a6',
							icons: [{
								icon: lineSymbol,
								offset: '0',
								repeat: '10px'
                            }],
                            clickable:false,
                            strokeWeight:4
						},
					});
					geoXml.parse(kmlFilePathName);

          }
          }
        }
	},


}
