/*
 * Copyright (c) 2019 British Columbia Ferry Services Inc
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of British Columbia Ferry Services Inc("Confidential Information").
 * You shall not disclose such Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with British Columbia Ferry Services Inc.
 *
 * Last Modified: 02/07/19 14:01
 */

package com.bcf.storefront.controllers.pages;

import de.hybris.platform.acceleratorstorefrontcommons.breadcrumb.impl.ContentPageBreadcrumbBuilder;
import de.hybris.platform.acceleratorstorefrontcommons.constants.WebConstants;
import de.hybris.platform.acceleratorstorefrontcommons.controllers.pages.AbstractPageController;
import de.hybris.platform.cms2.exceptions.CMSItemNotFoundException;
import de.hybris.platform.cms2.model.pages.AbstractPageModel;
import de.hybris.platform.cms2.model.pages.ContentPageModel;
import de.hybris.platform.search.restriction.SearchRestrictionService;
import de.hybris.platform.servicelayer.session.SessionExecutionBody;
import javax.annotation.Resource;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;


/**
 * Simple CMS Content Page controller. Used only to preview CMS Pages. The DefaultPageController is used to serve
 * generic content pages.
 */
@Controller
@RequestMapping(value = "/preview-content")
public class PreviewContentPageController extends AbstractPageController
{
	@Resource(name = "contentPageBreadcrumbBuilder")
	private ContentPageBreadcrumbBuilder contentPageBreadcrumbBuilder;

	@Resource(name = "searchRestrictionService")
	private SearchRestrictionService searchRestrictionService;

	@RequestMapping(method = RequestMethod.GET, params =
			{ "uid" })
	public String get(@RequestParam(value = "uid") final String cmsPageUid, final Model model) throws CMSItemNotFoundException
	{
		final AbstractPageModel pageForId = getPageForId(cmsPageUid);
		if (pageForId == null)
		{
			throw new CMSItemNotFoundException("No page with id [" + cmsPageUid + "] found.");
		}
		storeCmsPageInModel(model, pageForId);
		final ContentPageModel contentPageForLabelOrId = getCmsPageForLabelOrId(cmsPageUid);
		setUpMetaDataForContentPage(model, contentPageForLabelOrId);
		model.addAttribute(WebConstants.BREADCRUMBS_KEY,
				contentPageBreadcrumbBuilder.getBreadcrumbs((ContentPageModel) pageForId));
		return getViewForPage(pageForId);
	}

	private AbstractPageModel getPageForId(final String cmsPageUid)
	{
		return ((AbstractPageModel) getSessionService().executeInLocalView(new SessionExecutionBody()
		{
			@Override
			public AbstractPageModel execute()
			{
				searchRestrictionService.disableSearchRestrictions();
				try
				{
					return getCmsPageService().getPageForId(cmsPageUid);
				}
				catch (final CMSItemNotFoundException e)
				{
					return null;
				}
			}
		}));
	}

	protected ContentPageModel getCmsPageForLabelOrId(final String cmsPageUid)
	{
		return ((ContentPageModel) getSessionService().executeInLocalView(new SessionExecutionBody()
		{
			@Override
			public ContentPageModel execute()
			{
				searchRestrictionService.disableSearchRestrictions();
				try
				{
					return PreviewContentPageController.super.getContentPageForLabelOrId(cmsPageUid);
				}
				catch (final CMSItemNotFoundException e)
				{
					return null;
				}
			}
		}));
	}


}
