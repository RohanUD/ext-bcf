/*
 * Copyright (c) 2019 British Columbia Ferry Services Inc
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of British Columbia Ferry Services Inc("Confidential Information").
 * You shall not disclose such Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with British Columbia Ferry Services Inc.
 *
 * Last Modified: 06/06/19 20:21
 */

package com.bcf.storefront.renderer;

import de.hybris.platform.acceleratorcms.component.renderer.CMSComponentRenderer;
import de.hybris.platform.acceleratorstorefrontcommons.tags.Functions;
import de.hybris.platform.category.model.CategoryModel;
import de.hybris.platform.cms2.enums.LinkTargets;
import de.hybris.platform.cms2.model.contents.components.CMSLinkComponentModel;
import de.hybris.platform.commercefacades.product.data.CategoryData;
import de.hybris.platform.commercefacades.product.data.ProductData;
import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.servicelayer.dto.converter.Converter;
import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.jsp.JspException;
import javax.servlet.jsp.PageContext;
import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.apache.taglibs.standard.tag.common.core.UrlSupport;
import org.owasp.html.HtmlPolicyBuilder;
import org.owasp.html.PolicyFactory;
import org.springframework.beans.factory.annotation.Required;


/**
 *
 */
public class CMSLinkComponentRenderer implements CMSComponentRenderer<CMSLinkComponentModel>
{
	private static final Logger LOG = Logger.getLogger(CMSLinkComponentRenderer.class);

	protected static final PolicyFactory policy = new HtmlPolicyBuilder().allowStandardUrlProtocols()
			.allowElements("a", "span")
			.allowAttributes("href", "style", "class", "title", "target", "download", "rel", "rev",
					"hreflang", "type", "text", "accesskey", "contenteditable", "contextmenu", "dir", "draggable",
					"dropzone", "hidden", "id", "lang", "spellcheck", "tabindex", "translate")
			.onElements("a")
			.allowAttributes("class")
			.onElements("span")
			.toFactory();


	private Converter<ProductModel, ProductData> productUrlConverter;
	private Converter<CategoryModel, CategoryData> categoryUrlConverter;

	protected Converter<ProductModel, ProductData> getProductUrlConverter()
	{
		return productUrlConverter;
	}

	@Required
	public void setProductUrlConverter(final Converter<ProductModel, ProductData> productUrlConverter)
	{
		this.productUrlConverter = productUrlConverter;
	}

	protected Converter<CategoryModel, CategoryData> getCategoryUrlConverter()
	{
		return categoryUrlConverter;
	}

	@Required
	public void setCategoryUrlConverter(final Converter<CategoryModel, CategoryData> categoryUrlConverter)
	{
		this.categoryUrlConverter = categoryUrlConverter;
	}

	protected String getUrl(final CMSLinkComponentModel component)
	{
		// Call the function getUrlForCMSLinkComponent so that this code is only in one place
		return Functions.getUrlForCMSLinkComponent(component, getProductUrlConverter(), getCategoryUrlConverter());
	}

	@Override
	public void renderComponent(final PageContext pageContext, final CMSLinkComponentModel component) throws ServletException,
			IOException
	{
		try
		{
			final String url = getUrl(component);
			final String encodedUrl = UrlSupport.resolveUrl(url, null, pageContext);
			final String linkName = component.getLinkName();

			final StringBuilder html = new StringBuilder();

			html.append("<a href=\"");
			html.append(encodedUrl);
			html.append("\" ");

			if (component.getStyleAttributes() != null)
			{
				html.append(component.getStyleAttributes());
			}
			if (component.getTarget() != null && !LinkTargets.SAMEWINDOW.equals(component.getTarget()))
			{
				html.append(" target=\"_blank\"");
			}
			html.append(">");
			if (StringUtils.isNotBlank(linkName))
			{
				html.append(linkName);
			}
			if (component.isDisplayChevronArrow())
			{
				html.append("&nbsp;<i class=\"fa fa-angle-right\"></i>");
			}
			html.append("</a>");
			pageContext.getOut().write(html.toString());
		}
		catch (final JspException e)
		{
			if (LOG.isDebugEnabled())
			{
				LOG.debug(e);
			}
		}
	}
}
