/*
 * Copyright (c) 2019 British Columbia Ferry Services Inc
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of British Columbia Ferry Services Inc("Confidential Information").
 * You shall not disclose such Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with British Columbia Ferry Services Inc.
 *
 * Last Modified: 06/06/19 20:21
 */

package com.bcf.storefront.renderer;

import de.hybris.platform.acceleratorcms.component.renderer.impl.CMSParagraphComponentRenderer;
import de.hybris.platform.cms2.model.contents.components.CMSParagraphComponentModel;
import java.io.IOException;
import javax.servlet.jsp.JspWriter;
import javax.servlet.jsp.PageContext;
import org.apache.commons.lang.StringUtils;


/**
 */
public class BcfCmsParagraphComponentRenderer extends CMSParagraphComponentRenderer
{
	@Override
	public void renderComponent(final PageContext pageContext, final CMSParagraphComponentModel component) throws
			IOException
	{
		final String content = component.getContent();

		final StringBuilder html = new StringBuilder();

		html.append("<div class=\"container\"><div class=\"row\"><div class=\"col-lg-12 col-md-12 col-sm-12 col-xs-12\">");

		if (StringUtils.isNotBlank(content))
		{
			html.append("<div class=\"content\">");
			html.append(content);
		}
		html.append("</div></div></div></div>");

		final JspWriter out = pageContext.getOut();
		out.write(html.toString());
	}
}
