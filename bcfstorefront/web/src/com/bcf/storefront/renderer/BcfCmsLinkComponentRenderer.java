/*
 * Copyright (c) 2019 British Columbia Ferry Services Inc
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of British Columbia Ferry Services Inc("Confidential Information").
 * You shall not disclose such Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with British Columbia Ferry Services Inc.
 *
 * Last Modified: 21/06/19 00:21
 */

package com.bcf.storefront.renderer;

import de.hybris.platform.b2b.model.B2BUnitModel;
import de.hybris.platform.cms2.enums.LinkTargets;
import de.hybris.platform.cms2.model.contents.components.CMSLinkComponentModel;
import de.hybris.platform.servicelayer.session.SessionService;
import de.hybris.platform.servicelayer.user.UserService;
import de.hybris.platform.util.Config;
import java.io.IOException;
import java.util.Objects;
import javax.annotation.Resource;
import javax.servlet.ServletException;
import javax.servlet.jsp.JspException;
import javax.servlet.jsp.PageContext;
import org.apache.commons.lang.BooleanUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.apache.taglibs.standard.tag.common.core.UrlSupport;
import org.springframework.beans.factory.annotation.Required;
import com.bcf.bcfintegrationservice.integration.utility.IntegrationUtility;
import com.bcf.bcfstorefrontaddon.model.components.BCFCMSLinkComponentModel;
import com.bcf.core.bcfproperties.BcfConfigurablePropertiesService;
import com.bcf.core.constants.BcfCoreConstants;
import com.bcf.core.util.BcfB2bUtil;


/**
 */
public class BcfCmsLinkComponentRenderer extends CMSLinkComponentRenderer
{

	private static final Logger LOG = Logger.getLogger(BcfCmsLinkComponentRenderer.class);
	private static final String MY_ACCOUNT_LINK_ID = "MyAccountLink";
	private static final String DROP_TRAILER_LINK_ID = "DropTrailerLink";

	private UserService userService;
	private SessionService sessionService;

	@Resource(name = "bcfB2bUtil")
	private BcfB2bUtil bcfB2bUtil;

	@Resource(name = "integrationUtils")
	private IntegrationUtility integrationUtils;

	@Resource(name = "bcfConfigurablePropertiesService")
	private BcfConfigurablePropertiesService bcfConfigurablePropertiesService;


	@Override
	public void renderComponent(final PageContext pageContext, final CMSLinkComponentModel component) throws ServletException,
			IOException
	{
		if (component instanceof BCFCMSLinkComponentModel)
		{
			final BCFCMSLinkComponentModel bcfCmsLinkComponentModel = (BCFCMSLinkComponentModel) component;
			if (bcfCmsLinkComponentModel.getVisibleOnlyToAnonymousUser() == null)
			{
				renderBcfComponent(pageContext, component);
				return;
			}
			if ((BooleanUtils.isTrue(bcfCmsLinkComponentModel.getVisibleOnlyToAnonymousUser()) && getUserService()
					.isAnonymousUser(getUserService().getCurrentUser())) || (
					BooleanUtils.isFalse(bcfCmsLinkComponentModel.getVisibleOnlyToAnonymousUser()) && !getUserService()
							.isAnonymousUser(getUserService().getCurrentUser())))
			{
				renderBcfComponent(pageContext, component);
			}
		}
		else
		{
			super.renderComponent(pageContext, component);
		}
	}

	public void renderBcfComponent(final PageContext pageContext, final CMSLinkComponentModel component) throws ServletException,
			IOException
	{
		try
		{
			final String url = getUrl(component);
			String encodedUrl = UrlSupport.resolveUrl(url, null, pageContext);

			String linkName = component.getLinkName();

			if (StringUtils.equalsIgnoreCase(MY_ACCOUNT_LINK_ID, component.getUid()))
			{
				linkName = userService.getCurrentUser().getName();

				final B2BUnitModel b2BUnitModel = bcfB2bUtil.getB2bUnitInContext();
				if (Objects.nonNull(b2BUnitModel))
				{
					linkName = linkName + " - " + b2BUnitModel.getName();
				}
			}

			if (StringUtils.equalsIgnoreCase(DROP_TRAILER_LINK_ID, component.getUid()))
			{
				final B2BUnitModel b2BUnitModel = bcfB2bUtil.getB2bUnitInContext();
				if (Objects.nonNull(b2BUnitModel))
				{
					final String customerId = integrationUtils
							.getSystemIdentifierForKey(BcfCoreConstants.CRM_SOURCE_SYSTEM, b2BUnitModel.getSystemIdentifiers());
					encodedUrl = this.getDropTrailerUrl().replace("{customerId}", customerId);
				}
			}

			final StringBuilder html = new StringBuilder();

			html.append("<a href=\"");
			html.append(encodedUrl);
			html.append("\" ");

			if (component.getStyleAttributes() != null)
			{
				html.append("class=\"");
				html.append(component.getStyleAttributes());
				html.append("\" ");
			}
			if (component.getTarget() != null && !LinkTargets.SAMEWINDOW.equals(component.getTarget()))
			{
				html.append(" target=\"_blank\"");
			}
			html.append(">");
			if (StringUtils.isNotBlank(linkName))
			{
				html.append(linkName);
			}
			if (component.isDisplayChevronArrow())
			{
				html.append("&nbsp;<i class=\"fa fa-angle-right\"></i>");
			}
			html.append("</a>");
			pageContext.getOut().write(html.toString());
		}
		catch (final JspException e)
		{
			if (LOG.isDebugEnabled())
			{
				LOG.debug(e);
			}
		}
	}

	private String getDropTrailerUrl()
	{
		final boolean isDev = Config.getBoolean("development.mode", true);
		String dropTrailerConfigProperty = "DevDropTrailerUrl";
		if (!isDev)
		{
			dropTrailerConfigProperty = "ProdDropTrailerUrl";
		}
		return bcfConfigurablePropertiesService.getBcfPropertyValue(dropTrailerConfigProperty);
	}

	public UserService getUserService()
	{
		return userService;
	}

	@Required
	public void setUserService(final UserService userService)
	{
		this.userService = userService;
	}

	protected SessionService getSessionService()
	{
		return sessionService;
	}

	@Required
	public void setSessionService(final SessionService sessionService)
	{
		this.sessionService = sessionService;
	}
}
