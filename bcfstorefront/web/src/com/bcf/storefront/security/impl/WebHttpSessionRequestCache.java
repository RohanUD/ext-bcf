/*
 * Copyright (c) 2019 British Columbia Ferry Services Inc
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of British Columbia Ferry Services Inc("Confidential Information").
 * You shall not disclose such Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with British Columbia Ferry Services Inc.
 *
 * Last Modified: 06/06/19 20:21
 */

package com.bcf.storefront.security.impl;

import de.hybris.platform.acceleratorstorefrontcommons.constants.WebConstants;
import de.hybris.platform.servicelayer.session.SessionService;
import java.io.Serializable;
import java.util.Set;
import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang3.BooleanUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Required;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.web.PortResolver;
import org.springframework.security.web.PortResolverImpl;
import org.springframework.security.web.savedrequest.DefaultSavedRequest;
import org.springframework.security.web.savedrequest.HttpSessionRequestCache;
import org.springframework.security.web.savedrequest.SavedRequest;
import org.springframework.security.web.util.UrlUtils;
import org.springframework.security.web.util.matcher.AnyRequestMatcher;
import org.springframework.security.web.util.matcher.RequestMatcher;
import com.bcf.core.constants.BcfCoreConstants;
import com.bcf.facades.constants.BcfFacadesConstants;


/**
 * Extension of HttpSessionRequestCache that allows pass through of cookies from the current request. This is required
 * to allow the GUIDInterceptor to see the secure cookie written during authentication.
 * <p>
 * The <tt>RequestCache</tt> stores the <tt>SavedRequest</tt> in the HttpSession, this is then restored perfectly.
 * Unfortunately the saved request also hides new cookies that have been written since the saved request was created.
 * This implementation allows the current request's cookie values to override the cookies within the saved request.
 */
public class WebHttpSessionRequestCache extends HttpSessionRequestCache implements Serializable
{
	private static final Logger LOG = Logger.getLogger(WebHttpSessionRequestCache.class);

	private static final long serialVersionUID = 1L;
	private static final String REFERER = "referer";

	static final String SAVED_REQUEST = "SPRING_SECURITY_SAVED_REQUEST";

	private transient PortResolver portResolver = new PortResolverImpl();
	private transient RequestMatcher requestMatcher = AnyRequestMatcher.INSTANCE;
	private boolean createSessionAllowed = true;

	private SessionService sessionService;

	@Resource(name = "bookingFlowUrlSet")
	private Set<String> bookingFlowUrlSet;

	@Required
	public void setSessionService(final SessionService sessionService)
	{
		this.sessionService = sessionService;
	}

	public SessionService getSessionService()
	{
		return sessionService;
	}



	@Override
	public void setRequestMatcher(final RequestMatcher requestMatcher)
	{
		this.requestMatcher = requestMatcher;
		super.setRequestMatcher(requestMatcher);
	}


	@Override
	public void setPortResolver(final PortResolver portResolver)
	{
		this.portResolver = portResolver;
		super.setPortResolver(portResolver);
	}


	@Override
	public void setCreateSessionAllowed(final boolean createSessionAllowed)
	{
		this.createSessionAllowed = createSessionAllowed;
	}

	@Override
	public void saveRequest(final HttpServletRequest request, final HttpServletResponse response)
	{
		//this might be called while in ExceptionTranslationFilter#handleSpringSecurityException in this case base implementation
		if (SecurityContextHolder.getContext().getAuthentication() == null)
		{
			super.saveRequest(request, response);
		}
		else
		{
			final SavedRequest savedBefore = getRequest(request, response);
			if (savedBefore != null)//to not override request saved by ExceptionTranslationFilter#handleSpringSecurityException
			{
				return;
			}

			if (getRequestMatcher().matches(request))
			{
				final DefaultSavedRequest savedRequest = new DefaultSavedRequest(request, getPortResolver())
				{
					private final String referer = request.getHeader(REFERER);
					private final String contextPath = request.getContextPath();

					@Override
					public String getRedirectUrl()
					{
						return calculateRelativeRedirectUrl(contextPath, referer);
					}
				};

				if (isCreateSessionAllowed() || request.getSession(false) != null)
				{
					request.getSession().setAttribute(SAVED_REQUEST, savedRequest);
					logger.debug("DefaultSavedRequest added to Session: " + savedRequest);
				}
			}
			else
			{
				logger.debug("Request not saved as configured RequestMatcher did not match");
			}
		}
	}


	protected boolean isCreateSessionAllowed()
	{
		return createSessionAllowed;
	}

	protected PortResolver getPortResolver()
	{
		return portResolver;
	}

	protected RequestMatcher getRequestMatcher()
	{
		return requestMatcher;
	}

	@Override
	public HttpServletRequest getMatchingRequest(final HttpServletRequest request, final HttpServletResponse response)
	{
		//unfortunately the original referer gets override by default request['\'] in super.getMatchingRequest(), thus need to save the referer in the savedRequest before getting any matched request
		if (isLoginViaBookingFlow(request, response))
		{
			saveRequest(request, response);
		}
		HttpServletRequest result = super.getMatchingRequest(request, response);
		if (result != null)
		{
			result = new CookieMergingHttpServletRequestWrapper(result, request);
		}
		return result;
	}

	/**
	 * checks when the login button is clicked, the referer belongs to any of the booking URLs
	 *
	 * @param request
	 * @param response
	 * @return
	 */
	boolean isLoginViaBookingFlow(final HttpServletRequest request, final HttpServletResponse response)
	{
		boolean isLoginViaBookingFlow = false;
		if (CollectionUtils.isNotEmpty(bookingFlowUrlSet))
		{
			final String referer = request.getHeader(REFERER);
			if (StringUtils.isNotBlank(referer))
			{
				isLoginViaBookingFlow = bookingFlowUrlSet.stream().anyMatch(url -> referer.contains(url));
			}
		}
		return isLoginViaBookingFlow;
	}

	protected String calculateRelativeRedirectUrl(final String contextPath, final String url)
	{
		LOG.debug(String.format("contextPath is [%s] and url is [%s]", contextPath, url));
		if (UrlUtils.isAbsoluteUrl(url))
		{
			String relUrl = url.substring(url.indexOf("://") + 3);
			String modifiedContextPath = StringUtils.isNotEmpty(contextPath) ? contextPath : "/";
			final String urlEncodingAttributes = getSessionService().getAttribute(WebConstants.URL_ENCODING_ATTRIBUTES);
			if (urlEncodingAttributes != null && !url.contains(urlEncodingAttributes)
					&& modifiedContextPath.contains(urlEncodingAttributes))
			{
				modifiedContextPath = StringUtils.remove(modifiedContextPath, urlEncodingAttributes);
			}
			relUrl = relUrl.substring(relUrl.indexOf(modifiedContextPath) + modifiedContextPath.length());
			final String convertFailed = sessionService.getAttribute(BcfCoreConstants.CONVERT_QUOTATION_TO_OPTION_FAILED);
			if (BooleanUtils.toBoolean(convertFailed))
			{
				LOG.debug("Redirecting to homepage as cart conversion failed");
				return BcfFacadesConstants.HOME_PAGE_PATH + "?cart-upgrade=false";
			}
			LOG.debug(String.format("Redirecting to relURL [%s]", relUrl));
			return StringUtils.isBlank(relUrl) ? "/" : "/" + relUrl;
		}
		else
		{
			LOG.debug(String.format("Redirecting to URL [%s]", url));
			return StringUtils.isBlank(url) ? "/" : url;
		}
	}

}
