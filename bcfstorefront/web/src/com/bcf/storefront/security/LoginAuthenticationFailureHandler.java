/*
 * Copyright (c) 2019 British Columbia Ferry Services Inc
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of British Columbia Ferry Services Inc("Confidential Information").
 * You shall not disclose such Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with British Columbia Ferry Services Inc.
 *
 * Last Modified: 06/06/19 20:21
 */

package com.bcf.storefront.security;

import de.hybris.platform.acceleratorstorefrontcommons.security.BruteForceAttackCounter;
import de.hybris.platform.core.model.user.UserModel;
import de.hybris.platform.servicelayer.exceptions.UnknownIdentifierException;
import de.hybris.platform.servicelayer.model.ModelService;
import de.hybris.platform.servicelayer.user.UserService;
import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Required;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.web.DefaultRedirectStrategy;
import org.springframework.security.web.RedirectStrategy;
import org.springframework.security.web.authentication.SimpleUrlAuthenticationFailureHandler;


public class LoginAuthenticationFailureHandler extends SimpleUrlAuthenticationFailureHandler
{
	private static final Logger LOG = Logger.getLogger(LoginAuthenticationFailureHandler.class);
	public static final String LOGIN_MESSAGE_PARAM = "/login?message=";

	private BruteForceAttackCounter bruteForceAttackCounter;
	private String defaultFailureUrl;
	private final boolean forwardToDestination = false;
	private final RedirectStrategy redirectStrategy = new DefaultRedirectStrategy();
	private UserService userService;
	private ModelService modelService;

	@Override
	public void onAuthenticationFailure(final HttpServletRequest request, final HttpServletResponse response,
			final AuthenticationException exception) throws IOException, ServletException
	{
		// Register brute attacks
		final String j_username = request.getParameter("j_username");
		bruteForceAttackCounter.registerLoginFailure(j_username);
		final int userLoginFailCount = bruteForceAttackCounter.getUserFailedLogins(j_username);
		LOG.error("Login failed count for " + j_username + " is " + userLoginFailCount);
		if (userLoginFailCount >= 5)
		{
			try
			{
				final UserModel user = userService.getUserForUID(j_username);
				if (!user.isLoginDisabled())
				{
					LOG.error("Disabling the login for the user " + j_username);
					user.setLoginDisabled(true);
					modelService.save(user);
				}

				this.redirectStrategy.sendRedirect(request, response, "/login?message=user.account.locked");
				return;
			}
			catch (final UnknownIdentifierException unEx)
			{
				LOG.error(unEx.getMessage(), unEx);
			}
		}

		// Store the j_username in the session
		request.getSession().setAttribute("SPRING_SECURITY_LAST_USERNAME", j_username);
		final String queryString = request.getHeader("referer").toString();
		String loginToCheckoutParam = null;
		if(queryString.contains("loginToCheckout"))
		{
			loginToCheckoutParam=queryString.substring(queryString.indexOf("loginToCheckout")
					, queryString.length());
		}
		if (exception.getMessage().equals("account.locked.message"))
		{
			String encodedRedirectUrl = null;
			if(StringUtils.isNotEmpty(loginToCheckoutParam))
			{
				encodedRedirectUrl = response
						.encodeRedirectURL(request.getContextPath() + "/?error=" + exception.getMessage() + "&" + loginToCheckoutParam);
			}
			else {
				encodedRedirectUrl = response
						.encodeRedirectURL(request.getContextPath() + "/?error=" + exception.getMessage());
			}
			response.sendRedirect(encodedRedirectUrl);
		}
		else if (exception.getMessage().equals("verification.email.message"))
		{
			if(StringUtils.isNotEmpty(loginToCheckoutParam))
			{
				this.redirectStrategy.sendRedirect(request, response,
						LOGIN_MESSAGE_PARAM + exception.getMessage() + "&email=" + j_username + "&" + loginToCheckoutParam);
			}
			else {
				this.redirectStrategy.sendRedirect(request, response,
						LOGIN_MESSAGE_PARAM + exception.getMessage() + "&email=" + j_username);
			}
		}
		else
		{
			if (this.defaultFailureUrl == null)
			{
				this.logger.debug("No failure URL set, sending 401 Unauthorized error");
				this.logger.debug("Redirecting to " + this.defaultFailureUrl);
				if(StringUtils.isNotEmpty(loginToCheckoutParam))
				{
					this.redirectStrategy.sendRedirect(request, response, LOGIN_MESSAGE_PARAM + exception.getMessage() + "&" + loginToCheckoutParam);
				}
				else {

					this.redirectStrategy.sendRedirect(request, response, LOGIN_MESSAGE_PARAM + exception.getMessage());
				}
			}
			else
			{
				this.saveException(request, exception);
				if (this.forwardToDestination)
				{
					this.logger.debug("Forwarding to " + this.defaultFailureUrl);
					request.getRequestDispatcher(this.defaultFailureUrl).forward(request, response);
				}
				else
				{
					this.logger.debug("Redirecting to " + this.defaultFailureUrl);
					if (StringUtils.isNotEmpty(loginToCheckoutParam))
					{
						this.redirectStrategy.sendRedirect(request, response, LOGIN_MESSAGE_PARAM + exception.getMessage() + "&" + loginToCheckoutParam);
					}
					else {
						this.redirectStrategy.sendRedirect(request, response, LOGIN_MESSAGE_PARAM + exception.getMessage());
					}
				}
			}
		}
	}

	protected BruteForceAttackCounter getBruteForceAttackCounter()
	{
		return bruteForceAttackCounter;
	}

	@Required
	public void setBruteForceAttackCounter(final BruteForceAttackCounter bruteForceAttackCounter)
	{
		this.bruteForceAttackCounter = bruteForceAttackCounter;
	}

	protected UserService getUserService()
	{
		return userService;
	}

	@Required
	public void setUserService(final UserService userService)
	{
		this.userService = userService;
	}

	protected ModelService getModelService()
	{
		return modelService;
	}

	@Required
	public void setModelService(final ModelService modelService)
	{
		this.modelService = modelService;
	}
}
