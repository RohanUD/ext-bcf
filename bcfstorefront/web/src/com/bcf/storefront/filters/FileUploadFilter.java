/*
 * Copyright (c) 2019 British Columbia Ferry Services Inc
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of British Columbia Ferry Services Inc("Confidential Information").
 * You shall not disclose such Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with British Columbia Ferry Services Inc.
 *
 * Last Modified: 06/06/19 20:21
 */

package com.bcf.storefront.filters;

import java.io.IOException;
import java.util.Map;
import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.springframework.beans.factory.annotation.Required;
import org.springframework.http.HttpMethod;
import org.springframework.util.PathMatcher;
import org.springframework.web.filter.OncePerRequestFilter;
import org.springframework.web.multipart.support.MultipartFilter;


public class FileUploadFilter extends OncePerRequestFilter
{
	private Map<String, MultipartFilter> urlFilterMapping;
	private PathMatcher pathMatcher;

	@Override
	public void doFilterInternal(final HttpServletRequest request, final HttpServletResponse response,
			final FilterChain filterChain) throws IOException, ServletException
	{
		if (HttpMethod.POST.name().equalsIgnoreCase(request.getMethod()))
		{
			final MultipartFilter multipartFilter = getMultipartFilter(request.getServletPath());
			if (multipartFilter != null)
			{
				multipartFilter.doFilter(request, response, filterChain);
			}
			else
			{
				filterChain.doFilter(request, response);
			}
		}
		else
		{
			filterChain.doFilter(request, response);
		}
	}

	protected MultipartFilter getMultipartFilter(final String servletPath)
	{
		for (final Map.Entry<String, MultipartFilter> multipartFilterMapping : getUrlFilterMapping().entrySet())
		{
			if (getPathMatcher().match(multipartFilterMapping.getKey(), servletPath))
			{
				return multipartFilterMapping.getValue();
			}
		}

		return null;
	}

	protected Map<String, MultipartFilter> getUrlFilterMapping()
	{
		return urlFilterMapping;
	}

	@Required
	public void setUrlFilterMapping(final Map<String, MultipartFilter> urlFilterMapping)
	{
		this.urlFilterMapping = urlFilterMapping;
	}

	protected PathMatcher getPathMatcher()
	{
		return pathMatcher;
	}

	@Required
	public void setPathMatcher(final PathMatcher pathMatcher)
	{
		this.pathMatcher = pathMatcher;
	}
}
