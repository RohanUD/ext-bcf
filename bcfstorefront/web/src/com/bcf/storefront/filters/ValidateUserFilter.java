/*
 * Copyright (c) 2019 British Columbia Ferry Services Inc
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of British Columbia Ferry Services Inc("Confidential Information").
 * You shall not disclose such Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with British Columbia Ferry Services Inc.
 *
 * Last Modified: 06/06/19 20:21
 */

package com.bcf.storefront.filters;

import de.hybris.platform.assistedserviceservices.AssistedServiceService;
import de.hybris.platform.assistedserviceservices.utils.AssistedServiceSession;
import de.hybris.platform.core.model.user.UserModel;
import de.hybris.platform.servicelayer.user.UserService;
import java.io.IOException;
import javax.annotation.Resource;
import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.commons.lang.StringUtils;
import org.springframework.web.filter.OncePerRequestFilter;


/**
 * Filter that initializes the session for the bcfstorefront. This is a spring configured filter that is
 * executed by the PlatformFilterChain.
 */
public class ValidateUserFilter extends OncePerRequestFilter
{
	@Resource(name = "userService")
	private UserService userService;

	@Resource(name = "assistedServiceService")
	private AssistedServiceService assistedServiceService;

	@Override
	public void doFilterInternal(final HttpServletRequest request, final HttpServletResponse response,
			final FilterChain filterChain) throws IOException, ServletException
	{

		final UserModel currentUser = userService.getCurrentUser();
		if (!userService.isAnonymousUser(currentUser))
		{
			AssistedServiceSession assistedServiceSession = assistedServiceService.getAsmSession();
			if (!(assistedServiceSession != null && assistedServiceSession.getAgent() != null) && currentUser.isLoginDisabled()
					&& !StringUtils
					.equalsIgnoreCase(request.getRequestURI(), "/logout"))
			{
				response.sendRedirect("/logout");
				return;
			}
		}
		filterChain.doFilter(request, response);
	}
}
