/*
 * Copyright (c) 2019 British Columbia Ferry Services Inc
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of British Columbia Ferry Services Inc("Confidential Information").
 * You shall not disclose such Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with British Columbia Ferry Services Inc.
 *
 * Last Modified: 02/07/19 14:01
 */

package com.bcf.storefront.filters;

import java.io.IOException;
import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.springframework.beans.factory.annotation.Required;
import org.springframework.web.filter.OncePerRequestFilter;
import com.bcf.storefront.security.GuestCheckoutCartCleanStrategy;


public class AnonymousCheckoutFilter extends OncePerRequestFilter
{

	private GuestCheckoutCartCleanStrategy guestCheckoutCartCleanStrategy;

	@Override
	protected void doFilterInternal(final HttpServletRequest request, final HttpServletResponse response,
			final FilterChain filterChain) throws ServletException, IOException
	{
		getGuestCheckoutCartCleanStrategy().cleanGuestCart(request);
		filterChain.doFilter(request, response);
	}

	public GuestCheckoutCartCleanStrategy getGuestCheckoutCartCleanStrategy()
	{
		return guestCheckoutCartCleanStrategy;
	}

	@Required
	public void setGuestCheckoutCartCleanStrategy(final GuestCheckoutCartCleanStrategy guestCheckoutCartCleanStrategy)
	{
		this.guestCheckoutCartCleanStrategy = guestCheckoutCartCleanStrategy;
	}

}
