/*
 * Copyright (c) 2019 British Columbia Ferry Services Inc
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of British Columbia Ferry Services Inc("Confidential Information").
 * You shall not disclose such Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with British Columbia Ferry Services Inc.
 *
 * Last Modified: 02/07/19 14:01
 */

package com.bcf.storefront.interceptors.beforeview;

import de.hybris.platform.acceleratorstorefrontcommons.constants.WebConstants;
import de.hybris.platform.acceleratorstorefrontcommons.interceptors.BeforeViewHandler;
import de.hybris.platform.commercefacades.order.CartFacade;
import de.hybris.platform.core.model.order.CartModel;
import de.hybris.platform.servicelayer.session.SessionService;
import java.util.List;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Required;
import org.springframework.web.servlet.ModelAndView;
import com.bcf.core.service.BCFTravelCartService;


/**
 * Handler to add cart restoration messages when appropriate
 */
public class CartRestorationBeforeViewHandler implements BeforeViewHandler
{
	private SessionService sessionService;
	private List<String> pagesToShowModifications;
	private CartFacade cartFacade;
	private BCFTravelCartService bcfTravelCartService;

	public static final String SESSION_CART_PARAMETER_NAME = "cart";

	@Override
	public void beforeView(final HttpServletRequest request, final HttpServletResponse response, final ModelAndView modelAndView)
	{

		if (getSessionService().getAttribute(WebConstants.CART_RESTORATION) != null
				&& getSessionService().getAttribute(WebConstants.CART_RESTORATION_SHOW_MESSAGE) != null
				&& ((Boolean) getSessionService().getAttribute(WebConstants.CART_RESTORATION_SHOW_MESSAGE)).booleanValue())
		{

			if (getSessionService().getAttribute(WebConstants.CART_RESTORATION_ERROR_STATUS) != null)
			{
				modelAndView.getModel().put("restorationErrorMsg",
						getSessionService().getAttribute(WebConstants.CART_RESTORATION_ERROR_STATUS));
			}
			else
			{
				modelAndView.getModel().put("restorationData", getSessionService().getAttribute(WebConstants.CART_RESTORATION));
			}

			modelAndView.getModel().put("showModifications", showModifications(request));
		}

		getSessionService().setAttribute(WebConstants.CART_RESTORATION_SHOW_MESSAGE, Boolean.FALSE);

		if (getBcfTravelCartService().hasSessionCart())
		{
			final CartModel cart = getBcfTravelCartService().getSessionCart();
			if (getBcfTravelCartService().isCartValidWithMinimumRequirements(cart))
			{
				modelAndView.addObject("cartId", cart.getCode());
			}
		}
	}

	/**
	 * Decide whether or not the modifications related to the cart restoration or merge should be displayed in the
	 * notifications
	 *
	 * @param request
	 * @return whether or not to display the modifications
	 */
	protected Boolean showModifications(final HttpServletRequest request)
	{
		for (final String targetPage : getPagesToShowModifications())
		{
			if (StringUtils.contains(request.getRequestURI(), targetPage))
			{
				return Boolean.TRUE;
			}
		}
		return Boolean.FALSE;
	}

	protected SessionService getSessionService()
	{
		return sessionService;
	}

	@Required
	public void setSessionService(final SessionService sessionService)
	{
		this.sessionService = sessionService;
	}

	protected List<String> getPagesToShowModifications()
	{
		return pagesToShowModifications;
	}

	@Required
	public void setPagesToShowModifications(final List<String> pagesToShowModifications)
	{
		this.pagesToShowModifications = pagesToShowModifications;
	}
	public CartFacade getCartFacade() {
		return cartFacade;
	}

	public void setCartFacade(final CartFacade cartFacade) {
		this.cartFacade = cartFacade;
	}

	protected BCFTravelCartService getBcfTravelCartService()
	{
		return bcfTravelCartService;
	}

	@Required
	public void setBcfTravelCartService(final BCFTravelCartService bcfTravelCartService)
	{
		this.bcfTravelCartService = bcfTravelCartService;
	}
}
