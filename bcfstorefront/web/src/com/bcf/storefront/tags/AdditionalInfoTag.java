/*
 * Copyright (c) 2019 British Columbia Ferry Services Inc
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of British Columbia Ferry Services Inc("Confidential Information").
 * You shall not disclose such Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with British Columbia Ferry Services Inc.
 *
 * Last Modified: 02/07/19 14:01
 */

package com.bcf.storefront.tags;

import de.hybris.platform.core.Registry;
import java.io.IOException;
import java.util.Objects;
import java.util.Optional;
import javax.servlet.jsp.PageContext;
import javax.servlet.jsp.tagext.SimpleTagSupport;
import org.apache.commons.lang3.StringUtils;
import com.bcf.custom.propertysource.data.PropertySourceData;
import com.bcf.custom.propertysource.facade.PropertySourceFacade;


public class AdditionalInfoTag extends SimpleTagSupport
{

	private String code;
	private String title;
	private String description;
	private String text;
	private PropertySourceFacade propertySourceFacade;

	@Override
	public void doTag() throws IOException
	{
		propertySourceFacade = Registry.getApplicationContext().getBean("propertySourceFacade", PropertySourceFacade.class);
		final PageContext pageContext = (PageContext) getJspContext();
		final PropertySourceData additionalInfoSource = propertySourceFacade.getAdditionalInfoSource(code);
		pageContext.getOut().println(Optional.ofNullable(additionalInfoSource).map(this::getAdditionalInfoText).orElse(text));
	}

	private String getAdditionalInfoText(final PropertySourceData additionalInfoSource)
	{
		final StringBuilder outputTextBuilder = new StringBuilder();
		if (Objects.nonNull(additionalInfoSource.getTitle()))
		{
			outputTextBuilder.append(additionalInfoSource.getTitle());
		}
		if (Objects.nonNull(additionalInfoSource.getDescription()))
		{
			if (outputTextBuilder.length() > 0)
			{
				outputTextBuilder.append(StringUtils.SPACE);
			}
			outputTextBuilder.append(additionalInfoSource.getDescription());
		}
		return outputTextBuilder.toString();
	}

	public String getDescription()
	{
		return description;
	}

	public void setDescription(final String description)
	{
		this.description = description;
	}

	public void setTitle(final String title)
	{
		this.title = title;
	}

	public String getCode()
	{
		return code;
	}

	public void setCode(final String code)
	{
		this.code = code;
	}

	public String getTitle()
	{
		return title;
	}

	public String getText()
	{
		return text;
	}

	public void setText(final String text)
	{
		this.text = text;
	}

	public PropertySourceFacade getPropertySourceFacade()
	{
		return propertySourceFacade;
	}

	public void setPropertySourceFacade(final PropertySourceFacade propertySourceFacade)
	{
		this.propertySourceFacade = propertySourceFacade;
	}
}
