/*
 * Copyright (c) 2019 British Columbia Ferry Services Inc
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of British Columbia Ferry Services Inc("Confidential Information").
 * You shall not disclose such Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with British Columbia Ferry Services Inc.
 *
 * Last Modified: 02/07/19 12:36
 */

package com.bcf.storefront.session.listener;

import de.hybris.platform.core.Constants;
import de.hybris.platform.core.PK;
import de.hybris.platform.core.Registry;
import de.hybris.platform.core.model.order.CartModel;
import de.hybris.platform.jalo.JaloObjectNoLongerValidException;
import de.hybris.platform.jalo.JaloSession;
import de.hybris.platform.servicelayer.config.ConfigurationService;
import de.hybris.platform.servicelayer.exceptions.ModelLoadingException;
import de.hybris.platform.servicelayer.exceptions.ModelSavingException;
import de.hybris.platform.servicelayer.model.ModelService;
import de.hybris.platform.servicelayer.session.SessionService;
import de.hybris.platform.servicelayer.web.DefaultSessionCloseStrategy;
import de.hybris.platform.util.RedeployUtilities;
import de.hybris.platform.util.jeeapi.YNoSuchEntityException;
import java.util.Arrays;
import javax.annotation.Resource;
import javax.servlet.http.HttpSession;
import org.apache.log4j.Logger;
import com.bcf.core.service.BcfTravelCommerceStockService;


public class BCFSessionCloseStrategy extends DefaultSessionCloseStrategy
{

   private static final Logger LOG = Logger.getLogger(BCFSessionCloseStrategy.class);

   @Resource(name="bcfTravelCommerceStockService")
   private BcfTravelCommerceStockService bcfTravelCommerceStockService;

   @Resource(name="modelService")
   private ModelService modelService;

   @Resource(name = "sessionService")
   private SessionService sessionService;

   @Resource(name = "configurationService")
   private ConfigurationService configurationService;

   @Override
   public void closeSessionInHttpSession(HttpSession session) {
      final JaloSession jaloSession = (JaloSession) session.getAttribute(Constants.WEB.JALOSESSION);

      if(jaloSession != null && !RedeployUtilities.isShutdownInProgress()) {

         if (LOG.isInfoEnabled()) {
            LOG.info(String.format("Session-Expiry....[httpSession....%s/jalosession....%s/configured timeout....%s]",
                  session.getId(), jaloSession.getSessionID(), String.valueOf(session.getMaxInactiveInterval())));
         }
         if (LOG.isDebugEnabled()) {
            LOG.debug(Arrays.toString(Thread.currentThread().getStackTrace()));
         }

         releaseStocks(jaloSession);

      }
      super.closeSessionInHttpSession(session);
   }

   private void releaseStocks(final JaloSession js) {
      try {
         PK pk;
         if (( pk = (PK)js.getAttribute("cartPK")) != null) {
            Registry.activateMasterTenant();
            CartModel cart = modelService.get(pk);
            LOG.info("Processing Session Expiry for Cart " + cart.getCode());
            bcfTravelCommerceStockService.releaseStocks(cart);
         }
      }
      catch (ModelLoadingException | ModelSavingException | JaloObjectNoLongerValidException | YNoSuchEntityException e) {
         LOG.warn("A concurrency issue has occurred when trying to release stock which has been removed. Cannot release stock of cart against");
         LOG.debug(e);
      }
   }

}
