/*
 * Copyright (c) 2019 British Columbia Ferry Services Inc
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of British Columbia Ferry Services Inc("Confidential Information").
 * You shall not disclose such Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with British Columbia Ferry Services Inc.
 *
 * Last Modified: 30/11/18 18:06
 */

package com.bcf.storefront.renderer;

import static org.mockito.Mockito.doCallRealMethod;
import static org.mockito.Mockito.when;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.cms2.enums.LinkTargets;
import de.hybris.platform.cms2.model.contents.components.CMSLinkComponentModel;
import java.io.IOException;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.io.Writer;
import javax.servlet.ServletException;
import javax.servlet.jsp.JspWriter;
import javax.servlet.jsp.PageContext;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;
import org.springframework.mock.web.MockJspWriter;


/**
 * Created by dan on 19/04/2017.
 */
@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class CMSLinkComponentRendererTest
{

	private static final String COMPONENT_URL = "https://www.somesite.com/page/subpage";
	private static final String LINK_NAME = "Link Name";
	private static final String LINK_NAME_ALERT = "Link Name<script>alert('attacked')</script>";
	private static final String EXPECTED_HREF_COMMON = "<a href=\"https://www.somesite.com/page/subpage\" ";

	@Mock
	PageContext pageContext;

	@Mock
	CMSLinkComponentModel component;

	@Mock
	CMSLinkComponentRenderer componentRenderer;

	Writer stringWriter;

	JspWriter out;

	@Before
	public void setUp()
	{
		stringWriter = new StringWriter();
		out = new MockJspWriter(new PrintWriter(stringWriter));
	}

	@Test
	public void testRenderComponentHappyPath() throws ServletException, IOException
	{
		final String styleAttributes = "class=\"fsa-logo\" style=\"font-weight: bold\" download=\"download\" "
				+ "rev=\"rev\" hreflang=\"hreflang\" type=\"type\" text=\"text\" accesskey=\"accesskey\" "
				+ "contenteditable=\"contenteditable\" contextmenu=\"contextmenu\" dir=\"dir\" draggable=\"draggable\" "
				+ "dropzone=\"dropzone\" hidden=\"hidden\" id=\"id\" lang=\"lang\" spellcheck=\"spellcheck\" "
				+ "tabindex=\"tabindex\" translate=\"translate\"";
		final LinkTargets linkTarget = LinkTargets.NEWWINDOW;

		createMockExpectations(COMPONENT_URL, LINK_NAME, styleAttributes, linkTarget);

		componentRenderer.renderComponent(pageContext, component);
		final String actual = stringWriter.toString();
		final String expected = EXPECTED_HREF_COMMON
				+ "class=\"fsa-logo\" style=\"font-weight: bold\" download=\"download\" "
				+ "rev=\"rev\" hreflang=\"hreflang\" type=\"type\" text=\"text\" "
				+ "accesskey=\"accesskey\" contenteditable=\"contenteditable\" contextmenu=\"contextmenu\" dir=\"dir\" "
				+ "draggable=\"draggable\" dropzone=\"dropzone\" hidden=\"hidden\" id=\"id\" lang=\"lang\" "
				+ "spellcheck=\"spellcheck\" tabindex=\"tabindex\" translate=\"translate\" "
				+ "title=\"Link Name\" target=\"_blank\" rel=\"noopener noreferrer\">Link Name</a>";

		Assert.assertEquals(expected, actual);
	}

	@Test
	public void testRenderComponentStyleAttributesWithXSS() throws ServletException, IOException
	{
		final String styleAttributes = "class=\"fsa-logo\" style=\"font-weight: bold\" <script>alert('attacked')</script>";
		final LinkTargets linkTarget = null;

		createMockExpectations(COMPONENT_URL, LINK_NAME, styleAttributes, linkTarget);

		componentRenderer.renderComponent(pageContext, component);
		final String actual = stringWriter.toString();
		final String expected = EXPECTED_HREF_COMMON
				+ "class=\"fsa-logo\" style=\"font-weight: bold\">"
				+ "alert(&#39;attacked&#39;) title&#61;&#34;Link Name&#34; &gt;Link Name</a>";

		Assert.assertEquals(expected, actual);
	}

	@Test
	public void testRenderComponentLinkNameWithXSS() throws ServletException, IOException
	{
		final String styleAttributes = null;
		final LinkTargets linkTarget = null;

		createMockExpectations(COMPONENT_URL, LINK_NAME_ALERT, styleAttributes, linkTarget);

		componentRenderer.renderComponent(pageContext, component);
		final String actual = stringWriter.toString();
		final String expected = EXPECTED_HREF_COMMON
				+ "title=\"Link Name&lt;script&gt;alert(&#39;attacked&#39;)&lt;/script&gt;\">" + "Link Name</a>";

		Assert.assertEquals(expected, actual);
	}

	@Test
	public void testRenderComponentLinkNameAndStyleAttributesWithXSS() throws ServletException, IOException
	{
		final String styleAttributes = "class=\"fsa-logo\" style=\"font-weight: bold\" <script>alert('attacked')</script>";
		final LinkTargets linkTarget = null;

		createMockExpectations(COMPONENT_URL, LINK_NAME_ALERT, styleAttributes, linkTarget);

		componentRenderer.renderComponent(pageContext, component);
		final String actual = stringWriter.toString();
		final String expected = "<a href=\"https://www.somesite.com/page/subpage\" class=\"fsa-logo\" "
				+ "style=\"font-weight: bold\">" + "alert(&#39;attacked&#39;) title&#61;&#34;Link Name&#34; &gt;Link Name</a>";

		Assert.assertEquals(expected, actual);
	}

	@Test
	public void testRenderComponentBlankUrlHappyPath() throws ServletException, IOException
	{
		final String componentUrl = "";
		final String styleAttributes = null;
		final LinkTargets linkTarget = null;

		createMockExpectations(componentUrl, LINK_NAME, styleAttributes, linkTarget);

		componentRenderer.renderComponent(pageContext, component);
		final String actual = stringWriter.toString();
		final String expected = "<span class=\"empty-nav-item\">" + "Link Name</span>";

		Assert.assertEquals(expected, actual);
	}

	@Test
	public void testRenderComponentBlankUrlWithXSS() throws ServletException, IOException
	{
		final String componentUrl = "";
		final String styleAttributes = null;
		final LinkTargets linkTarget = null;

		createMockExpectations(componentUrl, LINK_NAME_ALERT, styleAttributes, linkTarget);

		componentRenderer.renderComponent(pageContext, component);
		final String actual = stringWriter.toString();
		final String expected = "<span class=\"empty-nav-item\">" + "Link Name</span>";

		Assert.assertEquals(expected, actual);
	}

	private void createMockExpectations(final String componentUrl, final String linkName, final String styleAttributes,
			final LinkTargets linkTarget) throws ServletException, IOException
	{
		doCallRealMethod().when(componentRenderer).renderComponent(pageContext, component);
		when(pageContext.getOut()).thenReturn(out);
		when(componentRenderer.getUrl(component)).thenReturn(componentUrl);
		when(component.getLinkName()).thenReturn(linkName);
		when(component.getStyleAttributes()).thenReturn(styleAttributes);
		when(component.getTarget()).thenReturn(linkTarget);
	}
}
