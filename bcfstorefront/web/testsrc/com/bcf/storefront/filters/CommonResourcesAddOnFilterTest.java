/*
 * Copyright (c) 2019 British Columbia Ferry Services Inc
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of British Columbia Ferry Services Inc("Confidential Information").
 * You shall not disclose such Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with British Columbia Ferry Services Inc.
 *
 * Last Modified: 30/11/18 18:06
 */

package com.bcf.storefront.filters;

import de.hybris.bootstrap.annotations.UnitTest;
import java.io.File;
import java.io.IOException;
import javax.servlet.ServletException;
import org.apache.commons.io.FilenameUtils;
import org.junit.Test;
import org.mockito.Mockito;


@UnitTest
public class CommonResourcesAddOnFilterTest extends AbstractAddOnFilterTest
{
	private static final String C_TEXT_PATH = "c.txt";
	private static final String UI_ADDONS_PATH = "/_ui/addons/";
	private static final String ABC_PATH = "/a/b/c";
	private static final String ABCC_TEXT_PATH = "/a/b/c/c.txt";
	private static final String CHANGE_HERE = "changed here";

	@Test
	public void testResourceForNotExistingTarget() throws ServletException, IOException
	{
		//create specific resource
		createResource(addOnSourceResource, "/", C_TEXT_PATH);
		prepareRequest(C_TEXT_PATH);
		prepareLocalContextPathRequest(STOREFRONT_NAME + UI_ADDONS_PATH + ADDONTWO_NAME + "/c.txt");
		filter.doFilter(request, response, filterChain);
		verifyFileCreated(webTargetResource, "/", C_TEXT_PATH);
	}

	@Test
	public void testResourceForNotExistingTargetInSubFolder() throws ServletException, IOException
	{
		//create specific resource
		createResource(addOnSourceResource, ABC_PATH, C_TEXT_PATH);
		prepareRequest(ABCC_TEXT_PATH);
		prepareLocalContextPathRequest(STOREFRONT_NAME + UI_ADDONS_PATH + ADDONTWO_NAME + ABCC_TEXT_PATH);
		filter.doFilter(request, response, filterChain);
		verifyFileCreated(webTargetResource, ABC_PATH, C_TEXT_PATH);
	}


	@Test
	public void testResourceForUpdateExistingTarget() throws ServletException, IOException, InterruptedException
	{
		//assume resource exists
		createResource(webTargetResource, "/", C_TEXT_PATH);
		waitASecond();
		//updating locally
		createResourceWithContent(addOnSourceResource, "/", C_TEXT_PATH, CHANGE_HERE);
		prepareRequest(C_TEXT_PATH);
		prepareLocalContextPathRequest(STOREFRONT_NAME + UI_ADDONS_PATH + ADDONTWO_NAME + "/c.txt");
		filter.doFilter(request, response, filterChain);
		verifyFileCreatedWithContent(webTargetResource, "/", C_TEXT_PATH, CHANGE_HERE);
	}


	@Test
	public void testResourceForUpdateExistingTargetInSubFolder() throws ServletException, IOException, InterruptedException
	{
		//assume resource exists
		createResource(webTargetResource, ABC_PATH, C_TEXT_PATH);
		waitASecond();
		//updating locally
		createResourceWithContent(addOnSourceResource, ABC_PATH, C_TEXT_PATH, CHANGE_HERE);
		prepareRequest(ABCC_TEXT_PATH);
		prepareLocalContextPathRequest(STOREFRONT_NAME + UI_ADDONS_PATH + ADDONTWO_NAME + ABCC_TEXT_PATH);
		filter.doFilter(request, response, filterChain);
		verifyFileCreatedWithContent(webTargetResource, ABC_PATH, C_TEXT_PATH, CHANGE_HERE);
	}


	@Override
	protected String getFolder()
	{
		return UI_FOLDER;
	}


	@Override
	protected void prepareRequest(final String remotePath)
	{
		final String normalized = FilenameUtils.normalize(new File(webTargetResource, remotePath).getPath(), true);
		Mockito.doReturn(normalized).when(filter).getFullPathNameFromRequest(request);
	}

}
