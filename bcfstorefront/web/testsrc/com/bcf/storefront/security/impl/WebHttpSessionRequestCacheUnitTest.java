/*
 * Copyright (c) 2019 British Columbia Ferry Services Inc
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of British Columbia Ferry Services Inc("Confidential Information").
 * You shall not disclose such Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with British Columbia Ferry Services Inc.
 *
 * Last Modified: 30/11/18 18:06
 */

package com.bcf.storefront.security.impl;

import static org.junit.Assert.assertEquals;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.acceleratorstorefrontcommons.constants.WebConstants;
import de.hybris.platform.servicelayer.session.SessionService;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;
import org.mockito.Answers;
import org.mockito.ArgumentMatcher;
import org.mockito.BDDMockito;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.web.savedrequest.DefaultSavedRequest;


@UnitTest
public class WebHttpSessionRequestCacheUnitTest
{

	private static final String ELECTRONICS_EN = "electronics/en";
	private static final String BCFSTOREFRONT_ELECTRONICS_EN = "/bcfstorefront/electronics/en";

	@InjectMocks
	private final WebHttpSessionRequestCache cache = new WebHttpSessionRequestCache();

	@Mock(answer = Answers.RETURNS_DEEP_STUBS)
	private HttpServletRequest request;

	@Mock
	private HttpServletResponse response;

	@Mock
	private SessionService sessionService;

	@Mock
	private Authentication authentication;

	@Before
	public void prepare()
	{
		MockitoAnnotations.initMocks(this);
	}


	@Test
	public void testSaveRequest()
	{

		SecurityContextHolder.getContext().setAuthentication(authentication);
		BDDMockito.given(request.getRequestURL()).willReturn(new StringBuffer("dummy"));
		BDDMockito.given(request.getScheme()).willReturn("dummy");
		BDDMockito.given(request.getHeader("referer")).willReturn("some blah");
		BDDMockito.given(request.getSession(false)).willReturn(null);

		cache.saveRequest(request, response);

		Mockito.verify(request.getSession()).setAttribute(Mockito.eq("SPRING_SECURITY_SAVED_REQUEST"),
				Mockito.argThat(new DefaultSavedRequestArgumentMatcher("some blah")));
	}


	@Test
	@Ignore
	public void testCalcRedirectUrlWithEncodingAttrs()
	{
		assertEquals(
				"/",
				executeCalculateRelativeRedirectUrl(ELECTRONICS_EN, BCFSTOREFRONT_ELECTRONICS_EN,
						"https://electronics.local:9002/bcfstorefront/electronics/en"));
		assertEquals(
				"/",
				executeCalculateRelativeRedirectUrl(ELECTRONICS_EN, BCFSTOREFRONT_ELECTRONICS_EN,
						"https://electronics.local:9002/bcfstorefront/electronics/en/"));
	}


	@Test
	@Ignore
	public void testCalcRedirectUrlWithMismatchEncodingAttrs()
	{
		assertEquals(
				ELECTRONICS_EN,
				executeCalculateRelativeRedirectUrl("electronics/ja/Y/Z", "/bcfstorefront/electronics/ja/Y/Z",
						"https://electronics.local:9002/bcfstorefront/electronics/en"));
		assertEquals(
				"/",
				executeCalculateRelativeRedirectUrl("electronics/ja/Y/Z", BCFSTOREFRONT_ELECTRONICS_EN,
						"https://electronics.local:9002/bcfstorefront/electronics/en/"));
	}

	@Test
	@Ignore
	public void testCalcRedirectUrlWithoutEncodingAttrs()
	{
		assertEquals(
				"/",
				executeCalculateRelativeRedirectUrl("", "/bcfstorefront",
						"https://electronics.local:9002/bcfstorefront"));
		assertEquals(
				"/",
				executeCalculateRelativeRedirectUrl("", "/bcfstorefront",
						"https://electronics.local:9002/bcfstorefront/"));
	}

	@Test
	@Ignore
	public void testCalcRedirectUrlWithEncodingAttrsServletPath()
	{
		assertEquals(
				"/Open-Catalogue/Cameras/Digital-Cameras/c/575",
				executeCalculateRelativeRedirectUrl(ELECTRONICS_EN, BCFSTOREFRONT_ELECTRONICS_EN,
						"https://electronics.local:9002/bcfstorefront/electronics/en/Open-Catalogue/Cameras/Digital-Cameras/c/575"));
	}

	@Test
	@Ignore
	public void testCalcRedirectUrlEmptyContextWithoutEncodingAttrs()
	{
		assertEquals("/", executeCalculateRelativeRedirectUrl("", "", "https://electronics.local:9002/"));
	}

	@Test
	@Ignore
	public void testCalcRedirectUrlEmptyContextWithEncodingAttrs()
	{
		assertEquals(
				"/",
				executeCalculateRelativeRedirectUrl(ELECTRONICS_EN, "/" + ELECTRONICS_EN,
						"https://electronics.local:9002/electronics/en"));
		assertEquals(
				"/",
				executeCalculateRelativeRedirectUrl(ELECTRONICS_EN, "/" + ELECTRONICS_EN,
						"https://electronics.local:9002/electronics/en/"));
	}

	@Test
	@Ignore
	public void testCalcRedirectUrlEmptyContextWithEncAttrsServletPath()
	{
		assertEquals(
				"/login",
				executeCalculateRelativeRedirectUrl(ELECTRONICS_EN, "/" + ELECTRONICS_EN,
						"https://electronics.local:9002/electronics/en/login"));
		assertEquals(
				"/login/",
				executeCalculateRelativeRedirectUrl(ELECTRONICS_EN, "/" + ELECTRONICS_EN,
						"https://electronics.local:9002/electronics/en/login/"));
		assertEquals(
				"/Open-Catalogue/Cameras/Hand-held-Camcorders/c/584",
				executeCalculateRelativeRedirectUrl(ELECTRONICS_EN, "/" + ELECTRONICS_EN,
						"https://electronics.local:9002/electronics/en/Open-Catalogue/Cameras/Hand-held-Camcorders/c/584"));
	}

	@Test
	@Ignore
	public void testCalcRedirectUrlEmptyContextWithoutEncAttrsServletPath()
	{
		assertEquals(
				"Open-Catalogue/Cameras/Hand-held-Camcorders/c/584",
				executeCalculateRelativeRedirectUrl("", "",
						"https://electronics.local:9002/Open-Catalogue/Cameras/Hand-held-Camcorders/c/584"));
	}

	protected String executeCalculateRelativeRedirectUrl(final String urlEncodingAttrs, final String contextPath, final String url)
	{
		BDDMockito.given(sessionService.getAttribute(WebConstants.URL_ENCODING_ATTRIBUTES)).willReturn(urlEncodingAttrs);
		return cache.calculateRelativeRedirectUrl(contextPath, url);
	}


	class DefaultSavedRequestArgumentMatcher extends ArgumentMatcher<DefaultSavedRequest>
	{

		private final String url;

		DefaultSavedRequestArgumentMatcher(final String url)
		{
			this.url = url;
		}

		@Override
		public boolean matches(final Object argument)
		{
			if (argument instanceof DefaultSavedRequest)
			{
				final DefaultSavedRequest arg = (DefaultSavedRequest) argument;
				return url.equals(arg.getRedirectUrl());
			}
			return false;
		}

	}
}
