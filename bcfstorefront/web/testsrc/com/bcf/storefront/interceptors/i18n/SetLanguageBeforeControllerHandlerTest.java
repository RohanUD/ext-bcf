/*
 * Copyright (c) 2019 British Columbia Ferry Services Inc
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of British Columbia Ferry Services Inc("Confidential Information").
 * You shall not disclose such Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with British Columbia Ferry Services Inc.
 *
 * Last Modified: 30/11/18 18:06
 */

package com.bcf.storefront.interceptors.i18n;

import de.hybris.platform.commerceservices.i18n.LanguageResolver;
import de.hybris.platform.core.model.c2l.LanguageModel;
import de.hybris.platform.servicelayer.i18n.CommonI18NService;
import javax.servlet.http.HttpServletRequest;
import org.junit.Before;
import org.junit.Test;
import org.mockito.BDDMockito;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import com.bcf.storefront.interceptors.beforecontroller.SetLanguageBeforeControllerHandler;


/**
 *
 */
public class SetLanguageBeforeControllerHandlerTest
{
	private static final String DUMMY = "dummy";

	@InjectMocks
	private final SetLanguageBeforeControllerHandler beforeControllerHandler = new SetLanguageBeforeControllerHandler();

	@Mock
	private LanguageResolver languageResolver;

	@Mock
	private CommonI18NService commonI18NService;

	@Mock
	private HttpServletRequest request;

	@Before
	public void prepare()
	{
		MockitoAnnotations.initMocks(this);
	}

	@Test
	public void testCallForNonGetRequest()
	{
		testCallForNonGetRequest("PUT");
		testCallForNonGetRequest("Put");
		testCallForNonGetRequest("put");

		testCallForNonGetRequest("POST");
		testCallForNonGetRequest("Post");
		testCallForNonGetRequest("post");

		testCallForNonGetRequest("DELETE");
		testCallForNonGetRequest("Delete");
		testCallForNonGetRequest("delete");
	}

	@Test
	public void testCallForAnyGetRequest()
	{
		testCallForGetRequest("GET");
		testCallForGetRequest("Get");
		testCallForGetRequest("get");
	}

	private void testCallForNonGetRequest(final String nonGet)
	{
		BDDMockito.given(request.getMethod()).willReturn(nonGet);
		BDDMockito.given(request.getParameter(SetLanguageBeforeControllerHandler.DEFAULT_LANG_PARAM)).willReturn(DUMMY);

		beforeControllerHandler.beforeController(request, null, null);

		Mockito.verifyZeroInteractions(commonI18NService);
		Mockito.verifyZeroInteractions(languageResolver);

		Mockito.reset(languageResolver, request, commonI18NService);
	}

	private void testCallForGetRequest(final String getMethod)
	{
		final LanguageModel lang = Mockito.mock(LanguageModel.class);

		BDDMockito.given(languageResolver.getLanguage(Mockito.anyString())).willReturn(lang);
		BDDMockito.given(request.getMethod()).willReturn(getMethod);
		BDDMockito.given(request.getParameter(SetLanguageBeforeControllerHandler.DEFAULT_LANG_PARAM)).willReturn(DUMMY);

		beforeControllerHandler.beforeController(request, null, null);

		Mockito.verify(languageResolver).getLanguage(DUMMY);
		Mockito.verify(commonI18NService).setCurrentLanguage(lang);

		Mockito.reset(languageResolver, request, commonI18NService);
	}
}
