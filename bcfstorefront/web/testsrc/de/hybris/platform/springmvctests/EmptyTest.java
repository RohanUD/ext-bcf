/*
 * Copyright (c) 2019 British Columbia Ferry Services Inc
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of British Columbia Ferry Services Inc("Confidential Information").
 * You shall not disclose such Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with British Columbia Ferry Services Inc.
 *
 * Last Modified: 30/11/18 18:06
 */

package de.hybris.platform.springmvctests;

import junit.framework.Assert;

import de.hybris.bootstrap.annotations.UnitTest;
import org.junit.Test;


@UnitTest
public class EmptyTest
{
	@Test
	public void emptyTest()
	{
		Assert.assertTrue(true);
	}
}
