module.exports = function(grunt) {
  // Project configuration.
  grunt.initConfig({
    pkg: grunt.file.readJSON('package.json'),
    watch: {
        less: {
            files: ['web/webroot/WEB-INF/_ui-src/shared/less/variableMapping.less',
                    'web/webroot/WEB-INF/_ui-src/shared/less/generatedVariables.less',
                    'web/webroot/WEB-INF/_ui-src/**/themes/**/less/variables.less',
                    'web/webroot/WEB-INF/_ui-src/**/themes/**/less/theme-variables.less',
                    'web/webroot/WEB-INF/_ui-src/**/themes/**/less/style.less',
                    'web/webroot/WEB-INF/_ui-src/addons/bcfstorefrontaddon/responsive/less/travelacc.less',
                    'web/webroot/WEB-INF/_ui-src/addons/bcfstorefrontaddon/responsive/less/components.less',
                    'web/webroot/WEB-INF/_ui-src/addons/bcfstorefrontaddon/responsive/less/landing.less',
                    'web/webroot/WEB-INF/_ui-src/addons/bcfstorefrontaddon/responsive/less/booking.less',
                    'web/webroot/WEB-INF/_ui-src/addons/bcfstorefrontaddon/responsive/less/myaccount.less',
                    'web/webroot/WEB-INF/_ui-src/addons/bcfstorefrontaddon/responsive/less/checkout.less',
                    'web/webroot/WEB-INF/_ui-src/addons/bcfstorefrontaddon/responsive/less/staticpages.less',
                    'web/webroot/WEB-INF/_ui-src/responsive/lib/ybase-*/less/*'],
            tasks: ['less'],
        },
        fonts: {
            files: ['web/webroot/_ui/addons/bcfstorefrontaddon/prototype/**/fonts/*'],
            tasks: ['sync:syncfonts'],
        },
        ybasejs: {
            files: ['web/webroot/WEB-INF/_ui-src/responsive/lib/ybase-0.1.0/js/**/*.js'],
            tasks: ['sync:syncybase'],
        },
        jquery: {
            files: ['web/webroot/WEB-INF/_ui-src/responsive/lib/jquery*.js'],
            tasks: ['sync:syncjquery'],
        },
        html: {
            files: ['web/webroot/_ui/addons/bcfstorefrontaddon/prototype/*.html'],
            tasks: ['sync:prototypesynctask'],
            options: {
                // Start a live reload server for html changes
                livereload: 54321,
            },
        },
    },
    less: {
        default: {
            files: [
                {
                    expand: true,
                    cwd: 'web/webroot/',
                    src: 'WEB-INF/_ui_src/responsive/themes/alpha/less/style.less',
                    dest: 'web/webroot/_ui/addons/bcfstorefrontaddon/prototype/assets/css/',
                    ext: '.css'
                }
            ]
        },
        uisrccompile: {
            options: {
                compress: false,
                yuicompress: false,
                optimization: 2,
                sourceMap: true,
                outputSourceFiles: true,
                sourceMapFilename: "web/webroot/_ui/addons/bcfstorefrontaddon/prototype/assets/css/style.css.map",
                sourceMapURL: "style.css.map",
                sourceMapRootpath: "../../../../",
                sourceMapBasepath: "web/webroot",
                livereload: true,
                modifyVars: {
                    'theme-images-url': '"../images"'
                }
            },

            files: [
                {
                    expand: true,
                    flatten: true,
                    cwd: 'web/webroot/',
                    src: 'WEB-INF/_ui-src/responsive/themes/alpha/less/style.less',
                    dest: 'web/webroot/_ui/addons/bcfstorefrontaddon/prototype/assets/css/',
                    ext: '.css',
                }
            ],
        },
        hybris: {
             options: {
                compress: false,
                yuicompress: false,
                optimization: 2,
                sourceMap: true,
                outputSourceFiles: true,
                sourceMapFilename: "web/webroot/_ui/responsive/theme-alpha/css/style.css.map",
                sourceMapURL: "style.css.map",
                sourceMapRootpath: "../../../../",
                sourceMapBasepath: "web/webroot"
            },
            
            files: [
                {
                    expand: true,
                    cwd: 'web/webroot/WEB-INF/_ui-src/',
                    src: '**/themes/**/less/style.less',
                    dest: 'web/webroot/_ui/',
                    ext: '.css',
                    rename:function(dest,src){
                       var nsrc = src.replace(new RegExp("/themes/(.*)/less"),"/theme-$1/css");
                       return dest+nsrc;
                    }
                }
            ]
        }
    },
    
    concat: {
        css: {
            src: 'web/webroot/_ui/addons/bcfstorefrontaddon/prototype/assets/css/**',
            dest: 'web/webroot/_ui/responsive/theme-alpha/css/style.css'
          }
    },

    sync : {
        syncfonts: {
            files: [{
                expand: true,
                cwd: 'web/webroot/WEB-INF/_ui-src/',
                src: '**/themes/**/fonts/*',
                dest: 'web/webroot/_ui/',
                rename:function(dest,src){
                    var nsrc = src.replace(new RegExp("/themes/(.*)"),"/theme-$1");
                    return dest+nsrc;
             }
            }]
        },
        syncybase: {
            files: [{
                cwd: 'web/webroot/WEB-INF/_ui-src/responsive/lib/ybase-0.1.0/js/',
                src: '**/*.js',
                dest: 'web/webroot/_ui/responsive/common/js',
            }]
        },
        syncjquery: {
            files: [{
                cwd: 'web/webroot/WEB-INF/_ui-src/responsive/lib',
                src: 'jquery*.js',
                dest: 'web/webroot/_ui/responsive/common/js',
            }]
        },
        synctheme: {
            files: [{
                cwd: 'web/webroot/WEB-INF/_ui-src/responsive/themes/travelacc/assets',
                src: ['images/**','fonts/**'],
                dest: 'web/webroot/_ui/responsive/theme-alpha',
            },{
                cwd: 'web/webroot/WEB-INF/_ui-src/responsive/themes/travelacc/assets',
                src: 'js/**',
                dest: 'web/webroot/_ui/responsive/common',
            }]
        },
        prototypesyncybase: {
            files: [{
                cwd: 'web/webroot/WEB-INF/_ui-src/responsive/lib/ybase-0.1.0/js/',
                src: '**/*.js',
                dest: 'web/webroot/_ui/responsive/common/js',
            }]
        },
        prototypesyncjquery: {
            files: [{
                cwd: 'web/webroot/WEB-INF/_ui-src/responsive/lib',
                src: 'jquery*.js',
                dest: 'web/webroot/_ui/responsive/common/js',
            }]
        },
        prototypesyncall: {
            files: [{
                cwd: 'web/webroot/WEB-INF/_ui-src/responsive/themes/travelacc',
                src: ['assets/fonts/**','assets/images/**','**.html','assets/js/**'],
                dest: 'web/webroot/prototype/',
            }]
        },
        prototypesynctask: {
            files: [{
                cwd: 'web/webroot/_ui/addons/bcfstorefrontaddon/prototype/**',
                src: '**',
                dest: 'ext-travel/bcfstorefrontaddon/acceleratoraddon/web/webroot/_ui/prototype/',
                verbose: true, // Display log messages when copying files
                // pretend: true, // Don't do any disk operations - just write log. Default: false 
                // updateAndDelete: true, // Remove all files from dest that are not found in src. Default: false
            }]
        }
    },

    serve: {
        options: {
            port: 9000,
             'serve': {
                 'path': 'web/webroot/_ui/addons/bcfstorefrontaddon/prototype'
            }
        }
    },

    uglify:{
        minifyCustomJavascript:{
          options:{
          compress:true
          },
          files:{
          'web/webroot/_ui/responsive/js/common.min.js':[
            'web/webroot/_ui/responsive/common/js/acc.address.js',
            'web/webroot/_ui/responsive/common/js/acc.autocomplete.js',
            'web/webroot/_ui/responsive/common/js/acc.carousel.js',
            'web/webroot/_ui/responsive/common/js/acc.cart.js',
            'web/webroot/_ui/responsive/common/js/acc.checkout.js',
            'web/webroot/_ui/responsive/common/js/acc.checkoutaddress.js',
            'web/webroot/_ui/responsive/common/js/acc.checkoutsteps.js',
            'web/webroot/_ui/responsive/common/js/acc.cms.js',
            'web/webroot/_ui/responsive/common/js/acc.colorbox.js',
            'web/webroot/_ui/responsive/common/js/acc.common.js',
            'web/webroot/_ui/responsive/common/js/acc.forgottenpassword.js',
            'web/webroot/_ui/responsive/common/js/acc.global.js',
            'web/webroot/_ui/responsive/common/js/acc.hopdebug.js',
            'web/webroot/_ui/responsive/common/js/acc.imagegallery.js',
            'web/webroot/_ui/responsive/common/js/acc.langcurrencyselector.js',
            'web/webroot/_ui/responsive/common/js/acc.minicart.js',
            'web/webroot/_ui/responsive/common/js/acc.navigation.js',
            'web/webroot/_ui/responsive/common/js/acc.order.js',
            'web/webroot/_ui/responsive/common/js/acc.paginationsort.js',
            'web/webroot/_ui/responsive/common/js/acc.payment.js',
            'web/webroot/_ui/responsive/common/js/acc.paymentDetails.js',
            'web/webroot/_ui/responsive/common/js/acc.pickupinstore.js',
            'web/webroot/_ui/responsive/common/js/acc.product.js',
            'web/webroot/_ui/responsive/common/js/acc.productDetail.js',
            'web/webroot/_ui/responsive/common/js/acc.quickview.js',
            'web/webroot/_ui/responsive/common/js/acc.ratingstars.js',
            'web/webroot/_ui/responsive/common/js/acc.refinements.js',
            'web/webroot/_ui/responsive/common/js/acc.silentorderpost.js',
            'web/webroot/_ui/responsive/common/js/acc.tabs.js',
            'web/webroot/_ui/responsive/common/js/acc.termsandconditions.js',
            'web/webroot/_ui/responsive/common/js/acc.track.js',
            'web/webroot/_ui/responsive/common/js/acc.storefinder.js',
            'web/webroot/_ui/responsive/common/js/acc.futurelink.js',
            'web/webroot/_ui/responsive/common/js/acc.productorderform.js',
            'web/webroot/_ui/responsive/common/js/acc.savedcarts.js',
            'web/webroot/_ui/responsive/common/js/acc.multidgrid.js',
            'web/webroot/_ui/responsive/common/js/acc.quickorder.js',
            'web/webroot/_ui/responsive/common/js/acc.quote.js',
            'web/webroot/_ui/responsive/common/js/acc.terminalListing.js',
            'web/webroot/_ui/responsive/common/js/acc.travelBookingListing.js',
            'web/webroot/_ui/responsive/common/js/bcfMultimediaTextComponent.js',
            'web/webroot/_ui/responsive/common/js/acc.ferryDetails.js',
            'web/webroot/_ui/responsive/common/js/acc.getAQuote.js',
            'web/webroot/_ui/responsive/common/js/acc.hoteldetail.js',
            'web/webroot/_ui/responsive/common/js/acc.shipinfo.js',
            'web/webroot/_ui/responsive/common/js/_autoload.js',
            'web/webroot/_ui/addons/smarteditaddon/shared/common/js/webApplicationInjector.js',
            'web/webroot/_ui/addons/smarteditaddon/shared/common/js/reprocessPage.js',
            'web/webroot/_ui/addons/smarteditaddon/shared/common/js/adjustComponentRenderingToSE.js',
            'web/webroot/_ui/addons/smarteditaddon/responsive/common/js/smarteditaddon.js',
            'web/webroot/_ui/addons/bcfassistedservicesstorefront/responsive/common/js/bcfassistedservicesstorefront.js',
            'web/webroot/_ui/addons/bcfassistedservicesstorefront/responsive/common/js/jquery.tablesorter.min.js',
            'web/webroot/_ui/addons/bcfassistedservicesstorefront/responsive/common/js/Chart.min.js',
            'web/webroot/_ui/addons/bcfassistedservicesstorefront/responsive/common/js/asm.storefinder.js',
            'web/webroot/_ui/addons/bcfstorefrontaddon/responsive/common/js/jquery.validate.min.js',
            'web/webroot/_ui/addons/bcfstorefrontaddon/responsive/common/js/jquery.ddslick.min.js',
            'web/webroot/_ui/addons/bcfstorefrontaddon/responsive/common/js/jquery.autosuggest.js',
            'web/webroot/_ui/addons/bcfstorefrontaddon/responsive/common/js/jquery.seat-charts.js',
            'web/webroot/_ui/addons/bcfstorefrontaddon/responsive/common/js/jquery.cookie.js',
            'web/webroot/_ui/addons/bcfstorefrontaddon/responsive/common/js/affix-custom.js',
            'web/webroot/_ui/addons/bcfstorefrontaddon/responsive/common/js/acc.appmodel.js',
            'web/webroot/_ui/addons/bcfstorefrontaddon/responsive/common/js/acc.services.js',
            'web/webroot/_ui/addons/bcfstorefrontaddon/responsive/common/js/acc.basketsummary.js',
            'web/webroot/_ui/addons/bcfstorefrontaddon/responsive/common/js/acc.pendingorders.js',
            'web/webroot/_ui/addons/bcfstorefrontaddon/responsive/common/js/acc.travelcommon.js',
            'web/webroot/_ui/addons/bcfstorefrontaddon/responsive/common/js/acc.formvalidation.js',
            'web/webroot/_ui/addons/bcfstorefrontaddon/responsive/common/js/acc.myaccount.js',
            'web/webroot/_ui/addons/bcfstorefrontaddon/responsive/common/js/acc.asmorderdetails.js',
            'web/webroot/_ui/addons/bcfstorefrontaddon/responsive/common/js/acc.navigation.js',
            'web/webroot/_ui/addons/bcfstorefrontaddon/responsive/common/js/acc.payment.js',
            'web/webroot/_ui/addons/bcfstorefrontaddon/responsive/common/js/acc.registration.js',
            'web/webroot/_ui/addons/bcfstorefrontaddon/responsive/common/js/acc.reservation.js',
            'web/webroot/_ui/addons/bcfstorefrontaddon/responsive/common/js/acc.addressValidation.js',
            'web/webroot/_ui/addons/bcfstorefrontaddon/responsive/common/js/acc.bookingdetails.js',
            'web/webroot/_ui/addons/bcfstorefrontaddon/responsive/common/js/_autoloadtracc.js',
            'web/webroot/_ui/addons/bcfvoyageaddon/responsive/common/js/acc.validationMessages.js',
            'web/webroot/_ui/addons/bcfvoyageaddon/responsive/common/js/acc.farefinder.js',
            'web/webroot/_ui/addons/bcfvoyageaddon/responsive/common/js/acc.custom.validation.js',
            'web/webroot/_ui/addons/bcfvoyageaddon/responsive/common/js/acc.currentconditions.js',
            'web/webroot/_ui/addons/bcfvoyageaddon/responsive/common/js/acc.dealfinder.js',
            'web/webroot/_ui/addons/bcfvoyageaddon/responsive/common/js/acc.schedulefinder.js',
            'web/webroot/_ui/addons/bcfvoyageaddon/responsive/common/js/acc.activityfinder.js',
            'web/webroot/_ui/addons/bcfvoyageaddon/responsive/common/js/acc.packagefinder.js',
            'web/webroot/_ui/addons/bcfvoyageaddon/responsive/common/js/acc.transportofferingstatus.js',
            'web/webroot/_ui/addons/bcfvoyageaddon/responsive/common/js/acc.fareselection.js',
            'web/webroot/_ui/addons/bcfvoyageaddon/responsive/common/js/acc.tripfinder.js',
            'web/webroot/_ui/addons/bcfvoyageaddon/responsive/common/js/acc.ancillary.js',
            'web/webroot/_ui/addons/bcfvoyageaddon/responsive/common/js/acc.travellerdetails.js',
            'web/webroot/_ui/addons/bcfvoyageaddon/responsive/common/js/acc.managebookings.js',
            'web/webroot/_ui/addons/bcfvoyageaddon/responsive/common/js/acc.checkin.js',
            'web/webroot/_ui/addons/bcfvoyageaddon/responsive/common/js/acc.advancePassengerInfo.js',
            'web/webroot/_ui/addons/bcfcheckoutaddon/responsive/common/js/acc.silentorderpost.js',
            'web/webroot/_ui/addons/bcfcheckoutaddon/responsive/common/js/acc.paymentType.js',
            'web/webroot/_ui/addons/bcfaccommodationsaddon/responsive/common/js/acc.accommodationfinder.js',
            'web/webroot/_ui/addons/bcfaccommodationsaddon/responsive/common/js/acc.hotelgallery.js',
            'web/webroot/_ui/addons/bcfaccommodationsaddon/responsive/common/js/acc.accommodationselection.js',
            'web/webroot/_ui/addons/bcfaccommodationsaddon/responsive/common/js/acc.accommodationdetails.js',
            'web/webroot/_ui/addons/bcfaccommodationsaddon/responsive/common/js/acc.guestdetails.js',
            'web/webroot/_ui/addons/bcfaccommodationsaddon/responsive/common/js/acc.accommodationmanagebookings.js',
            'web/webroot/_ui/addons/bcfcommonsaddon/responsive/common/js/acc.travelfinder.js',
            'web/webroot/_ui/addons/bcfcommonsaddon/responsive/common/js/acc.dealselection.js',
            'web/webroot/_ui/addons/bcfcommonsaddon/responsive/common/js/acc.packageListing.js',
            'web/webroot/_ui/addons/bcfcommonsaddon/responsive/common/js/acc.packagedetails.js',
            'web/webroot/_ui/addons/bcfcommonsaddon/responsive/common/js/acc.personaldetails.js',
            'web/webroot/_ui/addons/bcfcommonsaddon/responsive/common/js/acc.ancillaryextras.js',
            'web/webroot/_ui/addons/bcfcommonsaddon/responsive/common/js/acc.activityListing.js',
            'web/webroot/_ui/addons/bcfcommonsaddon/responsive/common/js/acc.activitydetails.js',
            'web/webroot/_ui/addons/bcfcommonsaddon/responsive/common/js/acc.optionbooking.js',
            'web/webroot/_ui/addons/bcfcommonsaddon/responsive/common/js/acc.schedules.js',
            'web/webroot/_ui/addons/bcfcommonsaddon/responsive/common/js/acc.endofdayreport.js',
            'web/webroot/_ui/addons/bcfcommonsaddon/responsive/common/js/acc.inventoryreports.js'
          ]
          }
        },
    minifyJavascriptLibrary:{
      options:{
           compress: true
      },
      files:{
            'web/webroot/_ui/responsive/js/jsLibrary.min.js':[
            'web/webroot/_ui/responsive/common/js/js-custom/tether.min.js',
            'web/webroot/_ui/responsive/common/js/js-custom/geoxml3.js',
            'web/webroot/_ui/responsive/common/js/js-custom/jquery-1.11.3.min.js',
            'web/webroot/_ui/responsive/common/js/js-custom/jquery-ui.js',
            'web/webroot/_ui/responsive/common/js/js-custom/lodash.min.js',
            'web/webroot/_ui/responsive/common/js/js-custom/popper.min.js',
            'web/webroot/_ui/responsive/common/js/js-custom/bootstrap.min.js',
            'web/webroot/_ui/responsive/common/js/js-custom/owl.carousel.min.js',
            'web/webroot/_ui/responsive/common/js/js-custom/datepicker.js',
            'web/webroot/_ui/responsive/common/js/js-custom/bootstrap.bundle.min.js',
            'web/webroot/_ui/responsive/common/js/js-custom/bootstrap-select.js',
            'web/webroot/_ui/responsive/common/js/js-custom/js-custom.js',

            'web/webroot/_ui/responsive/common/js/enquire.min.js',
            'web/webroot/_ui/responsive/common/js/Imager.min.js',
            'web/webroot/_ui/responsive/common/js/jquery.blockUI-2.66.js',
            'web/webroot/_ui/responsive/common/js/jquery.colorbox-min.js',
            'web/webroot/_ui/responsive/common/js/jquery.form.min.js',
            'web/webroot/_ui/responsive/common/js/jquery.hoverIntent.js',
            'web/webroot/_ui/responsive/common/js/jquery.pstrength.custom-1.2.0.js',
            'web/webroot/_ui/responsive/common/js/jquery.syncheight.custom.js',
            'web/webroot/_ui/responsive/common/js/jquery.tabs.custom.js',
            'web/webroot/_ui/responsive/common/js/ui-widgets.js',
            'web/webroot/_ui/responsive/common/js/jquery.zoom.custom.js',
            'web/webroot/_ui/responsive/common/js/jquery.tmpl-1.0.0pre.min.js',
            'web/webroot/_ui/responsive/common/js/jquery.currencies.min.js',
            'web/webroot/_ui/responsive/common/js/jquery.waitforimages.min.js',
            'web/webroot/_ui/responsive/common/js/jquery.slideviewer.custom.1.2.js',
            'web/webroot/_ui/responsive/common/js/datatables.min.js',
        ]}
    }
     },
     cssmin: {
	minifycss : {
		src : [
                "web/webroot/_ui/responsive/theme-bcf/css/style.css",
                "web/webroot/_ui/responsive/theme-bcf/css/media_query.css"
		      ],
		dest : "web/webroot/_ui/responsive/theme-bcf/css/stylesheet.min.css"
	}
     	}
});
 
  // Plugins
  grunt.loadNpmTasks('grunt-contrib-watch');
  grunt.loadNpmTasks('grunt-contrib-less');
  grunt.loadNpmTasks('grunt-contrib-concat');
  grunt.loadNpmTasks('grunt-contrib-uglify');
  grunt.loadNpmTasks('grunt-contrib-cssmin');
  grunt.loadNpmTasks('grunt-sync');
  grunt.loadNpmTasks('grunt-serve');

  // Default task(s).
  grunt.registerTask('default', ['less', 'sync']);

   // Compile LESS in _ui-src task(s).
  grunt.registerTask('uisrccompile', ['less:uisrccompile','serve','watch:less']);

  // Compile LESS in Hybris directory.
  grunt.registerTask('hybris', ['less:hybris','watch:less']);

  // Compile and serve HTML files in Prototype directory.
  //grunt.registerTask('')
  
  // UI Dev task(s).
  grunt.registerTask('prototype', ['sync:prototypesyncall']);
  
  // UI task to Copy Prototype changes across
  grunt.registerTask('html', ['sync:prototypesynctask']);


  // Prototype theme task(s).
  grunt.registerTask('prototypesynctheme', ['less:uisrccompile','concat:css']);

  

};
