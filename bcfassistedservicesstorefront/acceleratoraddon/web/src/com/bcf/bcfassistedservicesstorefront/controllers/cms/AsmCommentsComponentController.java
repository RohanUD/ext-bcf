/*
 * Copyright (c) 2019 British Columbia Ferry Services Inc
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of British Columbia Ferry Services Inc("Confidential Information").
 * You shall not disclose such Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with British Columbia Ferry Services Inc.
 *
 * Last Modified: 02/07/19 14:01
 */

package com.bcf.bcfassistedservicesstorefront.controllers.cms;

import de.hybris.platform.servicelayer.session.SessionService;
import java.util.List;
import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import org.apache.commons.lang3.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import com.bcf.bcfaccommodationsaddon.validators.ChangeBookingRequestValidator;
import com.bcf.bcfassistedservicesstorefront.controllers.BcfassistedservicesstorefrontControllerConstants;
import com.bcf.bcfassistedservicesstorefront.model.AsmCommentsComponentModel;
import com.bcf.bcfstorefrontaddon.constants.BcfstorefrontaddonWebConstants;
import com.bcf.bcfstorefrontaddon.controllers.cms.SubstitutingCMSAddOnComponentController;
import com.bcf.bcfstorefrontaddon.util.BcfControllerUtil;
import com.bcf.core.service.BCFTravelCartService;
import com.bcf.core.service.BcfBookingService;
import com.bcf.core.user.BcfUserService;
import com.bcf.facades.bcffacades.BCFChangeBookingRequestFacade;
import com.bcf.facades.bcffacades.BcfBookingFacade;
import com.bcf.facades.cart.AsmCommentsData;
import com.bcf.facades.helper.BcfTravelCartFacadeHelper;
import com.bcf.facades.order.BcfAsmCommentsFacade;
import com.bcf.facades.order.BcfTravelCheckoutFacade;
import com.bcf.facades.order.impl.DefaultBcfTravelCartFacade;


@Controller("AsmCommentsComponentController")
@RequestMapping(value = BcfassistedservicesstorefrontControllerConstants.Actions.Cms.AsmCommentsComponent)
public class AsmCommentsComponentController extends SubstitutingCMSAddOnComponentController<AsmCommentsComponentModel>
{

	private static final Logger LOG = Logger.getLogger(AsmCommentsComponentController.class);

	private static final String GUEST_BOOKING_DETAILS_PAGE = "/manage-booking/guest-booking-details?guestBookingIdentifier=";
	public static final String SUPPLIER_TYPE_ACTIVITY = "activity";
	@Resource(name = "bcfChangeBookingRequestFacade")
	private BCFChangeBookingRequestFacade bcfChangeBookingRequestFacade;

	@Resource(name = "changeBookingRequestValidator")
	private ChangeBookingRequestValidator changeBookingRequestValidator;

	@Resource(name = "bookingFacade")
	private BcfBookingFacade bcfBookingFacade;

	@Resource(name = "sessionService")
	private SessionService sessionService;

	@Resource(name = "bcfAsmCommentsFacade")
	private BcfAsmCommentsFacade bcfAsmCommentsFacade;

	@Resource(name = "defaultBcfTravelCartFacade")
	private DefaultBcfTravelCartFacade defaultBcfTravelCartFacade;

	@Resource(name = "bcfControllerUtil")
	private BcfControllerUtil bcfControllerUtil;

	@Resource(name = "bcfTravelCheckoutFacade")
	private BcfTravelCheckoutFacade bcfTravelCheckoutFacade;

	@Resource(name = "bcfBookingService")
	private BcfBookingService bcfBookingService;

	@Resource(name = "travelCartService")
	private BCFTravelCartService bcfTravelCartService;

	@Resource(name = "userService")
	protected BcfUserService userService;

	@Resource(name = "bcfTravelCartFacadeHelper")
	private BcfTravelCartFacadeHelper bcfTravelCartFacadeHelper;

	@Override
	protected void fillModel(final HttpServletRequest request, final Model model,
			final AsmCommentsComponentModel component)
	{
		final String orderCode = sessionService.getAttribute(BcfstorefrontaddonWebConstants.BOOKING_REFERENCE);
		model.addAttribute("comments", bcfAsmCommentsFacade.getAsmComments(orderCode));
		model.addAttribute("orderCode", orderCode);

	}

	@RequestMapping(value = "get-supplier-comment", method = RequestMethod.POST)
	public String getSupplierComment(
			@RequestParam(value = "orderCode", required = false) final String orderCode,
			@RequestParam("code") final String code, @RequestParam(value = "ref", required = false) final String ref,
			@RequestParam("type") final String type,
			final Model model, final HttpServletRequest request)
	{
		if (StringUtils.isNotBlank(code))
		{
			final List<AsmCommentsData> asmSupplierComments;
			if (SUPPLIER_TYPE_ACTIVITY.equals(type))
			{
				asmSupplierComments = bcfAsmCommentsFacade.getAsmActivitySupplierComments(code, orderCode);
			}
			else
			{
				asmSupplierComments = bcfAsmCommentsFacade.getAsmAccommodationSupplierComments(code, ref, orderCode);
			}
			model.addAttribute("comments", asmSupplierComments);
			return BcfassistedservicesstorefrontControllerConstants.Views.Fragments.CommentList.ASMCommentListTable;
		}
		return "";
	}

	@RequestMapping(value = "/activity/add-supplier-comment", method = RequestMethod.POST)
	public String addActivitySupplierComment(
			@RequestParam(value = "orderCode", required = false) final String orderCode,
			@RequestParam("code") final String code,
			@RequestParam(value = "ref", required = false) final String ref,
			@RequestParam("comment") final String comment,
			final Model model, final HttpServletRequest request)
	{
		if (StringUtils.isNotBlank(code) && StringUtils.isNotBlank(comment))
		{
			bcfAsmCommentsFacade.addAsmActivitySupplierComment(code, comment, orderCode);
			final List<AsmCommentsData> asmActivitySupplierComments = bcfAsmCommentsFacade
					.getAsmActivitySupplierComments(code, orderCode);
			model.addAttribute("comments", asmActivitySupplierComments);
		}
		return BcfassistedservicesstorefrontControllerConstants.Views.Fragments.CommentList.ASMCommentListTable;
	}

	@RequestMapping(value = "/accommodation/add-supplier-comment", method = RequestMethod.POST)
	public String addAccommodationSupplierComment(
			@RequestParam(value = "orderCode", required = false) final String orderCode,
			@RequestParam("code") final String code,
			@RequestParam(value = "ref", required = false) final String ref,
			@RequestParam("comment") final String comment,
			final Model model, final HttpServletRequest request)
	{
		if (StringUtils.isNotBlank(code) && StringUtils.isNotBlank(comment))
		{
			bcfAsmCommentsFacade.addAsmAccommodationSupplierComment(code, ref, comment, orderCode);
			final List<AsmCommentsData> asmAccommodationSupplierComments = bcfAsmCommentsFacade
					.getAsmAccommodationSupplierComments(code, ref, orderCode);
			model.addAttribute("comments", asmAccommodationSupplierComments);
		}
		return BcfassistedservicesstorefrontControllerConstants.Views.Fragments.CommentList.ASMCommentListTable;
	}

	@RequestMapping(value = "/add-internal-comment", method = RequestMethod.POST)
	public String addInternalComment(
			@RequestParam("orderCode") final String orderCode,
			@RequestParam("comment") final String comment,
			final Model model, final HttpServletRequest request)
	{
		if (!StringUtils.isEmpty(comment))
		{
			bcfAsmCommentsFacade.addAsmComment(orderCode, comment);
			model.addAttribute("comments", bcfAsmCommentsFacade.getAsmComments(orderCode));
		}
		return BcfassistedservicesstorefrontControllerConstants.Views.Fragments.CommentList.ASMCommentListTable;
	}
}
