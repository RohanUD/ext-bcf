/*
 * [y] hybris Platform
 *
 * Copyright (c) 2017 SAP SE or an SAP affiliate company.
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of SAP
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with SAP.
 */
package com.bcf.bcfassistedservicesstorefront.controllers.cms;

import de.hybris.platform.acceleratorstorefrontcommons.controllers.pages.AbstractPageController;
import de.hybris.platform.acceleratorstorefrontcommons.controllers.util.GlobalMessages;
import de.hybris.platform.assistedservicefacades.constants.AssistedservicefacadesConstants;
import de.hybris.platform.assistedserviceservices.AssistedServiceService;
import de.hybris.platform.assistedserviceservices.exception.AssistedServiceException;
import de.hybris.platform.assistedserviceservices.exception.AssistedServiceWrongCustomerIdException;
import de.hybris.platform.cms2.exceptions.CMSItemNotFoundException;
import de.hybris.platform.cms2.model.pages.ContentPageModel;
import de.hybris.platform.commercefacades.travel.GlobalTravelReservationData;
import de.hybris.platform.commercefacades.user.data.CustomerData;
import de.hybris.platform.commerceservices.enums.SalesApplication;
import de.hybris.platform.core.model.order.CartModel;
import de.hybris.platform.core.model.user.CustomerModel;
import de.hybris.platform.core.model.user.UserGroupModel;
import de.hybris.platform.core.model.user.UserModel;
import de.hybris.platform.enumeration.EnumerationService;
import de.hybris.platform.jalo.JaloSession;
import de.hybris.platform.order.CartService;
import de.hybris.platform.servicelayer.session.SessionService;
import de.hybris.platform.servicelayer.user.UserService;
import de.hybris.platform.travelservices.enums.BookingJourneyType;
import java.io.UnsupportedEncodingException;
import java.util.Objects;
import java.util.Set;
import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.http.HttpStatus;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.SessionAttributes;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;
import com.bcf.bcfassistedservicesstorefront.constants.BcfassistedservicesstorefrontConstants;
import com.bcf.bcfassistedservicesstorefront.enums.ASMSalesChannels;
import com.bcf.bcfassistedservicesstorefront.redirect.AssistedServiceRedirectStrategy;
import com.bcf.bcfassistedservicesstorefront.security.AssistedServiceAgentAuthoritiesManager;
import com.bcf.bcfassistedservicesstorefront.security.impl.AssistedServiceAgentLoginStrategy;
import com.bcf.bcfassistedservicesstorefront.security.impl.AssistedServiceAgentLogoutStrategy;
import com.bcf.bcfassistedservicesstorefront.security.impl.AssistedServiceAuthenticationToken;
import com.bcf.bcfassistedservicesstorefront.util.FormPopulatorFromCart;
import com.bcf.bcfcommonsaddon.forms.cms.TravelFinderForm;
import com.bcf.bcfstorefrontaddon.constants.BcfstorefrontaddonWebConstants;
import com.bcf.core.constants.BcfCoreConstants;
import com.bcf.facades.assistedservicefacades.BCFAssistedServiceFacade;
import com.bcf.facades.bcffacades.BCFGlobalReservationFacade;
import com.bcf.facades.constants.BcfFacadesConstants;
import com.bcf.facades.customer.search.CustomerSearchData;
import com.bcf.facades.customer.search.SearchParams;
import com.bcf.facades.order.BcfTravelCartFacade;
import com.bcf.facades.quote.BcfQuoteFacade;
import com.bcf.facades.reservation.data.BcfGlobalReservationData;
import com.bcf.integrations.core.exception.IntegrationException;
import com.sap.security.core.server.csi.XSSEncoder;


@Controller
@RequestMapping(value = "/assisted-service")
@SessionAttributes(BcfFacadesConstants.TRAVEL_FINDER_FORM)
public class AssistedServiceComponentController extends AbstractPageController
{
	private static final String ASSISTED_SERVICE_COMPONENT = "addon:/bcfassistedservicesstorefront/cms/asm/assistedServiceComponent";

	private static final String ASM_MESSAGE_ATTRIBUTE = "asm_message";
	private static final String ASM_REDIRECT_URL_ATTRIBUTE = "redirect_url";
	private static final String ASM_ALERT_CLASS = "asm_alert_class";
	private static final String ENABLE_360_VIEW = "enable360View";
	private static final String CUSTOMER_ID = "customerId";
	private static final String CART_ID = "cartId";
	private static final String CUSTOMER_NAME = "customerName";
	private static final String EBOOKING_REFERENCE_NUMBER = "eBookingRefNumber";
	private static final String NO_CUSTOMER_FOUND = "crm.customer.not.found";
	private static final String PIN_PAD_ID = "pinPadID";
	private static final String ICE_BAR_ID = "iceBarID";

	private static final String SEARCH_RESULTS_CMS_PAGE = "asmSearchResultsPage";

	private static final Logger LOG = Logger.getLogger(AssistedServiceComponentController.class);

	@Resource(name = "assistedServiceFacade")
	private BCFAssistedServiceFacade assistedServiceFacade;

	@Resource(name = "userService")
	private UserService userService;

	@Resource(name = "cartService")
	private CartService cartService;

	@Resource(name = "assistedServiceAgentLoginStrategy")
	private AssistedServiceAgentLoginStrategy assistedServiceAgentLoginStrategy;

	@Resource(name = "assistedServiceAgentLogoutStrategy")
	private AssistedServiceAgentLogoutStrategy assistedServiceAgentLogoutStrategy;

	@Resource(name = "sessionService")
	private SessionService sessionService;

	@Resource(name = "assistedServiceRedirectStrategy")
	private AssistedServiceRedirectStrategy assistedServiceRedirectStrategy;

	@Resource(name = "assistedServiceAgentAuthoritiesManager")
	private AssistedServiceAgentAuthoritiesManager authoritiesManager;

	@Resource(name = "bcfCartFacade")
	private BcfTravelCartFacade cartFacade;

	@Resource(name = "globalReservationFacade")
	private BCFGlobalReservationFacade globalReservationFacade;

	@Resource(name = "formPopulatorFromCart")
	private FormPopulatorFromCart formPopulatorFromCart;

	@Resource(name = "assistedServiceService")
	private AssistedServiceService assistedServiceService;

	@Resource(name = "enumerationService")
	private EnumerationService enumerationService;

	@Resource(name = "bcfQuoteFacade")
	private BcfQuoteFacade quoteFacade;


	@RequestMapping(value = "/quit", method = RequestMethod.POST)
	@ResponseStatus(HttpStatus.OK)
	public void quitAssistedServiceMode()
	{
		assistedServiceFacade.quitAssistedServiceMode();
	}

	@RequestMapping(value = "/login", method = RequestMethod.POST)
	public String loginAssistedServiceAgent(final Model model, final HttpServletRequest request,
			final HttpServletResponse response, @RequestParam("username") final String username,
			@RequestParam("password") final String password, @RequestParam("salesChannel") final String salesChannel,
			@RequestParam("pinPadID") final String pinPadID, @RequestParam("iceBarID") final String iceBarID)
	{
		try
		{
			assistedServiceFacade.authenticateAndCheckPrivileges(username, password, salesChannel);
			assistedServiceAgentLoginStrategy.login(username, request, response);
			sessionService.setAttribute(BcfCoreConstants.SALES_CHANNEL, salesChannel);
			sessionService.setAttribute(BcfCoreConstants.SALES_CHANNEL_NAME,
					enumerationService.getEnumerationName(ASMSalesChannels.valueOf(salesChannel)));
			assistedServiceFacade.emulateAfterLogin();
			refreshSpringSecurityToken();
			setSessionTimeout();
			model.addAttribute(ASM_REDIRECT_URL_ATTRIBUTE, assistedServiceRedirectStrategy.getRedirectPath());

			if (StringUtils.equalsIgnoreCase(SalesApplication.CALLCENTER.getCode(), salesChannel))
			{
				sessionService.setAttribute(ICE_BAR_ID, iceBarID);
				sessionService.removeAttribute(PIN_PAD_ID);
			}
			else
			{
				sessionService.setAttribute(PIN_PAD_ID, pinPadID);
				sessionService.setAttribute(ICE_BAR_ID, iceBarID);
			}
			model.addAllAttributes(assistedServiceFacade.getAssistedServiceSessionAttributes());

		}
		catch (final AssistedServiceException e)
		{
			model.addAttribute(ASM_MESSAGE_ATTRIBUTE, e.getMessageCode());
			model.addAttribute(ASM_ALERT_CLASS, e.getAlertClass());
			model.addAttribute(ASMSalesChannels._TYPECODE, assistedServiceFacade.getAsmSalesChannels());
			model.addAttribute("username", this.encodeValue(username));
			LOG.error(e.getMessage(), e);
		}
		return ASSISTED_SERVICE_COMPONENT;
	}

	@RequestMapping(value = "/logoutasm", method = RequestMethod.POST)
	public String logoutAssistedServiceAgent(final Model model, final HttpServletRequest request)
	{
		try
		{
			assistedServiceFacade.logoutAssistedServiceAgent();
			sessionService.removeAttribute(PIN_PAD_ID);
			sessionService.removeAttribute(ICE_BAR_ID);
			sessionService.removeAttribute(BcfCoreConstants.SALES_CHANNEL);

			authoritiesManager.restoreInitialAuthorities();
			assistedServiceFacade.stopEmulateCustomer();
		}
		catch (final AssistedServiceException e)
		{
			model.addAttribute(ASM_MESSAGE_ATTRIBUTE, e.getMessage());
			LOG.debug(e.getMessage(), e);
		}
		model.addAllAttributes(assistedServiceFacade.getAssistedServiceSessionAttributes());
		model.addAttribute("customerReload", "reload");
		assistedServiceAgentLogoutStrategy.logout(request);
		return ASSISTED_SERVICE_COMPONENT;
	}

	@RequestMapping(value = "/personify-customer", method = RequestMethod.POST)
	public String emulateCustomer(final Model model, @RequestParam("customerId") final String customerId,
			@RequestParam("customerName") final String customerName, @RequestParam("cartId") final String cartId)
	{

		try
		{
			if(StringUtils.isNotBlank(customerId)){
				final UserModel customer = assistedServiceService.getCustomer(customerId);
				boolean guestind=false;

				if(customer!=null && customer instanceof CustomerModel){
					guestind=((CustomerModel)customer).isConsumer();
				}
				assistedServiceFacade.getCustomer(customerId,guestind,"");
			}
			assistedServiceFacade.emulateCustomer(customerId, cartId, null);
			refreshSpringSecurityToken();
			model.addAttribute(ASM_REDIRECT_URL_ATTRIBUTE, assistedServiceRedirectStrategy.getRedirectPath());
		}
		catch (final AssistedServiceException e)
		{
			model.addAttribute(ASM_MESSAGE_ATTRIBUTE, e.getMessageCode());
			model.addAttribute(ASM_ALERT_CLASS, e.getAlertClass());
			model.addAttribute(CUSTOMER_ID, this.encodeValue(customerId));
			model.addAttribute(CART_ID, this.encodeValue(cartId));
			model.addAttribute(CUSTOMER_NAME, this.encodeValue(customerName));
			LOG.debug(e.getMessage(), e);
		}
		catch (final IntegrationException | UnsupportedEncodingException ie)
		{
			model.addAttribute(ASM_MESSAGE_ATTRIBUTE, ie.getMessage());
			model.addAttribute(CUSTOMER_ID, this.encodeValue(customerId));
			model.addAttribute(CART_ID, this.encodeValue(cartId));
			model.addAttribute(CUSTOMER_NAME, this.encodeValue(customerName));
			LOG.debug(ie.getMessage(), ie);
		}


		model.addAllAttributes(assistedServiceFacade.getAssistedServiceSessionAttributes());
		return ASSISTED_SERVICE_COMPONENT;
	}

	@RequestMapping(value = "/linkcart", method = RequestMethod.POST)
	public String linkCart(final Model model, @RequestParam("customerId") final String customerId,
			@RequestParam("cartId") final String cartId)
	{
		try
		{
			assistedServiceFacade.emulateCustomer(customerId, cartId);
			refreshSpringSecurityToken();
			if (Objects.isNull(sessionService.getAttribute(AssistedservicefacadesConstants.ASM_ORDER_ID_TO_REDIRECT)))
			{
				final BcfGlobalReservationData bcfGlobalReservationData = globalReservationFacade.getCurrentGlobalReservationData();

				if(!BookingJourneyType.BOOKING_ALACARTE.getCode().equals(bcfGlobalReservationData.getBookingJourneyType())){
					final GlobalTravelReservationData globalTravelReservationData = bcfGlobalReservationData.getPackageReservations()
							.getPackageReservationDatas().stream().findFirst().get();
					final TravelFinderForm travelFinderForm = formPopulatorFromCart
							.initializeTravelFinderForm(globalTravelReservationData);

					model.addAttribute(BcfstorefrontaddonWebConstants.TRAVEL_FINDER_FORM, travelFinderForm);
					sessionService.setAttribute(BcfCoreConstants.CART_ENTRY_JOURNEY_REF_NUM, Integer.parseInt(
							globalTravelReservationData.getAccommodationReservationData().getJourneyRef()));
					cartFacade.recalculateCart();
				}
			}

			model.addAttribute(ASM_REDIRECT_URL_ATTRIBUTE, assistedServiceRedirectStrategy.getRedirectPath());
		}
		catch (final AssistedServiceException e)
		{
			model.addAttribute(ASM_MESSAGE_ATTRIBUTE, e.getMessageCode());
			model.addAttribute(ASM_ALERT_CLASS, e.getAlertClass());
			model.addAttribute(CUSTOMER_ID, this.encodeValue(customerId));
			model.addAttribute(CART_ID, this.encodeValue(cartId));
			LOG.error(e.getMessage(), e);
		}
		model.addAllAttributes(assistedServiceFacade.getAssistedServiceSessionAttributes());
		return ASSISTED_SERVICE_COMPONENT;
	}

	@RequestMapping(value = "/emulateByeBooking", method = RequestMethod.GET)
	public String emulateCustomerByeBookingRef(final RedirectAttributes redirectAttrs,
			@RequestParam(value = "eBookingRefNumber", required = false) final String eBookingRefNumber, final Model model)
	{
		try
		{
			// launch AS mode in case it has not been launched yet
			if (!assistedServiceFacade.isAssistedServiceModeLaunched())
			{
				LOG.debug("ASM launched after link-emulate request");
				assistedServiceFacade.launchAssistedServiceMode();
			}

			if (assistedServiceFacade.isAssistedServiceAgentLoggedIn())
			{
				assistedServiceFacade.stopEmulateCustomer();
				refreshSpringSecurityToken();
				LOG.debug(String.format("Previous emulation for customerId:[%s] has been stopped.",
						userService.getCurrentUser().getUid()));
			}

			assistedServiceFacade.emulateByeBookingReference(eBookingRefNumber);
			refreshSpringSecurityToken();
		}
		catch (final AssistedServiceException e)
		{
			redirectAttrs.addFlashAttribute(ASM_MESSAGE_ATTRIBUTE, e.getMessageCode());
			redirectAttrs.addFlashAttribute(ASM_ALERT_CLASS, "ASM_alert_eBookingRef");
			redirectAttrs.addFlashAttribute(EBOOKING_REFERENCE_NUMBER, this.encodeValue(eBookingRefNumber));
			LOG.error(e.getMessage(), e);
			//model.addAllAttributes(assistedServiceFacade.getAssistedServiceSessionAttributes());
			return REDIRECT_PREFIX + assistedServiceRedirectStrategy.getErrorRedirectPath();
		}
		return REDIRECT_PREFIX + assistedServiceRedirectStrategy.getRedirectPath();
	}

	@RequestMapping(value = "/emulate", method = RequestMethod.GET)
	public String emulateCustomerByLink(final RedirectAttributes redirectAttrs,
			@RequestParam(value = "customerId", required = false) final String customerId,
			@RequestParam(value = "cartId", required = false) final String cartId,
			@RequestParam(value = "orderId", required = false) final String orderId,
			@RequestParam(value = "guestInd", required = false) final boolean guestInd,
			@RequestParam(value = "fwd", required = false) final String fwd,
			@RequestParam(value = "enable360View", required = false, defaultValue = "false") final boolean enable360View)
	{
		try
		{
			// launch AS mode in case it has not been launched yet
			if (!assistedServiceFacade.isAssistedServiceModeLaunched())
			{
				LOG.debug("ASM launched after link-emulate request");
				assistedServiceFacade.launchAssistedServiceMode();
			}

			if (assistedServiceFacade.isAssistedServiceAgentLoggedIn())
			{
				assistedServiceFacade.stopEmulateCustomer();
				refreshSpringSecurityToken();
				LOG.debug(String.format("Previous emulation for customerId:[%s] has been stopped.",
						userService.getCurrentUser().getUid()));
			}

			//only set the flash attribute if this value is true and will only happen in case 360 view is initiated from customer list
			if (enable360View)
			{
				redirectAttrs.addFlashAttribute(ENABLE_360_VIEW, Boolean.valueOf(enable360View));
			}

			assistedServiceFacade.emulateCustomer(customerId, cartId, orderId);

			LOG.debug(String.format(
					"Link-emulate request successfuly started an emulation with parameters: customerId:[%s], cartId:[%s]", customerId,
					cartId));
			refreshSpringSecurityToken();

			if(quoteFacade.isQuote()){

				return REDIRECT_PREFIX + "/quote/"+cartId+"/edit?isSaveCartCurrentSession=true";
			}
			return REDIRECT_PREFIX + assistedServiceRedirectStrategy.getRedirectPath();
		}
		catch (final AssistedServiceWrongCustomerIdException ex)
		{
			//Get customer from CRM and create in Hybris
			try
			{
				assistedServiceFacade.getCustomer(customerId, guestInd, "");
				assistedServiceFacade.emulateCustomer(customerId, cartId, orderId);
				refreshSpringSecurityToken();
			}
			catch (final IntegrationException e)
			{
				LOG.error(e.getMessage(), e);
				AddErrorAttributes(redirectAttrs, customerId, cartId, "asm.emulate.error.customer", "ASM_alert_customer");
			}
			catch (final UnsupportedEncodingException e)
			{
				LOG.error(e.getMessage(), e);
			}
			catch (final AssistedServiceException e)
			{
				LOG.debug(e.getMessage(), e);
				AddErrorAttributes(redirectAttrs, customerId, cartId, e.getMessageCode(), e.getAlertClass());
			}
		}
		catch (final AssistedServiceException e)
		{
			LOG.debug(e.getMessage(), e);
			AddErrorAttributes(redirectAttrs, customerId, cartId, e.getMessageCode(), e.getAlertClass());
		}
		return REDIRECT_PREFIX + assistedServiceRedirectStrategy.getErrorRedirectPath();
	}

	protected void AddErrorAttributes(final RedirectAttributes redirectAttrs, final String customerId, final String cartId,
			final String messageCode, final String alertClass)
	{
		redirectAttrs.addFlashAttribute(ASM_MESSAGE_ATTRIBUTE, messageCode);
		redirectAttrs.addFlashAttribute(ASM_ALERT_CLASS, alertClass);
		redirectAttrs.addFlashAttribute(CUSTOMER_ID, this.encodeValue(customerId));
		redirectAttrs.addFlashAttribute(CUSTOMER_NAME, this.encodeValue(customerId));
		redirectAttrs.addFlashAttribute(CART_ID, this.encodeValue(cartId));
	}

	@RequestMapping(value = "/create-account", method = RequestMethod.POST)
	public String createCustomer(final Model model, @RequestParam("customerId") final String customerId,
			@RequestParam("customerName") final String customerName)
	{
		String redirectTo = ASSISTED_SERVICE_COMPONENT;
		try
		{
			assistedServiceFacade.createCustomer(customerId, customerName);
			// customerId is lower cased when user registered using customer account service
			final String customerIdLowerCased = customerId.toLowerCase();
			final CartModel sessionCart = cartService.getSessionCart();
			if (sessionCart != null && userService.isAnonymousUser(sessionCart.getUser())
					&& CollectionUtils.isNotEmpty(sessionCart.getEntries()))
			{
				// if there's already an anonymous cart with entries - bind it to customer
				bindCart(customerIdLowerCased, null, model);
				redirectTo = emulateCustomer(model, customerIdLowerCased, null, cartService.getSessionCart().getCode());
			}
			else
			{
				redirectTo = emulateCustomer(model, customerIdLowerCased, null, null);
			}
		}
		catch (final AssistedServiceException e)
		{
			if (e.getMessage() != null && e.getMessage().toLowerCase().contains("duplicate"))
			{
				model.addAttribute(ASM_MESSAGE_ATTRIBUTE, "asm.createCustomer.duplicate.error");
				model.addAttribute(ASM_ALERT_CLASS, "ASM_alert_customer ASM_alert_create_new");
			}
			else
			{
				model.addAttribute(ASM_MESSAGE_ATTRIBUTE, "asm.createCustomer.error");
			}
			model.addAttribute(CUSTOMER_ID, this.encodeValue(customerId));
			model.addAttribute(CUSTOMER_NAME, this.encodeValue(customerName + ", " + customerId));
			LOG.debug(e.getMessage(), e);
		}
		model.addAllAttributes(assistedServiceFacade.getAssistedServiceSessionAttributes());
		return redirectTo;
	}

	@RequestMapping(value = "/personify-stop", method = RequestMethod.POST)
	public String endEmulateCustomer(final Model model)
	{
		authoritiesManager.restoreInitialAuthorities();
		assistedServiceFacade.stopEmulateCustomer();
		refreshSpringSecurityToken();
		model.addAllAttributes(assistedServiceFacade.getAssistedServiceSessionAttributes());
		model.addAttribute(ASM_REDIRECT_URL_ATTRIBUTE, "/");
		return ASSISTED_SERVICE_COMPONENT;
	}

	@RequestMapping(value = "/resetSession", method = RequestMethod.POST)
	@ResponseStatus(HttpStatus.OK)
	public void resetSession()
	{
		return;
	}

	@RequestMapping(value = "/searchUser", method = RequestMethod.GET)
	public String searchUser(@RequestParam("customerId") final String customerId,
			@RequestParam("mobileNumber") final String mobileNumber, @RequestParam("firstName") final String firstName,
			@RequestParam("lastName") final String lastName, @RequestParam("emailAddress") final String emailAddress,
			@RequestParam("companyName") final String companyName, @RequestParam("callback") final String callback,
			@RequestParam("lastIndex") final int lastIndex, final RedirectAttributes redirectAttributes, final Model model)
			throws CMSItemNotFoundException
	{
		try
		{
			final SearchParams searchParams = populateSearchParams(firstName, lastName, mobileNumber, emailAddress, companyName,
					lastIndex);
			final CustomerSearchData customerSearchData = assistedServiceFacade.searchCustomer(searchParams);
			model.addAttribute("customerData", customerSearchData);
			model.addAttribute("firstName", firstName);
			model.addAttribute("lastName", lastName);
			model.addAttribute("emailAddress", emailAddress);
			model.addAttribute("mobileNumber", mobileNumber);
			model.addAttribute("companyName", companyName);
		}
		catch (final AssistedServiceException e)
		{
			GlobalMessages.addErrorMessage(model, "text.unsubscribe.customerNotFound.label");
			// just do nothing and return empty string
			LOG.debug(e);
		}
		catch (final IntegrationException e)
		{
			//Show error message to the user.
			GlobalMessages.addErrorMessage(model, "text.unsubscribe.integrationExceptions.label");
			LOG.error(e);
		}

		return returnToPage(model, SEARCH_RESULTS_CMS_PAGE);
	}

	protected String returnToPage(final Model model, final String pageId) throws CMSItemNotFoundException
	{
		final ContentPageModel contentPage = getContentPageForLabelOrId(pageId);
		storeCmsPageInModel(model, contentPage);
		setUpMetaDataForContentPage(model, contentPage);

		return getViewForPage(model);
	}

	private SearchParams populateSearchParams(final String firstName, final String lastName, final String mobileNumber,
			final String emailAddress, final String companyName, final int lastIndex)
	{
		final SearchParams searchParams = new SearchParams();
		searchParams.setFirstName(firstName);
		searchParams.setLastName(lastName);
		searchParams.setEmailAddress(emailAddress);
		searchParams.setMobileNumber(mobileNumber);
		searchParams.setCompanyName(companyName);
		searchParams.setLastIndex(lastIndex);
		searchParams.setLastPageInd(true);

		return searchParams;
	}

	private void appendCustomer(final StringBuilder autocompleteResult, final CustomerData customer)
	{
		try
		{
				autocompleteResult.append(getCustomerJSON(customer));
		}
		catch (final UnsupportedEncodingException e)
		{
			LOG.error("Error occured during encoding customer data: " + customer.getUid(), e);
		}
	}

	protected String getCustomerJSON(final CustomerData customer) throws UnsupportedEncodingException
	{
		return String.format("{email:'%s',fName:'%s',lName:'%s',companyName:'%s'", XSSEncoder.encodeJavaScript(customer.getUid()),
				XSSEncoder.encodeJavaScript(customer.getFirstName()), XSSEncoder.encodeJavaScript(customer.getLastName()),
				XSSEncoder.encodeJavaScript(customer.getCompanyName()));
	}

	@RequestMapping(value = "/bind-cart", method = RequestMethod.POST)
	public String bindCart(@RequestParam(value = "customerId", required = false) final String customerId,
			@RequestParam(value = "cartId", required = false) final String cartId, final Model model)
	{
		try
		{
			assistedServiceFacade.bindCustomerToCart(customerId, cartId);
			refreshSpringSecurityToken();
			model.addAttribute(ASM_REDIRECT_URL_ATTRIBUTE, "/");
		}
		catch (final AssistedServiceException e)
		{
			model.addAttribute(ASM_MESSAGE_ATTRIBUTE, e.getMessage());
			model.addAttribute(CUSTOMER_ID, this.encodeValue(customerId));
			LOG.debug(e.getMessage(), e);
		}
		model.addAllAttributes(assistedServiceFacade.getAssistedServiceSessionAttributes());
		return ASSISTED_SERVICE_COMPONENT;
	}

	@RequestMapping(value = "/add-to-cart", method = RequestMethod.POST)
	public String addToCartEventHandler(final Model model)
	{
		try
		{
			// since cart isn't empty anymore - emulate mode should be on
			assistedServiceFacade
					.emulateCustomer(userService.getCurrentUser().getUid(), cartService.getSessionCart().getCode(), null);
		}
		catch (final AssistedServiceException e)
		{
			LOG.debug(e.getMessage(), e);
			return null; // there will be 'page not found' response in case of exception
		}
		return refresh(model);
	}

	@RequestMapping(value = "/refresh", method = RequestMethod.POST)
	public String refresh(final Model model)
	{
		model.addAllAttributes(assistedServiceFacade.getAssistedServiceSessionAttributes());
		return ASSISTED_SERVICE_COMPONENT;
	}


	protected void setSessionTimeout()
	{
		JaloSession.getCurrentSession().setTimeout(assistedServiceFacade.getAssistedServiceSessionTimeout());
		// since agent is logged in - change session timeout to the value from properties
		((ServletRequestAttributes) RequestContextHolder.currentRequestAttributes()).getRequest().getSession()
				.setMaxInactiveInterval(assistedServiceFacade.getAssistedServiceSessionTimeout()); // in seconds
	}

	/**
	 * This method should be called after any facade method where user substitution may occur
	 */
	protected void refreshSpringSecurityToken()
	{
		final Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
		if (authentication instanceof AssistedServiceAuthenticationToken)
		{
			final UserModel currentUser = userService.getCurrentUser();
			if (currentUser == null || userService.isAnonymousUser(currentUser) || isASAgent(currentUser))
			{
				((AssistedServiceAuthenticationToken) authentication).setEmulating(false);
			}
			else
			{
				((AssistedServiceAuthenticationToken) authentication).setEmulating(true);
				authoritiesManager.addCustomerAuthoritiesToAgent(currentUser.getUid());
			}
		}
	}

	protected boolean isASAgent(final UserModel currentUser)
	{
		final Set<UserGroupModel> userGroups = userService.getAllUserGroupsForUser(currentUser);
		for (final UserGroupModel userGroup : userGroups)
		{
			if (BcfassistedservicesstorefrontConstants.AS_AGENT_GROUP_UID.equals(userGroup.getUid()))
			{
				return true;
			}
		}
		return false;
	}

	protected String encodeValue(final String inputValue)
	{
		final String trimmedInputValue = StringUtils.isEmpty(inputValue) ? "" : inputValue.trim();

		try
		{
			return XSSEncoder.encodeHTML(trimmedInputValue);
		}
		catch (final UnsupportedEncodingException e)
		{
			LOG.error("Error occured during encoding the input value: " + inputValue, e);
		}
		return null;
	}
}
