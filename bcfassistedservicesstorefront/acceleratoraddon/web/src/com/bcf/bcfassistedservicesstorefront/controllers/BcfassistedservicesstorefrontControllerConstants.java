/*
 * [y] hybris Platform
 *
 * Copyright (c) 2017 SAP SE or an SAP affiliate company.
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of SAP
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with SAP.
 */
package com.bcf.bcfassistedservicesstorefront.controllers;



import com.bcf.bcfassistedservicesstorefront.model.AsmCommentsComponentModel;
import com.bcf.bcfassistedservicesstorefront.model.AsmDevicesUsedComponentModel;
import com.bcf.bcfassistedservicesstorefront.model.AsmFavoriteColorsComponentModel;


public interface BcfassistedservicesstorefrontControllerConstants
{
	String ADDON_PREFIX = "addon:/bcfassistedservicesstorefront/";

	// implement here controller constants used by this extension

	interface Views
	{

		interface Fragments
		{
			interface CommentList
			{
				String ASMCommentListTable = ADDON_PREFIX + "fragments/asmCommentListTable";
			}

			interface CustomerListComponent
			{
				String ASMCustomerListPopup = ADDON_PREFIX + "fragments/asmCustomerListComponent";
				String ASMCustomerListTable = ADDON_PREFIX + "fragments/asmCustomerListTable";
			}

		}

	}

	interface Actions
	{
		interface Cms
		{
			String _Prefix = "/view/"; // NOSONAR
			String _Suffix = "Controller"; // NOSONAR

			String AsmDevicesUsedComponent = _Prefix + AsmDevicesUsedComponentModel._TYPECODE + _Suffix; // NOSONAR
			String AsmFavoriteColorsComponent = _Prefix + AsmFavoriteColorsComponentModel._TYPECODE + _Suffix; // NOSONAR
			String AsmCommentsComponent = _Prefix + AsmCommentsComponentModel._TYPECODE + _Suffix; // NOSONAR
		}
	}
}
