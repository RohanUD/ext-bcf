/*
 * Copyright (c) 2019 British Columbia Ferry Services Inc
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of British Columbia Ferry Services Inc("Confidential Information").
 * You shall not disclose such Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with British Columbia Ferry Services Inc.
 *
 * Last Modified: 28/06/19 16:13
 */

package com.bcf.bcfassistedservicesstorefront.controllers;

import de.hybris.platform.acceleratorstorefrontcommons.controllers.AbstractController;
import javax.annotation.Resource;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import com.bcf.facades.order.BcfTravelCartFacade;


@Controller
@RequestMapping(value = "clear-cart")
public class ASMClearCartController extends AbstractController
{

	@Resource(name = "bcfCartFacade")
	private BcfTravelCartFacade bcfTravelCartFacade;

	@RequestMapping(method = RequestMethod.GET)
	public String clearCart(final Model model)
	{
		bcfTravelCartFacade.removeSessionCart();
		return REDIRECT_PREFIX + ROOT;
	}
}
