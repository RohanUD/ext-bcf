/*
 * Copyright (c) 2019 British Columbia Ferry Services Inc
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of British Columbia Ferry Services Inc("Confidential Information").
 * You shall not disclose such Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with British Columbia Ferry Services Inc.
 *
 * Last Modified: 02/07/19 14:01
 */

package com.bcf.bcfassistedservicesstorefront.util;

import de.hybris.platform.commercefacades.accommodation.AccommodationReservationData;
import de.hybris.platform.commercefacades.travel.GlobalTravelReservationData;
import de.hybris.platform.commercefacades.travel.LocationData;
import de.hybris.platform.commercefacades.travel.PTCFareBreakdownData;
import de.hybris.platform.commercefacades.travel.PassengerTypeQuantityData;
import de.hybris.platform.commercefacades.travel.TransportFacilityData;
import de.hybris.platform.commercefacades.travel.TravelRouteData;
import de.hybris.platform.commercefacades.travel.reservation.data.ReservationItemData;
import de.hybris.platform.order.CartService;
import de.hybris.platform.servicelayer.session.SessionService;
import de.hybris.platform.travelservices.utils.TravelDateUtils;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Objects;
import org.springframework.beans.factory.annotation.Required;
import com.bcf.bcfaccommodationsaddon.forms.cms.AccommodationFinderForm;
import com.bcf.bcfcommonsaddon.controllers.util.BcfAccommodationControllerUtil;
import com.bcf.bcfcommonsaddon.forms.cms.TravelFinderForm;
import com.bcf.bcfstorefrontaddon.constants.BcfstorefrontaddonWebConstants;
import com.bcf.bcfstorefrontaddon.util.BcfControllerUtil;
import com.bcf.bcfvoyageaddon.forms.cms.FareFinderForm;
import com.bcf.core.bcfproperties.BcfConfigurablePropertiesService;
import com.bcf.core.dao.BcfTravelLocationDao;
import com.bcf.facades.constants.BcfFacadesConstants;
import com.bcf.facades.ferry.VehicleTypeQuantityData;
import com.bcf.facades.travelfacility.BCFTransportFacilityFacade;
import com.bcf.facades.travelroute.BCFTravelRouteFacade;


public class FormPopulatorFromCart
{
	private static final String ECONOMY_CABIN_CLASS_CODE = "M";
	private static final int DEFAULT_ACCOMMODATION_QUANTITY = 1;

	private BCFTransportFacilityFacade transportFacilityFacade;
	private BcfTravelLocationDao bcfTravelLocationDao;
	private SessionService sessionService;
	private BCFTravelRouteFacade travelRouteFacade;
	private BcfConfigurablePropertiesService bcfConfigurablePropertiesService;
	private BcfAccommodationControllerUtil bcfAccommodationControllerUtil;
	private CartService cartService;
	private BcfControllerUtil bcfControllerUtil;

	public TravelFinderForm initializeTravelFinderForm(final GlobalTravelReservationData globalTravelReservationData)
	{
		final TravelFinderForm travelFinderForm = new TravelFinderForm();
		final AccommodationReservationData accommodationReservationData = globalTravelReservationData
				.getAccommodationReservationData();
		final AccommodationFinderForm accommodationFinderForm = new AccommodationFinderForm();

		bcfAccommodationControllerUtil.initializeAccommodationFinderForm(accommodationFinderForm, cartService.getSessionCart(), Integer.valueOf(globalTravelReservationData.getAccommodationReservationData().getJourneyRef()));
		accommodationFinderForm.setRoomStayCandidates(bcfControllerUtil
				.cleanUpChildRoomStayCandidates(accommodationFinderForm.getNumberOfRooms(),
						accommodationFinderForm.getRoomStayCandidates()));

		travelFinderForm.setTravelRoute(
				globalTravelReservationData.getReservationData().getReservationItems().stream().findFirst().get()
						.getReservationItinerary().getRoute().getCode());
		travelFinderForm.setAccommodationFinderForm(accommodationFinderForm);
		travelFinderForm
				.setFareFinderForm(initializeFareFinderForm(travelFinderForm, accommodationFinderForm, globalTravelReservationData));
		return travelFinderForm;
	}

	private AccommodationFinderForm initializeAccommodationFinderForm(
			final AccommodationReservationData accommodationReservationData)
	{
		final AccommodationFinderForm accommodationFinderForm = new AccommodationFinderForm();
		final String locationName = accommodationReservationData.getAccommodationReference().getAddress().getTown();
		final Date checkInDate = accommodationReservationData.getRoomStays().stream().findFirst().get().getCheckInDate();
		final Date checkOutDate = accommodationReservationData.getRoomStays().stream().findFirst().get().getCheckOutDate();
		accommodationFinderForm.setDestinationLocationName(locationName);
		accommodationFinderForm
				.setCheckInDateTime(TravelDateUtils.convertDateToStringDate(checkInDate, BcfFacadesConstants.DATE_PATTERN_MM_DD_YYYY));
		accommodationFinderForm
				.setCheckOutDateTime(
						TravelDateUtils.convertDateToStringDate(checkOutDate, BcfFacadesConstants.DATE_PATTERN_MM_DD_YYYY));
		accommodationFinderForm.setDestinationLocation(getBcfTravelLocationDao().findLocationByName(locationName).getCode());
		accommodationFinderForm.setNumberOfRooms(DEFAULT_ACCOMMODATION_QUANTITY);
		getSessionService().setAttribute(BcfstorefrontaddonWebConstants.ACCOMMODATION_FINDER_FORM, accommodationFinderForm);
		return accommodationFinderForm;
	}

	private FareFinderForm initializeFareFinderForm(final TravelFinderForm travelFinderForm,
			final AccommodationFinderForm accommodationFinderForm, final GlobalTravelReservationData globalTravelReservationData)
	{
		final FareFinderForm fareFinderForm = new FareFinderForm();
		final TravelRouteData travelRouteData = getTravelRouteFacade().getTravelRoute(travelFinderForm.getTravelRoute());
		fareFinderForm.setDepartingDateTime(accommodationFinderForm.getCheckInDateTime());
		fareFinderForm.setReturnDateTime(accommodationFinderForm.getCheckOutDateTime());

		fareFinderForm.setDepartureLocation(travelRouteData.getOrigin().getCode());
		fareFinderForm.setDepartureLocationName(getLocationName(fareFinderForm.getDepartureLocation()));
		fareFinderForm.setArrivalLocation(travelRouteData.getDestination().getCode());
		fareFinderForm.setArrivalLocationName(
				getLocationName(fareFinderForm.getArrivalLocation()));
		final List<ReservationItemData> reservationItemDatas = globalTravelReservationData.getReservationData()
				.getReservationItems();
		fareFinderForm.setVehicleInfo(initializeVehicle(reservationItemDatas));
		fareFinderForm.setTripType(getBcfConfigurablePropertiesService().getBcfPropertyValue("defaultReturnTripType"));
		fareFinderForm.setPassengerTypeQuantityList(initializePassenger(reservationItemDatas));
		fareFinderForm.setCabinClass(ECONOMY_CABIN_CLASS_CODE);
		return fareFinderForm;
	}

	private String getFareFinderDate(final String date)
	{
		final DateTimeFormatter formatter = DateTimeFormatter.ofPattern(BcfstorefrontaddonWebConstants.DATE_FORMAT);
		final LocalDate localDate = LocalDate.parse(date, formatter);

		return localDate
				.format(DateTimeFormatter.ofPattern(BcfFacadesConstants.DATE_PATTERN_MMM_DD_YYYY));
	}

	private String getLocationName(final String locationCode)
	{
		final TransportFacilityData transportFacilityData = getTransportFacilityFacade().getTransportFacility(locationCode);
		final LocationData location = getTransportFacilityFacade().getLocation(locationCode);
		String locationName = location.getName();
		if (Objects.nonNull(transportFacilityData))
		{
			locationName = locationName + " ( " + transportFacilityData.getName() + " )";
		}
		return locationName;
	}

	private List<VehicleTypeQuantityData> initializeVehicle(final List<ReservationItemData> reservationItemDatas)
	{
		final List<VehicleTypeQuantityData> vehicleTypeQuantityDataList = new ArrayList<>();
		for (final ReservationItemData reservationItemData : reservationItemDatas)
		{
			final VehicleTypeQuantityData vehicleTypeQuantityData = reservationItemData.getReservationPricingInfo()
					.getItineraryPricingInfo().getVehicleFareBreakdownDatas().stream().findFirst().get().getVehicleTypeQuantity();
			final VehicleTypeQuantityData vehicleInfo = populateVehicle(vehicleTypeQuantityData);
			vehicleInfo.setTravelRouteCode(reservationItemData.getReservationItinerary().getRoute().getCode());
			vehicleTypeQuantityDataList.add(vehicleInfo);
		}
		return vehicleTypeQuantityDataList;
	}

	private VehicleTypeQuantityData populateVehicle(final VehicleTypeQuantityData vehicleTypeQuantityData)
	{
		final VehicleTypeQuantityData vehicleInfo = new VehicleTypeQuantityData();
		vehicleInfo.setVehicleType(vehicleTypeQuantityData.getVehicleType());
		vehicleInfo.getVehicleType().setCategory(vehicleTypeQuantityData.getVehicleType().getCategory().toLowerCase());
		vehicleInfo.getVehicleType().setCode(vehicleTypeQuantityData.getVehicleType().getCode());
		vehicleInfo.setHeight(vehicleTypeQuantityData.getHeight());
		vehicleInfo.setLength(vehicleTypeQuantityData.getLength());
		vehicleInfo.setWidth(vehicleTypeQuantityData.getWidth());
		vehicleInfo.setQty(vehicleTypeQuantityData.getQty());
		return vehicleInfo;
	}

	private List<PassengerTypeQuantityData> initializePassenger(final List<ReservationItemData> reservationItemDatas)
	{
		final List<PTCFareBreakdownData> ptcFareBreakdownDatas = reservationItemDatas.stream().findFirst().get()
				.getReservationPricingInfo().getItineraryPricingInfo().getPtcFareBreakdownDatas();
		final List<PassengerTypeQuantityData> passengerTypeQuantityList = new ArrayList<>();
		for (final PTCFareBreakdownData ptcFareBreakdownData : ptcFareBreakdownDatas)
		{
			passengerTypeQuantityList.add(ptcFareBreakdownData.getPassengerTypeQuantity());
		}
		return passengerTypeQuantityList;
	}

	protected BCFTransportFacilityFacade getTransportFacilityFacade()
	{
		return transportFacilityFacade;
	}

	@Required
	public void setTransportFacilityFacade(final BCFTransportFacilityFacade transportFacilityFacade)
	{
		this.transportFacilityFacade = transportFacilityFacade;
	}

	protected BcfTravelLocationDao getBcfTravelLocationDao()
	{
		return bcfTravelLocationDao;
	}

	@Required
	public void setBcfTravelLocationDao(final BcfTravelLocationDao bcfTravelLocationDao)
	{
		this.bcfTravelLocationDao = bcfTravelLocationDao;
	}

	protected SessionService getSessionService()
	{
		return sessionService;
	}

	@Required
	public void setSessionService(final SessionService sessionService)
	{
		this.sessionService = sessionService;
	}

	protected BCFTravelRouteFacade getTravelRouteFacade()
	{
		return travelRouteFacade;
	}

	@Required
	public void setTravelRouteFacade(final BCFTravelRouteFacade travelRouteFacade)
	{
		this.travelRouteFacade = travelRouteFacade;
	}

	protected BcfConfigurablePropertiesService getBcfConfigurablePropertiesService()
	{
		return bcfConfigurablePropertiesService;
	}

	@Required
	public void setBcfConfigurablePropertiesService(
			final BcfConfigurablePropertiesService bcfConfigurablePropertiesService)
	{
		this.bcfConfigurablePropertiesService = bcfConfigurablePropertiesService;
	}

	public BcfAccommodationControllerUtil getBcfAccommodationControllerUtil()
	{
		return bcfAccommodationControllerUtil;
	}

	public void setBcfAccommodationControllerUtil(
			final BcfAccommodationControllerUtil bcfAccommodationControllerUtil)
	{
		this.bcfAccommodationControllerUtil = bcfAccommodationControllerUtil;
	}

	public CartService getCartService()
	{
		return cartService;
	}

	public void setCartService(final CartService cartService)
	{
		this.cartService = cartService;
	}

	public BcfControllerUtil getBcfControllerUtil()
	{
		return bcfControllerUtil;
	}

	public void setBcfControllerUtil(final BcfControllerUtil bcfControllerUtil)
	{
		this.bcfControllerUtil = bcfControllerUtil;
	}
}
