<%@ tag body-content="empty" trimDirectiveWhitespaces="true" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ attribute name="actionNameKey" required="true" type="java.lang.String" %>
<%@ attribute name="actionNameKeyEnding" required="true" type="java.lang.String" %>
<%@ attribute name="action" required="true" type="java.lang.String" %>
<%@ attribute name="disabledButton" required="false" type="java.lang.Boolean" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="cms" uri="http://hybris.com/tld/cmstags" %>

<c:set var="usernamePlaceholder">
   <spring:theme code="asm.emulate.username.placeholder"/>
</c:set>
<c:set var="mobilePlaceholder">
   <spring:theme code="asm.emulate.mobile.placeholder"/>
</c:set>
<c:set var="firstNamePlaceholder">
   <spring:theme code="asm.emulate.firstName.placeholder"/>
</c:set>
<c:set var="lastNamePlaceholder">
   <spring:theme code="asm.emulate.lastname.placeholder"/>
</c:set>
<c:set var="companyNamePlaceholder">
   <spring:theme code="asm.emulate.companyName.placeholder"/>
</c:set>
<c:set var="cartPlaceholder">
   <c:choose>
      <c:when test="${not emulateByOrder}">
         <spring:theme code="asm.emulate.cart.placeholder"/>
      </c:when>
      <c:otherwise>
         <spring:theme code="asm.emulate.cart-order.placeholder"/>
      </c:otherwise>
   </c:choose>
</c:set>

<form action="${action}" method="post" id="_asmPersonifyForm" class="asmForm">
   <input name="customerId" type="hidden" value="${customerId}" class="ASM-input"/>
   <input name="lastIndex" type="hidden" value="0" class="ASM-cust-lastIndex"/>
   <div class="row">
      <%-- Order / Cart Number --%>
      <div class="col-md-3 col-sm-4 col-xs-12">
         <div class="ASM_input_holder cartId">
            <div class="input-group">
               <span class="input-group-addon ASM_icon-cart" id="cartId"></span>
               <c:choose>
                  <c:when test="${not emulateByOrder}">
                     <input name="cartId" type="text" value="${fn:escapeXml(cartId)}" placeholder="${cartPlaceholder}" class="ASM-input form-control" aria-describedby="cartId"/>
                  </c:when>
                  <c:otherwise>
                     <input name="cartId" type="text" value="${fn:escapeXml(cartId)}" placeholder="${cartPlaceholder}" class="ASM-input form-control" aria-describedby="cartId"/>
                  </c:otherwise>
               </c:choose>
            </div>
         </div>
      </div>

      <cms:component uid="eBookingInputFieldComponent" evaluateRestriction="true"/>

         <%-- Company name --%>
         <div class="col-md-3 col-sm-4 col-xs-12">
            <div class="ASM_input_holder mobileNumber">
               <div class="input-group">
                  <span class="input-group-addon  ASM_icon-user" id="companyName"></span>
                  <input name="companyName" type="text" value="${companyName}" placeholder="${companyNamePlaceholder}" class="ASM-input form-control" aria-describedby="companyName"/>
               </div>
            </div>
         </div>
      </div>

      <div class="row">
         <%-- Last name --%>
         <div class="col-md-3 col-sm-4 col-xs-12">
            <div class="ASM_input_holder customerId">
               <div class="input-group">
                  <span class="input-group-addon  ASM_icon-user" id="lastName"></span>
                  <input name="lastName" type="text" value="${lastName}" placeholder="${lastNamePlaceholder}" class="ASM-input form-control" aria-describedby="lastName"/>
               </div>
            </div>
         </div>

            <%--  First name --%>
            <div class="col-md-3 col-sm-4 col-xs-12">
               <div class="ASM_input_holder customerId">
                  <div class="input-group">
                     <span class="input-group-addon ASM_icon-user" id="firstName"></span>
                     <input name="firstName" type="text" value="${firstName}" placeholder="${firstNamePlaceholder}" class="ASM-input form-control" aria-describedby="firstName"/>
                  </div>
               </div>
            </div>

         <%--  Mobile / phone number --%>
         <div class="col-md-3 col-sm-4 col-xs-12">
            <div class="ASM_input_holder mobileNumber">
               <input name="mobile" type="hidden" value="${fn:escapeXml(mobile)}" placeholder="${mobilePlaceholder}" class="ASM-input" />
               <div class="input-group">
                  <span class="input-group-addon ASM_icon-contacts" id="mobileNumber"></span>
                  <input name="mobileNumber" type="text" value="${mobileNumber}" placeholder="${mobilePlaceholder}" class="ASM-input form-control" aria-describedby="mobileNumber"/>
               </div>
            </div>
         </div>

            <%-- Email Address --%>
            <div class="col-md-3 col-sm-4 col-xs-12">
               <div class="ASM_input_holder customerId">
                  <div class="input-group">
                     <span class="input-group-addon ASM_icon-user" id="customerEmailAddress"></span>
                     <input name="emailAddress" type="text" value="${emailAddress}" placeholder="${usernamePlaceholder}"
                            class="ASM-input form-control" aria-describedby="customerEmailAddress"/>
                  </div>
               </div>
            </div>
      </div>

      <div class="row">
      <div class=" col-md-3 col-sm-4 col-xs-12">
         <button type="button" class="ASM-btn ASM-btn-search" id="search">
            <span class="hidden-sm">
               <spring:theme code="asm.emulate.search.text"/>
            </span>
         </button>
      </div>
      </div>

   </div>
</form>
