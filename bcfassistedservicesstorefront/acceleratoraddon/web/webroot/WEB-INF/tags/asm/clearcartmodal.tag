<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<div class="cart-removal-modal">
	<div class="modal fade" id="y_clearCartModal" tabindex="-1" role="dialog" aria-labelledby="y_cartRemoveModal">
		<input id="y_journeyType" type="hidden" value="" />
		<input id="y_buttonToClick" type="hidden" value="" />
		<div class="modal-dialog" role="document">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal" aria-label="Close">
						<span aria-hidden="true"><i class="bcf bcf-icon-cancel-solid"></i></span>
					</button>
					<h3 class="modal-title" id="cartRemoveLabel">
						<spring:theme code="text.cart.removal.modal.title" text="Cart Removal" />
					</h3>
				</div>
				<div class="modal-body">
					<p class="col-sm-12 warning-msg mt-0 bcf-icon-text" id="cartRemove">
						<i class="bcf bcf-icon-alert bcf-2x bcf-vertical-middle" aria-hidden="true"></i>
						<spring:theme code="text.clear.cart.message" text="Are you sure you want to clear your cart?" />
					</p>
				</div>
				<div class="modal-footer">
					<div class="row">
						<div class="col-xs-12 col-sm-6">
							<button id="y_clearCartYes" class="btn btn-primary btn-block">
								<spring:theme code="text.clear.cart.yes.btn.label" text="Yes" />
							</button>
						</div>
						<div class="col-xs-12 col-sm-6">
							<button class="btn btn-secondary btn-block" data-dismiss="modal">
								<spring:theme code="text.clear.cart.no.btn.label" text="No" />
							</button>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
