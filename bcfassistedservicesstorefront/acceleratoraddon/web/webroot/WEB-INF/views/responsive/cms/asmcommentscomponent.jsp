<%@ taglib prefix="json" uri="http://www.atg.com/taglibs/json"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>

		<div class="modal fade" id="y_asmInternalCommentsModal" tabindex="-1"
			role="dialog" aria-labelledby="asmInternalCommentsLabel">
			<div class="modal-dialog" role="document">
				<div class="modal-content">
					<div class="modal-header">
						<button type="button" class="close" data-dismiss="modal"
							aria-label="Close">
							<span aria-hidden="true"><i class="bcf bcf-icon-cancel-solid"></i></span>
						</button>
						<h3 class="modal-title" id="replanJourneyLabel">
							<spring:theme
								code="label.asm.internal.comments"
								text="Internal Comments" />
						</h3>
					</div>

					<div class="modal-body">
					    <c:url var="addCommentsUrl" value="/view/AsmCommentsComponentController/add-internal-comment" />

                        <div class="y_asmCommentContent">
                            <c:if test="${not empty comments}">
                                <table border="1" cellspacing="5">
                                    <thead>
                                        <tr>
                                            <th>Date</th>
                                            <th>Agent</th>
                                            <th>Comment</th>
                                         </tr>
                                     </thead>

                                    <c:forEach items="${comments}" var="comment" varStatus="j">
                                        <tr border="1">
                                          <td border="1">${comment.date}</td>
                                          <td border="1">${comment.agent}</td>
                                          <td border="1">${comment.comment}</td>
                                        </tr>
                                    </c:forEach>
                                </table>
                            </c:if>
                        </div>

						<form:form action="${fn:escapeXml(addCommentsUrl)}" method="POST" id="y_AddComments">

					        <div style="margin-top:20px" class="tab-content">
							    <spring:theme code="label.asm.add.internal.comments" text="Add Internal Comments" />
                                <div class="input-required-wrap">
                                     <input type="hidden" name="orderCode" id="orderCode" value="${fn:escapeXml(orderCode)}">
                                     <input type="text" name="comment" id="comment">
                                 </div>
							</div>

							<div class="form-group col-xs-12 col-sm-4 col-xs-offset-0 col-sm-offset-8">
                                <a class="btn btn-primary btn-block y_addInternalComments">
                                    <spring:theme code="label.asm.add.internal.comments" />
                                </a>
                            </div>

						</form:form>
					</div>

				</div>
			</div>
		</div>

        <div class="col-lg-4 col-md-4 col-md-offset-4 text-center">
            <button class="btn btn-primary btn-block tab-content" type="button" id="internalCommentsButton" value="button">
            <spring:theme code="text.asm.internal.comments" text="Add Internal Comments" />
            </button>&nbsp;
        </div>
