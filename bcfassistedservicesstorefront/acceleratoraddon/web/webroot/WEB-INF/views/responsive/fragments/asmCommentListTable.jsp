<%@ page trimDirectiveWhitespaces="true" contentType="application/json"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="json" uri="http://www.atg.com/taglibs/json"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="dealdetails" tagdir="/WEB-INF/tags/addons/bcfcommonsaddon/responsive/dealdetails"%>
<json:object escapeXml="false">
    <c:choose>
        <c:when test="${not empty comments}">
            <json:property name="content">
                 <div class="y_asmCommentContent">
                    <table border="1" cellspacing="5">
                        <thead>
                            <tr>
                                <th>Date</th>
                                <th>Agent</th>
                                <th>Comment</th>
                             </tr>
                         </thead>

                        <c:forEach items="${comments}" var="comment" varStatus="j">
                            <tr border="1">
                              <td border="1">${comment.date}</td>
                              <td border="1">${comment.agent}</td>
                              <td border="1">${comment.comment}</td>
                            </tr>
                        </c:forEach>
                    </table>
                </div>
            </json:property>
        </c:when>
        <c:otherwise>
            <json:property name="content">
                 <div class="y_asmCommentContent">
                 </div>
             </json:property>
        </c:otherwise>
    </c:choose>
</json:object>
