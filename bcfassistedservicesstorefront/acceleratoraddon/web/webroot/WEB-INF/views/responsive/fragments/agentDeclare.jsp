<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>

<c:url var="agentDeclareUrl" value="/endofday-report?agentToDeclare="/>

<div class="ASM_timer element-separator-height">
    <a href="${agentDeclareUrl}"> <button type="button"> <spring:theme code="asm.login.agentDeclare.btn.label"/> </button> </a>
</div>
