<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>

<div class="ASM_timer element-separator-height">
    <c:out value="${agent.name}"/> <br/>
    <spring:theme code="asm.login.iceBarID.valueHolder" arguments="${jalosession.getAttribute('iceBarID')}"/>
</div>
