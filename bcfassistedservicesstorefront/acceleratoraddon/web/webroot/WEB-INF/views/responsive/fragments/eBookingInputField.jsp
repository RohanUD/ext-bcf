<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>

<c:set var="eBookingRefPlaceholder">
    <spring:theme code="asm.login.eBookingRef.placeHolder"/>
</c:set>

<%-- eBooking Ref Number --%>
<div class="col-md-3 col-sm-4 col-xs-12">
    <div class="ASM_input_holder customerId">
        <div class="input-group">
            <span class="input-group-addon ASM_icon-user" id="customerName"></span>
            <input name="eBookingRefNumber" type="text" value="${eBookingRefNumber}" placeholder="${eBookingRefPlaceholder}"
                   class="ASM-input form-control" aria-describedby="customerName"/>
        </div>
    </div>
</div>
