<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="cms" uri="http://hybris.com/tld/cmstags" %>

<spring:url var="changeBookingRequestsUrl" value="/manage-booking/change-booking-requests" />
<spring:url var="pendingOrdersUrl" value="/pendingOrderDetailsPage" />
<spring:url var="endOfDayReport" value="/endofday-report" />
<spring:url var="inventoryReports" value="/inventory-reports" />
<spring:url var="clearCartUrl" value="/clearCart" />

<c:if test="${not empty agent.uid}">
    <div class="container">
        <div class="row">
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <ul class="nav nav-tabs nav-tabs--responsive asm__customer360__menu" role="tablist" id="customer-360-tabs">
                    <li><cms:component uid="inventoryReport" evaluateRestriction="true"/></li>

                    <li><cms:component uid="changeBookingRequest" evaluateRestriction="true"/></li>

                    <li><cms:component uid="onRequestBookings" evaluateRestriction="true"/></li>

                    <li><cms:component uid="bookingInquiries" evaluateRestriction="true"/></li>

                    <li><cms:component uid="optionBookings" evaluateRestriction="true"/></li>

                    <li><cms:component uid="cartSummary" evaluateRestriction="true"/></li>

                    <li><cms:component uid="clearCart" evaluateRestriction="true"/></li>
                </ul>
            </div>
        </div>
    </div>
</c:if>
