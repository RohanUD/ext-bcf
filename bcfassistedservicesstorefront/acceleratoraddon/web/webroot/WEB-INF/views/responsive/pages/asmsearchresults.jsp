<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>

<c:choose>
    <c:when test="${not empty customerData and not empty customerData.customers}">
        <c:set var="customers" value="${customerData.customers}"/>
        <spring:theme code="text.asm.searchResults.label" text="Search results" arguments="${fn:length(customers)}"/>
        <table>
            <tr>
                <th><spring:theme code="text.asm.lastNameFirstName.label"/> </th>
                <th><spring:theme code="text.asm.phoneNumber.label"/> </th>
                <th><spring:theme code="text.asm.emailAddress.label"/> </th>
                <th><spring:theme code="text.asm.address.label"/> </th>
                <th><spring:theme code="text.asm.companyName.label"/> </th>
            </tr>
            <c:forEach var="customer" items="${customers}">
                <c:url var="emulateCustomerUrl" value="/assisted-service/emulate">
                    <c:param name="customerId" value="${customer.email}"/>
                    <c:param name="guestInd" value="${customer.guestInd}"/>
                    <c:param name="fwd" value="/my-account"/>
                </c:url>
                <tr>
                    <td>
                        <a href="${emulateCustomerUrl}"> ${customer.lastName}&nbsp;${customer.firstName} </a>
                    </td>
                    <td>
                            ${customer.mobileNumber}
                    </td>
                    <td>
                            ${customer.email}
                    </td>
                    <td>
                        <c:if test="${not empty customer.defaultAddress}">
                            <p>${customer.defaultAddress.line1}</p>
                            <p>${customer.defaultAddress.line2}</p>
                            <p>${customer.defaultAddress.town}</p>
                            <p>${customer.defaultAddress.postalCode}</p>
                            <c:if test="${not empty customer.defaultAddress.region}">
                                <p>${customer.defaultAddress.region.name}</p>
                            </c:if>
                            <c:if test="${not empty customer.defaultAddress.country}">
                                <p>${customer.defaultAddress.country.name}</p>
                            </c:if>
                        </c:if>
                    </td>
                    <td>
                            ${customer.companyName}
                    </td>
                </tr>
            </c:forEach>

            <c:if test="${customerData.lastPageInd eq false}">
                <tr>
                    <td colspan="5">
                        <c:url var="loadMoreUrl" value="/assisted-service/searchUser">
                            <c:param name="customerId" value="${customerId}"/>
                            <c:param name="firstName" value="${firstName}"/>
                            <c:param name="lastName" value="${lastName}"/>
                            <c:param name="emailAddress" value="${emailAddress}"/>
                            <c:param name="mobileNumber" value="${mobileNumber}"/>
                            <c:param name="companyName" value="${companyName}"/>
                            <c:param name="callback" value="${callback}"/>
                            <c:param name="lastIndex" value="${customerData.currentIndex}"/>
                        </c:url>
                        <a href="${loadMoreUrl}"> <spring:theme code="text.asm.loadMore.label" text="Load more"/> </a>
                    </td>
                </tr>
            </c:if>
        </table>
    </c:when>
    <c:otherwise>
        <spring:theme code="text.asm.searchResults.label" text="Search results" arguments="0"/> <br/>
        <spring:theme code="text.asm.noResults.label"/> <br/>
        <spring:theme code="text.asm.pageNumbers.label" arguments="0,0"/> <br/>
    </c:otherwise>
</c:choose>

