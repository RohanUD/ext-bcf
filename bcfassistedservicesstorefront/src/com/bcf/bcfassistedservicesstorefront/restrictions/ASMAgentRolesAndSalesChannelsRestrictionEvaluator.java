/*
 * Copyright (c) 2019 British Columbia Ferry Services Inc
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of British Columbia Ferry Services Inc("Confidential Information").
 * You shall not disclose such Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with British Columbia Ferry Services Inc.
 *
 * Last Modified: 02/07/19 12:36
 */

package com.bcf.bcfassistedservicesstorefront.restrictions;

import de.hybris.platform.assistedservicefacades.AssistedServiceFacade;
import de.hybris.platform.assistedserviceservices.AssistedServiceService;
import de.hybris.platform.cms2.model.restrictions.ASMAgentRolesAndSalesChannelsRestrictionModel;
import de.hybris.platform.cms2.model.restrictions.SalesChannelContainerModel;
import de.hybris.platform.cms2.servicelayer.data.RestrictionData;
import de.hybris.platform.cms2.servicelayer.services.evaluator.CMSRestrictionEvaluator;
import de.hybris.platform.core.model.user.UserGroupModel;
import de.hybris.platform.servicelayer.session.SessionService;
import de.hybris.platform.servicelayer.user.UserService;
import java.util.Collection;
import java.util.Collections;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;
import javax.annotation.Resource;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Required;
import com.bcf.bcfassistedservicesstorefront.enums.ASMSalesChannels;
import com.bcf.core.constants.BcfCoreConstants;


public class ASMAgentRolesAndSalesChannelsRestrictionEvaluator
		implements CMSRestrictionEvaluator<ASMAgentRolesAndSalesChannelsRestrictionModel>
{
	private SessionService sessionService;
	private UserService userService;
	private AssistedServiceService assistedServiceService;

	@Resource(name = "assistedServiceFacade")
	private AssistedServiceFacade assistedServiceFacade;

	@Override
	public boolean evaluate(final ASMAgentRolesAndSalesChannelsRestrictionModel asmAgentRolesAndSalesChannelsRestrictionModel,
			final RestrictionData restrictionData)
	{
		if (!assistedServiceFacade.isAssistedServiceAgentLoggedIn())
		{
			return false;
		}
		final List<ASMSalesChannels> salesChannels = Optional
				.ofNullable(asmAgentRolesAndSalesChannelsRestrictionModel.getSalesChannelContainer())
				.map(SalesChannelContainerModel::getSalesChannels).orElse(Collections.emptyList());
		return checkUserRoles(asmAgentRolesAndSalesChannelsRestrictionModel.getUserGroups(), salesChannels);
	}

	private boolean checkUserRoles(final Collection<UserGroupModel> allowedGroups,
			final Collection<ASMSalesChannels> allowedSalesChannels)
	{
		final String salesChannelInSession = getSessionService().getAttribute(BcfCoreConstants.SALES_CHANNEL);
		if (CollectionUtils.isEmpty(allowedGroups) || CollectionUtils.isEmpty(allowedSalesChannels)
				|| StringUtils.isEmpty(salesChannelInSession))
		{
			return false;
		}

		final List<String> allowedRoles = allowedGroups.stream().map(userGroupModel -> userGroupModel.getUid())
				.collect(Collectors.toList());

		final List<String> asmAgentRoles = assistedServiceService.getAsmSession().getAgent().getAllGroups().stream()
				.map(principalGroupModel -> principalGroupModel.getUid()).collect(Collectors.toList());


		return CollectionUtils.containsAny(asmAgentRoles, allowedRoles) && allowedSalesChannels
				.contains(ASMSalesChannels.valueOf(salesChannelInSession));

	}

	protected SessionService getSessionService()
	{
		return sessionService;
	}

	@Required
	public void setSessionService(final SessionService sessionService)
	{
		this.sessionService = sessionService;
	}

	protected UserService getUserService()
	{
		return userService;
	}

	@Required
	public void setUserService(final UserService userService)
	{
		this.userService = userService;
	}

	protected AssistedServiceService getAssistedServiceService()
	{
		return assistedServiceService;
	}

	@Required
	public void setAssistedServiceService(final AssistedServiceService assistedServiceService)
	{
		this.assistedServiceService = assistedServiceService;
	}
}
