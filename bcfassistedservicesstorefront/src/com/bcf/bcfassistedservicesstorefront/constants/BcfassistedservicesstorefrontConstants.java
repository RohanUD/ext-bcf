/*
 * [y] hybris Platform
 *
 * Copyright (c) 2017 SAP SE or an SAP affiliate company.
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of SAP
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with SAP.
 */
package com.bcf.bcfassistedservicesstorefront.constants;

/**
 * Global class for all Bcfassistedservicesstorefront constants. You can add global constants for your extension into this
 * class.
 */
public final class BcfassistedservicesstorefrontConstants extends GeneratedBcfassistedservicesstorefrontConstants
{
	public static final String EXTENSIONNAME = "bcfassistedservicesstorefront";
	public static final String REDIRECT_WITH_CART = "bcfassistedservicesstorefront.redirect.customer_and_cart";
	public static final String REDIRECT_WITH_ORDER = "bcfassistedservicesstorefront.redirect.order";
	public static final String REDIRECT_CUSTOMER_ONLY = "bcfassistedservicesstorefront.redirect.customer_only";
	public static final String REDIRECT_ERROR = "bcfassistedservicesstorefront.redirect.error";
	public static final String AIF_TIMEOUT = "bcfassistedservicesstorefront.aif.timeout";
	public static final int AIF_DEFAULT_TIMEOUT = 7000; //default timeout in milliseconds
	public static final String AIF_OVERVIEW_CART_ITMES_TO_BE_DISPLAYED = "aif.overview.cart.items.to.display";
	public static final int AIF_OVERVIEW_CART_ITMES_TO_BE_DISPLAYED_DEFAULT = 6;

	// Default parent group id for all AS agents
	public static final String AS_AGENT_GROUP_UID = "asagentgroup";


	private BcfassistedservicesstorefrontConstants()
	{
		//empty to avoid instantiating this constant class
	}
}
