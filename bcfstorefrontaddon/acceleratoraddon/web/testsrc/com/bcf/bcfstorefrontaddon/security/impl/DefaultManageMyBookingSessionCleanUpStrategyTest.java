/*
 * Copyright (c) 2019 British Columbia Ferry Services Inc
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of British Columbia Ferry Services Inc("Confidential Information").
 * You shall not disclose such Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with British Columbia Ferry Services Inc.
 *
 * Last Modified: 30/11/18 18:06
 */

package com.bcf.bcfstorefrontaddon.security.impl;

import static org.mockito.BDDMockito.given;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.servicelayer.session.SessionService;
import java.util.regex.Pattern;
import javax.servlet.http.HttpServletRequest;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.runners.MockitoJUnitRunner;
import com.bcf.bcfstorefrontaddon.constants.BcfstorefrontaddonWebConstants;


/**
 * Unit Test class for DefaultManageMyBookingSessionCleanUpStrategy.
 */
@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class DefaultManageMyBookingSessionCleanUpStrategyTest
{
	@InjectMocks
	private DefaultManageMyBookingSessionCleanUpStrategy classToTest;

	@Mock
	private SessionService sessionService;

	@Mock
	private HttpServletRequest request;

	@BeforeClass
	public void setUp()
	{
		classToTest = new DefaultManageMyBookingSessionCleanUpStrategy();
		classToTest.setManageMyBookingUrlPattern(Pattern.compile("(^https://.*/manage-booking/.*)"));
	}

	@Test
	public void testManageMyBookingCleanUp_mmbFlow()
	{
		given(sessionService.getAttribute(BcfstorefrontaddonWebConstants.MANAGE_MY_BOOKING_AUTHENTICATION))
				.willReturn(Boolean.TRUE);
		given(request.getRequestURL().toString()).willReturn("https://yacceleratorstorefront/manage-booking/booking-details");

		classToTest.manageMyBookingCleanUp(request);

		Mockito.verify(sessionService);
		Mockito.verify(request);
	}

}
