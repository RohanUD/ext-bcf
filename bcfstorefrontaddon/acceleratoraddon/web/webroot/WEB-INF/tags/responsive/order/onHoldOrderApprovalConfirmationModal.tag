<%@ tag body-content="empty" trimDirectiveWhitespaces="true"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<spring:htmlEscape defaultHtmlEscape="false" />

<div class="modal fade" id="y_onHoldOrderConfirmModal" tabindex="-1" role="dialog" aria-labelledby="onHoldOrderConfirmModal"data-elementid="" data-elementid="">
	<div class="modal-dialog" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
				<h4 class="modal-title" id="onHoldOrderConfirmModalTitle">
                    <spring:theme code="text.onHoldOrder.modal.header" />
				</h4>
			</div>
			<div class="modal-body">
				<div class="y_onHoldOrderConfirmModalBody">
				    <spring:theme code="text.onHoldOrder.modal.approve.order.confirm.message" />
				</div>
			</div>
			<div class="modal-footer y_onHoldOrderConfirmModalFooter">
                <div class="row">
                    <div class="col-xs-12 col-sm-6">
                        <button class="btn btn-primary btn-block y_onHoldOrderApprovalConfirmBtn">
                            <spring:theme code="text.onHoldOrder.button.confirm" text="Confirm" />
                        </button>
                    </div>
                    <div class="col-xs-12 col-sm-6">
                        <button class="btn btn-transparent btn-block onHoldOrderApprovalRejectBtn" data-dismiss="modal">
                            <spring:theme code="text.onHoldOrder.button.close" text="Close" />
                        </button>
                    </div>
                </div>
            </div>
		</div>
	</div>
</div>
