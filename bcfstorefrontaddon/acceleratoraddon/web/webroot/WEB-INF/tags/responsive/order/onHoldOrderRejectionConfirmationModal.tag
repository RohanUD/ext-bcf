<%@ tag body-content="empty" trimDirectiveWhitespaces="true"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<spring:htmlEscape defaultHtmlEscape="false" />

<div class="modal fade" id="y_onHoldOrderRejectModal" tabindex="-1" role="dialog" aria-labelledby="onHoldOrderRejectModal" data-elementid="">
	<div class="modal-dialog" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
				<h4 class="modal-title" id="onHoldOrderRejectModalTitle">
					<spring:theme code="text.onHoldOrder.modal.header" />
				</h4>
			</div>
			<div class="modal-body">
				<div class="y_onHoldOrderRejectModalBody">
				    <spring:theme code="text.onHoldOrder.modal.reject.order.confirm.message" />
				</div>
			</div>
			<div class="modal-footer y_onHoldOrderRejectionModalFooter">
                <div class="row">
                    <div class="col-xs-12 col-sm-6">
                        <button class="btn btn-primary btn-block y_onHoldOrderRejectionConfirmBtn">
                            <spring:theme code="text.onHoldOrder.button.confirm" text="Confirm" />
                        </button>
                    </div>
                    <div class="col-xs-12 col-sm-6">
                        <button class="btn btn-transparent btn-block onHoldOrderRejectionDeniedBtn" data-dismiss="modal">
                            <spring:theme code="text.onHoldOrder.button.close" text="Close" />
                        </button>
                    </div>
                </div>
            </div>
		</div>
	</div>
</div>
