<%@ tag body-content="empty" trimDirectiveWhitespaces="true"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="cms" uri="http://hybris.com/tld/cmstags"%>
<%@ attribute name="visible" required="false" type="java.lang.Boolean"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>

  <input type="hidden" name="cartTimerContinueUrl" id="cartTimerContinueUrl">

  <form action="" method="GET" id="cartTimerContinueUrlForm">
  </form>

   <div class="modal ${visible ? 'show' : ''}" id="y_showCartAboutToExpireModal" tabindex="-1" role="dialog" aria-labelledby="y_showCartAboutToExpireModal">
	<div class="modal-dialog" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true"><i class="bcf bcf-icon-cancel-solid"></i></span>
				</button>
				<h4 class="modal-title" id="cartAboutToExpireModal">
					<spring:theme code="text.cart.timer" text="Timeout notice" />
				</h4>
			</div>
			<div class="modal-body">
                <span id="cartTimerAboutToExpireMessage"></span><br/>

                    <div class="row">
                        <div class="col-lg-6 col-md-6 col-xs-12">
                            <button type="button" class="btn btn-transparent btn-block btn-cart-timer-continue" data-dismiss="modal">
                                 <spring:theme code="text.cart.timer.continue" text="Continue" />
                            </button>
                        </div>
                    </div>
			</div>
		</div>
	</div>
</div>


<div class="modal ${visible ? 'show' : ''}" id="y_showCartTimerExpired" tabindex="-1" role="dialog" aria-labelledby="y_showCartTimerExpired">
	<div class="modal-dialog" role="document">
		<div class="modal-content">
			<div class="modal-header">

				<h4 class="modal-title" id="cartTimerExpired">

						<spring:theme code="text.cart.timer" text="Timeout notice" />
				</h4>
			</div>

			<div class="modal-body">
                  <spring:theme code="text.cart.timer.expired.items"/> <br/>
                    <div class="row">
                        <div class="col-lg-6 col-md-6 col-xs-12">

                          <button class="btn btn-primary btn-block cartTimerContinueButton" type="button" id="cartTimerContinueButton">

                            <spring:theme code="text.cart.timer.search.again" text="Search again" />
                               </button>
                          </div>
                    </div>
			</div>
		</div>
	</div>
</div>




