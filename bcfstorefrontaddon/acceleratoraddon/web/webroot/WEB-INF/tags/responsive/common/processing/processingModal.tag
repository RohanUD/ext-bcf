<%@ tag body-content="empty" trimDirectiveWhitespaces="true" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ attribute name="visible" required="true" type="java.lang.Boolean"%>

<%-- Processing Modal --%>
<div class="processing-modal">
    <div class="modal ${visible ? 'show' : ''}" id="y_processingModal" tabindex="-1" aria-labelledby="y_processingModal">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button align="right" type="button" class="close" data-dismiss="modal"><span aria-hidden="true"><i class="bcf bcf-icon-cancel-solid"></i></span></button>
                    <h4 class="modal-title">Modal title</h4>
                </div>
                <div class="modal-body">
                </div>
            </div>
        </div>
    </div>
</div>
