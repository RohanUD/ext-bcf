<%@ tag body-content="empty" trimDirectiveWhitespaces="true" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="format" tagdir="/WEB-INF/tags/shared/format" %>
<%@ taglib prefix="ycommerce" uri="http://hybris.com/tld/ycommercetags" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ attribute name="currentConditionsData" required="true"
              type="de.hybris.platform.commercefacades.travel.CurrentConditionsData" %>

<jsp:useBean id="dateValue" class="java.util.Date"/>
<jsp:useBean id="now" class="java.util.Date"/>
<spring:htmlEscape defaultHtmlEscape="false"/>

<c:set var="currentConditionsResponseData" value="${currentConditionsData.currentConditionsResponseData[0]}"/>
<c:set var="vesselNames" value="${currentConditionsData.vessels}"/>

<c:url var="getTerminalNameUrl" value="/currentconditions-details/getTerminalName?terminalCode="/>
<div>
    <div class="current-condition-sailing-detail col-lg-12 col-md-12 col-sm-12 col-xs-12 mobile-margin-0 margin-top-40 margin-bottom-40">
        <div class="row cc-detail-box detail-box-spacing bg-grey">
            <c:if test="${isSailingsFull eq 'true'}">
                <div class="text-center">
                    <strong><span class="bcf bcf-icon-notice-outline bcf-2x"></span><br/>
                        <spring:theme code="text.cc.today.sailings.full.header"/></strong>
                    <p class="fnt-14"><spring:theme code="text.cc.today.sailings.full.label"/></p>
                </div>
                <hr class="margin-top-40 margin-bottom-40"/>
            </c:if>
            <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12 text-center">
                <h4 class="mb-0"><spring:theme code="text.cc.sailingdetails.label" text="Sailing Details"/></h4>
                <p><i class="last-update-text"><spring:theme code="text.cc.lastUpdated.label" text="Last Updated"/>:
                    <c:if test="${not empty currentConditionsResponseData.refreshedAt}">
                        <fmt:parseDate var="now" value="${currentConditionsResponseData.refreshedAt}"
                                       pattern="yyyy-MM-dd HH:mm:ss"/>
                        <fmt:parseDate type="both" var="dateValue" dateStyle="short" timeStyle="short"
                                       value="${currentConditionsResponseData.refreshedAt}" pattern="yyyy-MM-dd HH:mm:ss"/>
                        <fmt:formatDate value="${dateValue}" pattern="h:mm a"/>. <a href="#"
                                                                                    onclick="window.location.reload(true);">
                        <spring:theme code="text.cc.refreshDetails.label" text="Refresh details"/> </a>
                    </c:if></i></p>

                <c:choose>
                    <c:when test="${noMoreSailings eq 'false'}">
                        <c:set var="sailing"><spring:theme code="text.cc.sailing.label" text="Sailing"/> </c:set>
                        <input type="hidden" id="getTerminalNameUrl" value="${getTerminalNameUrl}">
                        <select class="select-custom form-control margin-top-30  selectpicker currentconditions-details-select"
                                id="SailingDetails" data-show-subtext="true">
                            <c:forEach items="${currentConditionsResponseData.arrivalDepartures}" var="entry">
                                <c:forEach var="item" items="${entry.value}" varStatus="counter">
                                    <fmt:parseDate value="${item.scheduledDeparture}" var="dateObject"
                                                   pattern="yyyy-MM-dd HH:mm:ss"/>
                                    <c:if test="${dateObject gt now}">
                                        <c:set var="sailingVal" value="${item.dest}-${counter.index}"/>
                                        <option data-subtext="<fmt:formatDate value="${dateObject}" pattern="h:mm a"/>"
                                                value="${sailingVal}"
                                                <c:if test='${selectedSailing eq sailingVal}'>selected</c:if> >
                                                ${sailing}&nbsp;&nbsp;&nbsp;<c:if
                                                test="${item.webDisplaySailingFlag eq 'Y' and item.status eq 'S'}"><i class='text-red txt-14'><spring:theme
                                                code="text.cc.cancelled.status.label"/></i></c:if>
                                        </option>
                                    </c:if>
                                </c:forEach>
                            </c:forEach>
                        </select>


                        <div class="view-deck-wrapper fnt-14">
                            <c:forEach items="${currentConditionsResponseData.arrivalDepartures}" var="entry">
                                <c:forEach var="item" items="${entry.value}" varStatus="counter">
                                    <fmt:parseDate value="${item.scheduledDeparture}" var="dateObject"
                                                   pattern="yyyy-MM-dd HH:mm:ss"/>
                                    <c:if test="${dateObject gt now}">
                                        <div class="view-deck-full-box box-${item.dest}-${counter.index}">
                                            <div id="${item.dest}-${counter.index}" class="margin-bottom-20">
                                                <c:set var="ferryName" value="${vesselNames[item.vessel].name}"/>
                                                <c:set var="ferryTranslatedName"
                                                       value="${vesselNames[item.vessel].translatedName}"/>
                                                <c:if test="${empty ferryName}">
                                                    <c:set var="ferryName" value="${item.vessel}"/>
                                                    <c:set var="ferryTranslatedName" value="${item.vessel}"/>
                                                </c:if>

                                                <p class="margin-top-30 mb-0"><spring:theme code="text.cc.ferry.label"
                                                                                            text="Ferry"/>:
                                                    <i>
                                                        <span class="text-capitalize">
                                                            <a href="/ship-info/${vesselNames[item.vessel].shipInfoCode}"
                                                                data-toggle="modal" data-target="#detailsModal">
                                                                    ${ferryName}
                                                            </a>
                                                        </span>
                                                    </i>
                                                </p>
                                            </div>
                                            <c:choose>
                                                <c:when test="${item.webDisplaySailingFlag eq 'Y' and item.status eq 'S'}">
                                                    <div class="sailing-cancelled-msg"><span class="bcf bcf-icon-alert"></span>
                                                    <spring:theme code="text.cc.cancelled.sailing.label"/>
                                                    </div>
                                                </c:when>
                                                <c:otherwise>
                                                    <p class="mb-0">
                                                        <spring:theme code="text.cc.deckspaceAvailable.label"
                                                                      text="Total deck space available"/>
                                                        <a href="#totalDeckLink" data-toggle="modal"
                                                           data-target="#totalDeckAvailableModal">
                                                            <span class="bcf bcf-icon-info-solid"></span>
                                                        </a>
                                                    </p>
                                                    <div class="progress">
                                                        <c:choose>
                                                            <c:when test="${item.vesselFullPercent eq 100}">
                                                                <div class="progress-bar full-bar" role="progressbar"
                                                                     aria-valuenow="${100-item.vesselFullPercent}"
                                                                     aria-valuemin="0" aria-valuemax="100"
                                                                     style="width:${100-item.vesselFullPercent}%">
                                                                    <span>  <spring:theme code="text.full.label"
                                                                                          text="FULL"/> </span>
                                                                </div>
                                                            </c:when>
                                                            <c:otherwise>
                                                                <div class="progress-bar" role="progressbar"
                                                                     aria-valuenow="${100-item.vesselFullPercent}"
                                                                     aria-valuemin="0" aria-valuemax="100"
                                                                     style="width:${100-item.vesselFullPercent}%">
                                                                    <span>  ${100-item.vesselFullPercent}% </span>
                                                                </div>
                                                            </c:otherwise>
                                                        </c:choose>
                                                    </div>
                                                    <div class="progress-meter">
                                                        <div class="meter"><span class="meter-text">0</span></div>
                                                        <div class="meter"><span class="meter-text">10</span></div>
                                                        <div class="meter"><span class="meter-text">20</span></div>
                                                        <div class="meter"><span class="meter-text">30</span></div>
                                                        <div class="meter"><span class="meter-text">40</span></div>
                                                        <div class="meter"><span class="meter-text">50</span></div>
                                                        <div class="meter"><span class="meter-text">60</span></div>
                                                        <div class="meter"><span class="meter-text">70</span></div>
                                                        <div class="meter"><span class="meter-text">80</span></div>
                                                        <div class="meter"><span class="meter-text">90</span></div>
                                                        <div class="meter"><span class="meter-text">100</span></div>
                                                    </div>
                                                    <div class="clearfix"></div>
                                                    <div id="deckSpace">


                                                        <p class="view-deck margin-top-10"><spring:theme
                                                                code="text.cc.viewDeckspaceDetails.label"
                                                                text="View deck space details"/> <i
                                                                class="bcf bcf-icon-down-arrow desk-space-arrow"></i></p>
                                                        <div class="view-deck-div">
                                                            <p class="margin-0 padding-top-20"><spring:theme
                                                                    code="text.stdAvailable.label"/></p>
                                                            <p><spring:theme code="text.stdAvailables.list.label"/></p>
                                                            <div class="progress">
                                                                <c:choose>
                                                                    <c:when test="${item.uhFullPercent eq 100}">
                                                                        <div class="progress-bar full-bar" role="progressbar"
                                                                             aria-valuenow="${100-item.uhFullPercent}"
                                                                             aria-valuemin="0" aria-valuemax="100"
                                                                             style="width:${100-item.uhFullPercent}%">
                                                                            <span>  <spring:theme code="text.full.label"
                                                                                                  text="FULL"/> </span>
                                                                        </div>
                                                                    </c:when>
                                                                    <c:otherwise>
                                                                        <div class="progress-bar" role="progressbar"
                                                                             aria-valuenow="${100-item.uhFullPercent}"
                                                                             aria-valuemin="0" aria-valuemax="100"
                                                                             style="width:${100-item.uhFullPercent}%">
                                                                            <span>  ${100-item.uhFullPercent}% </span>
                                                                        </div>
                                                                    </c:otherwise>
                                                                </c:choose>
                                                            </div>
                                                            <div class="progress-meter">
                                                                <div class="meter"><span class="meter-text">0</span></div>
                                                                <div class="meter"><span class="meter-text">10</span></div>
                                                                <div class="meter"><span class="meter-text">20</span></div>
                                                                <div class="meter"><span class="meter-text">30</span></div>
                                                                <div class="meter"><span class="meter-text">40</span></div>
                                                                <div class="meter"><span class="meter-text">50</span></div>
                                                                <div class="meter"><span class="meter-text">60</span></div>
                                                                <div class="meter"><span class="meter-text">70</span></div>
                                                                <div class="meter"><span class="meter-text">80</span></div>
                                                                <div class="meter"><span class="meter-text">90</span></div>
                                                                <div class="meter"><span class="meter-text">100</span></div>
                                                            </div>
                                                            <div class="clearfix"></div>
                                                            <p class="margin-0 padding-top-20"><spring:theme
                                                                    code="text.mxdAvailable.label"/></p>
                                                            <p><spring:theme code="text.mxdAvailables.list.label"/></p>
                                                            <div class="progress">
                                                                <c:choose>
                                                                    <c:when test="${item.osFullPercent eq 100}">
                                                                        <div class="progress-bar full-bar" role="progressbar"
                                                                             aria-valuenow="${100-item.osFullPercent}"
                                                                             aria-valuemin="0" aria-valuemax="100"
                                                                             style="width:${100-item.osFullPercent}%">
                                                                            <span>  <spring:theme code="text.full.label"
                                                                                                  text="FULL"/> </span>
                                                                        </div>
                                                                    </c:when>
                                                                    <c:otherwise>
                                                                        <div class="progress-bar" role="progressbar"
                                                                             aria-valuenow="${100-item.osFullPercent}"
                                                                             aria-valuemin="0" aria-valuemax="100"
                                                                             style="width:${100-item.osFullPercent}%">
                                                                            <span>  ${100-item.osFullPercent}% </span>
                                                                        </div>
                                                                    </c:otherwise>
                                                                </c:choose>
                                                            </div>
                                                            <div class="progress-meter">
                                                                <div class="meter"><span class="meter-text">0</span></div>
                                                                <div class="meter"><span class="meter-text">10</span></div>
                                                                <div class="meter"><span class="meter-text">20</span></div>
                                                                <div class="meter"><span class="meter-text">30</span></div>
                                                                <div class="meter"><span class="meter-text">40</span></div>
                                                                <div class="meter"><span class="meter-text">50</span></div>
                                                                <div class="meter"><span class="meter-text">60</span></div>
                                                                <div class="meter"><span class="meter-text">70</span></div>
                                                                <div class="meter"><span class="meter-text">80</span></div>
                                                                <div class="meter"><span class="meter-text">90</span></div>
                                                                <div class="meter"><span class="meter-text">100</span></div>
                                                            </div>
                                                            <div class="clearfix"></div>
                                                        </div>
                                                    </div>
                                                </c:otherwise>
                                            </c:choose>
                                        </div>
                                    </c:if>
                                </c:forEach>
                            </c:forEach>
                        </div>
                    </c:when>
                    <c:otherwise>
                        <div class="text-center">
                            <c:choose>
                            <c:when test="${not empty nextSailings}">
                                    <div class="sailing-details-msg"">
                                        <strong>
                                            <span class="bcf bcf-icon-notice-outline bcf-2x"></span>
                                            <spring:theme code="text.cc.no.scheduled.sailing.label" />
                                        </strong>
                                    </div>
                                </c:when>
                                <c:otherwise>
                                    <div class="sailing-details-msg"">
                                <strong><span class="bcf bcf-icon-notice-outline bcf-2x"></span>
                                        <spring:theme code="text.cc.no.sailings.available.label" />
                                        </div>
                                    </c:otherwise>
                                    </c:choose>
                        </div>
                    </c:otherwise>
                </c:choose>


            </div>

            <div class="col-lg-5 col-md-5 col-sm-12 col-xs-12 col-md-offset-1 mobile-padding-0">
                <div class="bg-white p-5">
                    <h6 class="text-center margin-bottom-30">
                        <c:set var="nextSailingText">
                            <spring:theme code="text.cc.nextavailablesailing.info.label" text="Next available info"/>
                        </c:set>
                        <b>
                            <spring:theme code="text.cc.nextavailablesailing.label"/>
                        </b>
                        <a href="javascript:;" data-container="body" data-toggle="popover"
                           data-placement="bottom" class="popoverThis" data-content="${fn:escapeXml(nextSailingText)}">
                            <span class="bcf bcf-icon-info-solid"></span>
                        </a>
                    </h6>
                    <hr class="hr-grey">

                    <c:forEach items="${currentConditionsResponseData.arrivalDepartures}" var="entry">
                        <c:set var="arrivalDepartureRoutes" value="${entry.value}"/>
                    </c:forEach>

                    <c:if test="${not empty currentConditionsResponseData.waits}">
                        <%--Next Undersize Vehicle--%>
                        <c:forEach items="${currentConditionsResponseData.waits}" var="wait">
                            <c:set var="waitData" value="${wait.value}"/>

                            <c:choose>
                                <c:when test="${waitData.estUnderheightSailingWait eq 'P'}">
                                    <c:set var="underheightSailingWaitLength" value="1"/>
                                </c:when>
                                <c:otherwise>
                                    <c:set var="underheightSailingWaitLength" value="${waitData.estUnderheightSailingWait}"/>
                                </c:otherwise>
                            </c:choose>

                            <c:set var="underSizeCouter" value='0'/>
                            <c:forEach items="${arrivalDepartureRoutes}" var="dep" varStatus="usCounter">
                                <fmt:parseDate value="${dep.scheduledDeparture}" var="underSizeDepTime"
                                               pattern="yyyy-MM-dd HH:mm:ss"/>
                                <c:if test="${underSizeDepTime gt now and underSizeCouter < 1}">
                                    <c:set var="underSizeCouter" value='1'/>
                                    <fmt:parseDate
                                            value="${arrivalDepartureRoutes[usCounter.index + underheightSailingWaitLength].scheduledDeparture}"
                                            var="osDepTime" pattern="yyyy-MM-dd HH:mm:ss"/>
                                    <c:set var="underSizeVehicleTime"><fmt:formatDate value="${osDepTime}"
                                                                                      pattern="h:mm a"/></c:set>
                                </c:if>
                            </c:forEach>

                        </c:forEach>

                        <%--Next Oversized Vehicle--%>
                        <c:forEach items="${currentConditionsResponseData.waits}" var="wait">
                            <c:set var="waitData" value="${wait.value}"/>
                            <c:choose>
                                <c:when test="${waitData.estOverheightSailingWait eq 'P'}">
                                    <c:set var="overheightSailingWaitLength" value="1"/>
                                </c:when>
                                <c:otherwise>
                                    <c:set var="overheightSailingWaitLength" value="${waitData.estOverheightSailingWait}"/>
                                </c:otherwise>
                            </c:choose>

                            <c:set var="overSizeCouter" value='0'/>
                            <c:forEach items="${arrivalDepartureRoutes}" var="dep" varStatus="osCounter">
                                <fmt:parseDate value="${dep.scheduledDeparture}" var="overSizeDepTime"
                                               pattern="yyyy-MM-dd HH:mm:ss"/>
                                <c:if test="${overSizeDepTime gt now and overSizeCouter < 1}">
                                    <c:set var="overSizeCouter" value='1'/>
                                    <fmt:parseDate
                                            value="${arrivalDepartureRoutes[osCounter.index + overheightSailingWaitLength].scheduledDeparture}"
                                            var="osDepTime" pattern="yyyy-MM-dd HH:mm:ss"/>
                                    <c:set var="overSizeVehicleTime"><fmt:formatDate value="${osDepTime}"
                                                                                     pattern="h:mm a"/></c:set>
                                </c:if>
                            </c:forEach>

                        </c:forEach>
                        <c:choose>
                            <c:when test="${fn:contains(nextSailingTime,'N')}">
                                <c:set var="sailingTime" value="${fn:substringBefore(nextSailingTime, 'N')}"/>
                            </c:when>
                            <c:otherwise>
                                <c:set var="sailingTime" value="${nextSailingTime}"/>
                            </c:otherwise>
                        </c:choose>
                        <fmt:parseDate value="${sailingTime}" var="sailingDateTime" pattern="yyyy-MM-dd HH:mm:ss"/>
                        <fmt:formatDate value="${sailingDateTime}" var="nextSail" pattern="h:mm a"/>
                        <p>
                            <c:if test="${not empty nextSailingTime}">
                                <span class="bcf bcf-icon-std-car bcf-2x"></span>
                                <span class="word-wrap"><spring:theme code="text.cc.standard.vehicles.label"
                                                                      text="Standard vehicles"/></span>
                                <span class="pull-right">
                                    <c:choose>
                                        <c:when test="${fn:contains(nextSailingTime, 'N')}">
                                            ${nextSail}<br><spring:theme code="text.cc.tomorrow.date.label"/>
                                        </c:when>
                                        <c:otherwise>
                                            ${nextSail}
                                        </c:otherwise>
                                    </c:choose>
                                </span>
                            </c:if></p>
                        <hr class="hr-grey">
                        <p><c:if test="${not empty nextSailingTime}">
                            <span class="bcf bcf-icon-oversize-vehicle dir-right bcf-icon-sm"></span> <span
                                class="word-wrap"><spring:theme code="text.cc.overSized.vehicles.label"
                                                                text="Oversized vehicles"/> </span>
                            <span class="pull-right">
                                <c:choose>
                                    <c:when test="${fn:contains(nextSailingTime, 'N')}">
                                        ${nextSail}<br><spring:theme code="text.cc.tomorrow.date.label"/>
                                    </c:when>
                                    <c:otherwise>
                                        ${nextSail}
                                    </c:otherwise>
                                </c:choose>
                            </span>
                        </c:if></p>
                    </c:if>
                    <hr class="hr-grey">
                </div>
            </div>
        </div>
    </div>
</div>
