<%@ tag body-content="empty" trimDirectiveWhitespaces="true" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ attribute name="searchPageData" required="true" type="de.hybris.platform.commerceservices.search.pagedata.SearchPageData" %>
<%@ taglib prefix="nav" tagdir="/WEB-INF/tags/responsive/nav" %>

<spring:htmlEscape defaultHtmlEscape="false"/>

<c:set var="serviceNotices" value="${searchPageData.results[0]}" />

<c:forEach items="${serviceNotices.serviceNoticesByRouteRegion}" var="entry">
    <h3 class="service-region"><spring:theme code="text.cc.atAglance.${entry.key}.label"/></h3>
    <hr/>
    <c:choose>
        <c:when test="${not empty entry.value}">
            <c:forEach items="${entry.value}" var="serviceNotice">
                <c:forEach items="${serviceNotice.travelRoutes}" var="travelRoute">
                    <a
                        href="?serviceNoticeCode=${serviceNotice.code}&routeCode=${travelRoute.routeCode}">
                        ${serviceNotice.title}&nbsp;-&nbsp;${travelRoute.sourceLocationName}&nbsp;-&nbsp;${travelRoute.sourceTerminalName}
                    </a>
                    <hr />
                </c:forEach>
            </c:forEach>
        </c:when>
        <c:otherwise>
            <spring:theme code="text.cc.no.service.notice.for.route.region" />
        </c:otherwise>
	</c:choose>

</c:forEach>

<nav:serviceNoticePagination top="false" msgKey="text.service.notice.page" showCurrentPageInfo="true" hideRefineButton="true" supportShowPaged="${isShowPageAllowed}" supportShowAll="${isShowAllAllowed}" searchPageData="${searchPageData}" numberPagesShown="${numberPagesShown}" searchUrl="/serviceNotices"/>
<br/>
<br/>
