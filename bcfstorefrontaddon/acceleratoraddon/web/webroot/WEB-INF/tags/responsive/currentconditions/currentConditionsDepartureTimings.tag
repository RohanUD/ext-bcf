<%@ tag body-content="empty" trimDirectiveWhitespaces="true" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ attribute name="currentConditionsResponseData" required="true"
              type="com.bcf.integration.currentconditionsResponse.data.CurrentConditionsResponseData" %>
<%@ attribute name="vesselNames" required="true" type="java.util.Map" %>
<spring:htmlEscape defaultHtmlEscape="false"/>

<c:if test="${not empty currentConditionsResponseData.refreshedAt}">
    <fmt:parseDate var="now" value="${currentConditionsResponseData.refreshedAt}" pattern="yyyy-MM-dd HH:mm:ss"/>
</c:if>

<div class="table-responsive">
    <table class="col-md-12 col-sm-12 col-xs-12 text-center detail-departure-table">
        <tr>
            <td colspan="3">
                <p>
                    <spring:theme code="text.cc.atAglance.departures.sailingTime.label"/>:
                    <b>
                        ${sailingDuration}
                    </b>
                </p>
            </td>
        </tr>
        <tr>
            <th width="33.3%"><spring:theme code="text.cc.departuretime.label"
                                            text="Departure Time"/></th>
            <th width="33.3%"><spring:theme code="text.cc.status.label" text="Status"/></th>
            <th width="33.3%"><spring:theme code="text.cc.details.label"
                                            text="DeckSpace"/></th>
        </tr>
        <c:forEach items="${currentConditionsResponseData.arrivalDepartures}" var="arrivalDepartures">
            <c:forEach items="${arrivalDepartures.value}" var="routeArrivalDepartureData" varStatus="loop">

                <c:choose>
                    <c:when test="${not empty routeArrivalDepartureData.actualDepartureTs}">
                        <fmt:parseDate value="${routeArrivalDepartureData.actualDepartureTs}" var="departureTime"
                                       pattern="yyyy-MM-dd HH:mm:ss"/>
                    </c:when>
                    <c:otherwise>
                        <fmt:parseDate value="${routeArrivalDepartureData.scheduledDeparture}" var="departureTime"
                                       pattern="yyyy-MM-dd HH:mm:ss"/>
                    </c:otherwise>
                </c:choose>
                <c:set var="classGreyBlack" value=""/>
                <c:if test="${departureTime le now}">
                    <c:set var="classGreyBlack" value="text-grey"/>
                </c:if>

                <tr>
                    <td width="33.3%">
                        <p class="${classGreyBlack}"><fmt:formatDate value="${departureTime}" pattern="h:mm a"/></p>
                    </td>
                    <td width="33.3%">
                        <em>
                            <p class="${classGreyBlack}">
                                <fmt:parseDate value="${routeArrivalDepartureData.scheduledArrival}"
                                               var="scheduledArrivalTimeStamp" pattern="yyyy-MM-dd HH:mm:ss"/>

                                <c:set var="actualArrivalTimeStamp" value=""/>
                                <c:if test="${not empty routeArrivalDepartureData.actualArrivalTs and fn:contains(routeArrivalDepartureData.actualArrivalTs, 'variable') eq false}">
                                    <c:choose>
                                        <c:when test="${fn:startsWith(routeArrivalDepartureData.actualArrivalTs, 'ETA')}">
                                            <c:set var="actualArrivalTs">${fn:replace(routeArrivalDepartureData.actualArrivalTs,'ETA: ', '')}</c:set>
                                            <fmt:parseDate value="${actualArrivalTs}" var="actualArrivalTimeStamp"
                                                           pattern="yyyy-MM-dd HH:mm:ss"/>
                                        </c:when>
                                        <c:otherwise>
                                            <fmt:parseDate value="${routeArrivalDepartureData.actualArrivalTs}"
                                                           var="actualArrivalTimeStamp" pattern="yyyy-MM-dd HH:mm:ss"/>
                                        </c:otherwise>
                                    </c:choose>
                                </c:if>
                                <c:choose>
                                <c:when test="${routeArrivalDepartureData.status eq 'S' and routeArrivalDepartureData.webDisplaySailingFlag eq 'Y'}">
                                    <div class="text-red"><spring:theme code="text.cc.cancelled.status.label"/></div>
                                 </c:when>
                                 <c:otherwise>
                                <c:choose>
                                    <c:when test="${departureTime lt now}">
                                        <c:choose>
                                            <%-- Arrived without delay--%>
                                            <c:when test="${empty routeArrivalDepartureData.actualArrivalTs and scheduledArrivalTimeStamp lt now}">
                                                <spring:theme code="text.cc.departures.status.A.label" text="Arrived"/>:
                                                <fmt:formatDate value="${scheduledArrivalTimeStamp}" pattern="h:mm a"/>
                                            </c:when>

                                            <%-- Arrived with delay--%>
                                            <c:when test="${not empty routeArrivalDepartureData.actualArrivalTs and fn:contains(routeArrivalDepartureData.actualArrivalTs, 'variable') eq false and actualArrivalTimeStamp lt now}">
                                                <spring:theme code="text.cc.departures.status.A.label" text="Arrived"/>:
                                                <fmt:formatDate value="${scheduledArrivalTimeStamp}" pattern="h:mm a"/>
                                            </c:when>

                                            <%-- ETA --%>
                                            <c:when test="${not empty routeArrivalDepartureData.actualArrivalTs and fn:startsWith(routeArrivalDepartureData.actualArrivalTs, 'ETA')}">
                                                <spring:theme code="text.eta.label" text="ETA"/>:
                                                <c:choose>
                                                    <c:when test="${fn:contains(routeArrivalDepartureData.actualArrivalTs, 'variable')}">
                                                        <spring:theme code="text.cc.atAglance.departures.variable.label"
                                                                      text="Variable"/>
                                                    </c:when>
                                                    <c:otherwise>
                                                        <fmt:formatDate value="${actualArrivalTimeStamp}" pattern="h:mm a"/>
                                                    </c:otherwise>
                                                </c:choose>
                                            </c:when>
                                            <c:otherwise>
                                                <c:choose>
                                                    <c:when test="${not empty routeArrivalDepartureData.actualArrivalTs}">
                                                        <fmt:formatDate value="${actualArrivalTimeStamp}" pattern="h:mm a"/>
                                                    </c:when>
                                                    <c:otherwise>
                                                        <fmt:formatDate value="${scheduledArrivalTimeStamp}" pattern="h:mm a"/>
                                                    </c:otherwise>
                                                </c:choose>
                                            </c:otherwise>
                                        </c:choose>
                                    </c:when>

                                    <%-- Not Departed Yet--%>
                                    <c:when test="${departureTime gt now}">
                                        <c:choose>
                                            <c:when test="${routeArrivalDepartureData.vesselFullPercent eq 100}">
                                                <spring:theme code="text.cc.full.label"/>
                                            </c:when>
                                            <c:otherwise>
                                                <spring:theme code="text.cc.percentage.available.label" text="% Avalable"
                                                              arguments="${100 - routeArrivalDepartureData.vesselFullPercent}"/>
                                            </c:otherwise>
                                        </c:choose>
                                    </c:when>

                                    <c:otherwise>
                                        <c:choose>
                                            <c:when test="${fn:startsWith(routeArrivalDepartureData.actualArrivalTs, 'ETA')}">
                                                <c:set var="actualArrivalTs">${fn:replace(routeArrivalDepartureData.actualArrivalTs,'ETA: ', '')}</c:set>
                                                <fmt:parseDate value="${actualArrivalTs}" var="actualArrivalTsDate"
                                                               pattern="yyyy-MM-dd HH:mm:ss"/>
                                                <spring:theme code="text.eta.label"/>: <fmt:formatDate
                                                    value="${actualArrivalTsDate}" pattern="h:mm a"/>
                                            </c:when>
                                            <c:otherwise>
                                                <fmt:parseDate value="${routeArrivalDepartureData.actualArrivalTs}"
                                                               var="actualArrivalTsDate" pattern="yyyy-MM-dd HH:mm:ss"/>
                                                <fmt:formatDate value="${actualArrivalTsDate}"
                                                                pattern="h:mm a"/>
                                            </c:otherwise>
                                        </c:choose>
                                    </c:otherwise>
                                </c:choose>
                                </c:otherwise>
                                </c:choose>
                            </p>
                        </em>
                    </td>

                    <c:choose>
                        <c:when test="${departureTime gt  now}">
                            <td width="33.3%" class="toggle-arrow">
                                <i class="bcf bcf-icon-down-arrow"></i>
                            </td>
                        </c:when>
                        <c:otherwise>
                            <td width="33.3%"/>
                        </c:otherwise>
                    </c:choose>
                </tr>
                <c:if test="${departureTime gt  now}">
                    <tr class="toggle-div back-bg-gray">
                        <td colspan="3">
                            <div class="col-md-offset-3 col-md-6">
                                <p class="margin-top-30">

                                    <c:choose>
                                        <c:when test="${not empty vesselNames[routeArrivalDepartureData.vessel].name}">
                                            <c:set var="vesselName"
                                                   value="${vesselNames[routeArrivalDepartureData.vessel].name}"/>
                                            <c:set var="ferryTranslatedName"
                                                   value="${vesselNames[routeArrivalDepartureData.vessel].translatedName}"/>
                                        </c:when>
                                        <c:otherwise>
                                            <c:set var="vesselName" value="${routeArrivalDepartureData.vessel}"/>
                                            <c:set var="ferryTranslatedName" value="${routeArrivalDepartureData.vessel}"/>
                                        </c:otherwise>
                                    </c:choose>
                                    <spring:theme code="text.cc.ferry.label"/>:
                                    <a href="/ship-info/${vesselNames[routeArrivalDepartureData.vessel].shipInfoCode}" class="sailing-ferry-name" data-toggle="modal" data-target="#detailsModal">
                                        ${vesselName}
                                    </a>
                                </p>
                                <c:choose>
                                <c:when test="${routeArrivalDepartureData.webDisplaySailingFlag eq 'Y' and routeArrivalDepartureData.status eq 'S'}">
                                    <div class="sailing-cancelled-msg margin-top-10 margin-bottom-20">
                                        <span class="bcf bcf-icon-alert"></span>
                                        <spring:theme code="text.cc.cancelled.sailing.label"/>
                                    </div>
                                    <spring:theme code="text.cc.cancelled.sailing.reason.label"/>
                                </c:when>
                                <c:otherwise>
                                <p class="text-center">
                                    <c:set var="totalDeckSpaceText">
                                        <spring:theme code="text.cc.deckspaceAvailable.label" text="Total deck space available"/>
                                    </c:set>

                                    <spring:theme code="text.cc.deckspaceAvailable.label" text="Total deck space available"/>

                                    
                                    <a href="javascript:;" data-container="body" data-toggle="popover" data-placement="bottom" data-html="true" class="popoverThis" data-content="Info test data goes here..">
                                        <span class="bcf bcf-icon-info-solid"></span> 
                                    </a>
                                </p>
                                <div id="deckSpace">
                                    <div class="progress">
                                        <c:choose>
                                            <c:when test="${routeArrivalDepartureData.vesselFullPercent eq 100}">
                                                <div class="progress-bar full-bar" role="progressbar"
                                                     aria-valuenow="${100-routeArrivalDepartureData.vesselFullPercent}"
                                                     aria-valuemin="0" aria-valuemax="100"
                                                     style="width:${100-routeArrivalDepartureData.vesselFullPercent}%">
                                                    <span>  <spring:theme code="text.full.label" text="FULL"/> </span>
                                                </div>
                                            </c:when>
                                            <c:otherwise>
                                                <div class="progress-bar" role="progressbar"
                                                     aria-valuenow="${100-routeArrivalDepartureData.vesselFullPercent}"
                                                     aria-valuemin="0" aria-valuemax="100"
                                                     style="width:${100-routeArrivalDepartureData.vesselFullPercent}%">
                                                    <span>  ${100-routeArrivalDepartureData.vesselFullPercent}% </span>
                                                </div>
                                            </c:otherwise>
                                        </c:choose>
                                    </div>
                                    <div class="progress-meter">
                                        <div class="meter"><span class="meter-text">0</span></div>
                                        <div class="meter"><span class="meter-text">10</span></div>
                                        <div class="meter"><span class="meter-text">20</span></div>
                                        <div class="meter"><span class="meter-text">30</span></div>
                                        <div class="meter"><span class="meter-text">40</span></div>
                                        <div class="meter"><span class="meter-text">50</span></div>
                                        <div class="meter"><span class="meter-text">60</span></div>
                                        <div class="meter"><span class="meter-text">70</span></div>
                                        <div class="meter"><span class="meter-text">80</span></div>
                                        <div class="meter"><span class="meter-text">90</span></div>
                                        <div class="meter"><span class="meter-text">100</span></div>
                                    </div>
                                    <div class="clearfix"></div>

                                    <p class="view-deck margin-top-30"><spring:theme code="text.cc.viewDeckspaceDetails.label"
                                                                                     text="View deck space details"/> <i
                                            class="bcf bcf-icon-down-arrow"></i></p>
                                    <div class="view-deck-div">
                                        <p class="margin-0 padding-top-20"><spring:theme code="text.stdAvailable.label"/></p>
                                        <p><spring:theme code="text.stdAvailables.list.label"/></p>
                                        <div>
                                            <div class="progress">

                                                <c:choose>
                                                    <c:when test="${routeArrivalDepartureData.uhFullPercent eq 100}">
                                                        <div class="progress-bar full-bar" role="progressbar"
                                                             aria-valuenow="${100-routeArrivalDepartureData.uhFullPercent}"
                                                             aria-valuemin="0" aria-valuemax="100"
                                                             style="width:${100-routeArrivalDepartureData.uhFullPercent}%">
                                                            <span>  <spring:theme code="text.full.label" text="FULL"/> </span>
                                                        </div>
                                                    </c:when>
                                                    <c:otherwise>
                                                        <div class="progress-bar" role="progressbar"
                                                             aria-valuenow="${100-routeArrivalDepartureData.uhFullPercent}"
                                                             aria-valuemin="0" aria-valuemax="100"
                                                             style="width:${100-routeArrivalDepartureData.uhFullPercent}%">
                                                            <span>  ${100-routeArrivalDepartureData.uhFullPercent}% </span>
                                                        </div>
                                                    </c:otherwise>
                                                </c:choose>

                                            </div>
                                            <div class="progress-meter">
                                                <div class="meter"><span class="meter-text">0</span></div>
                                                <div class="meter"><span class="meter-text">10</span></div>
                                                <div class="meter"><span class="meter-text">20</span></div>
                                                <div class="meter"><span class="meter-text">30</span></div>
                                                <div class="meter"><span class="meter-text">40</span></div>
                                                <div class="meter"><span class="meter-text">50</span></div>
                                                <div class="meter"><span class="meter-text">60</span></div>
                                                <div class="meter"><span class="meter-text">70</span></div>
                                                <div class="meter"><span class="meter-text">80</span></div>
                                                <div class="meter"><span class="meter-text">90</span></div>
                                                <div class="meter"><span class="meter-text">100</span></div>
                                            </div>
                                            <div class="clearfix"></div>

                                            <p class="margin-0 padding-top-20"><spring:theme code="text.mxdAvailable.label"/></p>
                                            <p><spring:theme code="text.mxdAvailables.list.label"/></p>
                                            <div class="progress">
                                                <c:choose>
                                                    <c:when test="${routeArrivalDepartureData.osFullPercent eq 100}">
                                                        <div class="progress-bar full-bar" role="progressbar"
                                                             aria-valuenow="${100-routeArrivalDepartureData.osFullPercent}"
                                                             aria-valuemin="0" aria-valuemax="100"
                                                             style="width:${100-routeArrivalDepartureData.osFullPercent}%">
                                                            <span>  <spring:theme code="text.full.label" text="FULL"/> </span>
                                                        </div>
                                                    </c:when>
                                                    <c:otherwise>
                                                        <div class="progress-bar" role="progressbar"
                                                             aria-valuenow="${100-routeArrivalDepartureData.osFullPercent}"
                                                             aria-valuemin="0" aria-valuemax="100"
                                                             style="width:${100-routeArrivalDepartureData.osFullPercent}%">
                                                            <span>  ${100-routeArrivalDepartureData.osFullPercent}% </span>
                                                        </div>
                                                    </c:otherwise>
                                                </c:choose>
                                            </div>
                                            <div class="progress-meter">
                                                <div class="meter"><span class="meter-text">0</span></div>
                                                <div class="meter"><span class="meter-text">10</span></div>
                                                <div class="meter"><span class="meter-text">20</span></div>
                                                <div class="meter"><span class="meter-text">30</span></div>
                                                <div class="meter"><span class="meter-text">40</span></div>
                                                <div class="meter"><span class="meter-text">50</span></div>
                                                <div class="meter"><span class="meter-text">60</span></div>
                                                <div class="meter"><span class="meter-text">70</span></div>
                                                <div class="meter"><span class="meter-text">80</span></div>
                                                <div class="meter"><span class="meter-text">90</span></div>
                                                <div class="meter"><span class="meter-text">100</span></div>
                                            </div>
                                            <div class="clearfix"></div>
                                        </div>
                                    </div>
                                    </c:otherwise>
                                    </c:choose>
                                </div>
                            </div>
                        </td>
                    </tr>
                </c:if>
            </c:forEach>
        </c:forEach>
         <c:forEach items="${nextSailings}" var="nextSailing" varStatus="loop">
                    <c:set var="classGreyBlack" value=""/>
                    <tr>
                        <td width="33.3%">
                        <fmt:parseDate value="${nextSailing.departure}" var="dept"
                                          							   pattern="yyyy-MM-dd HH:mm:ss"/>
                                          				<fmt:formatDate value="${dept}" var="formattedDepartureDate" pattern="h:mm a"/>
                            <p class="${classGreyBlack}">${formattedDepartureDate}<br/>

                            <fmt:formatDate value="${dept}" var="formattedDeptDate" pattern="yyyy-MM-dd"/>
                            <c:if test="${tomorrowDate eq formattedDeptDate}">
                                <spring:theme code="text.cc.tomorrow.date.label"/>
                            </c:if>
                            </p>
                        </td>
                        <td width="33.3%">
                            <em>
                                <p class="${classGreyBlack}">
                                            <c:choose>
                                                <c:when test="${nextSailing.vesselFullPercent eq 100}">
                                                    <spring:theme code="text.cc.full.label"/>
                                                </c:when>
                                                <c:otherwise>
                                                    <spring:theme code="text.cc.percentage.available.label" text="% Avalable"
                                                                  arguments="${100 - nextSailing.vesselFullPercent}"/>
                                                </c:otherwise>
                                            </c:choose>
                                </p>
                            </em>
                        </td>

                                <td width="33.3%" class="toggle-arrow">
                                    <i class="bcf bcf-icon-down-arrow"></i>
                                </td>
                    </tr>
                        <tr class="toggle-div back-bg-gray">
                            <td colspan="3">
                                <div class="col-md-offset-3 col-md-6">
                                    <p class="margin-top-30">

                                        <c:choose>
                                            <c:when test="${not empty nextSailing.vessel[routeArrivalDepartureData.vessel]}">
                                                <c:set var="vesselName"
                                                       value="${nextSailing.vessel[routeArrivalDepartureData.vessel]}"/>
                                                <c:set var="ferryTranslatedName"
                                                       value="${nextSailing.vessel[routeArrivalDepartureData.vessel]}"/>
                                            </c:when>
                                            <c:otherwise>
                                                <c:set var="vesselName" value="${nextSailing.vessel}"/>
                                                <c:set var="ferryTranslatedName" value="${nextSailing.vessel}"/>
                                            </c:otherwise>
                                        </c:choose>

                                        <spring:theme code="text.cc.ferry.label"/>:
                                        <a href="/ship-info/${routeArrivalDepartureData.vessel}"  class="sailing-ferry-name" data-toggle="modal" data-target="#detailsModal">
                                            ${vesselName}
                                        </a>
                                    </p>
                                    <p class="text-center">
                                        <c:set var="totalDeckSpaceText">
                                            <spring:theme code="text.cc.deckspaceAvailable.label" text="Total deck space available"/>
                                        </c:set>

                                        <spring:theme code="text.cc.deckspaceAvailable.label" text="Total deck space available"/>

                                        <a href="javascript:;" data-container="body" data-toggle="popover" data-placement="bottom" data-html="true" class="popoverThis" data-content="Info test data goes here..">
                                            <span class="bcf bcf-icon-info-solid"></span> 
                                        </a>
                                    </p>
                                    <div id="deckSpace">
                                        <div class="progress">
                                            <c:choose>
                                            <c:when test="${nextSailing.status eq 'S' and nextSailing.webDisplaySailingFlag eq 'Y'}">
                                                <span class="icon-alert"></span>
                                                <strong><spring:theme code="text.cc.cancelled.sailing.label"/><br></strong>
                                            </c:when>
                                            <c:otherwise>
                                            <c:choose>
                                                <c:when test="${nextSailing.vesselFullPercent eq 100}">
                                                    <div class="progress-bar full-bar" role="progressbar"
                                                         aria-valuenow="${100-nextSailing.vesselFullPercent}"
                                                         aria-valuemin="0" aria-valuemax="100"
                                                         style="width:${100-nextSailing.vesselFullPercent}%">
                                                        <span>  <spring:theme code="text.full.label" text="FULL"/> </span>
                                                    </div>
                                                </c:when>
                                                <c:otherwise>
                                                    <div class="progress-bar" role="progressbar"
                                                         aria-valuenow="${100-nextSailing.vesselFullPercent}"
                                                         aria-valuemin="0" aria-valuemax="100"
                                                         style="width:${100-nextSailing.vesselFullPercent}%">
                                                        <span>  ${100-nextSailing.vesselFullPercent}% </span>
                                                    </div>
                                                </c:otherwise>
                                            </c:choose>
                                            </c:otherwise>
                                            </c:choose>
                                        </div>
                                        <div class="progress-meter">
                                            <div class="meter"><span class="meter-text">0</span></div>
                                            <div class="meter"><span class="meter-text">10</span></div>
                                            <div class="meter"><span class="meter-text">20</span></div>
                                            <div class="meter"><span class="meter-text">30</span></div>
                                            <div class="meter"><span class="meter-text">40</span></div>
                                            <div class="meter"><span class="meter-text">50</span></div>
                                            <div class="meter"><span class="meter-text">60</span></div>
                                            <div class="meter"><span class="meter-text">70</span></div>
                                            <div class="meter"><span class="meter-text">80</span></div>
                                            <div class="meter"><span class="meter-text">90</span></div>
                                            <div class="meter"><span class="meter-text">100</span></div>
                                        </div>
                                        <div class="clearfix"></div>

                                        <p class="view-deck margin-top-30"><spring:theme code="text.cc.viewDeckspaceDetails.label"
                                                                                         text="View deck space details"/> <i
                                                class="bcf bcf-icon-down-arrow"></i></p>
                                        <div class="view-deck-div">
                                            <p class="margin-0 padding-top-20"><spring:theme code="text.stdAvailable.label"/></p>
                                            <p><spring:theme code="text.stdAvailables.list.label"/></p>
                                            <div>
                                                <div class="progress">

                                                    <c:choose>
                                                        <c:when test="${nextSailing.uhFullPercent eq 100}">
                                                            <div class="progress-bar full-bar" role="progressbar"
                                                                 aria-valuenow="${100-nextSailing.uhFullPercent}"
                                                                 aria-valuemin="0" aria-valuemax="100"
                                                                 style="width:${100-nextSailing.uhFullPercent}%">
                                                                <span>  <spring:theme code="text.full.label" text="FULL"/> </span>
                                                            </div>
                                                        </c:when>
                                                        <c:otherwise>
                                                            <div class="progress-bar" role="progressbar"
                                                                 aria-valuenow="${100-nextSailing.uhFullPercent}"
                                                                 aria-valuemin="0" aria-valuemax="100"
                                                                 style="width:${100-nextSailing.uhFullPercent}%">
                                                                <span>  ${100-nextSailing.uhFullPercent}% </span>
                                                            </div>
                                                        </c:otherwise>
                                                    </c:choose>

                                                </div>
                                                <div class="progress-meter">
                                                    <div class="meter"><span class="meter-text">0</span></div>
                                                    <div class="meter"><span class="meter-text">10</span></div>
                                                    <div class="meter"><span class="meter-text">20</span></div>
                                                    <div class="meter"><span class="meter-text">30</span></div>
                                                    <div class="meter"><span class="meter-text">40</span></div>
                                                    <div class="meter"><span class="meter-text">50</span></div>
                                                    <div class="meter"><span class="meter-text">60</span></div>
                                                    <div class="meter"><span class="meter-text">70</span></div>
                                                    <div class="meter"><span class="meter-text">80</span></div>
                                                    <div class="meter"><span class="meter-text">90</span></div>
                                                    <div class="meter"><span class="meter-text">100</span></div>
                                                </div>
                                                <div class="clearfix"></div>

                                                <p class="margin-0 padding-top-20"><spring:theme code="text.mxdAvailable.label"/></p>
                                                <p><spring:theme code="text.mxdAvailables.list.label"/></p>
                                                <div class="progress">
                                                    <c:choose>
                                                        <c:when test="${nextSailing.osFullPercent eq 100}">
                                                            <div class="progress-bar full-bar" role="progressbar"
                                                                 aria-valuenow="${100-nextSailing.osFullPercent}"
                                                                 aria-valuemin="0" aria-valuemax="100"
                                                                 style="width:${100-nextSailing.osFullPercent}%">
                                                                <span>  <spring:theme code="text.full.label" text="FULL"/> </span>
                                                            </div>
                                                        </c:when>
                                                        <c:otherwise>
                                                            <div class="progress-bar" role="progressbar"
                                                                 aria-valuenow="${100-nextSailing.osFullPercent}"
                                                                 aria-valuemin="0" aria-valuemax="100"
                                                                 style="width:${100-nextSailing.osFullPercent}%">
                                                                <span>  ${100-nextSailing.osFullPercent}% </span>
                                                            </div>
                                                        </c:otherwise>
                                                    </c:choose>
                                                </div>
                                                <div class="progress-meter">
                                                    <div class="meter"><span class="meter-text">0</span></div>
                                                    <div class="meter"><span class="meter-text">10</span></div>
                                                    <div class="meter"><span class="meter-text">20</span></div>
                                                    <div class="meter"><span class="meter-text">30</span></div>
                                                    <div class="meter"><span class="meter-text">40</span></div>
                                                    <div class="meter"><span class="meter-text">50</span></div>
                                                    <div class="meter"><span class="meter-text">60</span></div>
                                                    <div class="meter"><span class="meter-text">70</span></div>
                                                    <div class="meter"><span class="meter-text">80</span></div>
                                                    <div class="meter"><span class="meter-text">90</span></div>
                                                    <div class="meter"><span class="meter-text">100</span></div>
                                                </div>
                                                <div class="clearfix"></div>
                                            </div>
                                        </div>

                                    </div>
                                </div>
                            </td>
                        </tr>
                </c:forEach>
            <c:if test="${noMoreSailings eq 'true' and empty nextSailings}">
                <tr>
                    <td colspan="3">
                        <p class="margin-top-20 margin-bottom-10 text-black"><strong><span class="bcf bcf-icon-notice-outline bcf-2x"></span>
                        <spring:theme code="text.cc.no.sailings.available.label"/></strong></p>
                        </td>
                    </tr>
            </c:if>
    </table>
</div>

<p class="margin-top-20 text-grey">
    <c:forEach items="${serviceNotices}" var="serviceNotice">
        <spring:theme code="text.cc.notcies.label" text="Notices"/>
        <c:url var="serviceNoticesUrl" value="/serviceNotices">
            <c:param name="serviceNoticeCode" value="${serviceNotice.code}"/>
            <c:forEach items="${serviceNotice.travelRoutes}" var="travelRoute">
                <c:if test="${travelRoute.sourceTerminalCode eq routeInfo.departureTerminalCode and travelRoute.destinationTerminalCode eq routeInfo.arrivalTerminalCode}">
                    <c:param name="routeCode" value="${travelRoute.routeCode}"/>
                </c:if>
            </c:forEach>
        </c:url>
        <span class="bcf bcf-icon-notice-outline bcf-2x"></span><a href="${serviceNoticesUrl}"
                                                    target="_blank"> ${serviceNotice.title} </a> &nbsp;
    </c:forEach>
</p>
