<%@ tag body-content="empty" trimDirectiveWhitespaces="true" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="format" tagdir="/WEB-INF/tags/shared/format" %>
<%@ taglib prefix="ycommerce" uri="http://hybris.com/tld/ycommercetags" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="ccSailingDetails" tagdir="/WEB-INF/tags/addons/bcfstorefrontaddon/responsive/currentconditions" %>
<%@ taglib prefix="currentconditions" tagdir="/WEB-INF/tags/addons/bcfvoyageaddon/responsive/currentconditions"%>
<%@ attribute name="currentConditionsData" required="true" type="de.hybris.platform.commercefacades.travel.CurrentConditionsData" %>
<%@ attribute name="routeInfo" required="true" type="com.bcf.facades.schedules.DestinationAndOriginForTravelRouteData" %>

<c:set var="currentConditionsResponseData" value="${currentConditionsData.currentConditionsResponseData[0]}"/>
<c:set var="vesselNames" value="${currentConditionsData.vessels}"/>

<%-- Cams --%>
<div id="tabs" class="custom-ui-detail">
    <ul>
        <input type="hidden" id="selectedTab" value="${param['selectedTab']}">
        <li>
            <a class="ccTabJs" name="tabs-1" href="#tabs-1"><spring:theme code="label.current.condition.details.departures"
                                                                          text="Departures"/> </a>
        </li>
        <li>
            <a class="ccTabJs" name="tabs-2" href="#tabs-2"><spring:theme code="label.current.condition.details.ferry.tracking"
                                                                          text="Ferry tracking"/></a>
        </li>
    </ul>
    <div id="tabs-1">
        <ccSailingDetails:currentConditionsDepartureTimings currentConditionsResponseData="${currentConditionsResponseData}"
                                                            vesselNames="${vesselNames}"/>
    </div>
    <div id="tabs-2">
        <currentconditions:currentconditionsferrytracking ferryTrackingLink="${ferryTrackingLink}"
                                                          sourceLocation="${routeInfo.departureTerminalCode}"
                                                          sourceLocationName="${routeInfo.departureLocationName}"
									                      sourceTerminal="${routeInfo.departureTerminalName}"
                                                          sourceTerminalName="${routeInfo.departureRouteName}"

                                                          destinationLocation="${routeInfo.arrivalTerminalCode}"
                                                          destinationLocationName="${routeInfo.arrivalLocationName}"
									                      destinationTerminal="${routeInfo.arrivalTerminalName}"
                                                          destinationTerminalName="${routeInfo.arrivalRouteName}"/>
    </div>
</div>
