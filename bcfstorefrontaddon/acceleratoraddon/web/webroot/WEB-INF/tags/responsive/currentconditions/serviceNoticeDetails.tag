<%@ tag body-content="empty" trimDirectiveWhitespaces="true" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ attribute name="serviceNotice" required="true" type="de.hybris.platform.commercefacades.travel.ServiceNoticeData" %>
<spring:htmlEscape defaultHtmlEscape="false"/>

<c:set var="travelRoute" value="${serviceNotice.travelRoutes[0]}"/>
<h2 class="title">
	<spring:theme code="text.cc.atAglance.serviceNotice.label" />
	&nbsp;-&nbsp;${serviceNotice.title}-${travelRoute.sourceLocationName}&nbsp;-&nbsp;${travelRoute.sourceTerminalName}
</h2>

<h3 class="service-region">
	<spring:theme code="text.cc.atAglance.serviceNotice.posted.label" />
	&nbsp;
	<fmt:formatDate value="${serviceNotice.publishedDate}" dateStyle="long"
		pattern="EEEE, MMMM dd',' yyyy" />
</h3>

<p>
    ${serviceNotice.bodyContent}
</p>
