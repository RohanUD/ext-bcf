<%@ taglib prefix = "fmt" uri = "http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ attribute name="displayCount" required="true" type="java.lang.String"%>

<c:choose>
    <c:when test="${not empty displayCount}">
        <fmt:parseNumber var = "colWidth" type = "number" value = "${12/displayCount}" integerOnly="true"/>
        <c:set var="colWidthClass" value="col-lg-${colWidth} col-md-${colWidth} col-sm-${colWidth} col-xs-12" scope="request"/>
    </c:when>
    <c:otherwise>
        <c:set var="colWidthClass" value="col-lg-3 col-md-3 col-sm-3 col-xs-12" scope="request"/>
    </c:otherwise>
</c:choose>

