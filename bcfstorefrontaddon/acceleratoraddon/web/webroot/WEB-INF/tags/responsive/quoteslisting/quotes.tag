<%@ tag language="java" pageEncoding="UTF-8"%>
<%@ tag body-content="empty" trimDirectiveWhitespaces="true"%>
<%@ taglib prefix="ferry" tagdir="/WEB-INF/tags/addons/bcfstorefrontaddon/responsive/ferrylisting"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ attribute name="quotes" required="true" type="java.util.List"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="format" tagdir="/WEB-INF/tags/shared/format"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="ycommerce" uri="http://hybris.com/tld/ycommercetags"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<c:if test="${fn:length(quotes) gt 0}">
     <c:forEach items="${quotes}" var="quote">
<div class="quotes-lists-wrapper">
<table class="quotes-lists-table responsive-table">
                 <spring:url value="/my-account/my-quotes/{/cartcode}/" var="quoteDetailLink" htmlEscape="false">
                    <spring:param name="cartcode" value="${quote.code}" />
                 </spring:url>
           <tr class="account-orderhistory-table-head responsive-table-head">
              <th>
                 <h4 class="mb-0"><spring:theme code="label.quote.id" /><span> ${fn:escapeXml(quote.code)}</span></h4>
              </th>
           </tr>


            <c:forEach items="${quote.quoteAccommodationDatas}" var="quoteAccommodationData">
               <fmt:formatDate value="${quoteAccommodationData.checkInDate}" pattern="MMM dd, yyyy" var="checkInDate" />
               <fmt:formatDate value="${quoteAccommodationData.checkOutDate}" pattern="MMM dd, yyyy" var="checkOutDate" />
               <tr>
                  <td>
                     <p><strong><spring:theme code="label.quote.hotel" />:</strong>
                     <span>${quoteAccommodationData.name}</span></p>

                     <p><strong><spring:theme code="text.vacation.packagefinder.checkindate.title" />:</strong>
                     <span>${checkInDate}</span></p>
                     <p><strong><spring:theme code="text.vacation.packagefinder.checkoutdate.title" />:</strong>
                     <span>${checkOutDate}</span></p>
                  </td>
               </tr>

            </c:forEach>
             <tr>
              <td><hr/></td>
           </tr>
            <c:forEach items="${quote.quoteActivityDatas}" var="quoteActivityData">
                <tr>
                    <td>

                        <fmt:parseDate value="${quoteActivityData.activityDate}" var="parsedActivityDate" pattern="MMM dd yyyy" />
                        <fmt:formatDate value="${parsedActivityDate}" var="formattedParsedActivityDate" pattern="EEEE, MMM dd" />
                        <p><strong><spring:theme code="label.quote.activity" />:</strong>
                        <span>${quoteActivityData.name}</span></p>
                        <p><strong><spring:theme code="label.quote.activity.date" />:</strong>
                        <span>${formattedParsedActivityDate}</span></p>
                        <c:if test="${not empty quoteActivityData.activityTime}">
                            <p><strong>><spring:theme code="label.quote.activity.time" />:</strong>
                            <span>${activityTime}</span></p>
                        </c:if>
                     </td>
                 </tr>
            </c:forEach>
            <c:if test="${not empty quote.quoteActivityDatas}">
            <tr>
              <td class="activity"><hr/></td>
           </tr>
           </c:if>
           <c:forEach items="${quote.quoteSailingDatas}" var="quoteSailingData">
                <fmt:formatDate value="${quoteSailingData.departureDate}" pattern="MMM dd, yyyy" var="departureDate" />
                <fmt:formatDate value="${quoteSailingData.departureDate}" pattern="hh:mm a" var="departureTime" />
                <tr>
                    <td>

                        <c:set var="dept" value="${fn:substringBefore(fn:substringAfter(quoteSailingData.route,' '),'-')}"/>
                        <c:set var="arr" value="${fn:substringAfter(fn:substringAfter(quoteSailingData.route,'-'),' ')}"/>
                        <p><strong><spring:theme code="label.quote.route" />:</strong>
                        <span>${dept}&nbsp;<spring:theme code="label.quote.route.to"/>&nbsp;${arr}</span></p>
                        <p><strong><spring:theme code="label.quote.departure.sailing" />:</strong>
                        </span>${departureDate}&nbsp;<spring:theme code="label.quote.route.at"/> &nbsp;${departureTime}, <a href="/ship-info/${quoteSailingData.departureVehicleCode}" class="sailing-ferry-name" data-toggle="modal" data-target="#detailsModal">${quoteSailingData.departureVehicle}</a></span></p>
                        <c:if test="${not empty quoteSailingData.returnDate}">
                            <fmt:formatDate value="${quoteSailingData.returnDate}" pattern="MMM dd, yyyy" var="returnDate" />
                            <fmt:formatDate value="${quoteSailingData.returnDate}" pattern="hh:mm a" var="returnTime" />
                            <p><strong><spring:theme code="label.quote.return.sailing" />:</strong>
                            <span>${returnDate}&nbsp;<spring:theme code="label.quote.route.at"/> &nbsp;${returnTime}, <a href="/ship-info/${quoteSailingData.returnVehicleCode}" class="sailing-ferry-name" data-toggle="modal" data-target="#detailsModal">${quoteSailingData.returnVehicle}</a></span></p>
                        </c:if>
                     </td>
                 </tr>

            </c:forEach>
        <tr>
        <td class="view-quote-details">
        <div class="row">
            <div class="col-md-8 col-sm-6 col-md-offset-0 col-xs-12">
                  <a href="${quoteDetailLink}" class="btn btn-primary btn-block find-button"><spring:theme code="label.quote.view.quote.details"/></a>
               </div>
         </div>
        </td>
        </tr>

    </table>
    </div>
     </c:forEach>



    </c:if>
