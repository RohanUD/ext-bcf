<%@ tag body-content="empty" trimDirectiveWhitespaces="true" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="nav" tagdir="/WEB-INF/tags/responsive/nav" %>
<%@ taglib prefix="cms" uri="http://hybris.com/tld/cmstags"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>

<div class="container">
   <div class="row">
      <div class="col-lg-12 col-xs-12">
         <h3>
            <strong>
            <spring:theme code="product.activity.termsConditions" text="Terms & Conditions"/>
         </h3>
      </div>
   </div>
</div>
<div class="container-fluid margin-top-30 margin-bottom-40">
   <div class="row">
      <div class="payment-agree">
         <div class="col-lg-12 col-xs-12">
            <cms:pageSlot position="TermsConditions" var="feature" element="section">
               <c:if test ="${bookingJourney eq 'BOOKING_TRANSPORT_ONLY'}">
                  <cms:component component="${feature}" />
               </c:if>
            </cms:pageSlot>
         </div>
         <div class="container">
            <div class="vacation-booking-details">
               <div class="row">
                  <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 mb-5">
                     <ul class="list-unstyled payment payment-confirmation-list">
                        <c:if test="${not empty vacationPolicyTermsAndConditions && not empty vacationPolicyTermsAndConditions.componentTermsAndConditions}">
                           <p>
                              <strong>
                                 <spring:theme code="text.vacation.default.termsConditions" text=" BC Ferries Vacations Exceptions Policies"/>
                              </strong>
                           </p>
                           <c:forEach items="${vacationPolicyTermsAndConditions.componentTermsAndConditions}" var="componentTermsAndCondition" >
                              <c:if test="${not empty componentTermsAndCondition.accommodationOfferings}">
                                 <c:forEach items="${componentTermsAndCondition.accommodationOfferings}" var="accommodationOffering" >
                                    <li>
                                       <b>${accommodationOffering}</b>
                                    </li>
                                    <li>
                                       ${componentTermsAndCondition.termsAndConditions}
                                    </li>
                                 </c:forEach>
                              </c:if>
                              <c:if test="${not empty componentTermsAndCondition.activityProducts}">
                                 <c:forEach items="${componentTermsAndCondition.activityProducts}" var="activityProduct">
                                    <li>
                                       <b> ${activityProduct}</b>
                                    </li>
                                    <li>
                                       ${componentTermsAndCondition.termsAndConditions}
                                    </li>
                                 </c:forEach>
                              </c:if>
                           </c:forEach>
                        </c:if>
                     </ul>
                  </div>
               </div>
            </div>
         </div>
         <div class="container">
            <div class="vacation-booking-details">
               <div class="row">
                  <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 mb-5">
                     <ul class="list-unstyled payment payment-confirmation-list">
                        <c:if test="${not empty vacationPolicyTermsAndConditions && not empty vacationPolicyTermsAndConditions.packageTermsAndConditions}">
                           <p>
                              <strong> ${vacationPolicyTermsAndConditions.title} </strong>
                           </p>
                           <li>
                              ${vacationPolicyTermsAndConditions.packageTermsAndConditions}
                           </li>
                        </c:if>
                     </ul>
                  </div>
               </div>
            </div>
         </div>
         <c:set var="agreement">
            <spring:theme code="text.payment.consent.vacations.terms.conditions" text="terms and conditions" />
         </c:set>
         <c:if test="${bookingJourney eq 'BOOKING_TRANSPORT_ONLY'}">
            <c:set var="agreement">
               <spring:theme code="text.payment.consent.terms.conditions" text="terms and conditions" />
            </c:set>
         </c:if>
         <div class="container">
            <div class="row">
               <div class="col-lg-12 col-xs-12">
                  <label class="custom-checkbox-input">
                     ${agreement}
                     <form:checkbox path="termsCheck" />
                     <input type="checkbox">
                     <span class="checkmark-checkbox"></span>
                  </label>
               </div>
            </div>
         </div>
      </div>
   </div>
</div>
