<%@ tag body-content="empty" trimDirectiveWhitespaces="true"%>
<%@ attribute name="actionNameKey" required="true" type="java.lang.String"%>
<%@ attribute name="action" required="true" type="java.lang.String"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="formElement" tagdir="/WEB-INF/tags/responsive/formElement"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="ycommerce" uri="http://hybris.com/tld/ycommercetags"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<div class="headline">
	<spring:theme code="guest.checkout" arguments="${fn:escapeXml(siteName)}" />
</div>
<form:form action="${action}" method="post" commandName="bcfGuestForm">
	<div class="form-group">
		<formElement:formInputBox idKey="guest.email" labelKey="guest.email" inputCSS="guestEmail" path="email" mandatory="true" />
	</div>
	<div class="form-group">
		<label class="control-label" for="guest.confirm.email">
			<spring:theme code="guest.confirm.email" />
		</label>
		<input class="confirmGuestEmail form-control" id="guest.confirm.email" />
	</div>
	<div class="form-group">
		<formElement:formInputBox idKey="guest.firstName" labelKey="guest.firstName" inputCSS="form-control" path="firstName" mandatory="true" />
	</div>
	<div class="form-group">
		<formElement:formInputBox idKey="guest.lastName" labelKey="guest.lastName" inputCSS="form-control" path="lastName" mandatory="true" />
	</div>
	PhoneType :
	<form:radiobutton path="phoneType" value="Personal"/>Personal
   <form:radiobutton path="phoneType" value="Work" />Work
   <form:radiobutton path="phoneType" value="Other" />Other	
	<div class="form-group">
		<formElement:formInputBox idKey="guest.phoneNo" labelKey="guest.phoneNo" inputCSS="form-control" path="phoneNo" mandatory="true" />
	</div>		
	<ycommerce:testId code="guest_Checkout_button">
		<div class="form-group">
			<button type="submit" disabled="true" class="btn btn-default btn-block guestCheckoutBtn">
				<spring:theme code="${actionNameKey}" />
			</button>
		</div>
	</ycommerce:testId>
</form:form>
