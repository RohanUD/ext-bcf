<%@ tag body-content="empty" trimDirectiveWhitespaces="true"%>
<%@ attribute name="actionNameKey" required="true"	type="java.lang.String"%>
<%@ attribute name="action" required="true" type="java.lang.String"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="formElement"	tagdir="/WEB-INF/tags/responsive/formElement"%>
<%@ taglib prefix="theme" tagdir="/WEB-INF/tags/shared/theme"%>
<%@ taglib prefix="ycommerce" uri="http://hybris.com/tld/ycommercetags"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="common" tagdir="/WEB-INF/tags/responsive/common" %>

<div class="user-register__headline">
	<h4><spring:theme code="register.new.customer" /></h4>
</div>
<p class="fnt-14">
	<spring:theme code="register.description" htmlEscape="false"/>
</p>

<p>
	<span class="red-text">*</span>
	<spring:theme code="fields.required.desc" text="These fields are required"/>
</p>
<form:form method="post" commandName="BCFSignupForm" action="${action}" >
	<c:set var="selectAccountType"><spring:theme code="text.account.select.accountType"/></c:set>

	<c:if test="${asmSession == true}">
		<div id="registerAccountTypebox"><formElement:formSelectBox idKey="registerAccountType" labelKey="register.accountType"
																	path="accountType" mandatory="true" skipBlank="false"
																	skipBlankMessageKey="text.account.select.accountType"
																	items="${accountTypes}" itemValue="code" itemLabel="name"
																	tabindex="10"
																	selectCSSClass="form-control"/></div>
		<div id="registerFirstNamebox"><formElement:formInputBox idKey="registerFirstName"
																 labelKey="register.firstName" path="firstName"
																 inputCSS="form-control"
																 mandatory="true"/></div>

		<div id="registerLastNamebox"><formElement:formInputBox idKey="registerLastName"
																labelKey="register.lastName" path="lastName"
																inputCSS="form-control"
																mandatory="true"/></div>

		<div id="registerMobileNumber"><formElement:formInputBox idKey="registerPhone" labelKey="register.phone"
																 path="phoneNo" inputCSS="form-control" mandatory="true"/></div>

		<div id="registerCountryBox"><formElement:formSelectBox idKey="registerCountry" labelKey="register.country"
																path="country" mandatory="true" skipBlank="false"
																skipBlankMessageKey="register.country.dropdown.message"
																items="${countries}" itemValue="isocode" tabindex="10"
																selectCSSClass="form-control billing-address-country"/></div>

                    <div id="registerRegionBox">
                        <formElement:formSelectBox idKey="name" labelKey="profile.address.region" path="province" items="${regions}"
                                                   itemValue="isocode" selectCSSClass="billing-address-regions form-control" skipBlank="false"
                                                   skipBlankMessageKey="text.page.default.select" mandatory="true"/>
                    </div>

		<div id="registerZipcodeBox">
			<formElement:formInputBox idKey="registerZipCode"
									  labelKey="register.zipCode" path="zipCode" inputCSS="form-control"
									  mandatory="true"/></div>

		<c:if test="${not empty userGroups}">
			<div id="registerUserGroupBox">
				<formElement:formSelectBox idKey="registerUserGroupType" labelKey="register.asm.usergroup"
										   path="userGroup" mandatory="true" skipBlank="false"
										   skipBlankMessageKey="text.account.select.userGroup"
										   items="${userGroups}" itemValue="uid" itemLabel="name[${locale}]"
										   selectCSSClass="form-control"/>
			</div>
		</c:if>

	</c:if>
	<div id="registerEmailbox"><formElement:formInputBox idKey="register.email" labelKey="register.email"
														 path="email" inputCSS="form-control form-email" mandatory="true"/></div>

	<div id="registerEmailbox"><formElement:formInputBox idKey="register.confirm.email" labelKey="register.checkEmail"
														 path="checkEmail" inputCSS="form-control form-confirm-email"
														 mandatory="true"/></div>

	<c:if test="${asmSession == false}">
		<formElement:formPasswordBox idKey="password" labelKey="register.pwd.create"
									 path="pwd" inputCSS="form-control password-strength form-password" mandatory="true"/>

		<formElement:formPasswordBox idKey="register.checkPwd"
									 labelKey="register.checkPwd" path="checkPwd" inputCSS="form-control form-checkPwd"
									 mandatory="true"/>
	</c:if>
	<input type="hidden" id="recaptchaChallangeAnswered"
		   value="${requestScope.recaptchaChallangeAnswered}"/>
	<div class="form_field-elements control-group js-recaptcha-captchaaddon"></div>
	<div class="form-actions clearfix">
		<ycommerce:testId code="register_Register_button">
			<button type="submit" class="btn btn-primary btn-block js-registerCustomer">
				<spring:theme code='${actionNameKey}' />
			</button>
		</ycommerce:testId>
	</div>
</form:form>
<p class="register-privacy"><spring:theme code="register.privacy.statement"/>
	<a href="${pageContext.request.contextPath}/privacy-statement"><spring:theme code="register.privacy.statement.link.text"
																				 text="Privacy Statement."/></a></p>

