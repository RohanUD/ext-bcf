<%@ tag body-content="empty" trimDirectiveWhitespaces="true" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="formElement" tagdir="/WEB-INF/tags/responsive/formElement" %>
<%@ taglib prefix="theme" tagdir="/WEB-INF/tags/shared/theme" %>
<%@ taglib prefix="ycommerce" uri="http://hybris.com/tld/ycommercetags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<spring:htmlEscape defaultHtmlEscape="false" />
<c:url var="loginPageUrl" value="/login"/>

<div class="forgotten-password1">
	<div class="row mb-4">
		<div class="col-lg-12 col-md-12 col-sm-8 col-xs-12">
			<p class="description fnt-14"><spring:theme code="forgottenPwd.description"/></p>
		</div>
	</div>

	<form:form method="post" commandName="forgottenPwdForm">
		<div class="row">
			<div class="col-lg-7 col-md-7 col-sm-7 col-xs-12 p-relative col-md-offset-3">
				<spring:theme code="forgottenPwd.email.placeholder" var="forgotEmailPlaceholder" />
				<ycommerce:testId code="login_forgotPasswordEmail_input">
					<formElement:formInputBox idKey="forgottenPwd.email" labelKey="forgottenPwd.email" path="email" mandatory="true" placeholder="${forgotEmailPlaceholder}"/>
				</ycommerce:testId>
				<div class="help-msg mb-3 ">
					<a href="#"> <spring:theme code="forgotPassword.help.label" text="Help"/> </a>
				</div>
			</div>
		</div>

		<div class="row">
			<div class="col-lg-5 col-md-5 col-sm-5 col-xs-12 col-md-offset-4 margin-top-20">
				<ycommerce:testId code="login_forgotPasswordSubmit_button">
					<button class="btn btn-primary btn-block" type="submit">
						<spring:theme code="forgottenPwd.title"/>
					</button>
				</ycommerce:testId>
			</div>
		</div>
		<div class="row">
			<div class="col-lg-5 col-md-5 col-sm-5 col-xs-12 col-md-offset-4 margin-top-20">
				<a href="${loginPageUrl}" class="btn btn-outline-primary btn-block">
					<spring:theme code="forgottenPwd.cancel"/>
				</a>
			</div>
		</div>
	</form:form>
</div>
<div class="row">
	<div class="col-lg-12 col-md-12 col-sm-8 col-xs-12 margin-top-40">
		<p class="text-sm privacy-statement"><spring:theme code="register.privacy.statement"/>&nbsp;
			<a href="${pageContext.request.contextPath}/privacy-statement"><spring:theme code="register.privacy.statement.link.text" text="Privacy Statement."/></a>
		</p>
	</div>
</div>
