<%@ tag language="java" pageEncoding="UTF-8"%>
<%@ tag body-content="empty" trimDirectiveWhitespaces="true"%>
<%@ taglib prefix="terminal" tagdir="/WEB-INF/tags/addons/bcfstorefrontaddon/responsive/terminallisting"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ attribute name="transportfacilities" required="true" type="java.util.List"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="format" tagdir="/WEB-INF/tags/shared/format"%>
<%@ taglib prefix="ycommerce" uri="http://hybris.com/tld/ycommercetags"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>

<c:if test="${fn:length(results) gt 0}">
   <c:forEach var="transportFacility" items="${results}"
      varStatus="Count">
      <terminal:terminal transportFacility="${transportFacility}" />
   </c:forEach>
   <c:if test = "${Count.count%3 eq 0}">
      </div>
      <div class="row">
   </c:if>
</c:if>
