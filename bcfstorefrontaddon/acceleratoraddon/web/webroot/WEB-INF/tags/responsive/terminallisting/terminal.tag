<%@ tag language="java" pageEncoding="UTF-8"%>
<%@ tag body-content="empty" trimDirectiveWhitespaces="true"%>
<%@ taglib prefix="article" tagdir="/WEB-INF/tags/addons/bcfstorefrontaddon/responsive/article"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ attribute name="transportFacility" required="true" type="de.hybris.platform.commercefacades.travel.TransportFacilityData"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="format" tagdir="/WEB-INF/tags/shared/format"%>
<%@ taglib prefix="ycommerce" uri="http://hybris.com/tld/ycommercetags"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<c:url var="conditionUrl" value="/majorTerminals?terminalCode="/>
<div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
    <hr class="dark-border"/>
    <p class="text-light text-uppercase mb-1">
        <spring:theme code="label.our.terminals.terminal"
                      text="TERMINAL"/>
    </p>
    <p><strong>${transportFacility.name}</strong></p>
    <hr class="light-border"/>
    <p class="text-light text-uppercase mb-1">
        <spring:theme code="label.our.terminals.location"
                      text="LOCATION"/>
        <br/>
    </p>
    <c:if
            test="${not empty transportFacility.pointOfServiceData && not empty transportFacility.pointOfServiceData.address}">
        <p>${transportFacility.pointOfServiceData.address.formattedAddress}</p>
    </c:if>
    <p class="text-light text-uppercase mb-1">
        <br/>
        <spring:theme code="label.our.terminals.terminal.pages"
                      text="TERMINAL PAGES"/>
    </p>
    <c:if test="${transportFacility.currentConditionEnabled eq true}">
        <a href="${conditionUrl}${transportFacility.code}">
            <spring:theme
                    code="label.our.terminals.view.conditions"
                    text="View terminal conditions"/>
        </a>
        <br />
    </c:if>

    <c:url var="terminalDetailsPageUrl" value="/travel-boarding/terminal-directions-parking-food"/>
    <a href="${terminalDetailsPageUrl}/${transportFacility.routeName}/${transportFacility.code}">
        <spring:theme code="label.our.terminals.view.details"
                      text="View terminal details"/>
    </a>
</div>
