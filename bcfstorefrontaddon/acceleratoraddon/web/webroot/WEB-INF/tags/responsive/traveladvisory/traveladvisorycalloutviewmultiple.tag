<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>

<div class="travel-advisories bg-transparent">
   <div class="container">
      <div class="row">
         <div class="col-lg-12">
            <div class="alert" style="display: none;">
               <div class="alert-header">
                  <span class="bcf bcf-icon-alert alert-icon-top pull-left"></span>
                  <p class="pull-left alert-header-text">
                     <strong>
                        <spring:theme code="text.travel.advisory.ribbon.header" text="Travel Advisory:" />
                     </strong>
                     <span>
                        <spring:theme code="text.travel.advisory.multiple.advisories" text="Multiple advisories in effect, please check before you travel" />
                     </span>
                  </p>
               </div>
               <a href="javascript:void(0)" class="viewTravelAdvisories">
                  <spring:theme code="text.travel.advisory.ribbon.hide" text="Hide" />
               </a>
               <a href="#" class="travel-advisories-close">&times;</a>
               <div class="detailTravelAdvisories">
                  <ol>
                     <c:forEach items="${traveladvisories}" var="traveladvisory">
                        <li>
                           <strong>${traveladvisory.advisoryTitle}</strong> -
                           <a href="/travel-advisories/${traveladvisory.code}">
                              <spring:theme code="text.travel.advisory.click.here.for.more" text="click here for more" />
                           </a>
                        </li>
                     </c:forEach>
                  </ol>
                  <p>
                     <spring:theme code="text.travel.advisory.see.more" text="To see more Travel Advisories please" />
                     <a href="/travel-advisories">
                        <spring:theme code="text.travel.advisory.click.here" text="Click here" />
                     </a>
                  </p>
               </div>
            </div>
            <div class="travel-advisories-reset" style="display: block;">
               <strong><span class="bcf bcf-icon-alert"></span></strong>
            </div>
         </div>
      </div>
   </div>
</div>
