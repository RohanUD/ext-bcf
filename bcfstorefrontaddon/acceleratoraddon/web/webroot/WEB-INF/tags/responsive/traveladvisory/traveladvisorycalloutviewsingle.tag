<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>

<div class="travel-advisories bg-transparent">
   <div class="container">
      <div class="row">
         <div class="col-lg-12">
            <div class="alert" style="display: none;">
               <span class="bcf bcf-icon-alert alert-icon-top"></span>
               <strong>
                  <spring:theme code="text.travel.advisory.ribbon.header" text="Travel Advisory:" />
               </strong>
               <span>${traveladvisories[0].advisoryTitle}</span>
               <a href="javascript:void(0)" class="viewTravelAdvisories">
                  <spring:theme code="text.travel.advisory.ribbon.view" text="View" />
               </a>
               <a href="#" class="travel-advisories-close">&times;</a>
               <div class="detailTravelAdvisories">
                  <p>${traveladvisories[0].advisoryDescription}</p>
                  <p>
                     <spring:theme code="text.travel.advisory.see.more" text="To see more Travel Advisories please" />
                     <a href="/travel-advisories">
                        <spring:theme code="text.travel.advisory.click.here" text="Click here" />
                     </a>
                  </p>
               </div>
            </div>
            <div class="travel-advisories-reset" style="display: block;">
               <strong><span class="bcf bcf-icon-alert"></span></strong>
            </div>
         </div>
      </div>
   </div>
</div>
