<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>

<spring:htmlEscape defaultHtmlEscape="false" />
<a href="javascript:;" data-toggle="modal" data-target="#emailItineraryModal">
    <i class="fas fa-envelope"></i> <spring:theme code="text.manage.account.my.bookings.email.itinerary" text="Email itinerary" />
</a>


<div class="modal fade" tabindex="-1" role="dialog" id="emailItineraryModal">
   <div class="modal-dialog" role="document">
      <div class="modal-content">
         <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true"><i class="bcf bcf-icon-cancel-solid"></i></span>
            </button>
            <h4 class="modal-title">
               <spring:theme code="text.page.managemybooking.bookingdetails.email.itinerary.pop.up.title" />
            </h4>
         </div>
         <div class="modal-body">
            <h4>
               <strong>
                  <spring:theme code="label.booking.confirmation.share.itinerary.header" text="Share Itinerary" />
               </strong>
            </h4>
            <p>
               <spring:theme code="text.booking.confirmation.share.itinerary.info" text="Separate multiple email addresses using comma." />
            </p>
            <label>
               <spring:theme code="label.booking.confirmation.share.itinerary.textbox" text="Email" />
            </label>
            <spring:theme var="emailPlaceholder" code="text.booking.confirmation.share.itinerary.textbox.placeholder" text="Recepient email address" />
            <form id="form-share-itinerary-email">
               <input id="share-itinerary-email" name="share-itinerary-email" placeholder="${emailPlaceholder}" type="text" class="form-control email-field mb-3" value="" />
               <button type="submit" class="btn btn-outline-blue btn-block form-control">
                  <spring:theme
                     code="text.booking.confirmation.share.itinerary.button" text="SHARE ITINERARY" />
               </button>
            </form>
            <div>
                <p class="medium-grey-text mt-0 mb-0 fnt-14 "><span><spring:theme code="label.booking.confirmation.share.itinerary.info.text1"/>&nbsp; </span></p>
                <span><br></span><p class="medium-grey-text mt-0 mb-0 fnt-14"><span><spring:theme code="label.booking.confirmation.share.itinerary.info.text2"/></span></p>
            </div>
         </div>
      </div>
   </div>
</div>
