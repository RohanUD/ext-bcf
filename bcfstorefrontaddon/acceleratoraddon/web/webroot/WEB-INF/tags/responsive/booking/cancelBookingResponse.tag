<%@ tag body-content="empty" trimDirectiveWhitespaces="true"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<spring:htmlEscape defaultHtmlEscape="false" />
<c:if test="${not empty cancelBookingFailureSailings}">
	<div class="modal fade in" id="y_cartUpdatedModal" tabindex="-1" role="dialog" aria-labelledby="cartUpdatedModal">
		<div class="modal-dialog" role="document">
			<div class="modal-content">
				<div class="modal-header clearfix">
					<h4 class="modal-title" id="cartUpdatedModal">
						<spring:theme code="text.page.cartdeletion.sailings.title" />
					</h4>
				</div>
				<div class="modal-body">
					<spring:theme code="text.page.cartdeletion.sailings.failure.message" />
					<br>
					<c:forEach items="${cancelBookingFailureSailings}" var="cancelBookingFailureSailing" varStatus="cancelBookingFailureSailingIdx">
						<div>
							<c:if test="${not empty cancelBookingFailureSailing.travelRoute}">
								<spring:theme code="text.page.cartdeletion.sailings.failure.body.message" arguments="${cancelBookingFailureSailingIdx.index+1},${cancelBookingFailureSailing.travelRoute.origin.name},${cancelBookingFailureSailing.travelRoute.destination.name}" />
							</c:if>
							<spring:theme code="cancelbooking.errorcode.${cancelBookingFailureSailing.errorCode}" />
						</div>
					</c:forEach>
				</div>
			</div>
		</div>
	</div>
</c:if>
