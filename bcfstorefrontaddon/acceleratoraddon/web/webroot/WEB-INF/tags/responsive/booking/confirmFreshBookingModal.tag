<%@ tag body-content="empty" trimDirectiveWhitespaces="true"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<spring:htmlEscape defaultHtmlEscape="false" />
<div class="modal fade" id="y_confirmFreshBookingModal" tabindex="-1" role="dialog" aria-labelledby="y_confirmFreshBookingModal">
	<div class="modal-dialog" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true"><i class="bcf bcf-icon-cancel-solid"></i></span>
				</button>
				<h3 class="modal-title" id="confirmFreshBookingLabel">
					<spring:theme code="text.page.managemybooking.confirm.freshbooking.modal.title" text="Confirm Fresh Booking" />
				</h3>
			</div>
			<div class="modal-body">
				
				<h3>
					<spring:theme code="text.page.managemybooking.freshbooking.confirmation.modal.subtitle" />
				</h3>
				<p>
					<spring:theme code="text.page.managemybooking.freshbooking.modal.statement"   />
				</p>
				
			</div>
			<div class="modal-footer">
				<div class="row">
					<div class="col-xs-12 col-sm-6">
						<button id="y_freshBooking" class="btn btn-primary btn-block" >
							<spring:theme code="text.page.managemybooking.fresh.booking.confirmation" text="Confirm" />
						</button>
					</div>
					<div class="col-xs-12 col-sm-6">
						<button class="btn btn-secondary btn-block" data-dismiss="modal">
							<spring:theme code="text.page.managemybooking.cancel.booking.close" text="No Thanks" />
						</button>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
