<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="template" tagdir="/WEB-INF/tags/responsive/common"%>
<%@ taglib prefix="cms" uri="http://hybris.com/tld/cmstags"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="formElement" tagdir="/WEB-INF/tags/responsive/formElement"%>
<%@ taglib prefix="ycommerce" uri="http://hybris.com/tld/ycommercetags"%>

<div class="account-section-content">
    <div id="myProfileDetailsPanel">

        <div class="col-md-10 col-sm-10 col-xs-10 p-0">
            <div class="account-section-form margin-bottom-30 fnt-14" >
                <div class="account-profile-detailssection">
                    <p class="margin-bottom-20">${customerData.titleCode} ${customerData.firstName} ${customerData.lastName}</p>
                    <p class="margin-bottom-20">${customerData.contactNumber}</p>
                    <div class="address-data ">
                        <c:set var="addressData" value="${customerData.defaultAddress}"/>
                        <c:if test="${not empty addressData}">
                            <p>${addressData.line1}</p>
                            <p>${addressData.town}</p>
                            <c:if test="${not empty addressData.region}">
                                <p>${addressData.region.name}</p>
                            </c:if>
                            <c:if test="${not empty addressData.country}">
                                <p>${addressData.country.name}</p>
                            </c:if>
                            <p>${addressData.postalCode}</p>
                        </c:if>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-2 col-sm-2 col-xs-2 text-right p-0">
            <i id="editProfileBtn" class="bcf bcf-icon-edit"></i>
        </div>
    </div>

</div>

<div class="account-section-content">
    <c:set var="profileUpdateInlineStyle">display:none</c:set>
    <c:if test="${param.expand eq 'updateProfile'}">
        <c:set var="profileUpdateInlineStyle">display:block</c:set>
    </c:if>
    <div class="account-section-form" id="myProfileUpdateEditPanel" style="${profileUpdateInlineStyle}">
        <form:form action="update-profile" method="post" commandName="bcfUpdateProfileForm">
            <div class="row">
                <div class="col-md-8 col-sm-12 col-xs-12 col-md-offset-2">

                    <form:hidden path="accountType"/>

                    <formElement:formInputBox idKey="profile.firstName" labelKey="profile.firstName" path="firstName" inputCSS="text" mandatory="true"/>
                    <formElement:formInputBox idKey="profile.lastName" labelKey="profile.lastName" path="lastName" inputCSS="text" mandatory="true"/><br>
                    <formElement:formInputBox idKey="profile.phoneNumber" labelKey="profile.phoneNumber" path="phoneNumber" inputCSS="text" mandatory="true" /><br>
                    <formElement:formInputBox idKey="profile.address" labelKey="profile.address" path="address" inputCSS="text"/>
                    <formElement:formInputBox idKey="profile.address.city" labelKey="profile.address.city" path="city"
                                              inputCSS="text"/>

                    <formElement:formSelectBox idKey="registerCountry" labelKey="profile.address.country"
                                               path="country" mandatory="true" skipBlank="false"
                                               skipBlankMessageKey="profile.address.country.dropdown.message"
                                               items="${countries}" itemValue="isocode" tabindex="10"
                                               selectCSSClass="form-control" />

                    <div id="registerRegionBox">
                        <formElement:formSelectBox idKey="name" labelKey="profile.address.region" path="region" items="${regions}"
                                                   itemValue="isocode" selectCSSClass="billing-address-regions form-control"
                                                   skipBlankMessageKey="text.page.default.select" mandatory="true"/>
                    </div>

                    <formElement:formInputBox idKey="profile.address.zipCode" labelKey="profile.address.zipCode" path="zipCode" inputCSS="form-control" mandatory="true" />

                    <div class="row margin-top-40">
                        <div class="col-md-8 col-md-offset-2 mt-3">
                            <div class="accountActions">
                                <ycommerce:testId code="personalDetails_savePersonalDetails_button">
                                    <button type="submit" class="btn btn-primary btn-block">
                                        <spring:theme code="text.account.profile.saveUpdates" text="Save Updates"/>
                                    </button>
                                </ycommerce:testId>
                            </div>
                        </div>
                        <div class="col-md-8 col-md-offset-2 mt-3 margin-top-30">
                            <div class="accountActions">
                                <ycommerce:testId code="personalDetails_cancelPersonalDetails_button">
                                    <button type="button" class="btn btn-outline-primary btn-block backToSamePage">
                                        <spring:theme code="text.account.profile.cancel" text="Cancel"/>
                                    </button>
                                </ycommerce:testId>
                            </div>
                        </div>
                    </div>
                </div>

            </div>
        </form:form>
    </div>
</div>
