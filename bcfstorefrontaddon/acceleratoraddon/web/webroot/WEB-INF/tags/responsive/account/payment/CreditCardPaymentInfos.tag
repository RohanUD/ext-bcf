<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="template" tagdir="/WEB-INF/tags/responsive/common" %>
<%@ taglib prefix="cms" uri="http://hybris.com/tld/cmstags" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="formElement" tagdir="/WEB-INF/tags/responsive/formElement" %>
<%@ taglib prefix="ycommerce" uri="http://hybris.com/tld/ycommercetags" %>
<%@ taglib prefix="address" tagdir="/WEB-INF/tags/responsive/order" %>
<%@ taglib prefix="payment" tagdir="/WEB-INF/tags/addons/bcfstorefrontaddon/responsive/account/payment" %>
<%@ attribute name="editedFormBean" type="com.bcf.bcfcheckoutaddon.forms.UpdatePaymentCardForm" required="false" %>

<c:url var="deletePaymentCardUrl" value="./payment-cards/remove/"/>
<c:url var="updatePaymentUrl" value="./payment-cards/update"/>
<%@ attribute name="savedCreditCards" required="true" type="java.util.List<de.hybris.platform.commercefacades.order.data.CCPaymentInfoData>" %>
<c:forEach items="${savedCreditCards}" var="paymentCard" varStatus="counter">
    <div class="account-profile-accordion">
        <h4 id="myProfileDetailsSection" class="py-5 px-0 mb-0">
            <spring:theme code="text.account.saved.payment.cards.ends.in"
                          arguments="${paymentCard.cardType},${paymentCard.cardNumber}"/> <br/>
            <span class="ui-accordion-header-icon ui-icon bcf bcf-bcf bcf-icon-up-arrow"></span>
            <span class="custom-arrow"></span>
        </h4>

        <div>
            <div class="payment-sec clearfix mb-4 pt-4 fnt-14">
                <div class="col-md-10 col-xs-10 col-sm-10 p-0">
                    <div>
                        <p class="margin-bottom-0"><strong><spring:theme code="text.account.credit.card.expiresOn"/></strong></p>
                        <p class="margin-bottom-0">${paymentCard.expiryMonth}/${paymentCard.expiryYear}</p>
                    </div>
                </div>
                <div class="col-md-2 col-sm-2 col-xs-2 text-right p-0">
                    <i id="updatePaymentBtn" data-paymenteditnumber="${counter.count}"
                       class="bcf bcf-icon-edit updatePaymentBtn"></i>
                </div>

                <div class="account-section-content">
                    <div id="myProfileDetailsPanel">
                        <div class="col-md-10 col-sm-10 col-xs-10 p-0">
                            <div class="margin-top-30 margin-bottom-30">
                                <p class="margin-bottom-0"><strong><spring:theme code="text.account.credit.card.nameOnCard"
                                                                                 text="Name on card"/></strong></p>
                                <p class="margin-bottom-0">${paymentCard.accountHolderName}</p>
                            </div>
                            <div class="margin-bottom-30">
                                <p class="margin-bottom-0"><strong><spring:theme code="text.account.credit.card.billingAddress"
                                                                                 text="Billing Address"/></strong></p>
                                <div id="payment-card-details${counter.count}">
                                    <address:addressItem address="${paymentCard.billingAddress}"/>

                                </div>
                            </div>
                            <div>


                            </div>


                        </div>


                        <div class="row">
                            <div class="col-md-6 col-md-offset-3">
                                <button class="btn btn-outline-primary btn-block js-open-cancel-card-modal" data-toggle="modal"
                                        data-target="#removePaymentCardModal" data-id="${deletePaymentCardUrl}${paymentCard.id}">
                                    <spring:theme code='text.account.credit.card.removeCard'/>
                                </button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <c:set var="updatePaymentCardString" value="updatePaymentCard${counter.count}"/>
    <c:set var="creditCardUpdateInlineStyle" value="display:none"/>
    <c:if test="${expand eq updatePaymentCardString}">
        <c:set var="creditCardUpdateInlineStyle" value="display:block"/>
    </c:if>

    <div class="updatePaymentCardSection" id="updatePaymentCardSection${counter.count}" style="${creditCardUpdateInlineStyle}">
        <c:choose>
            <c:when test="${not empty editedFormBean}">
                <payment:updatePaymentCard updateFormNumber="${counter.count}"
                                           regions="${cc_regionsByCountry[editedFormBean.country]}"/>
            </c:when>
            <c:otherwise>
                <payment:updatePaymentCard updateFormNumber="${counter.count}"
                                           regions="${cc_regionsByCountry[paymentCard.billingAddress.country.isocode]}"/>
            </c:otherwise>
        </c:choose>
    </div>
</c:forEach>
<payment:removeCardModal />
