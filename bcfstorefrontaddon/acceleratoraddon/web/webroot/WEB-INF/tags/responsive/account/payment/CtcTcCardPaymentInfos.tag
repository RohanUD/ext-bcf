<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="template" tagdir="/WEB-INF/tags/responsive/common" %>
<%@ taglib prefix="cms" uri="http://hybris.com/tld/cmstags" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="formElement" tagdir="/WEB-INF/tags/responsive/formElement" %>
<%@ taglib prefix="address" tagdir="/WEB-INF/tags/responsive/order" %>
<%@ taglib prefix="payment" tagdir="/WEB-INF/tags/addons/bcfstorefrontaddon/responsive/account/payment" %>
<%@ taglib prefix="ycommerce" uri="http://hybris.com/tld/ycommercetags" %>

<c:url var="updateCtcTcCardPaymentUrl" value="./payment-cards/update"/>
<c:url var="addCtcTcCardPaymentUrl" value="./payment-cards/add"/>
<c:url var="deleteCtcTcCardPaymentCardUrl" value="./payment-cards/remove"/>

<%@ attribute name="savedCtcTcCards" required="true" type="java.util.List<com.bcf.facades.payment.CTCTCCardData>" %>

<c:forEach items="${savedCtcTcCards}" var="paymentCard" varStatus="counter">
    <input type="hidden" name="cardType" value="CTCTC">
    <div class="ctc-custom-header" id="accountPayment-card-details${counter.count}">
        <h4 id="myProfileDetailsSection" class="py-5 px-0 mb-0">
            <spring:theme code="text.account.saved.payment.cards.ends.in" arguments="${paymentCard.cardType},${paymentCard.displayCardNumber}"/> <br/>
        </h4>
        <a href="${deleteCtcTcCardPaymentCardUrl}/${paymentCard.code}?cardType=CTCTCCard">
            <span class="glyphicon glyphicon-trash delete-icon"></span>
        </a>
    </div>

    <c:set var="updateCtcTcPaymentCardString" value="updateCtcTcPaymentCard${counter.count}"/>
    <c:set var="accountCardUpdateInlineStyle" value="display:none"/>
    <c:if test="${expand eq updateCtcTcPaymentCardString}">
        <c:set var="accountCardUpdateInlineStyle" value="display:block"/>
    </c:if>

    <div class="updateAccountPaymentCardSection" id="updateAccountPaymentCardSection${counter.count}">
        
    </div>
</c:forEach>
