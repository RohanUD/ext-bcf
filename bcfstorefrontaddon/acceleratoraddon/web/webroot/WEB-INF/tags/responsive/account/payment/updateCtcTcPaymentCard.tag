<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="template" tagdir="/WEB-INF/tags/responsive/common" %>
<%@ taglib prefix="cms" uri="http://hybris.com/tld/cmstags" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="formElement" tagdir="/WEB-INF/tags/responsive/formElement" %>
<%@ taglib prefix="ycommerce" uri="http://hybris.com/tld/ycommercetags" %>
<%@ attribute name="updateFormNumber" type="java.lang.String" required="true" %>
<c:url var="updatePaymentUrl" value="./payment-cards/updateCTCCard"/>

<form:form action="${updatePaymentUrl}" method="post" modelAttribute="accountUpdatePaymentCardForm${updateFormNumber}">
    <div class="row">
    <%--Address--%>
    <div class="row">
        <b><spring:theme code="text.account.credit.card.billingAddress"/> </b>
        <form:hidden path="code"/>
        <form:hidden path="cardType"/>
        <div class="form-group col-sm-3">
            <formElement:formInputBox idKey="address.line1" labelKey="text.address.label" path="addressLine1" inputCSS="text"
                                      mandatory="true" tabindex="${tabindex + 6}"/>
        </div>
        <div class="form-group col-sm-3">
            <formElement:formInputBox idKey="address.townCity" labelKey="address.townCity" path="city" inputCSS="text"
                                      mandatory="true" tabindex="${tabindex + 4}"/>
        </div>

        <div class="form-group col-sm-3">
            <formElement:formSelectBox idKey="registerCountry" labelKey="register.country"
                                       path="country" mandatory="true" skipBlank="false"
                                       skipBlankMessageKey="register.country.dropdown.message"
                                       items="${countries}" itemValue="isocode" tabindex="10"
                                       selectCSSClass="form-control custom-select"/>
        </div>

        <div class="form-group col-sm-3">
            <div class="form-group">
                <label class="control-label " for="ChooseRegion">
                    <spring:theme code="profile.address.region" text="Choose Region"/>
                </label>
                <form:select class="billing-address-regions form-control" id="billing-address-regions" path="province"
                             cssErrorClass="fieldError">
                    <form:option value="-1" disabled="true" selected="selected">
                        <spring:theme code="text.page.default.select" text="Please Select"/>
                    </form:option>
                    <form:options items="${regions}" itemLabel="name" itemValue="isocode"/>
                </form:select>
            </div>
        </div>


        <div class="form-group col-sm-3">
            <formElement:formInputBox idKey="address.postcode" labelKey="address.postcode" path="zipCode" inputCSS="text"
                                      mandatory="true" tabindex="${tabindex + 2}"/>
        </div>
    </div>

    <div class="row">
        <div class="form-group col-sm-3">
            <button type="submit" class="btn btn-primary btn-block">
                <spring:theme code='text.save.label'/>
            </button>
        </div>
        <div class="form-group col-sm-3">
            <button class="btn btn-primary btn-block">
                <spring:theme code='text.cancel.label'/>
            </button>
        </div>
    </div>
</form:form>
