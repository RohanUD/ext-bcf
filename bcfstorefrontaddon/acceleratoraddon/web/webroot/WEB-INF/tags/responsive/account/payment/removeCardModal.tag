<%@ tag body-content="empty" trimDirectiveWhitespaces="true"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<spring:htmlEscape defaultHtmlEscape="false" />
<div class="modal" id="removePaymentCardModal" tabindex="-1" role="dialog" aria-labelledby="removePaymentCardModal">
	<div class="modal-dialog" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true"><i class="bcf bcf-icon-cancel-solid"></i></span>
				</button>
				<h3 class="modal-title" id="removePaymentCardLabel">
					<spring:theme code="text.account.credit.card.removeCard.modal.title" text="Remove Payment Card"/>
				</h3>
			</div>
			<div class="modal-body">
				<spring:theme code="text.account.credit.card.removeCard.modal.body"/>
			</div>
			<div class="modal-footer">
				<div class="row">
					<div class="col-xs-12 col-sm-6">
                        <a href="#" id="remove-card-url">
                            <button id="y_proceedRemoveCard" class="btn btn-primary btn-block backToHome">
                                <spring:theme code="text.company.disable.confirm.yes" text="Yes" />
                            </button>
						</a>
					</div>
					<div class="col-xs-12 col-sm-6">
						<button class="btn btn-secondary btn-block" data-dismiss="modal">
							<spring:theme code="text.company.disable.confirm.no" text="No" />
						</button>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
