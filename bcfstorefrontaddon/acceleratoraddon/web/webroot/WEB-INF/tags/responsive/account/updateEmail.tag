<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="template" tagdir="/WEB-INF/tags/responsive/common"%>
<%@ taglib prefix="cms" uri="http://hybris.com/tld/cmstags"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="formElement" tagdir="/WEB-INF/tags/responsive/formElement"%>
<%@ taglib prefix="ycommerce" uri="http://hybris.com/tld/ycommercetags"%>
<spring:url value="update-email" var="changeEmailUrl"/>

<c:set var="updateEmailInlineStyle">display:none</c:set>
<c:if test="${param.expand eq 'updateEmail'}">
    <c:set var="updateEmailInlineStyle">display:block</c:set>
</c:if>
<div id="updateEmailPanel" style="${updateEmailInlineStyle}">
    <div >
        <div class="account-section-content">
            <div class="account-section-form" >
                <form:form action="${changeEmailUrl}" method="post" commandName="updateEmailForm">
                <div class="row margin-top-40">
  <div class="col-md-8 col-sm-12 col-xs-12 col-md-offset-2">
                    <formElement:formInputBox idKey="profile.email" labelKey="profile.email" path="email" inputCSS="text" mandatory="true"/>
                    <formElement:formInputBox idKey="profile.checkEmail"  labelKey="profile.checkEmail" path="chkEmail" inputCSS="text" mandatory="true"/>

                    <div class="row margin-top-40">
                       <div class="col-md-8 col-md-offset-2 mt-3">
                            <div class="accountActions">
                                <ycommerce:testId code="personalDetails_savePersonalDetails_button">
                                    <button type="submit" class="btn btn-primary btn-block">
                                        <spring:theme code="text.account.profile.saveUpdates" text="Save Updates"/>
                                    </button>
                                </ycommerce:testId>
                            </div>
                        </div>
                         <div class="col-md-8 col-md-offset-2 mt-3 margin-top-30">
                            <div class="accountActions">
                                <ycommerce:testId code="personalDetails_cancelPersonalDetails_button">
                                    <button type="button" class="btn btn-outline-primary btn-block backToSamePage">
                                        <spring:theme code="text.account.profile.cancel" text="Cancel"/>
                                    </button>
                                </ycommerce:testId>
                            </div>
                        </div>
                    </div>
                        </div>
                    </div>
                </form:form>
            </div>
        </div>
    </div>
</div>
