<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="template" tagdir="/WEB-INF/tags/responsive/common"%>
<%@ taglib prefix="cms" uri="http://hybris.com/tld/cmstags"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="formElement" tagdir="/WEB-INF/tags/responsive/formElement"%>
<%@ taglib prefix="ycommerce" uri="http://hybris.com/tld/ycommercetags"%>
<spring:url value="update-password" var="changePasswordUrl"/>

<div class="row">
    <div class="col-xs-12">
        <div id="changePasswordEditPanel">
            <p class="mb-5"><b><spring:theme code="changePassword.label" text="Change password"/></b></p>
            <p class="mb-5"><spring:theme code="changePassword.desc.label"/></p>
            <div class="row mt-5 margin-bottom-30">
                <div class="col-md-8 col-sm-12 col-xs-12 col-md-offset-2">
                    <div class="saving-tip-bx mx-3">
                        <p class="font-italic change-password-instruction"><spring:theme code="changePassword.instruction.label"/></p>
                    </div>
                </div>
            </div>

            <div class="row mt-5">
                <div class="col-md-8 col-sm-12 col-xs-12 col-md-offset-2">
                    <div class="account-section-content">
                        <div class="account-section-form">
                            <form:form action="${changePasswordUrl}" method="post" commandName="updatePasswordForm">

                                <formElement:formPasswordBox idKey="currentPassword"
                                                             labelKey="profile.currentPassword" path="currentPassword"
                                                             inputCSS="form-control"
                                                             mandatory="true"/>
                                <formElement:formPasswordBox idKey="newPassword"
                                                             labelKey="profile.newPassword" path="pwd"
                                                             inputCSS="form-control"
                                                             mandatory="true"/>
                                <formElement:formPasswordBox idKey="checkNewPassword"
                                                             labelKey="profile.checkNewPassword" path="checkPwd"
                                                             inputCSS="form-control"
                                                             mandatory="true"/>

                                <div class="row margin-top-40">
                                    <div class="col-md-8 col-md-offset-2 mt-3">
                                        <div class="accountActions">
                                            <button type="submit" class="btn btn-primary btn-block">
                                                <spring:theme code="updatePwd.submit" text="Update Password"/>
                                            </button>
                                        </div>
                                    </div>
                                    <div class="col-md-8 col-md-offset-2 mt-3 margin-top-30">
                                        <div class="accountActions">
                                            <button type="button" class="btn btn-outline-primary btn-block backToHome">
                                                <spring:theme code="text.button.cancel" text="Cancel"/>
                                            </button>
                                        </div>
                                    </div>
                                </div>
                            </form:form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
