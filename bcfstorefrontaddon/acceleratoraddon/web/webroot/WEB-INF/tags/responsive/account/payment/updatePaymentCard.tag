<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="template" tagdir="/WEB-INF/tags/responsive/common" %>
<%@ taglib prefix="cms" uri="http://hybris.com/tld/cmstags" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="formElement" tagdir="/WEB-INF/tags/responsive/formElement" %>
<%@ taglib prefix="ycommerce" uri="http://hybris.com/tld/ycommercetags" %>
<%@ attribute name="updateFormNumber" type="java.lang.String" required="true" %>
<%@ attribute name="regions" type="java.util.List" required="true" %>
<c:url var="updatePaymentUrl" value="./payment-cards/update"/>

<form:form action="${updatePaymentUrl}" method="post" modelAttribute="updatePaymentCardForm${updateFormNumber}">
    <%--Address--%>
    <div class="row">
        <div class="col-xs-12"><b><spring:theme code="text.account.credit.card.billingAddress"/> </b></div>

        <div class="col-md-8 col-sm-12 col-xs-12 col-md-offset-2">
            <form:hidden path="code"/>
            <div class="form-group">
                <formElement:formInputBox idKey="address.line1" labelKey="text.address.label" path="addressLine1" inputCSS="text"
                                          tabindex="1"/>
            </div>
            <div class="form-group">
                <formElement:formInputBox idKey="address.townCity" labelKey="address.townCity" path="city" inputCSS="text"
                                          tabindex="2"/>
            </div>

            <div class="form-group">
                <formElement:formSelectBox idKey="myAccountPaymentRegisterCountry_${updateFormNumber}" labelKey="register.country"
                                           path="country" mandatory="true" skipBlank="false"
                                           skipBlankMessageKey="register.country.dropdown.message"
                                           items="${countries}" itemValue="isocode" tabindex="3"
                                           selectCSSClass="form-control myAccountPaymentRegisterCountry billing-address-country"/>
            </div>

            <c:set var="hide" value=""/>
            <c:if test="${empty regions}">
                <c:set var="hide" value="hidden"/>
            </c:if>

            <div class="form-group" id="billing-address-regions-section_${updateFormNumber}" ${hide}>
                <formElement:formSelectBox idKey="myAccountBillingAddressRegions_${updateFormNumber}"
                                           labelKey="profile.address.region" path="province" items="${regions}"
                                           skipBlankMessageKey="text.page.default.select"
                                           selectCSSClass="billing-address-regions form-control" mandatory="true"
                                           itemValue="isocode" itemLabel="name"/>
            </div>
            <div class="form-group">
                <formElement:formInputBox idKey="address.postcode" labelKey="address.postcode" path="zipCode" inputCSS="text"
                                          mandatory="true" tabindex="5"/>
            </div>
            <div class="row margin-top-40 margin-bottom-30">
                <div class="col-md-8 col-md-offset-2 mt-3">
                    <button type="submit" class="btn btn-primary btn-block">
                        <spring:theme code='text.save.label'/>
                    </button>
                </div>
                <div class="col-md-8 col-md-offset-2 mt-3 margin-top-30">
                    <button class="btn btn-primary btn-block">
                        <spring:theme code='text.cancel.label'/>
                    </button>
                </div>
            </div>
        </div>
    </div>
</form:form>
