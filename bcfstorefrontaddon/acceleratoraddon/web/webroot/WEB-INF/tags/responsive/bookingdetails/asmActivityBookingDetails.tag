<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="format" tagdir="/WEB-INF/tags/addons/bcfstorefrontaddon/responsive/format"%>
<%@ attribute name="asmOrderData" required="true" type="com.bcf.facades.order.AsmOrderViewData"%>

<spring:htmlEscape defaultHtmlEscape="false" />

<div class="row">
    <table>
        <c:forEach items="${asmOrderData.activities}" var="activity">
            <c:set var="activityName" value="${activity.activityName}" />
            <c:if test="${asmOrderData.vacationOrder && not empty activityName && empty userRoles['ccagentgroup']}">
                <tr>
                    <td><spring:theme code="text.page.mybookings.activityName" text="Activity Name" /></td>
                    <td>${activityName}</td>
                </tr>
            </c:if>
            <c:set var="activityInventoryType" value="${activity.activityInventoryType}" />
            <c:if test="${asmOrderData.vacationOrder && not empty activityInventoryType && empty userRoles['ccagentgroup']}">
                <tr>
                    <td><spring:theme code="text.page.mybookings.activityInventoryType" text="Activity Inventory Type" /></td>
                    <td>${activityInventoryType}</td>
                </tr>
            </c:if>
            <c:set var="supplierName" value="${activity.supplierName}" />
            <c:if test="${asmOrderData.vacationOrder && not empty supplierName && empty userRoles['ccagentgroup']}">
                <tr>
                    <td><spring:theme code="text.page.mybookings.supplierName" text="Supplier Name" /></td>
                    <td>${supplierName}</td>
                </tr>
            </c:if>
            <c:set var="supplierPhone" value="${activity.supplierPhone}" />
            <c:if test="${asmOrderData.vacationOrder && not empty supplierPhone && empty userRoles['ccagentgroup']}">
                <tr>
                    <td><spring:theme code="text.page.mybookings.supplierPhone" text="Supplier Phone" /></td>
                    <td>${supplierPhone}</td>
                </tr>
            </c:if>
            <c:set var="supplierEmail" value="${activity.supplierEmail}" />
            <c:if test="${asmOrderData.vacationOrder && not empty supplierEmail && empty userRoles['ccagentgroup']}">
                <tr>
                    <td><spring:theme code="text.page.mybookings.supplierEmail" text="Supplier Email" /></td>
                    <td>${supplierEmail}</td>
                </tr>
            </c:if>
        </c:forEach>
	</table>
</div>
