<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="format" tagdir="/WEB-INF/tags/addons/bcfstorefrontaddon/responsive/format"%>
<%@ attribute name="asmOrderData" required="true" type="com.bcf.facades.order.AsmOrderViewData"%>

<spring:htmlEscape defaultHtmlEscape="false" />

<div class="row">
    <table>
        <c:forEach items="${asmOrderData.orderModificationsData}" var="asmOrderModificationsDataView">
            <c:set var="orderModificationDate" value="${asmOrderModificationsDataView.orderModificationDate}" />
            <c:if test="${asmOrderData.vacationOrder && not empty orderModificationDate && empty userRoles['ccagentgroup']}">
                <tr>
                    <td><spring:theme code="text.page.mybookings.orderModificationDate" text="Order Modification Date" /></td>
                    <td>${orderModificationDate}</td>
                </tr>
            </c:if>
            <c:set var="modifiedByAgent" value="${asmOrderModificationsDataView.modifiedByAgent}" />
                <c:if test="${asmOrderData.vacationOrder && not empty modifiedByAgent && empty userRoles['ccagentgroup']}">
                <tr>
                    <td><spring:theme code="text.page.mybookings.modifiedByAgent" text="Order Modified By Agent" /></td>
                    <td>${modifiedByAgent}</td>
                </tr>
            </c:if>
            <c:set var="orderTotalDifference" value="${asmOrderModificationsDataView.orderTotalDifference}" />
            <c:if test="${asmOrderData.vacationOrder && not empty orderTotalDifference && empty userRoles['ccagentgroup']}">
                <tr>
                    <td><spring:theme code="text.page.mybookings.orderTotalDifference" text="Order Total Difference" /></td>
                    <td>${orderTotalDifference}</td>
                </tr>
            </c:if>
            <c:set var="orderTotalCostToBCFDifference" value="${asmOrderModificationsDataView.orderTotalCostToBCFDifference}" />
            <c:if test="${asmOrderData.vacationOrder && not empty orderModificationDate && empty userRoles['ccagentgroup']}">
                <tr>
                    <td><spring:theme code="text.page.mybookings.orderTotalCostToBCFDifference" text=" Total Cost To BCF Difference" /></td>
                    <td>${orderTotalCostToBCFDifference}</td>
                </tr>
            </c:if>
            <c:set var="orderVersionId" value="${asmOrderModificationsDataView.orderVersionId}" />
            <c:if test="${asmOrderData.vacationOrder && not empty orderVersionId && empty userRoles['ccagentgroup']}">
            <c:url var="orderVersion" value="/manage-booking/booking-details/orders/${orderCode}?versionId=${orderVersionId}" />
                <tr>
                    <td><spring:theme code="text.page.mybookings.orderVersionId" text="Previous Order Version" /></td>
                    <td>
                        <a href="${orderVersion}" class="btn btn-secondary col-xs-12">
                            <spring:theme code="text.page.mybookings.viewOrder" text="View Previous Order" />
                        </a>
                    </td>
                </tr>
            </c:if>
        </c:forEach>
	</table>
</div>
