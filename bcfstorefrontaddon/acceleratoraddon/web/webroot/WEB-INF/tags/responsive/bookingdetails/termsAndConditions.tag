<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="bookingDetails" tagdir="/WEB-INF/tags/addons/bcfstorefrontaddon/responsive/bookingdetails"%>
<%@ attribute name="termsAndConditions" required="false" type="java.util.List"%>
<spring:htmlEscape defaultHtmlEscape="false" />
<c:forEach var="termsAndCondition" items="${termsAndConditions}">
	<bookingDetails:termsAndCondition termsAndCondition="${termsAndCondition}"/>
</c:forEach>
