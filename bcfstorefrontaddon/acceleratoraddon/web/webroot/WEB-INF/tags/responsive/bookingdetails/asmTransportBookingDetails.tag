<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="format" tagdir="/WEB-INF/tags/addons/bcfstorefrontaddon/responsive/format"%>
<%@ attribute name="asmOrderData" required="true" type="com.bcf.facades.order.AsmOrderViewData"%>
<%@ attribute name="bookingRef" required="true" type="java.lang.String"%>

<spring:htmlEscape defaultHtmlEscape="false" />

<div class="row">
    <table>
        <c:set var="asmTransportEntryDataView" value="${asmOrderData.bookingReferenceSailingDetails[bookingRef]}" />
        <c:if test="${not empty bookingRef}">
            <tr>
                <td><spring:theme code="text.page.mybookings.eBookingReference" text="EBooking Reference" /></td>
                <td>${bookingRef}</td>
            </tr>
        </c:if>
        <c:set var="openTicket" value="${asmTransportEntryDataView.openTicket}" />
        <c:if test="${asmOrderData.vacationOrder && not empty openTicket}">
            <tr>
                <td><spring:theme code="text.page.mybookings.openTicket" text="Open Ticket" /></td>
                <td>${openTicket}</td>
            </tr>
        </c:if>
        <c:set var="dangerousGoods" value="${asmTransportEntryDataView.dangerousGoods}" />
        <c:if test="${!asmOrderData.vacationOrder && not empty dangerousGoods}">
            <tr>
                <td><spring:theme code="text.page.mybookings.dangerousGoods" text="Dangerous Goods" /></td>
                <td>${dangerousGoods}</td>
            </tr>
         </c:if>
        <c:set var="liveStock" value="${asmTransportEntryDataView.liveStock}" />
        <c:if test="${!asmOrderData.vacationOrder && not empty liveStock}">
        <tr>
            <td><spring:theme code="text.page.mybookings.liveStock" text="live Stock" /></td>
            <td>${liveStock}</td>
        </tr>
        </c:if>
        <c:set var="specialInstructions" value="${asmTransportEntryDataView.specialInstructions}" />
        <c:if test="${not empty specialInstructions}">
        <tr>
            <td><spring:theme code="text.page.mybookings.specialInstructions" text="Special Instructions" /></td>
            <td>${specialInstructions}</td>
        </tr>
        </c:if>
        <c:set var="agentRemarks" value="${asmTransportEntryDataView.agentRemarks}" />
        <c:if test="${not empty agentRemarks}">
        <tr>
            <td><spring:theme code="text.page.mybookings.agentRemarks" text="Agent Remarks" /></td>
            <td>${agentRemarks}</td>
        </tr>
        </c:if>
        <c:set var="referenceCode" value="${asmTransportEntryDataView.referenceCode}" />
        <c:if test="${not empty referenceCode}">
        <tr>
            <td><spring:theme code="text.page.mybookings.referenceCode" text="Reference Code" /></td>
            <td>${referenceCode}</td>
        </tr>
        </c:if>

        <c:forEach items="${asmOrderData.bookingReferencePassengerDetails[bookingRef]}" var="asmEntryPassengerDataView">
            <c:set var="passengerType" value="${asmEntryPassengerDataView.passengerType}" />
            <c:if test="${not empty passengerType}">
            <tr>
                <td><spring:theme code="text.page.mybookings.passengerType" text="Passenger Type" /></td>
                <td>${passengerType}</td>
            </tr>
            </c:if>
            <c:set var="noOfPassengers" value="${asmEntryPassengerDataView.noOfPassengers}" />
            <c:if test="${not empty noOfPassengers}">
            <tr>
                <td><spring:theme code="text.page.mybookings.noOfPassengers" text="No Of Passengers" /></td>
                <td>${noOfPassengers}</td>
            </tr>
            </c:if>
            <c:set var="passengerCostToBCF" value="${asmEntryPassengerDataView.passengerCostToBCF}" />
            <c:if test="${asmOrderData.vacationOrder && not empty passengerCostToBCF && (not empty userRoles['bcfvmanagergroup'] ||  not empty userRoles['financestaffgroup'])}">
            <tr>
                <td><spring:theme code="text.page.mybookings.passengerCostToBCF" text="Passenger Cost To BCF" /></td>
                <td>${passengerCostToBCF}</td>
            </tr>
            </c:if>
        </c:forEach>

        <c:forEach items="${asmOrderData.bookingReferenceProductDetails[bookingRef]}" var="asmProductEntryDataView">
            <c:set var="productCode" value="${asmProductEntryDataView.productCode}" />
            <c:if test="${asmOrderData.vacationOrder && not empty productCode}">
            <tr>
                <td><spring:theme code="text.page.mybookings.productCode" text="Product Code" /></td>
                <td>${productCode}</td>
            </tr>
            </c:if>
            <c:set var="productCostToBCFV" value="${asmProductEntryDataView.productCostToBCFV}" />
            <c:if test="${asmOrderData.vacationOrder && not empty productCostToBCFV}">
            <tr>
                <td><spring:theme code="text.page.mybookings.productCostToBCFV" text="Product Cost To BCFV" /></td>
                <td>${productCostToBCFV}</td>
            </tr>
            </c:if>
            <c:set var="bcfvMarginRate" value="${asmProductEntryDataView.bcfvMarginRate}" />
            <c:if test="${asmOrderData.vacationOrder && not empty bcfvMarginRate}">
                <tr>
                    <td><spring:theme code="text.page.mybookings.bcfvMarginRate" text="Product BCFV Margin" /></td>
                    <td>${bcfvMarginRate}</td>
                </tr>
            </c:if>
            <c:set var="promotionName" value="${asmProductEntryDataView.promotionName}" />
            <c:if test="${asmOrderData.vacationOrder && not empty promotionName}">
                <tr>
                    <td><spring:theme code="text.page.mybookings.promotionName" text="Promotion Name" /></td>
                    <td>${promotionName}</td>
                </tr>
            </c:if>
            <c:set var="productPromotionDiscountAmount" value="${asmProductEntryDataView.productPromotionDiscountAmount}" />
            <c:if test="${asmOrderData.vacationOrder && not empty productPromotionDiscountAmount}">
                <tr>
                    <td><spring:theme code="text.page.mybookings.productPromotionDiscountAmount" text="Product discount Amount" /></td>
                    <td>${productPromotionDiscountAmount}</td>
                </tr>
            </c:if>
            <c:set var="productGSTToCustomerAmount" value="${asmProductEntryDataView.productGSTToCustomerAmount}" />
            <c:if test="${asmOrderData.vacationOrder && not empty productGSTToCustomerAmount}">
                <tr>
                    <td><spring:theme code="text.page.mybookings.productGSTToCustomerAmount" text="Product GST Amount To Customer" /></td>
                    <td>${productGSTToCustomerAmount}</td>
                </tr>
            </c:if>
            <c:set var="productPriceToCustomerAmount" value="${asmProductEntryDataView.productPriceToCustomerAmount}" />
            <c:if test="${asmOrderData.vacationOrder && not empty productPriceToCustomerAmount}">
                <tr>
                    <td><spring:theme code="text.page.mybookings.productPriceToCustomerAmount" text="Product Price To Customer" /></td>
                    <td>${productPriceToCustomerAmount}</td>
                </tr>
            </c:if>
         </c:forEach>
	</table>
</div>
