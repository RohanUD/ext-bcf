<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="format" tagdir="/WEB-INF/tags/addons/bcfstorefrontaddon/responsive/format"%>
<%@ attribute name="asmOrderData" required="true" type="com.bcf.facades.order.AsmOrderViewData"%>

<spring:htmlEscape defaultHtmlEscape="false" />

<div class="row">
    <table>
        <c:set var="vacationsTravellerName" value="${asmOrderData.vacationsTravellerName}" />
	    <c:if test="${asmOrderData.vacationOrder && not empty vacationsTravellerName && empty userRoles['ccagentgroup']}">
            <tr>
                <td><spring:theme code="text.page.mybookings.vacationsTravellerName" text="Vacations Traveller Name" /></td>
                <td>${vacationsTravellerName}</td>
            </tr>
        </c:if>
        <c:set var="vacationsTotalCostToBCF" value="${asmOrderData.vacationsTotalCostToBCF}" />
	    <c:if test="${asmOrderData.vacationOrder && not empty vacationsTotalCostToBCF && (not empty userRoles['bcfvmanagergroup'] ||  not empty userRoles['financestaffgroup'])}">
            <tr>
                <td><spring:theme code="text.page.mybookings.vacationsTotalCostToBCF" text="Vacations Total Cost To BCF" /></td>
                <td>${vacationsTotalCostToBCF}</td>
            </tr>
        </c:if>
        <c:set var="vacationsTotalMargin" value="${asmOrderData.vacationsTotalMargin}" />
	    <c:if test="${asmOrderData.vacationOrder && not empty vacationsTotalMargin && (not empty userRoles['bcfvmanagergroup'] ||  not empty userRoles['financestaffgroup'])}">
            <tr>
                <td><spring:theme code="text.page.mybookings.vacationsTotalMargin" text="Vacations Total Margin" /></td>
                <td>${vacationsTotalMargin}</td>
            </tr>
        </c:if>

	    <c:forEach items="${vacationPacakageAppliedPromotions}" var="vacationPromotion">
            <c:set var="promotionsName" value="${vacationPromotion.promotionsName}" />
            <c:if test="${asmOrderData.vacationOrder && not empty promotionsName && empty userRoles['ccagentgroup']}">
                <tr>
                    <td><spring:theme code="text.page.mybookings.promotionsName" text="Promotion Name" /></td>
                    <td>${promotionsName}</td>
                </tr>
            </c:if>
            <c:set var="discountsAmount" value="${vacationPromotion.discountsAmount}" />
            <c:if test="${asmOrderData.vacationOrder && not empty discountsAmount && empty userRoles['ccagentgroup']}">
                <tr>
                    <td><spring:theme code="text.page.mybookings.discountsAmount" text="Promotion Discount Amount" /></td>
                    <td>${discountsAmount}</td>
                </tr>
            </c:if>
            <c:set var="discountsGSTAmount" value="${vacationPromotion.discountsGSTAmount}" />
            <c:if test="${asmOrderData.vacationOrder && not empty discountsGSTAmount && empty userRoles['ccagentgroup']}">
                <tr>
                    <td><spring:theme code="text.page.mybookings.discountsGSTAmount" text="Promotion Discount GST Amount" /></td>
                    <td>${discountsGSTAmount}</td>
                </tr>
            </c:if>
	    </c:forEach>

	    <c:set var="vacationsCommentAgent" value="${vacationsCommentAgent}" />
        <c:if test="${asmOrderData.vacationOrder && not empty vacationsCommentAgent && empty userRoles['ccagentgroup']}">
            <tr>
                <td><spring:theme code="text.page.mybookings.vacationsCommentAgent" text="Vacation Package Remarks By Agent" /></td>
                <td>${vacationsCommentAgent}</td>
            </tr>
        </c:if>
        <c:set var="vacationsCommentText" value="${vacationPromotion.vacationsCommentText}" />
        <c:if test="${asmOrderData.vacationOrder && not empty vacationsCommentText && empty userRoles['ccagentgroup']}">
            <tr>
                <td><spring:theme code="text.page.mybookings.vacationsCommentText" text="Vacation Package Comments" /></td>
                <td>${vacationsCommentText}</td>
            </tr>
        </c:if>
        <c:set var="vacationsCommentDate" value="${vacationPromotion.vacationsCommentDate}" />
        <c:if test="${asmOrderData.vacationOrder && not empty vacationsCommentDate && empty userRoles['ccagentgroup']}">
            <tr>
                <td><spring:theme code="text.page.mybookings.vacationsCommentDate" text="Vacation Package Date" /></td>
                <td>${vacationsCommentDate}</td>
            </tr>
        </c:if>

        <c:forEach items="${asmOrderData.bookingReferenceHotelRoomDetails}" var="asmHotelRoomDataView">
            <c:set var="hotelName" value="${asmHotelRoomDataView.hotelName}" />
            <c:if test="${asmOrderData.vacationOrder && not empty hotelName}">
                <tr>
                    <td><spring:theme code="text.page.mybookings.hotelName" text="Hotel Name" /></td>
                    <td>${hotelName}</td>
                </tr>
            </c:if>
            <c:set var="roomType" value="${asmHotelRoomDataView.roomType}" />
            <c:if test="${asmOrderData.vacationOrder && not empty roomType && empty userRoles['ccagentgroup']}">
                <tr>
                    <td><spring:theme code="text.page.mybookings.roomType" text="Room Type" /></td>
                    <td>${roomType}</td>
                </tr>
            </c:if>
            <c:set var="roomInventoryType" value="${asmHotelRoomDataView.roomInventoryType}" />
            <c:if test="${asmOrderData.vacationOrder && not empty roomInventoryType && empty userRoles['ccagentgroup']}">
                <tr>
                    <td><spring:theme code="text.page.mybookings.roomInventoryType" text="Room Inventory Type" /></td>
                    <td>${roomInventoryType}</td>
                </tr>
            </c:if>
            <c:set var="pricePerNight" value="${asmHotelRoomDataView.pricePerNight}" />
            <c:if test="${asmOrderData.vacationOrder && not empty pricePerNight && (not empty userRoles['bcfvmanagergroup'] ||  not empty userRoles['financestaffgroup'])}">
                <tr>
                    <td><spring:theme code="text.page.mybookings.pricePerNight" text="Price Per Night" /></td>
                    <td>${pricePerNight}</td>
                </tr>
            </c:if>
            <c:set var="supplierName" value="${asmHotelRoomDataView.supplierName}" />
            <c:if test="${asmOrderData.vacationOrder && not empty supplierName && empty userRoles['ccagentgroup']}">
                <tr>
                    <td><spring:theme code="text.page.mybookings.supplierName" text="Supplier Name" /></td>
                    <td>${supplierName}</td>
                </tr>
            </c:if>
            <c:set var="supplierPhone" value="${asmHotelRoomDataView.supplierPhone}" />
            <c:if test="${asmOrderData.vacationOrder && not empty supplierPhone && empty userRoles['ccagentgroup']}">
                <tr>
                    <td><spring:theme code="text.page.mybookings.supplierPhone" text="Supplier Phone" /></td>
                    <td>${supplierPhone}</td>
                </tr>
            </c:if>
            <c:set var="supplierEmail" value="${asmHotelRoomDataView.supplierEmail}" />
            <c:if test="${asmOrderData.vacationOrder && not empty supplierEmail && empty userRoles['ccagentgroup']}">
                <tr>
                    <td><spring:theme code="text.page.mybookings.supplierEmail" text="Supplier Email" /></td>
                    <td>${supplierEmail}</td>
                </tr>
            </c:if>
            <c:set var="agentLogin" value="${asmHotelRoomDataView.agentLogin}" />
            <c:if test="${asmOrderData.vacationOrder && not empty agentLogin && empty userRoles['ccagentgroup']}">
                <tr>
                    <td><spring:theme code="text.page.mybookings.agentLogin" text="Agent Login" /></td>
                    <td>${agentLogin}</td>
                </tr>
            </c:if>
            <c:set var="agentCommentDate" value="${asmHotelRoomDataView.agentCommentDate}" />
            <c:if test="${asmOrderData.vacationOrder && not empty agentCommentDate && empty userRoles['ccagentgroup']}">
                <tr>
                    <td><spring:theme code="text.page.mybookings.agentCommentDate" text="Agent Comment Date" /></td>
                    <td>${agentCommentDate}</td>
                </tr>
            </c:if>
            <c:set var="agentComment" value="${asmHotelRoomDataView.agentComment}" />
            <c:if test="${asmOrderData.vacationOrder && not empty agentComment && empty userRoles['ccagentgroup']}">
                <tr>
                    <td><spring:theme code="text.page.mybookings.agentComment" text="Agent Comment" /></td>
                    <td>${agentComment}</td>
                </tr>
            </c:if>

            <c:forEach items="${asmHotelRoomDataView.guests}" var="asmRoomGuestDataView">
                <c:set var="guestType" value="${asmRoomGuestDataView.guestType}" />
                <c:if test="${asmOrderData.vacationOrder && not empty guestType && empty userRoles['ccagentgroup']}">
                    <tr>
                        <td><spring:theme code="text.page.mybookings.guestType" text="Guest Type" /></td>
                        <td>${guestType}</td>
                    </tr>
                </c:if>
                <c:set var="configuredRoomCost" value="${asmRoomGuestDataView.configuredRoomCost}" />
                <c:if test="${asmOrderData.vacationOrder && not empty configuredRoomCost && (not empty userRoles['bcfvmanagergroup'] ||  not empty userRoles['financestaffgroup'])}">
                    <tr>
                        <td><spring:theme code="text.page.mybookings.configuredRoomCost" text="Configured Room Cost" /></td>
                        <td>${configuredRoomCost}</td>
                    </tr>
                </c:if>
            </c:forEach>

        </c:forEach>

	</table>
</div>
