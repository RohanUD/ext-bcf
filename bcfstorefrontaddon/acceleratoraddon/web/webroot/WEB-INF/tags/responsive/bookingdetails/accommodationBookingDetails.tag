<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="format" tagdir="/WEB-INF/tags/addons/bcfstorefrontaddon/responsive/format"%>
<%@ taglib prefix="booking" tagdir="/WEB-INF/tags/addons/bcfvoyageaddon/responsive/bookingdetails"%>
<%@ taglib prefix="bookingDetails" tagdir="/WEB-INF/tags/addons/bcfstorefrontaddon/responsive/bookingdetails"%>
<%@ taglib prefix="accommodationBooking" tagdir="/WEB-INF/tags/addons/bcfaccommodationsaddon/responsive/accommodationbooking"%>
<%@ taglib prefix="accommodationReservation" tagdir="/WEB-INF/tags/addons/bcfaccommodationsaddon/responsive/accommodationreservation"%>
<%@ taglib prefix="bcfglobalreservation" tagdir="/WEB-INF/tags/addons/bcfcommonsaddon/responsive/bcfglobalreservation"%>
<%@ taglib prefix="accommodationReservation" tagdir="/WEB-INF/tags/addons/bcfcommonsaddon/responsive/accommodationreservation"%>
<%@ attribute name="accommodationReservationData" required="true" type="de.hybris.platform.commercefacades.accommodation.AccommodationReservationData"%>
<%@ attribute name="customerReviews" required="false" type="java.util.List"%>
<%@ attribute name="isTravelSite" type="java.lang.Boolean"%>
<c:url var="payNowUrl" value="/checkout/paynow/${bookingReference}" />

<spring:htmlEscape defaultHtmlEscape="false" />

<div class="row">
<div class="form-group col-xs-12 col-sm-6">
   <accommodationBooking:accommodationBookingAction   bookingActionResponseData="${bookingActionResponse}" actionType="AMEND_ACCOMMODATION" />
</div>

 <input type="hidden" name="hasAmendAccommodation" id="hasAmendAccommodation" value="${hasAmendAccommodation}">
 <input type="hidden" name="hasAdjustCost" id="hasAdjustCost" value="${hasAdjustCost}">
 <input type="hidden" name="hasAmendAccommodationError" id="hasAmendAccommodationError" value="${hasAmendAccommodationError}">
<div class="y_amendAccommodationSection">
<c:if test="${hasAmendAccommodation || hasAmendAccommodationError}">
    <accommodationBooking:amendAccommodation />
</c:if>
<c:if test="${hasAdjustCost}">
    <accommodationBooking:adjustCost />
</c:if>

</div>
 <div class="form-group col-xs-12 col-sm-6">
    <accommodationBooking:accommodationBookingAction   bookingActionResponseData="${bookingActionResponse}" actionType="AMEND_SAILING" />
</div>
 <div class="form-group col-xs-12 col-sm-6">
    <accommodationBooking:accommodationBookingAction   bookingActionResponseData="${bookingActionResponse}" actionType="AMEND_ACTIVITY" />
</div>

<div class="form-group col-xs-12 col-sm-6">
    <accommodationBooking:accommodationBookingAction   bookingActionResponseData="${bookingActionResponse}" actionType="ADJUST_COST" />
</div>

<div class="form-group col-xs-12 col-sm-6">
    <c:if test="${balanceAmountToPay}">
        <a class="btn btn-primary btn-block ${fn:escapeXml(y_class)}" href="${payNowUrl}"}>
            <spring:theme code="button.booking.details.accommodation.booking.action.paynow" text="${fn:escapeXml(fn:toLowerCase(paynow))}" />
        </a>
    </c:if>
</div>

</div>

<c:if test="${isWaitlisted}">
    <div class="green-bar-msg">
        <span class="bcf bcf-icon-checkmark"></span>
        <strong><spring:theme code="text.page.managemybooking.bookingdetails.waitlisted" text="Booking is waitlisted." /></strong>
    </div>
</c:if>

<c:forEach var="entry" items="${bookingMarkers}">
    <div class="green-bar-msg marker-${entry.key}" style="text-align:left">
        <c:forEach var="marker" items="${entry.value}">
            <strong><spring:theme code="text.page.managemybooking.bookingdetails.${marker}" arguments="${entry.key}" /></strong><br>
        </c:forEach>
        <a href="${entry.key}" class="btn btn-primary btn-block js-reset-booking-markers">
            <spring:theme code="button.page.managemybooking.bookingdetails.clearNotifications"  />
        </a>
        <hr>
    </div>
</c:forEach>

<div class="booking room-details ${accommodationReservationData.bookingStatusCode == 'CANCELLED' ? 'booking-cancel' : ''}">


<c:forEach items="${bcfGlobalReservationData.packageReservations.packageReservationDatas}" var="packageReservation" varStatus="packageReservationItemIdx">
        <div class="row">
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 px-5">
                <ul class="vacation-booking-box">
                    <li><accommodationReservation:accommodationReservation accommodationReservation="${packageReservation.accommodationReservationData}" accommodationResVarStatus="1" /></li>
                    <c:if test="${asmLoggedIn}">
                        <li><bookingDetails:asmTravelBookingDetails asmOrderData="${asmOrderData}"/></li>
                    </c:if>
                <hr>
                    <li><bcfglobalreservation:transportReservation reservationData="${packageReservation.reservationData}" /></li>
                </ul>
            </div>
        </div>
		<c:if test="${not empty packageReservation.activityReservations}">

			<bcfglobalreservation:activityReservations activityReservationDataList="${packageReservation.activityReservations}" />
			<c:if test="${asmLoggedIn}">
                <li><bookingDetails:asmActivityBookingDetails asmOrderData="${asmOrderData}"/></li>
            </c:if>
		</c:if>
	</c:forEach>

	<c:if test="${not empty bcfGlobalReservationData.accommodationReservations and not empty bcfGlobalReservationData.accommodationReservations.accommodationReservations}">
	<c:forEach items="${bcfGlobalReservationData.accommodationReservations.accommodationReservations}" var="accommodationReservation" varStatus="accommodationReservationItemIdx">
            <div class="row">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 px-5">
                    <ul class="vacation-booking-box">
                        <li><accommodationReservation:accommodationReservation accommodationReservation="${accommodationReservation}" accommodationResVarStatus="1" /></li>
 <hr>
                    </ul>
                </div>
            </div>

    	</c:forEach>
    	</c:if>


    	<c:if test="${not empty bcfGlobalReservationData.transportReservations and not empty bcfGlobalReservationData.transportReservations.reservationDatas}">
        	<c:forEach items="${bcfGlobalReservationData.transportReservations.reservationDatas}" var="transportReservation" varStatus="transportReservationItemIdx">
                    <div class="row">
                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 px-5">
                            <ul class="vacation-booking-box">
                                <li><bcfglobalreservation:transportReservation reservationData="${transportReservation}"/></li>
 <hr>
                            </ul>
                        </div>
                    </div>

            	</c:forEach>
            	</c:if>

        <c:if test="${not empty bcfGlobalReservationData.activityReservations and not empty bcfGlobalReservationData.activityReservations.activityReservationDatas }">

    			<bcfglobalreservation:activityReservations activityReservationDataList="${bcfGlobalReservationData.activityReservations}" />

    		</c:if>

			<c:if test="${asmLoggedIn}">
                <li><bookingDetails:asmPaymentTransactionDetails asmOrderData="${asmOrderData}"/></li>
                <li><bookingDetails:asmOrderHistoryDetails asmOrderData="${asmOrderData}"/></li>
            </c:if>

		<div id="date-update">
			<div class="row">
				<div class="col-xs-12 col-sm-6">
					<c:if test="${globalReservationData.reservationData != null}">
					 	<accommodationBooking:accommodationBookingAction bookingActionResponseData="${bookingActionResponse}" actionType="CANCEL_BOOKING" />

					</c:if>
				</div>
				<div class="col-xs-12 col-sm-6">
					<accommodationBooking:accommodationBookingAction bookingActionResponseData="${bookingActionResponse}" actionType="PAY_NOW" />
				</div>
			</div>
		</div>

        <div class="row">
		    <div class="col-xs-12 col-sm-6">
             <c:if test="${not empty bcfGlobalReservationData.vacationFees}">
                 <c:forEach var="vacationFee" items="${bcfGlobalReservationData.vacationFees}">
                     <li class="p-2">
                         ${vacationFee.feeType}
                         <span><format:price priceData="${vacationFee.vacationFee}" /></span>
                     </li>
                 </c:forEach>
             </c:if>
          </div>
        </div>
</div>
