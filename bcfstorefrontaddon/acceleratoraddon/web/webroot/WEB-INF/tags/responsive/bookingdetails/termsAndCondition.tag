<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="cms" uri="http://hybris.com/tld/cmstags"%>
<%@ attribute name="termsAndCondition" required="false" type="com.bcf.facades.booking.confirmation.TermsAndConditionsData"%>
<spring:htmlEscape defaultHtmlEscape="false" />
<div class="row">
<div class="col-lg-12 col-md-12">
	<div class="row margin-bottom-30 text-center">
	    <div class="col-lg-10 col-md-10 col-md-offset-1">
			<h5 class="heading-black"><strong>${termsAndCondition.checkInTitle}</strong></h5>
            <c:if test="${termsAndCondition.image.url ne null }">
                <img src="${termsAndCondition.image.url}" />
            </c:if>
		</div>
	</div>
	<div class="row text-center">
	    <div class="col-lg-10 col-md-10 col-md-offset-1">
			<h6>
				<a href="javascript:void(0)" class="importantInfo">
					<spring:theme code="text.confirmation.sailing.information" text="Important info for sailing" />
					<span class="bcf bcf-icon-down-arrow"></span>
				</a>
			</h6>
		</div>
	</div>
	<div class="row margin-bottom-40 info-sailing-details-show">
	    <div class="col-lg-10 col-md-10 col-md-offset-1">
		    <p>${termsAndCondition.checkInText}</p>
		</div>
		
	    <div class="col-lg-10 col-md-10 col-md-offset-1 mt-5 mb-5">
		    <h2 class="vacation-h2 heading-black">${termsAndCondition.title}</h2>
		</div>
	
		<div class="col-lg-10 col-md-10 col-md-offset-1">
            <strong>${termsAndCondition.additionalCheckInTitle}</strong>
            <p>${termsAndCondition.additionalCheckInText}</p>
        </div>
	</div>
	
     
      <div class="col-lg-12 col-xs-12">
        <cms:pageSlot position="SailingInfo" var="feature" element="section">
            <cms:component component="${feature}" />
        </cms:pageSlot>
     </div>
</div>
</div>
