<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags"%>
<%@ taglib prefix="booking" tagdir="/WEB-INF/tags/addons/bcfvoyageaddon/responsive/bookingdetails"%>
<%@ taglib prefix="bookingDetails" tagdir="/WEB-INF/tags/addons/bcfstorefrontaddon/responsive/bookingdetails"%>
<%@ taglib prefix="cancel" tagdir="/WEB-INF/tags/addons/bcfvoyageaddon/responsive/cancel"%>
<%@ taglib prefix="format" tagdir="/WEB-INF/tags/addons/bcfstorefrontaddon/responsive/format"%>
<%@ attribute name="reservationData" required="true" type="de.hybris.platform.commercefacades.travel.reservation.data.ReservationData"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<spring:htmlEscape defaultHtmlEscape="false" />
<spring:url value="/view/TravelBookingDetailsComponentController/add-goodwill-discount" var="addGoodWillUrl" />
<%-- MANAGE BOOKING COMPONENT --%>
<div class="panel-body collapse in ${reservationData.bookingStatusCode == 'CANCELLED' ? 'booking-cancel' : ''}" id="booking-details-body">
	<div class="row">
		<c:if test="${reservationData.bookingStatusCode == 'ACTIVE_DISRUPTED_PENDING'}">
			<div class="alert-wrap">
				<div class="alert alert-danger" role="alert">
					<div class="row">
						<p>
							<strong><spring:theme code="text.page.managemybooking.bookingdetails.disruption" text="Your flight has been changed due to disruption" /></strong>
						</p>
						<p>
							<spring:theme code="text.page.managemybooking.bookingdetails.disruption.info1" text="Please see your new booking below with your original flight highlighted in red" />
						</p>
						<p>
							<spring:theme code="text.page.managemybooking.bookingdetails.disruption.info2" text="Do you wish to accept these changes? If you choose not to your booking will be cancelled and you will be refunded." />
						</p>
					</div>
				</div>
			</div>
		</c:if>
	</div>
	<input type="hidden" value="${fn:escapeXml(reservationData.code)}" name="bookingReference" id="bookingReference">
	<%-- <input type="hidden" value="${fn:escapeXml(reservationData.journeyReferenceNumber)}" id="journeyReferenceNumber"> --%>
	<c:forEach var="reservationItem" items="${reservationData.reservationItems}">
		<div class="panel panel-primary">
			<div class="panel-heading">
				<h3 class="title">
					<spring:theme code="text.page.managemybooking.bookingreferece" arguments="${reservationItem.bcfBookingReference}" />
				</h3>
			</div>
		</div>
		<c:if test="${reservationData.bookingStatusCode != 'ACTIVE_DISRUPTED_PENDING'}">
			<div class="row">
				<div class="panel button-wrap">
					<div class="col-xs-12 col-md-4">
						<dl class="booking status">
							<dt>
								<spring:theme code="text.page.managemybooking.bookingstatus" text="Booking Status:" />
							</dt>
							<dd>
								<!-- get the booking status from first reservation data -->
								<bookingDetails:status code="${reservationData.bookingStatusCode}" name="${reservationData.bookingStatusName}" />
							</dd>
						</dl>
					</div>
				</div>
			</div>
		</c:if>
		<div class="journey-wrapper">
			<input type="hidden" value="${fn:escapeXml(reservationData.journeyReferenceNumber)}" id="journeyReferenceNumber"> <input type="hidden" value="${fn:escapeXml(reservationItem.originDestinationRefNumber)}" id="originDestinationRefNumber">
			<c:forEach var="bookingAction" items="${reservationItem.bookingActionData}">
				<booking:bookingaction bookingActionDatas="${reservationItem.bookingActionData}" actionType="${bookingAction.actionType}" isGlobal="false" waitlistedBooking="${reservationItem.waitlisted}" />
			</c:forEach>
			<booking:bookingitem reservationData="${reservationData}" reservationItem="${reservationItem}" cssClass="panel panel-default my-account-secondary-panel" />
		</div>
	</c:forEach>
	<div class="y_cancelTravellerConfirm"></div>
</div>
