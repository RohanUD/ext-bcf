<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="format" tagdir="/WEB-INF/tags/addons/bcfstorefrontaddon/responsive/format"%>
<%@ attribute name="asmOrderData" required="true" type="com.bcf.facades.order.AsmOrderViewData"%>

<spring:htmlEscape defaultHtmlEscape="false" />

<div class="row">
    <table>
        <c:forEach items="${asmOrderData.transactions}" var="asmTransactionsDataView">
            <c:set var="paymentTimeStamp" value="${asmTransactionsDataView.paymentTimeStamp}" />
            <c:if test="${asmOrderData.vacationOrder && not empty paymentTimeStamp && empty userRoles['ccagentgroup']}">
                <tr>
                    <td><spring:theme code="text.page.mybookings.paymentTimeStamp" text="Payment Time" /></td>
                    <td>${paymentTimeStamp}</td>
                </tr>
            </c:if>
            <c:set var="paymentAgent" value="${asmTransactionsDataView.paymentAgent}" />
            <c:if test="${asmOrderData.vacationOrder && not empty paymentAgent && empty userRoles['ccagentgroup']}">
                <tr>
                    <td><spring:theme code="text.page.mybookings.paymentAgent" text="Payment Agent" /></td>
                    <td>${paymentAgent}</td>
                </tr>
            </c:if>
            <c:set var="transactionAmount" value="${asmTransactionsDataView.transactionAmount}" />
            <c:if test="${asmOrderData.vacationOrder && not empty transactionAmount && empty userRoles['ccagentgroup']}">
                <tr>
                    <td><spring:theme code="text.page.mybookings.transactionAmount" text="Transaction Amount" /></td>
                    <td>${transactionAmount}</td>
                </tr>
            </c:if>
            <c:set var="paymentMethod" value="${asmTransactionsDataView.paymentMethod}" />
            <c:if test="${asmOrderData.vacationOrder && not empty paymentMethod && empty userRoles['ccagentgroup']}">
                <tr>
                    <td><spring:theme code="text.page.mybookings.paymentMethod" text="Payment Method" /></td>
                    <td>${paymentMethod}</td>
                </tr>
            </c:if>
            <c:set var="paymentNumberText" value="${asmTransactionsDataView.paymentNumberText}" />
            <c:if test="${asmOrderData.vacationOrder && not empty paymentNumberText && empty userRoles['ccagentgroup']}">
                <tr>
                    <td><spring:theme code="text.page.mybookings.paymentNumberText" text="Payment Number Text" /></td>
                    <td>${paymentNumberText}</td>
                </tr>
            </c:if>
        </c:forEach>
	</table>
</div>
