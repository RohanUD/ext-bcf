<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="format" tagdir="/WEB-INF/tags/addons/bcfstorefrontaddon/responsive/format"%>
<%@ attribute name="asmOrderData" required="true" type="com.bcf.facades.order.AsmOrderViewData"%>
<%@ attribute name="userRoles" required="true" type="java.util.Map"%>

<spring:htmlEscape defaultHtmlEscape="false" />

<div class="row">
    <table>
	    <c:if test="${not empty asmOrderData.bookingDate}">
            <tr>
                <td><spring:theme code="text.page.mybookings.bookingDate" text="Booking Date" /></td>
                <td>${asmOrderData.bookingDate}</td>
            </tr>
        </c:if>
	    <c:if test="${not empty asmOrderData.salesChannel}">
            <tr>
                <td><spring:theme code="text.page.mybookings.salesChannel" text="Sales Channel" /></td>
                <td>${asmOrderData.salesChannel}</td>
            </tr>
        </c:if>
	    <c:if test="${not empty asmOrderData.createdByAgent}">
            <tr>
                <td><spring:theme code="text.page.mybookings.createdByAgent" text="Created By" /></td>
                <td>${asmOrderData.createdByAgent}</td>
            </tr>
        </c:if>
	    <c:set var="packageDiscountsBeforeTax" value="${asmOrderData.packageDiscountsBeforeTax}" />
	    <c:if test="${asmOrderData.vacationOrder && not empty packageDiscountsBeforeTax && empty userRoles['ccagentgroup'] }">
            <tr>
                <td><spring:theme code="text.page.mybookings.packageDiscountsBeforeTax" text="Total Package Discounts Before Tax" /></td>
                <td>${packageDiscountsBeforeTax}</td>
            </tr>
        </c:if>
        <c:set var="goodwillBeforeTax" value="${asmOrderData.goodwillBeforeTax}" />
	    <c:if test="${asmOrderData.vacationOrder && not empty goodwillBeforeTax && empty userRoles['ccagentgroup']}">
            <tr>
                <td><spring:theme code="text.page.mybookings.goodwillBeforeTax" text="Total Good Will Before Tax" /></td>
                <td>${goodwillBeforeTax}</td>
            </tr>
        </c:if>
        <c:set var="vacationsDepartureDate" value="${asmOrderData.vacationsDepartureDate}" />
	    <c:if test="${asmOrderData.vacationOrder && not empty vacationsDepartureDate && empty userRoles['ccagentgroup']}">
            <tr>
                <td><spring:theme code="text.page.mybookings.vacationsDepartureDate" text="Vacations Departure Date" /></td>
                <td>${asmOrderData.vacationsDepartureDate}</td>
            </tr>
        </c:if>
        <c:set var="vacationsDepositPaidDate" value="${asmOrderData.vacationsDepositPaidDate}" />
        <c:if test="${asmOrderData.vacationOrder && not empty vacationsDepositPaidDate && empty userRoles['ccagentgroup']}">
            <tr>
                <td><spring:theme code="text.page.mybookings.vacationsDepositPaidDate" text="Vacations Deposit Paid Date" /></td>
                <td>${vacationsDepositPaidDate}</td>
            </tr>
        </c:if>
        <c:set var="vacationsBalanceDueDate" value="${asmOrderData.vacationsBalanceDueDate}" />
	    <c:if test="${asmOrderData.vacationOrder && not empty vacationsBalanceDueDate && empty userRoles['ccagentgroup']}">
            <tr>
                <td><spring:theme code="text.page.mybookings.vacationsBalanceDueDate" text="Vacations Balance Due Date" /></td>
                <td>${vacationsBalanceDueDate}</td>
            </tr>
        </c:if>
    </table>
</div>
