<%@ tag body-content="empty" trimDirectiveWhitespaces="true" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ attribute name="searchPageData" required="true" type="de.hybris.platform.commerceservices.search.pagedata.SearchPageData" %>
<%@ taglib prefix="nav" tagdir="/WEB-INF/tags/responsive/nav" %>

<spring:htmlEscape defaultHtmlEscape="false"/>
<c:url value="/news-release" var="newsReleaseURL" />
<c:set var="newsResults" value="${searchPageData.results}" />

<c:forEach items="${newsResults}" var="news">
    <a href="${request.contextPath}${news.pageUrl}">
        <fmt:formatDate value="${news.publishedDate}" pattern="MMMM dd, YYYY" />
        <div>
            ${news.title}
        </div>
    </a>
    <hr />
</c:forEach>

<nav:serviceNoticePagination top="false" msgKey="text.service.notice.page" showCurrentPageInfo="true" hideRefineButton="true" supportShowPaged="${isShowPageAllowed}" supportShowAll="${isShowAllAllowed}" searchPageData="${searchPageData}" numberPagesShown="${numberPagesShown}" searchUrl="${newsReleaseURL}"/>
<br/>
<br/>
