<%@ tag body-content="empty" trimDirectiveWhitespaces="true" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ attribute name="serviceNotice" required="true" type="de.hybris.platform.commercefacades.travel.ServiceNoticeData" %>
<spring:htmlEscape defaultHtmlEscape="false"/>

<h2 class="title">
	<spring:theme code="text.cc.atAglance.serviceNotice.label" />
	&nbsp;-&nbsp;${serviceNotice.title}
	&nbsp;
	<fmt:formatDate value="${serviceNotice.publishedDate}"
		pattern="MMMM dd" />
	&nbsp;
	<c:if test="${not empty serviceNotice.expiryDate}">
		<fmt:formatDate value="${serviceNotice.expiryDate}" pattern="'-'dd" />
	</c:if>
</h2>

<h3 class="service-region">
	<spring:theme code="text.cc.atAglance.serviceNotice.posted.label" />
	&nbsp;
	<fmt:formatDate value="${serviceNotice.publishedDate}" dateStyle="long"
		pattern="EEEE, MMMM dd',' yyyy" />
</h3>

<p>
    ${serviceNotice.bodyContent}
</p>
