<%@ tag body-content="empty" trimDirectiveWhitespaces="true"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ attribute name="stage" required="true" type="java.lang.String"%>
<%@ attribute name="bookingJourney" required="true" type="java.lang.String"%>
<%@ attribute name="amend" required="false" type="java.lang.Boolean"%>
<spring:htmlEscape defaultHtmlEscape="false" />
<c:if test="${bookingJourney ne 'BOOKING_ACTIVITY_ONLY'}">
<div class="package-wizard">
    <div class="container">
		<ul class="nav-wizard">
			<c:choose>
				<c:when test="${amend}">
					<li class=" ${!(stage eq 'payment') && !(stage eq 'confirmation') ? 'active' : ''}">
						<span class="nav-wizard-text">
							<spring:theme code="text.booking.progress.bar.amend" text="Amend Selections" />
						</span>
					</li>
					<li class=" ${stage eq 'payment' ? 'active' : ''}">
						<span class="nav-wizard-text">
							<spring:theme code="text.booking.progress.bar.paymentrefund" text="Payment/Refund" />
						</span>
					</li>
					<li class=" ${stage eq 'confirmation' ? 'active' : ''}">
						<span class="nav-wizard-text">
							<spring:theme code="text.booking.progress.bar.confirmation" text="Confirmation" />
						</span>
					</li>
				</c:when>
				<c:otherwise>
					<li class=" ${stage eq 'passengers-vehicles' ? 'active' : ''}">
					     <a href="/package-ferry-passenger-info">1</a>
						<span class="nav-wizard-text">
							<spring:theme code="text.booking.progress.bar.package.Passenger.Vehicle" text="Passengers & Vehicle" />
						</span>
					</li>
					<li class=" ${stage eq 'sailing' ? 'active' : ''}">
					    <a href="/package-review-ferries">2</a>
						<span class="nav-wizard-text">
							<spring:theme code="text.booking.progress.bar.package.Sailing" text="Sailing(s)" />
						</span>
					</li>
					<li class=" ${stage eq 'activities' ? 'active' : ''}">
					     <a href="/deal-activity-listing">3</a>
						<span class="nav-wizard-text">
							<spring:theme code="text.activities.label" text="Activities" />
						</span>
					</li>
					<li class=" ${stage eq 'mypackage' ? 'active' : ''}">
					     <a href="/cart/summary">4</a>
						<span class="nav-wizard-text">
							<spring:theme code="text.booking.progress.bar.package.MyPackage" text="My Package" />
						</span>
					</li>
					<li class=" ${stage eq 'payment' ? 'active' : ''}">
					     <a href="/checkout/multi/payment-method/add">5</a>
						<span class="nav-wizard-text">
							<spring:theme code="text.booking.progress.bar.package.payment" text="Payment" />
						</span>
					</li>
				</c:otherwise>
			</c:choose>
		</ul>
	</div>
</div>
</c:if>
