<%@ tag language="java" pageEncoding="UTF-8"%>
<%@ tag body-content="empty" trimDirectiveWhitespaces="true"%>
<%@ taglib prefix="city" tagdir="/WEB-INF/tags/addons/bcfstorefrontaddon/responsive/citieslisting"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ attribute name="cityDataList" required="true" type="java.util.List"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="format" tagdir="/WEB-INF/tags/shared/format"%>
<%@ taglib prefix="ycommerce" uri="http://hybris.com/tld/ycommercetags"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>

<c:if test="${fn:length(cityDataList) gt 0}">
   <c:forEach var="cityData" items="${cityDataList}" varStatus="Count">
      <city:city cityData="${cityData}" />
   </c:forEach>
</c:if>
