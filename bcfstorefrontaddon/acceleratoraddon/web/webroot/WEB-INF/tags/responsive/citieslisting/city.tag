<%@ tag language="java" pageEncoding="UTF-8"%>
<%@ tag body-content="empty" trimDirectiveWhitespaces="true"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ attribute name="cityData" required="true" type="com.bcf.facades.travel.data.BcfLocationData"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="format" tagdir="/WEB-INF/tags/shared/format"%>
<%@ taglib prefix="ycommerce" uri="http://hybris.com/tld/ycommercetags"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>

<div id="${cityData.name}"
     class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
    <div class="ferry-bx">
        <c:if test="${not empty cityData.normalImage}">
            <img class='img-fluid  img-w-h js-responsive-image' alt='${cityData.normalImage.altText}' title='${cityData.normalImage.altText}'
                 data-media='${cityData.normalImage.url}'/>
        </c:if>
    <div class="promo-cards">
      <div class="card-body">
        <h3>${cityData.name}</h3>
        <div class="card-text">${cityData.description}</div>
        <div class="card-link">
            <a href="${pageContext.request.contextPath}/destinations/${cityData.name}/${cityData.code}"><spring:theme code="text.package.listing.learn.more" text="Learn more"/><i class="far bcf bcf-icon-right-arrow"></i></a></div>
      </div>
      </div>
    
    </div>
</div>
