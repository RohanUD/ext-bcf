<%@ tag language="java" pageEncoding="UTF-8"%>
<%@ tag body-content="empty" trimDirectiveWhitespaces="true"%>
<%@ taglib prefix="article" tagdir="/WEB-INF/tags/addons/bcfstorefrontaddon/responsive/article"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ attribute name="ticketsList" required="true" type="java.util.List"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="format" tagdir="/WEB-INF/tags/shared/format"%>
<%@ taglib prefix="ycommerce" uri="http://hybris.com/tld/ycommercetags"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<c:if test="${fn:length(ticketsList) gt 0}">
   <table>
     <thead>
        <tr class="account-orderhistory-table-head responsive-table-head hidden-xs">
           <th id="header1">
              <spring:theme code='text.get.quote.code'/>
           </th>
           <th id="header2">
              <spring:theme code='text.get.quote.contact.number'/>
           </th>
           <th id="header3">
              <spring:theme code='text.get.quote.email'/>
           </th>
           <th id="header4">
              <spring:theme code='status.search.departure.date'/>
           </th>
           <th id="header5">
              <spring:theme code='text.get.quote.date'/>
           </th>
           <th id="header6">
              <spring:theme code='text.get.quote.assignee'/>
           </th>
           <th id="header7">
              <spring:theme code='text.get.quote.more.Info'/>
           </th>
        </tr>
     </thead>
     <tbody>
        <c:forEach var="ticket" items="${ticketsList}" varStatus='idx'>
           <tr>
              <td>${ticket.code}</td>
              <td>${ticket.customerContactNumber}</td>
              <td>${ticket.customerEmail}</td>
               <c:set var="checkInDate" value="${fn:substringBefore(fn:substringAfter(ticket.details, 'CheckInDateTime:'),',')}"></c:set>
              <td>${checkInDate}</td>
              <td>
                 <fmt:formatDate value="${ticket.creationTime}" dateStyle="medium" timeStyle="short" type="both"/>
              </td>
              <td>${ticket.assignee.name}</td>
              <td>
                 <a href="#" class="getAQuoteMoreInfo">More Info</a>
                 <div id="dialog_${idx.index}" class="ui-widget hidden dialog-Message">
                    <table>
                       <thead>
                          <h1>
                             <spring:theme code="text.get.quote.customer.details"/>
                          </h1>
                       </thead>
                       <tbody>
                          <tr>
                             <td>
                                <spring:theme code="text.get.quote.customer.name"/>
                             </td>
                             <td>
                                ${ticket.customerName}
                             </td>
                           </tr>
                          <tr>
                             <td>
                                <spring:theme code="text.get.quote.customer.phone.number"/>
                             </td>
                             <td>
                                ${ticket.customerContactNumber}
                             </td>
                           </tr>
                          <tr>
                             <td>
                                <spring:theme code="text.get.quote.customer.email"/>
                             </td>
                             <td>
                                ${ticket.customerEmail}
                             </td>
                           </tr>
                          <tr>
                             <td>
                                <spring:theme code="text.get.quote.customer.product.name"/>
                             </td>
                             <td>
                                <c:set var="productName" value="${fn:substringBefore(fn:substringAfter(ticket.details, 'ProductName:'),',')}"></c:set>
                                ${productName}
                             </td>
                           </tr>
                          <tr>
                             <td>
                                <spring:theme code="text.get.quote.customer.travel.info"/>
                             </td>
                             <td>
                                <c:set var="queryDetails" value="${fn:substringBefore(ticket.details, 'ProductName:')}"></c:set>
                                <c:set var="details" value="${fn:substring(queryDetails,0, fn:length(queryDetails)-2)}"></c:set>
                                ${details}
                             </td>
                          </tr>
                       </tbody>
                    </table>
                 </div>
              </td>
             <td>
             <c:if test="${ticket.ticketStatusCode ne 'InProgress'}">
                <a href="#" class="y_changeStatusResult inProgress" data-status="INPROGRESS" data-code="${ticket.code}">
                   <span class="y_showInProgress">
                      <spring:theme code="text.get.quote.listing.inprogress" text="In Progress" />
                   </span>
                </a>
                </c:if>
                <c:if test="${ticket.ticketStatusCode eq 'InProgress' && ticket.isClosed eq true || ticket.ticketStatusCode ne 'InProgress'}">
                <a href="#" class="y_changeStatusResult closed" data-status="CLOSED" data-code="${ticket.code}">
                   <span class="y_showClosed">
                      <spring:theme code="text.get.quote.listing.close" text="Close" />
                   </span>
                </a>
                </c:if>
             </td>
           </tr>
        </c:forEach>
     </tbody>
  </table>
</c:if>
