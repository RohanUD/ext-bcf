<%@ tag language="java" pageEncoding="UTF-8"%>
<%@ tag body-content="empty" trimDirectiveWhitespaces="true"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ attribute name="transportVehicle" required="true" type="com.bcf.facades.ship.BcfShipInfoData"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="format" tagdir="/WEB-INF/tags/shared/format"%>
<%@ taglib prefix="ycommerce" uri="http://hybris.com/tld/ycommercetags"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>

<c:url var="ferryDetailsPageUrl" value="/on-the-ferry/our-fleet"/>

<div id="${transportVehicle.code}"
     class="col-lg-6 col-md-6 col-sm-6 col-xs-12">

    <c:choose>
        <c:when test="${not empty transportVehicle.name}">
            <c:set var="ferryName" value="${transportVehicle.name}"/>
        </c:when>
        <c:otherwise>
            <c:set var="ferryName" value="${transportVehicle.code}"/>
        </c:otherwise>
    </c:choose>

    <div class="ferry-bx">
        <c:if test="${not empty transportVehicle.image}">
            <a href="${ferryDetailsPageUrl}/${ferryName}/${transportVehicle.code}">
                <img class='img-fluid  img-w-h js-responsive-image' alt='${transportVehicle.imageAltText}' title='${transportVehicle.imageAltText}'
                     data-media='${transportVehicle.image}'/>
            </a>
        </c:if>
        <div class="home-bx-text">
            <p class="grey-text text-uppercase mb-1">
                <spring:theme code="text.ferry.lisiting.ferry.label"
                              text="FERRY"/>
            </p>
            <p class="font-size-17">
                <a href="${ferryDetailsPageUrl}/${ferryName}/${transportVehicle.code}">
                    <strong>${ferryName}</strong>
                    <i class="far bcf bcf-icon-right-arrow"></i>
                </a>
            </p>
            <p>
                ${transportVehicle.shipDescription}
            </p>
        </div>
    </div>
</div>
