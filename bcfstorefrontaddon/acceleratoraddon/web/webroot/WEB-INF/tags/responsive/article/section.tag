<%@ tag language="java" pageEncoding="UTF-8"%>
<%@ tag body-content="empty" trimDirectiveWhitespaces="true"%>
<%@ taglib prefix="article" tagdir="/WEB-INF/tags/addons/bcfstorefrontaddon/responsive/article"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ attribute name="sections" required="true" type="java.util.List"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="format" tagdir="/WEB-INF/tags/shared/format"%>
<%@ taglib prefix="ycommerce" uri="http://hybris.com/tld/ycommercetags"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<c:if test="${fn:length(sections) gt 0}">
	<c:forEach items="${sections}" var="section" varStatus="status">

		<c:set var="alignment" value="${status.index % 2 != 0 ? 'right' : 'left' }" />
		<div class="section ${alignment }">
			<c:set var="secStyle" value="${empty section.titleStyle ? 'section-title' : section.titleStyle}" />
			<div class="${secStyle}">${fn:escapeXml(section.title)}</div>
			<div class="section-content">
				<span class="section-description">${section.shortDescription}</span>
					<div class="">
						${section.longDescription}</span>
					</div>
			</div>
		</div>
		
		
		
	</c:forEach>
</c:if>

