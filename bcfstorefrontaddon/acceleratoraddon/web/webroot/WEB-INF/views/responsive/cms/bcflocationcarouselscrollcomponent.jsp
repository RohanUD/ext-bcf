<%@ page trimDirectiveWhitespaces="true" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="cms" uri="http://hybris.com/tld/cmstags" %>
<div class="container">
	<div class="row flexbox-row">
		${headingText}
		<div class="col-lg-12 col-md-12 col-xs-12 flexbox-col">
			<div class="owl-carousel owl-theme js-owl-carousel js-owl-default"
				data-count="${displayCount}">
				<c:forEach items="${locations}" var="location">
					<cms:component component="${location}" evaluateRestriction="true" />
				</c:forEach>
			</div>
		</div>
	</div>
</div>


