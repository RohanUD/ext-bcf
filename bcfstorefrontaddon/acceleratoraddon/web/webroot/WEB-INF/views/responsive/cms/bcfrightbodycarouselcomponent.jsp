<%@ page trimDirectiveWhitespaces="true" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="cms" uri="http://hybris.com/tld/cmstags" %>

<div class="container">
    <div class="row">
    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
     <div class="card discover-box discover-box-left-img experience-details-carousel" style="background:${backgroundColour}">
         <div class="row card-no-body">
         	<div class="col-lg-4 col-md-4 col-sm-6 col-xs-12 card-media-wrapper card-media-left">
                   <c:if test="${not empty dataMedia}">
	                     <div class="p-relative">
		                    <img class='img-fluid  img-w-h js-responsive-image testimonial-img' alt='${altText}' title='${altText}' data-media='${dataMedia}'/>
		                    <div class="location-bx">
	                          ${location}<br />
	                          ${caption}
	                       </div>
	                  	</div>
                     </c:if>
             </div>
             <div class="col-lg-8 col-md-8 col-sm-5 col-xs-12">
                  <div class="card-body">
                    <div class="owl-carousel js-owl-carousel js-owl-rotating-gallery">
                             <c:if test="${not empty paragraphs}">
                                <c:forEach items="${paragraphs}" var="paragraph">
                                    <div class="owl-item">
                                        <cms:component component="${paragraph}" evaluateRestriction="true" />
                                    </div>
                                </c:forEach>
                             </c:if>
                          </div>
                  </div>
             </div>
            </div>
         </div>
         </div>
   </div>
</div>


