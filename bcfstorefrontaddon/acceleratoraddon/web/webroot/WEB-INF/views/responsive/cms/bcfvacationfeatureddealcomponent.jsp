<%@ page trimDirectiveWhitespaces="true" %>
<%@ taglib prefix="cms" uri="http://hybris.com/tld/cmstags" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<spring:htmlEscape defaultHtmlEscape="false"/>
<div class="container">
		<c:if test="${not empty dataMedia}">
		        <div class="row">
		        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div class="card offer-box" style="background:${backgroundColour}">
                    <c:if test="${not empty featuredText}">
                        <div class="featured-bx">${featuredText}</div>
                    </c:if>
                          <div class="card-no-body row">
                              <div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
                                <div class="card-body">
                                  <div class="deal-info">${topText}</div>
                                  <c:if test="${not empty dealNameImage}">
                                    <div class="margin-top-10 margin-bottom-10">
                                       <img class='img-fluid js-responsive-image' data-media='${dealNameImage}' />
                                    </div>
                                  </c:if>
                                  <div class="card-title"><h2 class="white-text">${promotionName}</h2></div>
                                  <div class="card-text white-text">${promotionDesc}</div>
                                  <c:if test="${not empty link}">
                                        <button class="btn btn-default">
                                            <cms:component component="${link}" evaluateRestriction="true" />
                                        </button>
                                    </c:if>
                                </div>
                              </div>
                              <div class="col-lg-8 col-xs-12 card-media-wrapper card-media-right">
                                  <img class='img-fluid js-responsive-image' data-media='${dataMedia}' alt='${altText}' title='${altText}' />
                                    <div class="location-bx">
                                           ${location} <br />
                                           ${caption}
                                    </div>
                               <c:if test="${not empty promotionText}">
                                  <div class="from-circle">
                                      <span class="text-light-blue"> ${promotionText}</span>
                                  </div>
	                           </c:if>
                              </div>
                           </div>
                    </div>
                   </div>
                    
				</div>
		</c:if>
</div>
