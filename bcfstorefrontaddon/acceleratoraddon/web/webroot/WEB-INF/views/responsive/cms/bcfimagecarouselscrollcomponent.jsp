<%@ page trimDirectiveWhitespaces="true" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="cms" uri="http://hybris.com/tld/cmstags" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>

	<div class="row">
		<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
			${headingText}</div>
	</div>

<div class="accommodation-image show-silder-count image-border slide-no-${fn:length(images)} owl-item">
<div class="owl-carousel-content">
   <div class="owl-carousel owl-theme js-owl-carousel js-owl-rotating-gallery" data-count="${displayCount}">
      <c:forEach items="${images}" var="image">
         <cms:component component="${image}" evaluateRestriction="true" />
      </c:forEach>
      
   </div>
</div>
</div>