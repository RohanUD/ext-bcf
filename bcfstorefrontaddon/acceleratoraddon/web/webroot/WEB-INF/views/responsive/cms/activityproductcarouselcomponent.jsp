<%@ page trimDirectiveWhitespaces="true"%>
<%@ taglib prefix="cms" uri="http://hybris.com/tld/cmstags"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<spring:htmlEscape defaultHtmlEscape="false" />
<c:if test="${not empty products}">
<div class="container">
	<div class="row">
		<c:forEach items="${products}" var="product">
			<div class="col-lg-4 col-md-4 col-sm-12  col-xs-12">
				<div class="card media-top vacation-deal-component" style="background:${backgroundColour}">
			        <div class="featured-box-main">
				       <div class="featured-bx">${product.promoText}</div>
			        </div>
			        <div class="p-relative">
			        	<img class='img-fluid  img-w-h js-responsive-image' alt='${altText}'  title='${altText}' data-media='${product.dataMedia}' />
			         </div>
					<div class="promo-cards">
					      <div class="card-body">
					      <p><spring:theme code="label.ourFerry.page.activity" text="ACTIVITY" /></p>
					        <h3 class="card-title">${product.name}</h3>
					        <div class="card-link">
					            <spring:theme code="label.ourFerry.page.view.details" text="View details" />
								<i class="fas bcf bcf-icon-right-arrow"></i>
					        </div>
					      </div>
			            <div class="package-circle">
			            		<span class="save-from-text"><spring:theme code="label.ourFerry.page.from" text="FROM" /></span>
			                <span class="text-dark-blue price"> ${product.price.basePrice.value}</span>
			            </div>
			      </div>
	    		</div>	
			</div>
		</c:forEach>
	</div>
	</div>
	<div class="row">
		<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 text-center">
			<button class="btn btn-primary"
				type="submit" id="fareFinderFindButton" value="Submit">
				<spring:theme code="text.view.activities" text="View all activities" />
			</button>


		</div>
	</div>

</c:if>
