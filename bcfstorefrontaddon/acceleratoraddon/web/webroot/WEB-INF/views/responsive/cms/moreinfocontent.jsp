<%@ page trimDirectiveWhitespaces="true" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="cms" uri="http://hybris.com/tld/cmstags" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<div class="container">
<c:if test="${not empty title}">
	<h4 class="margin-top-20 margin-bottom-20"><b><c:out value="${title}" /></b></h4>
</c:if>
<div class="row information-div margin-bottom-30">
	<c:forEach items="${links}" var="link">
		<div class="col-lg-4 col-sm-6 col-xs-12  img-icon-width">
			<div class="m-info">
				<cms:component component="${link}" evaluateRestriction="true" />
			</div>
		</div>
	</c:forEach>
</div>
</div>

