<%@ page trimDirectiveWhitespaces="true" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix = "fmt" uri = "http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="bcfcms" tagdir="/WEB-INF/tags/addons/bcfstorefrontaddon/responsive/cms"%>

<bcfcms:displayCount displayCount="${displayCount}"/>

<div class="container">
<div class="row margin-top-30 margin-bottom-20">
  <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
    <h2><spring:theme code="text.newsroom.title" text="Newsroom"/></h2>
  </div>
  <div class="col-lg-6 col-md-6 col-sm-6 text-right">
    <a href="${morePressReleases.url}">${morePressReleases.linkName} <i class="bcf bcf-icon-right-arrow arrow-sm"></i></a>
  </div>
</div>

<div class="row margin-top-20 margin-bottom-30">
  <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
      <div class="owl-carousel owl-theme js-owl-carousel js-owl-news-carousel" data-count="${displayCount}">
        <c:forEach items="${news}" var="newsItem">
          <div class="item ">
            <div class="news-bx">
              <p class="grey-text"><fmt:formatDate pattern = "MMMM dd, yyyy" value = "${newsItem.publishedDate}" />  </p>
              <h5 class="text-blue">   ${newsItem.title} </h5>
              <c:if test="${not empty newsItem.travelRoutes}">
                <c:forEach items="${newsItem.travelRoutes}" var="travelRoute">
                    <c:set var="travelRouteCode" value="${travelRoute.routeCode}"/>
                </c:forEach>
              </c:if>
              <p><a href="${request.contextPath}${newsItem.pageUrl}"><spring:theme code="text.newsroom.readmore" text="Read more"/> <i class="bcf bcf-icon-right-arrow"></i> </a></p>
            </div>
          </div>
        </c:forEach>
      </div>
  </div>
  <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12 hidden-md hidden-lg">
   <p class=" text-center"> <a href="${morePressReleases.url}"><b>${morePressReleases.linkName}</b></a></p>
 </div>
</div>
</div>

