<%@ page trimDirectiveWhitespaces="true"%>
<%@ taglib prefix="cms" uri="http://hybris.com/tld/cmstags"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<spring:htmlEscape defaultHtmlEscape="false" />

<div class="item location-scroll-view-component">
    <div class="card media-top " style="background:${backgroundColour}">
     <img class='img-fluid js-responsive-image' data-media='${dataMedia}' alt='${altText}' title='${altText}' />
      <div class="promo-cards-IIII">
      <div class="card-body">
      <h3 class="card-title">${location}</h3>
        <c:if test="${not empty link}">
				<cms:component component="${link}" evaluateRestriction="true" />
		</c:if>
      </div>
    </div>
 </div>
</div>
