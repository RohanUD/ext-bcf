<%@ page trimDirectiveWhitespaces="true"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="cms" uri="http://hybris.com/tld/cmstags" %>

<div class="item">
    <div class="card media-top" style="background:${backgroundColour}">
    <div class="p-relative">
     	<img class='img-fluid js-responsive-image' data-media='${dataMedia}' alt='${altText}' title='${altText}' />
	    <div class="location-bx">
	          ${location}<br/>
	           ${caption}
	    </div>
    </div>
    <div class="promo-cards">
      <div class="card-body">
      <h3>${title}</h3>
        <div class="card-text"><p class="p-ellipsis">${description}</p></div>
        
        <c:if test="${not empty link}">
          <cms:component component="${link}" evaluateRestriction="true" />
          </c:if>
          
      </div>
      <c:if test="${not empty overlayText}">
             <div class="package-circle-blue">
                    ${promoText}
             </div>
        </c:if>
      </div>
    </div>
</div>
