<%@ page trimDirectiveWhitespaces="true" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="cms" uri="http://hybris.com/tld/cmstags" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<c:if test="${not empty title}">
	<div class="container">
		<div class="row">
			<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
				<h2>
					<c:out value="${title}" />
				</h2>
			</div>
		</div>
	</div>
</c:if>
<div class="container">
    <div class="row">
        <div class="btn-group navigation-button-group btn-group-lg col-lg-12" role="group" aria-label="navigation
        button"
        data-count="${fn:length(links)}">
        <fmt:parseNumber var = "i" type = "number" value = "${12/fn:length(links)}" />
            <c:forEach items="${links}" var="link" varStatus="linkIndex">
             <button type="button" class="btn btn-nav no-padding-m col-lg-${i} col-md-${i} col-sm-6 col-xs-12"><cms:component component="${link}"
             evaluateRestriction="true"
             />
            </button>
            </c:forEach>
        </div>
    </div>
</div>