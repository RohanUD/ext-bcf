<%-- <%@ page trimDirectiveWhitespaces="true"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="cms" uri="http://hybris.com/tld/cmstags"%>
<%@ taglib prefix="footer" tagdir="/WEB-INF/tags/responsive/common/footer"%>

<div class="container">
<div class="row">
      <div class="grey-bg">
        <div class="col-sm-10 offset-1">
          <div class="row">
            <ul class="col-sm-3 text-center">
              <li><a href="#">Sailing Status</a></li>
              <li><a href="#">Manage Bookings</a></li>
              <li><a href="#">Service Notices</a></li>
              <li><a href="#">Gift Certificates</a></li>
            </ul>
            <ul class="col-sm-3 text-center border-left">
              <li><a href="#">About Us</a></li>
              <li><a href="#">Careers</a></li>
              <li><a href="#">Contact Us</a></li>
              <li><a href="#">Subscribe to Newsletter</a></li>
            </ul>
            
            <ol class="col-sm-3 list-unstyled d-flex align-items-start mt-4">
              <li><a href="#"><i class="fa fa-twitter" aria-hidden="true"></i></a></li>
              <li><a href="#"><i class="fa fa-facebook" aria-hidden="true"></i></a></li>
              <li><a href="#"><i class="fa fa-instagram" aria-hidden="true"></i></a></li>
              <li><a href="#"><i class="fa fa-google-plus" aria-hidden="true"></i></a></li>
              <li><a href="#"><i class="fa fa-tripadvisor" aria-hidden="true"></i></a></li>
            </ol>
          </div>
        </div>
      
        <nav>
		<ul class="flexbox row">
			<li class="col-xs-6">
				<small class="copyright">${fn:escapeXml(notice)}</small>&nbsp;&nbsp;&nbsp;
			</li>
			<c:forEach items="${navigationNodes}" var="node">
				<c:if test="${node.visible}">&nbsp;&nbsp;&nbsp;
					<c:forEach items="${node.links}" step="${wrapAfter}" varStatus="i">&nbsp;&nbsp;&nbsp;
						<c:forEach items="${node.links}" var="childlink" begin="${i.index}" end="${i.index + wrapAfter - 1}">
							<cms:component component="${childlink}" evaluateRestriction="true" element="li" class="col-xs-6" />
						</c:forEach>
					</c:forEach>
				</c:if>
			</c:forEach>
		</ul>
	</nav>
	
      </div>
    </div>
  </div>
  
 --%>
<%@ page trimDirectiveWhitespaces="true"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="cms" uri="http://hybris.com/tld/cmstags"%>
<%@ taglib prefix="footer" tagdir="/WEB-INF/tags/responsive/common/footer"  %>

 
 <footer class="footer-wrapper d-block d-sm-none">

    <div class="row">
      <div class="grey-bg">
        <div class="col-sm-10 offset-1">
         <center>
        <h5 class="blue-color font-weight-bold"> Our Company </h5>
         <h5 class="blue-color font-weight-bold mt-4"> Business Customers </h5>
			 <hr>
			  <h5 class="blue-color font-weight-bold mt-4"> <i class="fa fa-credit-card" aria-hidden="true"></i> Experience Card and ALT Card </h5>
			   <h5 class="blue-color font-weight-bold mt-4"><i class="fa fa-question-circle" aria-hidden="true"></i> Business Customers </h5>
			   
			 <h5 class="mt-4">  Join the Conversation</h5>
			   
			</center>
          <div class="row">
            
           
            
            <ol class="col-sm-3 list-unstyled d-flex align-items-start mt-2">
              <li><a href="#"><i class="fa fa-twitter" aria-hidden="true"></i></a></li>
              <li><a href="#"><i class="fa fa-facebook" aria-hidden="true"></i></a></li>
              <li><a href="#"><i class="fa fa-instagram" aria-hidden="true"></i></a></li>
              <li><a href="#"><i class="fa fa-google-plus" aria-hidden="true"></i></a></li>
              <li><a href="#"><i class="fa fa-tripadvisor" aria-hidden="true"></i></a></li>
            </ol>
          </div>
        </div>
      </div>
    </div>
  
 
  
  
   <h5 class="text-center blue-color">Terms &amp; Conditions | Privacy Policy <br> <br> Site Map </h5>
   <h5 class="text-center">Copyright � 2018</h5>
</footer>

 
            
 <div class="clearfix"></div>
 <footer class="footer-wrapper d-none d-sm-block">
    <div class="row">
      <div class="grey-bg">
        <div class="col-sm-12">
          <div class="row">
          <div class="col-sm-9">
            <ul class="count-c">
              <c:forEach items="${navigationNodes}" var="node">
             
				<c:if test="${node.visible}">
					<c:forEach items="${node.links}" step="${wrapAfter}" varStatus="i">
						<c:forEach items="${node.links}" var="childlink" begin="${i.index}" end="${i.index + wrapAfter - 1}">
						<cms:component component="${childlink}" evaluateRestriction="true" element="li" class="border-right" />
						</c:forEach>
					</c:forEach>
				</c:if>
			</c:forEach>
            </ul>
            </div>
            
            
             <div class="col-sm-3">
              <h5>Join The Conversation </h5>
            <ol class="list-unstyled d-flex align-items-start mt-4">
           
              <li><a href="#"><i class="fa fa-twitter" aria-hidden="true"></i></a></li>
              <li><a href="#"><i class="fa fa-facebook" aria-hidden="true"></i></a></li>
              <li><a href="#"><i class="fa fa-instagram" aria-hidden="true"></i></a></li>
              <li><a href="#"><i class="fa fa-google-plus" aria-hidden="true"></i></a></li>
              <li><a href="#"><i class="fa fa-tripadvisor" aria-hidden="true"></i></a></li>
            </ol>            
            <div class="footer__right col-xs-12 col-md-10">
                <c:if test="${showLanguageCurrency}">
                    <div class="row">
                        <div class="col-xs-10 col-md-10 footer__dropdown">
                            <footer:languageSelector languages="${languages}"
                                                     currentLanguage="${currentLanguage}" />
                        </div>
                        <div class="col-xs-6 col-md-6 footer__dropdown">
                            <footer:currencySelector currencies="${currencies}"
                                                     currentCurrency="${currentCurrency}" />
                        </div>
                    </div>
                </c:if>
            </div>    
            </div>          
          </div>
        </div>
      </div>
    </div>
  
  <!-- <div class="container">
    <nav>
      <ul class="bottom-bar row offset-3">
        <li> <small class="copyright">Copyright � 2018</small> </li>
         <li>  <a href="#" title="FAQ's">FAQ's</a></li>
        <li>  <a href="#" title="Privacy Policy">Privacy Policy</a></li>
         <li> <a href="#" title="Terms &amp; Conditions">Terms &amp; Conditions</a></li>
        <li>  <a href="#" title="Help">Help</a></li>
        <li> <a href="#" title="Contact Us">Contact Us</a></li>
      </ul>
    </nav>
  </div> -->
    
  
    <nav>
 <ul class="flexbox row">
			<li class="col-xs-6">
			<small class="copyright">${fn:escapeXml(notice)}</small>
			</li>
			
		</ul>
		</nav>
		
</footer>