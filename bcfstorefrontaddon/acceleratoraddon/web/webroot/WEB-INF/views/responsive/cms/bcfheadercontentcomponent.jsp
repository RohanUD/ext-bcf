<%@ page trimDirectiveWhitespaces="true"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="cms" uri="http://hybris.com/tld/cmstags"%>
<div class="container">
	<div class="row">
		<div class="col-sm-12">
			<h1 class="mb-4 mt-4">${title}</h1>
		</div>
	</div>
	<div class="row">
		<c:if test="${not empty dataMedia}">
			<div class="col-md-12">
				<img class='img-fluid  img-w-h js-responsive-image' alt='${altText}'
					title='${altText}' data-media='${dataMedia}' />
                <div class="location-bx">
                       <p class="card-text"> ${location}</p>
                       <p class="card-text">${caption}</p>
                </div>
			</div>
		</c:if>
	</div>
</div>
