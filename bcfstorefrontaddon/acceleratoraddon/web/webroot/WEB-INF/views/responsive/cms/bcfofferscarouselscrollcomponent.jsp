<%@ page trimDirectiveWhitespaces="true" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="cms" uri="http://hybris.com/tld/cmstags" %>
<%@ taglib prefix="packagehotellisting" tagdir="/WEB-INF/tags/addons/bcfcommonsaddon/responsive/packagehotellisting"%>

<div class="container">
	<div class="row offers-scrolled-component">
	    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
	    <c:if test="${not empty headingText}">
	      ${headingText}
	    </c:if>
	    </div>
		<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
			
            <c:choose>
                <c:when test="${not empty propertyDataList}">
                    <div id="y_packageResults" class=" deal-items deal-row" aria-label="package search results">
                        <packagehotellisting:packageHotelListView propertiesListParams="${propertyDataList}" />
                    </div>
                </c:when>
                <c:otherwise>
                    <c:if test="${not empty offers}">
                        <div class="heading-conten">
                            <div class="owl-carousel owl-theme js-owl-carousel js-owl-default" data-count="${displayCount}">
                                <c:forEach items="${offers}" var="offer">
                                    <cms:component component="${offer}" evaluateRestriction="true" />
                                </c:forEach>
                            </div>
                        </div>
                    </c:if>
                </c:otherwise>
            </c:choose>
		</div>
	</div>
</div>
