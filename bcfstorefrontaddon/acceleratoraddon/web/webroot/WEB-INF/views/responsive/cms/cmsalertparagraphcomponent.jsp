<%@ page trimDirectiveWhitespaces="true"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="cms" uri="http://hybris.com/tld/cmstags"%>

<c:if test="${DISPLAY_ALERT}">
	<div>
		<div class="text-center">
			<div class="alert alert-warning cookies-msg-hide" id="js-alert-msg-hide">
			<span><c:if test="${not empty component.title}">
				<h3>${component.title} </h3>
			</c:if></span>
					${component.content} <span class="alert-btn-span">
					<button class="btn btn-transparent-white alert-btn" data-dismiss="alert" aria-label="Close">Close</button>
				</span>
			</div>
		</div>
		<div class="clearfix"></div>
	</div>
</c:if>
