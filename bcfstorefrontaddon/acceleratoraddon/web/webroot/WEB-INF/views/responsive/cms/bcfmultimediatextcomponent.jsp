<%@ page trimDirectiveWhitespaces="true" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="format" tagdir="/WEB-INF/tags/addons/bcfstorefrontaddon/responsive/format" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<spring:htmlEscape defaultHtmlEscape="false"/>

<div class="bcfMultiMediaTextComponent">
    <input type="hidden" value='${data_media1}' class="dataMedia1"/>
    <input type="hidden" value='${data_media2}' class="dataMedia2"/>
    <input type="hidden" value='${data_media3}' class="dataMedia3"/>
    <input type="hidden" value='${data_media4}' class="dataMedia4"/>
    <input type="hidden" value='${data_media5}' class="dataMedia5"/>

    <input type="hidden" value='${altText_media1}' class="altText1"/>
    <input type="hidden" value='${altText_media2}' class="altText2"/>
    <input type="hidden" value='${altText_media3}' class="altText3"/>
    <input type="hidden" value='${altText_media4}' class="altText4"/>
    <input type="hidden" value='${altText_media5}' class="altText5"/>

    <input type="hidden" value='${location_media1}' class="location1"/>
    <input type="hidden" value='${location_media2}' class="location2"/>
    <input type="hidden" value='${location_media3}' class="location3"/>
    <input type="hidden" value='${location_media4}' class="location4"/>
    <input type="hidden" value='${location_media5}' class="location5"/>

    <input type="hidden" value='${caption_media1}' class="caption1"/>
    <input type="hidden" value='${caption_media2}' class="caption2"/>
    <input type="hidden" value='${caption_media3}' class="caption3"/>
    <input type="hidden" value='${caption_media4}' class="caption4"/>
    <input type="hidden" value='${caption_media5}' class="caption5"/>

    ${content}
</div>
