<%@ page trimDirectiveWhitespaces="true"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="cms" uri="http://hybris.com/tld/cmstags"%>
<%@ taglib prefix="bcfcms" tagdir="/WEB-INF/tags/addons/bcfstorefrontaddon/responsive/cms"%>

<bcfcms:displayCount displayCount="${displayCount}"/>

<div class="container">
	<div class="row">
		<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
			${headingText}</div>
	</div>
</div>

<div class="container grouped-scroll-component">
	<div class="owl-slider-content hidden-xs hidden-sm">
		<div class="row">
			<c:forEach items="${images}" var="image" varStatus="status">
				<div class="${colWidthClass}">
					<cms:component component="${image}" evaluateRestriction="true" />
				</div>
				<c:if test="${(status.index + 1) % displayCount eq 0}">
                    </div>
                    <div class="row">
			    </c:if>
			</c:forEach>
		</div>
	</div>

	<div class="owl-slider-content hidden-lg hidden-md">
	<div class="owl-carousel owl-theme js-owl-carousel js-owl-content" data-count="${displayCount}">
		<c:forEach items="${images}" var="image">
			<cms:component component="${image}" evaluateRestriction="true" />
		</c:forEach>
	</div>
</div>
</div>
