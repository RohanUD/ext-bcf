<%@ page trimDirectiveWhitespaces="true" %>
<%@ taglib prefix="cms" uri="http://hybris.com/tld/cmstags" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<spring:htmlEscape defaultHtmlEscape="false"/>
<div class="vacation-detail location-group-view-component clearfix">
	<div class="card" style="background:${backgroundColour}">
		<c:if test="${not empty dataMedia}">
				<img class='js-responsive-image card-img' data-media='${dataMedia}' alt='${altText}' title='${altText}' />
		</c:if>
	  <div class="anchorButton">
	    <button class="btn btn-primary" type="button">
        <c:if test="${not empty link}">
                <cms:component component="${link}" evaluateRestriction="true" />
            </c:if>
		</button>
	  </div>
	</div>
</div>

