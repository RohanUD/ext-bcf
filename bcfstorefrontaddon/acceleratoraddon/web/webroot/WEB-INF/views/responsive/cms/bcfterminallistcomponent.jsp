<%@ page trimDirectiveWhitespaces="true"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="cms" uri="http://hybris.com/tld/cmstags"%>
<%@ taglib prefix="ycommerce" uri="http://hybris.com/tld/ycommercetags"%>
<%@ taglib prefix="terminals" tagdir="/WEB-INF/tags/addons/bcfstorefrontaddon/responsive/terminallisting"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="format" tagdir="/WEB-INF/tags/shared/format"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ page language="java" contentType="text/html;charset=UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@ taglib prefix="pagination" tagdir="/WEB-INF/tags/responsive/pagination"%>

<c:url var="terminalDetailsUrl" value="/travel-boarding/terminal-directions-parking-food/"/>
<c:url var="conditionUrl" value="/majorTerminals?terminalCode="/>
<spring:htmlEscape defaultHtmlEscape="false" />
<div class="container">
	<div class="row">
		<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
			<h2 class="page-header"><spring:theme code="label.our.terminals.title" text="Our Terminals" /></h2>
		</div>
	</div>
</div>

<div class="container-fluid px-0  py-4 bg-light-gray padding-top-20 padding-bottom-30 margin-bottom-30">
   <div class="container">
		<div class="row">
			<div class="col-lg-8 col-md-8 col-sm-8 col-xs-12">
				<h5 class="mb-3"><spring:theme code="label.our.terminals.find.a.terminal" text="Find a terminal"/></h5>
			</div>
		</div>
      <div class="row">
         <div class="col-lg-8 col-md-8 col-sm-8 col-xs-12">
           <select class="form-control selectpicker"
              name='terminalFilter' id="terminalFilter">
              <option value="ALL">
                 <spring:theme code="label.our.terminals.make.selection" text="Select a terminal"/>
              </option>
              <c:forEach var="terminal" items="${terminals}">
                 <option data-defaultvalue="${terminal.routeName}" name="${terminal.routeName}" value="${terminal.code}">
                    ${terminal.name}
                 </option>
              </c:forEach>
           </select>
           
         </div>
         <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
         	<input type="button" class="btn btn-primary btn-block y_navigateToTerminalDetails" value='<spring:theme code="text.our.terminals.find.terminal" text="Find ferry" />' />
         </div>
      </div>
   </div>
</div>
<div class="container">
	<div class="row">
		<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
			<h4><spring:theme
						code="label.our.terminals.all.regions" text="All regions" />
			</h4>
		</div>
		<div class="col-lg-4 col-md-offset-8">
			<label><spring:theme code="label.our.terminals.view"
					text="View" /></label>
		</div>
	</div>
</div>


<div class="container">
		<div id="tabs" class="terminal-listing">
		<div class="row">
			<div class="col-lg-4 col-md-offset-8 terminal-listing-tab">
				<ul>
					<li><a href="#tabs-1"> <spring:theme
								code="label.our.terminals.list" text="List" />
					</a></li>
					<li><a href="#tabs-2" id="tabToTerminalListingMapClick" onclick=" return false"> <spring:theme
								code="label.our.terminals.map" text="Map" />
					</a></li>
				</ul>
			</div>
		</div>
			<div id="tabs-1">
				<div id="terminal_details">
					<div class="terminal_details_placeholder row margin-bottom-40">
						<terminals:terminals transportfacilities="${results}"/>
					</div>
                    <pagination:globalpagination/>
				</div>
			</div>
			<div id="tabs-2">
                    <c:if test="${not empty travelRouteDataList}">
                    <c:set var="googleApiKey" value="${jalosession.tenant.config.getParameter('google.api.key')}"/>
                    <c:if test="${not empty googleApiKey}">
                           <div class="row">
                           <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 mb-4">
                               <div  class="map-wrap close-icon-manage  y_terminallistmapurl" id="terminallistmap"
                                data-googleapi="${googleApiKey}">
                        </div>
                        </div>
                        </div>
                    </c:if>
                    </c:if>
			</div>
		</div>
	</div>
	   <div id="listOfTravelRouteCount" value="${travelRouteDataList.size()}"></div>
	   <spring:message code="label.our.terminals.view.conditions" text="View terminal conditions" var="conditionContent"/>
       <c:forEach items="${travelRouteDataList}" var="travelRoute" varStatus="status">
       <c:set var="currentConditionUrl" value="'<spring:theme code='label.our.terminals.view.conditions' text='View terminal conditions'/>'"></c:set>

         <c:if test="${travelRoute.mapKmlFileUrl ne null}">
           <c:set var="originCurrentConditionUrl" value=""></c:set>
           <c:set var="destinationCurrentConditionUrl" value=""></c:set>
           <c:if test="${travelRoute.origin.currentConditionEnabled eq true}">
                <c:set var="originCurrentConditionUrl" value="<br><a href='${conditionUrl}${travelRoute.origin
                .code}'>${conditionContent}</a>"></c:set>
           </c:if>
           <c:if test="${travelRoute.destination.currentConditionEnabled eq true}">
               <c:set var="destinationCurrentConditionUrl" value="<br><a href='${conditionUrl}${travelRoute.destination
               .code}'>${conditionContent}</a>"></c:set>
          </c:if>
           <input type="hidden" id="routeOriginDestination${status.index}"
                data-mapkmlfile="${travelRoute.mapKmlFileUrl}" data-mapkmlinternalfilename="${travelRoute.origin.code}-${travelRoute.destination.code}"
                data-origincode="${travelRoute.origin.code}" data-destinationcode="${travelRoute.destination.code}"

                data-originurl="<p class='text-light text-uppercase mb-1'><spring:theme code='label.our.terminals.terminal'
                                text="TERMINAL"/></p><br><b>${travelRoute.origin.name}</b><br><hr class='light-border'/>
                                <p class='text-light text-uppercase mb-1'><spring:theme code='label.our.terminals.location'
                                text="LOCATION"/></p><br>${travelRoute.origin.pointOfServiceData.address.formattedAddress}
                                <br><a href='${terminalDetailsUrl}${travelRoute.origin.routeName}/${travelRoute.origin.code}'>
                                <spring:theme code='label.our.terminals.view.details' text="View terminal
                                details"/></a>${originCurrentConditionUrl}"

                data-destinationurl="<p class='text-light text-uppercase mb-1'><spring:theme code='label.our.terminals.terminal'
                                    text="TERMINAL"/></p><br><b>${travelRoute.destination.name}</b><br><hr class='light-border'/>
                                    <p class='text-light text-uppercase mb-1'><spring:theme code='label.our.terminals.location'
                                    text="LOCATION"/></p><br>${travelRoute.destination.pointOfServiceData.address.formattedAddress}
                                    <br><a href='${terminalDetailsUrl}${travelRoute.destination.routeName}/${travelRoute.destination.code}'>
                                    <spring:theme code='label.our.terminals.view.details' text="View terminal details"/></a>${destinationCurrentConditionUrl}"
                data-index="${status.index}" />
         </c:if>
       </c:forEach>
