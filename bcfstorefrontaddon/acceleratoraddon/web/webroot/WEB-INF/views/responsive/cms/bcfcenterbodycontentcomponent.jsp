<%@ page trimDirectiveWhitespaces="true" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="cms" uri="http://hybris.com/tld/cmstags" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<div class="container">
	<div class="row">
		<div class="col-md-12">
			<div class="component-cnrl">${content}</div>
		</div>
	</div>
	<c:if test="${not empty links}">
		<div class="row">
			<div class="col-md-12">
				<ul class="bcf-center-ul">
					<c:forEach items="${links}" var="link">
						<li><cms:component component="${link}" evaluateRestriction="true" /></li>
					</c:forEach>
				</ul>
			</div>
		</div>
	</c:if>
</div>
