<%@ page trimDirectiveWhitespaces="true" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@ taglib prefix="pagination" tagdir="/WEB-INF/tags/responsive/pagination"%>
<div class="container">
   ${headingText}
   <c:if test="${not empty propertyCategories}">
      <h2>
         <spring:theme code="label.vacation.listing.seemore" arguments="${propertyCategories}" />
      </h2>
   </c:if>
   <p class="margin-top-30">
      <b>
         <spring:theme code="text.cms.accommodationrefinement.filterby" text="Filter by" />
      </b>
   </p>
   <div class="row">
      <c:if test="${displayHotelsFacet eq 'true'}">
         <div class="col-md-6 col-sm-6 col-xs-12">
            <p class="text-grey">
               <small>
                  <spring:theme code="accommodation.search.facet.destination" text="Destination" />
               </small>
            </p>
            <form action="${requestScope['javax.servlet.forward.request_uri']}">
               <Select  class="selectpicker form-control"  name="destinationFilter" onChange="this.form.submit();">
                  <option value="all">
                     <spring:theme code="text.vacation.listing.all" text="All" />
                  </option>
                  <c:forEach items="${facetSearchPageData.facets}" var="facet">
                     <c:if test="${facet.code eq 'cityCode'}">
                        <c:forEach items="${facet.values}" var="facetValue">
                           <option value="${facetValue.code}" ${param.destinationFilter eq facetValue.code ? 'selected' : ''}>${facetValue.name}</option>
                        </c:forEach>
                     </c:if>
                  </c:forEach>
               </Select>
               <pagination:storeRequestParams currentField="destinationFilter"/>
            </form>
         </div>
      </c:if>
      <c:if test="${displayPropertyCategoryFacet eq 'true' && empty propertyCategories}">
         <div class="col-md-6 col-sm-6 col-xs-12 vac-package">
            <form action="${requestScope['javax.servlet.forward.request_uri']}">
               <c:forEach items="${facetSearchPageData.facets}" var="facet">
                  <c:if test="${facet.code eq 'propertyCategories'}">
                     <div class="filter-form clearfix col-lg-12 col-sm-12 col-md-12 col-xs-12 pr0">
                        <div class="filter-facets">
                           <div class="accommodations_select mb-4">
                              <ul class="nav nav-pills">
                                 <li role="presentation" class="dropdown filter-li">
                                    <p class="text-grey">
                                       <small>
                                          <spring:theme code="accommodation.search.facet.propertyCategoryTypes" text="Category" />
                                       </small>
                                    </p>
                                    <a data-toggle="dropdown" class="filter-btn" href="#" role="button" aria-haspopup="true" aria-expanded="false">
                                       <span class="totalCheckboxesChecked"></span>
                                       <spring:theme code="accommodation.search.facet.propertyCategoryTypes" text="Category" />
                                       <span class="caret"></span>
                                    </a>
                                    <ul class="dropdown-menu filter-dropdown package-list-checkbox">
                                       <c:forEach items="${facet.values}" var="facetValue" varStatus="fcIdx">
                                          <c:set var="propertyCategoryFilterChecked" value="false" />
                                          <c:if test="${not empty paramValues.propertyCategoryFilter}">
                                             <c:forEach items="${paramValues.propertyCategoryFilter}" var="selectedPropertyCategoryFilter">
                                                <c:if test="${selectedPropertyCategoryFilter eq facetValue.code}">
                                                   <c:set var="propertyCategoryFilterChecked" value="true" />
                                                </c:if>
                                             </c:forEach>
                                          </c:if>
                                          <li class="">
                                             <div class="checkbox">
                                                <label class="custom-checkbox-input show deal-details-icon" for="facet_${facetValue.code}_${fcIdx.count}">
                                                <input type="checkbox" name="propertyCategoryFilter" id="facet_${facetValue.code}_${fcIdx.count}" value="${facetValue.code}" ${propertyCategoryFilterChecked ? 'checked' : ''} /> <span class="${fn:toUpperCase(facetValue.code)}"></span> <span>${facetValue.name}</span> <span
                                                   class="checkmark-checkbox"></span>
                                                </label>
                                             </div>
                                          </li>
                                       </c:forEach>
                                       <li>
                                          <input class="btn btn-primary custom-btn my-4" type="submit" value="Apply">
                                       </li>
                                    </ul>
                                 </li>
                              </ul>
                           </div>
                        </div>
                     </div>
                  </c:if>
               </c:forEach>
               <pagination:storeRequestParams currentField="propertyCategoryFilter"/>
            </form>
         </div>
      </c:if>
   </div>
   <c:choose>
      <c:when test="${fn:length(facetSearchPageData.results) gt 0}">
         <div class="row">
            <div class="col-md-6 col-sm-6 col-xs-12 text-right sort-by-align">
               <ul class="list-inline">
                  <li>
                     <b>
                        <spring:theme code="accommodation.sort.by.title" text="Sort by" />
                     </b>
                  </li>
                  <li>
                     <form action="${requestScope['javax.servlet.forward.request_uri']}">
                        <Select class="select-custom vacations-listing-select form-control" name="sortBy" onChange="this.form.submit();">
                           <option value="name-asc" ${param.sortBy eq 'name-asc' ? 'selected' : ''}>
                           <spring:theme code="text.vacation.listing.alphabeticalAsc" text="Alphabetical (A-Z)" />
                           </option>
                           <option value="name-desc" ${param.sortBy eq 'name-desc' ? 'selected' : ''}>
                           <spring:theme code="text.vacation.listing.alphabeticalDesc" text="Alphabetical (A-Z)" />
                           </option>
                        </Select>
                        <pagination:storeRequestParams currentField="sortBy"/>
                     </form>
                  </li>
               </ul>
            </div>
         </div>
         <div class="container">
            <div class="row offers-grouped-component vacations-listing-perfect-carousel">
               <div id="y_packageResults" aria-label="package search results">
                  <div  class="row">
                     <c:forEach items="${facetSearchPageData.results}" var="hotel" varStatus="loop">
                        <div class="col-lg-4 col-md-4 col-sm-6 col-xs-12 hotel-carousel-width">
                           <div class="accommodation-image">
                              <div class="owl-carousel owl-theme js-owl-carousel js-owl-rotating-gallery slide-no-${fn:length(hotel.images)}">
                                 <c:if test="${not empty hotel.images}">
                                    <c:forEach items="${hotel.images}" var="hotelImage">
                                       <div class="image-img">
                                          <img class='js-responsive-image' data-media='${hotelImage.url}' alt='${hotelImage.altText}' title='${hotelImage.altText}'/>
                                          <div class="slider-count">
                                             <div class="slider-img-thumb"></div>
                                             <div class="slider-num">1/4</div>
                                          </div>
                                       </div>
                                    </c:forEach>
                                 </c:if>
                              </div>
                           </div>
                           <div class="deal-details">
                           <div class="deal-labels">
                              <c:forEach items="${hotel.propertyCategories}" var="categoryType">
                                 <span class="${fn:toUpperCase(categoryType)}"></span>
                              </c:forEach>
                              </div>
                              <p class="text-grey margin-top-10"> ${fn:toUpperCase(hotel.locationName)}</p>
                              <spring:url value="${jalosession.tenant.config.getParameter('tripadvisor.url')}" var="tripAdvisorURL" htmlEscape="false">
                                 <spring:param name="locationId"  value="${hotel.customerReviewData.locationId}"/>
                              </spring:url>
                              <div class="review-img">
                                 <c:if test="${not empty hotel.customerReviewData.ratingImageUrl}">
                                    <img src="${fn:escapeXml(hotel.customerReviewData.ratingImageUrl)}" alt="Trip Advisor Rating">
                                    <a  class="tripAdvisorLink" href="${tripAdvisorURL}">
                                       <span class="review-text">
                                          ${hotel.customerReviewData.numOfReviews}
                                          <spring:theme code="text.accommodation.details.property.reviews" />
                                       </span>
                                    </a>
                                 </c:if>
                              </div>
                              <h2>${hotel.name}</h2>
                              <div class="card-height">
                              	<p> ${hotel.description}</p>
                              	</div>
                              <a href="${hotel.nextUrl}" class="btn btn-primary  margin-top-20">
                                 <spring:theme code="text.package.listing.button.continue" text="Book now"/>
                              </a>
                           </div>
                        </div>
                        <c:if test="${(loop.index + 1) % 3 == 0}">
                  </div>
                  <div class="row">
                  </c:if>
                  </c:forEach>
                  </div>
               </div>
            </div>
         </div>
      </c:when>
      <c:otherwise>
         <spring:theme code="text.vacation.listing.NoHotelsFound" text="No Hotels Found" />
      </c:otherwise>
   </c:choose>
   <pagination:globalpagination/>
</div>
