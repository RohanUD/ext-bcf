<%@ page trimDirectiveWhitespaces="true" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="format" tagdir="/WEB-INF/tags/addons/bcfstorefrontaddon/responsive/format" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="cms" uri="http://hybris.com/tld/cmstags" %>
<spring:htmlEscape defaultHtmlEscape="false"/>

<div class="card media-wrapper banner-media" style="background:${backgroundColour}">
	  <c:if test="${not empty featuredText}">
	          <div class="featured-bx-banner">${featuredText}</div>
	  </c:if>
       <c:if test="${not empty dataMedia}">
           <img class='js-responsive-image all-banner-height' alt='${altText}' title='${altText}' data-media='${dataMedia}'/>
       </c:if>

       <c:if test="${not empty videoUrl}">
        <div class='embed-container'><iframe src='${videoUrl}' frameborder='0' allowfullscreen allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture"></iframe></div>
       </c:if>
       <div class="slider-count silder-count-hidden">
		    <div class="slider-img-thumb"></div>
                <div class="slider-num">2/2</div>
       </div>
		<c:if test="${not empty promoText}">
             <div class="package-circle full-width-circle">
                 ${promoText}
             </div>
    	</c:if>
  <div class="container">
        <div>
            <div class="home-b-text clearfix"> 
                <div class="hero-text-box col-sm-8 col-md-12 col-xs-10">
                        <c:if test="${not empty topText}">
                            <div class="card-title">${topText}</div>
                        </c:if>
                        <c:if test="${not empty overlayText}">
                            <div class="card-title">${overlayText}</div>
                        </c:if>
                            <c:if test="${not empty description}">
                                <div class="card-text p-ellipsis">${description}</div>
                            </c:if>
                            <c:if test="${not empty links}">
                                <c:forEach items="${links}" var="link">
                                    <cms:component component="${link}" evaluateRestriction="true" />
                                </c:forEach>
                            </c:if>
                </div>
            </div>
        </div>
    </div>
    <div class="location-bx">
           ${location}<br />
           ${caption}
    </div>
     <c:if test="${not empty iconText}">
          <div class="vacation-p overlay-icon">${iconText}</div>
     </c:if>
</div>
