<%@ page trimDirectiveWhitespaces="true" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="cms" uri="http://hybris.com/tld/cmstags" %>

    <div class="card media-top promotional-content flexbox-col" style="background:${backgroundColour}">
    <div class="p-relative">
      <img class='js-responsive-image' data-media='${dataMedia}' alt='${altText}'  title='${altText}'  />
        <div class="location-bx">
               ${location}<br /> 
               ${caption}
        </div>
        </div>
      <div class="promo-cards">
      <div class="card-body">
        <div class="deal-info">${title}</div>
        <c:if test="${not empty overlayText}">
             <div class="package-circle-blue">
                    ${promoText}
             </div>
        </c:if>
        <h3 class="card-title">${description}</h3>
        <div class="card-link">
            <c:if test="${not empty link}">
                <cms:component component="${link}" evaluateRestriction="true" />
            </c:if>
        </div>
      </div>
      </div>
      <c:if test="${not empty featuredText}">
       <div class="featured-bx-banner"><span>${featuredText}</span></div>
      </c:if>
    </div>
