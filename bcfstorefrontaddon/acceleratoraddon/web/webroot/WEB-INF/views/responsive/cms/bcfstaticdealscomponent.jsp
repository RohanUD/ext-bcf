<%@ page trimDirectiveWhitespaces="true" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="cms" uri="http://hybris.com/tld/cmstags" %>
<div class="container">
<h2 class="margin-bottom-20">${headingText}</h2>
	<div class="offers-grouped-component">
		<div class="row">
            <c:forEach items="${staticDeals}" var="deal" varStatus="loop">
            <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                <cms:component component="${deal}" evaluateRestriction="true" />
            </div>
            <c:if test="${(loop.index + 1) % 2 == 0}">
                </div>
                <div class="row">
            </c:if>
            </c:forEach>
	    </div>
	</div>

</div>
