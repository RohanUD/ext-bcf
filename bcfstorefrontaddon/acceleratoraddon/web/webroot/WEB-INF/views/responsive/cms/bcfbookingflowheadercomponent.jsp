<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="cms" uri="http://hybris.com/tld/cmstags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<header class="header-region">
    <nav class="navbar navbar-default navbar-static-top menu-mega-ui" id="js_navbar_menu">
        <div class="container">
            <div class="row logo-sub-header">
                    <div class="col-md-3 col-xs-2">
                        <a href="${previousPageUrl}" class="header-back-link"/>
                        <span class="bcf bcf-icon-left-arrow"></span><span class="hidden-xs"><spring:theme code="text.header.back" text="Back" /></span>
                        </a>
                    </div>
                    <div class="col-md-offset-0 col-md-6 col-xs-offset-0 col-xs-8">
                            <div class="logo-centered"><a href="/"><cms:component component="${logo.logo}" /></a></div>
                    </div>
                    <div class="col-md-3 col-xs-2">
                    <div class="header-login-reg">
                            <div class="text-right top-header-links">
                                <ul class="list-unstyled list-inline link-none">
                                    <li>
                                        <c:forEach items="${topLinks}" var="topLink">
                                            <cms:component component="${topLink}" evaluateRestriction="true"/>
                                        </c:forEach>
                                    </li>
                                </ul>
                            </div>
                        </div>
                </div>

            </div>
        </div>
    </nav>
</header>
