<%@ page trimDirectiveWhitespaces="true" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="cms" uri="http://hybris.com/tld/cmstags" %>
<%@ taglib prefix="bcfcms" tagdir="/WEB-INF/tags/addons/bcfstorefrontaddon/responsive/cms"%>

<bcfcms:displayCount displayCount="${displayCount}"/>

<div class="container">
	<div class="row">
		${headingText}
        <c:forEach items="${locations}" var="location" varStatus="status">
          <div class="${colWidthClass}">
            <cms:component component="${location}" evaluateRestriction="true" />
          </div>
          <c:if test="${(status.index + 1) % displayCount eq 0}">
            </div>
            <div class="row">
          </c:if>
        </c:forEach>
    </div>
	</div>
</div>
