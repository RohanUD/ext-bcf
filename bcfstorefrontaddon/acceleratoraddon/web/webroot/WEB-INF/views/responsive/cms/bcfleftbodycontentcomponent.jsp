<%@ page trimDirectiveWhitespaces="true" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="cms" uri="http://hybris.com/tld/cmstags" %>
<div class="container">
  <div class="row">
     <div class=" card data-content discover-box " style="background:${backgroundColour}">
       <div class="card-no-body">
            <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12 card-order-II">
	            <div class="card-body">
	             <c:if test="${not empty content}">
	              <div class="card-text">
	                 ${content}
	              </div>
	              </c:if>
	             </div>
            </div>
            <c:if test="${not empty dataMedia}">
               <div class="col-lg-6  col-md-6 col-sm-6 col-xs-12 card-media-wrapper  border-radius-all">
                  <img class='img-fluid js-responsive-image' alt='${altText}' title='${altText}' data-media='${dataMedia}'/>
                    <div class="location-bx">
                           ${location} <br/>
                           ${caption}
                    </div>
               </div>
            </c:if>
       </div>
     </div>
</div>
</div>

