<%@ page trimDirectiveWhitespaces="true"%>
<%@ taglib prefix="cms" uri="http://hybris.com/tld/cmstags"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<spring:htmlEscape defaultHtmlEscape="false" />

    <div class="card media-top vacation-deal-component"  style="background:${backgroundColour}" >
      <c:if test="${not empty dataMedia}">
        <div class="featured-box-main">
	        <c:if test="${not empty featuredText}">
	            <div class="featured-bx">${featuredText}</div>
	        </c:if>
        </div>
        <div class="p-relative">
        <img class='img-fluid js-responsive-image' data-media='${dataMedia}' alt='${altText}' title='${altText}' />
        <div class="location-bx">
                ${location} <br />
               ${caption}
         </div>
         </div>
        <c:if test="${not empty videoUrl}">
               <c:if test="${not empty overlayText}">
                    <div class="card-overlayText">${overlayText}</div>
               </c:if>
               <div class="embed-responsive embed-responsive-16by9">
                 <iframe class="embed-responsive-item" src="${videoUrl}" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture"
                 allowfullscreen></iframe>
               </div>
        </c:if>
      </c:if>
	<div class="promo-cards">
	      <div class="card-body">
	        <div class="deal-info">${topText}</div>
              <c:if test="${not empty dealNameImage}">
                <div class="margin-top-10 margin-bottom-10">
                  <img class='img-fluid js-responsive-image' data-media='${dealNameImage}' />
                </div>
              </c:if>
	       <c:if test="${not empty promotionText}">
	        <div class="card-title  h3-wrap"><h2>${promotionName}</h2></div>
	        </c:if> 
	        <c:if test="${empty promotionText}">
	        <div class="card-title "><h2>${promotionName}</h2></div>
	        </c:if>
	        <div class="card-text">
	        	<div class="card-height">
	        		<p>${promotionDesc}</p>
        		</div>
	        </div>
	        <c:if test="${not empty link}">
	        <div class="card-link">
	            <cms:component component="${link}" evaluateRestriction="true" />
	        </div>
	        </c:if>
	      </div>
	  <c:if test="${not empty promotionText}">
	            <div class="package-circle">
	                <span class="text-dark-blue"> ${promotionText}</span>
	            </div>
	        </c:if>
      </div>

    </div>
