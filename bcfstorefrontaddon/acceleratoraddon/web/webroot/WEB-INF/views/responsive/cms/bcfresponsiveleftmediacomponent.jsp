<%@ page trimDirectiveWhitespaces="true" %>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="cms" uri="http://hybris.com/tld/cmstags" %>
<div class="container">
   <div class="row">
   <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
     <div class="card discover-box discover-box-left-img" style="background:${backgroundColour}">
     <c:if test="${not empty featuredText}">
                  <div class="featured-bx-banner">${featuredText}</div>
     </c:if>
     <div class="row card-no-body">
        <div class="col-lg-7 col-md-7 col-sm-7 col-xs-12 card-media-wrapper card-media-left">
           <c:if test="${not empty dataMedia}">
                <img class='img-fluid js-responsive-image' alt='${altText}'
                    title='${altText}' data-media='${dataMedia}' />
            </c:if>
            <c:if test="${not empty videoUrl}">
             <div class='embed-container'><iframe src='${videoUrl}' frameborder='0' allowfullscreen allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture"></iframe></div>
            </c:if>
        </div>
            <div class="col-lg-5 col-md-5 col-sm-5 col-xs-12">
                 <div class="card-body">
                 <c:if test="${not empty overlayText}">
                    <div class="card-title">${overlayText}</div>
                 </c:if>
                  <c:if test="${not empty description}">
                    <div class="card-text">${description}</div>
                  </c:if>
                    
                    <c:if test="${not empty links}">
                        <c:forEach items="${links}" var="link">
                            <cms:component component="${link}" evaluateRestriction="true" />
                        </c:forEach>
                    </c:if>
                 </div>
            </div>
        </div>
     </div>
     </div>
   </div>
</div>
