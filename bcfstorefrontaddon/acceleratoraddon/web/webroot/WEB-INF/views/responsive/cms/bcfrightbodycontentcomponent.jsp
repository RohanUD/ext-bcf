<%@ page trimDirectiveWhitespaces="true" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="cms" uri="http://hybris.com/tld/cmstags" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<div class="container margin-top-40">
	<div class="row">
		<c:if test="${not empty dataMedia}">
			<div class="col-md-6 col-sm-3 col-xs-12">
				<img class='img-fluid border-r-10 img-w-h js-responsive-image' alt='${altText}'
					title='${altText}' data-media='${dataMedia}' />
                <div class="location-bx">
                       <p class="card-text"> ${location}</p>
                       <p class="card-text">${caption}</p>
                </div>
			</div>
		</c:if>
		<div class="col-md-6 col-sm-9 col-xs-12">
			<div class="component-cnrl">${content}</div>
		</div>
	</div>
</div>

