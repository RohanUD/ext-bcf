<%@ page trimDirectiveWhitespaces="true"%>
<%@ taglib prefix="cms" uri="http://hybris.com/tld/cmstags"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<spring:htmlEscape defaultHtmlEscape="false" />
<div class="row">
	<div class="p-relative destination-landing">
		<c:if test="${not empty featuredText}">
			<div class="featured-bx-banner">${featuredText}</div>
		</c:if>
		<c:if test="${not empty dataMedia}">
			<img class='img-fluid js-responsive-image all-banner-height' data-media='${dataMedia}' alt='${altText}' title='${altText}' />
		</c:if>
		<div class="container">
		<div class="row">
			<div class="destination-details-banner-text">
					<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
							<div class="white-text">${locationType}</div>
							<h1 class="padding-top-10 padding-bottom-20">${name}</h1>
							<div>${locationTag}</div>
							<button class="btn btn-default">
								<c:if test="${not empty link}">
										<cms:component component="${link}" evaluateRestriction="true" />
									</c:if>
							</button>
					</div>
				</div>
		</div>
		</div>
		<div class="location-bx">
			${location}<br/>
			${caption}
		</div>
	</div>
</div>
