<%@ page trimDirectiveWhitespaces="true" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="cms" uri="http://hybris.com/tld/cmstags" %>

<div class="row">
<div class="d-flex">
<div class="col-lg-5 col-md-5  col-sm-4 col-xs-12">
<c:if test="${not empty dataMedias}">
<div class="owl-slider-ferry-d show-silder-count  accommodation-image image-border">
   <div class="js-owl-carousel js-owl-rotating-gallery  owl-carousel owl-theme ferry-detail-owl slide-no-${fn:length(dataMedias)}">
      <c:forEach items="${dataMedias}" var="dataMedia">
         <cms:component component="${dataMedia}" evaluateRestriction="true" />
      </c:forEach>
   </div>
</div>
</c:if>
</div>
<div class="col-md-1 col-lg-1 col-sm-2 col-xs-12">
              <div class="indicator"><span><c:if test="${not empty startDay}">${startDay}</c:if></span></div>
            </div>
<div class="col-lg-5 col-md-5  col-sm-4 col-xs-12">
<c:if test="${not empty paragraph}">
${paragraph}
</c:if>
</div>

             
</div>

<div class="d-flex hidden-lg hidden-md">

<div class="col-md-1 col-lg-1 col-sm-2 col-xs-2 pl-0 pr-0">
              <div class="indicator"><span><c:if test="${not empty startDay}">${startDay}</c:if></span></div>
            </div>
<div class="col-lg-5 col-md-5  col-sm-10 col-xs-10">
<c:if test="${not empty paragraph}">
${paragraph}
</c:if>
</div>

             
</div>
</div>
