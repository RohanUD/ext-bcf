<%@ page trimDirectiveWhitespaces="true" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="cms" uri="http://hybris.com/tld/cmstags" %>
<%@ taglib prefix="bcfcms" tagdir="/WEB-INF/tags/addons/bcfstorefrontaddon/responsive/cms"%>

<bcfcms:displayCount displayCount="${displayCount}"/>
<div class="container">
    <c:if test="${not empty backgroundImage}">
        <img class='img-fluid  img-w-h js-responsive-image' alt='${altText}' data-media='${backgroundImage}'/>
        <div class="location-bx">
               <p class="card-text"> ${location}</p>
               <p class="card-text">${caption}</p>
        </div>
    </c:if>
    <div class="row location-contactus" style="background:${backgroundColour}">
     ${headingText}
     <div class="owl-carousel owl-theme js-owl-carousel js-owl-default" data-count="${displayCount}">
        <c:forEach items="${contacts}" var="contactUsInfo" varStatus="loop">
        <div class="item">
                <cms:component component="${contactUsInfo}" evaluateRestriction="true" />
			
			</div>
        </c:forEach>
        </div>
    </div>
</div>
