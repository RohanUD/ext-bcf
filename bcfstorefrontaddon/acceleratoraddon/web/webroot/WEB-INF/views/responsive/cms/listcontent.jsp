<%@ page trimDirectiveWhitespaces="true" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="cms" uri="http://hybris.com/tld/cmstags" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<div class="container">
<div class="row">

    <%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
    <div class="container">
    <div class="row">
    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 hidden-sm hidden-xs">
        <ul class="list-inline desktop-inline-component link-component-5">
            <c:if test="${not empty links}">
                <c:forEach items="${links}" var="link">
                    <li class="list-inline-item"><cms:component component="${link}" evaluateRestriction="true" /></li>
                </c:forEach>
            </c:if>
        </ul>
    <hr />
    </div>

    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 hidden-lg hidden-md p-0">
        <div class="js-accordion js-accordion-default mobile-group-component">
          <h4>Go to</h4>
          <div>
            <ul class="list-group">
                 <c:if test="${not empty links}">
                    <c:forEach items="${links}" var="link">
                        <li class="list-group-item"><cms:component component="${link}" evaluateRestriction="true" /></li>
                    </c:forEach>
                </c:if>
            </ul>
          </div>
        </div>
    </div>
    </div>
    </div>
</div>
</div>
