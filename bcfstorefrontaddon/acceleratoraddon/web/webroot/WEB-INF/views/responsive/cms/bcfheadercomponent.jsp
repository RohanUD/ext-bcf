<%@ page trimDirectiveWhitespaces="true"%>
<%@ taglib prefix="sec"
	uri="http://www.springframework.org/security/tags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="cms" uri="http://hybris.com/tld/cmstags"%>
<header class="header-region">
<div class="container menu-display">
	<!-- for tablet -->
	<div class="row mobile-logo hidden-xs">
		<div class="col-xs-1" >
			<button class="navbar-toggle" type="button" data-toggle="collapse"
				data-target=".js-navbar-collapse">
				<span class="sr-only">Toggle navigation</span> <span
					class="icon-bar"></span> <span class="icon-bar"></span> <span
					class="icon-bar"></span>
					<span class="mobile-menu-link">Menu</span>
			</button>
			<div class="close-menu">
				<i class="fa fa-times" aria-hidden="true"></i>
			</div>
		</div>
		<div class="col-xs-3 m-p-0">
			<cms:component component="${logo.logo}" />
		</div>
		<div class="navbar-additional hidden-xs  col-xs-8">
			<div class="text-right top-header-links">
				<ul class="list-unstyled list-inline link-none">
					<c:forEach items="${topLinks}" var="topLink">
						<li><cms:component component="${topLink}" evaluateRestriction="true"/></li>
					</c:forEach>
				</ul>
			</div>
		</div>
	</div>
	<!-- for desktop -->
	<div class="navbar-additional hidden-xs hidden-sm hidden-md">
		<div class="text-right top-header-links">
			<ul class="list-unstyled list-inline link-none">
				<c:forEach items="${topLinks}" var="topLink">
					<li><cms:component component="${topLink}" evaluateRestriction="true"/></li>
				</c:forEach>
			</ul>
		</div>
	</div>
</div>
	<nav class="navbar navbar-default navbar-static-top menu-mega-ui" id="js_navbar_menu">
	<div class="container">
		<div class="navbar-header ">
			<button class="navbar-toggle" type="button" data-toggle="collapse"
				data-target=".js-navbar-collapse">
				<span class="sr-only">Toggle navigation</span> <span
					class="icon-bar"></span> <span class="icon-bar"></span> <span
					class="icon-bar"></span>
					<span class="mobile-menu-link">Menu</span>
			</button>
			<div class="close-menu hidden-lg">
				<i class="fa fa-times" aria-hidden="true"></i>
			</div>
			<a class="navbar-toggle pull-right text-center" href="${pageContext.request.contextPath}/my-account">
				<span class="sr-only">Toggle navigation</span> 
				<span class="bcf bcf-icon-account header-login-icon"></span><br/>
				<span class="mobile-login-user-link">My account</span>
			</a>
			<cms:component component="${logo.logo}" />
		</div>

		<div class="collapse navbar-collapse js-navbar-collapse">
			<ul class="collapse-sailling link-none sec-nav fnt-14">
				<c:forEach items="${topLinks}" var="topLink">
					<li><cms:component component="${topLink}" evaluateRestriction="true"/></li>
				</c:forEach>
			</ul>
			<ul class="nav navbar-nav navbar-right mob-navigation ">
				<c:if test="${not empty navigationNodes}">
					<c:forEach items="${navigationNodes}" var="navNode" varStatus="status">
						<li class="dropdown mega-dropdown dropdown-masked menu-${status.index+1}">
							<a href="#" class="dropdown-toggle" data-toggle="dropdown">
								${navNode.title}
								<span class="down-a">
									<i class="bcf bcf-icon-down-arrow" aria-hidden="true"></i>
								</span>
							</a>
							<ul class="dropdown-menu mega-dropdown-menu" data-panel='menu-${status.index+1}'>
									<li>
										<ul class="container list-unstyled m-auto">
										<li class="col-sm-4 padding-left-0 icon-manage navigator">
											<h2 class="heding-menu-1">${navNode.title}</h2>
											<p class="p-ellipsis">${navNode.description}</p> <c:if
												test="${not empty navNode.link}">
												<p class="navigation-mob-link-n">
													<cms:component component="${navNode.link}" evaluateRestriction="true" />
												</p>
											</c:if>
										</li>

										<li class="col-sm-3 nav-value padding-0 navigator">
											<ul>
												<c:if test="${not empty navNode.childLinks}">
													<c:forEach items="${navNode.childLinks}" var="childLink">
														<li class="navigator deals-menu"><cms:component component="${childLink}" evaluateRestriction="true"/></li>
													</c:forEach>
												</c:if>
											</ul>
										</li>
										<li class="col-sm-5 padding-right-0 mob-img-box">
											<ul>
														<div class="menu-image-box navigator">
															<a href=""> <c:if
																	test="${not empty promotionalmages[navNode.uid]}">
																	<img class='img-fluid js-responsive-image'
																		alt='${altText}' title='${altText}'
																		data-media='${promotionalmages[navNode.uid]}' />
																</c:if>
															</a>
															<div class="navigation-img">
																<p class="text-dark-blue">${navNode.promotionalTitle}</p>
																<p class="m-0">
																	<b><c:if test="${not empty navNode.promotionalLink}">
																		<cms:component component="${navNode.promotionalLink}" evaluateRestriction="true" />
																	</c:if></b>
																</p>
															</div>
														</div>
											</ul>
										</li>
										</ul>
									</li>
							</ul>

							</li>
					</c:forEach>
				</c:if>
			</ul>
		</div>
		</div>
		<!-- /.nav-collapse -->
	</nav>
</header>
