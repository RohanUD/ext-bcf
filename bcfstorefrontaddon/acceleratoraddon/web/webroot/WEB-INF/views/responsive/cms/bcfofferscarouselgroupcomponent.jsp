<%@ page trimDirectiveWhitespaces="true" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="cms" uri="http://hybris.com/tld/cmstags" %>
<%@ taglib prefix="packagehotellisting" tagdir="/WEB-INF/tags/addons/bcfcommonsaddon/responsive/packagehotellisting"%>
<%@ taglib prefix="bcfcms" tagdir="/WEB-INF/tags/addons/bcfstorefrontaddon/responsive/cms"%>

<bcfcms:displayCount displayCount="${displayCount}"/>

<div class="container">
	${headingText}
	<div class="row offers-grouped-component">
	
		<c:choose>
            <c:when test="${not empty propertyDataList}">
                <div id="y_packageResults" class=" deal-items deal-row" aria-label="package search results">
                    <packagehotellisting:packageHotelListView propertiesListParams="${propertyDataList}" />
                </div>
            </c:when>
            <c:otherwise>
                <c:if test="${not empty offers}">
                    <c:forEach items="${offers}" var="offer">
                        <div class="js-list-box-width ${colWidthClass}">
                            <cms:component component="${offer}" evaluateRestriction="true" />
                        </div>
                        <c:if test="${(status.index + 1) % displayCount eq 0}">
                            </div>
                            <div class="row offers-grouped-component">
                        </c:if>
                    </c:forEach>
                </c:if>
            </c:otherwise>
        </c:choose>
	</div>
</div>
