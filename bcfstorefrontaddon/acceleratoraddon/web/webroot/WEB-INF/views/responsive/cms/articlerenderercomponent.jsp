<%@ page trimDirectiveWhitespaces="true"%>
<%@ taglib prefix="theme" tagdir="/WEB-INF/tags/shared/theme"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="article" tagdir="/WEB-INF/tags/addons/bcfstorefrontaddon/responsive/article"%>
<%@ taglib prefix="ycommerce" uri="http://hybris.com/tld/ycommercetags"%>
<div class="container">
	<c:choose>
		<c:when test="${articleNotFound eq true }">
			<div class="row no-article">
				<h3>
					Article <b>${articleId }</b> not found!!
				</h3>
			</div>
		</c:when>
		<c:otherwise>
			<div class="row">
				<div class="article-content-area">
					<div class="article-image">
						<c:if test="${article.image.url ne null }">
							<img src="${article.image.url}" />
						</c:if>
					</div>
					<c:set var="titleStyle" value="${empty article.titleStyle ? 'h1' : article.titleStyle}" />
					<${titleStyle}>${fn:escapeXml(article.title)}</${titleStyle}>
					<div class="article-des">
						<h6>${article.shortDescription}</h6>
					</div>
					<div class="article-des">
						<h6>${article.longDescription}</h6>
					</div>
					
					<article:section sections="${article.sections}" />
					
					
					<%-- <c:forEach items="${article.sections}" var="section" varStatus="status">
						<c:set var="alignment" value="left" />
						<c:if test="${status.index % 2 != 0}">
							<c:set var="alignment" value="right" />
						</c:if>
						<div class="section ${alignment}">
							<c:set var="secStyle" value="${empty section.titleStyle ? 'section-title' : section.titleStyle}" />
							<div class="${secStyle}">${fn:escapeXml(section.title)}</div>
							<div class="section-content">
								<span class="article-img"><c:if test="${section.image.url ne null }">
										<img src="${section.image.url}" />
									</c:if></span> <span class="section-description">${section.shortDescription}</span>
								<div class="section-content">
									<span class="section-description">${section.longDescription}</span>
								</div>
							</div>
						</div>
					</c:forEach> --%>
				</div>
			</div>
		</c:otherwise>
	</c:choose>
</div>
