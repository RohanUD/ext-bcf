<%@ page trimDirectiveWhitespaces="true"%>
<%@ taglib prefix="template" tagdir="/WEB-INF/tags/responsive/common"%>
<%@ taglib prefix="cms" uri="http://hybris.com/tld/cmstags"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags"%>
<%@ taglib prefix="format" tagdir="/WEB-INF/tags/addons/bcfstorefrontaddon/responsive/format"%>
<%@ taglib prefix="booking" tagdir="/WEB-INF/tags/addons/bcfstorefrontaddon/responsive/booking"%>


<spring:htmlEscape defaultHtmlEscape="false" />
<template:page pageTitle="${pageTitle}">
	<div class="container">
		<div class="row heading-with-button">
			<div class="col-xs-12 col-sm-offset-3 col-sm-5">
				<h2 class="h2">
					<spring:theme code="text.changebooking.request.list" text="Change Booking Requests" />
				</h2>
			</div>

		</div>
		<div class="margin-reset clearfix">
			<div class="col-xs-12 col-sm-3">
				<aside id="sidebar">
					<div class="promotions hidden-xs">
						<cms:pageSlot position="LeftContent" var="feature">
							<cms:component component="${feature}" element="section" />
						</cms:pageSlot>
					</div>
				</aside>
			</div>
			<div class="col-xs-12 col-sm-9">				
				<div class="panel panel-primary panel-list booking room-details clearfix">
					<cms:pageSlot position="RightContent" var="feature">
						<cms:component component="${feature}"/>
					</cms:pageSlot>
				</div>
				<div class="row">

				</div>
			</div>
		</div>
	</div>
</template:page>
