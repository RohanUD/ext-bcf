<%@ page trimDirectiveWhitespaces="true"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="currentconditions"
		   tagdir="/WEB-INF/tags/addons/bcfvoyageaddon/responsive/currentconditions"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ page language="java" contentType="text/html;charset=UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<jsp:useBean id="now" class="java.util.Date" />
<spring:htmlEscape defaultHtmlEscape="false" />

<div class="container">
	<%-- Page Title and description --%>
	<p class="text-grey margin-top-10"><spring:theme code="text.cc.atAglance.header.label"
		text="At-a-glance" /></p>
	<h2 class="text-dark-blue">
		<b><spring:theme code="text.cc.atAglance.ferryTracking.page.label"
						 text="Ferry tracking" /></b>
	</h2>


	<div class="row">
		<div class="col-xl-8 col-md-8 col-sm-12 col-xs-12">
			<p>
				<spring:theme code="text.cc.atAglance.ferryTracking.page.desc.label" />
			</p>
		</div>
	</div>


	<%-- Refresh --%>
	<p class="italic-style margin-top-20 margin-bottom-20 text-sm">
		<em> <spring:theme code="text.cc.lastUpdated.label"
						   text="Last updated" /> : <fmt:formatDate value="${now}"
																	pattern="h:mm a EEEE, MMM dd, yyyy" />. <a href=""
																												   onclick="return false" id="refreshDepartures"> <spring:theme
				code="text.cc.atAglance.click.label" text="Click" />&nbsp;
			<spring:theme code="text.cc.atAglance.to.refresh.label" text="to refresh"/></a>
		</em>
	</p>

</div>
<section >
	<div class="container">
		<div class="row my-5">
			<div class="col-md-4">
				<label class="text-grey">
					<spring:theme code="text.find.label" text="Find"/>
				</label>
				<c:set var="to"><spring:theme code="text.to.label"/> </c:set>
				<select class="select-custom form-control" id="selectFerryTrackingRoute">
					<option value="default">
						<spring:theme code="text.cc.terminals.selectRoute.label" text="Select a route"/>
					</option>

					<c:forEach items="${currentConditionsRouteData}" var="routes">
						<c:forEach items="${routes.value}" var="route">
							<option value="${routes.key}__${route.routeCode}">
									${route.sourceTerminalName}&nbsp;${to}&nbsp;${route.destinationTerminalName}
							</option>
						</c:forEach>
					</c:forEach>
				</select>
			</div>
		</div>
		<hr class="margin-top-30">
	</div>
</section>

<div class="container">
	<c:set var="showCount" value="6" />
	<c:forEach items="${currentConditionsRouteData}" var="routes">
		<div class="content-wrapper">
			<h4 class="ferry-region-map margin-top-30 margin-bottom-20">
				<b>${routes.key}</b>
			</h4>
			<c:forEach items="${routes.value}" var="route" varStatus="counter">
				<c:if test="${counter.index % 3 eq 0}">

					<div class="row">
				</c:if>
				<c:choose>
					<c:when test="${counter.count lt showCount or counter.count eq showCount}">
						<div id="ferryMap-${route.routeCode}"
							 class="ferry-box-map col-lg-4 col-md-4 col-sm-6 col-xs-12 margin-bottom-30">
						<span class="popup" data-toggle="modal" data-target="#ferryTrackingModals${route.routeCode}">
							<currentconditions:currentconditionsferrytracking
									ferryTrackingLink="${route.ferryTrackingLink}"
									sourceLocationName="${route.sourceLocationName}"
									sourceTerminalName="${route.sourceRouteName}"
									sourceTerminal="${route.sourceTerminalName}"
									sourceLocation="${route.sourceTerminalCode}"
									colClass="col-lg-12 col-md-12"

									destinationLocationName="${route.destinationLocationName}"
									destinationTerminalName="${route.destinationRouteName}"
									destinationTerminal="${route.destinationTerminalName}"
									destinationLocation="${route.destinationTerminalCode}"
							/>
						</span>
							<div class="modal fade" id="ferryTrackingModals${route.routeCode}" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
								<div class="modal-dialog" role="document">
									<div class="modal-content">
										<div class="modal-header">
											<spring:theme code="text.cc.atAglance.ferryTracking.page.label" text="FERRY TRACKING" />
											<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true"><i class="bcf bcf-icon-cancel-solid"></i></span></button>

										</div>

										<div class="modal-body">
											<h5>${route.sourceTerminalName}&nbsp;<spring:theme code="text.to.label" text="to" />&nbsp;${route.destinationTerminalName}
											</h5>
											<div class="text-center">
												<iframe width="89%" height="633" style="overflow: hidden;" src="${route.ferryTrackingLink}" frameborder="0" marginheight="0" marginwidth="0" scrolling="no"></iframe>
											</div></div>
									</div>
								</div>
							</div>
						</div>
						<c:if test="${counter.count eq showCount}">
							<div class="row">
								<div class="col-lg-4 col-md-4 col-sm-4 col-xs-12 text-center col-md-offset-4 col-sm-offset-4">
									<button class="btn btn-primary btn-block ferryTrackingLoadMore margin-bottom-10">
										<spring:theme code="text.cc.atAglance.loadMore.label"
													  text="Load more" />
									</button>
								</div>
							</div>
						</c:if>
					</c:when>
					<c:otherwise>
						<div class="showMore box ferry-box-map hidden col-lg-4 col-md-4 col-sm-6 col-xs-12 margin-bottom-30" id="ferryMap-${route.routeCode}">
							<span class="popup" onClick="javascript:window.open('${route.ferryTrackingLink}', '_blank','toolbar=no,width=500,height=750');">
								<currentconditions:currentconditionsferrytracking
										ferryTrackingLink="${route.ferryTrackingLink}"
										sourceLocationName="${route.sourceLocationName}"
										sourceTerminalName="${route.sourceTerminalName}"
										sourceLocation="${route.sourceTerminalCode}"
									    sourceTerminal="${route.sourceTerminalName}"
										colClass="col-lg-12 col-md-12"
										destinationLocationName="${route.destinationLocationName}"
										destinationTerminalName="${route.destinationTerminalName}"
										destinationLocation="${route.destinationTerminalCode}"
									    destinationTerminal="${route.destinationTerminalName}"
								/>
							</span>
						</div>
					</c:otherwise>
				</c:choose>

				<c:if test="${counter.count % 3 eq 0 or counter.last}">
					</div>
				</c:if>

			</c:forEach>
			<div class="ferry-hr-separator" id="${routes.key}">
				<hr />
			</div>
		</div>
	</c:forEach>
</div>
