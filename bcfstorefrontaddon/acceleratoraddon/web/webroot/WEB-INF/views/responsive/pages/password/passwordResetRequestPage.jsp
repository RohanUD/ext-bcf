<%@ page trimDirectiveWhitespaces="true" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="user" tagdir="/WEB-INF/tags/addons/bcfstorefrontaddon/responsive/user" %>
<%@ taglib prefix="template" tagdir="/WEB-INF/tags/responsive/common" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<c:url var="loginPageUrl" value="/login"/>
<div class="login-page__headline">
    <h4><spring:theme code="login.title"/></h4>
</div>

	<div class="forgotten-password">
	    <div class="row mb-4">
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <p class="mb-5 fnt-14"><b><spring:theme code="forgotPassword.label" text="Reset Password"/></b></p>
            </div>
        </div>
    </div>
<c:choose>
	<c:when test="${passwordRestRequestSuccess}">
		<p><spring:theme code="forgotPassword.success.label"/>&nbsp;<strong>${email}</strong></p>
		<p><spring:theme code="forgotPassword.success.text"/></p>
		<br/>
		<p>
			<ycommerce:testId code="loginAndCheckoutButton">
				<a href="${loginPageUrl}" class="btn btn-primary btn-block">
					<spring:theme code="login.login" />
				</a>
			</ycommerce:testId>
		</p>
	</c:when>
	<c:otherwise>
		<user:forgottenPwd />
	</c:otherwise>
</c:choose>
