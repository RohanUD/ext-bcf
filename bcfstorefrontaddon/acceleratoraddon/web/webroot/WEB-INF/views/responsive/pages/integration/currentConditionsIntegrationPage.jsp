<%@ page trimDirectiveWhitespaces="true"%>
<%@ taglib prefix="template" tagdir="/WEB-INF/tags/responsive/common"%>
<%@ taglib prefix="cms" uri="http://hybris.com/tld/cmstags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<div class="row">
	<div class="col-3-md">
		<a href="/bcfstorefront/current-conditions/getConditions/TSA" class="btn btn-primary">Get Current Conditions for TSA</a>
	</div>
	&nbsp
	<div class="col-3-md">
		<a href="/bcfstorefront/current-conditions/getConditions/SWB" class="btn btn-primary">Get Current Conditions for SWB</a>
	</div>
	&nbsp
	<div class="col-3-md">
		<a href="/bcfstorefront/current-conditions/getConditions/TSA/route30" class="btn btn-primary">Get Current Conditions for TSA for route30</a>
	</div>
	&nbsp
	<div class="col-3-md">
		<a href="/bcfstorefront/current-conditions/getConditions/SWB/route30" class="btn btn-primary">Get Current Conditions for SWB for route30</a>
	</div>
	&nbsp
	<div class="col-3-md">
		<a href="/bcfstorefront/current-conditions/getConditions/multi/TSA,SWB" class="btn btn-primary">Get Current Conditions for TSA & SWB</a>
	</div>
</div>
<div>
	<c:choose>
		<c:when test="${result == 'noMovementsError'}">
			<p>Result : Unable to get current conditions for the terminal!</p>
		</c:when>
		<c:when test="${not empty result}">
			<p>Result :</p>
			<c:set var="waits" value="${result.waits}" />
			<c:set var="route30" value="${waits.route30}" />
			<c:set var="route09" value="${waits.route30}" />
			<c:set var="route01" value="${waits.route30}" />
			<c:set var="route66" value="${waits.route30}" />
			<c:set var="cam" value="${result.cams}" />
			<c:set var="parking" value="${result.parking}" />
			<c:set var="arrivalDepartures" value="${result.arrivalDepartures}" />
			<p class="font-weight-bold">
				<u> Waits </u>
			</p>
			<p>Route : ${route30.route}</p>
			<p>ConditionTS : ${route30.conditionTS}</p>
			<p>Terminal ID : ${route30.terminalID}</p>
			<p>Estimated Over-Height Sailing Wait : ${route30.estOverheightSailingWait}</p>
			<p>Estimated Under-Height Sailing Wait : ${route30.estUnderheightSailingWait}</p>
			<br>
			<p>Route : ${route09.route}</p>
			<p>ConditionTS : ${route09.conditionTS}</p>
			<p>Terminal ID : ${route09.terminalID}</p>
			<p>Estimated Over-Height Sailing Wait : ${route09.estOverheightSailingWait}</p>
			<p>Estimated Under-Height Sailing Wait : ${route09.estUnderheightSailingWait}</p>
			<br>
			<p>Route : ${route01.route}</p>
			<p>ConditionTS : ${route01.conditionTS}</p>
			<p>Terminal ID : ${route01.terminalID}</p>
			<p>Estimated Over-Height Sailing Wait : ${route01.estOverheightSailingWait}</p>
			<p>Estimated Under-Height Sailing Wait : ${route01.estUnderheightSailingWait}</p>
			<br>
			<p>Route : ${route66.route}</p>
			<p>ConditionTS : ${route66.conditionTS}</p>
			<p>Terminal ID : ${route66.terminalID}</p>
			<p>Estimated Over-Height Sailing Wait : ${route66.estOverheightSailingWait}</p>
			<p>Estimated Under-Height Sailing Wait : ${route66.estUnderheightSailingWait}</p>
			<br>
			<p class="font-weight-bold">
				<u> Cams </u>
			</p>
			<p>Route : ${cam.route30.route}</p>
			<p>Location : ${cam.route30.location}</p>
			<p>TrafficToDestCam : ${cam.route30.trafficOutsideTerminalCam}</p>
			<p>TrafficOutsideTerminalCam: ${cam.route30.trafficToDestCam}</p>
			<p>LastUpdateTime : ${cam.route30.lastUpdateTime}</p>
			<br>
			<p>Route : ${cam.route09.route}</p>
			<p>Location : ${cam.route09.location}</p>
			<p>TrafficToDestCam : ${cam.route09.trafficOutsideTerminalCam}</p>
			<p>TrafficOutsideTerminalCam: ${cam.route09.trafficToDestCam}</p>
			<p>LastUpdateTime : ${cam.route09.lastUpdateTime}</p>
			<br>
			<p>Route : ${cam.route01.route}</p>
			<p>Location : ${cam.route01.location}</p>
			<p>TrafficToDestCam : ${cam.route01.trafficOutsideTerminalCam}</p>
			<p>TrafficOutsideTerminalCam: ${cam.route01.trafficToDestCam}</p>
			<p>LastUpdateTime : ${cam.route01.lastUpdateTime}</p>
			<br>
			<p class="font-weight-bold">
				<u>Arrival Departures</u>
			</p>
			<c:forEach var="entry" items="${arrivalDepartures.route30}" varStatus="loop">
				<p>Dept: ${entry.dept}</p>
				<p>Dest : ${entry.dest}</p>
				<p>Vessel : ${entry.vessel}</p>
				<p>Route : ${entry.route}</p>
				<p>Scheduled Departure : ${entry.scheduledDeparture}</p>
				<p>Scheduled Arrival : ${entry.scheduledArrival}</p>
				<p>Tarrif Type : ${entry.tariffType}</p>
				<p>Transfer Dept. : ${entry.transferDept}</p>
				<p>Status : ${entry.status}</p>
				<p>Licence : ${entry.licence}</p>
				<p>Sailing Type : ${entry.sailingType}</p>
				<p>Departure Status : ${entry.departureStatus}</p>
				<p>Berth : ${entry.berth}</p>
				<p>Actual Departure Ts : ${entry.actualDepartureTs}</p>
				<p>Actual Arrival TS : ${entry.actualArrivalTs}</p>
				<p>Delay Cancel : ${entry.delayCancel}</p>
				<p>UH Full Percent : ${entry.uhFullPercent}</p>
				<p>OS Full Percent : ${entry.osFullPercent}</p>
				<p>Vessel Full Percent : ${entry.vesselFullPercent}</p>
				<br>
			</c:forEach>
			<c:forEach var="entry" items="${arrivalDepartures.route09}" varStatus="loop">
				<p>Dept: ${entry.dept}</p>
				<p>Dest : ${entry.dest}</p>
				<p>Vessel : ${entry.vessel}</p>
				<p>Route : ${entry.route}</p>
				<p>Scheduled Departure : ${entry.scheduledDeparture}</p>
				<p>Scheduled Arrival : ${entry.scheduledArrival}</p>
				<p>Tarrif Type : ${entry.tariffType}</p>
				<p>Transfer Dept. : ${entry.transferDept}</p>
				<p>Status : ${entry.status}</p>
				<p>Licence : ${entry.licence}</p>
				<p>Sailing Type : ${entry.sailingType}</p>
				<p>Departure Status : ${entry.departureStatus}</p>
				<p>Berth : ${entry.berth}</p>
				<p>Actual Departure Ts : ${entry.actualDepartureTs}</p>
				<p>Actual Arrival TS : ${entry.actualArrivalTs}</p>
				<p>Delay Cancel : ${entry.delayCancel}</p>
				<p>UH Full Percent : ${entry.uhFullPercent}</p>
				<p>OS Full Percent : ${entry.osFullPercent}</p>
				<p>Vessel Full Percent : ${entry.vesselFullPercent}</p>
				<br>
			</c:forEach>
			<c:forEach var="entry" items="${arrivalDepartures.route01}" varStatus="loop">
				<p>Dept: ${entry.dept}</p>
				<p>Dest : ${entry.dest}</p>
				<p>Vessel : ${entry.vessel}</p>
				<p>Route : ${entry.route}</p>
				<p>Scheduled Departure : ${entry.scheduledDeparture}</p>
				<p>Scheduled Arrival : ${entry.scheduledArrival}</p>
				<p>Tarrif Type : ${entry.tariffType}</p>
				<p>Transfer Dept. : ${entry.transferDept}</p>
				<p>Status : ${entry.status}</p>
				<p>Licence : ${entry.licence}</p>
				<p>Sailing Type : ${entry.sailingType}</p>
				<p>Departure Status : ${entry.departureStatus}</p>
				<p>Berth : ${entry.berth}</p>
				<p>Actual Departure Ts : ${entry.actualDepartureTs}</p>
				<p>Actual Arrival TS : ${entry.actualArrivalTs}</p>
				<p>Delay Cancel : ${entry.delayCancel}</p>
				<p>UH Full Percent : ${entry.uhFullPercent}</p>
				<p>OS Full Percent : ${entry.osFullPercent}</p>
				<p>Vessel Full Percent : ${entry.vesselFullPercent}</p>
				<br>
			</c:forEach>
			<p class="font-weight-bold">
				<u>Parking</u>
			</p>
			<p>ConditioN: ${parking.conditionTS}</p>
			<p>Terminal ID : ${parking.terminalID}</p>
			<p>Parking Full Percent : ${parking.parkingFullPercent}</p>
			<p>Est. Outside Lot Vehicle Count : ${parking.estOutsideLotVehicleCount}</p>
			<p>Est. Outside Lot Km : ${parking.estOutsideLotKm}</p>
			<p>Weather Code : ${parking.weatherCode}</p>
			<p>Condition Comment : ${parking.condComment}</p>
			<p>Audit UserID : ${parking.auditUserID}</p>
			<p>AuditTS : ${parking.auditTS}</p>
			<br>
			<p class="font-weight-bold">
				<u>Refreshed At:${result.refreshedAt}</u>
			</p>
		</c:when>
		<c:otherwise>Result :</c:otherwise>
	</c:choose>
</div>
