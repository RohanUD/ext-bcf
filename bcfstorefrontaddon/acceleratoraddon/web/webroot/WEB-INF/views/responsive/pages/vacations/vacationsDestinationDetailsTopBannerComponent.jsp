<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<section class="p-relative">
<c:if test="${not empty destinationDetails.dataMedia}">
    <img class='img-fluid  img-w-h js-responsive-image all-banner-height'  alt='${destinationDetails.pictureAltText}' title='${destinationDetails.pictureAltText}' data-media='${destinationDetails.dataMedia}'/>
    
    <div class="container">
        <div class="row">
            <div class="destination-details-banner-text">
                <div class="col-lg-10 col-sm-10 col-xs-12">
                <h1>${destinationDetails.overlayText}</h1>
                </div>
            </div>
         </div>
    </div>
    
</c:if>
</section>
