<%@ page trimDirectiveWhitespaces="true" contentType="application/json"%>
<%@ taglib prefix="json" uri="http://www.atg.com/taglibs/json"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="transportreservation" tagdir="/WEB-INF/tags/addons/bcfvoyageaddon/responsive/reservation"%>
<json:object escapeXml="false">
	<json:property name="htmlContent">
        <transportreservation:transportBooking transportBookings="${transportBookings}"/>
	</json:property>
</json:object>
