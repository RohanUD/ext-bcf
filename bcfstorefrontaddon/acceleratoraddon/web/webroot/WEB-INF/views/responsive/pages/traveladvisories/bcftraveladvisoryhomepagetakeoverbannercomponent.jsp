<%@ page trimDirectiveWhitespaces="true" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<div class="sailing-cancellations-banner">
<div class="container">
        <div class="home-b-text">
            <div class="card-title">
                <c:if test="${not empty homePageTakeOverAdvisory}">
                    <h1>${homePageTakeOverAdvisory.advisoryTitle}</h1>
                </c:if> 
            </div> 
            <a href="/travel-advisories" class="btn-white">
                <spring:theme code="text.travel.advisory.view.advisories" text="View Travel Advisories" />
            </a> 
        </div>
        <span class="sailing-cancellations-icon">
                <span class="bcf bcf-icon-alert "></span>
        </span>
    </div>
</div>