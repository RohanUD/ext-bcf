<%@ page trimDirectiveWhitespaces="true"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="nav" tagdir="/WEB-INF/tags/responsive/nav" %>
<%@ taglib prefix="ycommerce" uri="http://hybris.com/tld/ycommercetags" %>

<c:set var="searchUrl" value="/option-booking/list/?sort=${ycommerce:encodeUrl(searchPageData.pagination.sort)}"/>
<div>
    <h2> Option Booking Queue </h2>
	<div class="account-orderhistory-pagination">
		<nav:pagination top="true" msgKey="text.account.orderHistory.page" showCurrentPageInfo="true" hideRefineButton="true" supportShowPaged="${isShowPageAllowed}" supportShowAll="${isShowAllAllowed}" searchPageData="${searchPageData}" searchUrl="${searchUrl}"  numberPagesShown="${numberPagesShown}"/>
		<c:set var="previousUrl" value="/option-booking/list?page=${previousPage}&sort=${ycommerce:encodeUrl(searchPageData.pagination.sort)}"/>
		<c:set var="nextUrl" value="/option-booking/list?page=${naxtPage}&sort=${ycommerce:encodeUrl(searchPageData.pagination.sort)}"/>
		<a class="btn btn-primary" href="<c:url value="${previousUrl}"/>">
			<spring:theme text="Previous" />
		</a>
		<a class="btn btn-primary" href="<c:url value="${nextUrl}"/>">
			<spring:theme text="Next" />
		</a>
	</div>
    <c:if test="${not empty searchPageData.results}">
        <table class="table">
            <thead>
                <tr>
                    <th><spring:theme code="text.page.optionbooking.header.cart.number" text="Booking Reference" /></th>
                    <th><spring:theme code="text.page.optionbooking.header.cart.created.by" text="Created By" /></th>
                    <th><spring:theme code="text.page.optionbooking.header.cart.created.time" text="Date Created" /></th>
                    <th><spring:theme code="text.page.optionbooking.header.cart.total.price" text="Total Price" /></th>
                    <th><spring:theme code="text.page.optionbooking.header.cart.departure.time" text="Departure Date" /></th>
                    <th><spring:theme code="text.page.optionbooking.header.cart.expiration.time" text="Expiration Date" /></th>
                    <th><spring:theme code="text.page.optionbooking.header.actions" text="Expiration Date" /></th>
                </tr>
            </thead>
            <tbody>
                <c:forEach items="${searchPageData.results}" var="cart">
                        <tr>
                            <td>${cart.code}</td>
                            <td><c:out value="${cart.createdBy.name}" /></td>
                            <td><c:out value="${cart.creationTime}" /></td>
                            <td><c:out value="${cart.subTotal.formattedValue}" /></td>
                            <td><c:out value="${cart.departureDate}" /></td>
                            <td><c:out value="${cart.expirationTime}" /></td>
                            <td>
                                <a href="/option-booking/details/${cart.code}">View Details</a>&nbsp;
                            </td>
                        </tr>
                </c:forEach>
            </tbody>
        </table>
    </c:if>
</div>
<div class="account-orderhistory-pagination">
	<nav:pagination top="false" msgKey="text.account.orderHistory.page" showCurrentPageInfo="true" hideRefineButton="true" supportShowPaged="${isShowPageAllowed}" supportShowAll="${isShowAllAllowed}" searchPageData="${searchPageData}" searchUrl="${searchUrl}"  numberPagesShown="${numberPagesShown}"/>
</div>
<div class="modal fade" id=" " tabindex="-1" role="dialog" aria-labelledby="y_asmOrderActionMessageModal">
	<div class="modal-dialog" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true"><i class="bcf bcf-icon-cancel-solid"></i></span>
				</button>
				<h3 class="modal-title" id="y_asmOrderActionTitle">
					<spring:theme code="text.page.optionbooking.actions.modal.title" text="Response" />
				</h3>
			</div>
			<div class="modal-body">
				<div id="y_asmOrderActionMessage"></div>
			</div>
		</div>
	</div>
</div>
