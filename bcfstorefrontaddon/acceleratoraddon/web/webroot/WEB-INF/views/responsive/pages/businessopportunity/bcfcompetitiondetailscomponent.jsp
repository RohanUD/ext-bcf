<%@ page trimDirectiveWhitespaces="true" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix = "fmt" uri = "http://java.sun.com/jsp/jstl/fmt" %>

<div class="container">
    <div class="row">
        <div class="col-12 col-md-12">
            <c:if test="${not empty Competition}">
                <h2>${Competition.title}</h2>
                    <p>${Competition.bodyContent}</p> 
                <h3><spring:theme code="text.competition.key.dates" text="Key Dates" /></h3>
                <p>
                <spring:theme code="text.service.notice.published" text="Published" />:
                <fmt:formatDate value="${Competition.publishedDate}" pattern="MMMM dd, yyyy" />
                <br>
                <spring:theme code="text.service.notice.closingDate" text="Closing Date" />:
                <fmt:formatDate value="${Competition.expiryDate}" pattern="MMMM dd, yyyy" />
                <br>
                <spring:theme code="text.service.notice.closingTime" text="Closing Time" />:
                <fmt:formatDate type = "time"  value = "${Competition.expiryDate}" timeStyle = "short"/>
                </p>

                <h3><spring:theme code="text.competition.primaryContact" text="Primary Contact" /></h3>
                <p>${Competition.mainContact}</p>
            </c:if>
        

            <a class="btn btn-primary btn-lg closebtn" role="button" href="/business-ops">
                <spring:theme code="text.business.opportunity.back" text="Back to Listings" />
            </a>
        </div>
    </div>
</div>