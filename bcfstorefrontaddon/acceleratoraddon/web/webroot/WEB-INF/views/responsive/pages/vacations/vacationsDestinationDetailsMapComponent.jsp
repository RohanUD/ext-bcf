<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<div class="row">
    <div class="col-lg-12">
        <div class="maps-city-detail">
            <c:if test="${not empty destinationDetails.mainImageDataMedia}"> 
                <img  class='img-fluid  img-w-h js-responsive-image margin-top-20' alt='${destinationDetails.mainImageAltText}' title='${destinationDetails.mainImageAltText}' data-media='${destinationDetails.mainImageDataMedia}'/>
            </c:if>
        </div>
    </div>
</div>

