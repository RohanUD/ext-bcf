<%@ page trimDirectiveWhitespaces="true"%>
<%@ taglib prefix="template" tagdir="/WEB-INF/tags/responsive/common"%>
<%@ taglib prefix="cms" uri="http://hybris.com/tld/cmstags"%>
<template:page pageTitle="${pageTitle}">

     <cms:pageSlot position="Header" var="feature" element="section">
            <cms:component component="${feature}" />
     </cms:pageSlot>


    <cms:pageSlot position="HeaderContent" var="feature" element="section">
        <div class="container-fluid package-listing-header">
            <cms:component component="${feature}" />
        </div>
    </cms:pageSlot>


     <cms:pageSlot position="MiddleContent" var="feature" element="section">
            <cms:component component="${feature}" />
    </cms:pageSlot>


     <cms:pageSlot position="PackageFooterContent" var="feature" element="section">
        <div class="container">
            <div class="row package-footer-content-box">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <cms:component component="${feature}" />
                </div>
            </div>
        </div>
     </cms:pageSlot>

     <cms:pageSlot position="PackageFooterNavigationContent" var="feature" element="section">
        <div class="container mb-5">
            <div class="row package-footer-img">
                <cms:component component="${feature}" />
            </div>
        </div>
     </cms:pageSlot>


     <cms:pageSlot position="PackageContactUsContent" var="feature" element="section">
        <div class="container">
            <div class="row mb-5">
                <cms:component component="${feature}" />
            </div>
        </div>
     </cms:pageSlot>

</template:page>
