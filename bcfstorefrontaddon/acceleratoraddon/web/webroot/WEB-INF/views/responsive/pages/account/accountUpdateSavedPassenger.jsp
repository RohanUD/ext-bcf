<%@ page trimDirectiveWhitespaces="true"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="formElement" tagdir="/WEB-INF/tags/desktop/formElement"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="ycommerce" uri="http://hybris.com/tld/ycommercetags"%>
<%@ taglib prefix="travellerdetails" tagdir="/WEB-INF/tags/addons/bcfvoyageaddon/responsive/travellerdetails"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<c:url var="submitUrl" value="/my-account/update-saved-passenger" />
<div class="account-section-header">
	<div class="row">
		<div class="container-lg col-md-6">
			<spring:theme code="text.account.profile.updateSavedPassengerDetails" />
		</div>
	</div>
</div>

<div class="row">
	<div class="container-lg col-md-12">
		<div class="account-section-content">
			<div class="account-section-form">
				<div class="panel-body">
					<form:form id="y_travellerForms" modelAttribute="travellerDetailsForms" action="${submitUrl}" method="POST">
						<travellerdetails:bcfTraveller travellerForms="${travellerDetailsForms.travellerForms}" adultsTitles="${adultsTitles}" childrenTitles="${childrenTitles}" reasonFortravel="${reasonForTravelOptions}" savedTravellers="${savedTravellers}"  isCollapsable="false"
							showPhoneNumber="false" />
						<div class="row bottom-row">
							<div class="col-xs-12 col-sm-4 pull-right backToHome">
								<button type="button" class="btn btn-secondary col-xs-12">
									<spring:theme code="text.account.profile.cancel" text="Cancel" />
								</button>
							</div>
							<div class="col-xs-12 col-sm-4 pull-right">
								<button type="submit" class="btn btn-secondary col-xs-12">
									<spring:theme code="text.account.profile.saveUpdates" text="Save Updates" />
								</button>
							</div>
						</div>
					</form:form>
				</div>
			</div>
		</div>
	</div>
</div>