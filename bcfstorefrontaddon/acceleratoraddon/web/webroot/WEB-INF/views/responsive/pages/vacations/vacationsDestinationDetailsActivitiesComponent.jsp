<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<c:if test="${not empty activities}">
    <div class="container margin-top-40">
    <h2>
        <span id="ExploreActivities" name="ExploreActivities">
            <spring:theme code="label.destination.details.exploreActivities" text="Explore activities" />
        </span>
    </h2>
        <div class="row margin-top-20">
            <c:forEach items="${activities}" var="activity">
            <div class="col-md-6 mb-5">
           
                <c:if test="${activity.images ne null }">
                	<div class="accommodation-image">
                	 <c:if test="${not empty image.promoText}">
                	  <div class="featured-bx">  ${image.promoText}</div>
                	  </c:if>
                    <div class="owl-carousel owl-theme js-owl-carousel js-owl-rotating-gallery ferry-detail-owl">
                        <c:forEach var="image" items="${activity.images}">
                         
                            <div class="image">
                            <img class='img-fluid  img-w-h js-responsive-image'
                                alt='${image.altText}' title='${image.altText}' data-media='${image.url}' />
                                </div>
                        </c:forEach>
                    </div>
                    </div>
                </c:if>
                 <div class="bg-explore-activities"> 
                <c:if test="${not empty activity.activityCategoryTypes}">
                    <c:forEach items="${activity.activityCategoryTypes}" var="categoryType">
                        <span class="${categoryType}"></span>
                    </c:forEach>
                </c:if>
                <div class="row">
                   <div class="col-lg-8 col-md-8 col-sm-7 col-xs-7 border-right">
               <h2>${activity.name}</h2> 
                <p>${activity.description}</p>
               <a href="${pageContext.request.contextPath}/vacations/activities/${activity.code}"> <b><spring:theme code="label.destination.details.learnMore" text="Learn more >" /></b></a>
                </div>
                <div class="col-lg-4 col-md-4 col-sm-5 col-xs-5  text-center">
                <p><spring:theme code="activity.details.from.text" text="From"/><br/>
                <b>${fn:escapeXml(activity.price.basePrice.formattedValue)}</b><br/>
                <span><spring:theme code="label.destination.details.per.adult" text="per adult"/></span></p>
                <a href="${pageContext.request.contextPath}/vacations/activities/${activity.code}">
                <button class="btn btn-primary btn-block"><spring:theme code="text.package.listing.button.continue" text="Book Now"/> </button></a>
                 <div>
                 </div>
                 </div>
                 </div>
                 </div>
                 </div>
            </c:forEach>
        </div>
    </div>
    <div class="container margin-top-20 margin-bottom-30 padding-bottom-30">
    <div class="row">
    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
	<div class="row">
		<div class="col-lg-4 col-md-4 col-md-offset-4">
			<a class="btn btn-primary d-block" type="submit" id="fareFinderFindButton" value="Submit">
				<spring:theme code="text.view.activities" text="View all activities" />
			</a>
		</div>
		</div>
		</div>
		</div>
	</div>
</c:if>
