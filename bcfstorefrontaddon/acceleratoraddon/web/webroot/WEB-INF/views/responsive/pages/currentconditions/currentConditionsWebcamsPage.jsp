<%@ page trimDirectiveWhitespaces="true"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="cms" uri="http://hybris.com/tld/cmstags"%>
<%@ taglib prefix="ycommerce" uri="http://hybris.com/tld/ycommercetags"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="progress"
		   tagdir="/WEB-INF/tags/addons/bcfvoyageaddon/responsive/progress" %>
<%@ taglib prefix="currentconditions"
		   tagdir="/WEB-INF/tags/addons/bcfvoyageaddon/responsive/currentconditions" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@ taglib prefix="template" tagdir="/WEB-INF/tags/responsive/common"%>
<%@ taglib prefix="ccSailingDetails"
		   tagdir="/WEB-INF/tags/addons/bcfstorefrontaddon/responsive/currentconditions" %>

<jsp:useBean id="dateValue" class="java.util.Date" />
<jsp:useBean id="now" class="java.util.Date" />

<spring:htmlEscape defaultHtmlEscape="false" />

<c:set var="terminalNames" value="${currentConditionsData.terminals}" scope="page"/>

<%-- Page Title and description --%>
<div class="container">
<p class="text-grey margin-top-10"><spring:theme code="text.cc.atAglance.header.label"
	text="At-a-glance" /></p>
<h2 class="text-dark-blue">
	<b><spring:theme code="text.cc.atAglance.webcams.page.label"
					 text="WEBCAMS"/></b>
</h2>


<div class="row">
	<div class="col-lg-8 col-md-8 col-sm-12 col-xs-12">
		<p>
			<spring:theme code="text.cc.atAglance.webcams.page.desc.label"/>
		</p>
	</div>
</div>


<c:choose>
	<c:when test="${ccServiceNotAvailable}">
		<spring:theme code="text.cc.notAvailable.label"
					  text="Services are not available"/><
	</c:when>
	<c:otherwise>
		<%-- Refresh --%>
		<div class="row">
			<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
				<p class="italic-style margin-top-20 text-sm"><em> <spring:theme code="text.cc.lastUpdated.label" text="Last updated"/> :
					<c:forEach items="${currentConditionsData.currentConditionsResponseData}" var="currentConditionsByTreminal"
							   begin="0" end="0">
						<c:if test="${not empty currentConditionsByTreminal.refreshedAt}">
							<fmt:parseDate value="${currentConditionsByTreminal.refreshedAt}" var="parsedEmpDate"
										   pattern="yyyy-MM-dd HH:mm:ss"/>
							<fmt:formatDate value="${parsedEmpDate}" pattern="h:mm a EEEE, MMM dd, yyyy"/>.
							<a href="" onclick="return false" id="refreshDepartures">
								<spring:theme code="text.cc.atAglance.click.label" text="Click"/>&nbsp;
								<spring:theme code="text.cc.atAglance.to.refresh.label" text="to refresh"/>
							</a>
						</c:if>
					</c:forEach>
				</em></p>
			</div>
		</div>
		</div>

		<section>
			<div class="container">
				<div class="row margin-top-30">
					<div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
						<p class="text-grey">
							<spring:theme code="text.find.label" text="Find"/>
						</p>
						<select class="selectpicker form-control" id="selectDepartingTerminal" name="terminalCode">
							<option value="default" disabled <c:if test='${empty param.terminalCode}'>selected</c:if>>
								<spring:theme code="label.our.terminals.make.selection" text="Select a terminal"/>
							</option>
							<c:forEach items="${sourceTerminals.terminals}" var="terminal">
								<option value="${terminal.key}"
										<c:if test='${param.terminalCode eq terminal.key}'>selected</c:if>>${sourceTerminals.terminals[terminal.key].name}
								</option>
							</c:forEach>
						</select>
					</div>
				</div>
			</div>
		</section>

		<div class="container">
			<div class="margin-top-40">
				<hr/>

				<c:forEach items="${currentConditionsData.currentConditionsResponseData}" var="ccRespData">
					<div class="webcam-main">
						<c:forEach items="${ccRespData.cams}" var="cam" varStatus="counter">
							<c:set var="camData" value="${cam.value}" scope="page"/>
							<c:if test="${counter.first}">
								<h6 class="webcam-h">
									<c:set var="arrivalDeparture" value="${ccRespData.arrivalDepartures[cam.key][0]}"/>
										${terminalNames[arrivalDeparture.dept].name}&nbsp;<spring:theme code="text.cc.terminal.label" text="Terminal"/>
									<i class="fa fa-angle-down pull-right"></i>
								</h6>
							</c:if>
							<div class="webcam-media d-none">
								<c:choose>
									<c:when test="${counter.first}">
										<currentconditions:currentconditionscam
												routeCamData="${camData}"

												displaySourceMap="true"
												sourceLocation="${arrivalDeparture.dept}"
												sourceTerminalName="${terminalNames[arrivalDeparture.dept].translatedName}"

												displayDestniationMap="true"
												destinationLocation="${camData.location}"
												destinationTerminalName="${terminalNames[camData.location].translatedName}"
										/>
									</c:when>
									<c:otherwise>
										<currentconditions:currentconditionscam
												routeCamData="${camData}"

												sourceTerminalName=""
												sourceLocation=""

												displayDestniationMap="true"
												destinationLocation="${camData.location}"
												destinationTerminalName="${terminalNames[camData.location].translatedName}"
										/>

									</c:otherwise>
								</c:choose>
							</div>
							<c:if test="${counter.last}">
								<hr/>
							</c:if>
						</c:forEach>
					</div>
				</c:forEach>


			</div>
		</div>
	</c:otherwise>
</c:choose>
