<%@ page trimDirectiveWhitespaces="true"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<c:url value="/login" var="loginPageUrl"/>
<div class="container">
    <div class="account-section-header">
        <div class="row">
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <h3 class="font-weight-bold create-account"><spring:theme code="register.new.customer" /></h3>
            </div>
        </div>
    </div>
    <div class="row my-4">
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
            <p class="fnt-14"><spring:theme code="registration.thankyou.message.title" arguments="${email}" /></p>
        </div>
    </div>
    <div class="row registration-confirmation-text">
        <div class="col-lg-9 col-md-9 col-sm-10 col-xs-12">
            <p><spring:theme code="registration.confirmation.message.title" arguments="${email}" /></p>
            <c:if test="${asmLoggedIn eq false}">
                <p><spring:theme code="registration.confirmation.message.email"/></p>
                <p><spring:theme code="registration.confirmation.message.subtitle"/></p>
            </c:if>
            <p><spring:theme code="registration.confirmation.message.update" /></p>
            <p><spring:theme code="registration.confirmation.message.welcome" /></p>
            <p><spring:theme code="registration.confirmation.message.customer.ervice.team"/></p>
            <p><spring:theme code="registration.confirmation.message.continue" /></p>
        </div>
    </div>
    <div class="row my-5 margin-top-40 margin-bottom-10">
        <div class="col-lg-3 col-md-3 col-sm-3 col-md-offset-5 col-xs-12">
            <a href="${loginPageUrl}">
                <button class="btn btn-primary btn-block" type="submit" value="Continue">
                    <spring:theme code="registration.confirmation.continue.btn.label" text="Continue"/>
                </button>
            </a>
        </div>
    </div>
</div>
