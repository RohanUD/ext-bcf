<%@ page trimDirectiveWhitespaces="true" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<div class="container">
    <div class="row">
        <div class="col-lg-12">
            <c:if test="${not empty TravelAdvisory}"> 
                <h2>
                    ${TravelAdvisory.advisoryTitle} 
                </h2>
                <div>
                    ${TravelAdvisory.advisoryDescription}
                </div>
            </c:if>
        </div>
    </div>
</div>
