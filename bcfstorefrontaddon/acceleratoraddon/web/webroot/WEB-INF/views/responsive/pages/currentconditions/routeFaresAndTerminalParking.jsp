<%@ page trimDirectiveWhitespaces="true" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>

<c:url var="switchDirectionUrl" value=""/>
<c:url var="directionsUrl" value="http://www.google.com/maps/place/"/>
<spring:htmlEscape defaultHtmlEscape="false"/>

<jsp:useBean id="now" class="java.util.Date" />

<fmt:formatDate var="scheduleDate" value="${now}" pattern="MM/dd/yyy"/>

<c:url var="fareCalculateUrl" value="/routes-fares/fare-calculator">
    <c:param name="departureLocation" value="${routeInfo.departureTerminalCode}" />
    <c:param name="arrivalLocation" value="${routeInfo.arrivalTerminalCode}" />
    <c:param name="scheduleDate" value="${scheduleDate}" />
</c:url>

<div class="container padding-0">
    <%-- Route Fares --%>
    <div class="col-lg-4 col-md-4 col-sm-6 col-xs-12 custom-detail-accordion">
        <div class="accordion"><h3><span class="blue-color bcf bcf-icon-fare-price schedules-icons"></span> <spring:theme code="text.route.fares.label" text="Route fares"/> </h3>
            <div><p><b><spring:theme code="text.fare.calculator.label"/> </b></p>
                <p><small><spring:theme code="text.fare.calculator.desc.label"/></small>
                </p>
                <a href=${fareCalculateUrl}>
                    <button class="btn btn-primary btn-block margin-top-20"><spring:theme code="text.fare.calculator.btn.label"/></button>
                </a>
                <p class="margin-top-30"><spring:theme code="text.fare.calculator.download.label"/></p>
                <a href='${downloadFareGuideUrl}' target="_blank">
                    <button class="btn btn-outline-blue margin-top-20"><spring:theme code="text.fare.calculator.download.guide.label"/></button>
                </a>
            </div>
        </div>
    </div>

    <%-- Terminal Parking --%>
    <div class="col-lg-4 col-md-4 col-sm-6 col-xs-12 custom-detail-accordion t-parking-padding">
        <c:if test="${not empty currentConditionsData}">
            <div class="js-accordion js-accordion-contentpage custom-ferrycontentpage-accordion  mb-3">
                <c:set var="currentConditionsResponseData" value="${currentConditionsData.currentConditionsResponseData[0]}"/>
                <header>
                    <span class="blue-color bcf bcf-icon-parking 5X"></span>
                    <spring:theme code="text.cc.terminalParking.parking.label" text="Parking"/>
                    <c:if test="${not empty currentConditionsResponseData.parking.parkingFullPercent}">
                        &nbsp;<spring:theme code="text.cc.terminalParking.parking.available.label" text="% Available"
                                            arguments="${100-currentConditionsResponseData.parking.parkingFullPercent}"/>
                    </c:if>
                </header>
                <div>
                    <c:if test="${not empty currentConditionsResponseData.parking.parkingFullPercent}">
                        <p class="bg-light-gray terminal-parking-dropdown">
                            <b>
                                <spring:theme code="text.cc.terminalParking.available.label" text="Terminal parking"
                                              arguments="${100-currentConditionsResponseData.parking.parkingFullPercent}"/>
                            </b>
                        </p>
                    </c:if>
                    <div class="p-4">${transportFacilityData.parkingInfo}</div>
                </div>
            </div>
        </c:if>
    </div>
</div>
<div class="clearfix"></div>
