<%@ page trimDirectiveWhitespaces="true" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="formElement"
           tagdir="/WEB-INF/tags/responsive/formElement" %>
<spring:htmlEscape defaultHtmlEscape="false"/>

<div class="row">
    <div class="col-lg-12 col-md-12 col-sm-8 col-xs-12 mb-5">
        <h4 class="my-5 margin-top-30">
            <spring:theme code="subscriptions.label" text="My subscriptions"/>
        </h4>
    </div>
</div>

<div class="my-subscriptions">
    <div class="row">
        <div class="col-md-12 col-md-12 col-xs-12">
            <div class="custom-account-profile  bc-accordion">

                <%-- Service notices --%>
                <div class="account-profile-accordion">
                    <h4 class="px-0 mb-0">
                        <spring:theme code="subscriptions.service.notices.label" text="Service notices"/>
                        <span class="ui-accordion-header-icon ui-icon bcf bcf-bcf bcf-icon-up-arrow"></span>
                        <span class="custom-arrow"></span>
                    </h4>
                    <div>
                        <div id="isAsmUser" class="hidden">${isAsmUser}</div>
                        <form:form commandName="serviceNoticesSubscriptionsForm" action="/my-account/serviceNotices"
                                   method="post">
                            <div>
                                <div>
                                    <div name="serviceNotices" class="service-notices">

                                        <label for="serviceNoticesOptedIn" class="custom-radio-input"><form:radiobutton
                                                cssClass="jsServiceNotices serviceNoticesYes" id="serviceNoticesOptedIn"
                                                path="serviceNoticesOptedIn" value="true"/> <spring:theme
                                                code="subscriptions.service.notices.optIn.label"/>
                                            <span class="checkmark"></span></label>
                                        <label for="serviceNoticesOptedIn" class="custom-radio-input"><form:radiobutton
                                                cssClass="jsServiceNotices serviceNoticesNo" id="serviceNoticesOptedIn"
                                                path="serviceNoticesOptedIn" value="false"/> <spring:theme
                                                code="subscriptions.service.notices.optOut.label"/>
                                            <span class="checkmark"></span></label>
                                    </div>
                                    <div>
                                        <p><spring:theme code="subscriptions.total.subscribed.label" text="Total subscribed"
                                                         arguments="${totalServiceNoticesSubscribed}"/></p>
                                        <c:choose>
                                            <c:when test="${not empty subscriptions.serviceNotices}">
                                                <div id="routeRegions">
                                                    <c:forEach items="${subscriptions.serviceNotices}" var="serviceNotice"
                                                               varStatus="counter">
                                                        <div class="js-accordion js-accordion-default custom-contentpage-accordion subscriptions-accordion">
                                                            <h4>${serviceNotice.groupName}</h4>
                                                            <div class="mb-3">
                                                                <div id="routeRegionsData" class="routesData">
                                                                    <c:forEach items="${serviceNotice.subscriptionMasterDetails}"
                                                                               var="subscriptionMasterEntry">
                                                                        <div class="input-group ">
                                                                            <label class="custom-checkbox-input">
                                                                                <form:checkbox path="routeCodes"
                                                                                               label="${subscriptionMasterEntry.name}"
                                                                                               value="${subscriptionMasterEntry.code}"/>
                                                                                <span class="checkmark-checkbox"></span>
                                                                            </label>
                                                                        </div>
                                                                    </c:forEach>
                                                                </div>
                                                            </div>
                                                        </div>

                                                    </c:forEach>
                                                </div>
                                            </c:when>
                                            <c:otherwise>
                                                <h3><spring:theme code="subscriptions.serviceNotice.empty.label"
                                                                  text="Service notices are empty"/></h3>
                                            </c:otherwise>
                                        </c:choose>
                                    </div>
                                    <p class="select-save-text"><spring:theme code="subscriptions.save.description.label"/></p>
                                </div>

                                <div class="row">
                                    <div class="col-md-offset-4 col-md-5 margin-bottom-20">
                                        <div class="accountActions">
                                            <ycommerce:testId code="personalDetails_savePersonalDetails_button">
                                                <button type="submit" class="btn btn-primary btn-block">
                                                    <spring:theme code="text.account.profile.saveUpdates" text="Save Updates"/>
                                                </button>
                                            </ycommerce:testId>
                                        </div>
                                    </div>
                                    <div class="col-md-offset-4 col-md-5">
                                        <div class="accountActions">
                                            <ycommerce:testId code="personalDetails_cancelPersonalDetails_button">
                                                <button type="button" class="btn btn-outline-primary btn-block backToSamePage">
                                                    <spring:theme code="text.account.profile.cancel" text="Cancel"/>
                                                </button>
                                            </ycommerce:testId>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </form:form>
                    </div>
                </div>


                <%-- News Releases --%>
                <div class="account-profile-accordion">
                    <h4 class="px-0 mb-0">
                        <spring:theme code="subscriptions.news.releases.label" text="News releases"/>
                        <span class="ui-accordion-header-icon ui-icon bcf bcf-bcf bcf-icon-up-arrow"></span>
                        <span class="custom-arrow"></span>
                    </h4>
                    <div>
                        <form action="/my-account/newsRelease" method="get">
                            <div>
                                <div class="row">
                                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">

                                        <label class="custom-radio-input"><input type="radio" class="jsNewsRelease"
                                                                                 id="newsReleaseYes" name="newsRelease"
                                                                                 value="true" select
                                                                                 <c:if test='${newsReleasesOptedIn}'>checked</c:if>/>
                                            <spring:theme
                                                    code="subscriptions.news.releases.optIn.label"/>
                                            <span class="checkmark"></span></label>
                                        <label class="custom-radio-input"><input type="radio" class="jsNewsRelease"
                                                                                 id="newsReleaseNo" name="newsRelease"
                                                                                 value="false"
                                                                                 <c:if test='${!newsReleasesOptedIn}'>checked</c:if>/>
                                            <spring:theme
                                                    code="subscriptions.news.releases.optOut.label"/>
                                            <span class="checkmark"></span></label>
                                    </div>
                                </div>
                                <p class="select-save-text"><spring:theme code="subscriptions.save.description.label"/></p>
                                <div class="row">
                                    <div class="col-md-offset-4 col-md-5 margin-bottom-20">
                                        <div class="accountActions">
                                            <ycommerce:testId code="personalDetails_savePersonalDetails_button">
                                                <button type="submit" class="btn btn-primary btn-block">
                                                    <spring:theme code="text.account.profile.saveUpdates" text="Save Updates"/>
                                                </button>
                                            </ycommerce:testId>
                                        </div>
                                    </div>
                                    <div class="col-md-offset-4 col-md-5">
                                        <div class="accountActions">
                                            <ycommerce:testId code="personalDetails_cancelPersonalDetails_button">
                                                <button type="button" class="btn btn-outline-primary btn-block backToSamePage">
                                                    <spring:theme code="text.account.profile.cancel" text="Cancel"/>
                                                </button>
                                            </ycommerce:testId>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>

                <%-- Special offers and promotions --%>
                <div class="account-profile-accordion">
                    <h4 class="px-0 mb-0">
                        <spring:theme code="subscriptions.special.offers.promotions.label" text="Special offers and promotions"/>
                        <span class="ui-accordion-header-icon ui-icon bcf bcf-bcf bcf-icon-up-arrow"></span>
                        <span class="custom-arrow"></span>
                    </h4>
                    <div>
                        <form action="/my-account/offersAndPromotions" method="get">
                            <div>
                                <div class="row">
                                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">

                                        <label class="custom-radio-input"><input type="radio" id="offersAndPromotionsYes" name="offersAndPromotions" value="true"
                                                                                 <c:if test='${offersAndPromotionsOptedIn}'>checked</c:if>/>
                                            <spring:theme
                                                    code="subscriptions.special.offers.promotions.optIn.label"/>
                                            <span class="checkmark"></span></label>
                                        <label class="custom-radio-input"><input type="radio" id="offersAndPromotionsNo" name="offersAndPromotions" value="false"
                                                                                 <c:if test='${!offersAndPromotionsOptedIn}'>checked</c:if> />
                                            <spring:theme code="subscriptions.special.offers.promotions.optOut.label"/>
                                            <span class="checkmark"></span></label>
                                    </div>
                                </div>
                                <p class="select-save-text"><spring:theme code="subscriptions.save.description.label"/></p>

                                <div class="row">
                                    <div class="col-md-offset-4 col-md-5 margin-bottom-20">
                                        <div class="accountActions">
                                            <ycommerce:testId code="personalDetails_savePersonalDetails_button">
                                                <button type="submit" class="btn btn-primary btn-block">
                                                    <spring:theme code="text.account.profile.saveUpdates" text="Save Updates"/>
                                                </button>
                                            </ycommerce:testId>
                                        </div>
                                    </div>
                                    <div class="col-md-offset-4 col-md-5">
                                        <div class="accountActions">
                                            <ycommerce:testId code="personalDetails_cancelPersonalDetails_button">
                                                <button type="button" class="btn btn-outline-primary btn-block backToSamePage">
                                                    <spring:theme code="text.account.profile.cancel" text="Cancel"/>
                                                </button>
                                            </ycommerce:testId>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </form>

                    </div>
                </div>

                <%-- New Business Opportunities --%>
                <c:if test="${displayBusinessOpportunities}">
                    <div class="account-profile-accordion">
                        <h4 class="px-0 mb-0">
                            <spring:theme code="subscriptions.business.opportunities.promotions.label"
                                          text="New business opportunities"/>
                            <span class="ui-accordion-header-icon ui-icon bcf bcf-bcf bcf-icon-up-arrow"></span>
                            <span class="custom-arrow"></span>
                        </h4>
                        <div>

                            <form:form commandName="businessOpportunitiesSubscriptionsForm"
                                       action="/my-account/businessOpportunities" method="post">
                                <div>
                                    <div class="row mb-5">
                                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">

                                            <p class="mb-5"><b><form:radiobutton id="businessOpportunitiesOptedIn"
                                                                                 path="businessOpportunitiesOptedIn" value="true"
                                                                                 cssClass="businessOpportunities"/> <spring:theme
                                                    code="subscriptions.business.opportunities.optIn.label"/></b></p>
                                            <p class="mb-5"><b><form:radiobutton id="businessOpportunitiesOptedIn"
                                                                                 path="businessOpportunitiesOptedIn" value="false"
                                                                                 cssClass="businessOpportunities"/> <spring:theme
                                                    code="subscriptions.business.opportunities.optOut.label"/></b></p>
                                        </div>
                                        <div>
                                            <p class="grey-text"><spring:theme
                                                    code="subscriptions.business.opportunities.save.info.lable"/></p>
                                        </div>
                                        <div><p class="h3"><spring:theme
                                                code="subscriptions.business.opportunities.details.lable"/></p></div>
                                        <div>
                                            <formElement:formInputBox inputCSS="jsCompanyName" path="companyName"
                                                                      labelKey="subscriptions.business.opportunities.company.lable"
                                                                      idKey="subscriptions.business.opportunities.company.lable"
                                                                      placeholder="subscriptions.business.opportunities.company.placeHolder.lable"/>
                                            <formElement:formInputBox inputCSS="jsJobTitle" path="jobTitle"
                                                                      labelKey="subscriptions.business.opportunities.title.lable"
                                                                      idKey="subscriptions.business.opportunities.title.lable"
                                                                      placeholder="subscriptions.business.opportunities.title.placeHolder.lable"/>
                                        </div>
                                        <div>
                                            <p class="h3"><spring:theme code="subscriptions.service.supplies.title.lable"/></p>
                                            <p class="grey-text"><spring:theme
                                                    code="subscriptions.service.supplies.desc.lable"/></p>
                                        </div>
                                        <div>
                                            <form:checkboxes items="${serviceSupplies}" path="serviceSupplies"
                                                             id="serviceSupplies" itemLabel="name" itemValue="code"
                                                             cssClass="display-block"/>
                                        </div>
                                    </div>
                                    <div class="grey-text"><spring:theme code="subscriptions.save.description.label"/></div>

                                    <div class="row">
                                        <div class="col-sm-6 col-sm-push-6 mb-5">
                                            <div class="accountActions">
                                                <ycommerce:testId code="personalDetails_savePersonalDetails_button">
                                                    <button type="submit" class="btn btn-primary btn-block">
                                                        <spring:theme code="text.account.profile.saveUpdates"
                                                                      text="Save Updates"/>
                                                    </button>
                                                </ycommerce:testId>
                                            </div>
                                        </div>
                                        <div class="col-sm-6 col-sm-pull-6 mb-5">
                                            <div class="accountActions">
                                                <ycommerce:testId code="personalDetails_cancelPersonalDetails_button">
                                                    <button type="button"
                                                            class="btn btn-outline-primary btn-block backToSamePage">
                                                        <spring:theme code="text.account.profile.cancel" text="Cancel"/>
                                                    </button>
                                                </ycommerce:testId>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </form:form>


                        </div>
                    </div>
                </c:if>


            </div>
        </div>

    </div>
</div>
</div>
