<%@ page trimDirectiveWhitespaces="true" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix = "fmt" uri = "http://java.sun.com/jsp/jstl/fmt" %>

<div class="container">
    <div class="row">
        <div class="col-12 col-md-12">
        
            <c:if test="${not empty SubstantialPerformanceNotice}">
                <h2>${SubstantialPerformanceNotice.title}</h2>
                    <p>${SubstantialPerformanceNotice.bodyContent}</p>

                <p><strong><spring:theme code="text.substantial.performance.owner" text="Owner" /></strong>
                <span>${SubstantialPerformanceNotice.productOwner} ${SubstantialPerformanceNotice.productOwnerAddress}</span></p>

                <p><strong><spring:theme code="text.substantial.performance.contractor" text="Contractor" /></strong>
                ${SubstantialPerformanceNotice.productOwner}
                </p>
                
                <p><strong><spring:theme code="text.substantial.performance.consultant" text="Consultant" /></strong>
                ${SubstantialPerformanceNotice.consultant}
                <br>
                ${SubstantialPerformanceNotice.additionalNote}</p>

                <h3><spring:theme code="text.substantial.performance.consultant" text="Key Dates" /></h3>  
                <p><spring:theme code="text.service.notice.published" text="Published" />:
                <fmt:formatDate value="${SubstantialPerformanceNotice.publishedDate}" pattern="MMMM dd, yyyy" />
                <br>
                <spring:theme code="text.service.notice.closingDate" text="Closing Date" />:
                <fmt:formatDate value="${SubstantialPerformanceNotice.expiryDate}" pattern="MMMM dd, yyyy" />
                <br>
                <spring:theme code="text.service.notice.closingTime" text="Closing Time" />:
                <fmt:formatDate type = "time"  value = "${SubstantialPerformanceNotice.expiryDate}" timeStyle = "short"/>
                </p>

                <h3><spring:theme code="text.substantial.performance.primaryContact" text="Primary Contact" /></h3>
                <p>${SubstantialPerformanceNotice.primaryContact}</p>
            </c:if>

            <a class="btn btn-primary btn-lg closebtn" role="button" href="/business-ops">
                <spring:theme code="text.business.opportunity.back" text="Back to Listings" />
            </a>
        </div>
    </div>
</div>