<%@ page trimDirectiveWhitespaces="true"%>
<%@ taglib prefix="template" tagdir="/WEB-INF/tags/responsive/common"%>
<%@ taglib prefix="cms" uri="http://hybris.com/tld/cmstags"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="format" tagdir="/WEB-INF/tags/addons/bcfstorefrontaddon/responsive/format"%>
<%@ taglib prefix="bcfglobalreservation" tagdir="/WEB-INF/tags/addons/bcfcommonsaddon/responsive/bcfglobalreservation"%>
<%@ taglib prefix="accommodationReservation" tagdir="/WEB-INF/tags/addons/bcfcommonsaddon/responsive/accommodationreservation"%>
<spring:url value="/quote/" var="editQuoteUrl" />
<spring:htmlEscape defaultHtmlEscape="false" />
<template:page pageTitle="${pageTitle}">
	<div class="container">
	<h4 class="margin-bottom-30">
    	<spring:theme code="label.quote.details.page.header" text="Quote details" />
    </h4>


    <h4 class="margin-bottom-30">
        <spring:theme code="label.quote.id" /> ${quoteID}
        <fmt:formatDate value="${quoteExpiryDate}" pattern="E MMM dd,yyyy" var="quoteExpiryDate" />
        <div style="clear:both;"/><spring:theme code="label.quote.expires" /> ${quoteExpiryDate}
    </h4>

		<div class="row">
			<div class="col-xs-12 col-md-12 col-sm-9">
			<c:if test="${not empty bcfGlobalReservationData.accommodationReservations}">
            <c:forEach items="${bcfGlobalReservationData.accommodationReservations.accommodationReservations}" var="accommodationReservation" varStatus="accommodationReservationItemIdx">
                <div class="row">
                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 px-5">
                        <ul class="vacation-booking-box">
                            <li><accommodationReservation:accommodationReservation accommodationReservation="${accommodationReservation}" accommodationResVarStatus="1" /></li>
                        </ul>
                    </div>
                </div>
            </c:forEach>
            </c:if>
			<c:if test="${not empty bcfGlobalReservationData.packageReservations}">
			<c:forEach items="${bcfGlobalReservationData.packageReservations.packageReservationDatas}" var="packageReservation" varStatus="packageReservationItemIdx">
                <div class="row">
                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 px-5">
                        <ul class="vacation-booking-box">
                            <li><accommodationReservation:accommodationReservation accommodationReservation="${packageReservation.accommodationReservationData}" accommodationResVarStatus="1" /></li>
                        <hr>
                            <c:if test="${not empty packageReservation.reservationData}">
                                <li><bcfglobalreservation:transportReservation reservationData="${packageReservation.reservationData}" /></li>
                            </c:if>
                        </ul>
                    </div>
                </div>
                <c:if test="${not empty packageReservation.activityReservations}">

                    <bcfglobalreservation:activityQuoteReservations activityReservationDataList="${packageReservation.activityReservations}" />
                </c:if>
            </c:forEach>
            </c:if>
            <c:if test="${not empty bcfGlobalReservationData.transportReservations}">
            <c:forEach items="${bcfGlobalReservationData.transportReservations.reservationDatas}" var="transportReservation" varStatus="transportReservationItemIdx">
                <div class="row">
                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 px-5">
                        <ul class="vacation-booking-box">
                            <li><bcfglobalreservation:transportReservation reservationData="${transportReservation}" /></li>
                        </ul>
                    </div>
                </div>
            </c:forEach>
            </c:if>
            <c:if test="${not empty bcfGlobalReservationData.activityReservations}">
                <div class="row">
                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 px-5">
                        <ul class="vacation-booking-box">

                            <li><bcfglobalreservation:activityQuoteReservations activityReservationDataList="${bcfGlobalReservationData.activityReservations}" /></li>
                        </ul>
                    </div>
                </div>
            </c:if>
            	<c:if test="${not empty bcfGlobalReservationData}">
            		<bcfglobalreservation:totalFare globalReservationData="${bcfGlobalReservationData}" />
            	</c:if>

			</div>
		</div>
		<input type="hidden" name="quoteCode" value="${bcfGlobalReservationData.code}" id="y_quoteCode" />
        <input type="hidden" name="sessionCartCode" value="${sessionCartCode}" id="sessionCartCode"/>
        <c:choose>
            <c:when test="${bcfGlobalReservationData.bookingJourneyType eq 'BOOKING_ALACARTE'}">
              <c:choose>
                <c:when test="${isASMSession}">
                 <form class="editQuoteForm" method="get" action="${fn:escapeXml(editQuoteUrl)}${bcfGlobalReservationData.code}/edit">
                    <input type="hidden" name="isSaveCartCurrentSession" value="true" id="y_isSaveCartCurrentSession" />
                 </form>

                 <button type="submit" class="btn btn-primary btn-block" id="y_checkSessionCart">
                   <spring:theme code="text.page.create.booking.for.quote" />
                 </button>

                 <div class="col-lg-4 col-lg-offset-4 col-md-offset-4 col-md-4 col-sm-12 col-xs-12">
                      <a href="/option-booking/convert-quote-to-option/${bcfGlobalReservationData.code}" class="btn btn-primary btn-block"><spring:theme code="label.fareselectionreview.button.save.as.optionbooking" /></a>
                  </div>
                </c:when>

                <c:otherwise>
                    <button type="button" class="btn btn-primary btn-block">
                     <spring:theme code="label.quote.calltobook" />
                    </button>
                </c:otherwise>
               </c:choose>
            </c:when>
            <c:otherwise>
                <form class="editQuoteForm" method="get" action="${fn:escapeXml(editQuoteUrl)}${bcfGlobalReservationData.code}/edit">
                    <input type="hidden" name="isSaveCartCurrentSession" value="true" id="y_isSaveCartCurrentSession" />
                </form>

                <c:if test="${isASMSession ||  not isQuoteExpired}">
                <button type="submit" class="btn btn-primary btn-block" id="y_checkSessionCart">
                                   <spring:theme code="text.page.create.booking.for.quote" />
                                </button>
                </c:if>


                <c:if test="${isASMSession}">
                    <div class="col-lg-4 col-lg-offset-4 col-md-offset-4 col-md-4 col-sm-12 col-xs-12">
                        <a href="/option-booking/convert-quote-to-option/${bcfGlobalReservationData.code}" class="btn btn-primary btn-block"><spring:theme code="label.fareselectionreview.button.save.as.optionbooking" /></a>
                    </div>
                </c:if>

            </c:otherwise>
        </c:choose>


	</div>

   <cms:pageSlot position="InternalComments" var="features">
      <cms:component component="${features}" element="section" />
  </cms:pageSlot>

<div class="modal fade" tabindex="-1" role="dialog" id="quoteEditConfirm">
   <div class="modal-dialog" role="document">
      <div class="modal-content">
         <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true"><i class="bcf bcf-icon-cancel-solid"></i></span>
            </button>
            <h4 class="modal-title">
               <spring:theme code="label.remove.session.cart" text="Remove Existing Cart" />
            </h4>
         </div>
         <div class="modal-body">
            <p>
               <spring:theme code="label.remove.session.cart.message" text="Are you sure you want to remove existing cart? This action cannot be undone." />
            </p>
         </div>
         <div class="modal-footer">
             <button type="button" class="btn btn-transparent" id="y_confirmedCurrentCartCancellation">
               <spring:theme code="text.company.disable.confirm.yes" />
            </button>
            <button type="button" class="btn btn-transparent" data-dismiss="modal" id="y_noCurrentCartCancellation">
               <spring:theme code="text.company.disable.confirm.no" text="No" />
            </button>
         </div>
      </div>
   </div>
</div>

</template:page>
