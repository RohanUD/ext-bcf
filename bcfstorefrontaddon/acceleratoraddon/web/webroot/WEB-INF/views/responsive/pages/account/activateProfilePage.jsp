<%@ page trimDirectiveWhitespaces="true"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="template" tagdir="/WEB-INF/tags/responsive/common"%>
<%@ taglib prefix="cms" uri="http://hybris.com/tld/cmstags"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="formElement" tagdir="/WEB-INF/tags/responsive/formElement"%>
<%@ taglib prefix="ycommerce" uri="http://hybris.com/tld/ycommercetags"%>
<%@ taglib prefix="common" tagdir="/WEB-INF/tags/responsive/common"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>

<div class="account-section-header">
	<div class="row mb-5">
		<div class=" col-md-6">
			<p><b><spring:theme code="text.account.profile.activate" /></b></p>
		</div>
	</div>
</div>

<p>
	<span class="red-text">*</span>
	<spring:theme code="fields.required.desc" text="These fields are required"/>
</p>

<form:form method="post" commandName="activateProfileForm" action="${action}" class="activate-profile-form">
	<c:set var="selctAccountType"><spring:theme code="text.account.select.accountType"/></c:set>

	<formElement:formSelectBox idKey="registerAccountType" labelKey="register.accountType"
							   path="accountType" mandatory="true" skipBlank="false"
							   skipBlankMessageKey="text.account.select.accountType"
							   items="${accountTypes}" itemValue="code" itemLabel="name" tabindex="10"
							   selectCSSClass="form-control"/>

	<formElement:formInputBox idKey="register.email"
							  labelKey="register.email" path="email" inputCSS="form-control"
							  mandatory="true" disabled="true"/>

	<formElement:formInputBox idKey="register.firstName"
							  labelKey="register.firstName" path="firstName" inputCSS="form-control"
							  mandatory="true" />

	<formElement:formInputBox idKey="register.lastName"
							  labelKey="register.lastName" path="lastName" inputCSS="form-control"
							  mandatory="true" />

	<div id="registerMobileNumber">
		<formElement:formInputBox idKey="register.mobileNumber" labelKey="register.mobileNumber" path="mobileNumber"
								  inputCSS="form-control" mandatory="true"/>
	</div>

	<div id="registerCountryBox">
		<formElement:formSelectBox idKey="registerCountry" labelKey="register.country"
								   path="country" mandatory="true" skipBlank="false"
								   skipBlankMessageKey="register.country.dropdown.message"
								   items="${countries}" itemValue="isocode" tabindex="10"
								   selectCSSClass="form-control"/>
	</div>

	<div id="registerZipcodeBox">
		<formElement:formInputBox idKey="registerZipCode"
								  labelKey="register.zipCode" path="zipCode" inputCSS="form-control"
								  mandatory="true" />
	</div>

	<div id="registerAddressLine1">
		<formElement:formInputBox idKey="register.addressLine1" labelKey="register.streetAddress" path="addressLine1"
								  inputCSS="text"/>
	</div>

	<div id="registerCity">
		<formElement:formInputBox idKey="register.city" labelKey="register.city" path="city" inputCSS="text"/>
	</div>

	<div id="registerRegionBox">
		<div class="form-group">
			<formElement:formSelectBox idKey="name" labelKey="register.province" path="province" items="${regions}"
									   itemValue="isocode" selectCSSClass="billing-address-regions form-control"
									   skipBlankMessageKey="profile.address.region.dropdown.message"/>
		</div>
	</div>

	<c:if test="${not empty userGroups}">
		<spring:theme code="register.asm.usergroup" />
		<form:select path="userGroup" class="form-control">
			<option value="" selected="selected" disabled="disabled">Select User Group</option>
			<form:options items="${userGroups}" itemValue="uid" itemLabel="uid"/>
		</form:select>
	</c:if>

	<c:set var="promotions" value="${subscriptions.promotions}"/>
	<div class="form-group">
		<label class="custom-checkbox-input">
			<form:checkbox idKey="${promotions.name}" path="optInSubscriptions" value="${promotions.name}"/>
			<spring:theme code="profile.activate.subscribe.${promotions.type}"/>
			<span class="checkmark-checkbox"></span>
		</label>
	</div>

	<c:set var="newsRelease" value="${subscriptions.newsReleases}"/>
	<div class="form-group">
		<label class="custom-checkbox-input">
			<form:checkbox idKey="${newsRelease.name}" path="optInSubscriptions" value="${newsRelease.name}"/>
			<spring:theme code="profile.activate.subscribe.${newsRelease.type}"/>
			<span class="checkmark-checkbox"></span>
		</label>
	</div>

	<div class="form-group">
		<label class="custom-checkbox-input">
			<spring:theme code="text.account.profile.activate.serviceNotice.message" htmlEscape="false"/>
		</label>
	</div>

	<input type="hidden" id="recaptchaChallangeAnswered"
		   value="${requestScope.recaptchaChallangeAnswered}" />
	<div class="form_field-elements control-group js-recaptcha-captchaaddon"></div>
	<div class="form-actions clearfix">
		<ycommerce:testId code="register_Register_button">
			<button type="submit" class="btn btn-default">
				<spring:theme code='${actionNameKey}' />
			</button>
		</ycommerce:testId>
	</div>
	<div class="row">
		<div class="col-lg-3 col-md-3 col-md-offset-5 col-sm-3">
			<button type="submit" class="btn btn-primary btn-block"><spring:theme code="text.activate.profile.button"/> </button>
		</div>
	</div>
</form:form>
