<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<div class="container">
<h2 class="text-dark-blue">
        <span id="Overview" name="Overview">
            <spring:theme code="label.destination.details.overview" text="OVERVIEW" />
       </span>
</h2>
<div class="row">

<div class="col-lg-8 col-md-8 col-sm-12 col-xs-12">
<p>${destinationDetails.description}</p>
</div>
</div>
</div>

