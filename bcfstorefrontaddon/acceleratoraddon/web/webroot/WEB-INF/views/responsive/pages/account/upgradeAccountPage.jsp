<%@ page trimDirectiveWhitespaces="true"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="formElement" tagdir="/WEB-INF/tags/responsive/formElement"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<spring:htmlEscape defaultHtmlEscape="false" />

<div class="subscription-box-div">
    <ul>
        <li><a href="${pageContext.request.contextPath}/my-account/profile"><spring:theme code="text.account.title"/></a></li>
        <li><a href="${pageContext.request.contextPath}/my-account/subscriptions"><spring:theme code="subscriptions.label"/></a></li>
    </ul>
</div>

<div class="js-accordion js-accordion-default custom-contentpage-accordion upgrade-account">
    <h4>
        <spring:theme code="upgrade.account"/>
    </h4>
    <div>
        <div id="personal-details">
            <%-- List group --%>
            <h5><spring:theme code="text.upgrade.account.changeToPersonalAccount.label"/></h5>
            <ul class="margin-bottom-30 changePersonalAccount">
                <li>
                    <spring:theme code="text.upgrade.account.reserve.items.label"/>
                </li>
                <li>
                    <spring:theme code="text.upgrade.account.viewAndManage.sailings.items.label"/>
                </li>
                <li>
                    <spring:theme code="text.upgrade.account.receiveOffers.items.label"/>
                </li>
            </ul>

            <p class="margin-bottom-30"><spring:theme code="text.upgrade.account.fillaForm.desc.label"/></p>

            <form:form action="/upgrade-account" commandName="upgradeAccountForm" method="post">
                <div class="col-md-8 col-sm-12 col-xs-12 col-md-offset-2">

                    <div id="registerMobileNumber">
                        <formElement:formInputBox idKey="register.mobileNumber" labelKey="register.mobileNumber" path="mobileNumber"
                                                  inputCSS="form-control" mandatory="true"/>
                    </div>

                    <div id="registerCity">
                        <formElement:formInputBox idKey="register.city" labelKey="register.city" path="city" inputCSS="text" mandatory="true"/>
                    </div>

                    <div id="registerZipcodeBox">
                        <formElement:formInputBox idKey="registerZipCode"
                                                  labelKey="register.zipCode" path="zipCode" inputCSS="form-control"
                                                  mandatory="true" />
                    </div>

                    <div id="registerCountryBox">
                        <formElement:formSelectBox idKey="registerCountry" labelKey="register.country"
                                                   path="country" mandatory="true" skipBlank="false"
                                                   skipBlankMessageKey="register.country.dropdown.message"
                                                   items="${countries}" itemValue="isocode" tabindex="10"
                                                   selectCSSClass="form-control"/>
                    </div>

                    <div id="registerRegionBox">
                        <div class="form-group">
                            <formElement:formSelectBox idKey="billing-address-regions" labelKey="profile.address.region"
                                                       path="province" items="${regions}"
                                                       skipBlankMessageKey="text.page.default.select"
                                                       selectCSSClass="billing-address-regions form-control" mandatory="true" itemLabel="name" itemValue="isocode"/>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-8 col-md-offset-2 mt-3 margin-top-30">
                            <div class="accountActions">

                                <button type="submit" class="btn btn-primary btn-block backToSamePage">
                                    <spring:theme code="text.account.upgrade.submit" text="Submit"/>
                                </button>
                            </div>
                        </div>
                    </div>
                </div>
            </form:form>
        </div>
    </div>
</div>

