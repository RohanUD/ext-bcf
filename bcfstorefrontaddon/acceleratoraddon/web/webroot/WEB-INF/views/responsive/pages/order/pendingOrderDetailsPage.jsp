<%@ page trimDirectiveWhitespaces="true"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="nav" tagdir="/WEB-INF/tags/responsive/nav" %>
<%@ taglib prefix="ycommerce" uri="http://hybris.com/tld/ycommercetags" %>

<c:set var="searchUrl" value="/pendingOrderDetailsPage?sort=${ycommerce:encodeUrl(searchPageData.pagination.sort)}"/>

<div>
    <h2> On Request Queue </h2>

    <c:if test="${not empty searchPageData.results}">
        <table class="table" id="pendingOrdersContentList">
            <thead>
                <tr>
                    <th><spring:theme code="text.page.pendingorders.header.order.number" text="Booking Reference" /></th>
                    <th><spring:theme code="text.page.pendingorders.header.order.placedby" text="Guest Name" /></th>
                    <th><spring:theme code="text.page.pendingorders.header.order.created.time" text="Date Created" /></th>
                    <th><spring:theme code="text.page.pendingorders.header.order.subtotal" text="Total Price" /></th>
                    <th><spring:theme code="text.page.pendingorders.header.order.departure.time" text="Departure Date" /></th>
                    <th><spring:theme code="text.page.pendingorders.header.order.locked.by" text="Locked By Agent" /></th>
                    <th></th>
                    <th></th>
                </tr>
            </thead>
            <tbody>
                <c:forEach items="${searchPageData.results}" var="order">
                        <tr class="pendingOrdersContentRow">
                            <td><a href="<c:url value="/pending-orders-onrequest/orderdetail/${order.code}"/>">${order.code}</a></td>
                            <td><c:out value="${order.placedBy}" /></td>
                            <td>
                                <fmt:formatDate value="${order.created}" var="orderCreatedFormatted" pattern="dd/MM/yyyy HH:mm:ss" />
                                <c:out value="${orderCreatedFormatted}" />
                            </td>
                            <td><c:out value="${order.totalPrice.formattedValue}" /></td>
                            <td>
                                <fmt:formatDate value="${order.departureTime}" var="departureTimeFormatted" pattern="dd/MM/yyyy HH:mm:ss" />
                                <c:out value="${departureTimeFormatted}" />
                            </td>
                            <td class="lockedBy"><c:out value="${order.lockedBy}" /></td>
                            <c:choose>
                                <c:when test="${empty order.lockedBy}">
                                    <td class="lockBtn">
                                        <a class="btn btn-primary btn-block y_lockOrderButton" href="<c:url value="/pendingOrderDetailsPage/lock/${order.code}"/>">
                                            <spring:theme code="text.page.pendingorders.lockorder" text="Lock Order" />
                                        </a>
                                    </td>
                                </c:when>
                                <c:otherwise>
                                    <td class="lockBtn">
                                        <a class="btn btn-primary btn-block y_lockOrderButton hide" href="<c:url value="/pendingOrderDetailsPage/lock/${order.code}"/>">
                                            <spring:theme code="text.page.pendingorders.lockorder" text="Lock Order" />
                                        </a>
                                    </td>
                                </c:otherwise>
                            </c:choose>
                            <td class="unlockBtn">
                                <a class="btn btn-primary btn-block y_unlockOrderButton" href="<c:url value="/pendingOrderDetailsPage/unlock/${order.code}"/>">
                                    <spring:theme code="text.page.pendingorders.unlockorder" text="Unlock Order" />
                                </a>
                            </td>
                        </tr>
                </c:forEach>
            </tbody>
        </table>
    </c:if>
</div>

<div class="modal fade" id=" " tabindex="-1" role="dialog" aria-labelledby="y_asmOrderActionMessageModal">
	<div class="modal-dialog" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true"><i class="bcf bcf-icon-cancel-solid"></i></span>
				</button>
				<h3 class="modal-title" id="y_asmOrderActionTitle">
					<spring:theme code="text.page.pendingorders.actions.modal.title" text="Response" />
				</h3>
			</div>
			<div class="modal-body">
				<div id="y_asmOrderActionMessage"></div>
			</div>
		</div>
	</div>
</div>

