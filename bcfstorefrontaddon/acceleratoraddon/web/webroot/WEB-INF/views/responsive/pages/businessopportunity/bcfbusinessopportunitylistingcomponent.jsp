<%@ page trimDirectiveWhitespaces="true" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix = "fmt" uri = "http://java.sun.com/jsp/jstl/fmt" %>

<div class="container">
<div class="row">
	<div class="col-12 col-md-12">
        <h2><spring:theme code="text.competitions.list" text="Active Competitions" /></h2>

    <c:if test ="${not empty competitionsList and fn:length(competitionsList) gt 0}">
        <div class="row">
            <c:forEach items="${competitionsList}" var="competition">
                
                <div class="col-12 col-md-6">
                    <h5>
                        <a href="/business-ops/competition/${competition.code}">
                        ${competition.title}
                    </a>
                    </h5>
                </div>

                <div class="col-3 col-md-3"><h5>
                    <spring:theme code="text.service.notice.published" text="Published" />:
                    <strong><fmt:formatDate value="${competition.publishedDate}" pattern="MMMM dd, yyyy" /></strong></h5>
                </div>

                <div class="col-3 col-md-3"><h5>
                    <spring:theme code="text.service.notice.closingDate" text="Closing Date" />:
                    <strong><fmt:formatDate value="${competition.expiryDate}" pattern="MMMM dd, yyyy" /></strong></h5>
                </div>
            </c:forEach>
        </div>
    </c:if>


<c:if test ="${empty competitionsList or fn:length(competitionsList) eq 0}">
    <spring:theme code="text.competition.not.exists" text="No Competition exists" />
</c:if>

<div class="row">
	<div class="col-12 col-md-12">

    <h2><spring:theme code="text.substantial.performance.notices.list" text="Notices of Substantial Performance" /></h2>

    <c:if test ="${not empty substantialPerformanceNoticesList and fn:length(substantialPerformanceNoticesList) gt 0}">
    <div class="row">
        <c:forEach items="${substantialPerformanceNoticesList}" var="substantialPerformanceNotice">
            <div class="col-12 col-md-6">
                <h5>
                    <a href="/business-ops/substantialPerformanceNotice/${substantialPerformanceNotice.code}">
                        ${substantialPerformanceNotice.title}
                    </a>
                </h5>
            </div>

            <div class="col-3 col-md-3"><h5>
            <spring:theme code="text.service.notice.published" text="Published Date" />:
            <strong><fmt:formatDate value="${substantialPerformanceNotice.publishedDate}" pattern="MMMM dd, yyyy" /></strong>
            </h5>
        </div>

            <div class="col-3 col-md-3">
            <h5>
                <spring:theme code="text.service.notice.closingDate" text="Closing Date" />:
                <strong><fmt:formatDate value="${substantialPerformanceNotice.expiryDate}" pattern="MMMM dd, yyyy" /></strong>
            </h5>
            </div>

        </c:forEach>
    </c:if>


        <c:if test ="${empty substantialPerformanceNoticesList or fn:length(substantialPerformanceNoticesList) eq 0}">
            <spring:theme code="text.substantialPerformanceNotice.not.exists" text="No Notices of Substantial Performance exists" />
        </c:if>
    </div>
 </div>

        </div>
    </div>
</div>