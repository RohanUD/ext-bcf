<%@ page trimDirectiveWhitespaces="true" contentType="application/json"%>
<%@ taglib prefix="json" uri="http://www.atg.com/taglibs/json"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>

<%@ taglib prefix="ccSailingDetails" tagdir="/WEB-INF/tags/addons/bcfstorefrontaddon/responsive/currentconditions" %>

<json:object escapeXml="false">
<json:property name="htmlContent">

<div id="ccDetails">
<ccSailingDetails:currentConditionsSailingDetails currentConditionsData="${currentConditionsData}" />
<ccSailingDetails:currentConditionsDepartures currentConditionsData="${currentConditionsData}" routeInfo="${routeInfo}" />
</div>

</json:property>
</json:object>
