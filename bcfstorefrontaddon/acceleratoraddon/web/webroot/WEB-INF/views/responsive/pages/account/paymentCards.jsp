<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="ycommerce" uri="http://hybris.com/tld/ycommercetags"%>
<%@ taglib prefix="payment" tagdir="/WEB-INF/tags/addons/bcfstorefrontaddon/responsive/account/payment"%>
<c:url var="addPaymentUrl" value="./payment-cards/add" />
<c:choose>
	<c:when test="${paymentCards ne null}">
		<div class="custom-account-profile bc-accordion">
			<h6>
				<spring:theme code="text.account.saved.payment.cards" />
			</h6>
			<payment:CreditCardPaymentInfos savedCreditCards="${paymentCards.savedCards}"
											editedFormBean="${editedUpdatePaymentCardForm}"/>
		</div>
		<ycommerce:testId code="personalDetails_savePersonalDetails_button">
			<div class="row margin-top-20">
				<div class="col-md-6 col-md-offset-3 col-xs-12">
					<a href="${addPaymentUrl}?paymentMethod=creditCard">
						<button class="btn btn-primary btn-block">
							<spring:theme code='text.account.add.credit.card' />
						</button>
					</a>
				</div>
			</div>
		</ycommerce:testId>
		<c:if test="${isB2bCustomer}">
			<div class="custom-account-profile bc-accordion">
				<h6>
					<spring:theme code="text.account.saved.ctcTc.cards" />
				</h6>
				<payment:CtcTcCardPaymentInfos savedCtcTcCards="${paymentCards.savedCtcTcCards}" />
			</div>
			<ycommerce:testId code="personalDetails_savePersonalDetails_button">
				<div class="row margin-top-20">
					<div class="col-md-6 col-md-offset-3 col-xs-12">
						<a href="${addPaymentUrl}?paymentMethod=CTCTCCard">
							<button class="btn btn-primary btn-block">
								<spring:theme code='text.account.add.ctcTC.card' />
							</button>
						</a>
					</div>
				</div>
			</ycommerce:testId>
		</c:if>
	</c:when>
</c:choose>
