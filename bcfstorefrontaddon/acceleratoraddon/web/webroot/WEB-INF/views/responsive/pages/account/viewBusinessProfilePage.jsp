<%@ page trimDirectiveWhitespaces="true"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="formElement" tagdir="/WEB-INF/tags/responsive/formElement" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<spring:htmlEscape defaultHtmlEscape="false" />
<div class="container">


${b2BUnitData.name}
</br>
			<c:forEach items="${b2BUnitData.addresses}" var="address">
					       				<div class="address">
            						        <strong>
            						        ${fn:escapeXml(address.title)}&nbsp;
            						        ${fn:escapeXml(address.firstName)}&nbsp;
            						        ${fn:escapeXml(address.lastName)}
            						        </strong>
            						        <br>
            						        ${fn:escapeXml(address.line1)}&nbsp;
            						        ${fn:escapeXml(address.line2)}
            						        <br>
            						        ${fn:escapeXml(address.town)}&nbsp;
            						        <c:if test="${not empty address.region.name }">
            						            ${fn:escapeXml(address.region.name)}&nbsp;
            						        </c:if>
            						        <br>
            						        ${fn:escapeXml(address.country.name)}&nbsp;
            						        ${fn:escapeXml(address.postalCode)}
            						        <br/>



            		       				</div>
            </c:forEach>
            						        ${fn:escapeXml(b2BUnitData.phoneNumber)}
            						        <br/>
            						        ${fn:escapeXml(b2BUnitData.phoneType)}
                                            <br/>

                                            ${fn:escapeXml(b2BUnitData.email)}
                                            <br/>
            <a href="#"><spring:theme code="text.business.account.deactivate" /></a>
            <p><span><spring:theme code="text.business.account.message" /></span></p>
</div>

