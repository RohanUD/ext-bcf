<%@ page trimDirectiveWhitespaces="true" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="cms" uri="http://hybris.com/tld/cmstags" %>
<%@ taglib prefix="ycommerce" uri="http://hybris.com/tld/ycommercetags" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="progress" tagdir="/WEB-INF/tags/addons/bcfvoyageaddon/responsive/progress" %>
<%@ taglib prefix="currentconditions" tagdir="/WEB-INF/tags/addons/bcfvoyageaddon/responsive/currentconditions" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ page language="java" contentType="text/html;charset=UTF-8" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib prefix="template" tagdir="/WEB-INF/tags/responsive/common"%>
<%@ taglib prefix="ccSailingDetails" tagdir="/WEB-INF/tags/addons/bcfstorefrontaddon/responsive/currentconditions" %>
<c:url var="currentConditionDetailsPageUrl" value="/current-conditions/"/>
<%@ taglib prefix="mapPath" tagdir="/WEB-INF/tags/addons/bcfcommonsaddon/responsive/map"%>

<spring:htmlEscape defaultHtmlEscape="false"/>


<div class="container">
	<div class="row">
		<div class="col-lg-12 col-md-12 col-xs-12">
			<h4 class="margin-top-40 mb-1">
				<b><spring:theme code="text.cc.by.terminal.label"
								 text="Current conditions by terminal"/></b>
			</h4>

			<p>
				<small><spring:theme code="text.cc.select.terminal.find.route.label"
									 text="Select terminal to find route conditions" /></small>
			</p>
		</div>
	</div>
</div>

<div class="container">
	<div id="tabs" class="terminal-listing">
		<div class="row">
			<div class="col-lg-4 col-md-offset-8 terminal-listing-tab">
				<label><spring:theme code="text.cc.view.label" text="View" /></label>
				<ul>
					<li><a href="#tabs-1"> <spring:theme
							code="text.view.list.label" text="List"/>
					</a></li>
					<li><a href="#tabs-2" id="tabToMapClick" onclick=" return false"><spring:theme
							code="text.view.map.label" text="Map"/></a></li>
				</ul>
			</div>
		</div>
		<div id="tabs-1">
			<div class="row">
				<c:set var="rowCounter" value="0"/>
				<c:forEach items="${currentConditionsByTerminals}" var="currentConditionsByTreminal" varStatus="counter">
					<c:if test="${not empty currentConditionsByTreminal.value}">
						<c:set var="rowCounter" value="${rowCounter + 1}"/>
						<c:if test="${rowCounter % 3 eq 0}">
							<div class="row sunshine-coast ">
						</c:if>
						<div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
							<hr>

							<c:forEach items="${currentConditionsByTreminal.value}" var="route" varStatus="index">

								<c:if test="${index.first}">
									<h5 class="mb-2">
											${route.sourceLocationName}
										(${route.sourceTerminalName})
									</h5>
									<p class="mb-2">
										<spring:theme code="text.to.label" text="to" />
									</p>
								</c:if>
								<c:url value="current-conditions/${route.sourceRouteName}-${route.destinationRouteName}/${route.sourceTerminalCode}-${route.destinationTerminalCode}" var="currentCondtionsUrl" />
								<p class="m-0">
									<a href="${currentCondtionsUrl}">
											${route.destinationTerminalName}
										(${route.destinationLocationName}) </a>
								</p>
							</c:forEach>
						</div>
						<c:if test="${rowCounter % 3 eq 0}">
							</div>
						</c:if>
					</c:if>
				</c:forEach>
				<hr>
			</div>
		</div>
		<div id="tabs-2">
            <c:if test="${not empty currentConditionsByTerminals}">
            <c:set var="googleApiKey" value="${jalosession.tenant.config.getParameter('google.api.key')}"/>
            <c:if test="${not empty googleApiKey}">
                 <c:set var="url" value="${currentConditionDetailsPageUrl}"/>
					<div class="row">
					<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 mb-4">
						<div  class="map-wrap close-icon-manage  y_currentconditionmapurl" id="currentconditionmap"
                        data-currentcondtiondetailsmapurl="${url}"
                        data-googleapi="${googleApiKey}">
                </div>
                </div>
                </div>
            </c:if>
            </c:if>
        </div>
                    </div>
                    <div id="capture"></div>
	</div>
<div>
    <mapPath:maptravelroute currentConditionsByTerminals="${currentConditionsByTerminals}" originToDestinationMapping="${originToDestinationMapping}" />
</div>
