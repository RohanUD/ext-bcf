<%@ page trimDirectiveWhitespaces="true"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="formElement" tagdir="/WEB-INF/tags/responsive/formElement" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="common" tagdir="/WEB-INF/tags/responsive/common" %>
<%@ taglib prefix="account" tagdir="/WEB-INF/tags/addons/bcfstorefrontaddon/responsive/account" %>
<spring:htmlEscape defaultHtmlEscape="false" />
 <div class="custom-account-profile  bc-accordion">
        <div class="account-profile-accordion"  id="personal-details">
			<h4 id="myProfileDetailsSection" class="px-0 mb-0" >
				<spring:theme code="myProfile.contactDetails.label" text="My contact details"/>
				<span class="ui-accordion-header-icon ui-icon bcf bcf-bcf bcf-icon-up-arrow"></span>
			    <span class="custom-arrow"></span>
			</h4>
			<div>
			    <account:accountProfileEdit/>
			    <div class="email-sec clearfix mb-4 pt-4 fnt-14 margin-top-30">
			    <div class="col-md-10 col-xs-10 col-sm-10 p-0 myProfileEmailid">${fn:escapeXml(customerData.displayUid)}</div>
			    <div class="col-md-2 col-sm-2 col-xs-2 text-right p-0 pull-right">
			        <i id="updateEmailBtn"class="bcf bcf-icon-edit"></i>
			    </div>
			    <account:updateEmail/>
			</div>
	    </div>
        <c:if test="${salesChannel eq 'Web'}">
		<h4 class="py-5 px-0">
		    <spring:theme code="changePassword.myPassword.label" text="My password"/>
			<span class="ui-accordion-header-icon ui-icon bcf bcf-bcf bcf-icon-up-arrow"></span>
			<span class="custom-arrow"></span>
		</h4>
		<div>
			<account:accountChangePassword/>
		</div>
		</c:if>
	</div>
	<div class="margin-top-40">
		<a href="#" class="margin-top-10" data-toggle="modal" data-target="#myModal"><i class="fas fa-ban"></i> <spring:theme code="text.account.deactivate" /></a>
    </div>
    <div id="myModal" class="modal fade" role="dialog">
      <div class="modal-dialog">
        <div class="modal-content">
          <div class="modal-header">
            <button type="button" class="close deactivate-close-icon" data-dismiss="modal">&times;</button>
            <h4 class="modal-title"><spring:theme code="text.account.deactivate" /></h4>
          </div>
          <div class="modal-body">
            <p><spring:theme code="text.account.deactivate.message"/></p>
          </div>
          <div class="modal-footer">
						<div class="row">
							<div class="col-md-4 col-sm-6 col-md-offset-4 col-xs-offset-3 col-xs-6">
								<button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
							</div>
							</div>
					</div>
        </div>

      </div>
</div>
      </div>
		<p class="privacy-statement"><spring:theme code="register.privacy.statement"/>&nbsp;
    <a href="${pageContext.request.contextPath}/privacy-statement"><spring:theme code="register.privacy.statement.link.text"/></a>
		</p>

