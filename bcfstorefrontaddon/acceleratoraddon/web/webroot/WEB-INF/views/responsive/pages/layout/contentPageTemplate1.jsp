<%@ page trimDirectiveWhitespaces="true"%>
<%@ taglib prefix="template" tagdir="/WEB-INF/tags/responsive/common"%>
<%@ taglib prefix="cms" uri="http://hybris.com/tld/cmstags"%>
<template:page pageTitle="${pageTitle}">
	<cms:pageSlot position="MainContent" var="feature" element="section">
		<cms:component component="${feature}" />
	</cms:pageSlot>
</template:page>
