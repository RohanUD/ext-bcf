<%@ page trimDirectiveWhitespaces="true"%>
<%@ taglib prefix="template" tagdir="/WEB-INF/tags/responsive/mastertemplate"%>
<%@ taglib prefix="cms" uri="http://hybris.com/tld/cmstags"%>
<template:page pageTitle="${pageTitle}">
	<cms:pageSlot position="CenterContent" var="feature" element="div">
		<div>
			<cms:component component="${feature}" />
		</div>
	</cms:pageSlot>
</template:page>
