<%@ page trimDirectiveWhitespaces="true" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="template" tagdir="/WEB-INF/tags/responsive/common" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="formElement" tagdir="/WEB-INF/tags/responsive/formElement" %>
<%@ taglib prefix="cms" uri="http://hybris.com/tld/cmstags" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="address" tagdir="/WEB-INF/tags/addons/bcfcheckoutaddon/responsive/address" %>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>
<%@ taglib prefix="ycommerce" uri="http://hybris.com/tld/ycommercetags" %>
<%@ taglib prefix="account" tagdir="/WEB-INF/tags/addons/bcfstorefrontaddon/responsive/account" %>

<form:form id="silentOrderPostForm" name="silentOrderPostForm" commandName="addPaymentDetailsForm" action="${submitButtonURL}" method="POST">

    <%-- Payment Details --%>

    <c:choose>
        <c:when test="${addPaymentMethod eq 'CTCTCCard'}">

            <div class="col-lg-offset-3 col-md-offset-3 col-sm-offset-0 col-md-6 col-sm-12 margin-bottom-30">
                <h6 class="margin-bottom-30"><spring:theme code="text.account.add.ctcTC.title.label"/></h6>
                <form:hidden path="cardType"/>
                <formElement:formInputBox idKey="pd-card-number" labelKey="text.account.add.ctcTC.label" path="ctcCardNo"
                                          inputCSS="y_cardVerification" mandatory="true" tabindex="0"/>

            </div>
        </c:when>
        <c:otherwise>
            <div class="row margin-top-30">
                <div class="col-md-8 col-sm-12 col-xs-12 col-md-offset-2">
                    <div class="form-group ">
                        <ul class="list-inline mb-0 payment-img py-4">
                            <c:forEach items="${paymentCardTypes}" var="paymentCard" varStatus="cardIdx">
                                <li id="cardType_${paymentCard.code}" class="cardType_${paymentCard.code}">
                                    <form:radiobutton path="cardType" value="${paymentCard.code}" />
                                    <span>
                                        <img src="../../../../_ui/responsive/common/images/${paymentCard.code}.png" width="50">
                                    </span>
                                </li>
                            </c:forEach>
                        </ul>
                    </div>
                </div>
                <div>
                    <form:input type="hidden" id="pd-card-token" path="token"/>
                    <form:input type="hidden" id="pd-card-number" path="cardNumber"/>
                </div>
            </div>
            <%-- / .row --%>
            <div class="row">
                <div class="col-md-8 col-sm-12 col-xs-12 col-md-offset-2">

                    <div class="nowrap">
                        <iframe id="monerisFrame" src="about:blank" scrolling="no" frameborder="0"></iframe>
                        <div class="monerisFrameError fe-error"></div>
                    </div>
                    <div class="nowrap">
                        <formElement:formSelectBox
                                selectCSSClass="form-control y_expirymonth"
                                idKey="pd-exp-day" labelKey="expiry.payment.month"
                                path="cardExpirationMonth" mandatory="true" skipBlank="false"
                                skipBlankMessageKey="payment.month" items="${months}"
                                tabindex="1"/>
                    </div>
                    <div class="row">
                        <div class="col-xs-4">
                            <div class="nowrap">
                                <formElement:formSelectBox
                                        selectCSSClass="form-control y_expiryyear"
                                        idKey="pd-exp-year" labelKey="expiry.payment.year"
                                        labelCSS="nowrap" path="cardExpirationYear" mandatory="true"
                                        skipBlank="false" skipBlankMessageKey="payment.year"
                                        items="${expiryYears}" tabindex="2"/>
                            </div>
                        </div>
                        <div class="col-xs-4">
                            <div class=" nowrap">
                                <formElement:formInputBox idKey="pd-card-verification"
                                                          labelKey="payment.cvn" path="cardCVNumber"
                                                          inputCSS="y_cardVerification" mandatory="true" tabindex="3"/>
                            </div>
                        </div>
                    </div>

                </div>
            </div>

            <%-- Billing Address --%>
            <div class="row">
                <div class="col-lg-12 col-xs-12">
                    <h2 class="title payment-h2">
                        <spring:theme code="checkout.summary.paymentMethod.billing.address.header" text="Billing Address"/>
                    </h2>
                </div>
            </div>
            <div class="row">
                <div class="col-md-8 col-sm-12 col-xs-12 col-md-offset-2">
                    <formElement:formInputBox idKey="first-name1" labelKey="label.payment.billing.nameOnCard" inputCSS="y_first_name"
                                              path="billingAddress.firstName" tabindex="4" mandatory="true"/>
                    <formElement:formInputBox idKey="addressLine1" labelKey="text.address.label" path="billingAddress.line1"
                                              tabindex="5"
                                              inputCSS="y_myAccount_address_line1"/>
                    <formElement:formInputBox idKey="city" labelKey="address.townCity" path="billingAddress.townCity" tabindex="6"
                                              inputCSS="y_myAccount_city"/>
                    <formElement:formSelectBox idKey="country" labelKey="address.country" path="billingAddress.countryIso"
                                               mandatory="true" skipBlank="false"
                                               skipBlankMessageKey="text.payment.guest.country.dropdown" items="${countries}"
                                               itemValue="isocode" tabindex="7"
                                               selectCSSClass="y_country billing-address-country form-control"/>


                    <div class="billing-address-regions-group">
                        <formElement:formSelectBox idKey="billing-address-regions" labelKey="profile.address.region"
                                                   path="billingAddress.regionIso" items="${regions}"
                                                   itemValue="isocode" selectCSSClass="billing-address-regions form-control"
                                                   skipBlankMessageKey="text.page.default.select" mandatory="true" tabindex="8"/>
                    </div>

                    <formElement:formInputBox idKey="postalcode" labelKey="address.postalcode" path="billingAddress.postcode"
                                              tabindex="9" mandatory="true" inputCSS="y_postal_code"/>
                </div>


            </div>
        </c:otherwise>
    </c:choose>

    <div class="row margin-top-30">
        <div class="col-lg-offset-4 col-md-offset-4 col-sm-offset-1 col-xs-offset-1 col-lg-4 col-md-4 col-sm-10 col-xs-10 text-center">
            <c:choose>
                <c:when test="${addPaymentMethod eq 'CTCTCCard'}">
                    <input type="submit" class="btn btn-block btn-primary"
                           value="<spring:theme code='text.account.add.paymentCard.addCard'/>">
                </c:when>
                <c:otherwise>
                    <input type="button" id="ht_account_submit" class="btn btn-block btn-primary"
                           value="<spring:theme code='text.account.add.paymentCard.addCard'/>">
                </c:otherwise>
            </c:choose>
        </div>
    </div>
</form:form>

<input type="hidden" id="ht_token"/> <input type="hidden" id="ht_id" value="${paymentTokenizerClientID }"/> <input type="hidden"
                                                                                                                   id="ht_host"
                                                                                                                   value="${paymentTokenizerHostURL }"/>
<input type="hidden" id="ht_host_tokenizerURL" value="${paymentTokenizerURL }"/>
