<%@ page trimDirectiveWhitespaces="true" contentType="application/json"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="json" uri="http://www.atg.com/taglibs/json"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="getquotes" tagdir="/WEB-INF/tags/addons/bcfstorefrontaddon/responsive/getaquotelisting"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<json:object escapeXml="false">
   <json:property name="htmlContent">
      <div class="row htmlContent">
         <getquotes:getquotes ticketsList="${ticketsList}"/>
      </div>
   </json:property>
   <json:property name="hasMoreResults" value="${hasMoreResults}" />
   <json:property name="totalNumberOfResults" value="${totalNumberOfResults}" />
   <json:property name="totalShownResults">
      <h3 class="h4">
         <spring:theme code="text.ferry.listing.shown.results" arguments="${startingNumberOfResults}, ${totalShownResults}, ${totalNumberOfResults}" />
      </h3>
   </json:property>
</json:object>
