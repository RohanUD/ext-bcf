<%@ page trimDirectiveWhitespaces="true"%>
<%@ taglib prefix="template" tagdir="/WEB-INF/tags/responsive/common"%>
<%@ taglib prefix="cms" uri="http://hybris.com/tld/cmstags"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags"%>
<%@ taglib prefix="format" tagdir="/WEB-INF/tags/addons/bcfstorefrontaddon/responsive/format"%>
<%@ taglib prefix="booking" tagdir="/WEB-INF/tags/addons/bcfstorefrontaddon/responsive/booking"%>
<spring:htmlEscape defaultHtmlEscape="false" />
<template:page pageTitle="${pageTitle}">
	<div class="container">
		<div class="row">
			<div class="col-xs-12 col-sm-offset-3 col-sm-9">
				<booking:amendBookingMessage amendBookingRefundAmount="${amendBookingRefundAmount}" amendBookingResult="${amendBookingResult}" amendBookingErrorResult="${amendBookingErrorResult}" isAccommodationAvailable="${isAccommodationAvailable}" />
				<booking:cancelResultMessage refundResult="${refundResult}" cancellationResult="${cancellationResult}" refundedAmount="${refundedAmount}" cancellationParameter="${cancellationParameter}" />
			</div>
		</div>
		<div class="row">
			<div class="col-xs-12 col-sm-6 mb-2">
				<h4>
					<spring:theme code="text.page.managemybooking.bookingdetails" text="Booking details" />
				</h4>
			</div>
		</div>
		<div class="row">
			<div>
					<cms:pageSlot position="RightContent" var="feature">
						<cms:component component="${feature}" />
					</cms:pageSlot>
                    <cms:pageSlot position="MiddleContent" var="feature">
                        <cms:component component="${feature}"/>
                    </cms:pageSlot>
				</div>
				</div>
	</div>
</template:page>
