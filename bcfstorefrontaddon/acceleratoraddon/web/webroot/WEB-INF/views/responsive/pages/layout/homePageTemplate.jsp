<%@ page trimDirectiveWhitespaces="true"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="template" tagdir="/WEB-INF/tags/responsive/common"%>
<%@ taglib prefix="cms" uri="http://hybris.com/tld/cmstags"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<template:page pageTitle="${pageTitle}">

	<div class="wrapper">
		<cms:pageSlot position="Banner" var="feature" element="section">
			<cms:component component="${feature}" />
		</cms:pageSlot>
		<div class="homepage-search-box hidden">
			<div class="container">
				<div class="row">
					<div class="col-sm-12 form-box-landing form-box-landing-mob">
						<cms:pageSlot position="Section1" var="feature" element="section"
							id="js-home-tab" class=" ">
							<cms:component component="${feature}" />
						</cms:pageSlot>
					</div>
				</div>
			</div>
		</div>
	</div>

	<cms:pageSlot position="NavigationLinks" var="feature"
		element="section" class="">
		<cms:component component="${feature}" />
	</cms:pageSlot>

	<cms:pageSlot position="DealsAndOffers" var="feature" element="section"
		class="deal-width">
			<cms:component component="${feature}" />
	</cms:pageSlot>

	<cms:pageSlot position="VacationPackages" var="feature"
		element="section">
		<cms:component component="${feature}" />
	</cms:pageSlot>


	<cms:pageSlot position="FeaturedDeals" var="feature" element="section"
		class="bg-dark-gray margin-top-n-30">
			<cms:component component="${feature}" />
	</cms:pageSlot>



	<cms:pageSlot position="SeeMoreVacations" var="feature" element="section" class="bg-dark-gray margin-top-n-30">
		<cms:component component="${feature}" />
	</cms:pageSlot>



	<cms:pageSlot position="Newsroom" var="feature" element="section">
			<cms:component component="${feature}" />
	</cms:pageSlot>


	<booking:cancelBookingResponse />
</template:page>
