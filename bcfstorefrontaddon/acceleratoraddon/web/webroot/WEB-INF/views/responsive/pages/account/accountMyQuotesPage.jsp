<%@ page trimDirectiveWhitespaces="true"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="nav" tagdir="/WEB-INF/tags/responsive/nav"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="format" tagdir="/WEB-INF/tags/shared/format" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="common" tagdir="/WEB-INF/tags/responsive/common"%>
<%@ taglib prefix="ycommerce" uri="http://hybris.com/tld/ycommercetags"%>
<%@ taglib prefix="quoteslist" tagdir="/WEB-INF/tags/addons/bcfstorefrontaddon/responsive/quoteslisting"%>
<spring:htmlEscape defaultHtmlEscape="false" />
<div class="container">
	<div id="globalMessages">
		<common:globalMessages />
	</div>
   <div class="account-section-header">
      <h4><spring:theme code="text.account.quote.myquotes" /></h4>
   </div>
   <c:if test="${empty quotes}">
      <div class="row">
         <div class="col-md-6 col-md-push-3">
            <div class="account-section-content	content-empty">
               <ycommerce:testId code="quoteHistory_noQuotes_label">
                  <spring:theme code="text.account.quotes.noQuotes" />
               </ycommerce:testId>
            </div>
         </div>
      </div>
   </c:if>
   <c:if test="${not empty quotes}">
      <div class="container  mt-3 white-bg-only py-5 quotes" id="quote_details">
         <div class="quotes_details_placeholder">
            <quoteslist:quotes quotes="${quotes}"/>
         </div>
         <div id="y_quotesListingShowMore" class="hidden"></div>
         <div class="row text-center">
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
               <div class="y_shownResultId text-center mb-4">
                  <p>
                     <spring:theme code="text.ferry.listing.shown.results"
                        arguments="${startingNumberOfResults},${totalShownResults},${totalNumberOfResults}" />
                  </p>
               </div>
            </div>
         </div>

		 <input id="hasMoreResults" name="hasMoreResults" type="hidden" value="${hasMoreResults}" />
         <div class="acco-pagination  text-center">
            <p>
               <a href="#" class="y_showSpecificQuotesPageResults prev"
                  data-pagenumber="${pageNum-1}">
                  <span
                     class="y_showPreviousQuotesPageResults">
                     <spring:theme
                        code="text.ferry.listing.show.previous.page" text="Previous Page" />
                  </span>
               </a>
               |
               <a href="">
                  <strong>
               <a href="#"
                  class="y_showSpecificQuotesPageResults next"
                  data-pagenumber="${pageNum}"> <span
                  class="y_showNextQuotesPageResults"> <spring:theme
                  code="text.ferry.listing.show.next.page" text="Next Page" />
               </span>
               </a></strong></a>
            </p>
         </div>
      </div>
   </c:if>
</div>
