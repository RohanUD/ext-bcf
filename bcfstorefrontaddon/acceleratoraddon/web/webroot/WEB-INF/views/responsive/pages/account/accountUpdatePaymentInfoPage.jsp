<%@ page trimDirectiveWhitespaces="true"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="formElement" tagdir="/WEB-INF/tags/responsive/formElement"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags"%>
<%@ taglib prefix="account" tagdir="/WEB-INF/tags/addons/bcfstorefrontaddon/responsive/account"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<c:url value="/my-account/update-payment-details" var="updatePaymentUrl" />
<%-- Payment Details --%>
<div class="panel panel-primary panel-list">
	<div class="account-section-header">
		<div class="row">
			<div class="container-lg col-md-6">
				<spring:theme code="checkout.summary.paymentMethod.payment.header" />
			</div>
		</div>
	</div>
	<input type="hidden" name="monerisToken" />
	<div class="row">
		<div class="container-lg col-md-12">
			<div class="account-section-content">
				<div class="account-section-form">
					<div class="panel-body">
						<fieldset class="fieldset">
							<form:form id="silentOrderPostForm" name="silentOrderPostForm" commandName="sopPaymentDetailsForm" class="create_update_payment_form" action="${fn:escapeXml(updatePaymentUrl)}/${fn:escapeXml(paymentInfoData.id)}" method="POST">
								<div class="row">
									<div class="form-group col-sm-6">
										<formElement:formSelectBox idKey="pd-cardtype" labelKey="payment.cardType" path="card_cardType" mandatory="true" skipBlank="false" skipBlankMessageKey="payment.cardType.pleaseSelect" items="${sopCardTypes}" selectCSSClass="form-control" tabindex="1" />
										<%--  <formElement:formSelectBox idKey="pd-cardtype" labelKey="payment.cardType"
                                                                       path="card_cardType" mandatory="true" skipBlank="false"
                                                                       skipBlankMessageKey="payment.cardType.pleaseSelect"
                                                                       items="${sopCardTypes}" tabindex="1"/> --%>
									</div>
									<div>
										<form:input type="hidden" id="pd-card-token" path="token" />
										<form:input type="hidden" id="pd-card-number" path="card_accountNumber" />
										<input type='hidden' id='subscriptionId' name='subscriptionId' value='${paymentInfoData.subscriptionId}' /> <input type='hidden' id='issueNumber' name='issueNumber' value='${card_issueNumber}' />
									</div>
								</div>
								<div class="row">
									<div class="form-group col-sm-6">
										<div class="monerisFrameError"></div>
										<iframe id="monerisFrame" src="about:blank" scrolling="no" frameborder="0"></iframe>
									</div>
								</div>
								<div class="row">
									<div class="col-sm-12">
										<div class="row">
											<div class="form-group col-sm-3 nowrap">
												<formElement:formSelectBox selectCSSClass="form-control" idKey="pd-valid-day" labelKey="valid.from.payment.month" path="card_startMonth" mandatory="true" skipBlank="false" skipBlankMessageKey="payment.month" items="${months}" tabindex="3" />
											</div>
											<div class="form-group col-sm-3">
												<formElement:formSelectBox selectCSSClass="form-control" idKey="pd-valid-year" labelKey="valid.from.payment.year" labelCSS="invisible nowrap" path="card_startYear" mandatory="true" skipBlank="false" skipBlankMessageKey="payment.year" items="${startYears}" tabindex="4" />
											</div>
											<div class="form-group col-sm-3 nowrap">
												<formElement:formSelectBox selectCSSClass="form-control" idKey="pd-exp-day" labelKey="expiry.payment.month" path="card_expirationMonth" mandatory="true" skipBlank="false" skipBlankMessageKey="payment.month" items="${months}" tabindex="5" />
											</div>
											<div class="form-group col-sm-3">
												<formElement:formSelectBox selectCSSClass="form-control" idKey="pd-exp-year" labelKey="expiry.payment.year" labelCSS="invisible nowrap" path="card_expirationYear" mandatory="true" skipBlank="false" skipBlankMessageKey="payment.year" items="${expiryYears}" tabindex="6" />
											</div>
										</div>
									</div>
								</div>
								<div class="row">
									<div class="form-group col-sm-6">
										<formElement:formInputBox idKey="pd-card-name" labelKey="payment.nameOnCard" path="card_nameOnCard" inputCSS="y_nameOnCard" tabindex="7" mandatory="true" />
									</div>
								</div>
							</form:form>
						</fieldset>
						<div class="col-xs-12 col-sm-4 pull-right">
							<button id="ht_submit" class="btn btn-secondary col-xs-12">
								<spring:theme code="text.account.profile.saveUpdates" text="Save Updates" />
							</button>
						</div>
						<div class="row bottom-row">
							<div class="col-xs-12 col-sm-4 pull-right backToHome">
								<button type="button" class="btn btn-secondary col-xs-12">
									<spring:theme code="text.account.profile.cancel" text="Cancel" />
								</button>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<%-- <fieldset>
		<div class="row">
			<div class="monerisFrameError"></div>
			<div class="form-group col-xs-12 col-sm-6 col-md-3 pull-right">
				<button id="ht_submit" class="btn y btn-block bottom-align">
					<spring:theme code="checkout.summary.paymentMethod.next.button" text="Next" />
				</button>
			</div>
		</div>
		/ .row
	</fieldset> --%>
	<input type="hidden" id="ht_token" /> <input type="hidden" id="ht_id" value="${paymentTokenizerClientID }" /> <input type="hidden" id="ht_host" value="${paymentTokenizerHostURL }" /> <input type="hidden" id="ht_host_tokenizerURL" value="${paymentTokenizerURL }" />
	<!-- <input type="button" id="ht_submit" value="Submit"> -->
</div>
<%-- / Payment Details --%>
