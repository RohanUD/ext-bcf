<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>

<c:if test="${not empty destinationPackages or (not empty destinationAccommodations and fn:length(destinationAccommodations.results) gt 0)}">
   <div class="container bg-Packages">
      <div class="row">
         <div class="col-md-12 col-sm-12 col-xs-12 margin-top-30  margin-bottom-10">
            <h2 class="text-dark-blue">
               <span id="Packages" name="Packages">
                  ${destinationDetails.name}&nbsp;
                  <spring:theme code="text.packages.label" text="packages" />
               </span>
            </h2>
         </div>
         <c:choose>
            <c:when test="${not empty destinationPackages}">
               <c:forEach items="${destinationPackages}" var="deal">
                  <div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
                     <div class="destination-details-package-box">
                        <div class="destination-details-promotext">${deal.promotext} </div>
                        <c:if test="${fn:length(deal.imageUrls) gt 0}">
                           <img class='img-fluid  img-w-h js-responsive-image' data-media='${deal.imageUrls[0].url}' alt='${deal.imageUrls[0].altText}' title='${deal.imageUrls[0].altText}'/>
                        </c:if>
                        <div class="destination-details-package-text">
                           <h3 class="text-dark-blue"><b>${deal.name}</b></h3>
                           <p class="destination-details-p-lenght">${deal.description}</p>
                           <p>
                              <b>
                                 <a href="${deal.packageDealHotelListingUrl}">
                                    <spring:theme code="label.destination.details.viewDetails" text="View details >" />
                                 </a>
                              </b>
                           </p>
                        </div>
                     </div>
                  </div>
               </c:forEach>
            </c:when>
            <c:otherwise>
               <c:choose>
                  <c:when test="${not empty destinationAccommodations and fn:length(destinationAccommodations.results) gt 0}">
                     <div class="container">
                        <div class="row offers-grouped-component">
                           <div id="y_packageResults" class=" deal-items deal-row" aria-label="package search results">
                              <c:forEach items="${destinationAccommodations.results}" var="hotel">
                                 <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                                    <c:if test="${not empty hotel.images}">
                                       <div class="accommodation-image package-list-owl">
                                          <div class="owl-carousel owl-theme js-owl-carousel js-owl-rotating-gallery">
                                             <c:forEach items="${hotel.images}" var="hotelImage">
                                                <div class="owl-item">
                                                   <img class='js-responsive-image' data-media='${hotelImage.url}' alt='${hotelImage.altText}' title='${hotelImage.altText}'/>
                                                </div>
                                             </c:forEach>
                                          </div>
                                       </div>
                                    </c:if>
                                    <div class="deal-details ">
                                       <c:if test="${not empty hotel.propertyCategories}">
                                          <c:forEach items="${hotel.propertyCategories}" var="categoryType">
                                             <span class="${categoryType}"></span>
                                          </c:forEach>
                                       </c:if>
                                       <p class="text-grey margin-top-10">${fn:toUpperCase(hotel.locationName)}</p>
                                       <div class="review-img">
                                          <spring:url value="${jalosession.tenant.config.getParameter('tripadvisor.url')}" var="tripAdvisorURL" htmlEscape="false">
                                             <spring:param name="locationId"  value="${hotel.customerReviewData.locationId}"/>
                                          </spring:url>
                                          <c:if test="${not empty hotel.customerReviewData.ratingImageUrl}">
                                             <img src="${fn:escapeXml(hotel.customerReviewData.ratingImageUrl)}" alt="Trip Advisor Rating">
                                             <a class="font-weight-bold tripAdvisorLink" href="${tripAdvisorURL}">
                                                <span class="review-text">
                                                   ${hotel.customerReviewData.numOfReviews}
                                                   <spring:theme code="text.accommodation.details.property.reviews" />
                                                </span>
                                             </a>
                                          </c:if>
                                       </div>
                                       <h2>${hotel.name}</h2>
                                       <p class="p-ellipsis">${hotel.description}</p>
                                       <a href="${pageContext.request.contextPath}${hotel.nextUrl}"><strong>Book now ></strong></a>
                                    </div>
                                 </div>
                              </c:forEach>
                           </div>
                        </div>
                     </div>
                  </c:when>
               </c:choose>
               <br>
            </c:otherwise>
         </c:choose>
      </div>
   </div>
    <div class="my-5 row">
       <div class="col-lg-4 col-md-4 col-md-offset-4">
            <a class="btn btn-primary d-block" href="/vacations/dealandofferslisting">
                <spring:theme code="text.view.packages" text = "View All Packages"/>
            </a>
        </div>
    </div>
</c:if>
