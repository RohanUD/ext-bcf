<%@ page trimDirectiveWhitespaces="true" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<spring:htmlEscape defaultHtmlEscape="false"/>
<c:url var="unsubscribeUrl" value="/subscriptions/unsubscribeAll"/>
<c:url var="subscriptionsPageUrl" value="/my-account/subscriptions"/>
<h4 id="myProfileDetailsSection" class="px-0 mb-0">
    <spring:theme code="text.unsubscribe.label" text="My contact details"/>
</h4>
<c:choose>
    <c:when test="${customerFound eq true and empty updateSuccessful}">
        <c:choose>
            <c:when test="${subscriptionCodes eq 'unsubscribeAll'}">
                <p><spring:theme code="text.unsubscribe.desc1.label"/></p>
            </c:when>
            <c:otherwise>
                <spring:theme code="text.unsubscribe.specific.services.desc1.label" arguments="${subscriptionCodes}"
                              text="subscriptions"/>
            </c:otherwise>
        </c:choose>

        <p class="margin-bottom-40 text-light"><spring:theme code="text.unsubscribe.desc2.label"/></p>
        <form action="${unsubscribeUrl}/${customerId}" method="GET">
            <div class="row">
                <div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
                    <input type="hidden" name="subscriptionCodes" value="${subscriptionCodes}">
                    <button type="submit" class="btn btn-primary btn-block margin-bottom-20"><spring:theme code='text.unsubscribe.confirm.label'/> </button>
                </div>
                <div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
                    <a href="${subscriptionsPageUrl}" class="btn btn-secondary btn-block margin-bottom-20"> <spring:theme code='text.unsubscribe.manageSubscriptions.label'/> </a>
                </div>
            </div>
        </form>
    </c:when>
    <c:when test="${updateSuccessful}">
        <spring:theme code="text.unsubscribe.desc2.label"/>
        <div class="row">
            <div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
                <a href="${subscriptionsPageUrl}" class="btn btn-secondary btn-block margin-bottom-20"><spring:theme
                        code='text.unsubscribe.manageSubscriptions.label'/></a>
            </div>
        </div>
    </c:when>
</c:choose>
