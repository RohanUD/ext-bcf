<%@ page trimDirectiveWhitespaces="true" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<div class="container">
    <div class="row">
        <div class="col-lg-12">
            <c:if test="${fn:length(traveladvisorieslist) gt 0}">
                <h2><spring:theme code="text.travel.advisory.current" text="Current Travel Advisories" /></h2>
                <c:forEach items="${traveladvisorieslist}" var="traveladvisory">
                    <a href="/travel-advisories/${traveladvisory.code}">
                        <fmt:formatDate value="${traveladvisory.publishDateTime}" pattern="MMMM dd, yyyy" /> - ${traveladvisory.advisoryTitle}
                        <hr>
                    </a>
                </c:forEach> 
            </c:if>
            <c:if test="${fn:length(traveladvisorieslist) eq 0}">
                <h2><spring:theme code="text.travel.advisory.smooth.sailing" text="Smooth sailing!" /></h2>
                <spring:theme code="text.travel.advisory.no.active.advisory" text="There are no active travel advisories at this time - we look forward to seeing you onboard soon" />
            </c:if>
        </div>
    </div>
</div>
