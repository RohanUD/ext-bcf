<%@ page trimDirectiveWhitespaces="true"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix = "fmt" uri = "http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="nav" tagdir="/WEB-INF/tags/responsive/nav" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="ycommerce" uri="http://hybris.com/tld/ycommercetags" %>
<%@ taglib prefix="bcfglobalreservation" tagdir="/WEB-INF/tags/addons/bcfcommonsaddon/responsive/bcfglobalreservation"%>
<%@ taglib prefix="accommodationReservation" tagdir="/WEB-INF/tags/addons/bcfcommonsaddon/responsive/accommodationreservation"%>

<c:if test="${not empty globalReservationDataList.packageReservations}">

	<bcfglobalreservation:packageReservationDetails packageDataList="${globalReservationDataList.packageReservations}" showProgressBar="false"/>
</c:if>
<c:if test="${not empty globalReservationDataList.transportReservations}">
	<bcfglobalreservation:transportReservations reservationDataList="${globalReservationDataList.transportReservations}" />
</c:if>
<c:if test="${not empty globalReservationDataList.accommodationReservations}">
	<div class="container">
		<c:forEach items="${globalReservationDataList.accommodationReservations.accommodationReservations}" var="accommodationReservationData" varStatus="accommodationResVarStatus">
			<c:if test="${not empty accommodationReservationData}">
				<accommodationReservation:hotelReservation accommodationReservationData="${accommodationReservationData}" />
			</c:if>

			<hr class="hr-payment">
			<div class="row mb-5">
				<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
					<c:set var="roomStays" value="${accommodationReservationData.roomStays}" />
					<bcfglobalreservation:roomReservation roomStays="${roomStays}"/>
				</div>
			</div>
			<hr class="hr-payment">
		</c:forEach>
	</div>
</c:if>
<c:if test="${not empty globalReservationDataList.activityReservations}">
    <bcfglobalreservation:totalTaxFare />
	<bcfglobalreservation:activityReservations activityReservationDataList="${globalReservationDataList.activityReservations}" />
</c:if>


<form:form method="post" commandName="optionBookingForm" id="optionBookingForm" action="${fn:escapeXml(updateOptionBookingUrl)}">
    <c:set var="formattedExpirationDate"><fmt:formatDate pattern='dd/MM/yyyy' value='${optionBookingDetail.expirationTime}' /></c:set>
    <form:input path="optionBookingId" value="${optionBookingDetail.code}" type="hidden" />
    <form:input path="originalExpirationDate"  value="${formattedExpirationDate}" type="hidden" />
    <input id="creationDate" value='<fmt:formatDate pattern="MM-dd-yyyy" value="${optionBookingDetail.creationTime}" />' type="hidden" />
    <input id="departureDate" value='<fmt:formatDate pattern="MM-dd-yyyy" value="${optionBookingDetail.departureDate}" />' type="hidden" />
    <div class="col-xs-12 col-sm-4">

        <label class="" for="datePickerOptionBookingExpirationDate">
            <spring:theme code="text.page.optionbooking.details.label.expiration.timer"/>
        </label>
         <fmt:formatDate pattern="dd-MM-yyyy HH:mm:ss" value="${optionBookingDetail.expirationTime}" />
    </div>
    <div class="col-xs-12 col-sm-4">
        <c:choose>
            <c:when test="${optionBookingDetail.cartStatus ne 'CANCELLED'}">
                <label class="" for="datePickerOptionBookingExpirationDate">
                    <spring:theme code="text.page.optionbooking.details.label.update.expiration.timer"/>
                </label>
                <form:input path="updatedExpirationDate" type="text" class="bc-dropdown optionbooking-datepicker" value="" autocomplete="off" aria-invalid="false"/>
            </c:when>
            <c:otherwise>
                <c:if test="${ not empty optionBookingDetail.optionBookingCreationDate}">
                    <label>
                        <spring:theme code="text.page.optionbooking.details.label.creation.date"/>
                    </label>
                    <fmt:formatDate pattern="dd-MM-yyyy HH:mm:ss" value="${optionBookingDetail.optionBookingCreationDate}" />
                </c:if>
                <c:if test="${ not empty optionBookingDetail.optionBookingReleaseDate}">
                    <label>
                        <spring:theme code="text.page.optionbooking.details.label.release.date"/>
                    </label>
                     <fmt:formatDate pattern="dd-MM-yyyy HH:mm:ss" value="${optionBookingDetail.optionBookingReleaseDate}" />
                 </c:if>
            </c:otherwise>
        </c:choose>
    </div>

    <c:if test="${optionBookingDetail.cartStatus ne 'CANCELLED'}">

        <div class="col-xs-12 col-sm-4">
             <form:button type="submit" class="btn btn-primary btn-block">
                <spring:theme code="text.page.optionbooking.details.action.update" text="Update" />
            </form:button>
        </div>

        <div class="row margin-bottom-20 margin-top-40 text-center">
            <div class="col-lg-4 col-lg-offset-4 col-md-offset-4 col-md-4 col-sm-12 col-xs-12">
                <a href="/option-booking/convert-to-booking/${optionBookingDetail.code}" class="btn btn-primary btn-block"><spring:theme code="label.fareselectionreview.button.convert.to.booking" /></a>
            </div>

            <div class="col-lg-4 col-lg-offset-4 col-md-offset-4 col-md-4 col-sm-12 col-xs-12">
                <a href="/option-booking/cancel/${optionBookingDetail.code}" class="btn btn-primary btn-block"><spring:theme code="label.fareselectionreview.button.cancel.optionbooking" /></a>
            </div>
        </div>
    </c:if>
</form:form>
