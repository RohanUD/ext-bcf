<%@ page trimDirectiveWhitespaces="true"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="currentconditions"
		   tagdir="/WEB-INF/tags/addons/bcfvoyageaddon/responsive/currentconditions" %>
<%@ taglib prefix="ccSailingDetails"
		   tagdir="/WEB-INF/tags/addons/bcfstorefrontaddon/responsive/currentconditions" %>
<jsp:useBean id="dateValue" class="java.util.Date" />

<c:url var="ferryDetailsPageUrl" value="/on-the-ferry/our-fleet"/>

<spring:htmlEscape defaultHtmlEscape="false" />

<c:url var="currentCondtionsUrlOld"
	   value="currentconditions-details?departureLocationName={0}&departureLocation={1}&departureLocationSuggestionType=AIRPORTGROUP&arrivalLocationName={3}&arrivalLocation={4}&arrivalLocationSuggestionType=AIRPORTGROUP"/>
<c:set var="baseCCUrl" value="currentconditions-details"/>

<c:set var="terminalNames" value="${currentConditionsData.terminals}" scope="page"/>
<c:set var="vesselNames" value="${currentConditionsData.vessels}" scope="page"/>

<%-- Page Title and description --%>
<div class="container">
<p class="text-grey margin-top-10"><spring:theme code="text.cc.atAglance.header.label"
	text="At-a-glance" /></p>
<h2 class="text-dark-blue">
	<b><spring:theme code="text.cc.atAglance.departures.page.label"
					 text="Departures and arrivals"/> </b>
</h2>

<div class="row">
	<div class="col-lg-8 col-md-8 col-sm-12 col-xs-12">
		<p>
			<spring:theme code="text.cc.atAglance.departures.page.desc.label"/>
		</p>
	</div>
</div>

<c:choose>
	<c:when test="${ccServiceNotAvailable}">
		<spring:theme code="text.cc.notAvailable.label"
					  text="Services are not available"/>
	</c:when>
	<c:otherwise>
		<%-- Refresh --%>
		<p class="italic-style margin-top-20 text-sm"><em> <spring:theme code="text.cc.lastUpdated.label"
																 text="Last updated"/> :
			<c:forEach items="${currentConditionsData.currentConditionsResponseData}" var="currentConditionsByTreminal" begin="0" end="0">
				<fmt:parseDate value="${currentConditionsByTreminal.refreshedAt}" var="now"
							   pattern="yyyy-MM-dd HH:mm:ss"/>
				<fmt:formatDate value="${now}" pattern="h:mm a EEEE, MMM dd, yyyy"/>.
				<a href="" onclick="return false" id="refreshDepartures">
					<spring:theme
							code="text.cc.atAglance.click.to.refresh.label"
							text="Click to refresh"/>
				</a>

			</c:forEach></em>
		</p>
		</div>
		<section class="bg-light-gray padding-top-20  margin-bottom-30 margin-top-20">
			<div class="container">
				<p>
					<b><spring:theme code="text.cc.atAglance.find.a.terminal.label"
									 text="Find a terminal"/></b>
				</p>

				<form id="findATerminal" action="departures" method="get">
					<div class="row" id="findTerminalDiv">
						<div class="col-lg-8 col-md-8 col-sm-6 col-xs-12 margin-bottom-30">
							<select class="form-control selectpicker departures-select" id="selectDepartingTerminal" name="terminalCode">
								<option value="default" <c:if test='${empty param.terminalCode}'>selected</c:if>>
									<spring:theme code="label.our.terminals.make.selection" text="Select a terminal"/>
								</option>
								<c:forEach items="${sourceTerminals.terminals}" var="terminal">
									<option value="${terminal.key}"
											<c:if test='${selectedTerminal eq terminal.key}'>selected</c:if>>${sourceTerminals.terminals[terminal.key].name}
									</option>
								</c:forEach>
							</select>
						</div>
					</div>
				</form>
			</div>
		</section>
		<div class="container">
		<div id="terminalInfo">
			<c:forEach
					items="${currentConditionsData.currentConditionsResponseData}"
					var="currentConditionsByTreminal">
				<c:forEach items="${currentConditionsByTreminal.arrivalDepartures}"
						   var="entry" varStatus="outerIndex">
						   <c:set var="emptyCount" value="0"/>
					<c:forEach var="item" items="${entry.value}" varStatus="index">
						<c:if test="${index.first}">
							<c:if test="${outerIndex.first}">
								<h3 class="my-5 text-black">
									<b>${terminalNames[item.dept].name}&nbsp;<spring:theme code="text.cc.terminal.label"
																						   text="terminal"/>
									</b>
								</h3>
							</c:if>
							<table class="table departures-tbl">
							<tr class="text-center">
								<td colspan="3"><fmt:parseDate
										value="${entry.value[0].scheduledDeparture}" var="depTime"
										pattern="yyyy-MM-dd HH:mm:ss" /> <fmt:parseDate
										value="${entry.value[0].scheduledArrival}" var="arrTime"
										pattern="yyyy-MM-dd HH:mm:ss" />
									<p class="text-dark-blue">
										<b>
												${terminalNames[item.dept].name} -
											<c:choose>
												<c:when test="${fn:contains(southernGulfIslandTerminals, item.dest)}">
													<spring:theme code="text.cc.atAglance.SOUTHERN_GULF.label" text="Southern Gulf Islands"/>
												</c:when>
												<c:otherwise>
													${terminalNames[item.dest].name}
												</c:otherwise>
											</c:choose>
										</b>
									</p>

									<p>
										<spring:theme code="text.cc.atAglance.departures.sailingTime.label"/>:
										<c:if test="${not empty arrTime and not empty depTime}">
										<c:choose>
											<c:when test="${fn:contains(southernGulfIslandTerminals, item.dest)}">
												<b><spring:theme code="text.varies.label" text="Varies" /></b>
											</c:when>
											<c:otherwise>
												<c:set var="sailingHour" value="${fn:substringBefore((arrTime.time-depTime.time)/(1000*60*60), '.')}"/>
												<c:set var="sailingMin" value="${fn:substringAfter((arrTime.time-depTime.time)/(1000*60*60), '.')*60}"/>

												<b>
													<c:if test="${sailingHour != 0}">  ${sailingHour}h   </c:if>
													<c:if test="${sailingMin != 0}">
														<c:out value="${fn:substring(sailingMin, 0, 2)}"/>min
													</c:if>
												</b>
											</c:otherwise>
										</c:choose>
										</c:if>
									</p></td>
							</tr>
							<tr class="bg-light-gray">
								<th width="33.33%" class="text-right"><div class="col-md-5 pull-right text-center"><spring:theme code="text.cc.ferry.label"
																																 text="FERRY"/></div></th>
								<th width="33.33%" class="text-center"><spring:theme code="text.cc.atAglance.departures.label"
																					 text="DEPARTURE"/></th>
								<th width="33.33%" class="text-left"><div class="col-md-5 pull-left text-center"><spring:theme code="text.cc.atAglance.status.label"
																															   text="STATUS"/></div></th>
							</tr>
						</c:if>
                        <c:if test="${not empty vesselNames[item.vessel] or not empty item.scheduledDeparture or not empty item.departureStatus}">
                            <c:set var="emptyCount" value="${emptyCount+1}"/>
                        </c:if>
                        <c:choose>
                        <c:when test="${index.last and emptyCount eq 0}">
                            <tr>
                                <td colspan="3" class="sailling-details-td">
                                    <div class="text-center">
                                        <div class="sailing-details-msg">
                                            <strong>
                                                <span class="bcf bcf-icon-notice-outline bcf-2x"></span>
                                                <spring:theme code="text.cc.no.sailings.available.label"/></strong>
                                        </div>
                                    </div>
                                </td>
                            </tr>
                        </c:when>
                        <c:otherwise>
                        <c:if test="${emptyCount ne 0}">
						<tr class="padding-departures-td">
							<td width="33.33%" class="text-right">
								<div class="col-md-5 pull-right text-center">
									<p>
										<c:choose>
											<c:when test="${not empty vesselNames[item.vessel]}">
												<c:set var="vesselName" value="${vesselNames[item.vessel].name}"/>
												<c:set var="vesselTranslatedName"
													   value="${vesselNames[item.vessel].translatedName}"/>
											</c:when>
											<c:otherwise>
												<c:set var="vesselName" value="${item.vessel}"/>
												<c:set var="vesselTranslatedName" value="${item.vessel}"/>
											</c:otherwise>
										</c:choose>
										<a href="${ferryDetailsPageUrl}/${vesselTranslatedName}/${vesselNames[item.vessel].shipInfoCode}">
												${vesselName}
										</a>
									</p>
								</div>
							</td>

							<td width="33.33%" >
								<c:if test="${not empty item.scheduledDeparture}">
									<ul class="list-inline departures-time-ul">
										<li><b>
											<spring:theme code="text.cc.atAglance.scheduled.label" text="SCHEDULED"/>:
										</b></li>
										<li>
											<fmt:parseDate value="${item.scheduledDeparture}" var="dateObject" pattern="yyyy-MM-dd HH:mm:ss"/>
											<fmt:formatDate value="${dateObject}" pattern="h:mm a"/>
										</li>
									</ul>
								</c:if>
								<c:if test="${not empty item.actualDepartureTs}">
									<fmt:parseDate value="${item.actualDepartureTs}" var="actualDateObject" pattern="yyyy-MM-dd HH:mm:ss"/>
									<c:if test="${actualDateObject lt now}">
										<ul class="list-inline departures-time-ul">
											<li><b>
												<spring:theme code="text.cc.atAglance.actual.label" text="ACTUAL"/>:
											</b></li>
											<li>
												<fmt:formatDate value="${actualDateObject}" pattern="h:mm a"/>
											</li>
										</ul>
									</c:if>
								</c:if>

								<ul class="list-inline departures-time-ul">
									<c:choose>
										<c:when test="${item.status eq 'D' }">
											<c:choose>
												<c:when test="${fn:startsWith(item.actualArrivalTs, 'ETA:')}">
													<li>
														<b>
															<spring:theme code="text.eta.label" text="ETA"/>:
														</b>
													</li>

													<c:set var="etaTimeStamp" value="${fn:substringAfter(item.actualArrivalTs, 'ETA: ')}"/>
													<c:choose>
														<c:when test="${fn:contains(etaTimeStamp, 'variable')}">
															<spring:theme code="text.variable.label" text="Variable" />
														</c:when>
														<c:otherwise>
															<fmt:parseDate value="${etaTimeStamp}" var="dateObject"
																		   pattern="yyyy-MM-dd HH:mm:ss"/>
															<li>
																<fmt:formatDate value="${dateObject}" pattern="h:mm a"/>
															</li>
														</c:otherwise>
													</c:choose>
												</c:when>
												<c:otherwise>
													<fmt:parseDate value="${item.actualArrivalTs}" var="dateObject"
																   pattern="yyyy-MM-dd HH:mm:ss"/>
													<c:if test="${dateObject lt now}">
														<li>
															<b>
																<spring:theme code="text.cc.atAglance.arrival.label" text="ARRIVAL"/>:
															</b>
														</li>
														<li>
															<fmt:formatDate value="${dateObject}" pattern="h:mm a"/>
														</li>
													</c:if>
												</c:otherwise>
											</c:choose>
										</c:when>
										<c:otherwise>
											<fmt:parseDate value="${item.scheduledArrival}" var="dateObject" pattern="yyyy-MM-dd HH:mm:ss" />
											<c:if test="${dateObject lt now}">
												<li><b><spring:theme code="text.cc.atAglance.arrival.label"
																	 text="ARRIVAL"/>:</b></li>
												<li>
													<fmt:formatDate value="${dateObject}" pattern="h:mm a"/>
												</li>
											</c:if>
										</c:otherwise>
									</c:choose>
								</ul>

							</td>

							<td width="33.33%" class="text-left"><div class="col-md-5 pull-left text-center">
								    <c:choose>
								    <c:when test="${item.status eq 'S' and item.webDisplaySailingFlag eq 'Y'}">
                                        <div class="text-red"><spring:theme code="text.cc.cancelled.status.label"/></div>
								    </c:when>
								    <c:otherwise>
							${item.departureStatus}
							</c:otherwise>
							</c:choose>
							</div>
						<c:if test="${index.last}">
						    </td>
						    </tr>
						    </c:if>
							<br/>
						</c:if>
						    </c:otherwise>
						    </c:choose>
						<c:if test="${index.last}">
							</table>
							</c:if>
					</c:forEach>
				</c:forEach>
			</c:forEach>


		</div>
	</c:otherwise>
</c:choose>
</div>
