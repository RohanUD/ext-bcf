<%@ taglib prefix="json" uri="http://www.atg.com/taglibs/json"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<spring:url value="/manage-booking/multi-cancel-bookings" var="multiCancelBookingsUrl" />
<json:object escapeXml="false">

			<json:property name="cancelBookingModalHtml">
				<div class="modal fade" id="y_cancelBookingModal" tabindex="-1" role="dialog" aria-labelledby="cancelBookingLabel">
					<div class="modal-dialog" role="document">
						<div class="modal-content">
							<div class="modal-header">
								<button type="button" class="close" data-dismiss="modal" aria-label="Close">
									<span aria-hidden="true"><i class="bcf bcf-icon-cancel-solid"></i></span>
								</button>
								<h3 class="modal-title" id="cancelBookingLabel">
									<spring:theme code="text.page.managemybooking.cancel.booking.modal.title" text="Cancel Booking" />
								</h3>
							</div>
							<div class="modal-body">
								<h3>
								${cancellableOrderCodes}
									<spring:theme code="text.page.managemybooking.cancel.booking.modal.subtitle.text" text="Are you sure you want to cancel the selected booking(s)? This action cannot be undone."/>
								</h3>
								<p>
									<spring:theme code="text.page.managemybooking.cancel.body" text="Cancellations are subject to cancelation fees. Your total refund will be calculated upon cancellation" />
								</p>

							</div>
							<div class="modal-footer">
								<div class="row">
									<div class="col-xs-12 col-sm-6">
										<a id="y_cancelBookingUrl" class="btn btn-primary btn-block" href=""> <spring:theme code="text.page.managemybooking.cancel.booking.confirmation" text="Confirm Cancellation" />
										</a>
									</div>
									<div class="col-xs-12 col-sm-6">
										<button class="btn btn-secondary btn-block" data-dismiss="modal">
											<spring:theme code="text.page.managemybooking.cancel.booking.close" text="No Thanks" />
										</button>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</json:property>

</json:object>
