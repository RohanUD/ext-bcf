<%@ page trimDirectiveWhitespaces="true"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="template" tagdir="/WEB-INF/tags/responsive/common"%>
<%@ taglib prefix="cms" uri="http://hybris.com/tld/cmstags"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="ycommerce" uri="http://hybris.com/tld/ycommercetags"%>
<%@ taglib prefix="common" tagdir="/WEB-INF/tags/responsive/common"%>

<spring:url value="/my-account/update-profile" var="updateProfileUrl" />
<spring:url value="/my-account/update-password" var="updatePasswordUrl" />
<spring:url value="/my-account/update-email" var="updateEmailUrl" />
<spring:url value="/my-account/address-book" var="addressBookUrl" />
<spring:url value="/my-account/payment-details" var="paymentDetailsUrl" />
<spring:url value="/my-account/orders" var="ordersUrl" />
<spring:htmlEscape defaultHtmlEscape="false" />
<template:page pageTitle="${pageTitle}">
<div class="container">
	<div class="row">
	    <div class="col-xs-12 col-sm-12 col-md-12">
		    <h4 class="my-5"><b><spring:theme code="text.account.title" /></b></h4>
		    <hr>
		</div>
	</div>
</div>
	<cms:pageSlot position="BodyContent" var="feature" element="div" class="accountPageBodyContent">
        <cms:component component="${feature}" />
    </cms:pageSlot>
    <cms:pageSlot position="BottomContent" var="feature" element="div" class="accountPageBodyContent">
        <cms:component component="${feature}" />
    </cms:pageSlot>

</template:page>
<c:if test="${cmsPage.uid eq 'update-payment-details'}">
	<script type="text/javascript" src="${commonResourcePath}/js/acc.tokenizer.js"></script>
</c:if>
