<%@ page trimDirectiveWhitespaces="true"%>
<%@ taglib prefix="template" tagdir="/WEB-INF/tags/responsive/common"%>
<%@ taglib prefix="cms" uri="http://hybris.com/tld/cmstags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<a href="/bcfstorefront/drop-trailer/searchMovements" class="btn btn-primary">Search Trailer Movements</a>
<div>
	<c:choose>
		<c:when test="${result == 'noMovementsError'}">
			<p>Result : No trailer movements found for the user!</p>
		</c:when>
		<c:otherwise>
		<p>Result :</p>
			<c:forEach var="entry" items="${result.movements}" varStatus="loop">
				<p class="font-weight-bold">
					<u> Item #${loop.index +1}</u>
				<p>
				<p>Trailer Id : ${entry.trailerId}</p>
				<p>Barcode : ${entry.barcode}</p>
				<p>Departure Terminal : ${entry.departureTerminal}</p>
				<p>Destination Terminal : ${entry.destinationTerminal}</p>
				<p>Reefer : ${entry.reefer}</p>
				<p>EventType : ${entry.eventType}</p>
				<p>Length : ${entry.length}</p>
				<p>Sailing : ${entry.sailing}</p>
				<br>
			</c:forEach>
		</c:otherwise>
	</c:choose>
</div>
