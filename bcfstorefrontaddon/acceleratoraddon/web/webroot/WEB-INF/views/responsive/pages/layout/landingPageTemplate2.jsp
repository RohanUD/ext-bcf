<%@ page trimDirectiveWhitespaces="true"%>
<%@ taglib prefix="template" tagdir="/WEB-INF/tags/responsive/common"%>
<%@ taglib prefix="cms" uri="http://hybris.com/tld/cmstags"%>
<template:page pageTitle="${pageTitle}">
	<cms:pageSlot position="SectionA" var="feature" element="section">
		<cms:component component="${feature}" />
	</cms:pageSlot>

	<cms:pageSlot position="SectionB" var="feature" element="section">
		<cms:component component="${feature}" />
	</cms:pageSlot>

	<cms:pageSlot position="SectionC" var="feature" element="section">
		<cms:component component="${feature}" />
	</cms:pageSlot>

	<cms:pageSlot position="SectionD" var="feature" element="section">
			<cms:component component="${feature}" />
	</cms:pageSlot>

	<cms:pageSlot position="SectionE" var="feature" element="section">
			<cms:component component="${feature}" />
	</cms:pageSlot>



	<cms:pageSlot position="SectionF" var="feature" element="section">
			<cms:component component="${feature}" />
	</cms:pageSlot>



	<cms:pageSlot position="SectionG" var="feature" element="section">
		<cms:component component="${feature}" />
	</cms:pageSlot>



	<cms:pageSlot position="SectionH" var="feature" element="section">
		<cms:component component="${feature}" />
	</cms:pageSlot>



	<cms:pageSlot position="SectionI" var="feature" element="section">
		<cms:component component="${feature}" />
	</cms:pageSlot>



	<cms:pageSlot position="SectionJ" var="feature" element="section">
		<cms:component component="${feature}" />
	</cms:pageSlot>

</template:page>
