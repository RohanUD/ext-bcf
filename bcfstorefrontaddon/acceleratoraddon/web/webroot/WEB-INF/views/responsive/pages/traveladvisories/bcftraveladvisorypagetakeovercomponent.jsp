<%@ page trimDirectiveWhitespaces="true" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="traveladvisory" tagdir="/WEB-INF/tags/addons/bcfstorefrontaddon/responsive/traveladvisory"%>

<c:if test="${fn:length(traveladvisories) eq 1}">
    <c:if test="${not empty sessionScope.suppressAlert and sessionScope.suppressAlert}">
        <traveladvisory:traveladvisorycalloutviewsingle/>
    </c:if>
    <c:if test="${empty sessionScope.suppressAlert}">
        <traveladvisory:traveladvisorysingle/>
    </c:if>
</c:if>

<c:if test="${fn:length(traveladvisories) gt 1}">
    <c:if test="${not empty sessionScope.suppressAlert and sessionScope.suppressAlert}">
        <traveladvisory:traveladvisorycalloutviewmultiple/>
    </c:if>
    <c:if test="${empty sessionScope.suppressAlert}">
        <traveladvisory:traveladvisorymultiple/>
    </c:if>
</c:if>
