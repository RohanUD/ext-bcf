<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<section class="bg-light-gray padding-bottom-30 margin-bottom-30">
<div class="container">
<h2>
    <span id="ExploreCities" name="ExploreCities">
        <spring:theme code="label.destination.details.exploreCities" text="Nearby Cities" />
    </span>
</h2>
    <div class="row flexbox-row">
    <c:if test="${not empty destinationDetails.featuredLocations}">
        <c:forEach items="${destinationDetails.featuredLocations}" var="featuredLocation">
            <div class="col-lg-3 col-md-3 col-sm-3 col-xs-12 destination-city-component flexbox-col">
                <div class="card " style="background:${backgroundColour}">
                  <c:if test="${not empty featuredLocation.normalImage}">
                     <img class='img-fluid  card-img-top img-w-h js-responsive-image' data-media='${featuredLocation.normalImage.url}' alt='${featuredLocation.normalImage.altText}' title='${featuredLocation.normalImage.altText}'/>
                  </c:if>
                  <div class="promo-cards-IIII">
	                  <div class="card-body">
	                  <h4 class="card-title">${featuredLocation.name}</h4>
	                    <div class="card-text p-ellipsis">${featuredLocation.description}</div>
	                     <div><a href="/destinations/${featuredLocation.translatedName}/${featuredLocation.code}">
	                     <b><spring:theme code="label.destination.details.learnMore" text="Learn more >" /></b></a></div>
	                  </div>
                  </div>
                </div>
           </div>
        </c:forEach>
    </c:if>
    </div>
</div>

