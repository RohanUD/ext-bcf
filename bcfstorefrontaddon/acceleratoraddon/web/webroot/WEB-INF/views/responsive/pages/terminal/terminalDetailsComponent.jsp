<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<c:if test="${terminal.active eq true}">
    <div class="container">
        <div class="row">
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 terminal-detail-text">
                <h2><strong>${terminal.name}</strong></h2>
                <p>${terminal.description}</p>


            </div>
        </div>
    </div>

    <div class="container">
        <div class="row terminal-detail-bg m-off-desktop">
            <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12 ">
                <div class="terminal-detail-map">
                    <h4>
                        <strong><spring:theme code="label.terminal.details"
                                              text="Details"/></strong>
                    </h4>
                    <label><spring:theme code="label.terminal.details.address"
                                         text="ADDRESS"/></label>
                    <p>${terminal.pointOfServiceData.address.formattedAddress}</p>
                    <p>
                        <c:choose>
                            <c:when test="${not empty terminal.googleDirections}">
                                <a href="${terminal.googleDirections}">
                                    <spring:theme code="label.terminal.details.directions" text="Get directions >"/>
                                </a>
                            </c:when>
                            <c:otherwise>
                                <a>
                                    <spring:theme code="label.terminal.details.directions" text="Get directions >"/>
                                </a>
                            </c:otherwise>
                        </c:choose>
                    </p>
                    <hr/>
                    <h4>
                        <strong><spring:theme code="label.terminal.details.transitOptions"
                                              text="Transit Options"/></strong>
                    </h4>
                        ${terminal.luggageServiceDescription}
                </div>
            </div>

            <div class="col-lg-8 col-md-8 col-sm-8 col-xs-12 p-0">
                <c:if test="${not empty travelRoutes}">
                    <c:set var="googleApiKey" value="${jalosession.tenant.config.getParameter('google.api.key')}"/>
                    <c:if test="${not empty googleApiKey}">
                        <div class="row">
                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 mb-4">
                                <div class="map-wrap close-icon-manage  y_terminalmapurl" id="terminalmap"
                                     data-googleapi="${googleApiKey}">
                                </div>
                            </div>
                        </div>
                    </c:if>
                </c:if>
            </div>
        </div>
    </div>
    <div id="listOfTravelRouteCount" value="${travelRoutes.size()}"></div>
    <div id="terminalCode" value="${terminal.code}"></div>
    <div id="terminalName" value="${terminal.name}"></div>
    <c:forEach items="${travelRoutes}" var="travelRoute" varStatus="status">
        <c:if test="${travelRoute.mapKmlFileUrl ne null}">
            <input type="hidden" id="routeOriginDestination${status.index}"
                   data-mapkmlfile="${travelRoute.mapKmlFileUrl}"
                   data-mapkmlinternalfilename="${travelRoute.origin.code}-${travelRoute.destination.code}"
                   data-index="${status.index}"/>
        </c:if>
    </c:forEach>

    <div class="container">
        <div id="" class="js-accordion js-accordion-contentpage custom-ferrycontentpage-accordion terminal-detail-accordion">
            <div class="row">
                <div class="col-lg-6 col-md-6 col-xs-12">
                    <c:if test="${terminal.currentConditionEnabled eq true}">
                        <div class="ferrydetails-accordion">
                            <header>
                                <span class="bcf bcf-icon-wave terminal-detail-icons"></span>
                                <spring:theme code="label.terminal.details.currentConditions" text="Current Conditions"/>
                                <span class="custom-arrow"></span>
                            </header>
                            <div>
                                <c:if test="${not empty terminal.travelRoutes}">
                                    <c:forEach items="${terminal.travelRoutes}" var="travelRoute">
                                        <c:url var="sailingStatusUrl" value="/current-conditions/${terminal.name}-${travelRoute.destination.name}/${terminal.code}-${travelRoute.destination.code}" />
                                        <br>
                                        <a href="${sailingStatusUrl}"><spring:theme code="label.terminal.details.toDestination"
                                                                                    text="To"/>
                                            &nbsp;${travelRoute.destination.name}
                                        </a>
                                    </c:forEach>
                                </c:if>
                            </div>
                        </div>
                    </c:if>
                    <div class="js-accordion js-accordion-contentpage custom-ferrycontentpage-accordion mb-3">
                        <div class="ferrydetails-accordion">
                            <header>
                                <span class="bcf bcf-icon-wheelchair terminal-detail-icons"></span>
                                <spring:theme code="label.terminal.details.terminalAccessibility" text="Terminal accessibility"/>
                                <span class="custom-arrow"></span>
                            </header>
                            <div>
                                    ${terminal.accessibilityDescription}
                            </div>
                        </div>
                    </div>
                    <div class="ferrydetails-accordion">
                        <header>
                            <span class="bcf bcf-icon-lost-and-found terminal-detail-icons"></span>
                            <spring:theme code="label.terminal.details.lostFound" text="Lost & Found"/>
                            <span class="custom-arrow"></span>
                        </header>
                        <div>
                            <div>${terminal.contactInformation}</div>
                            <div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="col-lg-6 col-md-6 col-xs-12">
                    <div class="ferrydetails-accordion">
                        <header>
                            <span class="bcf bcf-icon-food terminal-detail-icons"></span>
                            <spring:theme code="label.terminal.details.currentAmenities" text="Terminal Amenities"/>
                            <span class="custom-arrow"></span>
                        </header>
                        <div>
                            <c:if test="${not empty terminal.amenities}">
                                <div class="js-owl-carousel js-owl-rotating-image owl-theme owl-carousel owl-theme ferry-detail-owl">
                                    <c:forEach items="${terminal.amenities}" var="amenity">
                                        <div class="item">
                                            <c:if test="${not empty amenity.image}">
                                                <img class='img-fluid  img-w-h js-responsive-image'
                                                     data-media='${amenity.image}'/>
                                            </c:if>
                                                ${amenity.description}
                                        </div>
                                    </c:forEach>
                                </div>
                            </c:if>
                        </div>
                    </div>
                    <div class="ferrydetails-accordion">
                        <header>
                            <span class="bcf bcf-icon-parking terminal-detail-icons"></span>
                            <spring:theme code="label.terminal.details.parking" text="Parking"/>
                            <span class="custom-arrow"></span>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                            <c:if test="${terminal.currentConditionEnabled eq true and not empty terminal.parkingAvailable}">
                                <spring:theme code="label.terminal.details.parking.available" text="available"
                                              arguments="${terminal.parkingAvailable}"/>
                            </c:if>
                        </header>
                        <div>
                            <c:if test="${terminal.currentConditionEnabled eq true and not empty terminal.parkingAvailable}">
                                <div>
                                    <spring:theme code="label.terminal.details..percentage.parking.available"
                                                  text="% Parking Available"
                                                  arguments="${terminal.parkingAvailable}"/>
                                </div>
                            </c:if>
                            <div>${terminal.parkingInfo}</div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row margin-bottom-40 margin-top-40">
                <div class="col-lg-12 col-md-12 col-xs-12">
                    <c:if test="${not empty terminal.serviceNotices}">
                        <c:set var="count" value="1"/>
                        <c:forEach items="${terminal.serviceNotices}" var="serviceNotice">
                            <c:forEach items="${serviceNotice.travelRoutes}" var="route">
                                <c:if test="${count eq 1}">
                                    <div class="margin-bottom-10">
                                        <spring:theme code="text.cc.notcies.label"/>
                                    </div>
                                    <c:set var="count" value="${count+1}"/>
                                </c:if>

                                <c:if test="${route.sourceTerminalCode eq terminal.code}">
                            <span>
                               <span class="glyphicon glyphicon-exclamation-sign blue-color"></span>
                               <a href="${pageContext.request.contextPath}/current-conditions/service-notices?serviceNoticeCode=${serviceNotice.code}&routeCode=${route.routeCode}">${serviceNotice.title}</a>
                            </span>
                                    &nbsp;
                                </c:if>
                            </c:forEach>
                        </c:forEach>
                    </c:if>
                </div>
            </div>
        </div>
    </div>
</c:if>
<c:if test="${terminal.active eq false}">
    <spring:theme code="label.terminal.details.terminal.not.exist" text="Sorry, Terminal does not exist" />
</c:if>
<div class="container-fluid px-0  py-4 bg-light-gray padding-top-20 padding-bottom-30 margin-bottom-40">
    <div class="container pb-5">
        <div class="row">
        <h4 class="terminal-heading"><strong><spring:theme code="label.our.terminals.find.a.terminal" text="Find a terminal"/></strong></h4>
            <div class="col-lg-8 col-md-8 col-sm-12 col-xs-12 my-3">
                
                <select id="transportFacilitySelect" class="form-control selectpicker">
                    <c:forEach items="${terminalItems}" var="terminalItem">
                        <option data-defaultvalue="${terminalItem.routeName}" name="${terminalItem.routeName}" value="${terminalItem.code}" ${terminal.code eq terminalItem.code ? 'selected' : ''}>${terminalItem.name}</option>
                    </c:forEach>
                </select>
            </div>
            <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12  my-3">
                <h4 class="hidden-xs"> &nbsp;</h4>
                <button id="btnTerminalDetails" class="btn btn-primary btn-block y_findTerminalResults">
                    <spring:theme code="text.our.terminals.find.terminal" text="Find terminal"/>
                </button>
            </div>
        </div>
    </div>
</div>
<input type="hidden" id="contextPath" value="${pageContext.request.contextPath}"/>
<script type="text/javascript" defer="true" src="${commonResourcePath}/js/terminalDetailsComponent.js"></script>
