<%@ page trimDirectiveWhitespaces="true"%>
<%@ taglib prefix="template" tagdir="/WEB-INF/tags/responsive/common"%>
<%@ taglib prefix="cms" uri="http://hybris.com/tld/cmstags"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="formElement" tagdir="/WEB-INF/tags/responsive/formElement"%>
<%@ taglib prefix="progress" tagdir="/WEB-INF/tags/addons/bcfstorefrontaddon/responsive/progress"%>
<%@ taglib prefix="booking" tagdir="/WEB-INF/tags/addons/bcfstorefrontaddon/responsive/booking"%>
<%@ taglib prefix="bookingDetails" tagdir="/WEB-INF/tags/addons/bcfstorefrontaddon/responsive/bookingdetails"%>
<%@ taglib prefix="popupmodals" tagdir="/WEB-INF/tags/addons/bcfvoyageaddon/responsive/popupmodals"%>
<c:url var="submitUrl" value="/checkout/bookingConfirmation" />
<c:url var="payNowUrl" value="/checkout/paynow/${bookingReference}" />

<template:page pageTitle="${pageTitle}">
   <c:if test="${availabilityStatus eq 'ON_REQUEST'}">
      <div class="container">
         <br>
         <b>
            <spring:theme code="text.package.booking.on.request.title" text="Booking is on request"/>
         </b>
         <br>
         <spring:theme code="text.package.booking.on.request.ccard.message"/>
      </div>
   </c:if>
   <div class="container">
      <div class="row">
         <div class="col-lg-12 col-xs-12">
            <div class="vacation-booking-confirm-section">
               <cms:pageSlot position="RightContent" var="feature">
                  <cms:component component="${feature}" element="section" />
               </cms:pageSlot>
            </div>
         </div>
      </div>
   </div>
   <c:if test="${not isTentativeBooking}">
      <div class="container">
         <div class="row">
            <div class="col-lg-12 col-xs-12 mb-5">
               <ul class="list-unstyled payment payment-confirmation-list">
                  <c:if test="${not empty vacationPolicyTermsAndConditions && not empty vacationPolicyTermsAndConditions.packageTermsAndConditions}">
                     <li>
                        ${vacationPolicyTermsAndConditions.packageTermsAndConditions}
                     </li>
                  </c:if>
               </ul>
            </div>
            <div class="col-lg-12 col-xs-12 mb-5">
               <ul class="list-unstyled payment payment-confirmation-list">
                  <c:if test="${not empty vacationPolicyTermsAndConditions && not empty vacationPolicyTermsAndConditions.componentTermsAndConditions}">
                     <c:forEach items="${vacationPolicyTermsAndConditions.componentTermsAndConditions}" var="componentTermsAndCondition" >
                        <c:if test="${not empty componentTermsAndCondition.accommodationOfferings}">
                           <c:forEach items="${componentTermsAndCondition.accommodationOfferings}" var="accommodationOffering" >
                              <li>
                                 <b>${accommodationOffering}</b>
                              </li>
                              <li>
                                 ${componentTermsAndCondition.termsAndConditions}
                              </li>
                           </c:forEach>
                        </c:if>
                        <c:if test="${not empty componentTermsAndCondition.activityProducts}">
                           <c:forEach items="${componentTermsAndCondition.activityProducts}" var="activityProduct">
                              <li>
                                 <b>${activityProduct}</b>
                              </li>
                              <li>
                                 ${componentTermsAndCondition.termsAndConditions}
                              </li>
                           </c:forEach>
                        </c:if>
                     </c:forEach>
                  </c:if>
               </ul>
            </div>
            <div class="col-lg-12 col-xs-12 mb-5">
               <ul class="list-unstyled payment payment-confirmation-list">
                  <c:if test="${not empty bcfNonRefundableCostTermsAndConditions}">
                     <c:forEach items="${bcfNonRefundableCostTermsAndConditions}" var="bcfNonRefundableCostTermsAndCondition">
                        <li>
                           <b>${bcfNonRefundableCostTermsAndCondition.product}</b>
                        </li>
                        <li>
                           ${bcfNonRefundableCostTermsAndCondition.termsAndConditions}
                        </li>
                     </c:forEach>
                  </c:if>
               </ul>
            </div>
         </div>
      </div>
      <div class="container">
         <div class="row margin-bottom-40">
            <div class="col-lg-6 col-md-6 col-md-offset-3">
               <div class="booking-itinerary mb-2">
                  <h4 class="mb-3">
                     <strong>
                        <spring:theme code="label.booking.confirmation.share.itinerary.header" text="Share Itinerary" />
                     </strong>
                  </h4>
                  <p class="mb-3 fnt-14">
                     <spring:theme code="text.booking.confirmation.share.itinerary.info" text="Separate multiple email addresses using comma." />
                  </p>
                  <label>
                     <spring:theme code="label.booking.confirmation.share.itinerary.textbox" text="Email" />
                  </label>
                  <spring:theme var="emailPlaceholder" code="text.booking.confirmation.share.itinerary.textbox.placeholder" text="Recepient email address" />
                  <form id="form-share-itinerary-email">
                     <div class="row">
                        <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                           <input id="share-itinerary-email" name="share-itinerary-email" placeholder="${emailPlaceholder}" type="text" class="form-control email-field mb-3" value="" />
                        </div>
                        <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                           <button type="submit" class="btn btn-outline-blue btn-block form-control">
                              <spring:theme
                                 code="text.booking.confirmation.share.itinerary.button" text="SHARE ITINERARY" />
                           </button>
                        </div>
                     </div>
                  </form>
                  <div>
                     <p class="medium-grey-text mt-0 mb-0 fnt-14 ">
                        <span>
                           <spring:theme code="label.booking.confirmation.share.itinerary.info.text1"/>
                           &nbsp;
                        </span>
                     </p>
                     <span><br></span>
                     <p class="medium-grey-text mt-0 mb-0 fnt-14">
                        <span>
                           <spring:theme code="label.booking.confirmation.share.itinerary.info.text2"/>
                        </span>
                     </p>
                  </div>
               </div>
            </div>
         </div>
      </div>
   </c:if>
   <hr>
   <div class="row margin-bottom-40 margin-top-40">
      <div class="col-lg-6 col-md-6 col-md-offset-3">
         <div class="row">
            <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12 mb-3">
               <button type="submit" class="btn btn-primary btn-block y_addAnotherSailing">
                  <spring:theme code="text.booking.confirmation.sailing.button" text="Add another sailing" />
               </button>
            </div>
            <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
               <button type="submit" class="btn btn-outline-blue btn-block y_redirectToHome">
                  <spring:theme code="text.booking.confirmation.home.button" text="Home" />
               </button>
            </div>
         </div>
      </div>
   </div>
</template:page>
<popupmodals:bookingConfirmationInformationModal />
