<%@ page trimDirectiveWhitespaces="true"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>

<div class="unsubscribe-section">
        <h6>Unsubscribe</h6>
        <div class="text-sm">
            <div class="email-unsubscribe"><span class="bcf bcf-unsubscribe-all"></span> <a href="javascript:void(0)" class="on-underline unsubscribeLink"><spring:theme code="label.my.account.unsubscription.all.text"/></a></div>
            <spring:theme code="label.my.account.subscription.info.text"/>

        </div>

    </div>

    <div class="modal fade" id="y_unsubscribeLinkModal" tabindex="-1" role="dialog">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true"><i class="bcf bcf-icon-cancel-solid"></i></span>
                    </button>
                    <h5 class="modal-title"><spring:theme code="label.my.account.unsubscription.modal.text"/></h5>
                </div>
                <div class="modal-body">
                     <p><spring:theme code="label.my.account.unsubscription.modal.text"/></p>
                </div>
                <div class="modal-footer">
                    <div>
                        <div class="col-sm-6 col-xs-6">
                            <a href="subscriptions/unsubscribe" class="btn btn-outline-blue">
                                <spring:theme code="text.company.disable.confirm.yes" text="Yes" /></a>
                        </div>
                        <div class="col-sm-6 col-xs-6">
                            <button type="button" data-dismiss="modal" aria-label="No" class="btn btn-outline-blue">
							<spring:theme code="text.company.disable.confirm.no" text="No" /></button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
        
