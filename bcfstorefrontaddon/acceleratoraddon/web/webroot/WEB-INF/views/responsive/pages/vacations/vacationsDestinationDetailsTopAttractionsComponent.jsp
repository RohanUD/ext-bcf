<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>

<div class="container">
<div class="row padding-top-30">
	<div class="col-lg-12 col-md-12 col-xs-12">
		<h2 class="text-dark-blue">
                <span id="TopThingsToDo" name="TopThingsToDo">
                    <spring:theme code="label.destination.details.topThingsToSee" text="Top things to do" />
                </span>
		</h2>
	</div>
</div>
<c:if test="${not empty destinationDetails.topAttractions}">
<div class="row destination-details-carousel padding-bottom-30">
     <div class="card discover-box-left-img" style="background:${backgroundColour}">
     <div class="card-no-body">
     <div class="owl-carousel owl-theme js-owl-carousel js-owl-rotating-image">
            <c:forEach items="${destinationDetails.topAttractions}" var="topAttraction" varStatus="loop">
              <div class="owl-item">
            <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
            	  <div class="accommodation-image package-list-owl">
                      <div class="owl-carousel owl-theme js-owl-carousel js-owl-rotating-gallery slide-no-${fn:length(topAttraction.images)}">
		                <c:if test="${not empty topAttraction.images}">
		                     <c:forEach items="${topAttraction.images}" var="image">
		                     	<div class="p-relative border-radius-all">
		                        <img class='img-fluid  img-w-h js-responsive-image' alt='${image.altText}' title='${image.altText}' data-media='${image.dataMedia}'/>
		                     	<div class="slider-count">
		                                <div class="slider-img-thumb"></div>
		                                <div class="slider-num"></div>
		                       </div>
		                       </div>
		                     </c:forEach>
		                </c:if>
                     </div>
                </div>
                </div>
                 <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                 <h3 class="card-title"><b>${topAttraction.title}</b></h3>

                <p class="card-text">${topAttraction.description}</p>
                <p><a class="gotonextslide" data-key="${loop.index}" href="#"><b><spring:theme code="label.destination.details.viewNext" text="View next >" /></b></a></p>
                </div>
                </div>
            </c:forEach>
            </div>
        </div>
     </div>
   </div>
</c:if>
</div>
