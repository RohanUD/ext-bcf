<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>

<div class="container">
<div class="row padding-top-20">
<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 hidden-sm hidden-xs">
    <ul class="list-inline desktop-inline-component">
      <li class="list-inline-item"><a href="#Overview"><spring:theme code="label.destination.details.overviewAndMap" text="Overview & map" /></a></li>
      <li class="list-inline-item"><a href="#TopThingsToDo"><spring:theme code="label.destination.details.topThingsToDo" text="Top things to do" /></a></li>
      <c:if test="${not empty destinationPackages ||  (not empty destinationAccommodations and fn:length(destinationAccommodations.results) gt 0)}">
        <li class="list-inline-item"><a href="#Packages">${destinationDetails.name}&nbsp;<spring:theme code="label.destination.details.packages" text="packages" /></a></li>
      </c:if>
      <c:if test="${not empty activities and isRegionDetailsPage eq false}">
        <li class="list-inline-item"><a href="#ExploreActivities"><spring:theme code="label.destination.details.exploreActivities" text="Explore activities" /></a></li>
      </c:if>
      <li class="list-inline-item"><a href="#ExploreCities"><spring:theme code="label.destination.details.exploreCities" text="Explore cities" /></a></li>
    </ul>
</div>
</div>
</div>
<section class="hidden-xs"><hr /></section>
<div class="container">
<div class="row">
<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 hidden-lg hidden-md p-0">
    <div class="js-accordion js-accordion-default mobile-group-component">
      <h4>Go to</h4>
      <div>
        <ul class="list-group">
          <li class="list-group-item"><a href="#Overview"><spring:theme code="label.destination.details.overviewAndMap" text="Overview & map" /></a></li>
          <li class="list-group-item"><a href="#TopThingsToDo"><spring:theme code="label.destination.details.topThingsToDo" text="Top things to do" /></a></li>
      <c:if test="${not empty destinationPackages}">
          <li class="list-group-item"><a href="#Packages">${destinationDetails.name}&nbsp;<spring:theme code="label.destination.details.packages" text="packages" /></a></li>
      </c:if>
      <c:if test="${not empty activities and isRegionDetailsPage eq false}">
          <li class="list-group-item"><a href="#ExploreActivities"><spring:theme code="label.destination.details.exploreActivities" text="Explore activities" /></a></li>
          </c:if>
          <li class="list-group-item"><a href="#ExploreCities"><spring:theme code="label.destination.details.exploreCities" text="Explore cities" /></a></li>
        </ul>
      </div>
    </div>
</div>
</div>
</div>
