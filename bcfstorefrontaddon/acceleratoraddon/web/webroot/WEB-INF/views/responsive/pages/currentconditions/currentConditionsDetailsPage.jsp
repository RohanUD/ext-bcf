<%@ page trimDirectiveWhitespaces="true" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="cms" uri="http://hybris.com/tld/cmstags" %>
<%@ taglib prefix="ycommerce" uri="http://hybris.com/tld/ycommercetags" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="progress" tagdir="/WEB-INF/tags/addons/bcfvoyageaddon/responsive/progress" %>
<%@ taglib prefix="currentconditions" tagdir="/WEB-INF/tags/addons/bcfvoyageaddon/responsive/currentconditions" %>
<%@ taglib prefix="popupmodals" tagdir="/WEB-INF/tags/addons/bcfvoyageaddon/responsive/popupmodals" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ page language="java" contentType="text/html;charset=UTF-8" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib prefix="ccSailingDetails" tagdir="/WEB-INF/tags/addons/bcfstorefrontaddon/responsive/currentconditions" %>
<c:url var="directionsUrl" value="https://maps.google.com/maps/dir/?api=1&destination="/>
<c:url var="terminalDetailsUrl"
       value="/travel-boarding/terminal-directions-parking-food/${routeInfo.departureRouteName}/${routeInfo.departureTerminalCode}"/>
<%@ taglib prefix="common" tagdir="/WEB-INF/tags/responsive/common" %>
<spring:htmlEscape defaultHtmlEscape="false"/>

<input type="hidden" name="ccOriginCode" value="${routeInfo.departureTerminalCode}">
<input type="hidden" name="ccDestinationCode" value="${routeInfo.arrivalTerminalCode}">

<c:set var="currentConditionsResponseData" value="${currentConditionsData.currentConditionsResponseData[0]}"/>

<div class="container">
    <p class="text-grey">
        <spring:theme code="text.route.label" text="Route"/>
    </p>
    <h3 class="text-dark-blue"><b>${routeInfo.departureTerminalName}&nbsp;<spring:theme
            code="label.terminal.details.toDestination"/>&nbsp;${routeInfo.arrivalTerminalName}</b></h3>
    <c:if test="${showSwitchDirection}">
        <c:url var="switchDirectionUrl"
               value="/current-conditions/${routeInfo.arrivalRouteName}-${routeInfo.departureRouteName}/${routeInfo.arrivalTerminalCode}-${routeInfo.departureTerminalCode}"/>
        <p class="cc-switch-icon">
            <a href="${switchDirectionUrl}">
                <span class="switch-icon"></span>
                <spring:theme code="text.switchDirection.label" text="Swicth direction"/>
            </a>
        </p>
    </c:if>

    <c:if test="${not empty currentConditionsResponseData}">
        <c:set var="vesselNames" value="${currentConditionsData.vessels}"/>

        <div id="ccDetails">
            <ccSailingDetails:currentConditionsSailingDetails currentConditionsData="${currentConditionsData}"/>

                <%-- Cams and timings list --%>
            <ccSailingDetails:currentConditionsDepartures currentConditionsData="${currentConditionsData}"
                                                          routeInfo="${routeInfo}"/>
        </div>

        <%-- Ferry Tracking --%>
        <div id="terminalDetails" class="text-center">
            <h4 class="margin-top-30"><b>${transportFacilityData.name}</b></h4>
            <p class="margin-top-20">
                <c:if test="${not empty transportFacilityData.pointOfServiceData and not empty transportFacilityData.pointOfServiceData.address}">
                    <c:set value="${transportFacilityData.pointOfServiceData.address}" var="address" scope="page" />
                <span># ${address.line1}&nbsp;${address.town}&nbsp;${address.country.isocode}&nbsp;${address.postalCode}</span>
            <p class="margin-top-20">
                        <span>
                            <c:choose>
                                <c:when test="${not empty transportFacilityData.googleDirections}">
                                    <a href="${transportFacilityData.googleDirections}" target="_blank">
                                        <spring:theme code="text.cc.directionsLink.label" text="Get directions"/>
                                        <i class="bcf bcf-icon-right-arrow"></i>
                                    </a>
                                </c:when>
                                <c:otherwise>
                                    <a>
                                        <spring:theme code="text.cc.directionsLink.label" text="Get directions"/>
                                        <i class="bcf bcf-icon-right-arrow"></i>
                                    </a>
                                </c:otherwise>
                            </c:choose>

                        </span>
            </p>
            </c:if>
            </p>
            <p class="margin-top-20">
                <c:forEach var="service" items="${transportFacilityData.transportFacilityServices}">
                    <span class="bcf bcf-icon-${fn:toLowerCase(service)} bcf-amenities-icons"></span>
                </c:forEach>
            </p>
            <div class="row">
                <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12 text-center col-md-offset-4 col-sm-offset-4 margin-top-20">
                    <a href="${terminalDetailsUrl}">
                        <button class="btn btn-primary btn-block"><spring:theme code="text.terminals.details.label"
                                                                                text="Terminal details"/></button>
                    </a>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-md-4 col-sm-offset-2 col-md-offset-2">
                <p class="text-grey margin-top-30"><spring:theme code="text.cc.details.webcams.label" text="Webcams"/> </p>
            </div>
        </div>
        <c:forEach items="${currentConditionsResponseData.cams}" var="cams">
            <currentconditions:currentconditionscam routeCamData="${cams.value}"

                                                    displaySourceMap="true"
                                                    sourceTerminalName="${routeInfo.departureRouteName}"
                                                    sourceLocation="${routeInfo.departureTerminalCode}"

                                                    displayDestniationMap="true"
                                                    destinationTerminalName="${routeInfo.arrivalRouteName}"
                                                    destinationLocation="${routeInfo.arrivalTerminalCode}"
            />
        </c:forEach>
        <hr>
    </c:if>
</div>

<div class="container">
    <div class="row information-div">
        <c:choose>
            <%-- Book This Route Link --%>
            <c:when test="${reservable}">
                <c:url var="bookThisRouteUrl" value="/">
                    <c:param name="sailing" value="true"/>
                    <c:param name="departureLocation" value="${routeInfo.departureTerminalCode}"/>
                    <c:param name="departureLocationName"
                             value="${routeInfo.departureLocationName} - ${routeInfo.departureTerminalName}"/>
                    <c:param name="arrivalLocation" value="${routeInfo.arrivalTerminalCode}"/>
                    <c:param name="arrivalLocationName"
                             value="${routeInfo.arrivalLocationName} - ${routeInfo.arrivalTerminalName}"/>
                    <c:if test="${not empty param['scheduleDate']}">
                        <c:param name="departingDateTime" value="${param['scheduleDate']}"/>
                    </c:if>
                </c:url>
                <spring:theme var="bookThisRouteLabel" code='sailing.link.bookThisRoute.text'/>
                <common:sailingLink url="${bookThisRouteUrl}" imgSource=""
                                    label="${bookThisRouteLabel}"
                                    cssStyle="blue-color bcf bcf-icon-calendar bcf-2x schedules-icons"/>
            </c:when>

            <%-- Caluculate fare Link --%>
            <c:otherwise>
                <c:url var="fareCalculateUrl" value="/routes-fares/fare-calculator">
                    <c:param name="departureLocation" value="${routeInfo.departureTerminalCode}"/>
                    <c:param name="arrivalLocation" value="${routeInfo.arrivalTerminalCode}"/>
                    <c:param name="scheduleDate" value="${scheduleDate}"/>
                </c:url>

                <spring:theme var="calculatefareLabel" code='sailing.link.calculateFare.text'/>
                <common:sailingLink url="${fareCalculateUrl}" imgSource=""
                                    label="${calculatefareLabel}"
                                    cssStyle="blue-color bcf bcf-icon-calendar bcf-2x schedules-icons"/>
            </c:otherwise>
        </c:choose>

        <%-- Go to Schedule Link --%>
        <c:url var="gotoScheduleUrl"
               value="/routes-fares/schedules/${routeInfo.departureRouteName}-${routeInfo.arrivalRouteName}/${routeInfo.departureTerminalCode}-${routeInfo.arrivalTerminalCode}"/>
        <spring:theme var="gotoScheduleLabel" code='sailing.link.gotoSchedule.text'/>
        <common:sailingLink url="${gotoScheduleUrl}" imgSource=""
                            label="${gotoScheduleLabel}" cssStyle="blue-color bcf bcf-icon-time bcf-2x schedules-icons"/>

        <%-- Signup for route notices Link --%>
        <c:url var="subscriptionsUrl" value="/my-account/subscriptions"/>
        <spring:theme var="signupRouteLabel" code='sailing.link.signupForRouteNotices.text'/>
        <common:sailingLink url="${subscriptionsUrl}" imgSource=""
                            label="${signupRouteLabel}" cssStyle="blue-color bcf bcf-icon-email bcf-2x schedules-icons"/>
    </div>
</div>

<popupmodals:totalDeckAvailableModal/>

<script language="JavaScript">
    setInterval('autoRefreshPage()', ${pageRefreshTime});
    var selectedTabVal = "";
    if (selectedTabVal == null || selectedTabVal == 0) {
        selectedTabVal = "tabs-1";
    }

    function autoRefreshPage() {
        // var selectedSailingVal = $(".currentconditions-details-select").val();
        selectedTabVal = $("#selectedTab").val();

        var org = "${routeInfo.departureTerminalCode}";
        var dest = "${routeInfo.arrivalTerminalCode}";
        refreshCurrentConditions1(org, dest);
    }

    function activateTabs() {
        if (selectedTabVal != 'tabs-1') {
            $('a[href=#' + selectedTabVal + ']').click();
        }
    }
</script>
