<%@ page trimDirectiveWhitespaces="true"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<c:url var="switchAccountUrl" value="/my-account/switch-account"/>
<%@ taglib prefix="cms" uri="http://hybris.com/tld/cmstags" %>
<spring:htmlEscape defaultHtmlEscape="false" />
<div class="my-booking">
    <div class="custom-account-profile">
        <div class="account-profile-accordion">
            <h4 class="px-0 mb-0">
                <spring:theme code="text.switch.account.title" text="Use different account"/>
                <span class="bcf bcf-icon-account pull-right use-different-account"></span>
            </h4>
            <div id="preferences" class="switch-account-wrapper">
                <form:form action="${switchAccountUrl}" method="POST">
                    <div class="fieldset">
                        <div class="row">
                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 checkbox-label-align">
                                <div class="switch-account-sec">
                                    <c:forEach items="${userB2BUnits}" var="unit">
                                        <c:if test="${unit.b2bUnitCode ne 'Default'}">
                                            <div data-toggle="buttons">
                                                <label class="btn btn-primary switch-save-btn" data-toggle="modal"
                                                       data-target="#switchAccountModal">
                                                    <input type="radio" name="b2bUnitCode"
                                                           value="${unit.b2bUnitCode}" ${unit.selected ? 'checked="checked"' : ''}
                                                           autocomplete="off">
                                                    <div class="account-list">
                                                        <div class="account-user-icon pule-left">
                                                            <span class="bcf bcf-icon-account"/>
                                                        </div>
                                                        <p><span>${unit.displayName}</span><br/>
                                                            <span class="sm-text">${unit.userCode}</span>
                                                        </p>
                                                    </div>
                                                </label>
                                            </div>
                                        </c:if>
                                    </c:forEach>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="modal" tabindex="-1" role="dialog" id="switchAccountModal">
                        <div class="modal-dialog" role="document">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <h5 class="modal-title inline-block"><spring:theme code="text.account.switch.modal.header"
                                                                                       text="Switch account"/></h5>
                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                        <span aria-hidden="true"><i class="bcf bcf-icon-cancel-solid"></i></span>
                                    </button>
                                </div>
                                <div class="modal-body">
                                    <p><spring:theme code="text.account.switch.modal.body"
                                                     text="Are you sure you want to switch account? Cart associated with current account will be lost."/></p>
                                </div>
                                <div class="modal-footer">
                                    <div class="row">
                                        <div class="col-xs-12 col-sm-6">
                                            <button type="submit" class="btn btn-primary btn-block"><spring:theme
                                                    code="text.company.disable.confirm.yes" text="Yes"/></button>
                                        </div>
                                        <div class="col-xs-12 col-sm-6">
                                            <button type="button" class="btn btn-secondary btn-block" data-dismiss="modal">
                                                <spring:theme
                                                        code="text.company.disable.confirm.no" text="No"/></button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </form:form>
            </div>
        </div>
    </div>
</div>
