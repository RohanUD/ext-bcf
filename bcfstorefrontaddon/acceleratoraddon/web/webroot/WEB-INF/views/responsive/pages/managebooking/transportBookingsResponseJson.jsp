<%@ page trimDirectiveWhitespaces="true" contentType="application/json"%>
<%@ taglib prefix="json" uri="http://www.atg.com/taglibs/json"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="format" tagdir="/WEB-INF/tags/addons/bcfstorefrontaddon/responsive/format"%>
<%@ taglib prefix="transportreservation" tagdir="/WEB-INF/tags/addons/bcfvoyageaddon/responsive/reservation"%>
<json:object escapeXml="false">
	<json:property name="htmlContent">
		<c:choose>
			<c:when test="${hasErrorFlag}">
				<div class="alert alert-danger" role="alert">
					<p>
						<spring:message code="${errorMsg}" text="${errorMsg}" />
					</p>
				</div>
			</c:when>
			<c:otherwise>
				<c:if test="${not empty originalReservation and not empty originalReservation.reservationDatas}">
					<c:forEach items="${originalReservation.reservationDatas}" var="reservationData" varStatus="reservationItemIdx">
						<div class="js-accordion js-accordion-default original-booking-detail-accordion">
							<c:if test="${not empty reservationData.reservationItems}">
								<c:forEach items="${reservationData.reservationItems}" var="item" varStatus="itemIdx">
									<c:if test="${item.bookingStatus ne 'CANCELLED'}">
										<h4 class="original-booking-detail-title">
											<span class="original-booking-detail-title-view"><spring:theme code="text.ferry.common.view.original.booking.title" text="View booking before changes" /></span> <span class="original-booking-detail-title-hide hidden"><spring:theme
													code="text.ferry.common.hide.original.booking.title" text="Hide booking before changes" /></span>
										</h4>
										<div class="add-on-box">
											<c:choose>
												<c:when test="${item.reservationItinerary.originDestinationOptions[0].originDestinationRefNumber == 0}">
													<c:set var="labelPrefix" value="label.payment.reservation.transport.departure.sailing.total"></c:set>
												</c:when>
												<c:when test="${item.reservationItinerary.originDestinationOptions[0].originDestinationRefNumber == 1}">
													<c:set var="labelPrefix" value="label.payment.reservation.transport.return.sailing.total"></c:set>
												</c:when>
											</c:choose>
											<div class="text-center">
												<c:choose>
													<c:when test="${item.reservationItinerary.originDestinationOptions[0].originDestinationRefNumber == 0}">
														<c:set var="labelPrefix" value="label.payment.reservation.transport.departure.sailing.time.prefix"></c:set>
													</c:when>
													<c:when test="${item.reservationItinerary.originDestinationOptions[0].originDestinationRefNumber == 1}">
														<c:set var="labelPrefix" value="label.payment.reservation.transport.return.sailing.time.prefix"></c:set>
													</c:when>
												</c:choose>
												<fmt:formatDate var="departureDate" pattern="EEE, MMM dd, yyyy" value="${item.reservationItinerary.originDestinationOptions[0].transportOfferings[0].departureTime}" />
												<strong><spring:theme code="${labelPrefix}" text="Departs" />:</strong>&nbsp;${departureDate}
											</div>
											<div class="mt-4">
												<transportreservation:sailinginformation itinerary="${item.reservationItinerary}" />
											</div>
											<transportreservation:travellercount vehicleBreakdownList="${item.reservationPricingInfo.itineraryPricingInfo.vehicleFareBreakdownDatas}" ptcBreakdownList="${item.reservationPricingInfo.itineraryPricingInfo.ptcFareBreakdownDatas}" />
											<input type="hidden" id="vesselCode" value="${item.reservationItinerary.originDestinationOptions[0].transportOfferings[0].transportVehicle.vehicleInfo.code}"/>
											<div class="row">
												<div class="col-md-12">
													<p class="text-center my-3">
													    <i>
                                                            <span class="text-capitalize">
                                                                <a href="/ship-info/${item.reservationItinerary.originDestinationOptions[0].transportOfferings[0].transportVehicle.vehicleInfo.code}"
                                                                    data-toggle="modal" data-target="#detailsModal" id="shipInfoPopUp">
                                                                        ${item.reservationItinerary.originDestinationOptions[0].transportOfferings[0].transportVehicle.vehicleInfo.name}
                                                                </a>
                                                            </span>
                                                        </i>
													</p>
												</div>
												<div class="col-md-10 col-md-offset-1">
													<div class="vp-box">
														<h5 class="my-3">
															<strong><spring:theme code="text.page.managemybooking.bookingdetails.bundle.name.${item.reservationPricingInfo.itineraryPricingInfo.bundleType}" /></strong>
														</h5>
														<p>
															<spring:theme code="text.page.managemybooking.bookingdetails.bundle.description.${item.reservationPricingInfo.itineraryPricingInfo.bundleType}" />
														</p>
														<h5 class="my-3">
															<strong><spring:theme code="label.payment.reservation.traveller.header" text="Vehicles & passengers" /></strong>
														</h5>
														<ul class="list-unstyled payment-vp">
															<transportreservation:vehicleBreakdown vehicleBreakdownList="${item.reservationPricingInfo.itineraryPricingInfo.vehicleFareBreakdownDatas}" />
															<transportreservation:ptcBreakdown ptcBreakdownList="${item.reservationPricingInfo.itineraryPricingInfo.ptcFareBreakdownDatas}" travellers="${item.reservationItinerary.travellers}" routeType="${item.reservationItinerary.route.routeType}" />
															<c:forEach var="fee" items="${item.reservationPricingInfo.totalFare.fees}">
																<c:if test="${fee.price.value > 0.0}">
																	<li>
																		<div class="row">
																			<ul class="col-xs-9 col-md-9">
																				<li>${fee.quantity}&nbsp;x&nbsp;${fee.name}</li>
																			</ul>
																			<ul class="col-xs-3 col-md-3 text-right">
																				<li>
																					<format:price priceData="${fee.price}" />
																				</li>
																			</ul>
																		</div>
																	</li>
																</c:if>
															</c:forEach>
														</ul>
														<c:if test="${item.reservationItinerary.route.routeType eq 'LONG' and not empty item.reservationPricingInfo.offerBreakdowns}">
															<h5 class="my-3">
																<strong><spring:theme code="reservation.journey.extras" text="Your Extras:" /></strong>
															</h5>
															<ul class="list-unstyled payment-vp">
																<transportreservation:ancillaries offerBreakdowns="${item.reservationPricingInfo.offerBreakdowns}" />
															</ul>
														</c:if>
														<c:if test="${not empty item.reservationPricingInfo.itineraryPricingInfo.largeItemsBreakdownDataList}">
															<transportreservation:largeItems largeItems="${item.reservationPricingInfo.itineraryPricingInfo.largeItemsBreakdownDataList}" />
														</c:if>
														<c:if test="${not empty item.reservationPricingInfo.totalFare.taxes and not empty item.reservationPricingInfo.totalFare.discounts}">
															<div>
																<h5 class="my-3">
																	<strong><spring:theme code="label.payment.reservation.sailing.price.breakup.header" text="Taxes fees & discounts" /></strong>
																</h5>
																<ul class="list-unstyled payment-vp">
																	<c:forEach var="tax" items="${item.reservationPricingInfo.totalFare.taxes}">
																		<c:if test="${tax.price.value > 0.0}">
																			<li>
																				<div class="row">
																					<ul class="col-xs-9 col-md-9">
																						<li>
																							<spring:theme code="${tax.code}" />
																						</li>
																					</ul>
																					<ul class="col-xs-3 col-md-3 text-right">
																						<li>
																							<format:price priceData="${tax.price}" />
																						</li>
																					</ul>
																				</div>
																			</li>
																		</c:if>
																	</c:forEach>
																	<c:forEach var="discount" items="${item.reservationPricingInfo.totalFare.discounts}">
																		<c:if test="${discount.price.value < 0.0}">
																			<li>
																				<div class="row">
																					<ul class="col-xs-9 col-md-9">
																						<li>
																							<spring:theme code="${discount.purpose}" />
																						</li>
																					</ul>
																					<ul class="col-xs-3 col-md-3 text-right">
																						<li>
																							<format:price priceData="${discount.price}" />
																						</li>
																					</ul>
																				</div>
																			</li>
																		</c:if>
																	</c:forEach>
																</ul>
															</div>
														</c:if>
													</div>
													<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 mb-5">
														<ul class="list-unstyled payment payment-confirmation-list">
															<li class="green">
																<spring:theme code="text.ferry.calculator.summary.departure.sailing.total" text="Departure sailing total" />
																<span><strong><format:price priceData="${item.reservationPricingInfo.totalFare.totalPrice}" /></strong></span>
															</li>
														</ul>
													</div>
												</div>
											</div>
										</div>
									</c:if>
								</c:forEach>
							</c:if>
						</div>
					</c:forEach>
				</c:if>
			</c:otherwise>
		</c:choose>
	</json:property>
</json:object>
