<%@ page trimDirectiveWhitespaces="true"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="template" tagdir="/WEB-INF/tags/responsive/common"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="reservation" tagdir="/WEB-INF/tags/addons/bcfstorefrontaddon/responsive/reservation"%>
<%@ taglib prefix="theme" tagdir="/WEB-INF/tags/shared/theme"%>
<%@ taglib prefix="formElement" tagdir="/WEB-INF/tags/responsive/formElement"%>
<%@ taglib prefix="cms" uri="http://hybris.com/tld/cmstags"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="address" tagdir="/WEB-INF/tags/addons/bcfcheckoutaddon/responsive/address"%>
<%@ taglib prefix="checkout" tagdir="/WEB-INF/tags/addons/bcfcheckoutaddon/responsive/checkout/multi"%>
<%@ taglib prefix="voucher" tagdir="/WEB-INF/tags/addons/bcfcheckoutaddon/responsive/voucher"%>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags"%>
<%@ taglib prefix="ycommerce" uri="http://hybris.com/tld/ycommercetags"%>
<%@ taglib prefix="account" tagdir="/WEB-INF/tags/addons/bcfstorefrontaddon/responsive/account"%>
<%@ taglib prefix="progress" tagdir="/WEB-INF/tags/addons/bcfstorefrontaddon/responsive/progress"%>
<%@ taglib prefix="common" tagdir="/WEB-INF/tags/responsive/common"%>
<%@ taglib prefix="ferryprogress" tagdir="/WEB-INF/tags/addons/bcfvoyageaddon/responsive/progress"%>
<%@ taglib prefix="paymentmethod" tagdir="/WEB-INF/tags/addons/bcfcheckoutaddon/responsive/checkout/multi/cms"%>
<%@ taglib prefix="modifybooking" tagdir="/WEB-INF/tags/addons/bcfvoyageaddon/responsive/modifybooking"%>
<%@ taglib prefix="termsandconditions" tagdir="/WEB-INF/tags/addons/bcfstorefrontaddon/responsive/termsandconditions"%>
<%@ taglib prefix="checkout" tagdir="/WEB-INF/tags/addons/bcfcheckoutaddon/responsive/checkout/multi"%>

<spring:htmlEscape defaultHtmlEscape="false" />
<template:page pageTitle="${pageTitle}" hideHeaderLinks="true">
    <cms:pageSlot position="TopHeader" var="feature">
        <cms:component component="${feature}" element="section" />
    </cms:pageSlot>
    <c:url value="${currentStepUrl}" var="choosePaymentMethodUrl" />
    <c:url value="${paymentFormUrl}" var="submitButtonURL" />
    <c:set var="receiptNoidLabel">
	    <spring:theme code="checkout.paymentType.receiptNo.placeholder" />
    </c:set>
    <input type="hidden" id="hiddenModalVar" value="${hiddenModalVar}" />
    <div class="${isModifyingTransportBooking ? 'panel panel-primary modify-sailing-panel' : ''}">
    <div class="container">
    <c:url var="submitRefundUrl" value="${addRefundUrl}" />

    <div class="col-md-6 col-sm-6 col-xs-12">
        <c:if test="${not empty refundOptionForm.maxRefundAmount}">
            <spring:theme code="text.refund.total.amount" text="Total Refund Amount:"/>${refundOptionForm.maxRefundAmount}
         </c:if>
	</div>

     <form:form id="y_refundSelectionForm"  commandName="refundOptionForm" class="y_refundSelectionForm" action="${submitRefundUrl}" method="POST">

        <c:if test="${not empty refundReasonCodeData}">
            <div class="package-gray-box price-option accommodation-grey-bg">
                 <div class="row">
                        <div class="col-md-6 col-sm-6 col-xs-12">
                             <spring:theme code="label.apply.goodwill.discount.reasoncode"
                                                                                        text="Reason code" />
                            <form:select class="form-control refund-code" id="y_refundCode"
                                path="goodWillRefundCode">
                                <c:forEach items="${refundReasonCodeData}"
                                    var="refundReasonCode">
                                    <form:option value="${refundReasonCode.code}" htmlEscape="true">${fn:escapeXml(refundReasonCode.code)}</form:option>
                                </c:forEach>
                            </form:select>
                        </div>


                        <div class="col-md-6 col-sm-6 col-xs-12">
                            <spring:theme code="label.apply.goodwill.discount.comment"
                                text="Comment" />
                            <form:input type="text" path="goodWillRefundComment"
                                class="col-xs-12 input-grid form-control" id="comment"
                                name="goodWillRefundComment" value=""></form:input>

                        </div>

                </div>
            </div>

	    </c:if>

        <c:forEach var="refundSelection" items="${refundOptionForm.refundSelectionForms}" varStatus="i">
	        <div class="package-gray-box price-option accommodation-grey-bg">
			    <div class="row">
						<div class="col-md-6 col-sm-6 col-xs-12">
                            <spring:theme code="text.refund.type" text="Refund Type:"/> ${refundOptionForm.refundSelectionForms[i.index].refundType}
						</div>

						<div class="col-md-6 col-sm-6 col-xs-12">
                            <spring:theme code="text.refund.amount" text="Amount" />
                            <form:input type="text" path="refundSelectionForms[${i.index}].amount"
                                class="col-xs-12 input-grid form-control" id="amount"
                                name="amount" value="${refundOptionForm.refundSelectionForms[i.index].amount}"></form:input>
                        </div>

                        <div class="col-md-6 col-sm-6 col-xs-12">
                            <spring:theme code="text.refund.reason" text="Refund Reason" />
                            <form:input type="text" path="refundSelectionForms[${i.index}].refundReason"
                                class="col-xs-12 input-grid form-control" id="refundReason"
                                name="refundReason" value=""></form:input>
                        </div>

                        <div class="col-md-6 col-sm-6 col-xs-12">
                         <c:if test="${not empty refundOptionForm.refundSelectionForms[i.index].maxAmount}">
                            <spring:theme code="text.refund.maximum.amount" text="Maximum Amount:"/>${refundOptionForm.refundSelectionForms[i.index].maxAmount}
                            </c:if>

                        </div>

                        <form:input type="hidden"  path="refundSelectionForms[${i.index}].transactionCode" value="${refundOptionForm.refundSelectionForms[i.index].transactionCode}" />
                        <form:input type="hidden"  path="refundSelectionForms[${i.index}].maxAmount" value="${refundOptionForm.refundSelectionForms[i.index].maxAmount}" />
                        <form:input type="hidden"  path="refundSelectionForms[${i.index}].refundType" value="${refundOptionForm.refundSelectionForms[i.index].refundType}" />


                </div>
             </div>
         </c:forEach>
            <form:errors></form:errors>

         <checkout:captureLeadPaxDetails />


         <form:input type="hidden"  path="bookingReference" value="${refundOptionForm.bookingReference}" />
         <form:input type="hidden"  path="maxRefundAmount" value="${refundOptionForm.maxRefundAmount}" />




	<input type="hidden" id="ht_token" />
	<input type="hidden" id="ht_id" value="${paymentTokenizerClientID }" />
	<input type="hidden" id="ht_host" value="${paymentTokenizerHostURL }" />
	<input type="hidden" id="ht_host_tokenizerURL" value="${paymentTokenizerURL }" />

	<cms:pageSlot position="Reservation" var="feature" element="section">
        <cms:component component="${feature}" />
    </cms:pageSlot>
        <div class="container">
            <div class="vacation-booking-details">
                <div class="row">
	                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 mb-5">
                        <c:if test="${not empty cancelFee}">
                            <ul class="list-unstyled payment payment-confirmation-list">
                                <li>
                                    <spring:theme code="text.page.managemybooking.cancel.cancelFee" />
                                    <span>${cancelFee}</span>
                                </li>
                            </ul>

                        </c:if>
                    </div>
                 </div>
             </div>
         </div>

	<c:if test="${salesChannel=='Web'}">
        <termsandconditions:customertermsandconditions />
    </c:if>
     <c:if test="${salesChannel=='CallCenter' || salesChannel=='TravelCentre' || salesChannel=='TravelCentreFerryOnly'}">
        <termsandconditions:customerandbcftermsandconditions />

     </c:if>

     <div class="row mt-5">
                 <div class="col-md-12 col-sm-12 col-xs-12">
                     <button type="submit" class="btn btn-primary btn-block"
                         id="refundSelectionFormButton">
                         <spring:theme code="label.apply.goodwill.discount.submit"
                             text="Submit Refund" />
                      </button>
                 </div>


             </div>

     </form:form>

      <cms:pageSlot position="InternalComments" var="features">
            <cms:component component="${features}" element="section" />
        </cms:pageSlot>

      <div class="col-md-12 col-sm-12 col-xs-12">
          <a class="btn btn-secondary mb-3 mt-1" href="${cancelUrl}">
              <spring:theme code="text.cancel.label" text="Cancel" />
          </a>

       </div>
     </div>
	<checkout:CartValidationWebModal />
    <modifybooking:abortCancellation/>
</div>
</template:page>
<c:if test="${salesChannel=='Web'}">
	<script type="text/javascript" src="${commonResourcePath}/js/acc.tokenizer.js"></script>
</c:if>
