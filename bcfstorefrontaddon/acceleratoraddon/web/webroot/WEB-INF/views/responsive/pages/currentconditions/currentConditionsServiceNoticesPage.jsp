<%@ page trimDirectiveWhitespaces="true" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="cms" uri="http://hybris.com/tld/cmstags" %>
<%@ taglib prefix="ycommerce" uri="http://hybris.com/tld/ycommercetags" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="progress" tagdir="/WEB-INF/tags/addons/bcfvoyageaddon/responsive/progress" %>
<%@ taglib prefix="currentconditions" tagdir="/WEB-INF/tags/addons/bcfvoyageaddon/responsive/currentconditions" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ page language="java" contentType="text/html;charset=UTF-8" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib prefix="template" tagdir="/WEB-INF/tags/responsive/common" %>
<%@ taglib prefix="ccSailingDetails" tagdir="/WEB-INF/tags/addons/bcfstorefrontaddon/responsive/currentconditions" %>

<spring:htmlEscape defaultHtmlEscape="false"/>
<div class="container service-notice">
	<div class="row">
		<div class="col-lg-9 col-md-9 col-sm-9 col-xs-12">
			<c:if test="${empty serviceNoticeDetails}">
				<%-- Page Title and description --%>
				<h2 class="title">
					<spring:theme code="text.cc.atAglance.serviceNotices.label"
						text="Service notices" />
				</h2>
			</c:if>

			<c:choose>
				<c:when test="${not empty paginatedServiceNotices}">
					<ccSailingDetails:serviceNotices
						searchPageData="${paginatedServiceNotices}" />
				</c:when>
				<c:when test="${not empty serviceNoticeDetails}">
					<ccSailingDetails:serviceNoticeDetails
						serviceNotice="${serviceNoticeDetails}" />
				</c:when>
				<c:otherwise>
					<spring:theme code="text.cc.atAglance.serviceNotices.empty.label" />
				</c:otherwise>
			</c:choose>
		</div>
	</div>
</div>
