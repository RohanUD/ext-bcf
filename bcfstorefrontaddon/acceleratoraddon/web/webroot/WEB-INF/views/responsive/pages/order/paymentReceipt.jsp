<%@ page trimDirectiveWhitespaces="true"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="format" tagdir="/WEB-INF/tags/addons/bcfstorefrontaddon/responsive/format"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<style>
@page {
	size: auto;
	margin: 0mm;
	margin-left: 12mm;
}
@media print {
	div {
		font-size: 10px;
		line-height: 1.1;
		size: auto;
	}
	section {
	margin-top: 2.5em;
	max-width: 250px;
	}
}
</style>
<div class="container payment-receipt">
	<c:choose>
		<c:when test="${receiptData eq null}">
			<div>
				<spring:theme code="error.payment.receipt.unable.to.print" text="We are unable to print receipt this time, please try again later" />
			</div>
		</c:when>
		<c:otherwise>
			<div>
				<section class="card-holder-copy">
					<div class="bcf-logo-receipt-page">
						<img alt="BC Ferries logo" src="${contextPath}/_ui/responsive/common/images/bcf-vacations-logo.png" width="200" />
					</div>
					<div>${receiptData.businessAddressLine1}</div>
					<div>${receiptData.businessAddressLine2}</div>
					<div>${receiptData.businessAddressLine3}</div>
					<div>${receiptData.businessContactNumber}</div>
					<div>
						<spring:theme code="text.payment.receipt.gst.number" text="GST Number" />
						:&nbsp;${receiptData.businessGSTNumber}
					</div>
					<div>
						<spring:theme code="text.payment.receipt.booking.id" text="Booking ID" />
						:&nbsp;${receiptData.cartOrOrderId}
					</div>
					<div>
						<spring:theme code="text.payment.receipt.contact.email" text="Email" />
						:&nbsp;${receiptData.businessContactEmail}
					</div>
					<br>
					<div>${receiptData.receiptRetainInfo}</div>
					<div>${receiptData.transactionType}&nbsp;<fmt:formatDate pattern="yyyy/MM/dd" value="${receiptData.transactionTimestamp}" />
					</div>
					<c:choose>
						<c:when test="${not empty receiptData.debitAccountType}">
							<div>${receiptData.cardType}&nbsp;${receiptData.debitAccountType}&nbsp;${receiptData.cardNumberToPrint}&nbsp;<format:price priceData="${receiptData.transactionAmount}" />
							</div>
						</c:when>
						<c:otherwise>
							<div>${receiptData.cardType}&nbsp;${receiptData.cardNumberToPrint}&nbsp;<format:price priceData="${receiptData.transactionAmount}" />
							</div>
						</c:otherwise>
					</c:choose>
					<div>
						<spring:theme code="text.payment.receipt.auth" text="AUTH" />
						&nbsp;${receiptData.approvalNumber}&nbsp;${receiptData.serviceProviderPaymentTerminalId}&nbsp;${receiptData.transactionRefNumber}&nbsp;${receiptData.cardEntryMethod}
					</div>
					<div>${receiptData.emvApplicationPreferredName}</div>
					<div>${receiptData.emvAid}&nbsp;/&nbsp;${receiptData.emvTvr}&nbsp;/&nbsp;${receiptData.emvTsi}</div>
					<div>${receiptData.emvPinEntryMessage}</div>
					<br>
					<div>${receiptData.approvalOrDeclineMessage}</div>
					<br>
					<div>${receiptData.receiptCardHolderCopy}</div>
					<br>
					<div>
						<fmt:formatDate pattern="dd MMM yyyy HH:mm:ss" value="${receiptData.transactionTimestamp}" />
					</div>
				</section>
				<div class="pagebreak"></div>
				<br>
				<section class="merchant-copy">
					<div class="bcf-logo-receipt-page">
						<img alt="BC Ferries logo" src="${contextPath}/_ui/responsive/common/images/bcf-vacations-logo.png" width="200" />
					</div>
					<div>${receiptData.businessAddressLine1}</div>
					<div>${receiptData.businessAddressLine2}</div>
					<div>${receiptData.businessAddressLine3}</div>
					<div>${receiptData.businessContactNumber}</div>
					<div>
						<spring:theme code="text.payment.receipt.gst.number" text="GST Number" />
						:&nbsp;${receiptData.businessGSTNumber}
					</div>
					<div>
						<spring:theme code="text.payment.receipt.booking.id" text="Booking ID" />
						:&nbsp;${receiptData.cartOrOrderId}
					</div>
					<div>
						<spring:theme code="text.payment.receipt.contact.email" text="Email" />
						:&nbsp;${receiptData.businessContactEmail}
					</div>
					<br>
					<div>${receiptData.receiptRetainInfo}</div>
					<div>${receiptData.transactionType}&nbsp;<fmt:formatDate pattern="yyyy/MM/dd" value="${receiptData.transactionTimestamp}" />
					</div>
					<c:choose>
						<c:when test="${not empty receiptData.debitAccountType}">
							<div>${receiptData.cardType}&nbsp;${receiptData.debitAccountType}&nbsp;${receiptData.cardNumberToPrint}&nbsp;<format:price priceData="${receiptData.transactionAmount}" />
							</div>
						</c:when>
						<c:otherwise>
							<div>${receiptData.cardType}&nbsp;${receiptData.cardNumberToPrint}&nbsp;<format:price priceData="${receiptData.transactionAmount}" />
							</div>
						</c:otherwise>
					</c:choose>
					<div>
						<spring:theme code="text.payment.receipt.auth" text="AUTH" />
						&nbsp;${receiptData.approvalNumber}&nbsp;${receiptData.serviceProviderPaymentTerminalId}&nbsp;${receiptData.transactionRefNumber}&nbsp;${receiptData.cardEntryMethod}
					</div>
					<div>${receiptData.emvApplicationPreferredName}</div>
					<div>${receiptData.emvAid}&nbsp;/&nbsp;${receiptData.emvTvr}&nbsp;/&nbsp;${receiptData.emvTsi}</div>
					<div>${receiptData.emvPinEntryMessage}</div>
					<c:if test="${receiptData.signatureRequired}">
						<br>
						<div>-------------------------</div>
						<div>
							<spring:theme code="text.payment.receipt.signature" text="SIGNATURE" />
						</div>
					</c:if>
					<c:if test="${receiptData.transactionType eq 'PURCHASE'}">
						<div>${receiptData.receiptMerchantMessage}</div>
					</c:if>
					<br>
					<div>${receiptData.approvalOrDeclineMessage}</div>
					<br>
					<div>${receiptData.receiptMerchantCopy}</div>
					<br>
					<div>
						<fmt:formatDate pattern="dd MMM yyyy HH:mm:ss" value="${receiptData.transactionTimestamp}" />
					</div>
				</section>
			</div>
		</c:otherwise>
	</c:choose>
</div>
