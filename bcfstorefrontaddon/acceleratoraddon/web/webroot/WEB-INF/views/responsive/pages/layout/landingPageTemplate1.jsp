<%@ page trimDirectiveWhitespaces="true"%>
<%@ taglib prefix="template" tagdir="/WEB-INF/tags/responsive/common"%>
<%@ taglib prefix="cms" uri="http://hybris.com/tld/cmstags"%>
<template:page pageTitle="${pageTitle}">
	<div class="wrapper">
		<cms:pageSlot position="SectionA" var="feature" element="section">
			<cms:component component="${feature}" />
		</cms:pageSlot>

		<div class="homepage-search-box">
			<div class="container">
				<div class="row">
					<div class="col-sm-12 form-box-landing form-box-landing-mob ">
						<cms:pageSlot position="SectionB" var="feature" element="section" id ="js-vacation-form" >
								<cms:component component="${feature}" />
						</cms:pageSlot>
					</div>
				</div>
			</div>
		</div>
	</div>
	<cms:pageSlot position="SectionC" var="feature" element="section">
		<cms:component component="${feature}" />
	</cms:pageSlot>

	<cms:pageSlot position="SectionD" var="feature" element="section">
			<cms:component component="${feature}" />
	</cms:pageSlot>

	<cms:pageSlot position="SectionE" var="feature" element="section">
			<cms:component component="${feature}" />
	</cms:pageSlot>

	<cms:pageSlot position="SectionF" var="feature" element="section">
			<cms:component component="${feature}" />
	</cms:pageSlot>


	<cms:pageSlot position="SectionG" var="feature" element="section">
		<cms:component component="${feature}" />
	</cms:pageSlot>


	<cms:pageSlot position="SectionH" var="feature" element="section">
			<cms:component component="${feature}" />
	</cms:pageSlot>

	<cms:pageSlot position="SectionI" var="feature" element="section">
		<cms:component component="${feature}" />
	</cms:pageSlot>


	<cms:pageSlot position="SectionJ" var="feature" element="section">
			<cms:component component="${feature}" />
	</cms:pageSlot>

	<cms:pageSlot position="SectionK" var="feature" element="section">
		<cms:component component="${feature}" />
	</cms:pageSlot>

	<cms:pageSlot position="SectionL" var="feature" element="section">
			<cms:component component="${feature}" />
	</cms:pageSlot>

	<cms:pageSlot position="SectionM" var="feature" element="section">
		<cms:component component="${feature}" />
	</cms:pageSlot>

	<cms:pageSlot position="SectionN" var="feature" element="section">
			<cms:component component="${feature}" />
	</cms:pageSlot>

</template:page>

