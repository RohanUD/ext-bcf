<%@ taglib prefix="json" uri="http://www.atg.com/taglibs/json"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<json:object escapeXml="false">
	<json:property name="isCancelPossible" value="${isCancelPossible}" />
	<c:choose>
		<c:when test="${isCancelPossible}">
			<json:property name="cancelBookingModalHtml">
				<div class="modal fade" id="y_cancelBookingModal" tabindex="-1" role="dialog" aria-labelledby="cancelBookingLabel">
					<div class="modal-dialog" role="document">
						<div class="modal-content">
							<div class="modal-header">
								<button type="button" class="close" data-dismiss="modal" aria-label="Close">
									<span aria-hidden="true"><i class="bcf bcf-icon-cancel-solid"></i></span>
								</button>
								<h3 class="modal-title" id="cancelBookingLabel">
									<spring:theme code="text.page.managemybooking.cancel.${cancelOrderType}booking.modal.title" text="Cancel Booking" />
								</h3>
							</div>
							<div class="modal-body">
								<c:if test="${not empty departureTimeWarning}">
									<font color="red"> <spring:theme code="${departureTimeWarning}" text="The first sailing in this booking is going to depart within an hour." />
									</font>
								</c:if>
								<h3>
									<spring:theme code="text.page.managemybooking.cancel.${cancelOrderType}booking.modal.subtitle" />
								</h3>
								<p>
									<spring:theme code="text.page.managemybooking.cancel.${cancelOrderType}booking.refund" arguments="${totalToRefund}" argumentSeparator="#" />
								</p>

								<p>
                                    <spring:theme code="text.page.managemybooking.cancel.cancelFee" /> ${cancelFee}
                                </p>
								<form name="cancelOrderForm" class="y_cancelOrderForm" action="" method="POST">
									<input type="hidden" name="orderCode" value="" /> <input type="hidden" name="journeyRefNum" value="" /> <input type="hidden" name="odRefNum" value="" /> <input type="hidden" name="CSRFToken" value="${CSRFToken.token}" />
								</form>
							</div>
							<div class="modal-footer">
								<div class="row">
									<div class="col-xs-12 col-sm-6">
										<a id="y_cancelBookingUrl" class="btn btn-primary btn-block" href=""> <spring:theme code="text.page.managemybooking.cancel.booking.confirmation" text="Confirm Cancellation" />
										</a>
									</div>
									<div class="col-xs-12 col-sm-6">
										<button class="btn btn-secondary btn-block" data-dismiss="modal">
											<spring:theme code="text.page.managemybooking.cancel.booking.close" text="No Thanks" />
										</button>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</json:property>
		</c:when>
		<c:otherwise>
			<json:property name="errorMessage">
				<spring:theme code="text.page.managemybooking.cancel.${cancelOrderType}order.not.possible" />
			</json:property>
		</c:otherwise>
	</c:choose>
</json:object>
