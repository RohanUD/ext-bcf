<%@ page trimDirectiveWhitespaces="true"%>
<%@ taglib prefix="template" tagdir="/WEB-INF/tags/responsive/common"%>
<%@ taglib prefix="cms" uri="http://hybris.com/tld/cmstags"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags"%>
<%@ taglib prefix="format" tagdir="/WEB-INF/tags/addons/bcfstorefrontaddon/responsive/format"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="common" tagdir="/WEB-INF/tags/responsive/common"%>
<%@ taglib prefix="order" tagdir="/WEB-INF/tags/addons/bcfstorefrontaddon/responsive/order"%>
<spring:htmlEscape defaultHtmlEscape="false" />

<p> Order ID: <strong>${orderCode}</strong></p>

<div>
    <c:url value="/pending-orders-onrequest/updateorder" var="actionUrlOrder" />
    <form:form commandName="agentPendingOrderApprovalStatusForm" id="y_agentPendingOrderApprovalStatusForm" action="${actionUrlOrder}" method="post">
        <form:input type="hidden" path="orderNumber" value="${orderCode}" />
        <form:input type="hidden" path="orderAction" id="buttonClickedOrder" value="" />
        <table class="table">
            <thead>
            <tr>
                <th><spring:theme text="Lock" /></th>
                <th><spring:theme text="Unlock" /></th>
                <th><spring:theme text="Comments" /></th>
                <th><spring:theme text="Approve" /></th>
                <th><spring:theme text="Reject" /></th>
            </tr>
            </thead>
            <tbody>
            <tr>
            <td><form:button type="submit" class="btn btn-primary lockOrder" id="y_lockOrder" ><spring:theme code="text.page.pendingorders.lockorder" text="Lock Order" /></form:button></td>
            <td><form:button type="submit" class="btn btn-primary unLockOrder" id="y_unLockOrder" ><spring:theme code="text.page.pendingorders.unlockorder" text="Unlock Order" /></form:button></td>
            <td><form:input type="text" path="orderComments" id="orderComments" /></td>
            <td><form:button type="submit" class="btn btn-primary approve" id="y_approveOrder" > <spring:theme code="text.page.pendingorders.approveorder" text="Approve Order" /></form:button></td>
            <td><form:button type="submit" class="btn btn-primary reject" id="y_rejectOrder" ><spring:theme code="text.page.pendingorders.rejectorder" text="Reject Order" /></form:button></td>
            </tr>
            </tbody>
        </table>
    </form:form>
</div>




<c:url value="/pending-orders-onrequest/updateentry" var="actionUrl" />
<form:form commandName="agentPendingOrderDecisionForm" id="y_agentPendingOrderDecisionForm" action="${actionUrl}" method="post">
    <form:input type="hidden" path="orderCode" value="${orderCode}" />
    <form:input type="hidden" path="buttonClicked" id="buttonClicked" value="" />
    <form:input type="hidden" path="comments" id="comments" value="" />
    <form:input type="hidden" path="redirectEntry" id="redirectEntry" value="" />
    <form:input type="hidden" path="entryNumber" id="entryNumber" />
    <div class="activityContent">


        <table class="table">
            <thead>
                <tr>
                    <th><spring:theme code="text.page.pendingorders.header.order.entry.activity.name" text="Activity Name" /></th>
                    <th><spring:theme code="text.page.pendingorders.header.order.entry.activity.date" text="Activity Date" /></th>
                    <th><spring:theme code="text.page.pendingorders.header.order.entry.activity.quantity" text="Activity Quantity" /></th>
                    <th><spring:theme code="text.page.pendingorders.header.order.entry.activity.guest.type" text="Activity Guest Type" /></th>
                    <th><spring:theme code="text.page.pendingorders.header.order.entry.activity.subtotal" text="Activity SubTotal" /></th>
                    <th><spring:theme code="text.page.pendingorders.header.order.entry.booking.reference" text="Booking Reference" /></th>
                    <th><spring:theme text="Comments" /></th>
                    <th><spring:theme text="Approve" /></th>
                    <th><spring:theme text="Reject" /></th>
                </tr>
            </thead>
            <tbody>
            <c:forEach items="${order.activityEntries}" var="entry" varStatus="activityEntryStatus">
                <tr>
                    <input type="hidden" class="activityEntry" value="${entry.orderEntryNumber}" />
                    <td><c:out value="${entry.activityName}" /></td>
                    <td><c:out value="${entry.activityDate}" /></td>
                    <td><c:out value="${entry.quantity}" /></td>
                    <td><c:out value="${entry.activityGuestType}" /></td>
                    <td><c:out value="${entry.totalPrice.formattedValue}" /></td>
                    <td><c:out value="${entry.bookingReference}" /></td>
                    <td>
                        <textarea class="comment" id="activityComments-${activityEntryStatus.index}" cols="15" rows="3"> </textarea>
                    </td>
                    <td>
                    <c:choose>
                        <c:when test = "${entry.approvalStatus eq 'APPROVED'}">
                                <spring:theme text="Approved" /> <br>
                                <c:set var="activityCommentId"> 'activity-comment'+${entry.orderEntryNumber}</c:set>
                                <button type="button" id="commentsButton" onclick="getElementById(${activityCommentId}).style.display='block';"><spring:message text="View Comments"/></button>
                        </c:when>
                        <c:otherwise>
                                <a class="btn btn-primary approveActivityEntry approveEntryBtn" id="y_approveActivityEntry${activityEntryStatus.index}" href='#'>
                                    <spring:theme code="text.page.pendingorders.approveorder" text="Approve" />
                                </a>
                        </c:otherwise>
                    </c:choose>
                    </td>
                    <td>
                    <c:choose>
                        <c:when test = "${entry.approvalStatus eq 'REJECTED'}">
                                <spring:theme text="Rejected" /> <br>
                                <button type="button" id="commentsButton" onclick="getElementById('activity-comment').style.display='block';"><spring:message text="View Comments"/></button>
                        </c:when>
                        <c:otherwise>
                                <a class="btn btn-primary rejectActivityEntry rejectEntryBtn" id="y_rejectActivityEntry${activityEntryStatus.index}" href="#">
                                    <spring:theme code="text.page.pendingorders.rejectorder" text="Reject" />
                                </a>
                        </c:otherwise>
                    </c:choose>
                    </td>
                </tr>
            </c:forEach>
            </tbody>
        </table>
    </div>
        <br>
        <br>
    <div class="accommodationContent">
        <table class="table">
            <thead>
            <th><spring:theme code="text.page.pendingorders.header.order.entry.accommodation.name" text="Accommodation Name" /></th>
            <th><spring:theme code="text.page.pendingorders.header.order.entry.accommodation.room.name" text="Room Name" /></th>
            <th><spring:theme code="text.page.pendingorders.header.order.entry.accommodation.checkin.date" text="CheckIn Date" /></th>
            <th><spring:theme code="text.page.pendingorders.header.order.entry.accommodation.checkout.date" text="CheckOut Date" /></th>
            <th><spring:theme code="text.page.pendingorders.header.order.entry.accommodation.guest.name" text="Guest Name" /></th>
            <th><spring:theme code="text.page.pendingorders.header.order.entry.booking.reference" text="Booking Reference" /></th>
            <th><spring:theme code="text.page.pendingorders.header.order.entry.accommodation.quantity" text="Number of Nights" /></th>
            <th><spring:theme code="text.page.pendingorders.header.order.entry.accommodation.subtotal" text="Accommodation Subtotal" /></th>
            <th><spring:theme text="Comments" /></th>
            <th><spring:theme text="Approve" /></th>
            <th><spring:theme text="Reject" /></th>
            </thead>
            <tbody>
            <c:forEach items="${order.accommodationEntries}" var="entry" varStatus="theCount">
                <tr>
                    <input type="hidden" class="accommodationEntry" value="${entry.orderEntryNumber}" />
                    <td><c:out value="${entry.accommodationOfferingName}" /></td>
                    <td><c:out value="${entry.accommodationName}" />
                        <c:if test="${entry.product.productType eq 'ExtraGuestOccupancyProduct'}">
                             &nbsp;<spring:theme code="text.page.pendingorders.extra.guest" />
                        </c:if>
                    </td>
                    <td><c:out value="${entry.accommodationCheckInDate}" /></td>
                    <td><c:out value="${entry.accommodationCheckOutDate}" /></td>
                    <td><c:out value="${entry.accommodationGuestName}" /></td>
                    <td><c:out value="${entry.bookingReference}" /></td>
                    <td><c:out value="${entry.quantity}" /></td>
                    <td><c:out value="${entry.totalPrice.formattedValue}" /></td>
                    <td>
                        <textarea class="comment" id="accommodationComments-${theCount.index}" cols="15" rows="3"> </textarea>
                    </td>
                    <td>
                        <c:choose>
                            <c:when test = "${entry.approvalStatus eq 'APPROVED'}">
                                <spring:theme text="Approved" /> <br>
                                <c:set var="accommodationCommentId"> 'accommodation-comment'+ ${entry.orderEntryNumber}</c:set>
                               <button type="button" id="commentsButton" onclick="getElementById(${accommodationCommentId}).style.display='block';"><spring:message text="View Comments"/></button>
                                </c:when>
                            <c:otherwise>
                                <a href="#" class="btn btn-primary approveAccommodationEntry approveEntryBtn" id="y_approveAccommodationEntry${theCount.index}">
                                    <spring:theme code="text.page.pendingorders.approveorder" text="Approve" />
                                </a>
                            </c:otherwise>
                        </c:choose>
                    </td>
                    <td>
                        <c:choose>
                            <c:when test = "${entry.approvalStatus eq 'REJECTED'}">
                                <spring:theme text="Rejected" /> <br>
                                <button type="button" id="commentsButton" onclick="getElementById('accommodation-comment').style.display='block';"><spring:message text="View Comments"/></button>
                            </c:when>
                            <c:otherwise>
                                <a href="#" class="btn btn-primary rejectAccommodationEntry rejectEntryBtn" id="y_rejectAccommodationEntry${theCount.index}" >
                                    <spring:theme code="text.page.pendingorders.rejectorder" text="Reject" />
                                </a>
                            </c:otherwise>
                        </c:choose>
                    </td>
                </tr>
            </c:forEach>
            </tbody>
        </table>
    </div>
</form:form>

<c:forEach items="${order.activityEntries}" var="activityEntry" >
    <table class="table" id="activity-comment${activityEntry.orderEntryNumber}" style="display:none;">
        <thead>
            <tr>
                <th><spring:theme text="Comment" /></th>
                <th><spring:theme text="Last Modified" /></th>
            </tr>
        </thead>
        <tbody>
            <c:forEach items="${activityEntry.comments}" var="comment">
                <tr>
                    <td><c:out value="${comment.text}" /></td>
                    <td><c:out value="${comment.modifiedtime}" /></td>
                </tr>
            </c:forEach>
        </tbody>
    </table>
</c:forEach>

<c:forEach items="${order.accommodationEntries}" var="entry" >
    <table class="table" id="accommodation-comment${entry.orderEntryNumber}" style="display:none;">
        <thead>
            <tr>
                <th><spring:theme text="Comment" /></th>
                <th><spring:theme text="Last Modified" /></th>
            </tr>
        </thead>
        <tbody>
            <c:forEach items="${entry.comments}" var="comment">
                <tr>
                    <td><c:out value="${comment.text}" /></td>
                    <td><c:out value="${comment.modifiedtime}" /></td>
                </tr>
            </c:forEach>
        </tbody>
    </table>
</c:forEach>

<order:onHoldOrderApprovalConfirmationModal />
<order:onHoldOrderRejectionConfirmationModal />
<order:onHoldOrderEntryApprovalConfirmationModal />
<order:onHoldOrderEntryRejectionConfirmationModal />
