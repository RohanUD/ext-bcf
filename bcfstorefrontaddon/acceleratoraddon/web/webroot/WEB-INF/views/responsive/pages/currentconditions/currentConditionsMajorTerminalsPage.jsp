<%@ page trimDirectiveWhitespaces="true" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="cms" uri="http://hybris.com/tld/cmstags" %>
<%@ taglib prefix="ycommerce" uri="http://hybris.com/tld/ycommercetags" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="progress" tagdir="/WEB-INF/tags/addons/bcfvoyageaddon/responsive/progress" %>
<%@ taglib prefix="currentconditions" tagdir="/WEB-INF/tags/addons/bcfvoyageaddon/responsive/currentconditions" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ page language="java" contentType="text/html;charset=UTF-8" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib prefix="template" tagdir="/WEB-INF/tags/responsive/common" %>
<%@ taglib prefix="common" tagdir="/WEB-INF/tags/responsive/common" %>
<%@ taglib prefix="ccSailingDetails" tagdir="/WEB-INF/tags/addons/bcfstorefrontaddon/responsive/currentconditions" %>

<jsp:useBean id="dateValue" class="java.util.Date"/>
<spring:htmlEscape defaultHtmlEscape="false"/>

<c:url var="serviceNoticesUrl" value="/current-conditions/service-notices"/>
<c:set var="counter" value="0" scope="page"/>
<c:set var="val" value="0" scope="page"/>
<c:set var="vesselVal" scope="page"/>
<c:set var="terminalNames"
       value="${currentConditionsData.terminals}" scope="page"/>
<c:set var="displayTomorrowsScheduleLabel" scope="page"/>
<c:set var="noSailings" value="false" scope="page"/>

<%-- Page Title and description --%>
<p class="text-grey margin-top-10"><spring:theme code="text.cc.atAglance.header.label"
                                                 text="At-a-glance" /></p>
<div class="container">
    <h2 class="text-dark-blue">
        <b>
            <spring:theme code="text.cc.atAglance.majorTerminals.page.label" text="Major terminals"/>
        </b>
    </h2>

    <div class="row">
        <div class="col-lg-8 col-md-8 col-sm-12 col-xs-12">
            <p>
                <spring:theme code="text.cc.atAglance.majorTerminals.page.desc.label"/>
            </p>
        </div>
    </div>
</div>

<c:if test="${empty ccServiceNotAvailable}">
    <%-- Refresh --%>
    <div class="container">
        <p class="italic-style margin-top-20"><em>
            <spring:theme code="text.cc.lastUpdated.label" text="Last updated"/> :
            <c:forEach items="${currentConditionsData.currentConditionsResponseData}" var="currentConditionsByTreminal" begin="0"
                       end="0">
                <c:if test="${not empty currentConditionsByTreminal.refreshedAt}">
                    <fmt:parseDate value="${currentConditionsByTreminal.refreshedAt}" var="now" pattern="yyyy-MM-dd HH:mm:ss"/>
                    <fmt:parseDate value="${currentConditionsByTreminal.refreshedAt}" var="parsedEmpDate"
                                   pattern="yyyy-MM-dd HH:mm:ss"/>
                    <fmt:formatDate value="${parsedEmpDate}" pattern="h:mm a, EEEE, MMM dd, yyyy"/>.
                    <a href="" onclick="return false" id="refreshDepartures">
                        <spring:theme code="text.cc.atAglance.click.label" text="Refresh"/>
                        <spring:theme code="text.cc.atAglance.to.refresh.label" text="to refresh"/>
                    </a>
                </c:if>
            </c:forEach>
        </em></p>
    </div>

    <%-- Select Box --%>
    <section class="bg-light-gray padding-top-20  margin-bottom-30 margin-top-20">
        <div class="container">
            <p><b><spring:theme code="text.cc.atAglance.find.a.terminal.label" text="Find a terminal"/></b></p>
            <div class="row" id="findTerminalDiv">
                <div class="col-lg-8 col-md-8 col-sm-6 col-xs-12 margin-bottom-30">
                    <select class="form-control selectpicker" id="selectDepartingTerminal" name="terminalCode">
                        <option value="default" selected>
                            <spring:theme code="label.our.terminals.make.selection" text="Select a terminal"/>
                        </option>
                        <c:forEach items="${sourceTerminals.terminals}" var="terminal">
                            <option value="${terminal.key}"
                                    <c:if test='${param.terminalCode eq terminal.key}'>selected</c:if>>${sourceTerminals.terminals[terminal.key].name}
                            </option>
                        </c:forEach>
                    </select>
                </div>
            </div>
        </div>
    </section>

    <%-- Data Display --%>
    <div class="container">

        <c:forEach items="${currentConditionsData.currentConditionsResponseData}" var="currentConditionsByTreminal">
            <div class="row">
                <div class="col-md-12 col-sm-12 col-xs-12 p-5">
                    <c:forEach items="${currentConditionsByTreminal.arrivalDepartures}" var="arrivalDeparturesHeading"
                               varStatus="arrivalDeparturesIndexHeading" begin="0" end="0">
                        <c:set var="arrivalDeparturesTempHeading" value="${arrivalDeparturesHeading.value[0]}"></c:set>
                        <h4>
                            <span>
                                <b>${terminalNames[arrivalDeparturesTempHeading.dept].name}&nbsp;<spring:theme code="text.cc.terminal.label" text="terminal"/></b><br/>
                            </span>
                        </h4>
                    </c:forEach>
                </div>
            </div>

            <%-- Route Data --%>
            <c:forEach items="${currentConditionsByTreminal.arrivalDepartures}" var="arrivalDeparture"
                       varStatus="arrivalDeparturesIndex">
                <div class="border-black margin-bottom-30">
                    <c:set var="vesselVal" value="0"/>
                    <c:set var="arrivalDepartureForHeading" value="${arrivalDeparture.value[0]}"/>
                    <c:set var="arrivalDepartureRoutes" value="${arrivalDeparture.value}"/>
                    <c:forEach items="${arrivalDepartureRoutes}" var="route1" varStatus="routeIndex">
                        <fmt:parseDate value="${route1.scheduledDeparture}" var="depTime" pattern="yyyy-MM-dd HH:mm:ss"/>
                        <fmt:formatDate value="${depTime}" var="date" pattern="yyyy-MM-dd"/>
                        <c:if test="${route1.vesselFullPercent ne 100 and date lt tomorrowDate and depTime ge now}">
                            <c:set var="vesselVal" value="${vesselVal+1}"/>
                            <fmt:formatDate var="actualDepDate" value="${depTime}" pattern="yyyy-MM-dd"/>
                        </c:if>
                    </c:forEach>
                    <c:if test="${not empty vesselVal and vesselVal eq 0 and empty actualDepDate}">
                       <div class="bg-light-gray">
                            <div class="fully-booked">
                           <h5><strong><span class="bcf bcf-icon-notice-outline bcf-2x"></span><br/>
                           <spring:theme code="text.cc.today.sailings.full.header"/></strong></h5>
                           <p class="fnt-14 padding-bottom-20"><spring:theme code="text.cc.today.sailings.full.label"/></p>
                           </div>
                        </div>
                    </c:if>
                        <%-- Heading --%>
                    <h4 class="text-center margin-top-30 text-dark-blue">
                        <b>
                                ${terminalNames[arrivalDepartureForHeading.dept].name} -
                            <c:choose>
                                <c:when test="${fn:contains(southernGulfIslandTerminals, arrivalDepartureForHeading.dest)}">
                                    <spring:theme code="text.cc.atAglance.SOUTHERN_GULF.label" text="Southern Gulf Islands"/>
                                </c:when>
                                <c:otherwise>
                                    ${terminalNames[arrivalDepartureForHeading.dest].name}</br>
                                </c:otherwise>
                            </c:choose>
                        </b>
                    </h4>
                        <%-- Heading Ends --%>

                        <%-- Sailing Time --%>
                    <p class="text-center mb-3"><spring:theme code="text.cc.atAglance.departures.sailingTime.label"/>:
                        <c:choose>
                            <c:when test="${fn:contains(southernGulfIslandTerminals, arrivalDepartureForHeading.dest)}">
                                <spring:theme code="text.varies.label" text="Varies" />
                            </c:when>
                            <c:otherwise>
                                <fmt:parseDate value="${arrivalDepartureForHeading.scheduledDeparture}" var="depTime"
                                               pattern="yyyy-MM-dd HH:mm:ss"/>
                                <fmt:parseDate value="${arrivalDepartureForHeading.scheduledArrival}" var="arrTime"
                                               pattern="yyyy-MM-dd HH:mm:ss"/>
                                <c:set var="sailingHour" value="${fn:substringBefore((arrTime.time-depTime.time)/(1000*60*60), '.')}"/>
                                <c:set var="sailingMin" value="${fn:substringAfter((arrTime.time-depTime.time)/(1000*60*60), '.')*60}"/>
                                <c:if test="${sailingHour != 0}">
                                    ${sailingHour}h
                                </c:if>
                                <c:if test="${sailingMin != 0}">
                                    <c:out value="${fn:substring(sailingMin, 0, 2)}"/>min
                                </c:if>
                            </c:otherwise>
                        </c:choose>
                    </p>
                        <%-- Sailing Time Ends --%>

                    <table class="table majorTerminals-table">
                        <tr class="bg-light-gray">
                            <th>
                                <spring:theme code="text.cc.atAglance.nextSailing.label" text="NEXT SAILINGS"/>
                            </th>
                            <th>
                                <spring:theme code="text.cc.atAglance.laterSailing.label" text="LATER SAILINGS"/>
                            </th>
                        </tr>

                        <c:set var="nextSailingsCount" value="0"/>
                        <c:set var="displayThreeCount" value="0"/>
                        <c:choose>
                            <c:when test="${not empty arrivalDepartureRoutes and not empty arrivalDepartureRoutes[0].scheduledDeparture}">
                                <c:forEach items="${arrivalDepartureRoutes}" var="route1" varStatus="routeIndex">
                                    <fmt:parseDate value="${route1.scheduledDeparture}" var="depTime" pattern="yyyy-MM-dd HH:mm:ss"/>
                                    <c:if test="${displayThreeCount < 3 or not empty arrivalDepartureRoutes[ 3 + routeIndex.index]}">
                                        <c:if test="${depTime gt now and nextSailingsCount lt laterSailingsCounts}">
                                            <tr class="text-blue">
                                                <td class="text-blue">
                                                    <c:if test="${displayThreeCount < 3}">
                                                        <c:if test="${depTime gt now}">
                                                            <c:set var="displayThreeCount" value="${displayThreeCount + 1}"/>
                                                            <p class="text-center"><div class="col-md-6 text-right col-xs-6"><fmt:formatDate value="${depTime}" pattern="h:mm a"/><br>
                                                            <fmt:formatDate var="todaysDate" pattern="yyyy-MM-dd" value="${now}"/>
                                                            <fmt:formatDate var="actualDepDate" value="${depTime}" pattern="yyyy-MM-dd"/>
                                                            <c:if test="${todaysDate ne actualDepDate}">
                                                                <span class="tomorrow-date"><spring:theme code="text.cc.tomorrow.date.label"/></span>
                                                            </c:if>
                                                        </div>
                                                            <div class="col-md-6 col-xs-6">
                                                                <c:choose>
                                                                    <c:when test="${route1.webDisplaySailingFlag eq 'Y' && route1.status eq 'S'}">
                                                                        <spring:theme code="text.cc.cancelled.status.label"/>
                                                                    </c:when>
                                                                    <c:otherwise>
                                                                        <c:choose>
                                                                            <c:when test="${route1.vesselFullPercent eq 100}">
                                                                                <spring:theme code="text.cc.atAglance.full.label" text="Full"/>
                                                                            </c:when>
                                                                            <c:otherwise>
                                                                                <spring:theme code="text.cc.atAglance.available.label"
                                                                                              text="available"
                                                                                              arguments="${100 - route1.vesselFullPercent}"/>
                                                                                <c:if test="${counter eq 0 and empty checkCounter}">
                                                                                    <c:set var="counter" value="${routeIndex.index}"/>
                                                                                    <c:set var="checkCounter" value="true"/>
                                                                                </c:if>
                                                                            </c:otherwise>
                                                                        </c:choose>
                                                                    </c:otherwise>
                                                                </c:choose>
                                                            </div>
                                                            </p>
                                                        </c:if>
                                                    </c:if>
                                                </td>
                                                <td class="text-center">
                                                    <c:set var="displayTomorrowsScheduleLabel" value="false"/>
                                                    <c:if test="${not empty arrivalDepartureRoutes[ 2 + routeIndex.index]}">
                                                        <c:set var="route" value="${arrivalDepartureRoutes[2 + routeIndex.count]}"/>
                                                        <fmt:parseDate value="${route.scheduledDeparture}" var="nextDepTime"
                                                                       pattern="yyyy-MM-dd HH:mm:ss"/>

                                                        <c:if test="${nextDepTime gt now and nextSailingsCount lt laterSailingsCounts}">
                                                            <fmt:formatDate var="t
                                                            odaysDate" pattern="yyyy-MM-dd" value="${now}"/>
                                                            <fmt:formatDate var="actualDepDate" value="${nextDepTime}" pattern="yyyy-MM-dd"/>
                                                            <c:choose>
                                                                <c:when test="${todaysDate eq actualDepDate}">
                                                                    <p><fmt:formatDate value="${nextDepTime}" pattern="h:mm a"/></p>
                                                                    <c:set var="nextSailingsCount" value="${nextSailingsCount + 1}"/>
                                                                </c:when>
                                                                <c:otherwise>
                                                                    <span class="text-grey"><fmt:formatDate value="${nextDepTime}"
                                                                                                         pattern="h:mm a"/></span></br>
                                                                    <span class="tomorrow-date"><spring:theme code="text.cc.tomorrow.date.label"/></span>
                                                                    <c:set var="displayTomorrowsScheduleLabel" value="true"/>
                                                                </c:otherwise>
                                                            </c:choose>
                                                        </c:if>
                                                    </c:if>
                                                </td>
                                            </tr>
                                        </c:if>
                                    </c:if>

                                </c:forEach>
                            </c:when>
                            <c:otherwise>
                                <tr class="text-black text-center">
                                    <td class="text-black" colspan="2">
                                        <div class="margin-top-10 margin-bottom-10"><strong><span class="bcf bcf-icon-notice-outline bcf-2x"></span>
                                                <spring:theme code="text.cc.no.sailings.available.label"/></strong>
                                                <c:set var="noSailings" value="true"/></sailing></div>
                                    </td>
                                </tr>
                            </c:otherwise>
                        </c:choose>
                        <tr>
                            <td colspan="2" class="text-center">
                                <c:if test="${displayTomorrowsScheduleLabel}">
                                    <p class="text-grey">*<spring:theme code="text.cc.terminal.tomorrows.schedule.label"/></p>
                                </c:if>
                                <b>
                                    <spring:theme code="text.cc.nextavailablesailing.label" text="Next available sailing"/>
                                </b>
                                <spring:theme code="text.cc.nextavailablesailing.info.label" var="nextavailablesailing"/>
                                <a href="javascript:;" class="popoverThis" data-container="body" data-toggle="popover"
                                   data-placement="bottom" data-content="${fn:escapeXml(nextavailablesailing)}">
                                    <span class="bcf bcf-icon-info-solid"></span>
                                </a>
                            </td>
                        </tr>

                            <%-- Recording Vehicle Data --%>
                            <%--Next Undersize Vehicle--%>
                        <c:forEach items="${arrivalDepartureRoutes}" var="dep" varStatus="usCounter">
                            <fmt:parseDate value="${dep.scheduledDeparture}" var="underSizeDepTime"
                                           pattern="yyyy-MM-dd HH:mm:ss"/>
                            <c:if test="${underSizeDepTime gt now}">
                                <c:if test="${counter ge 0}">
                                    <fmt:parseDate
                                            value="${arrivalDepartureRoutes[counter].scheduledDeparture}"
                                            var="usDepTime" pattern="yyyy-MM-dd HH:mm:ss"/>
                                    <c:set var="underSizeVehicleTime"><fmt:formatDate value="${usDepTime}" pattern="h:mm a"/></c:set>
                                    <c:set var="deptDate"><fmt:formatDate value="${usDepTime}" pattern="yyyy-MM-dd"/></c:set>
                                    <c:if test="${deptDate ge tomorrowDate}">
                                        <c:set var="val" value="1"/>
                                    </c:if>
                                </c:if>
                            </c:if>
                        </c:forEach>
                            <%--Next Oversize Vehicle--%>

                        <c:forEach items="${arrivalDepartureRoutes}" var="dep" varStatus="osCounter">
                            <fmt:parseDate value="${dep.scheduledDeparture}" var="overSizeDepTime" pattern="yyyy-MM-dd HH:mm:ss"/>
                            <c:if test="${overSizeDepTime gt now}">
                                <c:if test="${counter ge 0}">
                                    <fmt:parseDate
                                            value="${arrivalDepartureRoutes[counter].scheduledDeparture}"
                                            var="osDepTime" pattern="yyyy-MM-dd HH:mm:ss"/>
                                    <c:set var="overSizeVehicleTime"><fmt:formatDate value="${osDepTime}" pattern="h:mm a"/></c:set>
                                </c:if>
                            </c:if>
                        </c:forEach>
                        <c:set var="counter" value="0"/>
                            <%-- Recording Vehicle Data Ends --%>

                            <%-- display under sized vehicle info --%>
                        <tr>


                            <td class="col-md-6 text-right">
                                <span class="bcf bcf-icon-std-car bcf-2x"></span>
                                <spring:theme code="text.cc.standard.vehicles.label" text="Standard vehicles"/>
                            </td>
                            <td class="col-md-6  padding-major">
                                <c:choose>
                                    <c:when test="${not empty underSizeVehicleTime && val eq 0}">${underSizeVehicleTime}

                                        <c:set var="underSizeVehicleTime" value=""/>
                                    </c:when>
                                    <c:otherwise>
                                        <c:if test="${noSailings eq 'false'}">
                                            <spring:theme code="text.cc.tomorrow.next.sailings.label"/>
                                        </c:if>
                                    </c:otherwise>

                                </c:choose></td>
                        </tr>

                            <%-- display over sized vehicle info --%>
                        <tr class="major-truck-icon">
                            <td class="col-md-6 text-right">
                                <span class="flaticon-truck"></span>
                                <spring:theme code="text.cc.overSized.vehicles.label" text="Oversized vehicles"/></td>
                            <td class="col-md-6 padding-major">

                                <c:choose>
                                    <c:when test="${not empty overSizeVehicleTime && val eq 0}">
                                        ${overSizeVehicleTime}
                                        <c:set var="overSizeVehicleTime" value=""/>
                                    </c:when>
                                    <c:otherwise>
                                        <c:if test="${noSailings eq 'false'}">
                                            <spring:theme code="text.cc.tomorrow.next.sailings.label"/>
                                        </c:if>
                                    </c:otherwise>
                                </c:choose>
                            </td>
                        </tr>
                        <tr>
                            <c:set var="arrivalDepartureRoute" value="${arrivalDeparture.value[0]}"/>
                            <td class="col-md-6 text-right">

                                <c:set var="depRouteName" value="${terminalNames[arrivalDepartureRoute.dept].translatedName}"/>
                                <c:set var="arrRouteName" value="${terminalNames[arrivalDepartureRoute.dest].translatedName}"/>

                                <c:url var="sailingStatusUrl" value="/current-conditions/${depRouteName}-${arrRouteName}/${arrivalDepartureRoute.dept}-${arrivalDepartureRoute.dest}" />
                                <span class="icon-ferry"></span><a href="${sailingStatusUrl}"> <spring:theme
                                    code="text.cc.sailingStatus.label"/> </a>
                            </td>
                            <td class="col-md-6">
                                <span class="icon-notice"></span><a href="${serviceNoticesUrl}" target="_blank"> <spring:theme
                                    code="text.cc.routeNotices.label"/> </a>
                            </td>
                        </tr>
                    </table>
                </div>

            </c:forEach>
        </c:forEach>
    </div>
</c:if>
