<%@ page trimDirectiveWhitespaces="true" contentType="application/json"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="json" uri="http://www.atg.com/taglibs/json"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="quoteslist" tagdir="/WEB-INF/tags/addons/bcfstorefrontaddon/responsive/quoteslisting"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<json:object escapeXml="false">
   <json:property name="htmlContent">
        <quoteslist:quotes quotes="${quotes}"/>
   </json:property>
   <json:property name="hasMoreResults" value="${hasMoreResults}" />
   	<json:property name="totalNumberOfResults" value="${totalNumberOfResults}" />
   	<json:property name="totalShownResults">
   		<h3 class="h4">
   			<spring:theme code="text.ferry.listing.shown.results" arguments="${startingNumberOfResults}, ${totalShownResults}, ${totalNumberOfResults}" />
   		</h3>
   	</json:property>
</json:object>
