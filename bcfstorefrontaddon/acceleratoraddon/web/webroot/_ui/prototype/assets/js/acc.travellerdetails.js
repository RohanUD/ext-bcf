ACC.travellerdetails = {

	_autoloadTracc : [ 
		"bindMembershipButton"
	],

	bindMembershipButton : function() {

		$('.y_travellerDetailsMembershipYesBtn').each( function(index) {
			$(this).on('click', function() {
				$(this).closest('.row').find('.y_membershipNumber').show("fast");
			})
		});

		$('.y_travellerDetailsMembershipNoBtn').each( function(index) {
			$(this).on('click', function() {
				$(this).closest('.row').find('.y_membershipNumber').hide("fast");
			})
		});

	}
}
