/**
 * Represents the model for Travel Accelerator.
 * @namespace
 */
 ACC.appmodel = {
	/**
     * Refer to this by {@link ACC.appmodel.trips}.
     * @namespace
     */
	trips : {
		/**
	     * Refer to this by {@link ACC.appmodel.trips.outbound}.
	     * @namespace - outbound trip
	     */
		outbound: {
			/** @type {Date} */
			dateTime: null,

			/** @type {int} */
			pricedItinerary: -1,

			/** @type {string} - The travel class e.g. "ECONOMY", "BUSINESS" */
			bundleType: null
		},
		/**
	     * Refer to this by {@link ACC.appmodel.trips.inbound}.
	     * @namespace - inbound (return) trip.
	     */
		inbound: {
			/** @type {Date} */
			dateTime: null,

			/** @type {int} */
			pricedItinerary: -1,

			/** @type {string} - The travel class e.g. "ECONOMY", "BUSINESS". */
			bundleType: null
		}
	},
	/**
     * Refer to this by {@link ACC.appmodel.itineraryJsonObj}.
     * @namespace - itinerary object used on ancillary page (primarily for the seat map).
     */
	itineraryJsonObj : [],

	/**
	 * Get the itineraryJsonObj.
	 * @returns {json} - The trip Inbound and Outbound ID and Bundle type.
	 */
	getItineraryJson : function() {
		return {
			outboundPricedItineraryId : this.trips.outbound.pricedItinerary,
			outboundBundleType : this.trips.outbound.bundleType,
			inboundPricedItineraryId : this.trips.inbound.pricedItinerary,
			inboundBundleType : this.trips.inbound.bundleType
		};
	}

};