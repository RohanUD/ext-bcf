/**
 * Module for common form validation methods.
 * @namespace
 */
ACC.formvalidation = {

    _autoloadTracc: [
        "bindformValidationMessages",
        "initDateValidationMethods",
        "mapActivateProfile",
        "mapCountry",
        "getRegions"
    ],

    errorClass: "fe-error",
    errorTag: "span",
    bindformValidationMessages: function () {
        ACC.formValidationMessages = new validationMessages("ferry-formValidation");
        ACC.formValidationMessages.getMessages("error");
        ACC.formValidationMessages.getMessages("label");
    },
    mapActivateProfile: function () {
        var accountTypeVal = $('#registerAccountType').val();
        toggleAccountTypes(accountTypeVal);
        $('#registerAccountType').on('change', function () {
            var accountTypeVal = $(this).val();
            toggleAccountTypes(accountTypeVal);
        })
    },
    mapCountry: function () {
        var countryVal = $('#registerCountry').val();
        $('#registerRegionbox').hide();
        toggleCountry(countryVal);
        $('#registerCountry').on('change', function () {
            var countryVal = $(this).val();
            toggleCountry(countryVal);
        })
    },
    getRegions : function() {
        $('#registerCountry').on('change',function(e){
            var html = "";
            $.when(ACC.services.getRegionsAjax($(this).val())).then(function(response) {
                if(response) {
                    html = "<option value=-1 >" + $('.billing-address-regions option:eq(0)').text() + "</option>"
                    for(var key in response) {
                        html += "<option value=" + response[key].isocode  + ">" +response[key].name + "</option>"
                    }
                    $('.billing-address-regions').html( html );
                }
            });
        });
    },

    /**
     * create new methods for jquery validation
     */
    initDateValidationMethods: function () {
        // date greater than or equal to
        $.validator.addMethod("dateGreaterEqualTo",
            function (value, element, params) {
                if (!/Invalid|NaN/.test(ACC.travelcommon.convertToUSDate(value))) {
                    return ACC.travelcommon.convertToUSDate(value) >= ACC.travelcommon.convertToUSDate(params);
                }

                return isNaN(value) && isNaN(params) ||
                    (Number(value) >= Number(params));
            }, ACC.formValidationMessages.message('error.formvalidation.dateGreaterEqualTo'));

        // date less than or equal to
        $.validator.addMethod("dateLessEqualTo",
            function (value, element, params) {
                if (!/Invalid|NaN/.test(ACC.travelcommon.convertToUSDate(value))) {
                    return ACC.travelcommon.convertToUSDate(value) <= ACC.travelcommon.convertToUSDate(params);
                }

                return isNaN(value) && isNaN(params) ||
                    (Number(value) <= Number(params));
            }, ACC.formValidationMessages.message('error.formvalidation.dateLessEqualTo'));

        // date in UK format
        $.validator.addMethod("dateUK",
            function (value, element) {
                var check = false;
                var re = /^\d{1,2}\/\d{1,2}\/\d{4}$/;
                if (re.test(value)) {
                    var adata = value.split('/');
                    var dd = parseInt(adata[0], 10);
                    var mm = parseInt(adata[1], 10);
                    var yyyy = parseInt(adata[2], 10);
                    var xdata = new Date(yyyy, mm - 1, dd);
                    if ((xdata.getFullYear() == yyyy) && (xdata.getMonth() == mm - 1) && (xdata.getDate() == dd))
                        check = true;
                    else
                        check = false;
                } else
                    check = false;
                return this.optional(element) || check;
            }, ACC.formValidationMessages.message('error.formvalidation.dateUK'));

        $.validator.addMethod("dateUKFareFinder",
            function (value, element) {
                var check = false;
                var re = /^[a-zA-Z0-9 ]+$/;
                var date = new Date(value);
                var dd = date.getDate();
                var mm = date.getMonth() + 1;
                var yyyy = date.getFullYear();
                if (re.test(value)) {
                    var xdata = new Date(yyyy, mm - 1, dd);
                    if ((xdata.getFullYear() == yyyy) && (xdata.getMonth() == mm - 1) && (xdata.getDate() == dd))
                        check = true;
                    else
                        check = false;
                } else
                    check = false;
                return this.optional(element) || check;
            }, ACC.formValidationMessages.message('error.formvalidation.dateUK'));

        //Name validation,only allows A-Z, a-Z, space, hyphen or apostrophe characters
        $.validator.addMethod("nameValidation",
            function (value, element) {
                var re = new RegExp(/^([a-z A-Z-\']{1,35})$/);
                return this.optional(element) || re.test($.trim(value));
            }, ACC.formValidationMessages.message('error.formvalidation.nameValid'));

        $.validator.addMethod("guestNameValidation",
            function (value, element) {
                var re = new RegExp(/^[a-zA-Z]([a-z A-Z-\']{0,34})$/);
                return this.optional(element) || re.test(value);
            }, ACC.formValidationMessages.message('error.formvalidation.nameValid'));

        $.validator.addMethod("adultGuestValue",
            function (value, element) {
                if (Number(value) != 0) {
                    return true;
                }
            }, ACC.formValidationMessages.message('error.accommodationfinder.guest.adult'));

        $.validator.addMethod("validateCountryCode",
            function (value, element) {
                if (Number(value) != 0 || value != null) {
                    return true;
                }
            }, ACC.formValidationMessages.message('error.guestdetails.country.empty'));

        $.validator.addMethod("validatePhoneNumberPattern",
            function (value, element) {
                var atLeastOneNumRegex = /[0-9]+/;
                var reAtLeastOneNumber = new RegExp(atLeastOneNumRegex);
                var splCharRegex = /^\+?[0-9\(\).\s-]{1,50}$/;
                var reSplChars = new RegExp(splCharRegex);
                if (reAtLeastOneNumber.test($.trim(value)) && reSplChars.test($.trim(value))) {
                    return true;
                }
            }, ACC.formValidationMessages.message('error.lead.guest.phone.invalid'));

        $.validator.addMethod("validateEmailPattern",
            function (value, element) {
                var emailRegex = /\b[A-Za-z0-9._%+-]+@[A-Za-z0-9.-]+\.[A-Za-z]{2,4}\b/;
                var reEmail = new RegExp(emailRegex);
                if (value == '' || reEmail.test($.trim(value))) {
                    return true;
                }
            }, ACC.formValidationMessages.message('error.lead.guest.email.invalid'));

        //AccommodationFinderForm
        $.validator.addMethod("destinationLocationIsValid",
            function (value, element, params) {
                var inputId = element.getAttribute('id');
                var locationDestinationName = $(element).val();
                var locationDestination = $("." + inputId + "Code").val();
                if (locationDestinationName && locationDestination) {
                    return true;
                }
                var latitude = $("." + inputId + "Latitude").val();
                var longitude = $("." + inputId + "Longitude").val();
                var radius = $("." + inputId + "Radius").val();
                if (locationDestinationName && latitude && longitude && radius) {
                    return true;
                }
                return false;

            }, ACC.formValidationMessages.message('error.accommodationfinder.destination.location.autosuggestion'));

        $.validator.addMethod("locationIsValid",
            function (value, element, params) {
                if (value && !$(params).val()) {
                    return false;
                }
                return true;

            }, ACC.formValidationMessages.message('error.farefinder.from.locationExists'));
    },

    /**
     * validate the form using server side validation
     * and show inline error messages.
     * @param {string} formElement - selector to get the form element. e.g. "#firstName" or ".departureDate".
     * @param {string} serviceName - The name of the service that performs the backend validation. e.g. "ACC.services.refreshFareSelectionItinerary".
     * @returns {json} jsonData - The response from the service.
     */
    serverFormValidation: function (formElement, serviceName) {
        var $form = $(formElement);
        var jsonData;

        $.when(window["ACC"]["services"][serviceName]($form.serialize())).then(
            // success
            function (data, textStatus, jqXHR) {
                jsonData = JSON.parse(data);

                // clear validation messages
                $form.find(ACC.formvalidation.errorTag + "." + ACC.formvalidation.errorClass).remove();
                $form.find("." + ACC.formvalidation.errorClass).removeClass(ACC.formvalidation.errorClass);
                $("[id$=QuantityListError]").html('');  // for group errors

                $("div[class^='y_vehicleInfoError']").empty();
                $("div[class^='y_vehicleInfo[0].lengthError']").empty();
                $("div[class^='y_vehicleInfo[0].heightError']").empty();
                $("div[class^='y_vehicleInfo[0].widthError']").empty();
                $("div[class^='y_vehicleInfo[1].lengthError']").empty();
                $("div[class^='y_vehicleInfo[1].heightError']").empty();
                $("div[class^='y_vehicleInfo[1].widthError']").empty();
                $("div[class^='y_vehicleInfo[0].vehicleType.categoryError']").empty();
                $("div[class^='y_vehicleInfo[1].vehicleType.categoryError']").empty();
                $("div[class^='y_vehicleInfo[0].vehicleType.codeError']").empty();
                $("div[class^='y_vehicleInfo[1].vehicleType.codeError']").empty();
                $("div[class^='y_durationError']").empty();
                $("div[class^='y_departureDateError']").empty();
                // process JSON response and display validation messages
                if (jsonData.hasErrorFlag) {
                    var $formField, $nextElement;
                    // add error messages for each field
                    $.each(jsonData.errors, function (key, val) {
                        // If it's an error for a group of fields (denoted by 'QuantityList' in the name),
                        // display the error in the dedicated group error section
                        var value = val;
                        //remove previous errors from page and display fresh errors
                        $('div#hidden-form-field-error + span').remove();

                        $formField = $form.find("[name='" + key + "']");
                        $formField.removeClass("valid");
                        $formField.addClass(ACC.formvalidation.errorClass);
                        $formField.attr("aria-describedby", $formField.attr('id') + "-error");
                        $formField.attr("aria-invalid", "true");

                        var $newElem = document.createElement(ACC.formvalidation.errorTag);
                        $newElem.setAttribute("id", $formField.attr('id') + "-error");
                        $newElem.setAttribute("class", ACC.formvalidation.errorClass);
                        $newElem.innerHTML = value;
                        $('#hidden-form-field-error').after($newElem);
                    });
                }
            }
        );
        return jsonData;
    }
};

function toggleAccountTypes(accountTypeVal) {
    $('#PROMOTIONS').addClass('hidden');
    $('#SERVICE_NOTICE').addClass('hidden');
    $('#NEWS_RELEASE').addClass('hidden');
    if (accountTypeVal == 'SUBSCRIPTION_ONLY') {
        $('#registerAddressLine1').hide();
        $('#registerAddressLine2').hide();
        $('#registerCity').hide();
        $('#registerMobileNumber').hide();
        $('#registerCountryBox').hide();
        $('#registerRegionBox').hide();
        $('#registerZipcodeBox').hide();
        $('#PROMOTIONS').addClass('hidden');
        $('#SERVICE_NOTICE').removeClass('hidden');
        $('#NEWS_RELEASE').removeClass('hidden');
    }
    if(accountTypeVal=='FULL_ACCOUNT')
    {
        $('#registerMobileNumber').show();
        $('#registerAddressLine1').show();
        $('#registerAddressLine2').show();
        $('#registerCity').show();
        $('#registerCountryBox').show();
        $('#registerRegionBox').show();
        $('#registerZipcodeBox').show();
        $('#PROMOTIONS').removeClass('hidden');
        $('#SERVICE_NOTICE').removeClass('hidden');
        $('#NEWS_RELEASE').removeClass('hidden');
    }
}

function toggleCountry(countryVal) {
    $('#registerRegionBox').hide();
    $('#registerZipCodeBox').hide();
    if (countryVal == 'CA' || countryVal == 'US') {
        $('#registerRegionBox').show();
        $('#registerZipCodeBox').show();
    } else {
        $('#registerRegionBox').hide();
        $('#registerZipCodeBox').hide();
    }
}
