/**
 * The module for backend services.
 * @namespace
 */
ACC.services = {

    showPricesAndSchedules : function(productCode, changeActivity, changeActivityCode){
        return $.ajax({
            url : ACC.config.contextPath + "/deal-activity-listing/getActivityPricesAndSchedule",
            type : "GET",
            contentType : "application/json; charset=utf-8",
            data: {
                productCode : productCode,
                changeActivity : changeActivity,
                changeActivityCode : changeActivityCode
            },
            async : false
        });
    },

    getRegionsAjax : function(countryIso){

        if(countryIso === undefined || countryIso === ""){
            return;
        }
        return $.ajax({
            url : ACC.config.contextPath + "/traveller-details/refresh-provinces",
            type : "GET",
            data: {
                countryIso : countryIso
            },
            async : false
        });
    },

    getAccommodationListForSelectedView : function(pagenumber, resultsViewType) {
        return $.ajax({
            url : ACC.config.contextPath + "/accommodation-search/display-view",
            type : "GET",
            contentType : "application/json; charset=utf-8",
            data: {
                pageNum : pagenumber,
                resultsViewType : resultsViewType
            },
            async : false
        });
    },

    getPackageListForSelectedView : function(pagenumber, resultsViewType, priceRange) {
        return $.ajax({
            url : ACC.config.contextPath + "/package-listing/display-view",
            type : "GET",
            contentType : "application/json; charset=utf-8",
            data: {
                pageNum : pagenumber,
                resultsViewType : resultsViewType,
                priceRange : priceRange
            },
            async : false
        });
    },

    // Fare Selection services
    refreshFareSelectionReservation : function() {
        return $.ajax({
            url : ACC.config.contextPath + "/fare-selection/refresh-reservation",
            type : "GET",
            contentType : "application/json; charset=utf-8",
            async : false,
            data : ACC.appmodel.getItineraryJson()
        });
    },

    getNearestTransportFacilityAjax : function(position,activity){
        return $.ajax({
            url: "trip-finder/get-nearest-airport?activity="+activity+"&latitude="+position.coords.latitude+"&longitude="+position.coords.longitude,
            type : "POST",
            async : false
        });
    },

    // fetch destination locations using the originCode
    getDestinationLocationAjax : function(url){
        return $.ajax({
            url: url,
            type : "POST",
            async : false
        });
    },

    reloadDestinationLocation: function(serializedForm){
        return $.ajax({
            type: "POST",
            url: "trip-finder/get-destination-locations.json",
            data: serializedForm
        });

    },

    addProductToCartAjax: function(){
        var form = $('.y_addToCartForm');
        return $.ajax({
            type: form.attr('method'),
            url: form.attr('action'),
            async : false,
            data: form.serialize()
        });

    },

    checkOfferGroupsRestriction: function(url){
        return $.ajax({
            url: url,
            type: "GET",
            async : false
        });
    },

    validateTransportOfferingStatusForm: function(serializedForm){
        return $.ajax({
            url: ACC.config.contextPath + "/view/TransportOfferingStatusSearchComponentController/validate-transport-offering-status-form",
            type: "POST",
            async: false,
            data: serializedForm
        });
    },

    refreshTransportOfferingStatusResults: function(serializedForm){
        return $.ajax({
            url: ACC.config.contextPath + "/transport-offering-status/refresh-transport-offering-status-results",
            type: "POST",
            async: false,
            data: serializedForm
        });
    },

    validateFareFinderFormAttributes: function(serializedForm){
        return $.ajax({
            url: ACC.config.contextPath + "/view/FareFinderComponentController/validate-fare-finder-form",
            type: "POST",
            async: false,
            data: serializedForm
        });
    },

    validateDealFinderFormAttributes: function(serializedForm){
        return $.ajax({
            url: ACC.config.contextPath + "/view/DealFinderComponentController/validate-deal-finder-form",
            type: "POST",
            async: false,
            data: serializedForm
        });
    },

    validateAmendJourneyAttributes: function(serializedForm){
        return $.ajax({
            url: ACC.config.contextPath + "/view/JourneyAmendComponentController/validate-fare-finder-form",
            type: "POST",
            async: false,
            data: serializedForm
        });
    },


    sortingFareSelectionResults: function(fareFinderForm, refNumber, selectedSorting){
        return $.ajax({
            url: ACC.config.contextPath + "/fare-selection/sorting-fare-selection-results",
            type : "POST",
            async : false,
            data : fareFinderForm + "&refNumber=" + refNumber + "&displayOrder=" + selectedSorting
        });
    },

    // Save Search.
    saveSearch: function(fareFinderForm){
        return $.ajax({
            url: ACC.config.contextPath + "/fare-selection/save-search",
            type : "POST",
            async : false,
            data : fareFinderForm
        });
    },

    getSuggestedOriginLocationsAjax: function(originLocation) {
        return $.ajax({
            url: ACC.config.contextPath + "/suggestions/origin?text="+originLocation,
            type : "GET",
            async : false
        });
    },

    getSuggestedDestinationLocationsAjax: function(originLocation, destinationLocation) {
        return $.ajax({
            url: ACC.config.contextPath + "/suggestions/destination?text=" + destinationLocation + "&code=" + originLocation,
            type : "GET",
            async : false
        });
    },
    getTravelSectorsAjax: function() {
        return $.ajax({
            url: ACC.config.contextPath + "/view/FareFinderComponentController/travel-routes" ,
            dataType : "json",
            type : "GET",
            async : false
        });
    },


    getRouteInfoAjax: function() {
        return $.ajax({
            url: ACC.config.contextPath + "/route-info" ,
            dataType : "json",
            type : "GET",
            async : false
        });
    },

    getCartTimerAjax: function() {
            return $.ajax({
                url: ACC.config.contextPath + "/cart-timer" ,
                dataType : "json",
                type : "GET"
            });
        },

    getCCRouteInfoAjax: function() {
        return $.ajax({
            url: ACC.config.contextPath + "/cc-route-info" ,
            dataType : "json",
            type : "GET"
        });
    },

    getTravelRoutesForLocationAjax: function(locationCode) {
        return $.ajax({
            url: ACC.config.contextPath + "/view/PackageFinderComponentController/travelroutes-location?locationCode=" + locationCode,
            type : "GET",
            async : false
        });
    },

    getTravellerAjax : function(uid) {
        return $.ajax({
            url : "traveller-details/get-traveller?uid=" + uid,
            dataType : "json",
            type : "GET",
            async : false
        });
    },
    refreshTitles : function(passengerTypeCode) {
        return $.ajax({
            url :ACC.config.contextPath + "/allowed-passenger-titles/" + passengerTypeCode,
            contentType: "application/json; charset=utf-8",
            type : "GET",
            async : false
        });
    },

    getCurrentUserDetailsAjax : function() {
        return $.ajax({
            url : "traveller-details/get-current-user-details",
            dataType : "json",
            type : "GET",
            async : false
        });
    },

    addSeatToCartAjax : function(selectedSeatNo, previousSelectedSeat, currentTransportOfferingCode, currentTravellerCode, currentTravelRoute, currentOriginDestinationRefNo, accommodationMapCode) {
        return $.ajax({
            url : "cart/add/accommodation?accommodationNo="+selectedSeatNo+"&previousSelectedAccommodation="+previousSelectedSeat+"&transportOfferingCode="+currentTransportOfferingCode+"&travellerCode="+currentTravellerCode+"&originDestinationRefNo="+currentOriginDestinationRefNo+"&travelRoute="+currentTravelRoute+"&accommodationMapCode="+accommodationMapCode,
            type : "GET",
            async : false
        });
    },

    removeSeatFromCartAjax : function(selectedSeatNo, currentTransportOfferingCode, currentTravellerCode, currentTravelRoute, accommodationMapCode) {
        return $.ajax({
            url : "cart/remove/accommodation?accommodationNo="+selectedSeatNo+"&transportOfferingCode="+currentTransportOfferingCode+"&travellerCode="+currentTravellerCode+"&travelRoute="+currentTravelRoute+"&accommodationMapCode="+accommodationMapCode,
            type : "GET",
            async : false
        });
    },

    removeRoomAjax : function(href) {
        return $.ajax({
            url : href,
            type : "GET",
            contentType: "application/json; charset=utf-8",
            async : false
        });
    },

    cancelBookingRequest : function(requestUrl, orderCode) {
        return $.ajax({
            url : ACC.config.contextPath + requestUrl + orderCode,
            type : "GET",
            contentType: "application/json; charset=utf-8",
            async : false
        });
    },

    cancelSailingRequest : function(orderCode, journeyRefNum, odRefNum) {
        return $.ajax({
            url : ACC.config.contextPath + "/manage-booking/cancel-sailing-request/",
            type : "GET",
            contentType: "application/json; charset=utf-8",
            data: {
                orderCode : orderCode,
                journeyRefNum : journeyRefNum,
                odRefNum : odRefNum
            },
            async : false
        });
    },

    cancelTraveller : function(orderCode,travellerUid) {
        return $.ajax({
            url : ACC.config.contextPath + "/manage-booking/cancel-traveller-request/",
            type : "GET",
            contentType: "application/json; charset=utf-8",
            data: {
                orderCode : orderCode,
                travellerUid : travellerUid
            },
            async : false
        });
    },

    sendReplanJourneyRequest : function(orderCode, journeyRefNum, odRefNum) {
        return $.ajax({
            url : ACC.config.contextPath + "/manage-booking/replan-journey-request/",
            type : "GET",
            contentType: "application/json; charset=utf-8",
            data: {
                orderCode : orderCode,
                journeyRefNum : journeyRefNum,
                odRefNum : odRefNum
            },
            async : false
        });
    },

    redeemVoucher: function(voucherCode){
        var url = ACC.config.contextPath + "/cart/voucher/redeem?voucherCode="+voucherCode;
        return $.ajax({
            url: url,
            type: "POST",
            async : false
        });
    },

    releaseVoucher: function(voucherCode){
        var url = ACC.config.contextPath + "/cart/voucher/release?voucherCode="+voucherCode;
        return $.ajax({
            url: url,
            type: "POST",
            async : false
        });
    },

    getSeatMapAjax : function(url) {
        return $.ajax({
            url : url,
            type : "GET",
            async : false
        });
    },

    upgradeBundle: function(){
        var url = $('#y_upgradeBundleForm').attr('action');
        var selectedBundleType = $('.y_bundleType:checked').parents('.active').find('.y_bundleType:checked').val();
        var refNumber = $('.y_bundleType:checked').parents('.active').find('#y_refNumber').val();
        return $.ajax({
            type : "POST",
            url : url,
            async : false,
            data: {
                selectedBundleType : selectedBundleType,
                refNumber : refNumber
            }
        });

    },

    renewSession : function() {
        var url = ACC.config.contextPath + "/session-cookie/renew-session/";
        return $.ajax({
            url: url,
            type: "GET",
            async : false
        });
    },

    validateAccommodationFinderForm: function(serializedForm){
        return $.ajax({
            url: ACC.config.contextPath + "/view/AccommodationFinderComponentController/validate-accommodation-finder-form",
            type: "POST",
            async: false,
            data: serializedForm
        });
    },

    getSuggestedAccommodationLocationsAjax: function(destinationLocation){
        return $.ajax({
            url: ACC.config.contextPath + "/accommodation-suggestions?text=" + destinationLocation,
            type : "GET",
            async : false
        });
    },

    getDealsForDestinationAjax: function(destinationLocation){
        return $.ajax({
            url: ACC.config.contextPath + "/vacations/promotions/filter-deals?hotelLocationName=" + destinationLocation,
            type : "GET",
            async : false
        });
    },


    readCustomerReviews : function(accommodationOfferingCode) {
        return $.ajax({
            url : ACC.config.contextPath + "/accommodation-search/customer-review/" + accommodationOfferingCode,
            type : "GET",
            contentType: "application/json; charset=utf-8",
            async : false
        });
    },

    getPagedAccommodationDetailsCustomerReviews : function(accommodationOfferingCode,pageNumber) {
        return $.ajax({
            url : ACC.config.contextPath + "/accommodation-details/customer-review?accommodationOfferingCode=" + accommodationOfferingCode+"&pageNumber="+pageNumber,
            type : "GET",
            contentType: "application/json; charset=utf-8",
            async : false
        });
    },

    addAccommodationToCartAjax: function(form,formAction){
        return $.ajax({
            type: form.attr('method'),
            url: formAction,
            async: false,
            data: form.serialize()
        });
    },
    sendAmendAccommodationRequest: function(formAction){
            return $.ajax({
                type: "GET",
                url: formAction,
                async: false

            });
        },
    addTransportBundleToCartAjax: function(form){
        return $.ajax({
            type: form.attr('method'),
            url: form.attr('action'),
            async: false,
            data: form.serialize()
        });
    },

    addAccommodationToCartForPackageAjax: function(form){
        return $.ajax({
            type: form.attr('method'),
            url: form.attr('action'),
            data: form.serialize()
        });
    },

    addTransportBundleToCartForPackageAjax: function(form){
        return $.ajax({
            type: form.attr('method'),
            url: form.attr('action'),
            data: form.serialize()
        });
    },

    validateLeadGuestDetailsForms: function(serializedForm){
        return $.ajax({
            url: ACC.config.contextPath + "/checkout/guest-details/validate-lead-guest-details-forms",
            type: "POST",
            async: false,
            data: serializedForm
        });
    },

    getAccommodationListing : function(pagenumber, resultsViewType) {
        return $.ajax({
            url : ACC.config.contextPath + "/accommodation-search/show-more?pageNumber="+pagenumber+"&resultsViewType="+resultsViewType,
            type : "GET",
            contentType: "application/json; charset=utf-8",
            async : false
        });
    },

    getDealListing : function(pagenumber,totalNumberOfResults) {
        return $.ajax({
            url : ACC.config.contextPath + "/deal-listing/show-more?pageNumber="+pagenumber+"&totalNumberOfResults="+totalNumberOfResults,
            type : "GET",
            contentType: "application/json; charset=utf-8",
            async : false
        });
    },

    getPackageListing : function(pagenumber, resultsViewType, priceRange) {
        return $.ajax({
            url : ACC.config.contextPath + "/package-listing/show-more",
            type : "GET",
            contentType: "application/json; charset=utf-8",
            data : {
                pageNumber : pagenumber,
                resultsViewType : resultsViewType,
                priceRange : priceRange
            },
            async : false
        });
    },

     getActivityListing : function(activityFinderForm, requestMapping) {
            return $.ajax({
                url : ACC.config.contextPath + "/"+requestMapping+"/show-more",
                type : "GET",
                contentType: "application/json; charset=utf-8",
                data : activityFinderForm.serialize(),
                async : false
            });
     },
    getVacationActivityListing : function(pageNumber, sortBy, destination, changeActivityParams, requestMapping) {
         return $.ajax({
            url : ACC.config.contextPath + "/"+requestMapping+"/show-more",
            type : "GET",
            contentType: "application/json; charset=utf-8",
            data :  {
                pageNumber : pageNumber,
                sortBy : sortBy,
                destination: destination,
                changeActivity : changeActivityParams.isChangeActivity,
                changeActivityCode : changeActivityParams.changeActivityCode,
                date : changeActivityParams.changeActivityDate,
                time : changeActivityParams.changeActivityTime
            },
            async : false
         });
    },

    getRegionsListing : function(pagenumber) {
        return $.ajax({
            url : ACC.config.contextPath + "/regions-landing/show-more",
            type : "GET",
            contentType: "application/json; charset=utf-8",
            data : {
                pageNumber : pagenumber
            },
            async : false
        });
    },
    getCitiesListing : function(pagenumber,dropDownSelect,regionSelectValue) {
        return $.ajax({
            url : ACC.config.contextPath + "/cities-landing/show-more",
            type : "GET",
            contentType: "application/json; charset=utf-8",
            data : {
                pageNumber : pagenumber,
                dropDownSelect : dropDownSelect,
                code           : regionSelectValue
            },
            async : false
        });
    },
    getCitiesByRegion : function(code) {
        return $.ajax({
            url : ACC.config.contextPath + "/cities-landing/cities-by-region",
            type : "GET",
            contentType: "application/json; charset=utf-8",
            data : {
                code : code
            },
            async : false
        });
    },
    getAQuoteListing : function(pagenumber) {
        return $.ajax({
            url : ACC.config.contextPath + "/get-quotelist/show-more",
            type : "GET",
            contentType: "application/json; charset=utf-8",
            data : {
                pageNumber : pagenumber
            },
            async : false
        });
    },
    getAQuoteListingStatus : function(status,code) {
    console.log("status",status);
        return $.ajax({
            url : ACC.config.contextPath + "/get-quotelist/change-status",
            type : "GET",
            contentType: "application/json; charset=utf-8",
            data : {
                status : status,
                code   : code
            },
            async : false
        });
    },
    getTerminalListing : function(pagenumber) {
        return $.ajax({
            url : ACC.config.contextPath + "/view/BCFTerminalListComponentController/show-more",
            type : "GET",
            contentType: "application/json; charset=utf-8",
            data : {
                pageNumber : pagenumber
            },
            async : false
        });
    },
     getTravelBookingListing : function(pagenumber,isUpcoming) {
         return $.ajax({
             url : ACC.config.contextPath + "/view/TravelBookingListComponentController/show-more",
             type : "GET",
             contentType: "application/json; charset=utf-8",
             data : {
                 pageNumber : pagenumber,
                 isUpcoming : isUpcoming
             },
             async : false
         });
     },
     getTravelBookingUpcomingList : function(isUpcoming) {
         return $.ajax({
             url : ACC.config.contextPath + "/view/TravelBookingListComponentController/vacationslist",
             type : "GET",
             contentType: "application/json; charset=utf-8",
             data : {
                 isUpcoming : isUpcoming
             },
             async : false
         });
     },
    getQuotesListing : function(pagenumber) {
        return $.ajax({
            url : ACC.config.contextPath + "/my-account/my-quotes/show-more",
            type : "POST",
            contentType: "application/json; charset=utf-8",
            data : {
                pageNumber : pagenumber
            },
            async : false
        });
    },

    cancelQuotes : function(codes) {
        return $.ajax({
            url : ACC.config.contextPath + "/my-account/my-quotes/cancel-quotes?codes="+codes,
            type : "POST",
            contentType: "application/json; charset=utf-8",
            data : {
                codes : codes
            },
            async : false
        });
    },

    getPackageListingWithPriceRangeFilter : function(resultsViewType,pageNumber,priceRange) {
        return $.ajax({
            url : ACC.config.contextPath + "/package-listing/filter-price-range",
            type : "GET",
            contentType: "application/json; charset=utf-8",
            data: {
                resultsViewType : resultsViewType,
                pageNum : pageNumber,
                priceRange : priceRange
            },
            async : false
        });
    },

    addExtraToCartAjax: function(){
        var form = $('#y_addExtraToCartForm');
        return $.ajax({
            type: form.attr('method'),
            url: form.attr('action'),
            async : false,
            data: form.serialize()
        });
    },

    validateAccommodationCartAjax : function() {
        return $.ajax({
            url : ACC.config.contextPath + "/accommodation-details/validate-cart",
            type : "GET",
            async : false
        });
    },

    checkAmendmentCartInSession : function() {
        return $.ajax({
            url : ACC.config.contextPath + "/manage-booking/check-amendmentcart-in-session",
            type : "GET",
            async : false
        });
    },



    deleteAmendmentCartFromSession : function() {
        return $.ajax({
            url : ACC.config.contextPath + "/manage-booking/delete-amendment-cart",
            type : "POST",
            async : false
        });
    },

    removeCurrentCart : function() {
        return $.ajax({
            url : ACC.config.contextPath + "/cart/remove-current-cart",
            type : "GET",
            async : false
        });
    },

    shallCartBeRemoved : function(journeyType) {
        return $.ajax({
            url : ACC.config.contextPath + "/cart/validate-cart-type",
            type : "GET",
            data : {journeyType : journeyType},
            async : false
        });
    },

    validateCartBeforePayment : function() {
        return $.ajax({
            url : ACC.config.contextPath + "/checkout/multi/payment-method/verify-cart",
            type : "GET",
            async : false
        });
    },

    refreshTransportSummaryComponent: function(componentUid){
        $.ajaxSetup({cache:false});
        return $.ajax({
            url : ACC.config.contextPath + "/view/TransportSummaryComponentController/refresh",
            type : "GET",
            data : {componentUid : componentUid},
            async : false
        });
    },

    refreshBcfGlobalReservationComponent: function(componentUid){
        $.ajaxSetup({cache:false});
        return $.ajax({
            url : ACC.config.contextPath + "/view/BcfGlobalReservationComponentController/refresh",
            type : "GET",
            data : {componentUid : componentUid},
            async : false
        });
    },

    refreshReservationTotalsComponent: function(componentUid){
        $.ajaxSetup({cache:false});
        return $.ajax({
            url : ACC.config.contextPath + "/view/ReservationTotalsComponentController/refresh",
            type : "GET",
            data : {componentUid : componentUid},
            async : false
        });
    },

    refreshAccommodationSummaryComponent: function(componentUid){
        $.ajaxSetup({cache:false});
        return $.ajax({
            url : ACC.config.contextPath + "/view/AccommodationSummaryComponentController/refresh",
            type : "GET",
            data : {componentUid : componentUid},
            async : false
        });

    },

    getTransportReservationComponent : function(componentId) {
        $.ajaxSetup({cache:false});
        return $.ajax({
            url : ACC.config.contextPath + "/view/TransportReservationComponentController/load",
            type : "GET",
            data : {
                componentUid : componentId
            },
            async : false
        });
    },


    getAccommodationReservationComponent : function(componentId) {
        $.ajaxSetup({cache:false});
        return $.ajax({
            url : ACC.config.contextPath + "/view/AccommodationReservationComponentController/load",
            type : "GET",
            data : {
                componentUid : componentId
            },
            async : false
        });
    },

    getReservationOverlayTotalsComponent: function (componentId) {
        $.ajaxSetup({cache: false});
        return $.ajax({
            url: ACC.config.contextPath + "/view/ReservationOverlayTotalsComponentController/load",
            type: "GET",
            data: {componentUid : componentId},
            async: false
        });
    },

    updateBookingDates : function(url) {
        var form = $("#y_updateBookingDatesForm");
        return $.ajax({
            url : form.attr('action'),
            type : form.attr('method'),
            async : false,
            data : form.serialize()
        });
    },

    validateTravelFinderForm : function(serializedForm){
        return $.ajax({
            url: ACC.config.contextPath + "/view/TravelFinderComponentController/validate-travel-finder-form",
            type: "POST",
            async: false,
            data: serializedForm
        });
    },

    refreshTravelFinderComponent: function(context, componentUid){
        $.ajaxSetup({cache:false});
        return $.ajax({
            url : context,
            ype : "GET",
            data : {
                componentUid : componentUid
            },
            async : false
        });
    },

    submitPaymentOptionForm: function(formID){
        var form = $(formID);
        return $.ajax({
            type: form.attr('method'),
            url: form.attr('action'),
            async : false,
            data: form.serialize()
        });

    },

    validatePaymentOptions: function(){
        var url = ACC.config.contextPath + "/checkout/multi/payment-method/check-payment-options";
        return $.ajax({
            url: url,
            type: "GET",
            async : false
        });
    },

    getDealValidDates : function(dealDepartureDate,dealStartingDatePattern){
        return $.ajax({
            url : ACC.config.contextPath + "/view/DealComponentController/get-valid-dates",
            type : "GET",
            contentType : "application/json; charset=utf-8",
            data : {
                dealDepartureDate : dealDepartureDate,
                dealStartingDatePattern : dealStartingDatePattern
            },
            async : false
        });
    },

    validateAndRefreshComponent : function(dealSelectedDepartureDate,dealComponentId,dealBundleTemplateId){
        return $.ajax({
            url : ACC.config.contextPath + "/view/DealComponentController/validate-departure-date",
            type : "GET",
            contentType : "application/json; charset=utf-8",
            data : {
                dealSelectedDepartureDate : dealSelectedDepartureDate,
                dealComponentId : dealComponentId,
                dealBundleTemplateId : dealBundleTemplateId
            },
            async : false
        });
    },

    validateSelectedDepartureDate : function(dealSelectedDepartureDate,dealBundleTemplateId){
        return $.ajax({
            url : ACC.config.contextPath + "/deal-details/validate-departure-date",
            type : "GET",
            contentType : "application/json; charset=utf-8",
            data : {
                dealSelectedDepartureDate : dealSelectedDepartureDate,
                dealBundleTemplateId : dealBundleTemplateId
            },
            async : false
        });
    },

    addBundleToCartAjax: function(form){
        return $.ajax({
            type: form.attr('method'),
            url: form.attr('action'),
            async: false,
            data: form.serialize()
        });
    },

    fareCalculatorAjax: function(form){
        return $.ajax({
            type: form.attr('method'),
            url: form.attr('action'),
            data: form.serialize()
        });
    },

     editPackageAjax: function(form){
            return $.ajax({
                type: form.attr('method'),
                url: form.attr('action'),
                data: form.serialize()
            });
        },

    validatePersonalDetailsForms: function(serializedForm){
        return $.ajax({
            url: ACC.config.contextPath + "/checkout/personal-details/validate-personal-details-forms",
            type: "POST",
            async: false,
            data: serializedForm
        });
    },

    addRoomPreference: function(roomStayRefNum,roomPreferenceCode){
        return $.ajax({
            url: ACC.config.contextPath +"/cart/accommodation/save-room-preference",
            type: "POST",
            async: false,
            data : {
                roomStayRefNum : roomStayRefNum,
                roomPreferenceCode : roomPreferenceCode
            }
        });
    },

    validateTravellerDetailsForms: function(serializedForm){
        return $.ajax({
            url: ACC.config.contextPath + "/traveller-details/validate-traveller-details-forms",
            type: "POST",
            async: false,
            data: serializedForm
        });
    },

    getUpgradeBundleOptions: function(){
        return $.ajax({
            url: ACC.config.contextPath +"/ancillary/upgrade-bundle-options",
            type: "GET",
            async: false
        });
    },

    getPackageUpgradeBundleOptions: function(){
        return $.ajax({
            url: ACC.config.contextPath +"/ancillary-extras/upgrade-bundle-options",
            type: "GET",
            async: false
        });
    },

    getAddRoomOptions: function(orderCode){
        return $.ajax({
            url: ACC.config.contextPath +"/view/TravelBookingDetailsComponentController/add-room-options",
            type: "POST",
            async: false,
            data: {orderCode : orderCode}
        });
    },

    validateAddRoomAccommodationFinderForm: function(serializedForm, orderCode){
        return $.ajax({
            url: ACC.config.contextPath +"/view/TravelBookingDetailsComponentController/validate-accommodation-availability-form/"+ orderCode,
            type: "POST",
            async: false,
            data: serializedForm
        });
    },

    reloadFareFinderComponent : function(componentId, vehicleType) {
        return $.ajax({
            url : ACC.config.contextPath + "/view/FareFinderComponentController/refresh",
            type : "GET",
            data : {
                componentUid : componentId,
                vehicleType  : vehicleType
            },
            async : false
        });
    },

    refreshQuoteInfoComponent: function(componentUid){
        $.ajaxSetup({cache:false});
        return $.ajax({
            url : ACC.config.contextPath + "/view/QuoteInfoComponentController/refresh",
            type : "GET",
            data : {componentUid : componentUid},
            async : false
        });
    },

    checkAgentComment : function(url) {
        return $.ajax({
            url : url,
            type : "POST",
            async : false
        });
    },

    getJourneyAmendComponent : function(componentId, journeyRefNumber, originDestinationRefNumber, orderCode) {
        $.ajaxSetup({cache:false});
        return $.ajax({
            url : ACC.config.contextPath + "/view/JourneyAmendComponentController/load",
            type : "GET",
            data : {
                componentUid : componentId,
                journeyRefNumber : journeyRefNumber,
                originDestinationRefNumber : originDestinationRefNumber,
                orderCode : orderCode
            },
            async : false
        });
    },

    waiveOffAdminFees:function(amount,cacheKey){
        return $.ajax({
            url : ACC.config.contextPath + "/view/ReservationTotalsComponentController/amend-booking-fees",
            type : "POST",
            data : {
                amount : amount,
                cachekey: cacheKey
            },
            async : false
        });
    },

    addDealBundleToCartAjax:function(form){
        return $.ajax({
            type: "POST",
            url: ACC.config.contextPath + "/cart/addDeal",
            data :  form.serialize(),
            async : false
        });
    },

    removeDealBundleFromCartAjax:function(dealId,dealDepartureDate){
        return $.ajax({
            type: "POST",
            url: ACC.config.contextPath + "/cart/removeDeal",
            data :  {
                dealId:dealId,
                date:dealDepartureDate
            },
            async : false
        });
    },

    performActionOnOrderAjax : function(url) {
        return $.ajax({
            url: url,
            type : "POST",
            async : false
        });
    },
    contentPopupDataAjax : function(url) {
        return $.ajax({
            url: url,
            type : "GET",
            async : false
        });
    },

    fetchAccessibilityPerPAX: function(serializedForm,boundFor){
        return $.ajax({
            url: ACC.config.contextPath + "/view/FareFinderComponentController/retrieve-accessibility-needs?boundFor="+boundFor,
            type: "POST",
            async: false,
            data: serializedForm
        });
    },

   removeAccessibilityPerPAX: function(serializedForm,boundFor){
            return $.ajax({
                url: ACC.config.contextPath + "/view/FareFinderComponentController/remove-accessibility-needs?boundFor="+boundFor,
                type: "POST",
                async: false,
                data: serializedForm
            });
        },

    fetchAccessibilityPerPAXPackage: function(serializedForm){
        return $.ajax({
            url: ACC.config.contextPath + "/package-ferry-passenger-info/retrieve-accessibility-needs",
            type: "POST",
            async: false,
            data: serializedForm
        });
    },

    removeAlert: function(){
        return $.ajax({
            type: "GET",
            url: ACC.config.contextPath + "/view/CMSAlertParagraphComponentController/remove-alert"
        });
    },
    shareItineraryToEmail : function(email_list,ebookingId) {
        return $.ajax({
            url : ACC.config.contextPath
                + "/checkout/send-email-itinerary?email_list="+email_list+"&ebookingId="+ebookingId,
            type : "GET",
            async: false
        });
    },

    getValidationMessages : function(messageCategory, formName) {
		return $.ajax({
			url : ACC.config.contextPath + "/validation/message",
			type : "GET",
			async : false,
			contentType : "application/json; charset=utf-8",
			data : {
				messageCategory : messageCategory,
				formName : formName
			}
		});
	},

	getValidationMessageForCode : function(messageCode) {
		return $.ajax({
			url : ACC.config.contextPath + "/validation/message-for-code",
			type : "GET",
			async : false,
			data : {
				code : messageCode
			}
		});
	},

    saveQuoteAnonymousUserAjax : function(emailAddress){
        return $.ajax({
            url: ACC.config.contextPath + "/quote/create?emailAddress="+emailAddress,
            type : "POST",
            async : false
        });
    },

    saveQuoteLoggedinUserAjax : function(){
        return $.ajax({
            url: ACC.config.contextPath + "/quote/create",
            type : "POST",
             async : false
        });
    },

    submitPaymentTypeFormForTravelCentre: function(url,data,type){
            return $.ajax({
                type: type,
                url: url,
                data: data
            });
     },
	getTransportBookingsAjax : function(url) {
		return $.ajax({
			url : ACC.config.contextPath + url,
			type : "POST",
			async : false
		});
	},
    getCurrentConditionsAjax: function (url) {
        return $.ajax({
            url: ACC.config.contextPath + url,
            type: "POST",
            async: false
        });
    },
	getAdvanceTransportBookingsAjax : function(url, pageNumber, bookingType, bookingReference, departurePortCode, arrivalPortCode, departureDateTimeFrom, departureDateTimeTo, pageNumber) {
		return $.ajax({
			url : ACC.config.contextPath + url,
			type : "POST",
            data: {
                pageNumber : pageNumber,
                bookingType : bookingType,
                bookingReference : bookingReference,
                departurePortCode : departurePortCode,
                arrivalPortCode : arrivalPortCode,
                departureDateTimeFrom : departureDateTimeFrom,
                departureDateTimeTo : departureDateTimeTo,
                pageNumber : pageNumber
            },
			async : false
		});
	},
    getCartAjax : function(customerId,cartId){
        return $.ajax({
            url : ACC.config.contextPath + "/assisted-service/linkcart",
            type : "POST",
            data: {
                customerId : customerId,
                cartId : cartId
            },
            async : false
        });
    },
    getDepartureDates : function(origin,destination,selectedMonth,selectedYear){
        return $.ajax({
            url : ACC.config.contextPath + "/getDepartureDates",
            type : "GET",
            data: {
                origin : origin,
                destination : destination,
                selectedMonth:selectedMonth,
                selectedYear:selectedYear
            },
            async : false
        });
    },

    orderReportDownload : function() {
        return $.ajax({
        url: ACC.config.contextPath + "/endofday-report/generate-endofday-report",
        type: "POST",
        async:false
        });
    },

    findActivitySchedulesAjax: function(activityCode, dateSelected){
        return $.ajax({
            url: ACC.config.contextPath + "/findActivitySchedules" ,
            type: "POST",
            dataType : "json",
            data :  {
                activityCode:activityCode,
                dateSelected:dateSelected
            },
            async: false
        });
    },

    checkActivityAvailability: function(addActivityToCartDataForm) {
        return $.ajax({
            type: "POST",
            url: ACC.config.contextPath + "/checkActivityAvailability",
            async : false,
            data: addActivityToCartDataForm.serialize()
        });
    },
    checkDealActivityAvailability: function(addActivityPriceSchedule) {
        return $.ajax({
            type: "POST",
            url: ACC.config.contextPath + "/checkDealActivityAvailability",
            async : false,
            data: addActivityPriceSchedule.serialize()
        });
    },
    getHotels : function(code) {
        return $.ajax({
            url : ACC.config.contextPath + "/hotel-details",
            type : "GET",
            contentType: "application/json; charset=utf-8",
            data : {
                code : code
            },
            async : false
        });
    },
    getShipInfo : function(code) {
        return $.ajax({
            url : ACC.config.contextPath + "/ship-info",
            type : "GET",
            contentType: "application/json; charset=utf-8",
            data : {
                code : code
            },
            async : false
        });
    },
    getOriginalBooking: function(originalBookingReference){
        return $.ajax({
            url: ACC.config.contextPath + "/manage-booking/original-booking/" + originalBookingReference,
            type: "POST",
            async: true
        });
    },
    submitSelectedRouteRegionForm: function(routeRegions){
            return $.ajax({
                url: ACC.config.contextPath + "/routes-fares/schedules/schedule-routemap",
                type: "GET",
                dataType : "json",
                data :  {
                    routeRegions:routeRegions
                },
                async: false
            });
    },
    resetBookingMarkers: function(bookingRef){
        return $.ajax({
        	url: ACC.config.contextPath + "/view/TravelBookingDetailsComponentController/reset-booking-markers/" + bookingRef,
            type: "POST",
            async: true
        });
    },
    addInternalComment : function(url, orderCode, comment) {
        if(comment === undefined || comment === "") {
            return;
        }
        return $.ajax({
            url: ACC.config.contextPath + url,
            type: "POST",
            data: {
             orderCode: orderCode,
             comment: comment
            },
            async: true
        });
    },
    addSupplierComment: function(url, code, ref, orderCode, comment) {
        if(code === undefined || code === "" || comment === undefined || comment === "") {
            return;
        }
        return $.ajax({
            url: ACC.config.contextPath + url,
            type: "POST",
            data: {
             code:code,
             ref:ref,
             orderCode: orderCode,
             comment:comment
            },
            async: true
        });
    },
    getSupplierComments: function(url, orderCode, code, ref, type) {
        if(code === undefined || code === "") {
            return;
        }
        return $.ajax({
            url: ACC.config.contextPath + url,
            type: "POST",
            data: {
             orderCode: orderCode,
             code: code,
             ref: ref,
             type: type
            },
            async: true
        });
    }
};
