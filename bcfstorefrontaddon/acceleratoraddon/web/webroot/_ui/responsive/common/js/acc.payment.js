/**
 * The module for payment pages.
 * @namespace
 */
ACC.payment = {

    _autoloadTracc: [
        "bindPaymentCardTypeValidationMessages",
        "bindPaymentCardTypeSelect",
        "bindPayNowButton",
        "bindPaymentOptionRadioButton",
        "bindingConfirmBookingForm",
        "bindNextStepButton",
        "loadReservationComponents",
        "enablePayNowButton",
        "activateSavedPaymentButton",
        "bindFareTermsAndCondition",
        "bindingRefundSelectionForm",
        "getRegions",
        "getMyAccountPaymentRegions",
        "bindEditSailingModal",
        "bindCancelSailingModal",
        "bindingGuestCheckoutDetails",
        "unbindingPaymentDetails",
        "bindingPaymentDetails",
        "bindInputNewCardRadio",
        "bindUseSavedCreditCardRadios",
        "bindUseSavedCTCCardDropdown",
        "bindUseNewCTCCardInput",
        "bindRegionsInPaymentForm",
        "bindRegionsInAddPaymentForm",
        "toggleCallCentrePaymentOptions"
    ],

    bindPaymentCardTypeValidationMessages: function () {
        ACC.PaymentCardTypeValidationMessages = new validationMessages("ferry-PaymentCardTypeValidationMessages");
        ACC.PaymentCardTypeValidationMessages.getMessages("error");
    },
    bindFareTermsAndCondition: function () {
        $('#fareTermsAndConditionLink').on('click', function (e) {
            event.preventDefault();
            var url = $(this).attr('href');
            $.when(ACC.services.contentPopupDataAjax(url)).then(
                function (data) {
                    var doc = new DOMParser().parseFromString(data, 'text/html');
                    var title = doc.getElementsByTagName("title")[0];
                    var body = doc.getElementsByTagName("body")[0];
                    $('#fareTermsAndConditionPageTitle').replaceWith(title.innerHTML);
                    $('#fareTermsAndConditionPage').replaceWith(body);
                    $('#fareTermsAndConditionModal').modal();
                }
            );
        });
    },

    /**
     * bind the card type field to the filterCardInformationDisplayed function.
     */
    bindPaymentCardTypeSelect: function () {
        ACC.payment.filterCardInformationDisplayed();
        $("#pd-cardtype").change(function () {
            ACC.payment.filterCardInformationDisplayed();
        });
    },

    /**
     * hide/show payment fields based on the card type selected
     */
    filterCardInformationDisplayed: function () {
        var cardType = $('#pd-cardtype').val(),
            $startDate = $("#pd-valid-day").closest(".form-group").parent(),
            $issueNum = $("#pd-valid-year").closest(".form-group").parent();
        if (cardType == '024') {
            $startDate.show();
            $issueNum.show();
        }
        else {
            $startDate.hide();
            $issueNum.hide();
        }
    },

    /**
     * disable the pay now button after a click event to avoid a double payment/submission.
     */
    bindPayNowButton: function () {
        $(".y_payNow").on("click", function () {
            $(this).attr("disabled", true);
            $('#y_processingModal').modal({
                backdrop: 'static',
                keyboard: false
            });
            $(this).parents('form').submit();
        });
    },

    bindPaymentOptionRadioButton: function () {
        $("#payment-type").on("change", ".y_paymentOptionRadio", function () {
            $('.y_paymentOptionRadio').not(this).attr('checked', false);
            $(".y_paymentOptionError").hide();
            var formID = "#" + this.value;
            $.when(ACC.services.submitPaymentOptionForm(formID)).then(
                function (data) {
                    var jsonData = JSON.parse(data);
                    if (!jsonData.valid) {
                        $(".y_paymentOptionErrorContent").html(ACC.addons.bcfstorefrontaddon[jsonData.errors[0]]);
                        $(".y_paymentOptionError").show();
                        $(".y_paymentOptionRadio").prop('checked', false);
                    }
                }
            )
        });
    },

    bindNextStepButton: function () {
        $("#y_selectPaymentMethod").on("click", function (e) {
            $.when(ACC.services.validatePaymentOptions()).then(
                function (data) {
                    var jsonData = JSON.parse(data);
                    if (!jsonData.valid) {
                        e.preventDefault();
                        $(".y_paymentOptionErrorContent").html(ACC.addons.bcfstorefrontaddon[jsonData.errors[0]]);
                        $(".y_paymentOptionError").show();
                    }
                }
            );
        })

    },

    loadReservationComponents : function() {
        if($(".y_checkoutSummary")){
            if($("#y_transportReservationComponentId").val()){
                ACC.reservation.getTransportReservationComponent($("#y_transportReservationComponentId").val());
            }
            if($("#y_accommodationReservationComponentId").val()){
                ACC.reservation.getAccommodationReservationComponent($("#y_accommodationReservationComponentId").val());
            }
        }
    },

    enablePayNowButton: function() {
        $(".y_payNow").removeAttr("disabled");
    },

    activateSavedPaymentButton: function(){

        $(document).on("click", ".js-saved-payments", function (e) {
            e.preventDefault();

            var title = $("#savedpaymentstitle").html();

            $.colorbox({
                href: "#savedpaymentsbody",
                inline: true,
                maxWidth: "100%",
                opacity: 0.7,
                width: "320px",
                title: title,
                close: '<span class="glyphicon glyphicon-remove"></span>',
                onComplete: function () {
                }
            });
        })
    },
    bindRegionsInPaymentForm: function () {
        $(".billing-address-country").change(function () {
            var idVal = $(this).prop('id');
            var updateFormNumber = idVal.split("_")[1];
            var val = $(this).val();

            if (isNaN(updateFormNumber)) {
                updateFormNumber = 1;
            }

            if (val == "CA" || val == "US") {
                $('#billing-address-regions-section_' + updateFormNumber).show();
            } else {
                $('#billing-address-regions-section_' + updateFormNumber).hide();
            }
        })
    },
    bindRegionsInAddPaymentForm: function () {
        $('.billing-address-regions-group').hide();

        var currentVal = $(".billing-address-country").val();
        if (currentVal == "CA" || currentVal == "US") {
            $('.billing-address-regions-group').show();
        }

        $(".billing-address-country").change(function () {
            var val = $(this).val();
            if (val == "CA" || val == "US") {
                $('.billing-address-regions-group').show();
            } else {
                $('.billing-address-regions-group').hide();
            }
        })
    },
    getMyAccountPaymentRegions: function () {
        $('.myAccountPaymentRegisterCountry').on('change', function (e) {
            var idVal = $(this).prop('id');
            var html = "";
            $.when(ACC.services.getRegionsAjax($(this).val())).then(function (response) {
                if (response) {
                    html = "<option value=-1 > Please Select </option>"
                    for (var key in response) {
                        html += "<option value=" + response[key].isocode + ">" + response[key].name + "</option>"
                    }
                    var updateFormNumber = idVal.split("_")[1];
                    $('#myAccountBillingAddressRegions_' + updateFormNumber).html(html);
                    $('#myAccountBillingAddressRegions_' + updateFormNumber).show();
                }
            });
        });
    },

    getRegions: function () {
        $('.billing-address-country').on('change', function (e) {
            var html = "";
            $.when(ACC.services.getRegionsAjax($(this).val())).then(function (response) {
                if (response) {
                    html = "<option value=-1 >" + $('.billing-address-regions option:eq(0)').text() + "</option>"
                    for (var key in response) {
                        html += "<option value=" + response[key].isocodeShort + ">" + response[key].name + "</option>"
                    }
                    $('#billing-address-regions').html(html);
                }
            });
        });
    },
    bindEditSailingModal: function () {
        $(function () {
            $(".js-open-edit-modal").click(function () {
                $("a#edit-sailing-url").attr('href', $(this).data('id'));
            });
        });
    },
    bindCancelSailingModal: function () {
        $(function () {
            $(".js-open-cancel-modal").click(function () {
                $("a#remove-sailing-url").attr('href', $(this).data('id'));
            });
        });
    },
    unbindingPaymentDetails: function (removeValidation) {
        if (removeValidation == true) {
            $(".y_first_name").each(function () {
                $(this).rules("remove");
                $(this).removeClass("fe-error");
            });
            $(".y_card_expiry").each(function () {
                $(this).rules("remove");
                $(this).removeClass("fe-error");
            });
            $(".y_cardVerification").each(function () {
                $(this).rules("remove");
                $(this).removeClass("fe-error");
            });
            $(".y_address_line1").each(function () {
                $(this).rules("remove");
                $(this).removeClass("fe-error");
            });
            $(".y_address_line2").each(function () {
                $(this).rules("remove");
                $(this).removeClass("fe-error");
            });
            $(".y_city").each(function () {
                $(this).rules("remove");
                $(this).removeClass("fe-error");
            });
            $(".y_postal_code").each(function () {
                $(this).rules("remove");
                $(this).removeClass("fe-error");
            });
            $("#country").each(function () {
                $(this).rules("remove");
                $(this).removeClass("fe-error");
            });
        }

    },

    bindingRefundSelectionForm: function () {

        $("#refundSelectionFormButton").on("click", function (e) {


            var $form = $("#y_refundSelectionForm");

            $form.validate({
                errorElement: "span",
                errorClass: "fe-error",
                ignore: ".fe-dont-validate",
                onfocusout: function (element) {
                    $(element).valid();
                },
                invalidHandler: function (form, validator) {
                    var errors = validator.numberOfInvalids();
                    if (errors) {
                        validator.errorList[0].element.focus();
                    }
                },
                errorPlacement: function (error, element) {

                    error.insertAfter(element);
                }

            });
            $("#termsCheck1").each(function () {
                $(this).rules("add", {
                    required: true,
                    messages: {
                        required: ACC.PaymentCardTypeValidationMessages.message('error.formvalidation.payment.terms_and_conditions')
                    }
                });
            });


        })

    },

    bindingConfirmBookingForm: function () {


        var $form = $("#confirmBookingDetailsForm");
        if ($form.is('form')) {
            $form.validate();
        }

        $("#y_confirmBookingDetailsButton").on("click", function (e) {
            var $form = $("#confirmBookingDetailsForm");

            $form.validate({
                errorElement: "span",
                errorClass: "fe-error",
                ignore: ".fe-dont-validate",
                onfocusout: function(element) {
                    $(element).valid();
                },
                invalidHandler: function(form, validator) {
                    var errors = validator.numberOfInvalids();
                    if (errors) {
                        validator.errorList[0].element.focus();
                    }
                },
                errorPlacement: function(error, element) {

                    error.insertAfter(element);
                }

            });
            $("#termsCheck1").each(function () {
                $(this).rules("add", {
                    required: true,
                    messages: {
                        required: ACC.PaymentCardTypeValidationMessages.message('error.formvalidation.payment.terms_and_conditions')
                    }
                });
            });


        })

    },


    bindingPaymentDetails: function () {
        var $form = $("#silentOrderPostForm");
        $form.find('#pd-card-verification').attr('maxLength', '4');

        jQuery.validator.addMethod("alphanumeric", function (value, element) {
            return this.optional(element) || /^([a-zA-Z0-9]*[\ \-])*[a-zA-Z0-9]+$/.test(value);
        }, ACC.addons.bcfstorefrontaddon['error.formvalidation.payment.alphanumeric_text']);

        $form.validate({
            errorElement: "span",
            errorClass: "fe-error",
            ignore: ".fe-dont-validate",
            onfocusout: function(element) {
                $(element).valid();
            },
            invalidHandler: function(form, validator) {
                var errors = validator.numberOfInvalids();
                if (errors) {
                    validator.errorList[0].element.focus();
                }
            },
            errorPlacement: function (error, element) {
                if (element.attr('id') == 'cardType1') {
                    element.closest('div').append(error);
                } else {
                    error.insertAfter(element);
                }
            }

        });

        if($(".y_expiryyear").length >0) {
            $.validator.addMethod("expiryYearCheck", function (value, element) {
                var selMonth = $(".y_expirymonth").val();
                var selyear = $(element).val();
                var dt = new Date();
                var currMonth = parseInt(dt.getMonth()) + 1;
                var currYear = parseInt(dt.getFullYear());
                if (selyear != null && parseInt(selMonth) < currMonth && parseInt(currYear) >= selyear) {
                    $(".y_expirymonth").removeClass("fe-error");
                    $("#pd-exp-year-error").text("");
                    $("#pd-exp-day-error").text("");
                    return false;
                }
                $(".y_expirymonth").removeClass("fe-error");
                $("#pd-exp-year-error").text("");
                $("#pd-exp-day-error").text("");
                return true;
            });
            $(".y_expiryyear").each(function () {
                $(this).rules("add", {
                    required: true,
                    expiryYearCheck: true,
                    messages: {
                        required: ACC.PaymentCardTypeValidationMessages.message('error.formvalidation.payment.expiry_month'),
                        expiryYearCheck: ACC.PaymentCardTypeValidationMessages.message('error.formvalidation.payment.card_expire')
                    }
                });
            });
        }



        if($(".y_expirymonth").length >0){
            $.validator.addMethod("expiryMonthCheck", function (value, element) {
                var selMonth = $(element).val();
                var selyear = $(".y_expiryyear").val();
                var dt = new Date();
                var currMonth = parseInt(dt.getMonth()) + 1;
                var currYear = parseInt(dt.getFullYear());
                if (selyear != null && parseInt(selMonth) < currMonth && parseInt(currYear) >= selyear) {
                    $(".y_expiryyear").removeClass("fe-error");
                    $("#pd-exp-year-error").text("");
                    $("#pd-exp-day-error").text("");
                    return false;
                }
                $(".y_expiryyear").removeClass("fe-error");
                $("#pd-exp-year-error").text("");
                $("#pd-exp-day-error").text("");
                return true;
            });
            $(".y_expirymonth").each(function () {
                $(this).rules("add", {
                    required: true,
                    expiryMonthCheck: true,
                    messages: {
                        required: ACC.PaymentCardTypeValidationMessages.message('error.formvalidation.payment.expiry_month'),
                        expiryMonthCheck: ACC.PaymentCardTypeValidationMessages.message('error.formvalidation.payment.card_expire')
                    }
                });
            });
        }


        if($(".y_card_expiry").length >0){
            $.validator.addMethod("cardExpiryMonthCheck", function (value, element) {
                if ($(element).val().substring(0, 2).length < 2)
                    return false;
                return true;
            });
            $.validator.addMethod("cardExpiryYearCheck", function (value, element) {
                if ($(element).val().substring(3, 5).length < 2)
                    return false;
                return true;
            });
            $.validator.addMethod("cardExpiryCheck", function (value, element) {
                var selMonth = $(element).val().substring(0, 2);
                var selYear = $(element).val().substring(3, 5);
                var selDate = new Date(selMonth + " 1" + " 20" + selYear);
                if (selDate > new Date())
                    return true;
                return false;
            });
            $(".y_card_expiry").each(function () {
                $(this).rules("add", {
                    required: true,
                    cardExpiryMonthCheck: true,
                    cardExpiryYearCheck: true,
                    cardExpiryCheck: true,
                    messages: {
                        required: ACC.PaymentCardTypeValidationMessages.message('error.formvalidation.dateUK'),
                        cardExpiryMonthCheck: ACC.PaymentCardTypeValidationMessages.message('error.formvalidation.payment.expiry_month'),
                        cardExpiryYearCheck: ACC.PaymentCardTypeValidationMessages.message('error.formvalidation.payment.expiry_year'),
                        cardExpiryCheck: ACC.PaymentCardTypeValidationMessages.message('error.formvalidation.payment.card_expire')
                    }
                });
            });
        }

        $(".y_state").each(function(){
            $(this).rules( "add", {
                required: true,
                maxlength: 30,
                messages: {
                    required: ACC.PaymentCardTypeValidationMessages.message('error.formvalidation.payment.state')
                }
            });
        });


        $(".y_cardNumber").each(function () {
            $(this).rules( "add", {
                required: true,
                digits: true,
                messages: {
                    required: ACC.PaymentCardTypeValidationMessages.message('error.formvalidation.cardNumber')
                }
            });
        });

        $(".y_nameOnCard").each(function () {
            $(this).rules( "add", {
                required: true,
                maxlength: 22,
                nameValidation: true,
                messages: {
                    required: ACC.PaymentCardTypeValidationMessages.message('error.formvalidation.cardHolderName')
                }
            });
        });

        $(".y_cardVerification").each(function () {
            $(this).rules( "add", {
                required: true,
                digits: true,
                messages: {
                    required: ACC.PaymentCardTypeValidationMessages.message('error.formvalidation.cardVerification')
                }
            });
        });

        $("#first-name2").each(function(){
            $(this).rules( "add", {
                required: true,
                maxlength: 22,
                messages: {
                    required: ACC.PaymentCardTypeValidationMessages.message('error.formvalidation.payment.first.name')
                }
            });
        });
        //Fallback for IE as searchParams are not supported
        var sAgent = window.navigator.userAgent;
        var isIE = sAgent.indexOf("MSIE");
        if (isIE > 0) {
            function getUrlParameter(name) {
                name = name.replace(/[\[]/, '\\[').replace(/[\]]/, '\\]');
                var regex = new RegExp('[\\?&]' + name + '=([^&#]*)');
                var results = regex.exec(location.search);
                return results === null ? '' : decodeURIComponent(results[1].replace(/\+/g, ' '));
            };
            var urlbusiness = getUrlParameter('paymentMethod');
        } else {
            var urlbusiness = (new URL(document.location)).searchParams;
        }

        if (urlbusiness && urlbusiness.get('paymentMethod') != 'CTCTCCard') {
            $("#first-name1").each(function () {
                $(this).rules("add", {
                    required: true,
                    maxlength: 50,
                    messages: {
                        required: ACC.PaymentCardTypeValidationMessages.message('error.formvalidation.payment.first.name')
                    }
                });
            });


            $(".y_address_line2").each(function () {
                $(this).rules("add", {
                    maxlength: 100,
                    messages: {
                        required: ACC.PaymentCardTypeValidationMessages.message('error.formvalidation.payment.address_line2')
                    }
                });
            });

            $(".y_city").each(function () {
                $(this).rules("add", {
                    required: true,
                    maxlength: 30,
                    messages: {
                        required: ACC.PaymentCardTypeValidationMessages.message('error.formvalidation.payment.city')
                    }
                });
            });

            $("#country").each(function () {
                $(this).rules("add", {
                    required: true,
                    maxlength: 30,
                    messages: {
                        required: ACC.PaymentCardTypeValidationMessages.message('error.formvalidation.payment.country')
                    }
                });
            });
            $(".y_postal_code").each(function () {
                $(this).rules("add", {
                    required: true,
                    alphanumeric: true,
                    maxlength: 9,
                    messages: {
                        required: ACC.PaymentCardTypeValidationMessages.message('error.formvalidation.payment.postal_code')
                    }
                });
            });
        }
        $(".y_last_name").each(function(){
            $(this).rules( "add", {
                required: true,
                maxlength: 22,
                messages: {
                    required: ACC.PaymentCardTypeValidationMessages.message('error.formvalidation.payment.last.name')
                }
            });
        });

        $(".y_email").each(function(){
            $(this).rules( "add", {
                required: true,
                maxlength: 100,
                validateEmailPattern: true,
                messages: {
                    required: ACC.PaymentCardTypeValidationMessages.message('error.formvalidation.payment.email')
                }
            });
        });

        $(".y_confirm_email").each(function () {
            $(this).rules( "add", {
                required: true,
                equalTo: "#email",
                messages: {
                    required: ACC.PaymentCardTypeValidationMessages.message('error.formvalidation.payment.confirm.email')
                }
            });
        });

        $(".y_phone_number").each(function(){
            $(this).rules( "add", {
                required: true,
                maxlength: 15,
                minlength: 10,
                digits: true,
                messages: {
                    required: ACC.PaymentCardTypeValidationMessages.message('error.formvalidation.payment.phone.number'),
                    maxlength: ACC.PaymentCardTypeValidationMessages.message('error.formvalidation.payment.phone.number'),
                    minlength: ACC.PaymentCardTypeValidationMessages.message('error.formvalidation.payment.phone.number')
                }
            });
        });

        $("#termsCheck1").each(function(){
            var validator = $.data(this.form, "validator");
            if (validator) {
                $(this).rules("add", {
                    required: true,
                    messages: {
                        required: ACC.PaymentCardTypeValidationMessages.message('error.formvalidation.payment.terms_and_conditions')
                    }
                });
            }
        });
    },
    bindUpdateForm: function () {
        $('#updatePaymentCard').click(function () {
            $(".y_postal_code").each(function () {
                $(this).rules("add", {
                    required: true,
                    alphanumeric: true,
                    maxlength: 9,
                    messages: {
                        required: ACC.PaymentCardTypeValidationMessages.message('error.formvalidation.payment.postal_code')
                    }
                });
            });
        });
    },
    bindInputNewCardRadio: function () {
        $('#use-new-card-radio').click(function () {
            if ($(this).is(':checked')) {
                $('.new-card-and-billing-details').removeClass("hidden");
                $("input[id^='savedCCCardCode']").attr("checked", false);
                $("#savedCTCcardCode").val('default');
                $("#savedCTCcardCode").selectpicker("refresh");
                $("input[id='y_ctcCard']").val('');
                $("#y_paymentType").val("");
                ACC.payment.bindingPaymentDetails();
            }
        });
    },
    bindUseSavedCreditCardRadios: function () {
        $("input[id^='savedCCCardCode']").click(function () {
            if ($(this).is(':checked')) {
                $("input[id='use-new-card-radio']").attr("checked", false);
                $(".new-card-and-billing-details").addClass("hidden");
                $("#savedCTCcardCode").val('default');
                $("#savedCTCcardCode").selectpicker("refresh");
                $("input[id='y_ctcCard']").val('');
                $("#y_paymentType").val("");
                ACC.payment.unbindingPaymentDetails(true);
            }
        });
    },
    bindUseSavedCTCCardDropdown: function () {
        $("#savedCTCcardCode").change(function () {
            if ($('#savedCTCcardCode :selected').val() != '-1') {
                $("input[id='use-new-card-radio']").attr("checked", false);
                $(".new-card-and-billing-details").addClass("hidden");
                $("input[id='y_ctcCard']").val('');
                $("input[id^='savedCCCardCode']").attr("checked", false);
                $("#y_paymentType").val("CTCCard");
                ACC.payment.unbindingPaymentDetails(true);
            }
        })
    },
    bindUseNewCTCCardInput: function () {
        $("input[id='y_ctcCard']").on('input', function (e) {
            if ($.trim($(this).val()) !== '') {
                $("input[id='use-new-card-radio']").attr("checked", false);
                $(".new-card-and-billing-details").addClass("hidden");
                $("#savedCTCcardCode").val('default');
                $("#savedCTCcardCode").selectpicker("refresh");
                $("input[id^='savedCCCardCode']").attr("checked", false);
                $("#y_paymentType").val("CTCCard");
                ACC.payment.unbindingPaymentDetails(true);
            } else {
                ACC.payment.bindingPaymentDetails();
            }
        });
    },
    bindingGuestCheckoutDetails: function () {
        var $form = $("#y_agentPayNow");
        jQuery.validator.addMethod("alphanumeric", function(value, element) {
            return this.optional(element) || /^([a-zA-Z0-9]*[\ \-])*[a-zA-Z0-9]+$/.test(value);
        }, ACC.addons.bcfstorefrontaddon['error.formvalidation.payment.alphanumeric_text']);

        $form.validate({
            errorElement: "span",
            errorClass: "fe-error",
            ignore: ".fe-dont-validate",
            onfocusout: function(element) {
                $(element).valid();
            },
            invalidHandler: function(form, validator) {
                var errors = validator.numberOfInvalids();
                if (errors) {
                    validator.errorList[0].element.focus();
                }
            }
        });

    },
    toggleCallCentrePaymentOptions : function() {
        if ($('.asm-payment-type-selector').length > 0) {
            $('.asm-payment-type-selector').click(function () {
                if ($(this).is(':checked')) {
                    $("div.asm-payment-type-options").addClass('hidden');
                    $(this).parents('.asm-payment-type-wrapper').find('.asm-payment-type-options').removeClass('hidden');
                }
            });
        }
    }
};


$('.p-accordion .p-box').find('.p-header').click(function (e) {

    if ($(this).hasClass("payshow")) {
        setTimeout(function () {
            $(this).toggleClass('payshow');
        }.bind(this), 100);
    }
    $('.p-accordion .p-box .p-collapse').collapse('hide');
    $(this).closest('.p-box').find('.p-collapse').collapse('toggle');
    e.stopPropagation();
    $("#accordion-7").accordion({header: "h3", collapsible: true, active: false});
});

$("ul.payment-img").find('li').click(function(){
    $("ul.payment-img li").removeClass('active');
    $(this).addClass('active');
});


$(".activeShow").click(function(){
    $(".payment-agree").toggleClass("hidden");
});

$(".activeShow").click(function(){
    $(".payment-view-text").toggleClass("hidden");
    $(".payment-hide-text").toggleClass("hidden");
});

