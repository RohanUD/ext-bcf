/**
 * The module for basket summary pages.
 * 
 * @namespace
 */
ACC.basketsummary = {
	_autoloadTracc : [ "init", "bindCloseSaveQuote" ],

	componentParentSelector : '#y_journeyAmendForm',

	init : function() {
		ACC.basketsummary.bindJourneyAmendValidation();
		ACC.basketsummary.changeVehicleForOppositeLeg();
	},
	bindJourneyAmendValidation : function() {
		$(ACC.basketsummary.componentParentSelector)
				.validate(
						{
							errorElement : "span",
							errorClass : "fe-error",
							ignore : ".fe-dont-validate",
							submitHandler : function(form) {
								ACC.farefinder.setVehicleTravelRouteCode();
								var jsonData = ACC.formvalidation
										.serverFormValidation(form,
												"validateAmendJourneyAttributes");
								if (!jsonData.hasErrorFlag) {
									$('#y_processingModal').modal({
										backdrop : 'static',
										keyboard : false
									});
									form.submit();
								}
							},
							onfocusout : function(element) {
								$(element).valid();
							},
							rules : {
								departureLocationName : {
									required : {
										depends : function() {
											$(this).val($.trim($(this).val()));
											return true;
										}
									},
									locationIsValid : ".y_fareFinderOriginLocationCode"
								},
								arrivalLocationName : {
									required : {
										depends : function() {
											$(this).val($.trim($(this).val()));
											return true;
										}
									},
									locationIsValid : ".y_fareFinderDestinationLocationCode"
								},
								cabinClass : "required"
							},
							messages : {
								departureLocationName : {
									required : ACC.addons.bcfstorefrontaddon['error.farefinder.from.location'],
									locationIsValid : ACC.addons.bcfstorefrontaddon['error.farefinder.from.locationExists']
								},
								arrivalLocationName : {
									required : ACC.addons.bcfstorefrontaddon['error.farefinder.to.location'],
									locationIsValid : ACC.addons.bcfstorefrontaddon['error.farefinder.to.locationExists']
								},
								cabinClass : ACC.addons.bcfstorefrontaddon['error.farefinder.cabin.class']
							}
						});
	},

	changeVehicleForOppositeLeg : function() {
		$("#modifyRelatedSailing").change(function() {
			if (this.checked) {
				$(".differentVehicleCheckBox").removeClass("hidden");
				$(".dangerousGoodReturnCheckBox").removeClass("hidden");
				if ($("#vehicleinboundcheckbox").is(":checked")) {
					$(".vehicle_1").removeClass("hidden");
					$('#y_standardRadbtn_1').prop('checked', true);
					$('#y_standardRadbtn_1').trigger('click');
				} else {
					$(".vehicle_1").addClass("hidden");
					$('#y_standardRadbtn_1').prop('checked', false);
					$('#y_commercialRadbtn_1').prop('checked', false);
					$('#y_overSizeRadbtn_1').prop('checked', false);

				}
			} else {
				if ($('#vehicleinboundcheckbox').is(":checked")) {
					$("#vehicleinboundcheckbox").trigger('click');
				} // Unchecks 'Return with Different Vehicle' checkbox
				$(".differentVehicleCheckBox").addClass("hidden");
				$(".dangerousGoodReturnCheckBox").addClass("hidden");
				$(".vehicle_1").addClass("hidden");
			}
		});
	},

	bindCloseSaveQuote : function() {
        if( $("#y_saveQuoteModal").length>0){
        	$("#y_saveQuoteModal").modal();
        }

 },

	getJourneyAmendComponent : function(journeyRefNumber,
			originDestinationRefNumber, componentId, orderCode) {
		var component = $("#journeyAmendComponentId").val();
		if (component == undefined || component == null) {
			component = componentId;
		}
		$.when(
				ACC.services
						.getJourneyAmendComponent(component, journeyRefNumber,
								originDestinationRefNumber, orderCode)).then(
				function(response) {
					$('#journeyAmendComponent').replaceWith(response);
					$("#fareFinderModal").modal();
				}, function() {
					console.log("Error occoured while getting component from server");
				});
		ACC.farefinder.init();
		ACC.basketsummary.init();
		if ($("#isNorthernRoute").val() == "true") {
			$('.y_passengers').find('div.y_passengerWrapper').each(function() {
				if ($(this).hasClass('LONG') && $(this).hasClass('sr-only')) {
					$(this).removeClass('sr-only');
				}
			});
		} else {
			$('.y_passengers').find('div.y_passengerWrapper').each(function() {
				if ($(this).hasClass('LONG') && !$(this).hasClass('sr-only')) {
					$(this).addClass('sr-only');
				}
			});
		}
		$(':input[type="submit"]').prop('disabled', true);
		var $form = $('form'), origForm = $form.serialize();
		$('form :input').on('change input', function() {
			var isFormChanged = ($form.serialize() !== origForm);
			if (isFormChanged) {
				$(':input[type="submit"]').prop('disabled', false);
			} else {
				$(':input[type="submit"]').prop('disabled', true);
			}
		});
	}
};
