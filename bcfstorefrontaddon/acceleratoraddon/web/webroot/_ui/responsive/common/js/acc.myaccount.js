/**
 * The module for my account pages.
 * @namespace
 */
ACC.myaccount = {
	_autoloadTracc: [
		"bindMyaccountMessages",
		"bindCollapseOutboundInbound",
		"initializeDobExpiryDatePickers",
		"bindingUpdatePersonalDetailsSave",
		"bindConfirmDeleteBooking",
		"bindMyAccountMyBookingsShowMore",
		"bindRemoveBooking",
		"bindProfileUpdate",
		"showAddBusinessModalAlert",
		"logoutBtnhide",
		"enableDisableSubscriptions",
		"enableDisableSubscriptionsForASM",
		"showDetailsAfterUpdate",
		"bindingUpdatepasswordlDetailsSave",
		"bindRemoveCardModal"
	],

	/**
	 * Hide/show the chnage password sections
	 */

	bindMyaccountMessages : function (){
		ACC.validationMyaccountMessages = new validationMessages("ferry-Myaccountvalidation");
		ACC.validationMyaccountMessages.getMessages("error");
		ACC.validationMyaccountMessages.getMessages("label");
	},

	bindRemoveCardModal: function () {
		$(".js-open-cancel-card-modal").click(function () {
			$("a#remove-card-url").attr('href', $(this).data('id'));
		});
	},
	bindProfileUpdate: function () {

		$("#editProfileBtn").on('click', function () {
			$("#myProfileUpdateEditPanel").toggle();
			$("#updateEmailPanel").hide();
			$("#changePasswordEditPanel").hide();
			$('.account-profile-detailssection').toggle();
		});

		$("#changePasswordBtn").on('click', function () {
			$("#myProfileUpdateEditPanel").hide();
			$("#updateEmailPanel").hide();
			$("#changePasswordEditPanel").toggle();
		});

		$("#updateEmailBtn").on('click', function () {
			$("#myProfileUpdateEditPanel").hide();
			$("#updateEmailPanel").toggle();
			$("#changePasswordEditPanel").hide();
			$('.myProfileEmailid').toggle();
		});

		$(".updatePaymentBtn").on('click', function () {
			var val = $(this).attr('data-paymenteditnumber');
			$("#payment-card-details"+val).toggle();
			$("#updatePaymentCardSection"+val).show()
		});

		$(".updateAccountPaymentBtn").on('click', function () {
			var val = $(this).attr('data-accountPaymentEditNumber');
			$("#accountPayment-card-details"+val).toggle();
			$("#updateAccountPaymentCardSection"+val).show()
		});

	},
	showDetailsAfterUpdate : function (){

		$(document).ready(function() {
			//var urlParams = (new URL(document.location)).searchParams;
			// if (urlParams.has('expand') == true && urlParams.get('expand') == 'changePassword') // true
			//Fallback for IE as searchParams are not supported
			var sAgent = window.navigator.userAgent;
			var isIE = sAgent.indexOf("MSIE");
			if(isIE > 0){
				function getUrlParameter(name) {
					name = name.replace(/[\[]/, '\\[').replace(/[\]]/, '\\]');
					var regex = new RegExp('[\\?&]' + name + '=([^&#]*)');
					var results = regex.exec(location.search);
					return results === null ? '' : decodeURIComponent(results[1].replace(/\+/g, ' '));
				};
				var urlParams = getUrlParameter('expand');
			}
			else{
				var urlParams = (new URL(document.location)).searchParams;
			}
			if(urlParams && urlParams.has('expand') == true && urlParams.get('expand')== 'changePassword') // true
			{
				$('#ui-id-2').find('.ui-icon-triangle-1-e').addClass('ui-icon-triangle-1-s').click();
			} else if(urlParams && urlParams.has('expand') == true && (urlParams.get('expand')== 'updateEmail'  || urlParams.get('expand')== 'updateProfile')) // true
			{
				$('#myProfileDetailsSection').find('.ui-icon-triangle-1-e').addClass('ui-icon-triangle-1-s').click();
				$('#ui-id-1').find('.ui-icon-triangle-1-e').addClass('ui-icon-triangle-1-s').click();
			} else {
				$('#myProfileDetailsSection').find('.ui-icon-triangle-1-e').addClass('ui-icon-triangle-1-s').click();
			}
		});
	},

	enableDisableSubscriptions : function() {

		$(document).on('change', '#serviceNoticesOptedIn', function () {
			var val = $(this).val();
			if (val == 'true') {
				var routeCodeLength = $('input[name=routeCodes]').length;
				for (var i = 1; i <= routeCodeLength; i++) {
					$("#routeCodes" + i).remove("disabled");
				}
			} else {
				var routeCodeLength = $('input[name=routeCodes]').length;
				for (var i = 1; i <= routeCodeLength; i++) {
					$("#routeCodes" + i).attr("disabled", "disabled");
				}
			}
		});

		$(document).on('change', '#businessOpportunitiesOptedIn', function () {
			var val = $(this).val();
			if (val == 'true') {
				$("#businessOpportunitiesSubscriptionsForm input[type='checkbox']").remove("disabled");
				$('.jsCompanyName').prop("disabled", false);
				$('.jsJobTitle').prop("disabled", false);
			} else {
				$('.jsCompanyName').prop("disabled", true);
				$('.jsJobTitle').prop("disabled", true);
				$("#businessOpportunitiesSubscriptionsForm input[type='checkbox']").attr("disabled", "disabled");
			}
		});
	},

	enableDisableSubscriptionsForASM: function () {
		var isAsmUser = $("#isAsmUser").text();
		if (isAsmUser == 'true') {
			var routeCodeLength = $('input[name=routeCodes]').length;
			for (var i = 1; i <= routeCodeLength; i++) {
				var isCheck = $("#routeCodes" + i).attr('checked');
				if ($("#routeCodes" + i).attr('checked') != 'checked') {
					$("#routeCodes" + i).attr("disabled", "disabled");
				} else {
					$("#routeCodes" + i).removeAttr("disabled");
				}
			}

			$(".serviceNoticesYes").attr("disabled", "disabled");
			$("#newsReleaseYes").attr("disabled", "disabled");
			$("#offersAndPromotionsYes").attr("disabled", "disabled");
			$("#businessOpportunitiesOptedInYes").attr("disabled", "disabled");
		}
	},

	/**
	 * Hide/show the inbound & outbound trip accordion
	 */
	bindCollapseOutboundInbound: function() {
		$(".y_journey_collapse_btn").on('click', function() {
			var buttonTarget = $(this).attr('data-target').split("-");
			var sectionNo = buttonTarget[buttonTarget.length - 1];
			var passSummary = $("#passenger-summary-section-" + sectionNo);
			var ancillariesShared = $("#ancillary-shared-section-" + sectionNo);
			if ($(this).hasClass('collapsed')) {
				$(passSummary).collapse('show');
				$(ancillariesShared).collapse('show');
			} else {
				$(passSummary).collapse('hide');
				$(ancillariesShared).collapse('hide');
			}
		});
	},

	/**
	 * add date picker to Date of Birth fields
	 */
	initializeDobExpiryDatePickers: function() {
		var dateToday = new Date();

		$('#y_dob_datepicker').datepicker({
			changeYear: true,
			changeMonth: true,
			yearRange: (dateToday.getFullYear() - 100) + ":" + dateToday.getFullYear()
		});

		$('#y_expirydate_datepicker').datepicker({
			changeYear: true,
			changeMonth: true,
			yearRange: dateToday.getFullYear() + ":" + (dateToday.getFullYear() + 10)
		});
	},

	/**
	 * validation for update personal details form
	 */
	bindingUpdatePersonalDetailsSave: function() {
		$("input[name='firstName']").attr('maxLength','35');
		$("input[name='lastName']").attr('maxLength','35');

		$('#bcfUpdateProfileForm').validate({
			errorElement: "span",
			errorClass: "fe-error",
			ignore: ".fe-dont-validate",
			onfocusout: function (element) {
				$(element).valid();
			},
			rules: {
				firstName: {
					required: true,
					nameValidation: true
				},
				lastName: {
					required: true,
					nameValidation: true
				}
			},
			messages: {
				firstName: {
					required: ACC.validationMyaccountMessages.message('error.formvalidation.firstName'),
					nameValidation: ACC.validationMyaccountMessages.message('error.formvalidation.firstNameValid')
				},
				lastName: {
					required: ACC.validationMyaccountMessages.message('error.formvalidation.lastName'),
					nameValidation: ACC.validationMyaccountMessages.message('error.formvalidation.lastNameValid')
				}
			}
		});
	},
	/**
	 * validation for update Password details form
	 */
	bindingUpdatepasswordlDetailsSave: function() {
		$("#updatePasswordForm").validate({
			errorElement: "span",
			errorClass: "fe-error",
			onfocusout: function (element) {
				$(element).valid();
			},
			onkeyup: false,
			onclick: false
		});

		if ($("#updatePasswordForm").find("#currentPassword").length > 0) {
			$("#updatePasswordForm").find("#currentPassword").rules("add", {
				required: true,
				messages: {
					required: ACC.pwdEmptyValidationMsg
				}
			});
		}

		if ($("#updatePasswordForm").find("#newPassword").length > 0) {

			$.validator.addMethod("passwordPatternCheck", function (value, element) {

				var password = $(element).val();

				var ATLEAST_ONE_NUMBER_REGEX = new RegExp(/[0-9]/);
				var ATLEAST_ONE_UPPERCASE_LETTER_REGEX = new RegExp(/[A-Z]/);
				var ATLEAST_ONE_LOWERCASE_LETTER_REGEX = new RegExp(/[a-z]/);
				var ATLEAST_ONE_SPECIAL_SYMBOL_REGEX = new RegExp(/[^A-Za-z0-9]/);

				if (password.indexOf(" ") !== -1) {
					return false;
				}

				var count = 0;
				if (password.match(ATLEAST_ONE_UPPERCASE_LETTER_REGEX)) {
					count++;
				}
				if (password.match(ATLEAST_ONE_LOWERCASE_LETTER_REGEX)) {
					count++;
				}

				if (password.match(ATLEAST_ONE_NUMBER_REGEX)) {
					count++;
				}

				if (password.match(ATLEAST_ONE_SPECIAL_SYMBOL_REGEX)) {
					count++;
				}

				if (count > 2) {
					return true;
				}
				return false;
			}, "passwordPatternCheck");
			$("#updatePasswordForm").find("#newPassword").rules("add", {
				required: true,
				minlength: ACC.pwdStrengthMinChar,
				maxlength: ACC.pwdStrengthMaxChar,
				passwordPatternCheck: true,

				messages: {
					required: "Password not valid",
					minlength: ACC.pwdMinCharValidationMsg,
					maxlength: ACC.pwdMaxCharValidationMsg,
					passwordPatternCheck: ACC.pwdPatternValidationMsg
				}
			});
		}
		if ($("#updatePasswordForm").find("#checkNewPassword").length > 0) {
			$("#updatePasswordForm").find("#checkNewPassword").rules("add", {
				required: true,
				equalTo: "#newPassword",
				messages: {
					required: ACC.reconfirmPasswordValidationMsg
				}
			});
		}

	},

	bindConfirmDeleteBooking: function(){
		$('#confirm-delete').on('show.bs.modal', function(e) {
			$(this).find('.btn-ok').attr('href', $(e.relatedTarget).data('href'));
		});
	},

	bindMyAccountMyBookingsShowMore: function(){
		$(".y_myAccountMyBookingsShowMore").on('click', function () {
			var pageSize = $(this).data('pagesize');
			$(".y_myBookings li:hidden").slice(0, pageSize).show();
			if ($(".y_myBookings li:hidden").length == 0) {
				$(".y_myAccountMyBookingsShowMore").hide();
			}
			return false;
		});
	},

	/**
	 * disable the 'Remove from this list' until page fully rendered to prevent modal being opened before colorbox fix is loaded.
	 */
	bindRemoveBooking: function () {
		$(".y_removeBooking").removeClass("disabled");
	},

	showAddBusinessModalAlert: function () {
		$(".addBusinessAccountLink").on("click", function () {
			$(".addBusinessAccountLink").removeAttr("href");
			$("#addBusinessAccountLink").modal('show');
		});
	},
	logoutBtnhide: function(){

		$("#logoutMyAccount").parent("#accountBottomLink").removeClass("button");
		$("#logoutMyAccount").parent("#accountBottomLink").addClass("btn-outline-blue");

	},

	showModalAlert: function () {
		$(".unsubscribeLink").on("click", function () {
			$("unsubscribeLink").removeAttr("href");
			$("#y_unsubscribeLinkModal").modal('show');
		})
	}
};

$(document).ready(function(){
	function businessOpportunitiesOptedIn(){
		var selected = $("input:radio[name=businessOpportunitiesOptedIn]:checked").val();
		if(selected === "false"){
			$("input[type='checkbox']").prop('checked', false);
			$("input[type='checkbox']").attr('disabled', 'disabled');
			$("input[type='text']").attr('disabled', 'disabled');
		}
		else{
			$("input[type='checkbox']").removeAttr('disabled');
			$("input[type='text']").removeAttr('disabled');
		}
	}

	$("input:radio[name=businessOpportunitiesOptedIn]").on("change", function () {
		businessOpportunitiesOptedIn();
	});
	businessOpportunitiesOptedIn();


	function serviceNoticesOptedIn() {
		var selected = $("input:radio[name=serviceNoticesOptedIn]:checked").val();
		if(selected === "false"){
			$("input[type='checkbox']").prop('checked', false);
			$("input[type='checkbox']").attr('disabled', 'disabled');
			$("input[type='text']").attr('disabled', 'disabled');

		}
		else{
			$("input[type='checkbox']").removeAttr('disabled');
			$("input[type='text']").removeAttr('disabled');
		}
	}

	$("input:radio[name=serviceNoticesOptedIn]").on("change", function () {
		serviceNoticesOptedIn();
	});
	serviceNoticesOptedIn();
});

