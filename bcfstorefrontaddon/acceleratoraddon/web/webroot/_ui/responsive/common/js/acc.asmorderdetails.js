ACC.asmorderdetails = {
	_autoloadTracc: [
		"bindLockOrUnlockOrderAction",
		"bindApproveOrderModalDecisions",
		"bindRejectOrderModalDecisions",
		"bindApproveOrderEntryModalDecisions",
		"bindRejectOrderEntryModalDecisions"
	],

    bindLockOrUnlockOrderAction : function()
    {
        $('#y_lockOrder, #y_unLockOrder').on('click', function(){
            var form = $(this).closest('form#y_agentPendingOrderApprovalStatusForm');
            if($(this).hasClass('lockOrder'))
            {
                $("input[name='orderAction']",form).val('LOCKED');
            }
            else if($(this).hasClass('unLockOrder'))
            {
                $("input[name='orderAction']",form).val('UNLOCKED');
            }
            form.submit();
        });
    },

    bindApproveOrderModalDecisions : function()
    {
        $("#y_approveOrder").on("click",function(event){
            event.preventDefault();
            $("#y_onHoldOrderConfirmModal").modal("show");
            $("#y_onHoldOrderConfirmModal .y_onHoldOrderApprovalConfirmBtn").attr("data-elementid", $(this).attr("id"));
            ACC.asmorderdetails.bindApproveOrRejectOrderAction();
        });

        $(".onHoldOrderApprovalRejectBtn").on("click",function(event){
            event.preventDefault();
            $("#y_onHoldOrderConfirmModal").modal("hide");
        });
    },

    bindRejectOrderModalDecisions : function()
    {
        $("#y_rejectOrder").on("click",function(event){
            event.preventDefault();
            $("#y_onHoldOrderRejectModal").modal("show");
            $("#y_onHoldOrderRejectModal .y_onHoldOrderRejectionConfirmBtn").attr("data-elementid", $(this).attr("id"));
            ACC.asmorderdetails.bindApproveOrRejectOrderAction();
        });

        $(".onHoldOrderRejectionDeniedBtn").on("click",function(event){
            event.preventDefault();
            $("#y_onHoldOrderRejectModal").modal("hide");
        });
    },

    bindApproveOrRejectOrderAction : function()
    {
        $('.y_onHoldOrderApprovalConfirmBtn, .y_onHoldOrderRejectionConfirmBtn').on('click', function(event){
            event.preventDefault();

            var clickedElementId = $(this).attr("data-elementid");
            var element = $("#"+clickedElementId);
            var form = element.closest('form#y_agentPendingOrderApprovalStatusForm');
            if(element.hasClass('approve'))
            {
                $("input[name='orderAction']",form).val('APPROVED');
            }
            else if(element.hasClass('reject'))
            {
                $("input[name='orderAction']",form).val('REJECTED');
            }
            form.submit();

            $("#y_onHoldOrderConfirmModal").modal("hide");
            $("#y_onHoldOrderRejectModal").modal("hide");
        });
    },

    bindApproveOrderEntryModalDecisions : function()
    {
        $(".approveEntryBtn").on("click", function(event) {
            event.preventDefault();

            $("#y_onHoldOrderEntryConfirmModal").modal("show");
            $("#y_onHoldOrderEntryConfirmModal .y_onHoldOrderEntryApproveConfirmBtn").attr("data-elementid", $(this).attr("id"));
            ACC.asmorderdetails.bindIndividualEntryAction();
        });

        $(".onHoldOrderEntryApproveRejectBtn").on("click",function(event){
            event.preventDefault();
            $("#y_onHoldOrderEntryConfirmModal").modal("hide");
        });
    },

    bindRejectOrderEntryModalDecisions : function()
    {
        $(".rejectEntryBtn").on("click", function(event) {
            event.preventDefault();
            $("#y_onHoldOrderEntryRejectModal").modal("show");
            $("#y_onHoldOrderEntryRejectModal .y_onHoldOrderEntryRejectionConfirmBtn").attr("data-elementid", $(this).attr("id"));
            ACC.asmorderdetails.bindIndividualEntryAction();
        });

        $(".onHoldOrderEntryRejectionDeniedBtn").on("click",function(event){
            event.preventDefault();
            $("#y_onHoldOrderEntryRejectModal").modal("hide");
        });
    },

    bindIndividualEntryAction : function()
    {
        $('.y_onHoldOrderEntryApproveConfirmBtn, .y_onHoldOrderEntryRejectionConfirmBtn').on('click', function(event){
            event.preventDefault();

            var clickedElementId = $(this).attr("data-elementid");
            var element = $("#"+clickedElementId);
            var form = element.closest('form#y_agentPendingOrderDecisionForm');
            var entryNumber;
            if(element.hasClass('approveActivityEntry'))
            {
                entryNumber = element.closest("tr").find('input.activityEntry').val();
                $("input[name='buttonClicked']",form).val('APPROVED');
            }
            else if(element.hasClass('rejectActivityEntry'))
            {
                entryNumber = element.closest("tr").find('input.activityEntry').val();
                $("input[name='buttonClicked']",form).val('REJECTED');
            }
            else if(element.hasClass('approveAccommodationEntry'))
            {
                entryNumber = element.closest("tr").find('input.accommodationEntry').val();
                $("input[name='buttonClicked']",form).val('APPROVED');
            }
            else if(element.hasClass('rejectAccommodationEntry'))
            {
                entryNumber = element.closest("div.accommodationContent").find('input.accommodationEntry').val();
                $("input[name='buttonClicked']",form).val('REJECTED');
            }
            $("input[name='redirectEntry']",form).val(true);
            $("input[name='entryNumber']",form).val(entryNumber);
            $("input[name='comments']",form).val(element.closest("tr").find(".comment").val());
            form.submit();

            $("#y_onHoldOrderEntryRejectModal").modal("hide");
            $("#y_onHoldOrderEntryConfirmModal").modal("hide");

        });
    }


};
