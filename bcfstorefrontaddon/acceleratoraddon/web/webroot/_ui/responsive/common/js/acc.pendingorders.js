ACC.pendingorders = {

	_autoloadTracc: [
		"bindLockOrderButton",
		"bindApproveOrderButton",
		"bindRejectOrderButton",
		"bindUnlockOrderButton"
	],

	bindLockOrderButton: function () {
		$(document).on('click', '.y_lockOrderButton', function (event) {
			event.preventDefault();
			var selectedButton = $(this);
			var url = selectedButton.attr('href');
			$.when(ACC.services.performActionOnOrderAjax(url)).then(
				function (data) {
				    selectedButton.closest(".pendingOrdersContentRow").find(".lockedBy").text(data.message);
                    selectedButton.closest(".pendingOrdersContentRow").find(".lockBtn .y_lockOrderButton").addClass("hide");
					$('#y_asmOrderActionMessage').replaceWith("<h4>" + data.message + "</h4>");;
					$('#y_asmOrderActionMessageModal').modal('show');
				}
			);
		});
	},
	bindUnlockOrderButton: function () {
		$(document).on('click', '.y_unlockOrderButton', function (event) {
			event.preventDefault();
			var selectedButton = $(this);
			var url = selectedButton.attr('href');
			$.when(ACC.services.performActionOnOrderAjax(url)).then(
				function (data) {
				    selectedButton.closest(".pendingOrdersContentRow").find(".lockedBy").text("");
				    selectedButton.closest(".pendingOrdersContentRow").find(".lockBtn .y_lockOrderButton").removeClass("hide");
					$('#y_asmOrderActionMessage').replaceWith("<h4>" + data.message + "</h4>");;
					$('#y_asmOrderActionMessageModal').modal('show');
				}
			);
		});
	},
	bindApproveOrderButton: function () {
		$(document).on('click', '.y_approveOrderButton', function (event) {
			event.preventDefault();
			var url = $(this).attr('href');
			$.when(ACC.services.performActionOnOrderAjax(url)).then(
				function (data) {
					$('#y_asmOrderActionMessage').replaceWith("<h4>" + data.message + "</h4>");;
					$('#y_asmOrderActionMessageModal').modal('show');
				}
			);
		});
	},
	bindRejectOrderButton: function () {
		$(document).on('click', '.y_rejectOrderButton', function (event) {
			event.preventDefault();
			var url = $(this).attr('href');
			$.when(ACC.services.performActionOnOrderAjax(url)).then(
				function (data) {
					$('#y_asmOrderActionMessage').replaceWith("<h4>" + data.message + "</h4>");;
					$('#y_asmOrderActionMessageModal').modal('show');
				}
			);
		});
	}
};
