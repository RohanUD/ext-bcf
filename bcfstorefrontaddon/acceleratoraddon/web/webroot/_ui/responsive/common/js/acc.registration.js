/**
 * The module for registration pages.
 * @namespace
 */
ACC.registrationForm = {

    _autoloadTracc : [
        "bindRegistrationValidationMessages",
        "bindingRegistrationValidaiton"
    ],

    /**
     * validation for registration form
     */
    bindRegistrationValidationMessages : function () {
        ACC.RegistrationValidationMessages = new validationMessages("ferry-RegistrationValidationMessages");
        ACC.RegistrationValidationMessages.getMessages("error");
    },
    bindingRegistrationValidaiton : function() {
        $('#registerForm').validate({
            errorElement: "span",
            errorClass: "fe-error",
            ignore: ".fe-dont-validate",
            onfocusout: function(element) {
                $(element).valid();
            },
            rules: {
                firstName: {
                    required: true,
                    nameValidation: true
                },
                lastName: {
                    required: true,
                    nameValidation: true
                },
                mobileNumber: {
                    required: true,
                    validatePhoneNumberPattern: true
                },
                zipCode: {
                    required: true,
                    validatePhoneNumberPattern: false
                }
            },
            messages: {
                firstName: {
                    required: ACC.RegistrationValidationMessages.message('error.formvalidation.firstName'),
                    nameValidation: ACC.RegistrationValidationMessages.message('error.formvalidation.firstNameValid')
                },
                lastName: {
                    required: ACC.RegistrationValidationMessages.message('error.formvalidation.lastName'),
                    nameValidation: ACC.RegistrationValidationMessages.message('error.formvalidation.lastNameValid')
                },
                mobileNumber: {
                    required: ACC.RegistrationValidationMessages.message('error.formvalidation.mobileNumber'),
                    validatePhoneNumberPattern: ACC.RegistrationValidationMessages.message('error.lead.guest.phone.invalid')
                }
            }
        });
    }
};


