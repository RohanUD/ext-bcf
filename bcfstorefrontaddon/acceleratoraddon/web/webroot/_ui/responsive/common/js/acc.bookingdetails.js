ACC.bookingdetails = {

    _autoloadTracc : [
        "bindCancelBookingButton",
        "bindCancelTransportBookingButton",
        "bindCancelAccommodationBookingButton",
        "bindConfirmCancelBookingButton",
        "enableCancelButtons",
        "bindAddRoomButton",
        "bindRoomQuantity",
        "bindReplanJourneyButton",
        "bindCancelSailingButton",
        "bindFareFinderValidation",
        "bindCancelSailingConfirmButton",
        "validationMesagesInit",
        "redirectToHomePage",
        "bindShareItineraryEvents",
        "shareItineraryValidation",
        "bindPrintReceiptLink"
    ],

    componentParentSelector: '#y_fareFinderFormManageBooking',

    bindFareFinderValidation: function () {

    	$(ACC.bookingdetails.componentParentSelector).validate({
            errorElement: "span",
            errorClass: "fe-error",
            ignore: ".fe-dont-validate",
            submitHandler: function (form) {
            	form.submit();
            },
    	 rules: {
             arrivalLocationName: {
                 required: {
	           			 depends:function(){
	        				 $(this).val($.trim($(this).val()));
	        		            return true;
	        			 }
	        		},
                 locationIsValid : ".y_fareFinderDestinationLocationCode"
             }
         },
         messages: {
             arrivalLocationName: {
                 required: ACC.addons.bcfstorefrontaddon['error.farefinder.to.location'],
                 locationIsValid: ACC.addons.bcfstorefrontaddon['error.farefinder.to.locationExists']
             }
         }
        });
    },
    validationMesagesInit : function(){
      if(ACC.validationMessages ==undefined){
        ACC.validationMessages={};
      }
      ACC.validationMessages.bookingdetails = new validationMessages("ferry-bookingDetailsForm");
      ACC.validationMessages.bookingdetails.getMessages("error");
    },
    bindShareItineraryEvents: function(){
        $("#share-itinerary-email").click(function(){
            if($("#form-share-itinerary-email").find("#share-itinerary-email-success")){
                $("#share-itinerary-email-success").hide();
                $("#share-itinerary-email-error").hide();
            }
        });
    },
    shareItineraryValidation: function(){

      $("#form-share-itinerary-email").validate({
          errorElement: "span",
          errorClass: "fe-error",
          onfocusout: false,
          submitHandler: function(form) {
            var email_list = $("#share-itinerary-email").val();
            var ebookingId = $("#ebookingId").val();
                $.when(ACC.services.shareItineraryToEmail(email_list,ebookingId)).then(
                      function(response) {
                            if(response) {
                                $('#share-itinerary-email').val('');
                                if($("#form-share-itinerary-email").find("#share-itinerary-email-success").length == 0){
                                    $('#share-itinerary-email').parent().append('<span id="share-itinerary-email-success"> Itinerary shared successfully.</span>')
                                }
                                else {
                                    $("#share-itinerary-email-success").show();
                                }
                            } else {
                                if($("#form-share-itinerary-email").find("#share-itinerary-email-success")){
                                    $('#share-itinerary-email-success').hide();
                                }
                            }
                      }
                );
          },

          onkeyup: false,
          onclick: false
      });
      $.validator.addMethod("mutipleEmail", function(value, element) {

                              var emailRegex = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
                              var reEmail = new RegExp(emailRegex);
                              var emailList = $(element).val();
                              emailList = emailList.split(",");
                              var emailCheck = true;
                                  _.forEach(emailList,function(val)
                                  {
                                     val=val.trim();
                                     if(!val.match(reEmail))
                                     {
                                       emailCheck = false;
                                     }
                                  })
                              return emailCheck;
      }, ACC.validationMessages.bookingdetails.message("error.booking.confirmation.email"));

      if($("#share-itinerary-email").length !=0){
        $("#share-itinerary-email").rules("add", {
            required: true,
            mutipleEmail:true,
            messages: {
                required: ACC.validationMessages.bookingdetails.message("error.booking.confirmation.email"),
                email: ACC.validationMessages.bookingdetails.message("error.booking.confirmation.email")
            }
        });
      }

    },

	sendCancelBookingRequest : function(cancelRequestUrl, cancelBookingUrl) {
		var orderCode = $("#bookingReference").val();
		$.when(ACC.services.cancelBookingRequest(cancelRequestUrl, orderCode)).then(
				function(data) {
					var jsonData = JSON.parse(data);
					if (jsonData.isCancelPossible) {
						$(".y_cancelBookingConfirm").html(jsonData.cancelBookingModalHtml);
						$(".y_cancelOrderForm").attr('action', cancelBookingUrl);
						$(".y_cancelOrderForm").find("input[name=orderCode]").val(orderCode);
						$("#y_cancelBookingModal").modal();
						ACC.bookingdetails.bindCancelSailingConfirmButton();
					} else {
						$(".y_cancellationResult").removeClass("alert-success");
						$(".y_cancellationResult").addClass("alert-danger");
						$(".y_cancellationResult").show();
						$(".y_cancellationResultContent").html(jsonData.errorMessage)
						$(".y_cancellationRefundResultContent").html("");
					}
				});
	},

    redirectToHomePage: function () {
        $(".y_addAnotherSailing,.y_home").click(function () {
            window.location = ACC.config.contextPath;
        });
    },

	bindCancelBookingButton : function() {
		$(document).on('click', '.y_cancelBookingButton', function(event) {
			event.preventDefault();
			var cancelBookingUrl = $(this).attr('href');
			ACC.bookingdetails.sendCancelBookingRequest("/manage-booking/cancel-booking-request/", cancelBookingUrl);
		});
	},

	bindReplanJourneyButton: function () {
        $(document).on('click', '.y_replanJourneyButton', function (event) {
            event.preventDefault();
            var replanJourneyUrl = $(this).attr('href');
            var journeyRefNum = $(this).closest('div.journey-wrapper').find("#journeyReferenceNumber").val();
            var odRefNum = $(this).closest('div.journey-wrapper').find("#originDestinationRefNumber").val();
            ACC.bookingdetails.sendReplanJourneyRequest(replanJourneyUrl, journeyRefNum, odRefNum);
        });
    },

    sendReplanJourneyRequest: function (replanJourneyUrl, journeyRefNum, odRefNum) {
        var orderCode = $("#bookingReference").val();
        $.when(ACC.services.sendReplanJourneyRequest(orderCode, journeyRefNum, odRefNum)).then(
            function (data) {
                var jsonData = JSON.parse(data);
                $(".y_replanJourneySection").html(jsonData.replanJourneyModalHtml);
                var todayDate = new Date();
                $('.y_fareFinderDatePickerDeparting').datepicker({
                	minDate: todayDate,
                    maxDate: '+1y',
                     dateFormat: 'mm/dd/yy'
                });
                ACC.bookingdetails.bindFareFinderValidation();
                $("#y_replanJourneyModal").modal();
            });
        $(':input[type="submit"]').prop('disabled', true);
		var $form = $('form'),
		origForm = $form.serialize();
		$('form :input').on('change input', function() {
			var isFormChanged = ($form.serialize() !== origForm);
			if(isFormChanged){
				$(':input[type="submit"]').prop('disabled', false);
			}
			else{
				$(':input[type="submit"]').prop('disabled', true);
			}
		});
    },

	bindCancelSailingButton: function () {
        $(document).on('click', '.y_cancelSailingButton', function (event) {
            event.preventDefault();
            var cancelSailingUrl = $(this).attr('href');
            var journeyRefNum = $(this).closest('div.journey-wrapper').find("#journeyReferenceNumber").val();
            var odRefNum = $(this).closest('div.journey-wrapper').find("#originDestinationRefNumber").val();
            ACC.bookingdetails.sendCancelSailingRequest(cancelSailingUrl, journeyRefNum, odRefNum);
        });
    },

    bindCancelSailingConfirmButton : function()
    {
    	$('#y_cancelBookingUrl').on('click', function (event) {
    		event.preventDefault();
    		$('.y_cancelOrderForm').submit();
    	});
    },

    sendCancelSailingRequest : function(cancelSailingUrl, journeyRefNum, odRefNum) {
		var orderCode = $("#bookingReference").val();
		$.when(ACC.services.cancelSailingRequest(orderCode, journeyRefNum, odRefNum)).then(
			function(data) {
				var jsonData = JSON.parse(data);
				if (jsonData.isCancelPossible) {
					$(".y_cancelBookingConfirm").html(jsonData.cancelBookingModalHtml);
					$(".y_cancelOrderForm").attr('action', cancelSailingUrl);
					$(".y_cancelOrderForm").find("input[name=orderCode]").val(orderCode);
					$(".y_cancelOrderForm").find("input[name=journeyRefNum]").val(journeyRefNum);
					$(".y_cancelOrderForm").find("input[name=odRefNum]").val(odRefNum);
					$("#y_cancelBookingModal").modal();
					ACC.bookingdetails.bindCancelSailingConfirmButton();
				} else {
					$(".y_cancellationResult").removeClass("alert-success");
					$(".y_cancellationResult").addClass("alert-danger");
					$(".y_cancellationResult").show();
					$(".y_cancellationResultContent").html(jsonData.errorMessage)
					$(".y_cancellationRefundResultContent").html("");
				}
			}
		);
	},

	bindCancelTransportBookingButton : function() {
		$(document).on('click', '.y_cancelTransportBookingButton', function(event) {
			event.preventDefault();
			var cancelBookingUrl = $(this).attr('href');
			ACC.bookingdetails.sendCancelBookingRequest("/manage-booking/cancel-transport-order-request/", cancelBookingUrl);
		});
	},

	bindCancelAccommodationBookingButton : function() {
		$(document).on('click', '.y_cancelAccommodationBookingButton', function(event) {
			event.preventDefault();
			var cancelBookingUrl = $(this).attr('href');
			ACC.bookingdetails.sendCancelBookingRequest("/manage-booking/cancel-accommodation-order-request/", cancelBookingUrl);
		});
	},

	bindConfirmCancelBookingButton : function() {
	    $(document).on('click', '#y_cancelBookingUrl', function(event) {
	        $(this).attr('disabled', 'disabled');
	    });
	},

	bindAddRoomButton : function() {
		$('.y_addRoom').on("click", function() {
			var orderCode=$("#bookingReference").val();
			$.when( ACC.services.getAddRoomOptions(orderCode)).then(
					function(response){
						if(response.valid){
							$('#y_addRoomModal').html(response.addAccommodationRoomHtmlContent);
							ACC.bookingdetails.bindRoomQuantity();
							ACC.bookingdetails.bindAddRoomContinueButton();
							$('#y_addRoomModal').modal();
						}else{
							$("#y_addRoomToPackageErrorBody").html(response.addAccommodationRoomHtmlContent);
			            	$("#y_addRoomToPackageErrorModal").modal();
						}
					}
			);
			return false;
		});
	},

	bindRoomQuantity : function() {
	       roomId = '#room',
	       hide = 'hidden';
	       $('.y_accommodationRoomQuantity').change(function() {
	           var roomQuantity = $(this).val(),i;
	           for (i = 1; i <= roomQuantity; i++) {
	               $(roomId+'\\['+i+'\\]').removeClass(hide);
	           };
	           $(roomId+'\\['+(i-1)+'\\]').nextUntil('.age-info').addClass(hide);
	       });
	},

	enableCancelButtons: function() {
	    $(".y_cancelAccommodationBookingButton").removeAttr("disabled");
	    $(".y_cancelTransportBookingButton").removeAttr("disabled");
	    $(".y_cancelBookingButton").removeAttr("disabled");
	},

	bindAddRoomContinueButton:function(){
		$('#y_addRoomContinue').on("click", function() {
			var $form = $("#y_addRoomForm");
		     var orderCode=$("#bookingReference").val();
		     $.when(ACC.services.validateAddRoomAccommodationFinderForm($form.serialize(),orderCode)).then(
		          function(response){
		            if (response.valid) {
		                      $form.submit();
		                  }else{
		                    $("#y_roomStayCandidatesError").html(response.errorMsg);
		                    $("#y_roomStayCandidatesError").show();
		                  }
		          }
		      );
		});
	},

	bindPrintReceiptLink : function() {
		$("#print-receipt-link").click(function(e) {
			e.preventDefault();
			var url = $(this).attr('href');
			window.open(url, '_blank');
		});
	}
};
