/** 
 * The module for registration pages. 
 * @namespace 
 */ 
 ACC.addressForm = { 
 
  _autoloadTracc : [ 
        "bindAddressValidationMessages",
        "bindingAddressValidaiton" 
  ], 
 
    /** 
     * validation for registration form 
     */ 
    bindAddressValidationMessages : function () {
        ACC.AddressValidationMessages = new validationMessages("ferry-bindAddressValidationMessages");
        ACC.AddressValidationMessages.getMessages("error");
    },
  bindingAddressValidaiton : function() { 
      $('#addressForm').validate({ 
            errorElement: "span", 
            errorClass: "fe-error", 
            ignore: ".fe-dont-validate", 
            onfocusout: function(element) { 
                $(element).valid(); 
            }, 
            rules: { 
                firstName: { 
                    required: true, 
                    nameValidation: true 
                }, 
                lastName: { 
                    required: true, 
                    nameValidation: true 
                }, 
                mobileNumber: { 
                    required: true, 
                    validatePhoneNumberPattern: true 
                } 
            }, 
            messages: { 
                firstName: { 
                    required: ACC.AddressValidationMessages.message('error.formvalidation.firstName'), 
                    nameValidation: ACC.AddressValidationMessages.message('error.formvalidation.firstNameValid') 
                }, 
                lastName: { 
                    required: ACC.AddressValidationMessages.message('error.formvalidation.lastName'), 
                    nameValidation: ACC.AddressValidationMessages.message('error.formvalidation.lastNameValid') 
                }
            } 
        }); 
    } 
 
}; 
