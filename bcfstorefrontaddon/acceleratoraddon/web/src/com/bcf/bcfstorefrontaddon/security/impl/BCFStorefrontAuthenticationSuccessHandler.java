/*
 * Copyright (c) 2019 British Columbia Ferry Services Inc
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of British Columbia Ferry Services Inc("Confidential Information").
 * You shall not disclose such Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with British Columbia Ferry Services Inc.
 *
 * Last Modified: 06/06/19 20:21
 */

package com.bcf.bcfstorefrontaddon.security.impl;

import de.hybris.platform.acceleratorstorefrontcommons.constants.WebConstants;
import de.hybris.platform.acceleratorstorefrontcommons.security.StorefrontAuthenticationSuccessHandler;
import de.hybris.platform.commercefacades.user.data.CustomerData;
import de.hybris.platform.servicelayer.session.SessionService;
import de.hybris.platform.servicelayer.user.UserService;
import java.io.IOException;
import java.util.Objects;
import java.util.Set;
import javax.annotation.Resource;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.lang3.BooleanUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Required;
import org.springframework.security.core.Authentication;
import org.springframework.security.web.savedrequest.HttpSessionRequestCache;
import org.springframework.security.web.savedrequest.SavedRequest;
import com.bcf.core.constants.BcfCoreConstants;
import com.bcf.core.service.BCFTravelCartService;
import com.bcf.facades.ebooking.ModifyBookingIntegrationFacade;
import com.bcf.integration.data.MakeBookingRequestData;
import com.bcf.travelb2bfacades.checkout.BcfTravelb2bCheckoutFacade;


public class BCFStorefrontAuthenticationSuccessHandler extends StorefrontAuthenticationSuccessHandler
{
	private static final String ACTIVATE_PROFILE = "/activate-profile";
	private static final String CHECKOUT_URL = "/checkout";
	private static final String CART_URL = "/cart";
	private static final String IS_B2B_CUSTOMER = "isB2bCustomer";
	public static final String IS_LOGIN_VIA_BOOKING_FLOW = "isLoginViaBookingFlow";

	private ModifyBookingIntegrationFacade modifyBookingIntegrationFacade;
	private BCFTravelCartService bcfTravelCartService;
	private SessionService sessionService;
	private BcfTravelb2bCheckoutFacade bcfTravelb2bCheckoutFacade;

	@Resource(name = "httpSessionRequestCache")
	private HttpSessionRequestCache httpSessionRequestCache;

	@Resource(name = "bookingFlowUrlSet")
	private Set<String> bookingFlowUrlSet;

	@Resource(name = "userService")
	private UserService userService;

	@Override
	public void onAuthenticationSuccess(final HttpServletRequest request, final HttpServletResponse response,
			final Authentication authentication) throws IOException, ServletException
	{
		final boolean isLoginViaBookingFlow = isLoginViaBookingFlow(request, response);
		if (isLoginViaBookingFlow)
		{
			sessionService.setAttribute(IS_LOGIN_VIA_BOOKING_FLOW, isLoginViaBookingFlow);
		}

		String convertFailed = String.valueOf(false);
		if (getBcfTravelCartService().hasCacheEntriesToConvert() && Objects
				.isNull(getModifyBookingIntegrationFacade().getModifyBookingResponseForConvertBookingToOption(
						new MakeBookingRequestData(), BcfCoreConstants.CONVERT_QUOTATION_TO_OPTION_REQUEST)))
		{
			convertFailed = String.valueOf(true);
		}
		sessionService.setAttribute(BcfCoreConstants.CONVERT_QUOTATION_TO_OPTION_FAILED, convertFailed);

		final SavedRequest sr = httpSessionRequestCache.getRequest(request, response);
		if (Objects.nonNull(sr) && StringUtils.isNotBlank(sr.getRedirectUrl()))
		{
			sessionService.setAttribute(BcfCoreConstants.REFERER_REDIRECT, sr.getRedirectUrl());
		}

		super.onAuthenticationSuccess(request, response, authentication);

		if (getBcfTravelCartService().hasSessionCart() && bcfTravelb2bCheckoutFacade.getB2BCheckoutContextData() != null
				&& bcfTravelb2bCheckoutFacade.getB2BCheckoutContextData().getB2bUnit() != null)
		{
			getBcfTravelCartService().getSessionCart().setUnit(bcfTravelb2bCheckoutFacade.getB2BCheckoutContextData().getB2bUnit());
		}

		if (null != bcfTravelb2bCheckoutFacade.getB2BCheckoutContextData() && null != bcfTravelb2bCheckoutFacade
				.getB2BCheckoutContextData().getB2bUnit())
		{
			getSessionService().setAttribute(IS_B2B_CUSTOMER, true);
		}
	}

	@Override
	protected String determineTargetUrl(final HttpServletRequest request, final HttpServletResponse response)
	{
		final CustomerData customerData = getCustomerFacade().getCurrentCustomer();
		if (StringUtils.isEmpty(customerData.getAccountType()))
		{
			return ACTIVATE_PROFILE;
		}

		String targetUrl = super.determineTargetUrl(request, response);

		if (CollectionUtils.isNotEmpty(getRestrictedPages()))
		{
			for (final String restrictedPage : getRestrictedPages())
			{
				// When logging in from a restricted page, return user to default target url.
				if (targetUrl.contains(restrictedPage))
				{
					targetUrl = super.getDefaultTargetUrl();
				}
			}
		}
		/*
		 * If the cart has been merged and the user logging in through checkout, redirect to the cart page to display the
		 * new cart
		 */
		if (StringUtils.equals(targetUrl, CHECKOUT_URL)
				&& BooleanUtils.toBoolean((Boolean) request.getAttribute(WebConstants.CART_MERGED)))
		{
			targetUrl = CART_URL;
		}

		return targetUrl;
	}

	@Override
	protected boolean isAlwaysUseDefaultTargetUrl()
	{
		final boolean isLoginViaBookingFlow = sessionService.getAttribute(IS_LOGIN_VIA_BOOKING_FLOW) == null ?
				false : sessionService.getAttribute(IS_LOGIN_VIA_BOOKING_FLOW);
		if (Objects.nonNull(sessionService.getAttribute(IS_LOGIN_VIA_BOOKING_FLOW)) && isLoginViaBookingFlow)
		{
			return false;
		}

		if (StringUtils.isNotEmpty(sessionService.getAttribute(BcfCoreConstants.REFERER_REDIRECT)) && !StringUtils
				.equals(sessionService.getAttribute(BcfCoreConstants.REFERER_REDIRECT), "/"))
		{
			return false;
		}

		return super.isAlwaysUseDefaultTargetUrl();
	}

	/**
	 * checks when the login button is clicked, the referer saved in the request cahce belongs to any of the booking URLs
	 *
	 * @param request
	 * @param response
	 * @return
	 */
	boolean isLoginViaBookingFlow(final HttpServletRequest request, final HttpServletResponse response)
	{
		boolean isLoginViaBookingFlow = false;
		if (CollectionUtils.isNotEmpty(bookingFlowUrlSet))
		{
			final SavedRequest sr = httpSessionRequestCache.getRequest(request, response);
			if (sr != null)
			{
				isLoginViaBookingFlow = bookingFlowUrlSet.stream()
						.anyMatch(url -> sr.getRedirectUrl().startsWith(url));
			}
		}
		return isLoginViaBookingFlow;
	}

	protected SessionService getSessionService()
	{
		return sessionService;
	}

	@Required
	public void setSessionService(final SessionService sessionService)
	{
		this.sessionService = sessionService;
	}

	protected ModifyBookingIntegrationFacade getModifyBookingIntegrationFacade()
	{
		return modifyBookingIntegrationFacade;
	}

	@Required
	public void setModifyBookingIntegrationFacade(final ModifyBookingIntegrationFacade modifyBookingIntegrationFacade)
	{
		this.modifyBookingIntegrationFacade = modifyBookingIntegrationFacade;
	}


	protected BCFTravelCartService getBcfTravelCartService()
	{
		return bcfTravelCartService;
	}

	@Required
	public void setBcfTravelCartService(final BCFTravelCartService bcfTravelCartService)
	{
		this.bcfTravelCartService = bcfTravelCartService;
	}

	public BcfTravelb2bCheckoutFacade getBcfTravelb2bCheckoutFacade()
	{
		return bcfTravelb2bCheckoutFacade;
	}

	@Required
	public void setBcfTravelb2bCheckoutFacade(final BcfTravelb2bCheckoutFacade bcfTravelb2bCheckoutFacade)
	{
		this.bcfTravelb2bCheckoutFacade = bcfTravelb2bCheckoutFacade;
	}
}
