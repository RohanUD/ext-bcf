/*
 * Copyright (c) 2019 British Columbia Ferry Services Inc
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of British Columbia Ferry Services Inc("Confidential Information").
 * You shall not disclose such Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with British Columbia Ferry Services Inc.
 *
 * Last Modified: 06/06/19 20:21
 */

package com.bcf.bcfstorefrontaddon.security.impl;

import de.hybris.platform.acceleratorstorefrontcommons.security.BruteForceAttackCounter;
import de.hybris.platform.acceleratorstorefrontcommons.security.impl.DefaultBruteForceAttackCounter;


public class DefaultBcfBruteForceAttackCounter extends DefaultBruteForceAttackCounter implements BruteForceAttackCounter
{
	public DefaultBcfBruteForceAttackCounter(final Integer maxFailedLogins, final Integer cacheExpiration,
			final Integer cacheSizeLimit)
	{
		super(maxFailedLogins, cacheExpiration, cacheSizeLimit);
	}

	@Override
	public void registerLoginFailure(final String userUid)
	{
		//do nothing
	}

	@Override
	public boolean isAttack(final String userUid)
	{
		//do nothing
		return false;
	}

	@Override
	public void resetUserCounter(final String userUid)
	{
		//do nothing
	}

	@Override
	public int getUserFailedLogins(final String userUid)
	{
		//do nothing
		return 0;
	}

}
