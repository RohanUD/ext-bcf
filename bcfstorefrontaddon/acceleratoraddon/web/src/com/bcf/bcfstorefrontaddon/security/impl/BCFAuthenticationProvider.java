/*
 * Copyright (c) 2019 British Columbia Ferry Services Inc
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of British Columbia Ferry Services Inc("Confidential Information").
 * You shall not disclose such Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with British Columbia Ferry Services Inc.
 *
 * Last Modified: 21/06/19 00:21
 */

package com.bcf.bcfstorefrontaddon.security.impl;

import de.hybris.platform.acceleratorstorefrontcommons.security.AbstractAcceleratorAuthenticationProvider;
import de.hybris.platform.b2b.constants.B2BConstants;
import de.hybris.platform.core.model.user.CustomerModel;
import de.hybris.platform.core.model.user.UserGroupModel;
import de.hybris.platform.core.model.user.UserModel;
import de.hybris.platform.jalo.JaloSession;
import de.hybris.platform.jalo.user.UserManager;
import de.hybris.platform.servicelayer.exceptions.UnknownIdentifierException;
import de.hybris.platform.servicelayer.session.SessionService;
import java.io.UnsupportedEncodingException;
import java.util.Objects;
import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Required;
import org.springframework.security.authentication.AbstractAuthenticationToken;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.DisabledException;
import org.springframework.security.authentication.InsufficientAuthenticationException;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.userdetails.UserDetails;
import com.bcf.bcfstorefrontaddon.constants.BcfstorefrontaddonWebConstants;
import com.bcf.bcfstorefrontaddon.security.B2BUserGroupProvider;
import com.bcf.core.customer.service.CustomerUpdateService;
import com.bcf.core.enums.AccountStatusType;
import com.bcf.facades.constants.BcfFacadesConstants;
import com.bcf.integration.activedirectory.service.BCFCustomerLDAPService;
import com.bcf.integration.crm.service.CRMSearchCustomerService;
import com.bcf.integration.crm.service.CRMUpdateCustomerService;
import com.bcf.integration.data.CRMCustomerEmailUpdateResponseDTO;
import com.bcf.integration.data.CRMGetCustomerResponseDTO;
import com.bcf.integration.data.ValidateCustomerCredentialsResponseDTO;
import com.bcf.integration.data.VerifyCustomerResponseDTO;
import com.bcf.integrations.core.exception.IntegrationException;


/**
 * Derived authentication provider supporting additional authentication checks. See
 * {@link de.hybris.platform.spring.security.RejectUserPreAuthenticationChecks}.
 *
 * <ul>
 * <li>prevent login without password for users created via CSCockpit</li>
 * <li>prevent login as user in group admingroup</li>
 * <li>prevent login as user if not authorised for B2B</li>
 * </ul>
 * <p>
 * any login as admin disables SearchRestrictions and therefore no page can be viewed correctly
 */
public class BCFAuthenticationProvider extends AbstractAcceleratorAuthenticationProvider
{
	private static final Logger LOG = Logger.getLogger(BCFAuthenticationProvider.class);
	private CRMSearchCustomerService crmSearchCustomerService;
	private CRMUpdateCustomerService crmUpdateCustomerService;
	private CustomerUpdateService customerUpdateService;
	private B2BUserGroupProvider b2bUserGroupProvider;
	private BCFCustomerLDAPService customerLDAPService;
	private SessionService sessionService;
	private static final String UNABLE_TO_RETRIEVE_CUSTOMER = "Customer could not be retrieved from LDAP, Exception occured:";

	/**
	 * @see AbstractAcceleratorAuthenticationProvider#additionalAuthenticationChecks(UserDetails,
	 * AbstractAuthenticationToken)
	 */
	@Override
	protected void additionalAuthenticationChecks(final UserDetails details, final AbstractAuthenticationToken authentication)
			throws AuthenticationException
	{
		super.additionalAuthenticationChecks(details, authentication);

		final UserModel userModel = getUserService().getUserForUID(StringUtils.lowerCase(details.getUsername()));
		final UserGroupModel b2bgroup = getUserService().getUserGroupForUID(B2BConstants.B2BGROUP);
		// Check if the customer is B2B type
		if (getUserService().isMemberOfGroup(userModel, b2bgroup))
		{
			if (!getB2bUserGroupProvider().isUserAuthorized(details.getUsername()))
			{
				throw new InsufficientAuthenticationException(
						messages.getMessage("checkout.error.invalid.accountType", "You are not allowed to login"));
			}

			// if it's a b2b user, check if it is active
			if (!getB2bUserGroupProvider().isUserEnabled(details.getUsername()))
			{
				throw new DisabledException("User " + details.getUsername() + " is disabled... "
						+ messages.getMessage("text.company.manage.units.disabled"));
			}
		}
	}

	private ValidateCustomerCredentialsResponseDTO authenticate(final String authentication, final Object password)
			throws AuthenticationException
	{

		try
		{
			return getCustomerLDAPService()
					.validateCredentials(authentication, password);

		}
		catch (final IntegrationException e)
		{
			if (Objects.nonNull(e.getErorrListDTO()) && CollectionUtils.isNotEmpty(e.getErorrListDTO().getErrorDto())
					&& StringUtils.equalsIgnoreCase(BcfstorefrontaddonWebConstants.ACCOUNT_LOCKED,
					e.getErorrListDTO().getErrorDto().get(0).getErrorCode()))
			{
				LOG.error(UNABLE_TO_RETRIEVE_CUSTOMER + e.getErorrListDTO().getErrorDto().get(0).getErrorDetail(), e);
				throw new BadCredentialsException("user.account.locked");
			}
			LOG.error(UNABLE_TO_RETRIEVE_CUSTOMER + e.getMessage(), e);
			throw new BadCredentialsException("login.error.message");
		}
	}

	private boolean accountVerified(final ValidateCustomerCredentialsResponseDTO authResponse)
	{
		return AccountStatusType.VERIFIED.getCode().equalsIgnoreCase(authResponse.getAccountStatus());
	}

	private void verifyAccount(final String uid, final String accountActivationKey) throws BadCredentialsException
	{
		try
		{
			final VerifyCustomerResponseDTO response = getCustomerLDAPService().verifyAtLDAP(accountActivationKey, uid);
			if (!BcfFacadesConstants.VERIFIED.equalsIgnoreCase(response.getAccountStatus()))
			{
				LOG.error("Account not verified, account status from LDAP:" + response.getAccountStatus());
				throw new BadCredentialsException("verification.email.message");
			}

			final UserModel userModel = getUserService().getUserForUID(uid);
			this.updateCustomer(userModel, response);
		}
		catch (final IntegrationException intEx)
		{
			LOG.error(intEx.getMessage(), intEx);
			throw new BadCredentialsException("verification.email.message");
		}
	}

	private void verifyEmailChangeAtLDAP(final String oldEmailId, final String newEmailId, final String accountActivationKey)
	{
		try
		{
			final CustomerModel customer = (CustomerModel) getUserService().getUserForUID(oldEmailId);
			// if accountActivationKey and email update requested, call the email change request with LDAP.
			final VerifyCustomerResponseDTO response = getCustomerLDAPService()
					.verifyEmailChangeAtLDAP(accountActivationKey, oldEmailId, newEmailId);

			// if its verified, call CRM to update new email.
			if (BcfFacadesConstants.VERIFIED.equalsIgnoreCase(response.getAccountStatus()))
			{
				//If its verified, than update the email change with CRM
				final CRMCustomerEmailUpdateResponseDTO crmCustomerEmailUpdateResponseDTO = crmUpdateCustomerService
						.updateEmail(customer, newEmailId);
				if (Objects.nonNull(crmCustomerEmailUpdateResponseDTO) && StringUtils
						.equalsIgnoreCase(crmCustomerEmailUpdateResponseDTO.getResult(), BcfFacadesConstants.SUCCESS))
				{
					customer.setAccountStatus(AccountStatusType.VERIFIED);
					customer.setUid(newEmailId);
					customer.setRequestedUpdateEmail("");
					getModelService().save(customer);

					this.updateCustomer(customer, response);
				}
			}
		}
		catch (final IntegrationException intEx)
		{
			LOG.error(intEx.getMessage(), intEx);
			throw new BadCredentialsException("update.email.crm.error");
		}

	}

	private void getCustomerFromCRM(final String authenticationId)
	{
		CRMGetCustomerResponseDTO response = null;
		boolean guestInd = false;
		try
		{
			try
			{
				final CustomerModel customerModel = (CustomerModel) getUserService().getUserForUID(authenticationId);
				guestInd = customerModel.isConsumer();
			}
			catch (final UnknownIdentifierException unEx)
			{
				LOG.error("Customer not found in Hybris for the given id " + authenticationId);
			}
			response = getCrmSearchCustomerService().getCustomer(authenticationId, guestInd);
			if (Objects.nonNull(response))
			{
				getCustomerUpdateService().updateCustomerProfile(authenticationId, response.getBCF_Customer());
			}
		}
		catch (final IntegrationException intEx)
		{
			LOG.error("Customer " + authenticationId + ", could not be retrieved from CRM, " + intEx.getMessage());
			LOG.error(intEx.getMessage(), intEx);
		}
		catch (final UnsupportedEncodingException ex)
		{
			LOG.error("Customer " + authenticationId + ", could not be retrieved from CRM, " + ex.getMessage());
			LOG.error(ex.getMessage(), ex);
		}

	}

	@Override
	public Authentication authenticate(final Authentication authentication) throws AuthenticationException
	{
		String uid = authentication.getName();
		final String oldEmailId = sessionService.getAttribute(BcfstorefrontaddonWebConstants.ACCOUNT_UPDATE_OLD_EMAIL);
		if (Objects.nonNull(oldEmailId))
		{
			uid = oldEmailId;
		}

		final ValidateCustomerCredentialsResponseDTO authResponse = this.authenticate(uid, authentication.getCredentials());


		if (!accountVerified(authResponse) || (accountVerified(authResponse) && Objects.nonNull(oldEmailId)))
		{
			final String accountActivationKey = getSessionService().getCurrentSession()
					.getAttribute(BcfstorefrontaddonWebConstants.ACCOUNT_ACTIVATION_KEY);
			getSessionService().getCurrentSession().removeAttribute(BcfstorefrontaddonWebConstants.ACCOUNT_ACTIVATION_KEY);

			if (Objects.isNull(accountActivationKey))
			{
				throw new BadCredentialsException("verification.email.message");
			}

			if (Objects.nonNull(oldEmailId))
			{
				verifyEmailChangeAtLDAP(oldEmailId, authentication.getName(), accountActivationKey);
			}
			else
			{
				verifyAccount(authentication.getName(), accountActivationKey);
			}
		}

		// call getCustomer and update it in hybris
		getCustomerFromCRM(authentication.getName());

		final UserDetails userDetails = retrieveUser(authentication.getName());
		JaloSession.getCurrentSession().setUser(UserManager.getInstance().getUserByLogin(userDetails.getUsername()));
		return createSuccessAuthentication(authentication, userDetails);
	}

	private void updateAccountStatus(final CustomerModel customer, final String accountStatus)
	{
		if (!StringUtils.equalsIgnoreCase(accountStatus, customer.getAccountStatus().getCode()))
		{
			customer.setAccountStatus(AccountStatusType.VERIFIED);
			getModelService().save(customer);
		}
	}

	private void updateCustomer(final UserModel userModel, final VerifyCustomerResponseDTO verifyCustomerResponseDTO)
	{
		final CustomerModel customer = (CustomerModel) userModel;
		customer.setAccountStatus(AccountStatusType.valueOf(verifyCustomerResponseDTO.getAccountStatus().toUpperCase()));
		customer.setLdapUID(verifyCustomerResponseDTO.getUid());
		customer.setCN(verifyCustomerResponseDTO.getCn());
		customer.setLdapValidationExpiryDate(verifyCustomerResponseDTO.getValidationExpiryDate());
		customer.setLoginDisabled(Boolean.FALSE);
		getModelService().save(customer);
	}

	protected CustomerUpdateService getCustomerUpdateService()
	{
		return customerUpdateService;
	}

	@Required
	public void setCustomerUpdateService(final CustomerUpdateService customerUpdateService)
	{
		this.customerUpdateService = customerUpdateService;
	}

	protected B2BUserGroupProvider getB2bUserGroupProvider()
	{
		return b2bUserGroupProvider;
	}

	@Required
	public void setB2bUserGroupProvider(final B2BUserGroupProvider b2bUserGroupProvider)
	{
		this.b2bUserGroupProvider = b2bUserGroupProvider;
	}

	protected CRMSearchCustomerService getCrmSearchCustomerService()
	{
		return crmSearchCustomerService;
	}

	@Required
	public void setCrmSearchCustomerService(final CRMSearchCustomerService crmSearchCustomerService)
	{
		this.crmSearchCustomerService = crmSearchCustomerService;
	}

	public BCFCustomerLDAPService getCustomerLDAPService()
	{
		return customerLDAPService;
	}

	@Required
	public void setCustomerLDAPService(final BCFCustomerLDAPService customerLDAPService)
	{
		this.customerLDAPService = customerLDAPService;
	}

	protected SessionService getSessionService()
	{
		return sessionService;
	}

	@Required
	public void setSessionService(final SessionService sessionService)
	{
		this.sessionService = sessionService;
	}

	@Required
	public void setCrmUpdateCustomerService(final CRMUpdateCustomerService crmUpdateCustomerService)
	{
		this.crmUpdateCustomerService = crmUpdateCustomerService;
	}
}
