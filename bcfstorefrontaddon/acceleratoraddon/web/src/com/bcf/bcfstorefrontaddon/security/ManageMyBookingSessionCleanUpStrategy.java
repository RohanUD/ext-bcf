/*
 * Copyright (c) 2019 British Columbia Ferry Services Inc
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of British Columbia Ferry Services Inc("Confidential Information").
 * You shall not disclose such Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with British Columbia Ferry Services Inc.
 *
 * Last Modified: 06/06/19 20:21
 */

package com.bcf.bcfstorefrontaddon.security;

import javax.servlet.http.HttpServletRequest;


/**
 * Strategy to remove ManageMyBooking session attributes if the URL of the page does not belong to ManageMyBooking flow.
 */
public interface ManageMyBookingSessionCleanUpStrategy
{

	/**
	 * Removes manageMyBooking sessionAttributes if the URL of the page being accessed does not matches with
	 * ManageMyBooking URL Pattern.
	 *
	 * @param request HttpServletRequest object.
	 */
	void manageMyBookingCleanUp(HttpServletRequest request);

}
