/*
 * Copyright (c) 2019 British Columbia Ferry Services Inc
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of British Columbia Ferry Services Inc("Confidential Information").
 * You shall not disclose such Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with British Columbia Ferry Services Inc.
 *
 * Last Modified: 06/06/19 20:21
 */

package com.bcf.bcfstorefrontaddon.security;

import de.hybris.platform.acceleratorstorefrontcommons.security.StorefrontAuthenticationSuccessHandler;
import de.hybris.platform.cms2.servicelayer.services.CMSSiteService;
import de.hybris.platform.servicelayer.config.ConfigurationService;
import de.hybris.platform.servicelayer.session.SessionService;
import de.hybris.platform.travelfacades.order.TravelCartFacade;
import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Required;
import com.bcf.bcfstorefrontaddon.constants.BcfstorefrontaddonWebConstants;
import com.bcf.core.constants.BcfCoreConstants;
import com.bcf.facades.order.BcfTravelCheckoutFacade;


/**
 * Extends StorefrontAuthenticationSuccessHandler to redefine target urls for checkout journey
 */
public class TravelStorefrontAuthenticationSuccessHandler extends StorefrontAuthenticationSuccessHandler
{
	private static final String PAYMENT_FLOW = "sop";

	@Resource(name = "cartFacade")
	private TravelCartFacade travelCartFacade;

	@Resource(name = "configurationService")
	private ConfigurationService configurationService;

	private BcfTravelCheckoutFacade travelCheckoutFacade;

	private SessionService sessionService;

	private CMSSiteService cmsSiteService;


	@Override
	protected String determineTargetUrl(final HttpServletRequest request, final HttpServletResponse response)
	{
		final String sessionBookingJourney = getSessionService()
				.getAttribute(BcfCoreConstants.SESSION_BOOKING_JOURNEY);
		if (StringUtils.isEmpty(sessionBookingJourney))
		{
			return "/";
		}
		if (StringUtils.equalsIgnoreCase(sessionBookingJourney, BcfstorefrontaddonWebConstants.BOOKING_TRANSPORT_ONLY))
		{
			if (getTravelCartFacade().isAmendmentCart())
			{
				final String paymentFlowProperty = configurationService.getConfiguration()
						.getString(BcfstorefrontaddonWebConstants.PAYMENT_FLOW_PROPERTY);
				if (StringUtils.isNotBlank(paymentFlowProperty))
				{
					return BcfstorefrontaddonWebConstants.PAYMENT_DETAILS_PATH + paymentFlowProperty;
				}
				return BcfstorefrontaddonWebConstants.PAYMENT_DETAILS_PATH + PAYMENT_FLOW;
			}
			return getSuccessRedirectUrl();
		}
		else if (StringUtils.equalsIgnoreCase(sessionBookingJourney, BcfstorefrontaddonWebConstants.BOOKING_ACCOMMODATION_ONLY))
		{
			return BcfstorefrontaddonWebConstants.SUMMARY_DETAILS_PATH;
		}
		else if (StringUtils.equalsIgnoreCase(sessionBookingJourney, BcfstorefrontaddonWebConstants.BOOKING_PACKAGE) || StringUtils
				.equalsIgnoreCase(sessionBookingJourney,
						BcfstorefrontaddonWebConstants.BOOKING_TRANSPORT_ACCOMMODATION))
		{
			if (!getTravelCheckoutFacade().containsLongRoute())
			{
				return BcfstorefrontaddonWebConstants.SUMMARY_DETAILS_PATH;
			}
			return BcfstorefrontaddonWebConstants.PERSONAL_DETAILS_PATH;
		}
		return "/";
	}


	protected String getSuccessRedirectUrl()
	{
		if (!getTravelCheckoutFacade().containsLongRoute())
		{
			return BcfstorefrontaddonWebConstants.SUMMARY_DETAILS_PATH;
		}
		return BcfstorefrontaddonWebConstants.TRAVELLER_DETAILS_PATH;
	}

	/**
	 * @return the travelCartFacade
	 */
	protected TravelCartFacade getTravelCartFacade()
	{
		return travelCartFacade;
	}

	/**
	 * @param travelCartFacade the travelCartFacade to set
	 */
	public void setTravelCartFacade(final TravelCartFacade travelCartFacade)
	{
		this.travelCartFacade = travelCartFacade;
	}

	/**
	 * @return the configurationService
	 */
	protected ConfigurationService getConfigurationService()
	{
		return configurationService;
	}

	/**
	 * @param configurationService the configurationService to set
	 */
	public void setConfigurationService(final ConfigurationService configurationService)
	{
		this.configurationService = configurationService;
	}

	protected CMSSiteService getCmsSiteService()
	{
		return cmsSiteService;
	}

	@Required
	public void setCmsSiteService(final CMSSiteService cmsSiteService)
	{
		this.cmsSiteService = cmsSiteService;
	}

	protected SessionService getSessionService()
	{
		return sessionService;
	}

	@Required
	public void setSessionService(final SessionService sessionService)
	{
		this.sessionService = sessionService;
	}

	protected BcfTravelCheckoutFacade getTravelCheckoutFacade()
	{
		return travelCheckoutFacade;
	}

	@Required
	public void setTravelCheckoutFacade(final BcfTravelCheckoutFacade travelCheckoutFacade)
	{
		this.travelCheckoutFacade = travelCheckoutFacade;
	}
}
