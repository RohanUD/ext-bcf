/*
 * Copyright (c) 2019 British Columbia Ferry Services Inc
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of British Columbia Ferry Services Inc("Confidential Information").
 * You shall not disclose such Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with British Columbia Ferry Services Inc.
 *
 * Last Modified: 06/06/19 20:21
 */

package com.bcf.bcfstorefrontaddon.filters;

import de.hybris.platform.servicelayer.time.TimeService;
import java.io.IOException;
import java.net.URLEncoder;
import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.springframework.web.filter.OncePerRequestFilter;
import com.bcf.bcfstorefrontaddon.security.cookie.EnhancedCookieGenerator;


public class SessionTimeoutCookieFilter extends OncePerRequestFilter
{
	private TimeService timeService;

	protected EnhancedCookieGenerator enhancedCookieGenerator;

	@Override
	protected void doFilterInternal(final HttpServletRequest request, final HttpServletResponse response,
			final FilterChain filterChain) throws ServletException, IOException
	{
		if (request.isRequestedSessionIdValid())
		{
			final Long cookieExpirationTime = getTimeService().getCurrentTime().getTime()
					+ getEnhancedCookieGenerator().getCookieMaxAge() * 1000;
			getEnhancedCookieGenerator().addCookie(response, URLEncoder.encode(cookieExpirationTime.toString(), "UTF-8"));
		}
		filterChain.doFilter(request, response);
	}

	public TimeService getTimeService()
	{
		return timeService;
	}

	public void setTimeService(final TimeService timeService)
	{
		this.timeService = timeService;
	}

	public EnhancedCookieGenerator getEnhancedCookieGenerator()
	{
		return enhancedCookieGenerator;
	}

	public void setEnhancedCookieGenerator(final EnhancedCookieGenerator enhancedCookieGenerator)
	{
		this.enhancedCookieGenerator = enhancedCookieGenerator;
	}

}
