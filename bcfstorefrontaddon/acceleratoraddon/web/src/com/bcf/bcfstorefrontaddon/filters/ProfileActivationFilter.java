/*
 * Copyright (c) 2019 British Columbia Ferry Services Inc
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of British Columbia Ferry Services Inc("Confidential Information").
 * You shall not disclose such Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with British Columbia Ferry Services Inc.
 *
 * Last Modified: 06/06/19 20:21
 */

package com.bcf.bcfstorefrontaddon.filters;

import de.hybris.platform.assistedserviceservices.AssistedServiceService;
import de.hybris.platform.assistedserviceservices.utils.AssistedServiceSession;
import de.hybris.platform.core.model.user.CustomerModel;
import de.hybris.platform.core.model.user.UserModel;
import de.hybris.platform.jalo.JaloSession;
import de.hybris.platform.jalo.user.User;
import de.hybris.platform.servicelayer.session.SessionService;
import de.hybris.platform.servicelayer.user.UserService;
import java.io.IOException;
import java.util.List;
import java.util.Objects;
import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.springframework.beans.factory.annotation.Required;
import org.springframework.security.web.DefaultRedirectStrategy;
import org.springframework.web.filter.OncePerRequestFilter;


public class ProfileActivationFilter extends OncePerRequestFilter
{
	private static final String ACTIVATE_PROFILE = "/activate-profile?activateProfile";
	private static final String ANONYMOUS_USER = "anonymous";

	private UserService userService;
	private SessionService sessionService;
	private AssistedServiceService assistedServiceService;
	private DefaultRedirectStrategy redirectStrategy;
	private List<String> restrictedPages;

	@Override
	protected void doFilterInternal(final HttpServletRequest request, final HttpServletResponse response,
			final FilterChain filterChain) throws ServletException, IOException
	{
		final UserModel currentUser = userService.getCurrentUser();
		final User user = JaloSession.getCurrentSession().getUser();
		AssistedServiceSession assistedServiceSession = assistedServiceService.getAsmSession();
		if (!user.getUid().equalsIgnoreCase(ANONYMOUS_USER) && (
				Objects.isNull(((CustomerModel) currentUser).getAccountType()) && (getRestrictedPages()
						.contains(request.getServletPath()))) &&
				!(assistedServiceSession != null && assistedServiceSession.getAgent() != null))
		{
			getRedirectStrategy().sendRedirect(request, response, ACTIVATE_PROFILE);
		}

		filterChain.doFilter(request, response);
	}

	protected UserService getUserService()
	{
		return userService;
	}

	@Required
	public void setUserService(final UserService userService)
	{
		this.userService = userService;
	}

	protected SessionService getSessionService()
	{
		return sessionService;
	}

	@Required
	public void setSessionService(final SessionService sessionService)
	{
		this.sessionService = sessionService;
	}

	protected DefaultRedirectStrategy getRedirectStrategy()
	{
		return redirectStrategy;
	}

	@Required
	public void setRedirectStrategy(final DefaultRedirectStrategy redirectStrategy)
	{
		this.redirectStrategy = redirectStrategy;
	}

	protected List<String> getRestrictedPages()
	{
		return restrictedPages;
	}

	@Required
	public void setRestrictedPages(final List<String> restrictedPages)
	{
		this.restrictedPages = restrictedPages;
	}

	public AssistedServiceService getAssistedServiceService()
	{
		return assistedServiceService;
	}

	@Required
	public void setAssistedServiceService(final AssistedServiceService assistedServiceService)
	{
		this.assistedServiceService = assistedServiceService;
	}
}
