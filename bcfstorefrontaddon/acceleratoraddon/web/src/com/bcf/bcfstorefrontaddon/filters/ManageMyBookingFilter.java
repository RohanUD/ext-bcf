/*
 * Copyright (c) 2019 British Columbia Ferry Services Inc
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of British Columbia Ferry Services Inc("Confidential Information").
 * You shall not disclose such Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with British Columbia Ferry Services Inc.
 *
 * Last Modified: 06/06/19 20:21
 */

package com.bcf.bcfstorefrontaddon.filters;

import java.io.IOException;
import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.springframework.beans.factory.annotation.Required;
import org.springframework.web.filter.OncePerRequestFilter;
import com.bcf.bcfstorefrontaddon.security.ManageMyBookingSessionCleanUpStrategy;


/**
 * This Filter cleans up ManageMyBookingSession Attribute, if the url of the page being accessed is not in
 * ManageMyBooking flow.
 */
public class ManageMyBookingFilter extends OncePerRequestFilter
{
	private ManageMyBookingSessionCleanUpStrategy mmbSessionCleanUpStrategy;

	/**
	 * Checks the URL pattern of the page url, and if its not of ManageMyBooking pattern, removes the manageMyBooking
	 * session attributes.
	 */
	@Override
	protected void doFilterInternal(final HttpServletRequest request, final HttpServletResponse response,
			final FilterChain filterChain) throws ServletException, IOException
	{
		mmbSessionCleanUpStrategy.manageMyBookingCleanUp(request);
		filterChain.doFilter(request, response);

	}

	protected ManageMyBookingSessionCleanUpStrategy getMmbSessionCleanUpStrategy()
	{
		return mmbSessionCleanUpStrategy;
	}

	@Required
	public void setMmbSessionCleanUpStrategy(final ManageMyBookingSessionCleanUpStrategy mmbSessionCleanUpStrategy)
	{
		this.mmbSessionCleanUpStrategy = mmbSessionCleanUpStrategy;
	}

}
