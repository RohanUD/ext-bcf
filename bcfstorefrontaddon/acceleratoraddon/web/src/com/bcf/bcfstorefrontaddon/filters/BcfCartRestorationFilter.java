/*
 * Copyright (c) 2019 British Columbia Ferry Services Inc
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of British Columbia Ferry Services Inc("Confidential Information").
 * You shall not disclose such Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with British Columbia Ferry Services Inc.
 *
 * Last Modified: 02/07/19 14:01
 */

package com.bcf.bcfstorefrontaddon.filters;

import de.hybris.platform.acceleratorstorefrontcommons.constants.WebConstants;
import de.hybris.platform.commerceservices.order.CommerceCartRestorationException;
import de.hybris.platform.order.CartService;
import de.hybris.platform.servicelayer.session.SessionService;
import de.hybris.platform.servicelayer.user.UserService;
import de.hybris.platform.site.BaseSiteService;
import java.io.IOException;
import java.util.Date;
import java.util.Objects;
import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Required;
import org.springframework.web.filter.OncePerRequestFilter;
import com.bcf.facades.order.BcfTravelCartFacade;
import com.bcf.storefront.security.cookie.CartRestoreCookieGenerator;


public class BcfCartRestorationFilter extends OncePerRequestFilter
{
	private static final Logger LOG = Logger.getLogger(BcfCartRestorationFilter.class);

	private CartRestoreCookieGenerator cartRestoreCookieGenerator;
	private CartService cartService;
	private BcfTravelCartFacade cartFacade;
	private BaseSiteService baseSiteService;
	private UserService userService;
	private SessionService sessionService;

	@Override
	public void doFilterInternal(final HttpServletRequest request, final HttpServletResponse response,
			final FilterChain filterChain) throws IOException, ServletException
	{
		if (getUserService().isAnonymousUser(getUserService().getCurrentUser()))
		{
			processAnonymousUser(request, response);
		}
		else
		{
			restoreCart();
		}

		filterChain.doFilter(request, response);
	}

	protected void restoreCart()
	{
		if ((!getCartService().hasSessionCart() && getSessionService().getAttribute(WebConstants.CART_RESTORATION) == null)
				|| (getSessionService().getAttribute(WebConstants.CART_RESTORATION) != null && getCartService().hasSessionCart()
				&& Objects.nonNull(getCartService().getSessionCart().getQuoteExpirationDate())
				&& (new Date()).after(getCartService().getSessionCart().getQuoteExpirationDate())))
		{
			getSessionService().setAttribute(WebConstants.CART_RESTORATION_SHOW_MESSAGE, Boolean.TRUE);
			try
			{
				getSessionService().setAttribute(WebConstants.CART_RESTORATION, getCartFacade().restoreSavedCart());
			}
			catch (final CommerceCartRestorationException e)
			{
				if (LOG.isDebugEnabled())
				{
					LOG.debug(e);
				}
				getSessionService().setAttribute(WebConstants.CART_RESTORATION, WebConstants.CART_RESTORATION_ERROR_STATUS);
			}
		}
	}

	protected void processAnonymousUser(final HttpServletRequest request, final HttpServletResponse response)
	{
		if (getCartService().hasSessionCart() && getBaseSiteService().getCurrentBaseSite()
				.equals(getBaseSiteService().getBaseSiteForUID(getCartService().getSessionCart().getSite().getUid())))
		{
			final String guid = getCartService().getSessionCart().getGuid();

			if (!StringUtils.isEmpty(guid))
			{
				getCartRestoreCookieGenerator().addCookie(response, guid);
			}
		}
		else if (request.getSession().isNew() || (getCartService().hasSessionCart() && !getBaseSiteService().getCurrentBaseSite()
				.equals(getBaseSiteService().getBaseSiteForUID(getCartService().getSessionCart().getSite().getUid()))))
		{
			processRestoration(request);
		}
	}

	protected void processRestoration(final HttpServletRequest request)
	{
		String cartGuid = null;

		if (request.getCookies() != null)
		{
			final String anonymousCartCookieName = getCartRestoreCookieGenerator().getCookieName();

			for (final Cookie cookie : request.getCookies())
			{
				if (anonymousCartCookieName.equals(cookie.getName()))
				{
					cartGuid = cookie.getValue();
					break;
				}
			}
		}

		if (!StringUtils.isEmpty(cartGuid))
		{
			getSessionService().setAttribute(WebConstants.CART_RESTORATION_SHOW_MESSAGE, Boolean.TRUE);
			try
			{
				getSessionService().setAttribute(WebConstants.CART_RESTORATION, getCartFacade().restoreSavedCart(null));
			}
			catch (final CommerceCartRestorationException e)
			{
				if (LOG.isDebugEnabled())
				{
					LOG.debug(e);
				}
				getSessionService().setAttribute(WebConstants.CART_RESTORATION_ERROR_STATUS,
						WebConstants.CART_RESTORATION_ERROR_STATUS);
			}
		}
	}

	protected SessionService getSessionService()
	{
		return sessionService;
	}

	@Required
	public void setSessionService(final SessionService sessionService)
	{
		this.sessionService = sessionService;
	}

	protected CartRestoreCookieGenerator getCartRestoreCookieGenerator()
	{
		return cartRestoreCookieGenerator;
	}

	@Required
	public void setCartRestoreCookieGenerator(final CartRestoreCookieGenerator cartRestoreCookieGenerator)
	{
		this.cartRestoreCookieGenerator = cartRestoreCookieGenerator;
	}

	protected BcfTravelCartFacade getCartFacade()
	{
		return cartFacade;
	}

	@Required
	public void setCartFacade(final BcfTravelCartFacade cartFacade)
	{
		this.cartFacade = cartFacade;
	}

	protected BaseSiteService getBaseSiteService()
	{
		return baseSiteService;
	}

	@Required
	public void setBaseSiteService(final BaseSiteService baseSiteService)
	{
		this.baseSiteService = baseSiteService;
	}

	protected UserService getUserService()
	{
		return userService;
	}

	@Required
	public void setUserService(final UserService userService)
	{
		this.userService = userService;
	}

	protected CartService getCartService()
	{
		return cartService;
	}

	@Required
	public void setCartService(final CartService cartService)
	{
		this.cartService = cartService;
	}
}
