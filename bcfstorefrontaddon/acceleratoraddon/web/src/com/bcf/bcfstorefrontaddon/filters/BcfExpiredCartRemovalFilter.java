/*
 * Copyright (c) 2019 British Columbia Ferry Services Inc
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of British Columbia Ferry Services Inc("Confidential Information").
 * You shall not disclose such Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with British Columbia Ferry Services Inc.
 *
 * Last Modified: 08/08/19 15:16
 */

package com.bcf.bcfstorefrontaddon.filters;

import de.hybris.platform.core.model.order.CartModel;
import java.io.IOException;
import java.util.Date;
import java.util.List;
import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Required;
import org.springframework.web.filter.OncePerRequestFilter;
import com.bcf.core.service.BCFTravelCartService;
import com.bcf.core.util.OptionBookingUtil;


public class BcfExpiredCartRemovalFilter extends OncePerRequestFilter
{
	private static final Logger LOG = Logger.getLogger(BcfExpiredCartRemovalFilter.class);

	private BCFTravelCartService cartService;
	private List<String> urlsToSkip;

	@Override
	public void doFilterInternal(final HttpServletRequest request, final HttpServletResponse response,
			final FilterChain filterChain) throws IOException, ServletException
	{
		if (!getUrlsToSkip().contains(request.getServletPath()) && getCartService().hasSessionCart())
		{
			final CartModel cart = getCartService().getSessionCart();
			if (!(cart.isQuote() || OptionBookingUtil.isOptionBooking(cart)))
			{
				final long stockHoldTime = cart.getStockHoldTime();
				final Date timeNow = new Date();
				if (stockHoldTime > 0 && timeNow.getTime() > stockHoldTime)
				{
					LOG.warn(String.format("Cart [%s] no longer valid, removing...", cart.getCode()));
					getCartService().removeSessionCart();
				}
			}
		}
		filterChain.doFilter(request, response);
	}

	protected BCFTravelCartService getCartService()
	{
		return cartService;
	}

	@Required
	public void setCartService(final BCFTravelCartService cartService)
	{
		this.cartService = cartService;
	}

	protected List<String> getUrlsToSkip()
	{
		return urlsToSkip;
	}

	@Required
	public void setUrlsToSkip(final List<String> urlsToSkip)
	{
		this.urlsToSkip = urlsToSkip;
	}
}
