/*
 * Copyright (c) 2019 British Columbia Ferry Services Inc
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of British Columbia Ferry Services Inc("Confidential Information").
 * You shall not disclose such Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with British Columbia Ferry Services Inc.
 *
 * Last Modified: 06/06/19 20:21
 */

package com.bcf.bcfstorefrontaddon.controllers.cms;

import de.hybris.platform.addonsupport.controllers.cms.AbstractCMSAddOnComponentController;
import de.hybris.platform.servicelayer.session.SessionService;
import java.util.Objects;
import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import com.bcf.bcfstorefrontaddon.constants.BcfstorefrontaddonControllerConstants;
import com.bcf.core.model.components.CMSAlertParagraphComponentModel;


@Controller("CMSAlertParagraphComponentController")
@RequestMapping(value = BcfstorefrontaddonControllerConstants.Actions.Cms.CMSAlertParagraphComponent)
public class CMSAlertParagraphComponentController extends AbstractCMSAddOnComponentController<CMSAlertParagraphComponentModel>
{

	private static final String DISPLAY_ALERT = "display.alert";

	@Resource
	SessionService sessionService;

	@RequestMapping(value = "/remove-alert", method = RequestMethod.GET)
	public void removeAlert()
	{
		sessionService.setAttribute(DISPLAY_ALERT, Boolean.FALSE);
	}

	@Override
	protected void fillModel(final HttpServletRequest request, final Model model, final CMSAlertParagraphComponentModel component)
	{

		//First time display alert
		if (Objects.isNull(sessionService.getAttribute(DISPLAY_ALERT)))
		{
			model.addAttribute("DISPLAY_ALERT", Boolean.TRUE);
			sessionService.setAttribute(DISPLAY_ALERT, Boolean.TRUE);
		}

		model.addAttribute("DISPLAY_ALERT", sessionService.getAttribute(DISPLAY_ALERT));

	}

	@Override
	protected String getView(final CMSAlertParagraphComponentModel component)
	{
		return BcfstorefrontaddonControllerConstants.ADDON_PREFIX + BcfstorefrontaddonControllerConstants.Views.Cms.ComponentPrefix
				+ getViewResourceName(component);
	}

}
