/*
 * Copyright (c) 2019 British Columbia Ferry Services Inc
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of British Columbia Ferry Services Inc("Confidential Information").
 * You shall not disclose such Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with British Columbia Ferry Services Inc.
 *
 * Last Modified: 06/06/19 20:21
 */

package com.bcf.bcfstorefrontaddon.controllers.pages;

import static com.bcf.bcfstorefrontaddon.constants.BcfstorefrontaddonWebConstants.SERVICE_NOT_ACTIVATED;
import static com.bcf.bcfstorefrontaddon.constants.BcfstorefrontaddonWebConstants.SERVICE_NOT_AVAILABLE;

import de.hybris.platform.acceleratorstorefrontcommons.breadcrumb.ResourceBreadcrumbBuilder;
import de.hybris.platform.acceleratorstorefrontcommons.constants.WebConstants;
import de.hybris.platform.acceleratorstorefrontcommons.controllers.ThirdPartyConstants;
import de.hybris.platform.acceleratorstorefrontcommons.controllers.util.GlobalMessages;
import de.hybris.platform.acceleratorstorefrontcommons.forms.ForgottenPwdForm;
import de.hybris.platform.acceleratorstorefrontcommons.forms.UpdatePwdForm;
import de.hybris.platform.acceleratorstorefrontcommons.forms.validation.UpdatePasswordFormValidator;
import de.hybris.platform.cms2.exceptions.CMSItemNotFoundException;
import de.hybris.platform.commerceservices.customer.TokenInvalidatedException;
import de.hybris.platform.servicelayer.exceptions.UnknownIdentifierException;
import javax.annotation.Resource;
import javax.validation.Valid;
import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;
import com.bcf.bcfstorefrontaddon.controllers.imported.AcceleratorControllerConstants;
import com.bcf.bcfstorefrontaddon.forms.BcfUpdatePasswordForm;
import com.bcf.bcfstorefrontaddon.validators.BcfResetPasswordValidator;
import com.bcf.core.exception.BcfAccountVerifyException;
import com.bcf.facades.customer.impl.CustomBCFCustomerFacade;
import com.bcf.integrations.core.exception.IntegrationException;


/**
 * Controller for the forgotten password pages. Supports requesting a password reset email as well as changing the
 * password once you have got the token that was sent via email.
 */
@Controller
@RequestMapping(value = "/login/pw")
public class PasswordResetPageController extends BcfAbstractPageController
{
	private static final String FORGOTTEN_PWD_TITLE = "forgottenPwd.title";

	private static final Logger LOG = Logger.getLogger(PasswordResetPageController.class);

	private static final String REDIRECT_PWD_REQ_CONF = "redirect:/login/pw/request/external/conf";
	private static final String REDIRECT_LOGIN = "redirect:/login";
	private static final String REDIRECT_HOME = "redirect:/";
	private static final String UPDATE_PWD_CMS_PAGE = "updatePassword";

	private static final String FORGOT_PASSWORD_CMS_PAGE = "forgotPasswordPage";
	private static final String FORGOT_PASSWORD_FORM_ATTR = "forgottenPwdForm";
	private static final String PASSWORD_RESET_REQUEST_SUCCESS = "passwordRestRequestSuccess";

	@Resource(name = "simpleBreadcrumbBuilder")
	private ResourceBreadcrumbBuilder resourceBreadcrumbBuilder;

	@Resource(name = "updatePasswordFormValidator")
	private UpdatePasswordFormValidator updatePasswordFormValidator;

	@Resource(name = "bcfresetPasswordValidator")
	private BcfResetPasswordValidator bcfResetPasswordValidator;

	@Resource(name = "bcfCustomerFacade")
	private CustomBCFCustomerFacade bcfCustomerFacade;

	@RequestMapping(method = RequestMethod.GET)
	public String viewPage(final Model model, final RedirectAttributes redirectModel) throws CMSItemNotFoundException
	{
		model.addAttribute(FORGOT_PASSWORD_FORM_ATTR, new ForgottenPwdForm());
		return returnToPage(model);
	}

	@RequestMapping(method = RequestMethod.POST)
	public String forgotPassword(@RequestParam(value = "email", required = true) final String email,
			final Model model, final RedirectAttributes redirectModel) throws CMSItemNotFoundException
	{
		if (StringUtils.isEmpty(email))
		{
			GlobalMessages.addErrorMessage(model, "profile.email.invalid");
			model.addAttribute(FORGOT_PASSWORD_FORM_ATTR, new ForgottenPwdForm());
			return returnToPage(model);
		}

		try
		{
			bcfCustomerFacade.forgotPassword(email);
		}
		catch (final UnknownIdentifierException unEx)
		{
			LOG.error(unEx.getMessage(), unEx);
			GlobalMessages.addErrorMessage(model, "forgot.email.not.found.message");
			model.addAttribute(FORGOT_PASSWORD_FORM_ATTR, new ForgottenPwdForm());
			return returnToPage(model);
		}
		catch (final IntegrationException e)
		{
			LOG.error(e.getMessage(), e);
			model.addAttribute(FORGOT_PASSWORD_FORM_ATTR, new ForgottenPwdForm());
			GlobalMessages.addErrorMessage(model, SERVICE_NOT_AVAILABLE);
			return returnToPage(model);
		}
		catch (final BcfAccountVerifyException e)
		{
			LOG.error(e.getMessage(), e);
			model.addAttribute(FORGOT_PASSWORD_FORM_ATTR, new ForgottenPwdForm());
			GlobalMessages.addErrorMessage(model, SERVICE_NOT_ACTIVATED);
			return returnToPage(model);
		}

		model.addAttribute(PASSWORD_RESET_REQUEST_SUCCESS, Boolean.TRUE);
		model.addAttribute("email", email);
		return returnToPage(model);
	}

	private String returnToPage(final Model model) throws CMSItemNotFoundException
	{
		storeCmsPageInModel(model, getContentPageForLabelOrId(FORGOT_PASSWORD_CMS_PAGE));
		setUpMetaDataForContentPage(model, getContentPageForLabelOrId(FORGOT_PASSWORD_CMS_PAGE));

		model.addAttribute(WebConstants.BREADCRUMBS_KEY, resourceBreadcrumbBuilder.getBreadcrumbs("breadcrumb.home"));
		model.addAttribute(ThirdPartyConstants.SeoRobots.META_ROBOTS, ThirdPartyConstants.SeoRobots.NOINDEX_NOFOLLOW);
		return getViewForPage(model);
	}

	@RequestMapping(value = "/request/external", method = RequestMethod.GET)
	public String getExternalPasswordRequest(final Model model) throws CMSItemNotFoundException
	{
		model.addAttribute(new ForgottenPwdForm());
		storeCmsPageInModel(model, getContentPageForLabelOrId(null));
		setUpMetaDataForContentPage(model, getContentPageForLabelOrId(null));
		model.addAttribute(WebConstants.BREADCRUMBS_KEY, resourceBreadcrumbBuilder.getBreadcrumbs(FORGOTTEN_PWD_TITLE));
		return AcceleratorControllerConstants.Views.Pages.Password.PasswordResetRequest;
	}

	@RequestMapping(value = "/request/external/conf", method = RequestMethod.GET)
	public String getExternalPasswordRequestConf(final Model model) throws CMSItemNotFoundException
	{
		storeCmsPageInModel(model, getContentPageForLabelOrId(null));
		setUpMetaDataForContentPage(model, getContentPageForLabelOrId(null));
		model.addAttribute(WebConstants.BREADCRUMBS_KEY, resourceBreadcrumbBuilder.getBreadcrumbs(FORGOTTEN_PWD_TITLE));
		return AcceleratorControllerConstants.Views.Pages.Password.PasswordResetRequestConfirmation;
	}

	@RequestMapping(value = "/request/external", method = RequestMethod.POST)
	public String externalPasswordRequest(@Valid final ForgottenPwdForm form, final BindingResult bindingResult, final Model model,
			final RedirectAttributes redirectModel)
			throws CMSItemNotFoundException
	{
		storeCmsPageInModel(model, getContentPageForLabelOrId(null));
		setUpMetaDataForContentPage(model, getContentPageForLabelOrId(null));
		model.addAttribute(WebConstants.BREADCRUMBS_KEY, resourceBreadcrumbBuilder.getBreadcrumbs(FORGOTTEN_PWD_TITLE));

		if (bindingResult.hasErrors())
		{
			return AcceleratorControllerConstants.Views.Pages.Password.PasswordResetRequest;
		}
		else
		{
			try
			{
				bcfCustomerFacade.forgottenPassword(form.getEmail());
				GlobalMessages.addFlashMessage(redirectModel, GlobalMessages.CONF_MESSAGES_HOLDER,
						"account.confirmation.forgotten.password.link.sent");
			}
			catch (final UnknownIdentifierException unknownIdentifierException)
			{
				LOG.warn("Email: " + form.getEmail() + " does not exist in the database.");
			}
			return REDIRECT_PWD_REQ_CONF;
		}
	}

	@RequestMapping(value = "/change", method = RequestMethod.GET)
	public String getChangePassword(@RequestParam(required = false) final String token, final Model model)
			throws CMSItemNotFoundException
	{
		if (StringUtils.isBlank(token))
		{
			return REDIRECT_HOME;
		}
		final UpdatePwdForm form = new UpdatePwdForm();
		form.setToken(token);
		model.addAttribute(form);
		storeCmsPageInModel(model, getContentPageForLabelOrId(UPDATE_PWD_CMS_PAGE));
		setUpMetaDataForContentPage(model, getContentPageForLabelOrId(UPDATE_PWD_CMS_PAGE));
		model.addAttribute(WebConstants.BREADCRUMBS_KEY, resourceBreadcrumbBuilder.getBreadcrumbs("updatePwd.title"));
		return AcceleratorControllerConstants.Views.Pages.Password.PasswordResetChangePage;
	}

	@RequestMapping(value = "/resetpassword", method = RequestMethod.GET)
	public String getResetPassword(@RequestParam(required = false) final String key, final Model model)
			throws CMSItemNotFoundException
	{
		if (StringUtils.isBlank(key))
		{
			return REDIRECT_HOME;
		}
		final BcfUpdatePasswordForm form = new BcfUpdatePasswordForm();
		form.setKey(key);
		model.addAttribute(form);
		storeCmsPageInModel(model, getContentPageForLabelOrId(UPDATE_PWD_CMS_PAGE));
		setUpMetaDataForContentPage(model, getContentPageForLabelOrId(UPDATE_PWD_CMS_PAGE));
		model.addAttribute(WebConstants.BREADCRUMBS_KEY, resourceBreadcrumbBuilder.getBreadcrumbs("updatePwd.title"));
		return AcceleratorControllerConstants.Views.Pages.Password.PasswordResetChangePage;
	}

	@RequestMapping(value = "/resetpassword", method = RequestMethod.POST)
	public String resetPassword(@Valid final BcfUpdatePasswordForm form, final BindingResult bindingResult, final Model model,
			final RedirectAttributes redirectModel) throws CMSItemNotFoundException
	{
		bcfResetPasswordValidator.validate(form, bindingResult);
		if (bindingResult.hasErrors())
		{
			prepareErrorMessage(model, UPDATE_PWD_CMS_PAGE);
			return AcceleratorControllerConstants.Views.Pages.Password.PasswordResetChangePage;
		}
		if (!StringUtils.isBlank(form.getKey()))
		{
			try
			{
				bcfCustomerFacade.updateCustomerPassword(form.getKey(), form.getPwd());
				GlobalMessages.addFlashMessage(redirectModel, GlobalMessages.CONF_MESSAGES_HOLDER,
						"account.confirmation.password.updated");
			}
			catch (final TokenInvalidatedException e)
			{
				GlobalMessages.addFlashMessage(redirectModel, GlobalMessages.ERROR_MESSAGES_HOLDER, "updatePwd.token.invalidated");
			}
			catch (final UnknownIdentifierException ukEx)
			{
				GlobalMessages.addFlashMessage(redirectModel, GlobalMessages.ERROR_MESSAGES_HOLDER, "updatePwd.token.invalid");
			}
			catch (final IntegrationException intEx)
			{
				LOG.error(intEx.getMessage(), intEx);
				GlobalMessages.addFlashMessage(redirectModel, GlobalMessages.ERROR_MESSAGES_HOLDER, SERVICE_NOT_AVAILABLE);
			}
			catch (final IllegalArgumentException iaEx)
			{
				LOG.error(iaEx.getMessage(), iaEx);
				GlobalMessages.addFlashMessage(redirectModel, GlobalMessages.ERROR_MESSAGES_HOLDER, SERVICE_NOT_AVAILABLE);
			}
		}
		return REDIRECT_LOGIN;
	}

	@RequestMapping(value = "/change", method = RequestMethod.POST)
	public String changePassword(@Valid final UpdatePwdForm form, final BindingResult bindingResult, final Model model,
			final RedirectAttributes redirectModel) throws CMSItemNotFoundException
	{
		getUpdatePasswordFormValidator().validate(form, bindingResult);
		if (bindingResult.hasErrors())
		{
			prepareErrorMessage(model, UPDATE_PWD_CMS_PAGE);
			return AcceleratorControllerConstants.Views.Pages.Password.PasswordResetChangePage;
		}
		if (!StringUtils.isBlank(form.getToken()))
		{
			try
			{
				bcfCustomerFacade.updatePassword(form.getToken(), form.getPwd());
				GlobalMessages.addFlashMessage(redirectModel, GlobalMessages.CONF_MESSAGES_HOLDER,
						"account.confirmation.password.updated");
			}
			catch (final TokenInvalidatedException e)
			{
				GlobalMessages.addFlashMessage(redirectModel, GlobalMessages.ERROR_MESSAGES_HOLDER, "updatePwd.token.invalidated");
			}
			catch (final RuntimeException e)
			{
				if (LOG.isDebugEnabled())
				{
					LOG.debug(e);
				}
				GlobalMessages.addFlashMessage(redirectModel, GlobalMessages.ERROR_MESSAGES_HOLDER, "updatePwd.token.invalid");
			}
		}
		return REDIRECT_LOGIN;
	}

	/**
	 * Prepares the view to display an error message
	 *
	 * @throws CMSItemNotFoundException
	 */
	protected void prepareErrorMessage(final Model model, final String page) throws CMSItemNotFoundException
	{
		GlobalMessages.addErrorMessage(model, "form.global.error");
		storeCmsPageInModel(model, getContentPageForLabelOrId(page));
		setUpMetaDataForContentPage(model, getContentPageForLabelOrId(page));
	}
	

	public UpdatePasswordFormValidator getUpdatePasswordFormValidator()
	{
		return updatePasswordFormValidator;
	}

	public BcfResetPasswordValidator getBcfResetPasswordValidator()
	{
		return bcfResetPasswordValidator;
	}

	public CustomBCFCustomerFacade getBcfCustomerFacade()
	{
		return bcfCustomerFacade;
	}

	public void setBcfCustomerFacade(final CustomBCFCustomerFacade bcfCustomerFacade)
	{
		this.bcfCustomerFacade = bcfCustomerFacade;
	}
}
