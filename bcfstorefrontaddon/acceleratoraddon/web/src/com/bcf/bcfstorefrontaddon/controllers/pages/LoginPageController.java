/*
 * Copyright (c) 2019 British Columbia Ferry Services Inc
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of British Columbia Ferry Services Inc("Confidential Information").
 * You shall not disclose such Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with British Columbia Ferry Services Inc.
 *
 * Last Modified: 02/07/19 14:01
 */

package com.bcf.bcfstorefrontaddon.controllers.pages;

import de.hybris.platform.acceleratorstorefrontcommons.controllers.util.GlobalMessages;
import de.hybris.platform.acceleratorstorefrontcommons.forms.GuestForm;
import de.hybris.platform.acceleratorstorefrontcommons.forms.LoginForm;
import de.hybris.platform.assistedservicefacades.AssistedServiceFacade;
import de.hybris.platform.assistedserviceservices.AssistedServiceService;
import de.hybris.platform.cms2.exceptions.CMSItemNotFoundException;
import de.hybris.platform.cms2.model.pages.AbstractPageModel;
import de.hybris.platform.commercefacades.customer.CustomerFacade;
import de.hybris.platform.commercefacades.i18n.I18NFacade;
import de.hybris.platform.commercefacades.user.data.AccountTypeData;
import de.hybris.platform.commercefacades.user.data.CustomerData;
import de.hybris.platform.commercefacades.user.data.RegionData;
import de.hybris.platform.commercefacades.user.data.RegisterData;
import de.hybris.platform.commerceservices.customer.DuplicateUidException;
import de.hybris.platform.servicelayer.exceptions.UnknownIdentifierException;
import de.hybris.platform.travelfacades.facades.TravelI18NFacade;
import java.util.Collections;
import java.util.List;
import java.util.Objects;
import java.util.Set;
import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.security.web.savedrequest.HttpSessionRequestCache;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;
import com.bcf.bcfstorefrontaddon.constants.BcfstorefrontaddonWebConstants;
import com.bcf.bcfstorefrontaddon.controllers.imported.AcceleratorControllerConstants;
import com.bcf.bcfstorefrontaddon.form.validators.BCFSignupValidator;
import com.bcf.bcfstorefrontaddon.forms.BCFSignupForm;
import com.bcf.core.util.StreamUtil;
import com.bcf.facades.customer.BCFCustomerFacade;
import com.bcf.facades.customer.impl.CustomBCFCustomerFacade;
import com.bcf.integration.error.ErrorListDTO;
import com.bcf.integrations.core.exception.IntegrationException;


/**
 * Login Controller. Handles login and register for the account flow.
 */
@Controller
@RequestMapping(value = "/login")
public class LoginPageController extends BCFAbstractLoginPageController
{

	private static final Logger LOGGER = Logger.getLogger(LoginPageController.class);
	private static final String FORM_GLOBAL_ERROR = "form.global.error";
	private static final String SIGNUP_FORM = "BCFSignupForm";
	private static final String REGISTER_SUCCESS = "register-success";
	protected static final String COUNTRIES = "countries";
	private static final String USER_ALREADY_EXISTS = "registration.error.account.exists.title";
	private static final String SHOW_REGISTERATION_CONTENT = "showRegistrationContent";
	private static final String REGISTRATION_FAILED = "registration failed: ";
	private static final String EMAIL = "email";

	private HttpSessionRequestCache httpSessionRequestCache;

	@Resource(name = "travelCustomerFacade")
	private CustomerFacade customerFacade;

	@Resource(name = "travelI18NFacade")
	private TravelI18NFacade travelI18NFacade;

	@Resource(name = "bcfSignupValidator")
	private BCFSignupValidator bcfSignupValidator;

	@Resource(name = "bcfCustomerFacade")
	private BCFCustomerFacade bcfCustomerFacade;

	@Resource(name = "travelCustomerFacade")
	private CustomBCFCustomerFacade travelCustomerFacade;

	@Resource(name = "assistedServiceService")
	private AssistedServiceService assistedServiceService;

	@Resource(name = "bookingFlowUrlSet")
	private Set<String> bookingFlowUrlSet;

	@Resource(name = "i18NFacade")
	private I18NFacade i18NFacade;

	@Resource(name = "assistedServiceFacade")
	private AssistedServiceFacade assistedServiceFacade;

	@Override
	protected String getView()
	{
		return AcceleratorControllerConstants.Views.Pages.Account.AccountLoginPage;
	}

	@Override
	protected String getSuccessRedirect(final HttpServletRequest request, final HttpServletResponse response)
	{
		if (httpSessionRequestCache.getRequest(request, response) != null)
		{
			return httpSessionRequestCache.getRequest(request, response).getRedirectUrl();
		}
		return "/";
	}

	@Override
	protected AbstractPageModel getCmsPage() throws CMSItemNotFoundException
	{
		return getContentPageForLabelOrId("login");
	}

	@Resource(name = "httpSessionRequestCache")
	public void setHttpSessionRequestCache(final HttpSessionRequestCache accHttpSessionRequestCache)
	{
		this.httpSessionRequestCache = accHttpSessionRequestCache;
	}

	@RequestMapping(method = RequestMethod.GET)
	public String doLogin(@RequestHeader(value = "referer", required = false) final String referer,
			@RequestParam(value = "error", defaultValue = "false") final boolean loginError,@RequestParam(value =
					"loginToCheckout", defaultValue = "false") final boolean loginToCheckout, final Model model,
			final HttpServletRequest request, final HttpServletResponse response, final HttpSession session)
			throws CMSItemNotFoundException
	{
		model.addAttribute("isLoginPage", true);
		if (!loginError)
		{
			storeReferer(referer, request, response);
		}

		boolean isLoginFromBookingFlow = false;
		if (CollectionUtils.isNotEmpty(bookingFlowUrlSet) && referer!=null)
		{
			isLoginFromBookingFlow = bookingFlowUrlSet.stream().anyMatch(url -> referer.contains(url));
		}
		model.addAttribute(SHOW_REGISTERATION_CONTENT, !isLoginFromBookingFlow && !loginToCheckout);
		model.addAttribute(COUNTRIES, travelI18NFacade.getAllCountries());
		return getDefaultLoginPage(loginError, session, model);
	}

	protected void storeReferer(final String referer, final HttpServletRequest request, final HttpServletResponse response)
	{
		if (StringUtils.isNotBlank(referer) && !StringUtils.endsWith(referer, "/login")
				&& StringUtils.contains(referer, request.getServerName()))
		{
			httpSessionRequestCache.saveRequest(request, response);
		}
	}

	@RequestMapping(value = "/register", method = RequestMethod.POST)
	public String doRegister(@RequestHeader(value = "referer", required = false) final String referer,
			final BCFSignupForm bcfSignupForm, final BindingResult bindingResult, final Model model,
			final HttpServletRequest request, final HttpServletResponse response, final RedirectAttributes redirectModel)
			throws CMSItemNotFoundException
	{
		bcfSignupValidator.validate(bcfSignupForm, bindingResult);
		try
		{
			return processRegisterUserRequest(referer, bcfSignupForm, bindingResult, model, request, response, redirectModel);
		}
		catch (final IllegalArgumentException ex)
		{
			LOGGER.error(ex.getMessage(), ex);
			model.addAttribute(SIGNUP_FORM, bcfSignupForm);
			GlobalMessages.addErrorMessage(model, "account.registration.service.error");
			return handleRegistrationError(model);
		}
	}

	private List<RegionData> getRegions(final String countryIsoCode)
	{
		if (StringUtils.isEmpty(countryIsoCode))
		{
			return Collections.emptyList();
		}
		return i18NFacade.getRegionsForCountryIso(countryIsoCode);
	}

	@Override
	public String handleRegistrationError(final Model model) throws CMSItemNotFoundException
	{
		model.addAttribute(new LoginForm());
		model.addAttribute(new GuestForm());
		model.addAttribute(COUNTRIES, travelI18NFacade.getAllCountries());
		return super.handleRegistrationError(model);
	}

	@RequestMapping(value = "/resendEmail", method = RequestMethod.GET)
	public String resendEmail(@RequestParam(value = EMAIL, required = true) final String email,
			final RedirectAttributes redirectModel)
			throws CMSItemNotFoundException
	{
		GlobalMessages.addFlashMessage(redirectModel, GlobalMessages.CONF_MESSAGES_HOLDER,
				"registration.verification.email.success.message");

		resendVerificationEmail(email);
		return REDIRECT_PREFIX + "/login";
	}

	protected void resendVerificationEmail(final String email
	)  // NOSONAR
	{
		final BCFCustomerFacade bcfcustomerfacade = (BCFCustomerFacade) customerFacade;
		bcfcustomerfacade.resendVerificationEmail(email);
	}

	/**
	 * This method takes data from the registration form accountActivationKey and create a new customer account and attempts to log in using
	 * the credentials of this new user.
	 *
	 * @return true if there are no binding errors or the account does not already exists.
	 * @throws CMSItemNotFoundException
	 */
	//@Override
	protected String processRegisterUserRequest(final String referer, final BCFSignupForm bcfSignupForm,
			final BindingResult bindingResult,
			final Model model, final HttpServletRequest request, final HttpServletResponse response,
			final RedirectAttributes redirectModel) throws CMSItemNotFoundException // NOSONAR
	{
		if (bindingResult.hasErrors())
		{
			model.addAttribute(SHOW_REGISTERATION_CONTENT, true);
			model.addAttribute(BcfstorefrontaddonWebConstants.REGIONS,getRegions(bcfSignupForm.getCountry()));
			model.addAttribute(SIGNUP_FORM, bcfSignupForm);
			addUserGroupsInContext(model);
			GlobalMessages.addErrorMessage(model, "please.try.again.error.message");
			return handleRegistrationError(model);
		}

		CustomerData customerData = null;
		final RegisterData data = populateRegisterData(bcfSignupForm);
		try
		{
			customerData = getCustomerFacade().getUserForUID(bcfSignupForm.getEmail());
		}
		catch (final UnknownIdentifierException ex)
		{
			LOGGER.debug("User not found with " + bcfSignupForm.getEmail() + ", creating account");
		}

		if (Objects.nonNull(customerData))
		{
			model.addAttribute(SHOW_REGISTERATION_CONTENT, true);
			model.addAttribute(SIGNUP_FORM, bcfSignupForm);
			addUserGroupsInContext(model);
			LOGGER.error("user already exists with " + bcfSignupForm.getEmail());
			bindingResult.rejectValue(EMAIL, USER_ALREADY_EXISTS);
			GlobalMessages.addErrorMessage(model, USER_ALREADY_EXISTS);
			return handleRegistrationError(model);
		}
		try
		{
			travelCustomerFacade.doRegister(data);
			model.addAttribute("asmLoggedIn", assistedServiceFacade.isAssistedServiceAgentLoggedIn());
			model.addAttribute(EMAIL, bcfSignupForm.getEmail());
		}
		catch (final DuplicateUidException e)
		{
			LOGGER.error(e.getMessage(), e);
			model.addAttribute(SHOW_REGISTERATION_CONTENT, true);
			model.addAttribute(SIGNUP_FORM, bcfSignupForm);
			addUserGroupsInContext(model);
			LOGGER.warn(REGISTRATION_FAILED + e);
			bindingResult.rejectValue(EMAIL, USER_ALREADY_EXISTS);
			GlobalMessages.addErrorMessage(model, FORM_GLOBAL_ERROR);
			return handleRegistrationError(model);
		}
		catch (final IntegrationException intEx)
		{
			LOGGER.error(intEx.getMessage(), intEx);
			model.addAttribute(SIGNUP_FORM, bcfSignupForm);
			addUserGroupsInContext(model);
			final StringBuilder errorDetails = new StringBuilder();
			errorDetails.append(REGISTRATION_FAILED);
			final ErrorListDTO errorListDTO = intEx.getErorrListDTO();
			if (Objects.nonNull(errorListDTO))
			{
				errorDetails.append(StreamUtil.safeStream(errorListDTO.getErrorDto()).findFirst().get().getErrorDetail());
			}
			LOGGER.warn(errorDetails.toString());
			GlobalMessages.addErrorMessage(model, errorDetails.toString());
		}
		storeCmsPageInModel(model, getContentPageForLabelOrId(REGISTER_SUCCESS));
		return getViewForPage(model);
	}

	private RegisterData populateRegisterData(final BCFSignupForm bcfSignupForm)
	{
		final RegisterData data = new RegisterData();
		data.setLogin(bcfSignupForm.getEmail());
		data.setPassword(bcfSignupForm.getPwd());
		data.setAccountType(bcfSignupForm.getAccountType());
		data.setCountry(bcfSignupForm.getCountry());
		data.setFirstName(bcfSignupForm.getFirstName());
		data.setLastName(bcfSignupForm.getLastName());
		data.setPhoneNo(bcfSignupForm.getPhoneNo());
		data.setProvince(bcfSignupForm.getProvince());
		data.setZipCode(bcfSignupForm.getZipCode());
		data.setUserGroup(bcfSignupForm.getUserGroup());
		return data;
	}

	@ModelAttribute("accountTypes")
	protected List<AccountTypeData> getAccountTypes()
	{
		return bcfCustomerFacade.getCustomerAccountTypes();
	}

	@ModelAttribute("asmSession")
	protected Boolean getAsmSession()
	{
		return Objects.nonNull(assistedServiceService.getAsmSession());
	}

	@Override
	protected CustomerFacade getCustomerFacade()
	{
		return customerFacade;
	}

	public void setCustomerFacade(final CustomerFacade customerFacade)
	{
		this.customerFacade = customerFacade;
	}

}
