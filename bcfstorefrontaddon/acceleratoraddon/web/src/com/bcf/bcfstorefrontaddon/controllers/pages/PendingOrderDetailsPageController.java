/*
 * Copyright (c) 2019 British Columbia Ferry Services Inc
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of British Columbia Ferry Services Inc("Confidential Information").
 * You shall not disclose such Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with British Columbia Ferry Services Inc.
 *
 * Last Modified: 26/06/19 20:25
 */

package com.bcf.bcfstorefrontaddon.controllers.pages;

import static com.bcf.bcfstorefrontaddon.constants.BcfstorefrontaddonWebConstants.FALSE;

import de.hybris.platform.acceleratorstorefrontcommons.controllers.pages.AbstractSearchPageController;
import de.hybris.platform.cms2.exceptions.CMSItemNotFoundException;
import de.hybris.platform.commerceservices.search.pagedata.PageableData;
import de.hybris.platform.commerceservices.search.pagedata.PaginationData;
import de.hybris.platform.commerceservices.search.pagedata.SearchPageData;
import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import org.apache.commons.lang3.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import com.bcf.bcfstorefrontaddon.constants.BcfstorefrontaddonControllerConstants;
import com.bcf.bcfstorefrontaddon.constants.BcfstorefrontaddonWebConstants;
import com.bcf.bcfstorefrontaddon.forms.AgentPendingOrderDecisionForm;
import com.bcf.core.exception.BcfOrderUpdateException;
import com.bcf.core.order.data.OnHoldOrderData;
import com.bcf.facades.order.BcfOrderFacade;


@Controller
@RequestMapping("/pendingOrderDetailsPage")
public class PendingOrderDetailsPageController extends AbstractSearchPageController
{
	protected static final Logger LOG = Logger.getLogger(PendingOrderDetailsPageController.class);
	private static final String PENDING_ORDER_DETAILS_CMS_PAGE = "pendingOrderDetailsPage";
	private static final String ORDER_DETAILS_CMS_PAGE = "asmOrderDetailsPage";
	private static final String MESSAGE = "message";

	@Resource(name = "orderFacade")
	private BcfOrderFacade bcfOrderFacade;

	@RequestMapping(method = RequestMethod.GET)
	public String getPage(@RequestParam(value = "page", defaultValue = "0") final int page,
			@RequestParam(value = "sort", required = false) final String sortCode,
			final Model model) throws CMSItemNotFoundException
	{
		// Handle paged search results
		final PageableData pageableData = createPageableData(page, 0, sortCode, ShowMode.All);
		final SearchPageData searchPageData = bcfOrderFacade.getPagedPendingOrdersForInventoryApproval(pageableData);

		populateModel(model, searchPageData, ShowMode.All);

		storeCmsPageInModel(model, getContentPageForLabelOrId(PENDING_ORDER_DETAILS_CMS_PAGE));
		setUpMetaDataForContentPage(model, getContentPageForLabelOrId(PENDING_ORDER_DETAILS_CMS_PAGE));
		model.addAttribute("searchPageData", searchPageData);
		return getViewForPage(model);
	}

	@RequestMapping(value = "/orderdetail/{orderCode}", method = RequestMethod.GET)
	public String getOrderDetail(@PathVariable final String orderCode, final Model model) throws CMSItemNotFoundException
	{
		OnHoldOrderData onHoldOrderData = bcfOrderFacade.getOrderForCode(orderCode);
		model.addAttribute("order", onHoldOrderData);
		model.addAttribute("orderCode", orderCode);
		model.addAttribute(BcfstorefrontaddonWebConstants.AGENT_PENDING_ORDER_DECISION_FORM, new AgentPendingOrderDecisionForm());
		storeCmsPageInModel(model, getContentPageForLabelOrId(ORDER_DETAILS_CMS_PAGE));
		setUpMetaDataForContentPage(model, getContentPageForLabelOrId(ORDER_DETAILS_CMS_PAGE));
		return getViewForPage(model);
	}

	@RequestMapping(value = "/updateentry", method = RequestMethod.POST)
	public String updateOrderEntry(
			@ModelAttribute(BcfstorefrontaddonWebConstants.AGENT_PENDING_ORDER_DECISION_FORM) final AgentPendingOrderDecisionForm agentPendingOrderDecisionForm,
			final HttpServletRequest request,
			final Model model) throws CMSItemNotFoundException
	{
		model.addAttribute("agentPendingOrderDecisionForm", agentPendingOrderDecisionForm);
		storeCmsPageInModel(model, getContentPageForLabelOrId(ORDER_DETAILS_CMS_PAGE));
		setUpMetaDataForContentPage(model, getContentPageForLabelOrId(ORDER_DETAILS_CMS_PAGE));
		return getViewForPage(model);
	}

	@Override
	protected void populateModel(final Model model, final SearchPageData<?> searchPageData, final ShowMode showMode)
	{
		super.populateModel(model, searchPageData, showMode);
		final PaginationData paginationData = searchPageData.getPagination();
		final int next = (paginationData.getCurrentPage() + 1 < paginationData.getNumberOfPages()) ?
				paginationData.getCurrentPage() + 1 :
				paginationData.getCurrentPage();
		final int previous = (paginationData.getCurrentPage() - 1 > 0) ? paginationData.getCurrentPage() - 1 : 0;

		model.addAttribute("naxtPage", next);
		model.addAttribute("previousPage", previous);
	}

	@RequestMapping(value = "/lock/{orderCode}", method = RequestMethod.POST)
	public String lockOrderAjax(@PathVariable final String orderCode, final Model model)
	{
		LOG.debug("Locking order " + orderCode);

		String lockOrderResult;
		try
		{
			lockOrderResult = bcfOrderFacade.lockOrderByAgent(orderCode);
			model.addAttribute(MESSAGE, lockOrderResult);
		}
		catch (BcfOrderUpdateException e)
		{
			//do nothing, this just mean that the order is already locked
		}
		return BcfstorefrontaddonControllerConstants.Views.Pages.Order.ASMOrderActionResponse;
	}

	@RequestMapping(value = "/unlock/{orderCode}", method = RequestMethod.POST)
	public String unlockOrderAjax(@PathVariable final String orderCode, final Model model)
	{
		LOG.debug("Unlocking order " + orderCode);

		String unlockOrderResult = bcfOrderFacade.unlockOrder(orderCode);
		if (Boolean.valueOf(unlockOrderResult))
		{
			model.addAttribute(MESSAGE, "text.page.pendingorders.actions.unlock.successful");
		}
		else if (StringUtils.isNotBlank(unlockOrderResult) && !StringUtils.equals(unlockOrderResult, FALSE))
		{
			model.addAttribute(MESSAGE, unlockOrderResult);
		}
		else
		{
			model.addAttribute(MESSAGE, "text.page.pendingorders.actions.unlock.failure");
		}
		return BcfstorefrontaddonControllerConstants.Views.Pages.Order.ASMOrderActionResponse;
	}

}
