/*
 * Copyright (c) 2019 British Columbia Ferry Services Inc
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of British Columbia Ferry Services Inc("Confidential Information").
 * You shall not disclose such Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with British Columbia Ferry Services Inc.
 *
 * Last Modified: 02/07/19 12:36
 */

package com.bcf.bcfstorefrontaddon.controllers.pages;

import static com.bcf.bcfstorefrontaddon.constants.BcfstorefrontaddonWebConstants.SERVICE_NOT_AVAILABLE;

import de.hybris.platform.acceleratorstorefrontcommons.annotations.RequireHardLogIn;
import de.hybris.platform.acceleratorstorefrontcommons.controllers.util.GlobalMessages;
import de.hybris.platform.assistedserviceservices.AssistedServiceService;
import de.hybris.platform.cms2.exceptions.CMSItemNotFoundException;
import de.hybris.platform.cms2.model.pages.ContentPageModel;
import de.hybris.platform.commercefacades.user.data.CustomerData;
import de.hybris.platform.enumeration.EnumerationService;
import de.hybris.platform.servicelayer.exceptions.ModelNotFoundException;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.commons.lang3.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;
import com.bcf.bcfstorefrontaddon.forms.BusinessOpportunitiesSubscriptionsForm;
import com.bcf.bcfstorefrontaddon.forms.ServiceNoticesSubscriptionsForm;
import com.bcf.core.enums.CustomerSubscriptionType;
import com.bcf.core.enums.SERVICE_SUPPLIES;
import com.bcf.facades.customer.BCFCustomerFacade;
import com.bcf.facades.customer.data.BusinessOpportunitiesData;
import com.bcf.facades.customer.data.BusinessOpportunitiesServiceSuppliesData;
import com.bcf.facades.customer.data.BusinessOpportunitiesSubscriptionsData;
import com.bcf.facades.subscription.CustomerSubscriptionsFacade;
import com.bcf.integrations.core.exception.IntegrationException;


@Controller
public class MyAccountSubscriptionsPageController extends BcfAbstractSearchPageController
{
	private static final Logger LOG = Logger.getLogger(MyAccountSubscriptionsPageController.class);
	// Internal Redirects
	private static final String REDIRECT_TO_SUBSCRIPTIONS_PAGE = REDIRECT_PREFIX + "/my-account/subscriptions";
	private static final String REDIRECT_TO_UNSUBSCRIBE_PAGE = REDIRECT_PREFIX + "/subscriptions/unsubscribe";
	private static final String SUBSCRIPTIONS_CMS_PAGE = "subscriptions";
	private static final String UNSUBSCRIBE_CMS_PAGE = "unsubscribePage";
	private static final String SERVICE_NOTICES_SUBSCRIPTIONS_FORM_ATTR = "serviceNoticesSubscriptionsForm";
	private static final String BUSINESS_OPPORTUNITIES_SUBSCRIPTIONS_FORM_ATTR = "businessOpportunitiesSubscriptionsForm";
	private static final String IS_ASM_USER = "isAsmUser";

	private static final String CUSTOMER_SUBSCRIPTIONS = "subscriptions";
	private static final String SERVICE_NOTICES_SUBSCRIBED = "totalServiceNoticesSubscribed";

	@Resource(name = "bcfCustomerFacade")
	private BCFCustomerFacade bcfCustomerFacade;

	@Resource(name = "customerSubscriptionsFacade")
	private CustomerSubscriptionsFacade customerSubscriptionsFacade;

	@Resource(name = "assistedServiceService")
	private AssistedServiceService assistedServiceService;

	@Resource(name = "enumerationService")
	private EnumerationService enumerationService;

	@RequestMapping(value = "/my-account/subscriptions", method = RequestMethod.GET)
	@RequireHardLogIn
	public String getSubscriptions(
			@RequestParam(value = "displayBusinessOpportunities", defaultValue = "false", required = false) final boolean displayBusinessOpportunities,
			final Model model) throws CMSItemNotFoundException
	{
		model.addAttribute(CUSTOMER_SUBSCRIPTIONS, customerSubscriptionsFacade.getAllSubscriptions());
		final CustomerData currentCustomer = bcfCustomerFacade.getCurrentCustomer();
		model.addAttribute("displayBusinessOpportunities", displayBusinessOpportunities);
		populateSubscriptionsForms(model, currentCustomer);

		model.addAttribute(IS_ASM_USER,
				assistedServiceService.getAsmSession() != null && assistedServiceService.getAsmSession().getAgent() != null);

		return returnToPage(model, SUBSCRIPTIONS_CMS_PAGE);

	}

	@RequestMapping(value = "/my-account/serviceNotices", method = RequestMethod.POST)
	@RequireHardLogIn
	public String updateServiceNoticesSubscriptions(final ServiceNoticesSubscriptionsForm serviceNoticesSubscriptionsForm,
			final Model model, final RedirectAttributes redirectAttributes)
	{
		try
		{
			List<String> routeCodes = Collections.emptyList();
			if (serviceNoticesSubscriptionsForm.isServiceNoticesOptedIn())
			{
				routeCodes = serviceNoticesSubscriptionsForm.getRouteCodes();
			}
			customerSubscriptionsFacade.createServiceNoticesSubscriptions(routeCodes);

			GlobalMessages
					.addFlashMessage(redirectAttributes, GlobalMessages.CONF_MESSAGES_HOLDER,
							"subscriptions.changes.successfully.submitted.label");
		}
		catch (final IntegrationException e)
		{
			LOG.error(e.getMessage(), e);
			GlobalMessages.addFlashMessage(redirectAttributes, GlobalMessages.ERROR_MESSAGES_HOLDER, SERVICE_NOT_AVAILABLE);
		}

		return REDIRECT_PREFIX + SUBSCRIPTIONS_CMS_PAGE;

	}

	@RequestMapping(value = "/my-account/businessOpportunities", method = RequestMethod.POST)
	@RequireHardLogIn
	public String updateBusinessOpportunitiesSubscriptions(
			final BusinessOpportunitiesSubscriptionsForm businessOpportunitiesSubscriptionsForm,
			final Model model, final RedirectAttributes redirectAttributes)
	{
		try
		{

			final BusinessOpportunitiesSubscriptionsData businessOpportunitiesSubscriptionsData = populateBusinessOpportunitiesData(
					businessOpportunitiesSubscriptionsForm);
			customerSubscriptionsFacade.createBusinessOpportunitiesSubscriptions(businessOpportunitiesSubscriptionsData);

			GlobalMessages
					.addFlashMessage(redirectAttributes, GlobalMessages.CONF_MESSAGES_HOLDER,
							"subscriptions.changes.successfully.submitted.label");
		}
		catch (final IntegrationException e)
		{
			LOG.error(e.getMessage(), e);
			GlobalMessages.addFlashMessage(redirectAttributes, GlobalMessages.ERROR_MESSAGES_HOLDER, SERVICE_NOT_AVAILABLE);
		}

		return REDIRECT_PREFIX + SUBSCRIPTIONS_CMS_PAGE;
	}

	private BusinessOpportunitiesSubscriptionsData populateBusinessOpportunitiesData(
			final BusinessOpportunitiesSubscriptionsForm businessOpportunitiesSubscriptionsForm)
	{
		final BusinessOpportunitiesSubscriptionsData businessOpportunitiesSubscriptionsData = new BusinessOpportunitiesSubscriptionsData();
		businessOpportunitiesSubscriptionsData.setOptedIn(businessOpportunitiesSubscriptionsForm.isBusinessOpportunitiesOptedIn());
		businessOpportunitiesSubscriptionsData.setCompanyName(businessOpportunitiesSubscriptionsForm.getCompanyName());
		businessOpportunitiesSubscriptionsData.setJobTitle(businessOpportunitiesSubscriptionsForm.getJobTitle());
		businessOpportunitiesSubscriptionsData
				.setServiceSupplies(businessOpportunitiesSubscriptionsForm.getServiceSupplies());
		return businessOpportunitiesSubscriptionsData;
	}

	@RequestMapping(value = "/my-account/newsRelease", method = { RequestMethod.GET, RequestMethod.POST })
	@RequireHardLogIn
	public String updateNewsReleaseSubscriptions(
			@RequestParam(value = "newsRelease", defaultValue = "false", required = true) final String newsRelease,
			final Model model, final RedirectAttributes redirectAttributes)
	{
		try
		{
			customerSubscriptionsFacade.createNewsReleaseSubscriptions(Boolean.valueOf(newsRelease).booleanValue());
			GlobalMessages
					.addFlashMessage(redirectAttributes, GlobalMessages.CONF_MESSAGES_HOLDER,
							"subscriptions.changes.successfully.submitted.label");
		}

		catch (final ModelNotFoundException e)
		{
			LOG.error(e.getMessage(), e);
			GlobalMessages.addFlashMessage(redirectAttributes, GlobalMessages.ERROR_MESSAGES_HOLDER, SERVICE_NOT_AVAILABLE);
		}

		catch (final IntegrationException e)
		{
			LOG.error(e.getMessage(), e);
			GlobalMessages.addFlashMessage(redirectAttributes, GlobalMessages.ERROR_MESSAGES_HOLDER, SERVICE_NOT_AVAILABLE);
		}



		return REDIRECT_PREFIX + SUBSCRIPTIONS_CMS_PAGE;

	}

	@RequestMapping(value = "/my-account/offersAndPromotions", method = { RequestMethod.GET, RequestMethod.POST })
	@RequireHardLogIn
	public String updateOffersAndPromotionsSubscriptions(
			@RequestParam(value = "offersAndPromotions", defaultValue = "false", required = true) final String offersAndPromotions,
			final Model model, final RedirectAttributes redirectAttributes)
	{
		try
		{
			customerSubscriptionsFacade.createOffersAndPromotionsSubscriptions(Boolean.valueOf(offersAndPromotions).booleanValue());

			GlobalMessages
					.addFlashMessage(redirectAttributes, GlobalMessages.CONF_MESSAGES_HOLDER,
							"subscriptions.changes.successfully.submitted.label");
		}
		catch (final IntegrationException e)
		{
			LOG.error(e.getMessage(), e);
			GlobalMessages.addFlashMessage(redirectAttributes, GlobalMessages.ERROR_MESSAGES_HOLDER, SERVICE_NOT_AVAILABLE);
		}

		return REDIRECT_PREFIX + SUBSCRIPTIONS_CMS_PAGE;

	}

	@RequestMapping(value = "/my-account/subscriptions/unsubscribe", method = RequestMethod.GET)
	@RequireHardLogIn
	public String unSubscribeAll(final Model model, final RedirectAttributes redirectAttributes)
	{
		try
		{
			customerSubscriptionsFacade.unsubscribeAll(null);
			GlobalMessages.addFlashMessage(redirectAttributes, GlobalMessages.INFO_MESSAGES_HOLDER,
					"text.unsubscribeAll.success", null);
		}
		catch (final IntegrationException e)
		{
			LOG.error(e.getMessage(), e);
			GlobalMessages.addFlashMessage(redirectAttributes, GlobalMessages.ERROR_MESSAGES_HOLDER,
					"text.unsubscribe.integrationExceptions.label", null);
		}
		catch (final UnsupportedEncodingException e)
		{
			LOG.error(e.getMessage(), e);
		}
		return REDIRECT_TO_SUBSCRIPTIONS_PAGE;
	}

	@RequestMapping(value = "/subscriptions/unsubscribe/{uId}", method = RequestMethod.GET)
	public String getUnsubscribeView(@PathVariable("uId") final String uId,
			@RequestParam(value = "subscriptionCodes", required = true) final String subscriptionCodes,
			final Model model, final RedirectAttributes redirectAttributes) throws CMSItemNotFoundException
	{

		try
		{
			customerSubscriptionsFacade.checkCustomerExists(uId);
			model.addAttribute("customerFound", true);
			model.addAttribute("customerId", uId);
			model.addAttribute("subscriptionCodes", subscriptionCodes);
		}
		catch (final ModelNotFoundException ex)
		{
			LOG.error(ex.getMessage(), ex);
			GlobalMessages.addErrorMessage(model, "text.unsubscribe.customerNotFound.label");
		}

		return returnToPage(model, UNSUBSCRIBE_CMS_PAGE);
	}

	@RequestMapping(value = "/subscriptions/unsubscribeAll/{customerId}", method = { RequestMethod.GET, RequestMethod.POST })
	public String emailUnSubscribe(@PathVariable("customerId") final String customerId, final HttpServletRequest request,
			final HttpServletResponse response, final RedirectAttributes redirectAttributes, final Model model)
			throws CMSItemNotFoundException
	{
		final String subscriptionCodes = request.getParameter("subscriptionCodes");
		try
		{
			customerSubscriptionsFacade.unsubscribe(customerId, subscriptionCodes);
			redirectAttributes.addFlashAttribute("updateSuccessful", true);
			if (StringUtils.equalsAnyIgnoreCase(subscriptionCodes, "unsubscribeAll"))
			{
				GlobalMessages.addFlashMessage(redirectAttributes, GlobalMessages.CONF_MESSAGES_HOLDER,
						"text.unsubscribeAll.from.email.successMessage.label", null);
			}
			else
			{
				GlobalMessages.addFlashMessage(redirectAttributes, GlobalMessages.CONF_MESSAGES_HOLDER,
						"text.unsubscribe.selected.services.desc1.label", new Object[] { subscriptionCodes });
			}
		}
		catch (final ModelNotFoundException | UnsupportedEncodingException ex)
		{
			LOG.error(ex.getMessage(), ex);
			GlobalMessages.addFlashMessage(redirectAttributes, GlobalMessages.ERROR_MESSAGES_HOLDER,
					"text.unsubscribe.customerNotFound.label", null);
		}

		catch (final IntegrationException e)
		{
			LOG.error(e.getMessage(), e);
			GlobalMessages.addFlashMessage(redirectAttributes, GlobalMessages.ERROR_MESSAGES_HOLDER,
					"text.unsubscribe.integrationExceptions.label", null);
		}
		return REDIRECT_TO_UNSUBSCRIBE_PAGE + "/" + customerId + "/?subscriptionCodes=" + subscriptionCodes;
	}

	private void populateSubscriptionsForms(final Model model, final CustomerData currentCustomer)
	{

		final BusinessOpportunitiesSubscriptionsForm businessOpportunitiesSubscriptionsForm = new BusinessOpportunitiesSubscriptionsForm();
		final ServiceNoticesSubscriptionsForm serviceNoticesSubscriptionsForm = new ServiceNoticesSubscriptionsForm();

		final List<String> serviceNoticesRouteCodes = new ArrayList<>();
		currentCustomer.getSubscriptionList().stream().forEach(customerSubscriptionData -> {
			if (customerSubscriptionData.isOptIn())
			{
				if (CustomerSubscriptionType.SERVICE_NOTICE.getCode().equals(customerSubscriptionData.getType())
						&& customerSubscriptionData.isOptIn())
				{
					serviceNoticesSubscriptionsForm.setServiceNoticesOptedIn(true);
					serviceNoticesRouteCodes.add(customerSubscriptionData.getCode());
				}
				else if (CustomerSubscriptionType.NEWS_RELEASE.getCode().equals(customerSubscriptionData.getType())
						&& customerSubscriptionData.isOptIn())
				{
					model.addAttribute("newsReleasesOptedIn", true);
				}
				else if (CustomerSubscriptionType.VACATION_NEWSLETTER.getCode().equals(customerSubscriptionData.getType())
						&& customerSubscriptionData.isOptIn())
				{
					model.addAttribute("offersAndPromotionsOptedIn", true);
				}
				else if (CustomerSubscriptionType.BUSINESS_OPPORTUNITY.getCode().equals(customerSubscriptionData.getType())
						&& customerSubscriptionData.isOptIn())
				{
					final BusinessOpportunitiesData businessOpportunities = customerSubscriptionData.getBusinessOpportunities();
					businessOpportunitiesSubscriptionsForm.setBusinessOpportunitiesOptedIn(true);
					businessOpportunitiesSubscriptionsForm.setCompanyName(businessOpportunities.getCompanyName());
					businessOpportunitiesSubscriptionsForm.setJobTitle(businessOpportunities.getJobTitle());

					businessOpportunitiesSubscriptionsForm.setServiceSupplies(businessOpportunities.getServiceSupplies());
				}
			}
		});

		serviceNoticesSubscriptionsForm
				.setRouteCodes(serviceNoticesRouteCodes);

		if (businessOpportunitiesSubscriptionsForm.isBusinessOpportunitiesOptedIn())
		{
			model.addAttribute("displayBusinessOpportunities", true);
		}
		model.addAttribute(SERVICE_NOTICES_SUBSCRIPTIONS_FORM_ATTR, serviceNoticesSubscriptionsForm);
		model.addAttribute(BUSINESS_OPPORTUNITIES_SUBSCRIPTIONS_FORM_ATTR, businessOpportunitiesSubscriptionsForm);
		model.addAttribute(SERVICE_NOTICES_SUBSCRIBED, serviceNoticesSubscriptionsForm.getRouteCodes().size());
	}

	protected String returnToPage(final Model model, final String pageId) throws CMSItemNotFoundException
	{
		final ContentPageModel contentPage = getContentPageForLabelOrId(pageId);
		storeCmsPageInModel(model, contentPage);
		setUpMetaDataForContentPage(model, contentPage);
		return getViewForPage(model);
	}

	@ModelAttribute("serviceSupplies")
	public List<BusinessOpportunitiesServiceSuppliesData> getServiceSupplies()
	{
		final List<BusinessOpportunitiesServiceSuppliesData> serviceSuppliesData = new ArrayList<>();
		final List<SERVICE_SUPPLIES> enumerationValues = enumerationService.getEnumerationValues(SERVICE_SUPPLIES.class);
		enumerationValues.stream().forEach(serviceSupply -> {
			final BusinessOpportunitiesServiceSuppliesData serviceSupplyData = new BusinessOpportunitiesServiceSuppliesData();
			serviceSupplyData.setCode(serviceSupply.getCode());
			serviceSupplyData.setName(enumerationService.getEnumerationName(serviceSupply));
			serviceSuppliesData.add(serviceSupplyData);
		});
		return serviceSuppliesData;
	}
}
