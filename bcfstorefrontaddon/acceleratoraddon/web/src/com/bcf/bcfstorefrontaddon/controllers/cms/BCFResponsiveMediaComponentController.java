/*
 * Copyright (c) 2019 British Columbia Ferry Services Inc
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of British Columbia Ferry Services Inc("Confidential Information").
 * You shall not disclose such Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with British Columbia Ferry Services Inc.
 *
 * Last Modified: 02/07/19 14:01
 */

package com.bcf.bcfstorefrontaddon.controllers.cms;

import de.hybris.platform.commerceservices.i18n.CommerceCommonI18NService;
import de.hybris.platform.core.model.media.MediaModel;
import de.hybris.platform.servicelayer.config.ConfigurationService;
import java.util.Locale;
import java.util.Map;
import java.util.Optional;
import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import org.apache.commons.lang.StringUtils;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import com.bcf.bcfstorefrontaddon.constants.BcfstorefrontaddonControllerConstants;
import com.bcf.bcfstorefrontaddon.enums.OverlayPosition;
import com.bcf.bcfstorefrontaddon.model.components.BCFResponsiveMediaComponentModel;
import com.bcf.core.strategies.responsive.BCFResponsiveMediaStrategy;


@Controller("BCFResponsiveMediaComponentController")
@RequestMapping(value = BcfstorefrontaddonControllerConstants.Actions.Cms.BCFResponsiveMediaComponent)
public class BCFResponsiveMediaComponentController
		extends SubstitutingCMSAddOnComponentController<BCFResponsiveMediaComponentModel>
{
	@Resource(name = "bcfResponsiveMediaStrategy")
	private BCFResponsiveMediaStrategy bcfResponsiveMediaStrategy;

	@Resource(name = "commerceCommonI18NService")
	private CommerceCommonI18NService commerceCommonI18NService;

	@Resource(name = "configurationService")
	private ConfigurationService configurationService;

	@Resource(name = "imageViewMap")
	private Map<String, String> imageViewMap;

	@Override
	protected void fillModel(final HttpServletRequest request, final Model model,
			final BCFResponsiveMediaComponentModel component)
	{
		final Locale currentLocale = commerceCommonI18NService.getCurrentLocale();
		Optional.ofNullable(component.getMedia(commerceCommonI18NService.getCurrentLocale()))
				.ifPresent(media -> setModelAttributes(model, component, media));
		model.addAttribute("videoUrl", component.getVideoUrl(currentLocale));
	}

	private void setModelAttributes(final Model model, final BCFResponsiveMediaComponentModel component,
			final MediaModel media)
	{
		final Locale currentLocale = commerceCommonI18NService.getCurrentLocale();
		model.addAttribute("dataMedia", bcfResponsiveMediaStrategy.getResponsiveJson(media));
		model.addAttribute("overlayText", component.getOverlayText(currentLocale));
		model.addAttribute("altText", media.getAltText());
		model.addAttribute("location", media.getLocationTag(currentLocale));
		model.addAttribute("caption", media.getCaption(currentLocale));
		model.addAttribute("featuredText", media.getFeaturedText(currentLocale));
		model.addAttribute("locationVerticalPos",
				media.getLocationVerticalPos() != null ? media.getLocationVerticalPos().getCode() :
						OverlayPosition.TOP.getCode());
		model.addAttribute("locationHorizontalPos", media.getLocationHorizontalPos() != null ?
				media.getLocationHorizontalPos().getCode() : OverlayPosition.RIGHT.getCode());
		model.addAttribute("links", component.getLinks());
		model.addAttribute("iconText", media.getIconText(currentLocale));
		model.addAttribute("description", component.getDescription(currentLocale));
		model.addAttribute("promoText", media.getPromoText(currentLocale));
		model.addAttribute("topText", component.getTopText(currentLocale));
		model.addAttribute("backgroundColour", StringUtils.isNotEmpty(component.getBackgroundColour()) ?
				component.getBackgroundColour() :
				configurationService.getConfiguration().getString("responsive.image.default.background.colour"));
	}

	@Override
	protected String getView(final BCFResponsiveMediaComponentModel component)
	{
		return imageViewMap.get(component.getImageType().getCode());
	}
}
