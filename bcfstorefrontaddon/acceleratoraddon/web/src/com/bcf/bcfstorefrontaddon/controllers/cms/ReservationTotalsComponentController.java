/*
 * Copyright (c) 2019 British Columbia Ferry Services Inc
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of British Columbia Ferry Services Inc("Confidential Information").
 * You shall not disclose such Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with British Columbia Ferry Services Inc.
 *
 * Last Modified: 06/06/19 20:21
 */

package com.bcf.bcfstorefrontaddon.controllers.cms;

import de.hybris.platform.acceleratorstorefrontcommons.controllers.util.GlobalMessages;
import de.hybris.platform.commercefacades.product.data.ImageData;
import de.hybris.platform.commercefacades.travel.enums.SuggestionType;
import de.hybris.platform.servicelayer.session.SessionService;
import de.hybris.platform.travelfacades.facades.BookingFacade;
import de.hybris.platform.travelfacades.facades.TravelCommercePriceFacade;
import de.hybris.platform.travelfacades.facades.TravelImageFacade;
import de.hybris.platform.travelservices.enums.BookingJourneyType;
import java.util.List;
import java.util.Map;
import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.collections.MapUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;
import org.springframework.web.util.WebUtils;
import com.bcf.bcfstorefrontaddon.constants.BcfstorefrontaddonControllerConstants;
import com.bcf.bcfstorefrontaddon.constants.BcfstorefrontaddonWebConstants;
import com.bcf.bcfstorefrontaddon.model.components.ReservationTotalsComponentModel;
import com.bcf.core.constants.BcfCoreConstants;
import com.bcf.facades.bcffacades.BCFGlobalReservationFacade;
import com.bcf.facades.cart.RemoveSailingResponseData;
import com.bcf.facades.constants.BcfFacadesConstants;
import com.bcf.facades.order.BcfTravelCartFacade;
import com.bcf.facades.reservation.data.BcfGlobalReservationData;
import com.bcf.integrations.core.exception.IntegrationException;


/**
 * Controller for Itinerary Totals Component
 */
@Controller("ReservationTotalsComponentController")
@RequestMapping(value = BcfstorefrontaddonControllerConstants.Actions.Cms.ReservationTotalsComponent)
public class ReservationTotalsComponentController extends SubstitutingCMSAddOnComponentController<ReservationTotalsComponentModel>
{
	private static final Logger LOG = Logger.getLogger(ReservationTotalsComponentController.class);
	private static final String DESTINATION_LOCATION = "destinationLocation";
	private static final String RADIUS = "radius";
	private static final String ARRIVAL_LOCATION_SUGGESTION_TYPE = "arrivalLocationSuggestionType";
	private static final String ARRIVAL_LOCATION = "arrivalLocation";
	private static final String REMOVE_SAILING_FAILED = "reservation.remove.sailing.integration.failure";

	@Resource(name = "travelImageFacade")
	private TravelImageFacade travelImageFacade;

	@Resource(name = "sessionService")
	private SessionService sessionService;

	@Resource(name = "bcfCartFacade")
	private BcfTravelCartFacade bcfCartFacade;

	@Resource(name = "bookingFacade")
	private BookingFacade bookingFacade;

	@Resource(name = "travelCommercePriceFacade")
	private TravelCommercePriceFacade travelCommercePriceFacade;

	@Resource(name = "globalReservationFacade")
	private BCFGlobalReservationFacade globalReservationFacade;

	@Override
	protected void fillModel(final HttpServletRequest request, final Model model, final ReservationTotalsComponentModel component)
	{
		final ImageData image = getImage(request);

		if (image != null)
		{
			model.addAttribute(BcfstorefrontaddonWebConstants.DESTINATION_LOCATION_IMAGE, image);
		}

		if (bcfCartFacade.hasSessionCart())
		{
			model.addAttribute(BcfstorefrontaddonWebConstants.RESERVATION_TOTAL, bcfCartFacade.getCartTotal());
			model.addAttribute(BcfstorefrontaddonWebConstants.AMOUNT_TO_PAY, bcfCartFacade.getAmountToPay());

			model.addAttribute(BcfstorefrontaddonWebConstants.RESERVATION_CODE,
					bcfCartFacade.isAmendmentCart() ? bcfCartFacade.getOriginalOrderCode() : bcfCartFacade.getCurrentCartCode());
		}
	}

	/**
	 * This method is responsible for refreshing itinerary component after ancillary is added or removed to/from cart
	 *
	 * @param componentUid
	 * @param request
	 * @param response
	 * @param model
	 * @return
	 */
	@RequestMapping(value = "/refresh", method = RequestMethod.GET)
	protected String getComponent(@RequestParam final String componentUid, final HttpServletRequest request,
			final HttpServletResponse response, final Model model)
	{
		request.setAttribute(COMPONENT_UID, componentUid);
		try
		{
			return handleGet(request, response, model);
		}
		catch (final Exception e)
		{
			LOG.error("Exception loading the component", e);
		}
		return StringUtils.EMPTY;
	}

	@RequestMapping(value = "emptyBasket")
	public String performEmptyBasket(final RedirectAttributes redir, final HttpServletRequest request)
	{
		List<RemoveSailingResponseData> removeSailingResponseDatas = null;
		try
		{
			removeSailingResponseDatas = bcfCartFacade.emptyBasket();
			if (CollectionUtils.isNotEmpty(removeSailingResponseDatas))
			{
				redir.addFlashAttribute(BcfstorefrontaddonWebConstants.CANCEL_BOOKING_FAILURE_SAILINGS_FROM_CART,
						removeSailingResponseDatas);
			}
		}
		catch (final IntegrationException ex)
		{
			LOG.error(ex.getMessage(), ex);
			GlobalMessages.addFlashMessage(redir, GlobalMessages.ERROR_MESSAGES_HOLDER, REMOVE_SAILING_FAILED);
		}
		if (!bcfCartFacade.isCurrentCartValid())
		{
			WebUtils.setSessionAttribute(request, BcfFacadesConstants.BCF_FARE_FINDER_FORM, null);
			sessionService.removeAttribute(BcfFacadesConstants.BCF_FARE_FINDER_FORM);
		}
		return REDIRECT_PREFIX + ROOT;
	}

	@ResponseBody
	@RequestMapping(value = "amend-booking-fees", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
	public Boolean amendBookingFees(@ModelAttribute(value = "amount") final String amount,
			@ModelAttribute(value = "cachekey") final String cachekey, final RedirectAttributes redir)
	{
		try
		{
			return bcfCartFacade.updateAmendBookingFeesPrice(amount, cachekey);
		}
		catch (final NumberFormatException ex)
		{
			return false;
		}
	}

	/**
	 * This method is responsible for fetching the image which will be displayed in itinerary totals component in
	 * following order: 1) If there is ACCOMMODATION_OFFERING_CODE in the BcfGlobalReservationData - get image for a location of that
	 * accommodation offering 2) Else if there is a destinationLocation parameter in request - get image for the city
	 * location part of that parameter 3) Else if there is an arrivalLocation parameter in request - get image for the
	 * location of transport facility with that code 4) Else if there is a radius parameter in request - it means that
	 * this is Google based search and therefore we don't provide image for this search -> return null 5) If the image is
	 * still null, try to get it from the session
	 *
	 * @param request
	 * @return image
	 */
	protected ImageData getImage(final HttpServletRequest request)
	{
		ImageData image = null;

		final BcfGlobalReservationData bcfGlobalReservationData = globalReservationFacade.getCurrentGlobalReservationData();

		final Map<String, String[]> parameterMap = request.getParameterMap();

		String accommodationOfferingCode = null;

		if (BookingJourneyType.BOOKING_PACKAGE
				.equals(sessionService.getAttribute(BcfCoreConstants.SESSION_BOOKING_JOURNEY)) && CollectionUtils
				.isNotEmpty(bcfGlobalReservationData.getPackageReservations().getPackageReservationDatas()))
		{
			accommodationOfferingCode = bcfGlobalReservationData.getPackageReservations().getPackageReservationDatas().get(0)
					.getAccommodationReservationData().getAccommodationReference().getAccommodationOfferingCode();
		}

		if (accommodationOfferingCode != null)
		{
			image = travelImageFacade.getImageForAccommodationOfferingLocation(accommodationOfferingCode);
		}
		else if (MapUtils.isNotEmpty(parameterMap))
		{
			if (parameterMap.keySet().contains(ARRIVAL_LOCATION) && parameterMap.keySet().contains(ARRIVAL_LOCATION_SUGGESTION_TYPE))
			{
				if (StringUtils.equalsIgnoreCase(SuggestionType.CITY.toString(),
						request.getParameter(ARRIVAL_LOCATION_SUGGESTION_TYPE)))
				{
					image = travelImageFacade.getImageForDestinationLocation(request.getParameter(ARRIVAL_LOCATION));
				}
				else if (StringUtils.equalsIgnoreCase(SuggestionType.AIRPORTGROUP.toString(),
						request.getParameter(ARRIVAL_LOCATION_SUGGESTION_TYPE)))
				{
					image = travelImageFacade.getImageForArrivalTransportFacility(request.getParameter(ARRIVAL_LOCATION));
				}
			}
			else if (parameterMap.keySet().contains(DESTINATION_LOCATION))
			{
				image = travelImageFacade.getImageForDestinationLocation(request.getParameter(DESTINATION_LOCATION));
			}
			else if (parameterMap.keySet().contains(RADIUS))
			{
				return null;
			}
		}

		if (image == null)
		{
			image = travelImageFacade.getImageFromCart();
		}

		return image;
	}
}
