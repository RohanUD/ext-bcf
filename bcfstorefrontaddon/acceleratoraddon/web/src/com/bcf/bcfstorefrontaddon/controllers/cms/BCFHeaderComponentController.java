/*
 * Copyright (c) 2019 British Columbia Ferry Services Inc
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of British Columbia Ferry Services Inc("Confidential Information").
 * You shall not disclose such Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with British Columbia Ferry Services Inc.
 *
 * Last Modified: 25/06/19 20:04
 */

package com.bcf.bcfstorefrontaddon.controllers.cms;

import de.hybris.platform.acceleratorcms.data.CmsPageRequestContextData;
import de.hybris.platform.acceleratorcms.services.CMSPageContextService;
import de.hybris.platform.cms2.model.contents.components.CMSLinkComponentModel;
import de.hybris.platform.cms2.model.restrictions.AbstractRestrictionModel;
import de.hybris.platform.cms2.servicelayer.data.RestrictionData;
import de.hybris.platform.cms2.servicelayer.services.CMSRestrictionService;
import de.hybris.platform.commerceservices.enums.CustomerType;
import de.hybris.platform.commerceservices.i18n.CommerceCommonI18NService;
import de.hybris.platform.core.model.user.CustomerModel;
import de.hybris.platform.servicelayer.user.UserService;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Optional;
import java.util.stream.Collectors;
import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang3.ObjectUtils;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import com.bcf.bcfstorefrontaddon.constants.BcfstorefrontaddonControllerConstants;
import com.bcf.bcfstorefrontaddon.enums.HeaderViewType;
import com.bcf.bcfstorefrontaddon.model.components.BCFCMSLinkComponentModel;
import com.bcf.bcfstorefrontaddon.model.components.BCFHeaderComponentModel;
import com.bcf.bcfstorefrontaddon.model.components.BCFNavigationNodeComponentModel;
import com.bcf.core.strategies.responsive.BCFResponsiveMediaStrategy;
import com.bcf.core.util.StreamUtil;


@Controller("BCFHeaderComponentController")
@RequestMapping(value = BcfstorefrontaddonControllerConstants.Actions.Cms.BCFHeaderComponent)
public class BCFHeaderComponentController extends SubstitutingCMSAddOnComponentController<BCFHeaderComponentModel>
{

	@Resource(name = "userService")
	private UserService userService;

	@Resource(name = "bcfResponsiveMediaStrategy")
	private BCFResponsiveMediaStrategy bcfResponsiveMediaStrategy;

	@Resource(name = "commerceCommonI18NService")
	private CommerceCommonI18NService commerceCommonI18NService;

	@Resource(name = "headerViewMap")
	private Map<HeaderViewType, String> headerViewMap;

	@Resource(name = "cmsRestrictionService")
	private CMSRestrictionService cmsRestrictionService;

	@Resource(name = "cmsPageContextService")
	private CMSPageContextService cmsPageContextService;

	@Override
	protected void fillModel(final HttpServletRequest request, final Model model,
			final BCFHeaderComponentModel component)
	{
		final CmsPageRequestContextData cmsPageRequestContextData = cmsPageContextService
				.getCmsPageRequestContextData(request);
		storeTopLinksInModel(model, component.getTopLinks(), cmsPageRequestContextData);
		model.addAttribute("navigationNodes", component.getNavigationNodes());
		final Map<String, String> promotionalmages = StreamUtil.safeStream(component.getNavigationNodes())
				.collect(Collectors.toMap(BCFNavigationNodeComponentModel::getUid, this::getResponsiveImage));
		model.addAttribute("logo", component.getLogo());
		model.addAttribute("promotionalmages", promotionalmages);
	}

	private void storeTopLinksInModel(final Model model, final List<BCFCMSLinkComponentModel> topLinks,
			final CmsPageRequestContextData cmsPageRequestContextData)
	{
		final boolean isAnonymousUser = userService.isAnonymousUser(userService.getCurrentUser());
		boolean isGuestUser = false;
		if (!isAnonymousUser)
		{
			isGuestUser = CustomerType.GUEST.equals(((CustomerModel) userService.getCurrentUser()).getType());

		}
		final List<CMSLinkComponentModel> anonymousUserCmsLinks = new ArrayList<>();
		final List<CMSLinkComponentModel> loggedInUserCmsLinks = new ArrayList<>();
		updateCMSLinks(isAnonymousUser, anonymousUserCmsLinks, loggedInUserCmsLinks, topLinks, cmsPageRequestContextData);
		model.addAttribute("topLinks", (isAnonymousUser || isGuestUser) ? anonymousUserCmsLinks : loggedInUserCmsLinks);
	}

	protected boolean isAllowed(final CMSLinkComponentModel component, final CmsPageRequestContextData cmsPageRequestContextData)
	{
		final RestrictionData restrictionData = (RestrictionData) cmsPageRequestContextData.getRestrictionData();
		final List<AbstractRestrictionModel> restrictions = component.getRestrictions();
		if (restrictions == null || restrictions.isEmpty())
		{
			return true;
		}
		else
		{
			return cmsRestrictionService.evaluateCMSComponent(component, restrictionData);
		}
	}

	private void updateCMSLinks(final boolean isAnonymousUser,
			final List<CMSLinkComponentModel> anonymousUserCmsLinks,
			final List<CMSLinkComponentModel> loggedInUserCmsLinks,
			final List<BCFCMSLinkComponentModel> topLinks,
			final CmsPageRequestContextData cmsPageRequestContextData)
	{
		for (final CMSLinkComponentModel cmsLinkComponentModel : topLinks)
		{
			if (cmsLinkComponentModel instanceof BCFCMSLinkComponentModel)
			{


				final BCFCMSLinkComponentModel bcfCmsLinkComponentModel = (BCFCMSLinkComponentModel) cmsLinkComponentModel;
				if (Objects.isNull(bcfCmsLinkComponentModel.getVisibleOnlyToAnonymousUser()))
				{
					anonymousUserCmsLinks.add(cmsLinkComponentModel);
					loggedInUserCmsLinks.add(cmsLinkComponentModel);
					continue;
				}
				updateCMSIfNotNull(isAnonymousUser, bcfCmsLinkComponentModel, loggedInUserCmsLinks, anonymousUserCmsLinks,
						cmsLinkComponentModel);
			}
			else
			{
				if (isAllowed(cmsLinkComponentModel, cmsPageRequestContextData))
				{
					anonymousUserCmsLinks.add(cmsLinkComponentModel);
					loggedInUserCmsLinks.add(cmsLinkComponentModel);
				}

			}
		}
	}

	private void updateCMSIfNotNull(final boolean isAnonymousUser, final BCFCMSLinkComponentModel bcfCmsLinkComponentModel,
			final List<CMSLinkComponentModel> loggedInUserCmsLinks, final List<CMSLinkComponentModel> anonymousUserCmsLinks,
			final CMSLinkComponentModel cmsLinkComponentModel)
	{
		if (!isAnonymousUser && !bcfCmsLinkComponentModel.getVisibleOnlyToAnonymousUser())
		{
			loggedInUserCmsLinks.add(cmsLinkComponentModel);
		}
		else if (isAnonymousUser && bcfCmsLinkComponentModel.getVisibleOnlyToAnonymousUser())
		{
			anonymousUserCmsLinks.add(cmsLinkComponentModel);
		}
	}

	private String getResponsiveImage(final BCFNavigationNodeComponentModel bcfNavigationNodeComponentModel)
	{
		return Optional
				.ofNullable(bcfNavigationNodeComponentModel
						.getPromotionalImage(commerceCommonI18NService.getCurrentLocale()))
				.map(bcfResponsiveMediaStrategy::getResponsiveJson).orElse(StringUtils.EMPTY);
	}

	@Override
	protected String getView(final BCFHeaderComponentModel component)
	{
		final HeaderViewType view = component.getView();
		if (ObjectUtils.allNotNull(view))
			return headerViewMap.get(view);

		else
			return super.getView(component);

	}


}
