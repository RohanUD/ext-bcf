/*
 * Copyright (c) 2019 British Columbia Ferry Services Inc
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of British Columbia Ferry Services Inc("Confidential Information").
 * You shall not disclose such Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with British Columbia Ferry Services Inc.
 *
 * Last Modified: 06/06/19 20:21
 */

package com.bcf.bcfstorefrontaddon.controllers.cms;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import com.bcf.bcfstorefrontaddon.constants.BcfstorefrontaddonControllerConstants;
import com.bcf.bcfstorefrontaddon.model.components.BCFNewsCarouselComponentModel;
import com.bcf.facades.servicenotice.ServiceNoticeFacade;


@Controller("BCFNewsCarouselComponentController")
@RequestMapping(value = BcfstorefrontaddonControllerConstants.Actions.Cms.BCFNewsCarouselComponent)
public class BCFNewsCarouselComponentController
		extends SubstitutingCMSAddOnComponentController<BCFNewsCarouselComponentModel>
{
	@Resource(name = "serviceNoticeFacade")
	private ServiceNoticeFacade serviceNoticeFacade;

	@Override
	protected void fillModel(final HttpServletRequest request, final Model model,
			final BCFNewsCarouselComponentModel component)
	{
		model.addAttribute("morePressReleases", component.getMorePressReleases());
		model.addAttribute("displayCount", component.getDisplayCount());
		model.addAttribute("news", serviceNoticeFacade.getNewsData());
	}
}
