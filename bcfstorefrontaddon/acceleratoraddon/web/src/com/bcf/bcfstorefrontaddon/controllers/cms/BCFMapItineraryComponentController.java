/*
 * Copyright (c) 2019 British Columbia Ferry Services Inc
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of British Columbia Ferry Services Inc("Confidential Information").
 * You shall not disclose such Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with British Columbia Ferry Services Inc.
 *
 * Last Modified: 06/06/19 20:21
 */

package com.bcf.bcfstorefrontaddon.controllers.cms;

import java.util.Map;
import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import com.bcf.bcfstorefrontaddon.constants.BcfstorefrontaddonControllerConstants;
import com.bcf.bcfstorefrontaddon.model.components.BCFMapItineraryComponentModel;
import com.bcf.core.strategies.responsive.BCFResponsiveMediaStrategy;


@Controller("BCFMapItineraryComponentController")
@RequestMapping(value = BcfstorefrontaddonControllerConstants.Actions.Cms.BCFMapItineraryComponent)
public class BCFMapItineraryComponentController
		extends SubstitutingCMSAddOnComponentController<BCFMapItineraryComponentModel>
{
	@Resource(name = "bcfResponsiveMediaStrategy")
	private BCFResponsiveMediaStrategy bcfResponsiveMediaStrategy;

	@Resource(name = "imageContentViewMap")
	private Map<String, String> imageContentViewMap;
	@Override
	protected void fillModel(final HttpServletRequest request, final Model model,
			final BCFMapItineraryComponentModel component)
	{
		model.addAttribute("dataMedias", component.getImageCarousel());
		model.addAttribute("startDay", component.getStartDay());
		model.addAttribute("paragraph", component.getContent());
	}

	@Override
	protected String getView(final BCFMapItineraryComponentModel component)
	{
		return imageContentViewMap.get(component.getContentViewType().getCode());
	}
}
