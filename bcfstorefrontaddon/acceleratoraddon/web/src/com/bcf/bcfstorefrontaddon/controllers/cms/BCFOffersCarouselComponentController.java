/*
 * Copyright (c) 2019 British Columbia Ferry Services Inc
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of British Columbia Ferry Services Inc("Confidential Information").
 * You shall not disclose such Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with British Columbia Ferry Services Inc.
 *
 * Last Modified: 06/06/19 20:21
 */

package com.bcf.bcfstorefrontaddon.controllers.cms;

import de.hybris.platform.commerceservices.i18n.CommerceCommonI18NService;
import java.util.Map;
import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import org.apache.commons.lang3.ObjectUtils;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import com.bcf.bcfstorefrontaddon.constants.BcfstorefrontaddonControllerConstants;
import com.bcf.bcfstorefrontaddon.model.components.BCFOffersCarouselComponentModel;


@Controller("BCFOffersCarouselComponentController")
@RequestMapping(value = BcfstorefrontaddonControllerConstants.Actions.Cms.BCFOffersCarouselComponent)
public class BCFOffersCarouselComponentController
		extends SubstitutingCMSAddOnComponentController<BCFOffersCarouselComponentModel>
{
	@Resource(name = "offersCarouselViewMap")
	private Map<String, String> offersCarouselViewMap;

	@Resource(name = "commerceCommonI18NService")
	private CommerceCommonI18NService commerceCommonI18NService;

	private static final String DEFAULT_BACKGROUND_COLOUR = "#f2f2f2";

	@Override
	protected void fillModel(final HttpServletRequest request, final Model model,
			final BCFOffersCarouselComponentModel component)
	{
		model.addAttribute("headingText", component.getHeadingText(commerceCommonI18NService.getCurrentLocale()));
		model.addAttribute("displayCount", component.getDisplayCount());
		model.addAttribute("backgroundColour",
				ObjectUtils.firstNonNull(component.getBackgroundColour(), DEFAULT_BACKGROUND_COLOUR));
		model.addAttribute("offers", component.getOffers());
	}

	@Override
	protected String getView(final BCFOffersCarouselComponentModel component)
	{
		return offersCarouselViewMap.get(component.getContentViewType().getCode());
	}
}
