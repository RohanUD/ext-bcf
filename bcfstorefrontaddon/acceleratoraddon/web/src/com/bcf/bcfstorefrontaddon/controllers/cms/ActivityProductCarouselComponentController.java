/*
 * Copyright (c) 2019 British Columbia Ferry Services Inc
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of British Columbia Ferry Services Inc("Confidential Information").
 * You shall not disclose such Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with British Columbia Ferry Services Inc.
 *
 * Last Modified: 02/07/19 14:01
 */

package com.bcf.bcfstorefrontaddon.controllers.cms;

import de.hybris.platform.commerceservices.i18n.CommerceCommonI18NService;
import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import com.bcf.bcfstorefrontaddon.constants.BcfstorefrontaddonControllerConstants;
import com.bcf.bcfstorefrontaddon.model.components.ActivityProductCarouselComponentModel;
import com.bcf.core.strategies.responsive.BCFResponsiveMediaStrategy;
import com.bcf.facades.ActivityProductFacade;


/**
 * Controller for CMS ActivityProductCarouselComponent
 */
@Controller("ActivityProductCarouselComponentController")
@RequestMapping(value = BcfstorefrontaddonControllerConstants.Actions.Cms.ActivityProductCarouselComponent)
public class ActivityProductCarouselComponentController
		extends SubstitutingCMSAddOnComponentController<ActivityProductCarouselComponentModel>
{
	@Resource(name = "bcfResponsiveMediaStrategy")
	private BCFResponsiveMediaStrategy bcfResponsiveMediaStrategy;


	@Resource(name = "activityProductFacade")
	private ActivityProductFacade activityProductFacade;

	@Resource(name = "commerceCommonI18NService")
	private CommerceCommonI18NService commerceCommonI18NService;

	@Override
	protected void fillModel(final HttpServletRequest request, final Model model,
			final ActivityProductCarouselComponentModel component)
	{
		model.addAttribute("products", activityProductFacade.findActivityProductsDetails(component.getProducts()));
		model.addAttribute("displayCount", component.getDisplayCount());
	}
}
