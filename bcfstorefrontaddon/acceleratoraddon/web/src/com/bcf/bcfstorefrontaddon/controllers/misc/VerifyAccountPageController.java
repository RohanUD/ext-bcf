/*
 * Copyright (c) 2019 British Columbia Ferry Services Inc
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of British Columbia Ferry Services Inc("Confidential Information").
 * You shall not disclose such Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with British Columbia Ferry Services Inc.
 *
 * Last Modified: 02/07/19 14:01
 */

package com.bcf.bcfstorefrontaddon.controllers.misc;

import de.hybris.platform.acceleratorstorefrontcommons.controllers.util.GlobalMessages;
import de.hybris.platform.servicelayer.session.SessionService;
import java.util.Objects;
import javax.annotation.Resource;
import org.apache.log4j.Logger;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;
import com.bcf.bcfstorefrontaddon.constants.BcfstorefrontaddonWebConstants;
import com.bcf.bcfstorefrontaddon.controllers.pages.BcfAbstractPageController;
import com.bcf.facades.customer.BCFCustomerFacade;


@Controller
@RequestMapping("/verifyAccount")
public class VerifyAccountPageController extends BcfAbstractPageController
{

	private static final Logger LOG = Logger.getLogger(VerifyAccountPageController.class);
	private static final String LOGIN = "/login";

	@Resource(name = "travelCustomerFacade")
	private BCFCustomerFacade travelCustomerFacade;

	@Resource
	private SessionService sessionService;

	@RequestMapping(method = RequestMethod.GET)
	public String verifyAccount(
			@RequestParam(required = true, value = "token", defaultValue = "") final String accountValidationKey,
			final RedirectAttributes redirectModel)
	{
		final String customerId = travelCustomerFacade.checkAndgetCustomerIdForAccountValidationToken(accountValidationKey);
		if (Objects.isNull(customerId))
		{
			LOG.error("Customer not found with accountValidationKey " + accountValidationKey);
			GlobalMessages.addFlashMessage(redirectModel, GlobalMessages.ERROR_MESSAGES_HOLDER, "user.not.associated.to.token");
			return REDIRECT_PREFIX + LOGIN;
		}
		else
		{
			if (travelCustomerFacade.checkEmailVerified(customerId))
			{
				GlobalMessages.addFlashMessage(redirectModel, GlobalMessages.INFO_MESSAGES_HOLDER, "account.verified.already");
			}
			else if (travelCustomerFacade.validateActivationToken(accountValidationKey, customerId))
			{
				sessionService.getCurrentSession()
						.setAttribute(BcfstorefrontaddonWebConstants.ACCOUNT_ACTIVATION_KEY, accountValidationKey);
				GlobalMessages
						.addFlashMessage(redirectModel, GlobalMessages.CONF_MESSAGES_HOLDER, "login.to.verification.email.message");

				if (travelCustomerFacade.isUpdateEmailRequested(customerId))
				{
					sessionService.setAttribute(BcfstorefrontaddonWebConstants.ACCOUNT_UPDATE_OLD_EMAIL, customerId);
					return REDIRECT_PREFIX + LOGIN + "?updateEmail=" + true;
				}
			}
			else
			{
				sessionService.getCurrentSession()
						.setAttribute(BcfstorefrontaddonWebConstants.ACCOUNT_ACTIVATION_KEY, accountValidationKey);
				GlobalMessages.addFlashMessage(redirectModel, GlobalMessages.ERROR_MESSAGES_HOLDER,
						"verification.email.token.expired.message");
			}
		}
		return REDIRECT_PREFIX + LOGIN;
	}
}
