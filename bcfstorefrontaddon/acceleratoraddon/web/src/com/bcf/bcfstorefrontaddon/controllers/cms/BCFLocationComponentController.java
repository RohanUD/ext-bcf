/*
 * Copyright (c) 2019 British Columbia Ferry Services Inc
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of British Columbia Ferry Services Inc("Confidential Information").
 * You shall not disclose such Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with British Columbia Ferry Services Inc.
 *
 * Last Modified: 26/06/19 20:25
 */

package com.bcf.bcfstorefrontaddon.controllers.cms;

import de.hybris.platform.commerceservices.i18n.CommerceCommonI18NService;
import java.util.Locale;
import java.util.Map;
import java.util.Optional;
import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import com.bcf.bcfstorefrontaddon.constants.BcfstorefrontaddonControllerConstants;
import com.bcf.bcfstorefrontaddon.model.components.BCFLocationComponentModel;
import com.bcf.core.accommodation.service.BcfTravelLocationService;
import com.bcf.core.strategies.responsive.BCFResponsiveMediaStrategy;


@Controller("BCFLocationComponentController")
@RequestMapping(value = BcfstorefrontaddonControllerConstants.Actions.Cms.BCFLocationComponent)
public class BCFLocationComponentController
		extends SubstitutingCMSAddOnComponentController<BCFLocationComponentModel>
{
	@Resource(name = "travelLocationService")
	private BcfTravelLocationService travelLocationService;

	@Resource(name = "commerceCommonI18NService")
	private CommerceCommonI18NService commerceCommonI18NService;

	@Resource(name = "bcfResponsiveMediaStrategy")
	private BCFResponsiveMediaStrategy bcfResponsiveMediaStrategy;

	@Resource(name = "locationViewTypeMap")
	private Map<String, String> locationViewTypeMap;

	@Override
	protected void fillModel(final HttpServletRequest request, final Model model, final BCFLocationComponentModel component)
	{
		Optional.ofNullable(travelLocationService.findBcfVacationLocationByCode(component.getLocationCode()))
				.ifPresent(locationModel ->
				{
					final Locale currentLocale = commerceCommonI18NService.getCurrentLocale();
					model.addAttribute("location", locationModel.getName(currentLocale));
					Optional.ofNullable(locationModel.getNormalImage()).ifPresent(locationMainImage ->
					{
						model.addAttribute("dataMedia", bcfResponsiveMediaStrategy.getResponsiveJson(locationMainImage));
						model.addAttribute("altText", locationMainImage.getAltText());
						model.addAttribute("caption", locationMainImage.getCaption(currentLocale));
						model.addAttribute("locationTag", locationMainImage.getLocationTag(currentLocale));
						model.addAttribute("featuredText", locationMainImage.getFeaturedText(currentLocale));
					});
					model.addAttribute("name", locationModel.getName(currentLocale));
					model.addAttribute("link", component.getLink());
					model.addAttribute("locationType", locationModel.getLocationType().getCode());
				});
	}

	@Override
	protected String getView(final BCFLocationComponentModel component)
	{
		return locationViewTypeMap.get(component.getContentViewType().getCode());
	}
}
