/*
 * Copyright (c) 2019 British Columbia Ferry Services Inc
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of British Columbia Ferry Services Inc("Confidential Information").
 * You shall not disclose such Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with British Columbia Ferry Services Inc.
 *
 * Last Modified: 21/06/19 00:21
 */

package com.bcf.bcfstorefrontaddon.controllers.pages;

import de.hybris.platform.acceleratorstorefrontcommons.annotations.RequireHardLogIn;
import de.hybris.platform.acceleratorstorefrontcommons.controllers.ThirdPartyConstants;
import de.hybris.platform.acceleratorstorefrontcommons.controllers.util.GlobalMessages;
import de.hybris.platform.cms2.exceptions.CMSItemNotFoundException;
import de.hybris.platform.cms2.model.pages.ContentPageModel;
import de.hybris.platform.commercefacades.travel.TravellerData;
import de.hybris.platform.travelfacades.facades.TravellerFacade;
import java.util.ArrayList;
import java.util.List;
import javax.annotation.Resource;
import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import com.bcf.bcfstorefrontaddon.breadcrumb.BcfContentPageBreadCrumbBuilder;
import com.bcf.bcfstorefrontaddon.constants.BcfstorefrontaddonControllerConstants;
import com.bcf.bcfstorefrontaddon.constants.BcfstorefrontaddonWebConstants;
import com.bcf.bcfstorefrontaddon.forms.BookingsAdvanceSearchForm;
import com.bcf.core.bookings.BookingsAdvanceSearchData;
import com.bcf.facades.constants.BcfFacadesConstants;
import com.bcf.facades.ebooking.SearchBookingFacade;
import com.bcf.integrations.core.exception.IntegrationException;


/**
 * Extended Controller for home page - adds functionality to AccountPageController
 */
@Controller
public class TravelAccountPageController extends BcfAbstractPageController
{
	private static final Logger LOG = Logger.getLogger(TravelAccountPageController.class);

	// CMS Pages
	protected static final String ACCOUNT_MY_BOOKINGS_CMS_PAGE = "my-bookings";
	protected static final String ACCOUNT_MY_VACATIONS_CMS_PAGE = "my-vacations";
	protected static final String ACCOUNT_DISABILITY_AND_MOBILITY_CMS_PAGE = "disability-and-mobility";
	protected static final String ERROR = "error";
	protected static final String SPECIAL_SERVICE_REQUESTS = "specialServiceRequests";
	protected static final String DISABILITY = "disability";
	protected static final String TRUE = "true";
	protected static final String FORM_GLOBAL_CONFIRMATION = "form.global.confirmation";
	protected static final String BREADCRUMBS = "breadcrumbs";
	private static final String BUSINESS_BOOKING_TYPE = "business";

	@Resource(name = "contentPageBreadcrumbBuilder")
	protected BcfContentPageBreadCrumbBuilder contentPageBreadcrumbBuilder;

	@Resource(name = "travellerFacade")
	private TravellerFacade travellerFacade;

	@Resource(name = "searchBookingFacade")
	private SearchBookingFacade searchBookingFacade;

	@RequestMapping(value = "/my-account/my-bookings", method = RequestMethod.GET)
	@RequireHardLogIn
	public String getMyBookingsPage(final Model model) throws CMSItemNotFoundException
	{
		final ContentPageModel contentPageModel = getContentPageForLabelOrId(ACCOUNT_MY_BOOKINGS_CMS_PAGE);
		storeCmsPageInModel(model, contentPageModel);
		setUpMetaDataForContentPage(model, contentPageModel);

		model.addAttribute(BREADCRUMBS, contentPageBreadcrumbBuilder.getBreadcrumbs(contentPageModel));
		model.addAttribute(ThirdPartyConstants.SeoRobots.META_ROBOTS, ThirdPartyConstants.SeoRobots.NOINDEX_NOFOLLOW);
		return getViewForPage(model);
	}

	@RequestMapping(value = "/my-account/my-vacations", method = RequestMethod.GET)
	@RequireHardLogIn
	public String getMyVacationsPage(final Model model) throws CMSItemNotFoundException
	{
		final ContentPageModel contentPageModel = getContentPageForLabelOrId(ACCOUNT_MY_VACATIONS_CMS_PAGE);
		storeCmsPageInModel(model, contentPageModel);
		setUpMetaDataForContentPage(model, contentPageModel);
		model.addAttribute(BREADCRUMBS, contentPageBreadcrumbBuilder.getBreadcrumbs(contentPageModel));
		model.addAttribute(ThirdPartyConstants.SeoRobots.META_ROBOTS, ThirdPartyConstants.SeoRobots.NOINDEX_NOFOLLOW);
		return getViewForPage(model);
	}

	@RequestMapping(value = "/my-account/my-bookings/{transport-bookings}/{booking-type}/{filter-criteria}/{page}", method = RequestMethod.POST)
	@RequireHardLogIn
	public String getTransportBookings(@PathVariable("transport-bookings") final String transportBookings,
			@PathVariable("booking-type") final String bookingType, @PathVariable("filter-criteria") final String filterCriteria,
			@PathVariable("page") final String pageNumber,
			final Model model)
	{
		if (StringUtils.equals(BcfstorefrontaddonWebConstants.TRANSPORT_BOOKINGS, transportBookings))
		{
			final int page = getPageNumber(pageNumber);
			try
			{
				if (StringUtils.equals(BcfFacadesConstants.UPCOMING, filterCriteria))
				{
					model.addAttribute(BcfFacadesConstants.TRANSPORT_BOOKINGS,
							searchBookingFacade.searchUpcomingBookings(page));
				}
				else if (StringUtils.equals(BcfFacadesConstants.PAST, filterCriteria))
				{
					model.addAttribute(BcfFacadesConstants.TRANSPORT_BOOKINGS,
							searchBookingFacade.searchPastBookings(page));
				}
			}
			catch (final IntegrationException e)
			{
				handleIntegrationException(model, e);
			}
		}
		return getTransportBookingsResponse(bookingType);
	}

	@RequestMapping(value = "/my-account/my-bookings/advanceSearch")
	@RequireHardLogIn
	public String getAdvanceSearchBookings(
			@ModelAttribute("bookingsAdvanceSearchForm") final BookingsAdvanceSearchForm bookingsAdvanceSearchForm,
			final Model model)
	{
		try
		{
			model.addAttribute(BcfFacadesConstants.TRANSPORT_BOOKINGS, searchBookingFacade
					.advanceSearchBookings(getPageNumber(bookingsAdvanceSearchForm.getPageNumber()),
							getBookingsAdvanceSearchData(bookingsAdvanceSearchForm)));
		}
		catch (final IntegrationException e)
		{
			handleIntegrationException(model, e);
		}
		return getTransportBookingsResponse(bookingsAdvanceSearchForm.getBookingType());
	}

	@RequestMapping(value = "/my-account/disability-and-mobility", method = RequestMethod.GET)
	@RequireHardLogIn
	public String getDisabilityAndMobilityPage(final Model model) throws CMSItemNotFoundException
	{
		final TravellerData traveller = travellerFacade.getCurrentUserDetails();

		if (traveller != null && traveller.getSpecialRequestDetail() != null)
		{
			final List<String> specialServiceRequestCodes = new ArrayList<>();

			traveller.getSpecialRequestDetail().getSpecialServiceRequests().forEach(ssr ->
					specialServiceRequestCodes.add(ssr.getCode()));
			model.addAttribute(SPECIAL_SERVICE_REQUESTS, specialServiceRequestCodes);
		}

		final ContentPageModel contentPageModel = getContentPageForLabelOrId(ACCOUNT_DISABILITY_AND_MOBILITY_CMS_PAGE);
		storeCmsPageInModel(model, contentPageModel);
		setUpMetaDataForContentPage(model, contentPageModel);
		model.addAttribute(BREADCRUMBS, contentPageBreadcrumbBuilder.getBreadcrumbs(contentPageModel));
		model.addAttribute(ThirdPartyConstants.SeoRobots.META_ROBOTS, ThirdPartyConstants.SeoRobots.NOINDEX_NOFOLLOW);
		return getViewForPage(model);
	}

	@RequestMapping(value = "/my-account/disability-and-mobility", method = RequestMethod.POST)
	@RequireHardLogIn
	public String updateDisabilityAndMobilityPage(@RequestParam(required = false) final String specialAssistance,
			final Model model) throws CMSItemNotFoundException
	{
		final List<String> specialServiceRequestCodes = new ArrayList<>();

		if (StringUtils.isNotEmpty(specialAssistance) && StringUtils.equalsIgnoreCase(specialAssistance, TRUE))
		{
			specialServiceRequestCodes.add(DISABILITY);
		}

		travellerFacade.updateCurrentUserSpecialRequestDetails(specialServiceRequestCodes);

		GlobalMessages.addInfoMessage(model, FORM_GLOBAL_CONFIRMATION);
		final ContentPageModel contentPageModel = getContentPageForLabelOrId(ACCOUNT_DISABILITY_AND_MOBILITY_CMS_PAGE);
		storeCmsPageInModel(model, contentPageModel);
		setUpMetaDataForContentPage(model, contentPageModel);
		model.addAttribute(BREADCRUMBS, contentPageBreadcrumbBuilder.getBreadcrumbs(contentPageModel));
		model.addAttribute(ThirdPartyConstants.SeoRobots.META_ROBOTS, ThirdPartyConstants.SeoRobots.NOINDEX_NOFOLLOW);

		return getDisabilityAndMobilityPage(model);
	}

	private BookingsAdvanceSearchData getBookingsAdvanceSearchData(final BookingsAdvanceSearchForm bookingsAdvanceSearchForm)
	{
		final BookingsAdvanceSearchData bookingsAdvanceSearchData = new BookingsAdvanceSearchData();
		bookingsAdvanceSearchData.setBookingReference(getSearchValue(bookingsAdvanceSearchForm.getBookingReference()));
		bookingsAdvanceSearchData.setDeparturePortCode(getSearchValue(bookingsAdvanceSearchForm.getDeparturePortCode()));
		bookingsAdvanceSearchData.setArrivalPortCode(getSearchValue(bookingsAdvanceSearchForm.getArrivalPortCode()));
		bookingsAdvanceSearchData.setDepartureDateTimeFrom(getSearchValue(bookingsAdvanceSearchForm.getDepartureDateTimeFrom()));
		bookingsAdvanceSearchData.setDepartureDateTimeTo(getSearchValue(bookingsAdvanceSearchForm.getDepartureDateTimeTo()));
		return bookingsAdvanceSearchData;
	}

	private String getSearchValue(final String searchFieldData)
	{
		if (StringUtils.isNotBlank(searchFieldData))
		{
			return searchFieldData.trim();
		}
		return null;
	}

	private String getTransportBookingsResponse(final String bookingType)
	{
		if (StringUtils.equalsIgnoreCase(BUSINESS_BOOKING_TYPE, bookingType))
		{
			return BcfstorefrontaddonControllerConstants.Views.Pages.TransportBookings.getTransportBusinessBookingsResponse;
		}
		return BcfstorefrontaddonControllerConstants.Views.Pages.TransportBookings.getTransportBookingsResponse;
	}

	private void handleIntegrationException(final Model model, final IntegrationException e)
	{
		model.addAttribute(BcfFacadesConstants.HAS_ERROR_FLAG, true);
		model.addAttribute(BcfFacadesConstants.ERROR_MESSAGE, e.getErorrListDTO().getErrorDto().get(0).getErrorDetail());
		LOG.error("IntegrationException encountered while calling search bookings service", e);
	}

	private int getPageNumber(final String pageNumber)
	{
		if (!StringUtils.isNumeric(pageNumber.trim()))
		{
			return BcfFacadesConstants.DEFAULT_PAGE_NUMBER;
		}
		return Math.max(Integer.parseInt(pageNumber.trim()), BcfFacadesConstants.DEFAULT_PAGE_NUMBER);
	}
}
