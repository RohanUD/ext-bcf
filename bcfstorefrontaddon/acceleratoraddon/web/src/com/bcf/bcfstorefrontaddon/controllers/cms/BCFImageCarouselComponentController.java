/*
 * Copyright (c) 2019 British Columbia Ferry Services Inc
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of British Columbia Ferry Services Inc("Confidential Information").
 * You shall not disclose such Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with British Columbia Ferry Services Inc.
 *
 * Last Modified: 06/06/19 20:21
 */

package com.bcf.bcfstorefrontaddon.controllers.cms;

import de.hybris.platform.commerceservices.i18n.CommerceCommonI18NService;
import java.util.Map;
import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import com.bcf.bcfstorefrontaddon.constants.BcfstorefrontaddonControllerConstants;
import com.bcf.bcfstorefrontaddon.model.components.BCFImageCarouselComponentModel;


@Controller("BCFImageCarouselComponentController")
@RequestMapping(value = BcfstorefrontaddonControllerConstants.Actions.Cms.BCFImageCarouselComponent)
public class BCFImageCarouselComponentController
		extends SubstitutingCMSAddOnComponentController<BCFImageCarouselComponentModel>
{
	@Resource(name = "imageCarouselViewMap")
	private Map<String, String> imageCarouselViewMap;

	@Resource(name = "commerceCommonI18NService")
	private CommerceCommonI18NService commerceCommonI18NService;

	@Override
	protected void fillModel(final HttpServletRequest request, final Model model,
			final BCFImageCarouselComponentModel component)
	{
		model.addAttribute("headingText", component.getHeadingText(commerceCommonI18NService.getCurrentLocale()));
		model.addAttribute("images", component.getImages());
		model.addAttribute("displayCount", component.getDisplayCount());
	}

	@Override
	protected String getView(final BCFImageCarouselComponentModel component)
	{
		return imageCarouselViewMap.get(component.getContentViewType().getCode());
	}
}
