/*
 * Copyright (c) 2019 British Columbia Ferry Services Inc
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of British Columbia Ferry Services Inc("Confidential Information").
 * You shall not disclose such Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with British Columbia Ferry Services Inc.
 *
 * Last Modified: 02/07/19 14:01
 */

package com.bcf.bcfstorefrontaddon.controllers.pages;

import de.hybris.platform.cms2.exceptions.CMSItemNotFoundException;
import de.hybris.platform.travelservices.enums.LocationType;
import java.util.Optional;
import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import com.bcf.facades.location.BcfTravelLocationFacade;
import com.bcf.facades.travel.data.BcfLocationPaginationData;


/**
 * Vacations Cities Landing Page Controller
 */
@Controller
@RequestMapping(value = "/cities-landing")
public class VacationCitiesPageController extends BcfAbstractPageController
{
	private static final String CITIES_LANDING_PAGE_ID = "vacationCitiesLandingPage";
	private static final String REGIONS_LIST = "regionsList";
	private static final String ALL = "ALL";
	private static final String REGION_FILTER_VALUE = "regionFilterValue";

	@Resource(name = "bcfTravelLocationFacade")
	private BcfTravelLocationFacade bcfTravelLocationFacade;

	@RequestMapping(method = RequestMethod.GET)
	public String getCities(final Model model, final HttpServletRequest request,
			@RequestParam(value = REGION_FILTER_VALUE, required = false) final String regionFilterValue)
			throws CMSItemNotFoundException
	{
		final BcfLocationPaginationData bcfLocationPaginationData = Optional.ofNullable(regionFilterValue)
				.map(regionFilter -> findCitiesByRegion(regionFilter, getPageSize(CITIES_LANDING_PAGE_ID), getCurrentPage(request)))
				.orElse(findAllCities(getPageSize(CITIES_LANDING_PAGE_ID), getCurrentPage(request)));
		model.addAttribute(REGIONS_LIST,
				bcfTravelLocationFacade.getVacationLocationsByType(LocationType.GEOGRAPHICAL_AREA.getCode()));
		storePaginationResults(model, bcfLocationPaginationData.getResults(),
				bcfLocationPaginationData.getPaginationData());
		storeCmsPageInModel(model, getContentPageForLabelOrId(CITIES_LANDING_PAGE_ID));
		setUpMetaDataForContentPage(model, getContentPageForLabelOrId(CITIES_LANDING_PAGE_ID));
		return getViewForPage(model);
	}

	private BcfLocationPaginationData findCitiesByRegion(final String regionFilter, final int pageSize, final int currentPage)
	{
		if (ALL.equals(regionFilter))
		{
			return findAllCities(pageSize, currentPage);
		}
		return bcfTravelLocationFacade.getPaginatedCitiesByRegion(regionFilter, pageSize, currentPage);
	}

	private BcfLocationPaginationData findAllCities(final int pageSize, final int currentPage)
	{
		return bcfTravelLocationFacade.getPaginatedLocationsByType(LocationType.CITY.getCode(), pageSize,
				currentPage);
	}
}
