/*
 * Copyright (c) 2019 British Columbia Ferry Services Inc
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of British Columbia Ferry Services Inc("Confidential Information").
 * You shall not disclose such Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with British Columbia Ferry Services Inc.
 *
 * Last Modified: 01/07/19 15:16
 */

package com.bcf.bcfstorefrontaddon.controllers.pages;

import de.hybris.platform.acceleratorstorefrontcommons.controllers.util.GlobalMessages;
import de.hybris.platform.cms2.exceptions.CMSItemNotFoundException;
import de.hybris.platform.cms2.model.pages.ContentPageModel;
import de.hybris.platform.commercefacades.travel.TravelAdvisoryData;
import de.hybris.platform.servicelayer.session.SessionService;
import java.util.Optional;
import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;
import com.bcf.core.model.TravelAdvisoryModel;
import com.bcf.facades.traveladvisory.TravelAdvisoryFacade;


@Controller
@RequestMapping(value = "/travel-advisories")
public class TravelAdvisoryPageController extends BcfAbstractPageController
{
	private static final String TRAVEL_ADVISORY_CODE_PATTERN = "/{travelAdvisoryCode}";
	private static final String TRAVEL_ADVISORY_HOME_PAGE = "travelAdvisoryHomePage";
	private static final String TRAVEL_ADVISORY_DETAILS_PAGE = "travelAdvisoryDetailsPage";
	private static final String TRAVEL_ADVISORY_LIST_PAGE = "travelAdvisoryHomePage";
	private static final String RESPONSE_STATUS = "org.springframework.web.servlet.View.responseStatus";
	private static final String TEXT_TRAVEL_ADVISORY_NOT_FOUND = "text.travel.advisory.not.found";
	private static final String SUPPRESS_ALERT_URL = "/suppress-alert";
	private static final String SUPPRESS_ALERT = "suppressAlert";
	private static final String RESPORE_RIBBON_URL = "/restore-ribbon";
	private static final String TRAVEL_ADVISORIES_LIST = "traveladvisorieslist";

	@Resource(name = "travelAdvisoryFacade")
	private TravelAdvisoryFacade travelAdvisoryFacade;

	@Resource(name = "sessionService")
	private SessionService sessionService;

	@RequestMapping(method = RequestMethod.GET)
	public String listTravelAdvisories(final Model model) throws CMSItemNotFoundException
	{
		model.addAttribute(TRAVEL_ADVISORIES_LIST, travelAdvisoryFacade.findActiveTravelAdvisories());
		final ContentPageModel contentPageModel = getContentPageForLabelOrId(TRAVEL_ADVISORY_LIST_PAGE);
		storeCmsPageInModel(model, contentPageModel);
		setUpMetaDataForContentPage(model, contentPageModel);
		return getViewForPage(model);

	}

	@RequestMapping(value = TRAVEL_ADVISORY_CODE_PATTERN, method = RequestMethod.GET)
	public String displayTravelAdvisoryDetails(final HttpServletRequest request, @PathVariable final String travelAdvisoryCode,
			final Model model, final RedirectAttributes redirectModel) throws CMSItemNotFoundException
	{
		final Optional<TravelAdvisoryData> travelAdvisory = travelAdvisoryFacade.getTravelAdvisoryByCode(travelAdvisoryCode);
		if (travelAdvisory.isPresent())
		{
			return displayTravelAdvisoryDetails(model, travelAdvisory.get());
		}
		return redirectToTravelAdvisoriesHomePage(request, redirectModel);
	}

	private String displayTravelAdvisoryDetails(final Model model, final TravelAdvisoryData travelAdvisory)
			throws CMSItemNotFoundException
	{
		model.addAttribute(TravelAdvisoryModel._TYPECODE, travelAdvisory);
		final ContentPageModel contentPageModel = getContentPageForLabelOrId(TRAVEL_ADVISORY_DETAILS_PAGE);
		storeCmsPageInModel(model, contentPageModel);
		setUpMetaDataForContentPage(model, contentPageModel);
		return getViewForPage(model);
	}

	private String redirectToTravelAdvisoriesHomePage(final HttpServletRequest request, final RedirectAttributes redirectModel)
			throws CMSItemNotFoundException
	{
		final ContentPageModel contentPageModel = getContentPageForLabelOrId(TRAVEL_ADVISORY_HOME_PAGE);
		request.setAttribute(RESPONSE_STATUS, HttpStatus.MOVED_PERMANENTLY);
		GlobalMessages.addFlashMessage(redirectModel, GlobalMessages.ERROR_MESSAGES_HOLDER, TEXT_TRAVEL_ADVISORY_NOT_FOUND);
		return REDIRECT_PREFIX + contentPageModel.getLabel();
	}

	@RequestMapping(value = SUPPRESS_ALERT_URL, method = RequestMethod.GET)
	@ResponseBody
	public void suppressTravelAdvisoryAlert(final HttpServletRequest request)
	{
		request.getSession().setAttribute(SUPPRESS_ALERT, true);
	}

	@RequestMapping(value = RESPORE_RIBBON_URL, method = RequestMethod.GET)
	@ResponseBody
	public void restoreTravelAdvisoryRibbon(final HttpServletRequest request)
	{
		request.getSession().removeAttribute(SUPPRESS_ALERT);
	}
}
