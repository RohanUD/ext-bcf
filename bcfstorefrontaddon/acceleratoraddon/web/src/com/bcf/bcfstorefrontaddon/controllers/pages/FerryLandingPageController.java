/*
 * Copyright (c) 2019 British Columbia Ferry Services Inc
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of British Columbia Ferry Services Inc("Confidential Information").
 * You shall not disclose such Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with British Columbia Ferry Services Inc.
 *
 * Last Modified: 02/07/19 14:01
 */

package com.bcf.bcfstorefrontaddon.controllers.pages;

import reactor.util.StringUtils;

import de.hybris.platform.cms2.exceptions.CMSItemNotFoundException;
import java.util.Arrays;
import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import com.bcf.facades.shipinfo.BcfShipInfoFacade;
import com.bcf.facades.travel.data.ShipInfoPaginationData;


/**
 * Ferry Landing Page Controller
 */
@Controller
@RequestMapping(value = "/on-the-ferry/our-fleet")
public class FerryLandingPageController extends BcfAbstractPageController
{
	private static final String FERRY_LANDING_PAGE_ID = "ferryLandingPage";
	private static final String TRANSPORT_VEHICLES_LIST = "transportVehiclesList";
	private static final String ALL = "ALL";
	private static final String SHIP_CODE_FILTER = "shipCodeFilter";

	@Resource(name = "shipInfoFacade")
	private BcfShipInfoFacade bcfShipInfoFacade;

	@RequestMapping(method = RequestMethod.GET)
	public String getShips(final Model model, final HttpServletRequest request,
			@RequestParam(value = SHIP_CODE_FILTER, required = false) final String shipCodeFilter)
			throws CMSItemNotFoundException
	{
		model.addAttribute(TRANSPORT_VEHICLES_LIST, bcfShipInfoFacade.getShipInfos());

		if (StringUtils.isEmpty(shipCodeFilter) || ALL.equals(shipCodeFilter))
		{
			final ShipInfoPaginationData paginatedShipInfos = bcfShipInfoFacade
					.getPaginatedShipInfos(getPageSize(FERRY_LANDING_PAGE_ID), getCurrentPage(request));
			storePaginationResults(model, paginatedShipInfos.getResults(), paginatedShipInfos.getPaginationData());
		}
		else
		{
			storePaginationResults(model, Arrays.asList(bcfShipInfoFacade.getShipInfoByCode(shipCodeFilter)),
					null);
		}
		storeCmsPageInModel(model, getContentPageForLabelOrId(FERRY_LANDING_PAGE_ID));
		setUpMetaDataForContentPage(model, getContentPageForLabelOrId(FERRY_LANDING_PAGE_ID));
		return getViewForPage(model);
	}

}
