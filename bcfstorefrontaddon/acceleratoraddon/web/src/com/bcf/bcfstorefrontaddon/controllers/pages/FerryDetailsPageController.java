/*
 * Copyright (c) 2019 British Columbia Ferry Services Inc
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of British Columbia Ferry Services Inc("Confidential Information").
 * You shall not disclose such Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with British Columbia Ferry Services Inc.
 *
 * Last Modified: 02/07/19 14:01
 */

package com.bcf.bcfstorefrontaddon.controllers.pages;

import de.hybris.platform.cms2.exceptions.CMSItemNotFoundException;
import de.hybris.platform.cms2.model.pages.ContentPageModel;
import java.util.Objects;
import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import com.bcf.facades.ship.BcfShipInfoData;
import com.bcf.facades.shipinfo.BcfShipInfoFacade;


@Controller
@RequestMapping(value = "/on-the-ferry/our-fleet/")
public class FerryDetailsPageController extends BcfAbstractPageController
{
	private static final String FERRY_NAME_PATTERN = "{ferryName}";
	private static final String FERRY_CODE_PATTERN = "{ferryCode}";

	private static final String FERRY_DETAILS_CMS_PAGE = "ferryDetailsPage";

	@Resource(name = "shipInfoFacade")
	private BcfShipInfoFacade bcfShipInfoFacade;

	@RequestMapping(value = FERRY_NAME_PATTERN + "/" + FERRY_CODE_PATTERN, method = RequestMethod.GET)
	public String getFerryDetailsPage(@PathVariable final String ferryName, @PathVariable final String ferryCode,
			final Model model, final HttpServletRequest request, final HttpServletResponse response) throws CMSItemNotFoundException
	{
		final BcfShipInfoData shipInfo = bcfShipInfoFacade.getShipInfoByCode(ferryCode);
		if (Objects.nonNull(shipInfo))
		{
			model.addAttribute("transportVehiclesList", bcfShipInfoFacade.getShipInfos());
			model.addAttribute("transportVehicleInfo", shipInfo);
			final ContentPageModel contentPageModel = getContentPageForLabelOrId(FERRY_DETAILS_CMS_PAGE);
			storeCmsPageInModel(model, contentPageModel);
			setUpMetaDataForContentPage(model, contentPageModel);

			final String[] arguments = { ferryName };
			setUpMetaDataForContentPage(model, contentPageModel, arguments);
			setUpDynamicOGGraphData(model, contentPageModel, request, arguments);
			return getViewForPage(model);
		}
		else
		{
			prepareNotFoundPage(model, response);
			return getViewForPage(model);
		}
	}
}
