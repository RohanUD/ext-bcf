/*
 * Copyright (c) 2019 British Columbia Ferry Services Inc
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of British Columbia Ferry Services Inc("Confidential Information").
 * You shall not disclose such Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with British Columbia Ferry Services Inc.
 *
 * Last Modified: 02/07/19 14:01
 */

package com.bcf.bcfstorefrontaddon.controllers.pages;

import de.hybris.platform.cms2.exceptions.CMSItemNotFoundException;
import de.hybris.platform.cms2.model.pages.ContentPageModel;
import de.hybris.platform.commerceservices.search.pagedata.PageableData;
import de.hybris.platform.servicelayer.config.ConfigurationService;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Set;
import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import com.bcf.bcfstorefrontaddon.constants.BcfstorefrontaddonWebConstants;
import com.bcf.facades.accommodationoffering.search.AccommodationOfferingSolrSearchFacade;
import com.bcf.facades.location.BcfTravelLocationFacade;
import com.bcf.facades.packagedeal.search.PackageSolrSearchFacade;
import com.bcf.facades.packagedeal.search.response.data.PackageResponseData;
import com.bcf.facades.search.facetdata.PackageSearchPageData;
import com.bcf.facades.travel.data.BcfLocationData;
import com.google.common.collect.Sets;


@Controller
@RequestMapping(value = "/destinations/{destinationName}")
public class DestinationDetailsPageController extends BcfAbstractPageController
{
	private static final String DESTINATION_CITY_DETAILS = "vacationDestinationCityDetailsPage";
	private static final String DESTINATION_REGION_DETAILS = "vacationDestinationRegionDetailsPage";
	private static final String BCF_VACATION_LOCATION_CODE = "bcfVacationLocationCode";

	@Resource(name = "bcfTravelLocationFacade")
	private BcfTravelLocationFacade bcfTravelLocationFacade;

	@Resource(name = "packageSolrSearchFacade")
	private PackageSolrSearchFacade packageSolrSearchFacade;

	@Resource(name = "bcfAccommodationOfferingSolrSearchFacade")
	private AccommodationOfferingSolrSearchFacade accommodationOfferingSolrSearchFacade;

	@Resource(name = "configurationService")
	private ConfigurationService configurationService;

	@RequestMapping(value = "/{destinationCode}", method = RequestMethod.GET)
	public String getDestinationDetailsPage(@PathVariable("destinationCode") final String destinationCode,
			final Model model, final HttpServletRequest request, final HttpServletResponse response) throws CMSItemNotFoundException
	{
		final BcfLocationData bcfVacationLocationData = bcfTravelLocationFacade.getLocation(destinationCode);
		if (Objects.nonNull(bcfVacationLocationData))
		{
			model.addAttribute(BcfstorefrontaddonWebConstants.DESTINATION_DETAILS, bcfVacationLocationData);
			final List<PackageResponseData> packages = getPackages(bcfVacationLocationData.getCode());
			if (CollectionUtils.isNotEmpty(packages))
			{
				model.addAttribute(BcfstorefrontaddonWebConstants.DESTINATION_PACKAGES, packages);
			}
			else
			{
				final PageableData pageableData = new PageableData();
				pageableData.setCurrentPage(0);
				final Map<String, Set<String>> solrParamBcfVacationLocationCode = Collections
						.singletonMap(BCF_VACATION_LOCATION_CODE, Sets.newHashSet(bcfVacationLocationData.getCode()));
				model.addAttribute(BcfstorefrontaddonWebConstants.DESTINATION_ACCOMMODATIONS,
						accommodationOfferingSolrSearchFacade.searchHotels(solrParamBcfVacationLocationCode, pageableData));
			}
			return getViewForPageByLocation(model, bcfVacationLocationData, request);
		}
		else
		{
			prepareNotFoundPage(model, response);
			return getViewForPage(model);
		}
	}

	private String getViewForPageByLocation(final Model model, final BcfLocationData location,
			final HttpServletRequest request) throws CMSItemNotFoundException
	{
		String parentCategoryName = "";
		if (CollectionUtils.isNotEmpty(location.getSuperlocation()))
		{
			final BcfLocationData bcfLocationData = location.getSuperlocation().get(0);
			if (StringUtils.isNoneEmpty(bcfLocationData.getName()))
			{
				parentCategoryName = bcfLocationData.getName();
			}
		}

		ContentPageModel contentPageModel = getContentPageForLabelOrId(DESTINATION_REGION_DETAILS);
		model.addAttribute(BcfstorefrontaddonWebConstants.ACTIVITIES,
				bcfTravelLocationFacade.getActivityProductsByDestination(location.getCode()));
		model.addAttribute(BcfstorefrontaddonWebConstants.IS_REGION_DETAILS_PAGE, true);
		if (StringUtils.equalsIgnoreCase(BcfstorefrontaddonWebConstants.CITY, location.getLocationType()))
		{
			model.addAttribute(BcfstorefrontaddonWebConstants.IS_REGION_DETAILS_PAGE, false);
			contentPageModel = getContentPageForLabelOrId(DESTINATION_CITY_DETAILS);
		}

		storeCmsPageInModel(model, contentPageModel);
		setUpMetaDataForContentPage(model, contentPageModel);


		final String[] arguments = { location.getName(), parentCategoryName };
		setUpMetaDataForContentPage(model, contentPageModel, arguments);
		setUpDynamicOGGraphData(model, contentPageModel, request, arguments);

		setUpMetaDataRobotsForContentPage(model, contentPageModel.getMetaIndexable(), contentPageModel.getMetaFollow());
		return getViewForPage(model);
	}

	private List<PackageResponseData> getPackages(final String bcfVacationLocationCode)
	{
		final PackageSearchPageData packageSearchPageData = packageSolrSearchFacade
				.searchByBcfVacationLocationCode(bcfVacationLocationCode);
		if (Objects.nonNull(packageSearchPageData))
		{
			return packageSearchPageData.getResults();
		}
		return Collections.emptyList();
	}
}
