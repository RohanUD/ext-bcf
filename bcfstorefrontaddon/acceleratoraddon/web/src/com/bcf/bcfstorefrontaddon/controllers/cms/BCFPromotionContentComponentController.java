/*
 * Copyright (c) 2019 British Columbia Ferry Services Inc
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of British Columbia Ferry Services Inc("Confidential Information").
 * You shall not disclose such Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with British Columbia Ferry Services Inc.
 *
 * Last Modified: 06/06/19 20:21
 */

package com.bcf.bcfstorefrontaddon.controllers.cms;

import de.hybris.platform.commerceservices.i18n.CommerceCommonI18NService;
import java.util.Locale;
import java.util.Map;
import java.util.Optional;
import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import org.apache.commons.lang.StringUtils;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import com.bcf.bcfstorefrontaddon.constants.BcfstorefrontaddonControllerConstants;
import com.bcf.bcfstorefrontaddon.model.components.BCFPromotionContentComponentModel;
import com.bcf.core.strategies.responsive.BCFResponsiveMediaStrategy;


@Controller("BCFPromotionContentComponentController")
@RequestMapping(value = BcfstorefrontaddonControllerConstants.Actions.Cms.BCFPromotionContentComponent)
public class BCFPromotionContentComponentController
		extends SubstitutingCMSAddOnComponentController<BCFPromotionContentComponentModel>
{
	@Resource(name = "bcfResponsiveMediaStrategy")
	private BCFResponsiveMediaStrategy bcfResponsiveMediaStrategy;

	@Resource(name = "commerceCommonI18NService")
	private CommerceCommonI18NService commerceCommonI18NService;

	@Resource(name = "promotionalContentViewMap")
	private Map<String, String> promotionalContentViewMap;

	@Override
	protected void fillModel(final HttpServletRequest request, final Model model,
			final BCFPromotionContentComponentModel component)
	{
		final Locale currentLocale = commerceCommonI18NService.getCurrentLocale();
		Optional.ofNullable(component.getImage(currentLocale)).ifPresent(image -> {
			model.addAttribute("dataMedia", bcfResponsiveMediaStrategy.getResponsiveJson(image));
			model.addAttribute("location", image.getLocationTag(currentLocale));
			model.addAttribute("caption", image.getCaption(currentLocale));
			model.addAttribute("promoText", image.getPromoText(currentLocale));
			model.addAttribute("description", component.getDescription(currentLocale));
			model.addAttribute("link", component.getLink());
			model.addAttribute("title", component.getTitle(currentLocale));
			model.addAttribute("featuredText", image.getFeaturedText(currentLocale));
			model.addAttribute("altText", image.getAltText());
		});
		if (StringUtils.isNotEmpty(component.getBackgroundColour()))
		{
			model.addAttribute("backgroundColour", component.getBackgroundColour());
		}
	}

	@Override
	protected String getView(final BCFPromotionContentComponentModel component)
	{
		return promotionalContentViewMap.get(component.getContentViewType().getCode());
	}
}
