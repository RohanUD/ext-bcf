/*
 * Copyright (c) 2019 British Columbia Ferry Services Inc
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of British Columbia Ferry Services Inc("Confidential Information").
 * You shall not disclose such Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with British Columbia Ferry Services Inc.
 *
 * Last Modified: 06/06/19 20:21
 */

package com.bcf.bcfstorefrontaddon.controllers.pages;

import de.hybris.platform.cms2.exceptions.CMSItemNotFoundException;
import de.hybris.platform.ticket.enums.CsTicketCategory;
import java.util.List;
import javax.annotation.Resource;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import com.bcf.bcfstorefrontaddon.constants.BcfstorefrontaddonControllerConstants;
import com.bcf.core.bcfproperties.BcfConfigurablePropertiesService;
import com.bcf.facades.bcffacades.GetAQuoteFacade;
import com.bcf.facades.change.booking.CsTicketData;


/**
 * Ferry Landing Page Controller
 */
@Controller
@RequestMapping(value = "/get-quotelist")
public class GetAQuoteListingPageController extends BcfAbstractPageController
{
	private static final String GET_A_QUOTE_PAGE_ID = "getAQuoteListingPage";
	public static final String REDIRECT_PREFIX = "redirect:";
	private static final String STARTING_NUMBER_OF_RESULTS = "startingNumberOfResults";
	private static final String TOTAL_NUMBER_OF_RESULTS = "totalNumberOfResults";
	private static final String TOTAL_SHOWN_RESULTS = "totalShownResults";
	private static final String PAGE_NUM = "pageNum";
	private static final String HAS_MORE_RESULTS = "hasMoreResults";
	private static final String GET_A_QUOTE_LISTING_PAGE_RECORDS = "getAQuoteListingPageRecords";
	private static final String START_INDEX = "startIndex";
	public static final String GET_A_QUOTE_REQUESTS_PAGE = "/get-quotelist";
	private static final String GET_A_QUOTE_TICKETS_LIST = "ticketsList";

	@Resource(name = "getAQuoteFacade")
	private GetAQuoteFacade getAQuoteFacade;

	@Resource(name = "bcfConfigurablePropertiesService")
	private BcfConfigurablePropertiesService bcfConfigurablePropertiesService;

	@RequestMapping(method = RequestMethod.GET)
	public String getTicketsData(
			@RequestParam(value = "pageNum", required = false, defaultValue = "1") final Integer pageNumber, final Model model)
			throws CMSItemNotFoundException
	{
		final String recordsPerPage = bcfConfigurablePropertiesService.getBcfPropertyValue(GET_A_QUOTE_LISTING_PAGE_RECORDS);

		populateCommonModelAttributes(0, pageNumber, Integer.parseInt(recordsPerPage),
				getAQuoteFacade
						.getOpenTickets(0, pageNumber, Integer.parseInt(recordsPerPage), CsTicketCategory.CREATEQUOTE.getCode())
						.getCsTickets(),
				getAQuoteFacade
						.getOpenTickets(0, pageNumber, Integer.parseInt(recordsPerPage), CsTicketCategory.CREATEQUOTE.getCode())
						.getCount(), model);
		storeCmsPageInModel(model, getContentPageForLabelOrId(GET_A_QUOTE_PAGE_ID));
		setUpMetaDataForContentPage(model, getContentPageForLabelOrId(GET_A_QUOTE_PAGE_ID));
		return getViewForPage(model);
	}

	/**
	 * Show more method to return lazy loaded properties.
	 *
	 * @param pageNumber the page number
	 * @param model      the model
	 * @return the string
	 */
	@RequestMapping("/show-more")
	public String showMore(@RequestParam(value = "pageNumber", required = false) final int pageNumber, final Model model)
	{
		final String recordsPerPage = bcfConfigurablePropertiesService.getBcfPropertyValue(GET_A_QUOTE_LISTING_PAGE_RECORDS);
		final int startIndex = pageNumber * Integer.parseInt(recordsPerPage) - Integer.parseInt(recordsPerPage);
		final int size = getAQuoteFacade
				.getOpenTickets(startIndex, pageNumber, Integer.parseInt(recordsPerPage), CsTicketCategory.CREATEQUOTE.getCode())
				.getCount();
		populateCommonModelAttributes(startIndex, pageNumber, Integer.parseInt(recordsPerPage),
				getAQuoteFacade
						.getOpenTickets(startIndex, pageNumber, Integer.parseInt(recordsPerPage),
								CsTicketCategory.CREATEQUOTE.getCode()).getCsTickets(),
				size, model);
		return BcfstorefrontaddonControllerConstants.Views.Pages.GetAQuoteListing.getAQuoteListing;
	}

	@RequestMapping("/change-status")
	public String changeTicketStatus(@RequestParam(value = "status", required = false) final String status,
			@RequestParam(value = "code", required = false) final String ticketId, final Model model)
	{
		getAQuoteFacade.changeTicketStatusForQuote(status, ticketId);
		return REDIRECT_PREFIX + GET_A_QUOTE_REQUESTS_PAGE;
	}

	/**
	 * Populate common model attributes.
	 *
	 * @param pageNumber     the page number
	 * @param startIndex     the start index
	 * @param recordsPerPage the records per page number
	 * @param size           the size
	 * @param ticketData     the ticket data
	 * @param model          the model
	 */
	protected void populateCommonModelAttributes(final int startIndex, final int pageNumber, final int recordsPerPage,
			final List<CsTicketData> ticketData, final int size, final Model model)
	{
		model.addAttribute(GET_A_QUOTE_TICKETS_LIST, ticketData);
		model.addAttribute(TOTAL_SHOWN_RESULTS, Math.min(pageNumber * recordsPerPage, size));
		model.addAttribute(STARTING_NUMBER_OF_RESULTS,
				(pageNumber * recordsPerPage) - recordsPerPage + 1);
		model.addAttribute(PAGE_NUM, pageNumber);
		model.addAttribute(TOTAL_NUMBER_OF_RESULTS, size);
		model.addAttribute(START_INDEX, startIndex);
		final Boolean hasMoreResults = pageNumber * recordsPerPage < size;
		model.addAttribute(HAS_MORE_RESULTS, hasMoreResults);
	}

}
