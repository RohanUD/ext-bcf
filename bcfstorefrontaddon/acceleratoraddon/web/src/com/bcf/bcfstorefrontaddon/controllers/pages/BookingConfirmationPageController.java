/*
 * Copyright (c) 2019 British Columbia Ferry Services Inc
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of British Columbia Ferry Services Inc("Confidential Information").
 * You shall not disclose such Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with British Columbia Ferry Services Inc.
 *
 * Last Modified: 01/07/19 15:16
 */

package com.bcf.bcfstorefrontaddon.controllers.pages;

import de.hybris.platform.acceleratorfacades.flow.impl.SessionOverrideCheckoutFlowFacade;
import de.hybris.platform.acceleratorstorefrontcommons.controllers.ThirdPartyConstants;
import de.hybris.platform.cms2.exceptions.CMSItemNotFoundException;
import de.hybris.platform.cms2.model.pages.AbstractPageModel;
import de.hybris.platform.core.model.order.OrderModel;
import de.hybris.platform.servicelayer.exceptions.ModelNotFoundException;
import de.hybris.platform.servicelayer.session.SessionService;
import de.hybris.platform.travelservices.enums.BookingJourneyType;
import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;
import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.util.WebUtils;
import com.bcf.bcfstorefrontaddon.constants.BcfstorefrontaddonControllerConstants;
import com.bcf.bcfstorefrontaddon.constants.BcfstorefrontaddonWebConstants;
import com.bcf.bcfstorefrontaddon.util.BcfControllerUtil;
import com.bcf.core.bcfproperties.BcfConfigurablePropertiesService;
import com.bcf.core.constants.BcfCoreConstants;
import com.bcf.core.email.service.BcfEmailGenerationService;
import com.bcf.core.util.BCFPropertiesUtils;
import com.bcf.facades.bcffacades.BcfBookingFacade;
import com.bcf.facades.constants.BcfFacadesConstants;
import com.bcf.facades.helper.BcfTravelCartFacadeHelper;
import com.bcf.facades.order.impl.DefaultBcfTravelCartFacade;


/**
 * BookingConfirmationPageController
 */
@Controller
@Scope("tenant")
@RequestMapping(value = "/checkout")
public class BookingConfirmationPageController extends BcfAbstractPageController
{
	private static final String BOOKING_REFERENCE = "bookingReference";
	protected static final Logger LOG = Logger.getLogger(BookingConfirmationPageController.class);
	private static final String DATE_TIME_FORMAT_LABEL = "dateTimeFormat";
	/**
	 * We use this suffix pattern because of an issue with Spring 3.1 where a Uri value is incorrectly extracted if it
	 * contains on or more '.' characters. Please see https://jira.springsource.org/browse/SPR-6164 for a discussion on
	 * the issue and future resolution.
	 */
	private static final String BOOKING_REFERENCE_PATH_VARIABLE_PATTERN = "{bookingReference:.*}";

	private static final String CHECKOUT_BOOKING_CONFIRMATION_CMS_PAGE_LABEL = "bookingConfirmation";

	@Resource(name = "bookingFacade")
	private BcfBookingFacade bcfBookingFacade;

	@Resource(name = "sessionService")
	private SessionService sessionService;

	@Resource(name = "bcfEmailGenerationService")
	private BcfEmailGenerationService bcfEmailGenerationService;

	@Resource(name = "bcfConfigurablePropertiesService")
	private BcfConfigurablePropertiesService bcfConfigurablePropertiesService;

	@Resource(name = "defaultBcfTravelCartFacade")
	private DefaultBcfTravelCartFacade defaultBcfTravelCartFacade;

	@Resource(name = "bcfTravelCartFacadeHelper")
	private BcfTravelCartFacadeHelper bcfTravelCartFacadeHelper;

	@Resource(name = "bcfControllerUtil")
	private BcfControllerUtil bcfControllerUtil;

	/**
	 * @param exception
	 * @param request
	 * @return String
	 */
	@ExceptionHandler(ModelNotFoundException.class)
	public String handleModelNotFoundException(final ModelNotFoundException exception, final HttpServletRequest request)
	{
		request.setAttribute("message", exception.getMessage());
		return FORWARD_PREFIX + "/404";
	}

	/**
	 * @param bookingReference
	 * @param model
	 * @return String
	 * @throws CMSItemNotFoundException
	 */
	@RequestMapping(value = "/bookingConfirmation/" + BOOKING_REFERENCE_PATH_VARIABLE_PATTERN, method = RequestMethod.GET)
	public String orderConfirmation(@PathVariable(BOOKING_REFERENCE) final String bookingReference, final Model model,
			final HttpServletRequest request)
			throws CMSItemNotFoundException
	{
		WebUtils.setSessionAttribute(request, BcfFacadesConstants.BCF_FARE_FINDER_FORM, null);
		getSessionService().removeAttribute(BcfFacadesConstants.BCF_FARE_FINDER_FORM);
		getSessionService().removeAttribute(BcfFacadesConstants.ORIGINAL_BOOKING_REFERENCE);
		getSessionService().removeAttribute(BcfFacadesConstants.AMENDING_BOOKING_REFERENCES);
		getSessionService().removeAttribute(BcfFacadesConstants.JOURNEY_REF_NUMBER);
		getSessionService().removeAttribute(BcfFacadesConstants.DEPARTURE_LOCATION);
		getSessionService().removeAttribute(BcfFacadesConstants.ARRIVAL_LOCATION);
		if (!bcfBookingFacade.validateUserForCheckout(bookingReference))
		{
			return REDIRECT_PREFIX + "/";
		}
		SessionOverrideCheckoutFlowFacade.resetSessionOverrides();
		sessionService.setAttribute(BcfFacadesConstants.BOOKING_CONFIRMATION_REFERENCE, bookingReference);

		model.addAttribute(DATE_TIME_FORMAT_LABEL, bcfConfigurablePropertiesService.getBcfPropertyValue(DATE_TIME_FORMAT_LABEL));
		model.addAttribute(BcfstorefrontaddonWebConstants.AMEND, bcfBookingFacade.isAmendment(bookingReference));
		model.addAttribute(BcfstorefrontaddonWebConstants.BOOKING_TRANSPORT_ONLY,
				bcfBookingFacade.checkBookingJourneyType(bookingReference, Arrays.asList(BookingJourneyType.BOOKING_TRANSPORT_ONLY)));
		model.addAttribute(ThirdPartyConstants.SeoRobots.META_ROBOTS, ThirdPartyConstants.SeoRobots.NOINDEX_NOFOLLOW);
		model.addAttribute(BcfFacadesConstants.PACKAGE_AVAILABILITY_STATUS,
				bcfBookingFacade.getPackageAvailabilityStatus(bookingReference));
		model.addAttribute("vacationPolicyTermsAndConditions",
				bcfTravelCartFacadeHelper.getVacationPolicyTermsAndConditions(bookingReference));
		model.addAttribute("bcfNonRefundableCostTermsAndConditions",
				bcfTravelCartFacadeHelper.getBCFNonRefundableCostTermsAndConditions(bookingReference));
		model.addAttribute("isNonTransportBooking", bcfControllerUtil.isNonTransportBooking(bookingReference));
		model.addAttribute("confirmBooking", true);
		model.addAttribute("isTentativeBooking",
				bcfBookingFacade.getOrderFromBookingReferenceCode(bookingReference).isFerryOptionBooking());
		model.addAttribute("isOnRequestBooking",
				BcfCoreConstants.PACKAGE_ON_REQUEST.equals(bcfBookingFacade.getPackageAvailabilityStatus(bookingReference)));
		final AbstractPageModel cmsPage = getContentPageForLabelOrId(CHECKOUT_BOOKING_CONFIRMATION_CMS_PAGE_LABEL);
		storeCmsPageInModel(model, cmsPage);
		setUpMetaDataForContentPage(model, getContentPageForLabelOrId(CHECKOUT_BOOKING_CONFIRMATION_CMS_PAGE_LABEL));
		return BcfstorefrontaddonControllerConstants.Views.Pages.Order.OrderConfirmationPage;
	}

	@ModelAttribute("disableCurrencySelector")
	public Boolean getDisableCurrencySelector()
	{
		return Boolean.TRUE;
	}

	@RequestMapping(value = "/send-email-itinerary", method = RequestMethod.GET)
	public String sendEmailItinerary(@RequestParam(value = "email_list") final String emailList,
			@RequestParam(value = "ebookingId", required = false) final String ebookingId, final Model model)
	{
		final Set<String> emails = new HashSet<>(BCFPropertiesUtils.convertToList(emailList));
		final String bookingReference = sessionService.getAttribute(BcfFacadesConstants.BOOKING_CONFIRMATION_REFERENCE);
		final OrderModel order;
		if (StringUtils.isNotEmpty(bookingReference))
		{
			order = bcfBookingFacade.getOrderFromBookingReferenceCode(bookingReference);
		}
		else
		{
			final String orderId = bcfBookingFacade.getOrderCodeForEBookingCode(ebookingId);
			order = bcfBookingFacade.getOrderFromBookingReferenceCode(orderId);
		}
		bcfBookingFacade.shareItinerary(order, emails);
		model.addAttribute("sendingEmailSuccess", true);
		return BcfstorefrontaddonControllerConstants.Views.Pages.BookingConfirmation.sendItineraryResponse;
	}
}
