/*
 * Copyright (c) 2019 British Columbia Ferry Services Inc
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of British Columbia Ferry Services Inc("Confidential Information").
 * You shall not disclose such Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with British Columbia Ferry Services Inc.
 *
 * Last Modified: 02/07/19 14:01
 */

package com.bcf.bcfstorefrontaddon.controllers.pages;

import reactor.util.StringUtils;

import de.hybris.platform.cms2.exceptions.CMSItemNotFoundException;
import de.hybris.platform.cms2.model.pages.ContentPageModel;
import de.hybris.platform.commercefacades.travel.TransportFacilityData;
import java.util.Arrays;
import java.util.Objects;
import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import com.bcf.facades.travel.data.TransportFacilityPaginationData;
import com.bcf.facades.travelfacility.BCFTransportFacilityFacade;
import com.bcf.facades.travelroute.BCFTravelRouteFacade;


@Controller
@RequestMapping(value = "/travel-boarding/terminal-directions-parking-food")
public class TerminalPageController extends BcfAbstractPageController
{
	private static final String TERMINAL_NAME_PATTERN = "{terminalName}";
	private static final String TERMINAL_CODE_PATTERN = "{terminalCode}";

	private static final String TERMINAL_DETAILS = "terminalDetailsPage";
	private static final String TERMINAL_LISTING = "terminalListingPage";
	private static final String TERMINAL_FILTER = "terminalFilter";
	private static final String TERMINALS = "terminals";
	private static final String ALL = "ALL";
	private static final String TRAVEL_ROUTE_DATA_LIST = "travelRouteDataList";

	@Resource(name = "transportFacilityFacade")
	private BCFTransportFacilityFacade transportFacilityFacade;

	@Resource(name = "travelRouteFacade")
	private BCFTravelRouteFacade bcfTravelRouteFacade;

	@RequestMapping(method = RequestMethod.GET)
	public String getTerminals(final Model model, final HttpServletRequest request,
			@RequestParam(value = TERMINAL_FILTER, required = false) final String terminalFilter)
			throws CMSItemNotFoundException
	{
		model.addAttribute(TERMINALS, transportFacilityFacade.getAllTransportFacilities());
		model.addAttribute(TRAVEL_ROUTE_DATA_LIST, bcfTravelRouteFacade.getTravelRoutesForActiveTerminals());

		if (StringUtils.isEmpty(terminalFilter) || ALL.equals(terminalFilter))
		{
			final TransportFacilityPaginationData paginatedTransportFacilities = transportFacilityFacade
					.getPaginatedTransportFacilities(getPageSize(TERMINAL_LISTING), getCurrentPage(request));
			storePaginationResults(model, paginatedTransportFacilities.getResults(),
					paginatedTransportFacilities.getPaginationData());
		}
		else
		{
			storePaginationResults(model, Arrays.asList(transportFacilityFacade.getTravelFacilityDetailsForCode(terminalFilter)),
					null);
		}
		storeCmsPageInModel(model, getContentPageForLabelOrId(TERMINAL_LISTING));
		setUpMetaDataForContentPage(model, getContentPageForLabelOrId(TERMINAL_LISTING));
		return getViewForPage(model);
	}

	@RequestMapping(value = TERMINAL_NAME_PATTERN + "/" + TERMINAL_CODE_PATTERN, method = RequestMethod.GET)
	public String getTerminalDetailsPage(@PathVariable final String terminalName, @PathVariable final String terminalCode,
			final Model model, final HttpServletRequest request, final HttpServletResponse response) throws CMSItemNotFoundException
	{
		final TransportFacilityData transportFacilityData = transportFacilityFacade
				.getTerminalTransportFacility(terminalCode);
		if (Objects.nonNull(transportFacilityData))
		{
			model.addAttribute("terminal", transportFacilityData);
			model.addAttribute("travelRoutes", transportFacilityFacade.getTravelRoutesFromAndToTerminal(terminalCode));
			model.addAttribute("terminalItems", transportFacilityFacade.getAllTransportFacilities());
			final ContentPageModel contentPageModel = getContentPageForLabelOrId(TERMINAL_DETAILS);
			storeCmsPageInModel(model, contentPageModel);
			setUpMetaDataForContentPage(model, contentPageModel);
			setUpMetaDataForContentPage(model, contentPageModel);

			final String[] arguments = { transportFacilityData.getName() };
			setUpMetaDataForContentPage(model, contentPageModel, arguments);

			setUpDynamicOGGraphData(model, contentPageModel, request, arguments);

			return getViewForPage(model);
		}
		else
		{
			prepareNotFoundPage(model, response);
			return getViewForPage(model);
		}
	}
}
