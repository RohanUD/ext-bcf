/*
 * Copyright (c) 2019 British Columbia Ferry Services Inc
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of British Columbia Ferry Services Inc("Confidential Information").
 * You shall not disclose such Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with British Columbia Ferry Services Inc.
 *
 * Last Modified: 02/07/19 14:01
 */

package com.bcf.bcfstorefrontaddon.controllers.pages;

import de.hybris.platform.cms2.exceptions.CMSItemNotFoundException;
import de.hybris.platform.commercefacades.travel.TravelRouteData;
import de.hybris.platform.enumeration.EnumerationService;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.annotation.Resource;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import com.bcf.bcfstorefrontaddon.forms.RegionFinderForm;
import com.bcf.core.enums.RouteRegion;
import com.bcf.facades.location.BcfTravelLocationFacade;


/**
 * Discover Our Routes Page Controller
 */
@Controller
@RequestMapping(value = "/routes-fares/discover-route-map")
public class DiscoverRoutesPageController extends BcfAbstractPageController
{
	private static final String DISCOVER_ROUTES_PAGE_ID = "discoverOurRoutesPage";

	@Resource(name = "bcfTravelLocationFacade")
	private BcfTravelLocationFacade bcfTravelLocationFacade;

	@Resource(name = "enumerationService")
	private EnumerationService enumerationService;

	@RequestMapping(method = RequestMethod.GET)
	public String getGeographicalLocationsData(final Model model) throws CMSItemNotFoundException
	{
		populateRouteRegionsOnPage(model);
		model.addAttribute("regionFinderForm", new RegionFinderForm());
		return getViewForPage(model);
	}

	@RequestMapping(method = RequestMethod.GET, params = "routeRegions")
	public String findregions(@RequestParam(value = "routeRegions") final String routeRegions,
			final RegionFinderForm regionFinderForm,
			final Model model)
			throws CMSItemNotFoundException
	{
		final List<TravelRouteData> travelRoutelist = bcfTravelLocationFacade
				.getTravelRoutesByRegions(regionFinderForm.getRouteRegions());
		model.addAttribute("travelRoutelist", travelRoutelist);
		model.addAttribute("originToDestinationMapping", bcfTravelLocationFacade.createMapInfoWindowRouteMapping(travelRoutelist));
		populateRouteRegionsOnPage(model);
		return getViewForPage(model);
	}

	private void populateRouteRegionsOnPage(final Model model) throws CMSItemNotFoundException
	{
		final Map<String, String> routeRegions = new HashMap<>();
		final List<RouteRegion> routeRegionList = enumerationService.getEnumerationValues(RouteRegion._TYPECODE);
		routeRegionList.stream()
				.forEach(region -> routeRegions.put(region.getCode(), enumerationService.getEnumerationName(region)));
		model.addAttribute("routeRegions", routeRegions);
		storeCmsPageInModel(model, getContentPageForLabelOrId(DISCOVER_ROUTES_PAGE_ID));
		setUpMetaDataForContentPage(model, getContentPageForLabelOrId(DISCOVER_ROUTES_PAGE_ID));
	}
}
