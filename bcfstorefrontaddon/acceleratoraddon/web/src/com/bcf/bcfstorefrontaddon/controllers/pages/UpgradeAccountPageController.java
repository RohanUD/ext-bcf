/*
 * Copyright (c) 2019 British Columbia Ferry Services Inc
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of British Columbia Ferry Services Inc("Confidential Information").
 * You shall not disclose such Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with British Columbia Ferry Services Inc.
 *
 * Last Modified: 21/06/19 00:21
 */

package com.bcf.bcfstorefrontaddon.controllers.pages;

import de.hybris.platform.acceleratorstorefrontcommons.annotations.RequireHardLogIn;
import de.hybris.platform.acceleratorstorefrontcommons.controllers.ThirdPartyConstants;
import de.hybris.platform.acceleratorstorefrontcommons.controllers.util.GlobalMessages;
import de.hybris.platform.cms2.exceptions.CMSItemNotFoundException;
import de.hybris.platform.commercefacades.i18n.I18NFacade;
import de.hybris.platform.commercefacades.user.data.CountryData;
import de.hybris.platform.commercefacades.user.data.RegionData;
import de.hybris.platform.commercefacades.user.data.RegisterData;
import de.hybris.platform.travelfacades.facades.TravelI18NFacade;
import java.util.Collections;
import java.util.List;
import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.commons.lang.StringUtils;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;
import com.bcf.bcfstorefrontaddon.constants.BcfstorefrontaddonControllerConstants;
import com.bcf.bcfstorefrontaddon.form.validators.UpgradeAccountValidator;
import com.bcf.bcfstorefrontaddon.forms.UpgradeAccountForm;
import com.bcf.core.enums.AccountType;
import com.bcf.facades.customer.BCFCustomerFacade;


@Controller
@RequestMapping("/upgrade-account")
public class UpgradeAccountPageController extends BcfAbstractSearchPageController
{
	private static final String ACCOUNT_CMS_PAGE = "/my-account";
	private static final String UPGRADE_ACCOUNT_FORM = "upgradeAccountForm";

	private static final String UPGRADE_ACCOUNT_CMS_PAGE = "MyAccountUpgradeSubscription";
	private static final String REDIRECT_UPGRADE_ACCOUNT_PAGE = REDIRECT_PREFIX + "/upgrade-account";


	public static final String FORM_BINDING_RESULT = "org.springframework.validation.BindingResult.upgradeAccountForm";

	@Resource(name = "bcfCustomerFacade")
	private BCFCustomerFacade bcfCustomerFacade;

	@Resource(name = "travelI18NFacade")
	private TravelI18NFacade travelI18NFacade;

	@Resource(name = "i18NFacade")
	private I18NFacade i18NFacade;

	@Resource(name = "upgradeAccountValidator")
	private UpgradeAccountValidator upgradeAccountValidator;

	@RequestMapping(method = RequestMethod.GET)
	@RequireHardLogIn
	public String account(final Model model, final RedirectAttributes redirectModel) throws CMSItemNotFoundException
	{
		if (AccountType.FULL_ACCOUNT.getCode().equals(bcfCustomerFacade.getCurrentCustomer().getAccountType()))
		{
			return REDIRECT_PREFIX + ACCOUNT_CMS_PAGE;
		}

		final UpgradeAccountForm upgradeAccountForm;
		if (model.asMap().containsKey(UPGRADE_ACCOUNT_FORM))
		{
			upgradeAccountForm = (UpgradeAccountForm) model.asMap().get(UPGRADE_ACCOUNT_FORM);
		}
		else
		{
			upgradeAccountForm = new UpgradeAccountForm();
		}

		model.addAttribute(UPGRADE_ACCOUNT_FORM, upgradeAccountForm);
		if (StringUtils.isNotEmpty(upgradeAccountForm.getCountry()))
		{
			model.addAttribute("regions", this.getRegions(upgradeAccountForm.getCountry()));
		}
		
		storeCmsPageInModel(model, getContentPageForLabelOrId(UPGRADE_ACCOUNT_CMS_PAGE));
		setUpMetaDataForContentPage(model, getContentPageForLabelOrId(UPGRADE_ACCOUNT_CMS_PAGE));
		model.addAttribute(ThirdPartyConstants.SeoRobots.META_ROBOTS, ThirdPartyConstants.SeoRobots.NOINDEX_NOFOLLOW);
		return getViewForPage(model);
	}

	@RequireHardLogIn
	@RequestMapping(method = RequestMethod.POST)
	public String upgradeAccount(final UpgradeAccountForm upgradeAccountForm, final BindingResult bindingResult, final Model model,
			final HttpServletRequest request, final HttpServletResponse response, final RedirectAttributes redirectModel)
	{
		upgradeAccountValidator.validate(upgradeAccountForm, bindingResult);

		if (bindingResult.hasErrors())
		{
			redirectModel.addFlashAttribute(UPGRADE_ACCOUNT_FORM, upgradeAccountForm);
			GlobalMessages.addFlashMessage(redirectModel, GlobalMessages.ERROR_MESSAGES_HOLDER, "form.global.error");
			redirectModel.addFlashAttribute(FORM_BINDING_RESULT, bindingResult);
			redirectModel.addAttribute("expand", "true");
			return REDIRECT_UPGRADE_ACCOUNT_PAGE;
		}

		final boolean isSuccess = bcfCustomerFacade.upgradeAccount(this.populateRegisterData(upgradeAccountForm));

		if (!isSuccess)
		{
			redirectModel.addFlashAttribute(UPGRADE_ACCOUNT_FORM, upgradeAccountForm);
			GlobalMessages.addFlashMessage(redirectModel, GlobalMessages.ERROR_MESSAGES_HOLDER, "upgradeAccount.failed");
			return REDIRECT_UPGRADE_ACCOUNT_PAGE;
		}
		GlobalMessages.addFlashMessage(redirectModel, GlobalMessages.CONF_MESSAGES_HOLDER, "upgrade.account.success");
		return REDIRECT_PREFIX + ACCOUNT_CMS_PAGE;
	}

	private RegisterData populateRegisterData(final UpgradeAccountForm upgradeAccountForm)
	{
		final RegisterData registerData = new RegisterData();
		registerData.setPhoneNo(upgradeAccountForm.getMobileNumber());
		registerData.setCity(upgradeAccountForm.getCity());
		registerData.setZipCode(upgradeAccountForm.getZipCode());
		registerData.setCountry(upgradeAccountForm.getCountry());
		registerData.setProvince(upgradeAccountForm.getProvince());
		return registerData;
	}

	@RequestMapping(value = "/getRegions")
	public @ResponseBody
	List<RegionData> getRegions(@RequestParam final String countryIsoCode)
	{
		if (StringUtils.isEmpty(countryIsoCode))
		{
			return Collections.emptyList();
		}
		return i18NFacade.getRegionsForCountryIso(countryIsoCode);
	}

	@ModelAttribute("countries")
	protected List<CountryData> getCountries()
	{
		return travelI18NFacade.getAllCountries();
	}

	@ModelAttribute(BcfstorefrontaddonControllerConstants.ModelAttributes.AccountPages.TITLE)
	public String getTitle()
	{
		return "text.pageTitle.account.myAccount";
	}
}
