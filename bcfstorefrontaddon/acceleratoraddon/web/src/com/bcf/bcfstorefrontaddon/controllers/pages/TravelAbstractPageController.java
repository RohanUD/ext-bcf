/*
 * Copyright (c) 2019 British Columbia Ferry Services Inc
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of British Columbia Ferry Services Inc("Confidential Information").
 * You shall not disclose such Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with British Columbia Ferry Services Inc.
 *
 * Last Modified: 02/07/19 14:01
 */

package com.bcf.bcfstorefrontaddon.controllers.pages;


import static com.bcf.bcfstorefrontaddon.constants.BcfstorefrontaddonControllerConstants.ModelAttributes.ActivityPages.ACTIVITY_TO_CHANGE_DATE_KEY;
import static com.bcf.bcfstorefrontaddon.constants.BcfstorefrontaddonControllerConstants.ModelAttributes.ActivityPages.ACTIVITY_TO_CHANGE_NAME_KEY;
import static com.bcf.bcfstorefrontaddon.constants.BcfstorefrontaddonControllerConstants.ModelAttributes.ActivityPages.ACTIVITY_TO_CHANGE_REF;
import static com.bcf.bcfstorefrontaddon.constants.BcfstorefrontaddonControllerConstants.ModelAttributes.ActivityPages.ACTIVITY_TO_CHANGE_TIME_KEY;
import static com.bcf.bcfstorefrontaddon.constants.BcfstorefrontaddonControllerConstants.ModelAttributes.ActivityPages.IS_CHANGE_ACTIVITY;

import de.hybris.platform.travelfacades.facades.customer.TravelCustomerFacade;
import de.hybris.platform.travelfacades.order.TravelCartFacade;
import javax.annotation.Resource;
import org.apache.commons.lang.StringUtils;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;


/**
 * Base controller for all travel page controllers. Provides common functionality for all page controllers.
 */
public class TravelAbstractPageController extends BcfAbstractPageController
{
	protected static final int MAX_PAGE_LIMIT = 100;

	@Resource(name = "cartFacade")
	private TravelCartFacade travelCartFacade;

	@Resource(name = "travelCustomerFacade")
	private TravelCustomerFacade travelCustomerFacade;


	@ModelAttribute("reservationCode")
	public String getReservationCode()
	{
		if (travelCartFacade.isAmendmentCart())
		{
			return travelCartFacade.getOriginalOrderCode();
		}
		return travelCartFacade.getCurrentCartCode();
	}

	protected void populateChangeActivityRelatedFieldsInModel(final boolean changeActivity, final String changeActivityCode,
			final String date, final String time, String changeActivityRef,final Model model)
	{
		model.addAttribute(IS_CHANGE_ACTIVITY, changeActivity);
		if (changeActivity && (StringUtils.isNotBlank(changeActivityCode) || StringUtils.isNotBlank(changeActivityRef)))
		{
			model.addAttribute(ACTIVITY_TO_CHANGE_NAME_KEY, changeActivityCode);
			model.addAttribute(ACTIVITY_TO_CHANGE_DATE_KEY, date);
			model.addAttribute(ACTIVITY_TO_CHANGE_TIME_KEY, time);
			model.addAttribute(ACTIVITY_TO_CHANGE_REF, changeActivityRef);
		}
	}

	/**
	 * @return the travelCustomerFacade
	 */
	protected TravelCustomerFacade getTravelCustomerFacade()
	{
		return travelCustomerFacade;
	}

	/**
	 * @return the travelCustomerFacade
	 */
	protected TravelCartFacade getTravelCartFacade()
	{
		return travelCartFacade;
	}
}
