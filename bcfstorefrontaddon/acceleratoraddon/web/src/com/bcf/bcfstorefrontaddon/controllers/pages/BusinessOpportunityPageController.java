/*
 * Copyright (c) 2019 British Columbia Ferry Services Inc
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of British Columbia Ferry Services Inc("Confidential Information").
 * You shall not disclose such Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with British Columbia Ferry Services Inc.
 *
 * Last Modified: 01/07/19 15:16
 */

package com.bcf.bcfstorefrontaddon.controllers.pages;

import de.hybris.platform.acceleratorstorefrontcommons.controllers.util.GlobalMessages;
import de.hybris.platform.cms2.exceptions.CMSItemNotFoundException;
import de.hybris.platform.cms2.model.pages.ContentPageModel;
import de.hybris.platform.commercefacades.travel.CompetitionData;
import de.hybris.platform.commercefacades.travel.SubstantialPerformanceNoticeData;
import java.util.Optional;
import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;
import com.bcf.core.model.CompetitionModel;
import com.bcf.core.model.SubstantialPerformanceNoticeModel;
import com.bcf.facades.servicenotice.ServiceNoticeFacade;


@Controller
@RequestMapping(value = "/business-ops")
public class BusinessOpportunityPageController extends BcfAbstractPageController
{
	private static final String BUSINESS_OPPORTUNITY_HOME_PAGE = "businessOpportunityHomePage";
	private static final String COMPETITION_DETAILS_PAGE = "competitionDetailsPage";
	private static final String SUBSTANTIAL_PERFORMANCE_NOTICE_DETAILS_PAGE = "substantialPerformanceNoticeDetailsPage";

	private static final String COMPETITION_CODE_PATTERN = "/competition/{competitionCode}";
	private static final String SUBSTANTIAL_PERFORMANCE_NOTICE_CODE_PATTERN = "/substantialPerformanceNotice/{substantialPerformanceNoticeCode}";

	private static final String COMPETITIONS_LIST = "competitionsList";
	private static final String SUBSTANTIAL_PERFORMANCE_NOTICES_LIST = "substantialPerformanceNoticesList";

	private static final String RESPONSE_STATUS = "org.springframework.web.servlet.View.responseStatus";
	private static final String COMPETITION_NOT_FOUND = "text.competition.not.found";
	private static final String SUBSTANTIAL_PERFORMANCE_NOTICE_NOT_FOUND = "text.substantial.performance.notice.not.found";

	@Resource(name = "serviceNoticeFacade")
	private ServiceNoticeFacade serviceNoticeFacade;

	@RequestMapping(method = RequestMethod.GET)
	public String listBusinessOpportunities(final Model model) throws CMSItemNotFoundException
	{
		model.addAttribute(COMPETITIONS_LIST, serviceNoticeFacade.getCompetitionsData());
		model.addAttribute(SUBSTANTIAL_PERFORMANCE_NOTICES_LIST, serviceNoticeFacade.getSubstantialPerformanceNoticesData());
		return renderPage(model, BUSINESS_OPPORTUNITY_HOME_PAGE);
	}

	@RequestMapping(value = COMPETITION_CODE_PATTERN, method = RequestMethod.GET)
	public String displayCompetitionDetails(final HttpServletRequest request, @PathVariable final String competitionCode,
			final Model model, final RedirectAttributes redirectModel) throws CMSItemNotFoundException
	{
		final Optional<CompetitionData> competitionData = serviceNoticeFacade.getCompetitionDataForCode(competitionCode);
		if (competitionData.isPresent())
		{
			model.addAttribute(CompetitionModel._TYPECODE, competitionData.get());
			return renderPage(model, COMPETITION_DETAILS_PAGE);
		}
		return redirectToBusinessOpportunitiesHomePage(request, redirectModel, COMPETITION_NOT_FOUND);
	}

	@RequestMapping(value = SUBSTANTIAL_PERFORMANCE_NOTICE_CODE_PATTERN, method = RequestMethod.GET)
	public String displaySubstantialPerformanceNoticeDetails(final HttpServletRequest request,
			@PathVariable final String substantialPerformanceNoticeCode, final Model model, final RedirectAttributes redirectModel)
			throws CMSItemNotFoundException
	{
		final Optional<SubstantialPerformanceNoticeData> substantialPerformanceNoticeData = serviceNoticeFacade
				.getSubstantialPerformanceNoticeDataForCode(substantialPerformanceNoticeCode);
		if (substantialPerformanceNoticeData.isPresent())
		{
			model.addAttribute(SubstantialPerformanceNoticeModel._TYPECODE, substantialPerformanceNoticeData.get());
			return renderPage(model, SUBSTANTIAL_PERFORMANCE_NOTICE_DETAILS_PAGE);
		}
		return redirectToBusinessOpportunitiesHomePage(request, redirectModel, SUBSTANTIAL_PERFORMANCE_NOTICE_NOT_FOUND);
	}

	private String renderPage(final Model model, final String page) throws CMSItemNotFoundException
	{
		final ContentPageModel contentPageModel = getContentPageForLabelOrId(page);
		storeCmsPageInModel(model, contentPageModel);
		setUpMetaDataForContentPage(model, contentPageModel);
		return getViewForPage(model);
	}

	private String redirectToBusinessOpportunitiesHomePage(final HttpServletRequest request,
			final RedirectAttributes redirectModel, final String message) throws CMSItemNotFoundException
	{
		final ContentPageModel contentPageModel = getContentPageForLabelOrId(BUSINESS_OPPORTUNITY_HOME_PAGE);
		request.setAttribute(RESPONSE_STATUS, HttpStatus.MOVED_PERMANENTLY);
		GlobalMessages.addFlashMessage(redirectModel, GlobalMessages.ERROR_MESSAGES_HOLDER, message);
		return REDIRECT_PREFIX + contentPageModel.getLabel();
	}
}
