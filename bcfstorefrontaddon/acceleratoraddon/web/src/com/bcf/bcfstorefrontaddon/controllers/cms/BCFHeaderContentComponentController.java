/*
 * Copyright (c) 2019 British Columbia Ferry Services Inc
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of British Columbia Ferry Services Inc("Confidential Information").
 * You shall not disclose such Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with British Columbia Ferry Services Inc.
 *
 * Last Modified: 06/06/19 20:21
 */

package com.bcf.bcfstorefrontaddon.controllers.cms;

import de.hybris.platform.commerceservices.i18n.CommerceCommonI18NService;
import de.hybris.platform.core.model.media.MediaModel;
import java.util.Locale;
import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import com.bcf.bcfstorefrontaddon.constants.BcfstorefrontaddonControllerConstants;
import com.bcf.bcfstorefrontaddon.model.components.BCFHeaderContentComponentModel;
import com.bcf.core.strategies.responsive.BCFResponsiveMediaStrategy;


@Controller("BCFHeaderContentComponentController")
@RequestMapping(value = BcfstorefrontaddonControllerConstants.Actions.Cms.BCFHeaderContentComponent)
public class BCFHeaderContentComponentController extends SubstitutingCMSAddOnComponentController<BCFHeaderContentComponentModel>
{
	@Resource(name = "bcfResponsiveMediaStrategy")
	private BCFResponsiveMediaStrategy bcfResponsiveMediaStrategy;

	@Resource(name = "commerceCommonI18NService")
	private CommerceCommonI18NService commerceCommonI18NService;

	@Override
	protected void fillModel(final HttpServletRequest request, final Model model,
			final BCFHeaderContentComponentModel component)
	{
		final Locale currentLocale = commerceCommonI18NService.getCurrentLocale();
		final MediaModel image = component.getBanner(currentLocale);
		model.addAttribute("dataMedia", bcfResponsiveMediaStrategy.getResponsiveJson(image));
		model.addAttribute("location", image.getLocationTag(currentLocale));
		model.addAttribute("caption", image.getCaption(currentLocale));
		model.addAttribute("title", component.getTitle(currentLocale));
	}
}
