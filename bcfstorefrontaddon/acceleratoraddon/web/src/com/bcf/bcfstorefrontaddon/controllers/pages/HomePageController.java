/*
 * Copyright (c) 2019 British Columbia Ferry Services Inc
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of British Columbia Ferry Services Inc("Confidential Information").
 * You shall not disclose such Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with British Columbia Ferry Services Inc.
 *
 * Last Modified: 01/07/19 15:16
 */

package com.bcf.bcfstorefrontaddon.controllers.pages;

import de.hybris.platform.acceleratorstorefrontcommons.controllers.util.GlobalMessages;
import de.hybris.platform.cms2.exceptions.CMSItemNotFoundException;
import de.hybris.platform.cms2.model.pages.ContentPageModel;
import de.hybris.platform.servicelayer.session.SessionService;
import javax.annotation.Resource;
import org.apache.commons.lang3.StringUtils;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;
import com.bcf.bcfstorefrontaddon.constants.BcfstorefrontaddonWebConstants;
import com.bcf.core.bcfproperties.BcfConfigurablePropertiesService;
import com.bcf.core.constants.BcfCoreConstants;
import com.bcf.facades.constants.BcfFacadesConstants;


/**
 * Controller for home page
 */
@Controller
@Scope("tenant")
@RequestMapping("/")
public class HomePageController extends TravelAbstractPageController
{
	@Resource(name = "sessionService")
	private SessionService sessionService;

	@Resource(name = "bcfConfigurablePropertiesService")
	private BcfConfigurablePropertiesService bcfConfigurablePropertiesService;

	@RequestMapping(method = RequestMethod.GET)
	public String home(@RequestParam(value = "logout", defaultValue = "false") final boolean logout,
			@RequestParam(value = "cart-upgrade", required = false) final String cartUpgrade, final Model model,
			final RedirectAttributes redirectModel) throws CMSItemNotFoundException
	{
		if (logout)
		{
			GlobalMessages.addFlashMessage(redirectModel, GlobalMessages.INFO_MESSAGES_HOLDER,
					"account.confirmation.signout" + ".title");
			return REDIRECT_PREFIX + ROOT;
		}

		final String ferries = bcfConfigurablePropertiesService.getBcfPropertyValue("ferries");
		final String hotels = bcfConfigurablePropertiesService.getBcfPropertyValue("hotels");
		final String deals = bcfConfigurablePropertiesService.getBcfPropertyValue("deals");
		model.addAttribute("ferries", ferries);
		model.addAttribute("hotels", hotels);
		model.addAttribute("deals", deals);
		model.addAttribute("isHomePage", true);

		if (StringUtils.isNotBlank(cartUpgrade) && StringUtils.equalsIgnoreCase("false", cartUpgrade))
		{
			GlobalMessages.addErrorMessage(model, "error.ferry.quotation.to.option.conversion.failure");
		}

		final ContentPageModel homePage = getContentPageForLabelOrId(null);

		storeCmsPageInModel(model, homePage);
		setUpMetaDataForContentPage(model, homePage);
		setUpMetaDataRobotsForContentPage(model, homePage.getMetaIndexable(), homePage.getMetaFollow());
		sessionService.removeAttribute(BcfstorefrontaddonWebConstants.ACCOMMODATION_FINDER_FORM);
		sessionService.removeAttribute(BcfstorefrontaddonWebConstants.SESSION_FARE_FINDER_FORM);
		sessionService.removeAttribute(BcfCoreConstants.SESSION_BOOKING_JOURNEY);
		sessionService.removeAttribute(BcfstorefrontaddonWebConstants.SESSION_FARE_SELECTION_DATA);
		sessionService.removeAttribute(BcfstorefrontaddonWebConstants.ACCOMMODATION_SEARCH_RESPONSE_PROPERTIES);
		sessionService.removeAttribute(BcfstorefrontaddonWebConstants.ACCOMMODATION_SEARCH_RESPONSE);
		sessionService.removeAttribute(BcfstorefrontaddonWebConstants.PACKAGE_SEARCH_RESPONSE_PROPERTIES);
		sessionService.removeAttribute(BcfFacadesConstants.SELECTED_JOURNEY_REF_NO);
		sessionService.removeAttribute(BcfFacadesConstants.ORIGIN_DESTINATION_REF_NUM);
		sessionService.removeAttribute(BcfFacadesConstants.BOOKING_CONFIRMATION_REFERENCE);
		sessionService.removeAttribute(BcfFacadesConstants.DEPARTURE_LOCATION);
		sessionService.removeAttribute(BcfFacadesConstants.ARRIVAL_LOCATION);
		sessionService.removeAttribute(BcfstorefrontaddonWebConstants.BOOKING_REFERENCE);
		sessionService.removeAttribute(BcfCoreConstants.CONVERT_QUOTATION_TO_OPTION_FAILED);
		model.addAttribute(BcfCoreConstants.DISPLAY_ORDER_ID,homePage.isDisplayOrderId());

		return getViewForPage(model);
	}

}
