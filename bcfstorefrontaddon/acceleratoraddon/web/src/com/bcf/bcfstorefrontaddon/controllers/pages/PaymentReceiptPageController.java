/*
 * Copyright (c) 2019 British Columbia Ferry Services Inc
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of British Columbia Ferry Services Inc("Confidential Information").
 * You shall not disclose such Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with British Columbia Ferry Services Inc.
 *
 * Last Modified: 02/07/19 14:01
 */

package com.bcf.bcfstorefrontaddon.controllers.pages;

import de.hybris.platform.acceleratorstorefrontcommons.controllers.ThirdPartyConstants;
import de.hybris.platform.cms2.exceptions.CMSItemNotFoundException;
import javax.annotation.Resource;
import javax.validation.Valid;
import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import com.bcf.core.constants.BcfCoreConstants;
import com.bcf.facades.receipt.PaymentReceiptFacade;
import com.bcf.facades.receipt.ReceiptData;
import com.bcf.integration.payment.data.TransactionDetails;


@Controller
@RequestMapping("/payment/receipt")
public class PaymentReceiptPageController extends TravelAbstractPageController
{
	private static final String PAYMENT_RECEIPT_CMS_PAGE = "paymentReceiptPage";
	private static final String RECEIPT_DATA = "receiptData";

	@Resource(name = "paymentReceiptFacade")
	private PaymentReceiptFacade paymentReceiptFacade;

	@RequestMapping(value = "/order/{" + BcfCoreConstants.BOOKING_REFERENCE + "}", method = RequestMethod.GET)
	public String getPaymentReceiptForOrder(@PathVariable(BcfCoreConstants.BOOKING_REFERENCE) final String bookingReference,
			final Model model) throws CMSItemNotFoundException
	{
		if (StringUtils.isNotBlank(bookingReference))
		{
			final ReceiptData receiptData = paymentReceiptFacade.getReceiptDataForBooking(bookingReference);
			model.addAttribute(RECEIPT_DATA, receiptData);
		}
		storeCmsPageInModel(model, getContentPageForLabelOrId(PAYMENT_RECEIPT_CMS_PAGE));
		setUpMetaDataForContentPage(model, getContentPageForLabelOrId(PAYMENT_RECEIPT_CMS_PAGE));
		model.addAttribute(ThirdPartyConstants.SeoRobots.META_ROBOTS, ThirdPartyConstants.SeoRobots.NOINDEX_NOFOLLOW);
		return getViewForPage(model);
	}

	@RequestMapping(value = "/cart", method = RequestMethod.POST)
	public String getPaymentReceiptForCart(
			@Valid @ModelAttribute("transactionDetails") final TransactionDetails transactionDetails,
			final Model model) throws CMSItemNotFoundException
	{
		final ReceiptData receiptData = paymentReceiptFacade.getReceiptDataForCart(transactionDetails);
		model.addAttribute(RECEIPT_DATA, receiptData);
		storeCmsPageInModel(model, getContentPageForLabelOrId(PAYMENT_RECEIPT_CMS_PAGE));
		setUpMetaDataForContentPage(model, getContentPageForLabelOrId(PAYMENT_RECEIPT_CMS_PAGE));
		model.addAttribute(ThirdPartyConstants.SeoRobots.META_ROBOTS, ThirdPartyConstants.SeoRobots.NOINDEX_NOFOLLOW);
		return getViewForPage(model);
	}
}
