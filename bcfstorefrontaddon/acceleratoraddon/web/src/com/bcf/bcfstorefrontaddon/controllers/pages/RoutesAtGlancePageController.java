/*
 * Copyright (c) 2019 British Columbia Ferry Services Inc
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of British Columbia Ferry Services Inc("Confidential Information").
 * You shall not disclose such Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with British Columbia Ferry Services Inc.
 *
 * Last Modified: 01/07/19 15:16
 */

package com.bcf.bcfstorefrontaddon.controllers.pages;

import de.hybris.platform.cms2.exceptions.CMSItemNotFoundException;
import de.hybris.platform.cms2.model.pages.ContentPageModel;
import javax.annotation.Resource;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import com.bcf.facades.traveladvisory.TravelAdvisoryFacade;


@Controller
@RequestMapping(value = "/routes-at-glance")
public class RoutesAtGlancePageController extends BcfAbstractPageController
{
	private static final String ROUTES_AT_GLANCE_PAGE = "routesAtGlancePage";
	private static final String ALL_ROUTES_AT_GLANCE = "allRoutesAtGlance";

	@Resource(name = "travelAdvisoryFacade")
	private TravelAdvisoryFacade travelAdvisoryFacade;

	@RequestMapping(method = RequestMethod.GET)
	public String displayRoutesAtGlance(final Model model) throws CMSItemNotFoundException
	{
		model.addAttribute(ALL_ROUTES_AT_GLANCE, travelAdvisoryFacade.getRoutesAtGlanceContent());
		final ContentPageModel contentPageModel = getContentPageForLabelOrId(ROUTES_AT_GLANCE_PAGE);
		storeCmsPageInModel(model, contentPageModel);
		setUpMetaDataForContentPage(model, contentPageModel);
		return getViewForPage(model);
	}
}
