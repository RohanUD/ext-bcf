/*
 * Copyright (c) 2019 British Columbia Ferry Services Inc
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of British Columbia Ferry Services Inc("Confidential Information").
 * You shall not disclose such Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with British Columbia Ferry Services Inc.
 *
 * Last Modified: 06/06/19 20:21
 */

package com.bcf.bcfstorefrontaddon.controllers.cms;

import de.hybris.platform.commerceservices.i18n.CommerceCommonI18NService;
import de.hybris.platform.core.model.media.MediaModel;
import java.util.Locale;
import java.util.Optional;
import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import org.apache.commons.lang.StringUtils;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import com.bcf.bcfstorefrontaddon.constants.BcfstorefrontaddonControllerConstants;
import com.bcf.bcfstorefrontaddon.model.components.BCFMultimediaTextComponentModel;
import com.bcf.core.strategies.responsive.BCFResponsiveMediaStrategy;


/**
 * Controller for CMS BCFMultimediaTextComponentController
 */
@Controller("BCFMultimediaTextComponentController")
@RequestMapping(value = BcfstorefrontaddonControllerConstants.Actions.Cms.BCFMultimediaTextComponent)
public class BCFMultimediaTextComponentController extends SubstitutingCMSAddOnComponentController<BCFMultimediaTextComponentModel>
{
	private static final String MEDIA_DATA_PREFIX = "data_";
	private static final String ALT_TEXT_PREFIX = "altText_";
	private static final String LOCATION_PREFIX = "location_";
	private static final String CAPTION_PREFIX = "caption_";

	private static final String CONTENT = "content";

	@Resource(name = "bcfResponsiveMediaStrategy")
	private BCFResponsiveMediaStrategy bcfResponsiveMediaStrategy;

	@Resource(name = "commerceCommonI18NService")
	private CommerceCommonI18NService commerceCommonI18NService;

	@Override
	protected void fillModel(final HttpServletRequest request, final Model model, final BCFMultimediaTextComponentModel component)
	{
		final Locale currentLocale = commerceCommonI18NService.getCurrentLocale();
		setMediaAttributes(model, BCFMultimediaTextComponentModel.MEDIA1, Optional.ofNullable(component.getMedia1(currentLocale)));
		setMediaAttributes(model, BCFMultimediaTextComponentModel.MEDIA2, Optional.ofNullable(component.getMedia2(currentLocale)));
		setMediaAttributes(model, BCFMultimediaTextComponentModel.MEDIA3, Optional.ofNullable(component.getMedia3(currentLocale)));
		setMediaAttributes(model, BCFMultimediaTextComponentModel.MEDIA4, Optional.ofNullable(component.getMedia4(currentLocale)));
		setMediaAttributes(model, BCFMultimediaTextComponentModel.MEDIA5, Optional.ofNullable(component.getMedia5(currentLocale)));
		model.addAttribute(CONTENT, component.getContent(currentLocale));
	}

	private void setMediaAttributes(final Model model, final String mediaFieldName, final Optional<MediaModel> mediaModelOptional)
	{
		model.addAttribute(MEDIA_DATA_PREFIX + mediaFieldName, getDataMedia(mediaModelOptional));
		model.addAttribute(ALT_TEXT_PREFIX + mediaFieldName, getAltText(mediaModelOptional));
		model.addAttribute(LOCATION_PREFIX + mediaFieldName, getLocationTagText(mediaModelOptional));
		model.addAttribute(CAPTION_PREFIX + mediaFieldName, getCaptionText(mediaModelOptional));
	}

	private String getDataMedia(final Optional<MediaModel> mediaModelOptional)
	{
		return mediaModelOptional.map(bcfResponsiveMediaStrategy::getResponsiveJson).orElse(StringUtils.EMPTY);
	}

	private String getAltText(final Optional<MediaModel> mediaModelOptional)
	{
		return mediaModelOptional.map(MediaModel::getAltText).orElse(StringUtils.EMPTY);
	}

	private String getLocationTagText(final Optional<MediaModel> mediaModelOptional)
	{
		return mediaModelOptional.map(MediaModel::getLocationTag).orElse(StringUtils.EMPTY);
	}

	private String getCaptionText(final Optional<MediaModel> mediaModelOptional)
	{
		return mediaModelOptional.map(MediaModel::getCaption).orElse(StringUtils.EMPTY);
	}
}
