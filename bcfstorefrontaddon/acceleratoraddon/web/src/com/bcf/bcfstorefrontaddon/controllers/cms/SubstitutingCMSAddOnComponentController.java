/*
 * Copyright (c) 2019 British Columbia Ferry Services Inc
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of British Columbia Ferry Services Inc("Confidential Information").
 * You shall not disclose such Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with British Columbia Ferry Services Inc.
 *
 * Last Modified: 06/06/19 20:21
 */

package com.bcf.bcfstorefrontaddon.controllers.cms;

import de.hybris.platform.addonsupport.controllers.cms.AbstractCMSAddOnComponentController;
import de.hybris.platform.cms2.model.contents.components.AbstractCMSComponentModel;
import de.hybris.platform.commerceservices.search.facetdata.FacetSearchPageData;
import de.hybris.platform.commerceservices.search.pagedata.PageableData;
import de.hybris.platform.core.servicelayer.data.PaginationData;
import de.hybris.platform.servicelayer.config.ConfigurationService;
import java.io.Serializable;
import java.util.List;
import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import org.apache.commons.lang.StringUtils;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import com.bcf.bcfstorefrontaddon.constants.BcfstorefrontaddonWebConstants;
import com.bcf.bcfstorefrontaddon.util.GlobalPaginationUtil;
import com.bcf.core.substitute.ExtensionSubstitutionService;


/**
 * Controller which substitutes the path to view file from original to custom one.
 *
 * @param <T>
 */
public abstract class SubstitutingCMSAddOnComponentController<T extends AbstractCMSComponentModel>
		extends AbstractCMSAddOnComponentController<T>
{

	@Resource(name = "bcfExtensionSubstitutionService")
	private ExtensionSubstitutionService extensionSubstitutionService;

	@Resource(name = "configurationService")
	private ConfigurationService configurationService;

	@Resource(name = "globalPaginationUtil")
	private GlobalPaginationUtil globalPaginationUtil;

	private static final String MAX_ALLOWED_CHECKIN_CHECKOUT_DATE_DIFFERENCE = "maxAllowedCheckInCheckOutDateDifference";

	/*
	 * (non-Javadoc)
	 * @see de.hybris.platform.addonsupport.controllers.cms.AbstractCMSAddOnComponentController#getAddonUiExtensionName(de .hybris.platform.cms2.model.contents.components.AbstractCMSComponentModel)
	 */
	@Override
	protected String getAddonUiExtensionName(final T component)
	{
		final String addonUiExtensionName = super.getAddonUiExtensionName(component);
		return extensionSubstitutionService.getSubstitutedExtension(addonUiExtensionName);
	}

	@ModelAttribute(MAX_ALLOWED_CHECKIN_CHECKOUT_DATE_DIFFERENCE)
	public String setMaxAllowedCheckInCheckOutDateDifference()
	{
		return StringUtils.EMPTY + configurationService.getConfiguration()
				.getInt(BcfstorefrontaddonWebConstants.MAX_ALLOWED_CHECKIN_CHECKOUT_DATE_DIFFERENCE);
	}

	protected PageableData buildGlobalPaginationData(final HttpServletRequest request, final String cmsComponent)
	{
		return globalPaginationUtil.buildPaginationData(request, cmsComponent);
	}

	protected void storePaginationResults(final Model model, final FacetSearchPageData facetSearchPageData)
	{
		globalPaginationUtil.storePaginationResults(model, facetSearchPageData);
	}

	protected void storePaginationResults(final Model model, final List<? extends Serializable> results,
			final PaginationData paginationMetaData)
	{
		globalPaginationUtil.storePaginationResults(model, results, paginationMetaData);
	}

	protected int getPageSize(final String cmsComponent)
	{
		return globalPaginationUtil.getPageSize(cmsComponent);
	}

	protected int getCurrentPage(final HttpServletRequest httpServletRequest)
	{
		return globalPaginationUtil.getCurrentPage(httpServletRequest);
	}

	/**
	 * @return the configurationService
	 */
	protected ConfigurationService getConfigurationService()
	{
		return configurationService;
	}

}
