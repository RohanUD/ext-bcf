/*
 * Copyright (c) 2019 British Columbia Ferry Services Inc
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of British Columbia Ferry Services Inc("Confidential Information").
 * You shall not disclose such Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with British Columbia Ferry Services Inc.
 *
 * Last Modified: 26/06/19 20:25
 */

package com.bcf.bcfstorefrontaddon.controllers.pages;

import static com.bcf.bcfstorefrontaddon.constants.BcfstorefrontaddonWebConstants.FALSE;
import static com.bcf.core.constants.BcfCoreConstants.ORDER_LOCK_SUCCESSFUL;
import static com.bcf.core.constants.BcfCoreConstants.ORDER_UNLOCK_SUCCESSFUL;
import static com.bcf.core.constants.BcfCoreConstants.PENDING_ORDERS_APPROVE_SUCCESSFUL;
import static com.bcf.core.constants.BcfCoreConstants.PENDING_ORDERS_ENTRY_UPDATE_FAILED;
import static com.bcf.core.constants.BcfCoreConstants.PENDING_ORDERS_REJECT_SUCCESSFUL;
import static com.bcf.core.constants.BcfCoreConstants.PENDING_ORDERS_UPDATE_FAILED;
import static com.bcf.core.constants.BcfCoreConstants.PENDING_ORDER_ENTRY_UPDATE_SUCCESSFUL;

import de.hybris.platform.acceleratorstorefrontcommons.controllers.pages.AbstractSearchPageController;
import de.hybris.platform.acceleratorstorefrontcommons.controllers.util.GlobalMessages;
import de.hybris.platform.cms2.exceptions.CMSItemNotFoundException;
import de.hybris.platform.commerceservices.search.pagedata.PageableData;
import de.hybris.platform.commerceservices.search.pagedata.PaginationData;
import de.hybris.platform.commerceservices.search.pagedata.SearchPageData;
import javax.annotation.Resource;
import org.apache.commons.lang3.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import com.bcf.bcfstorefrontaddon.constants.BcfstorefrontaddonConstants;
import com.bcf.bcfstorefrontaddon.constants.BcfstorefrontaddonWebConstants;
import com.bcf.bcfstorefrontaddon.forms.AgentPendingOrderApprovalStatusForm;
import com.bcf.bcfstorefrontaddon.forms.AgentPendingOrderDecisionForm;
import com.bcf.core.exception.BcfOrderUpdateException;
import com.bcf.core.order.data.OnHoldOrderData;
import com.bcf.facades.order.BcfOrderFacade;
import com.bcf.facades.order.UpdatePendingOrderEntryRequestData;


@Controller
@RequestMapping("/pending-orders-onrequest")
public class AsmOrderDetailsPageController extends AbstractSearchPageController
{
	protected static final Logger LOG = Logger.getLogger(AsmOrderDetailsPageController.class);
	private static final String PENDING_ORDER_DETAILS_CMS_PAGE = "pendingOrderDetailsPage";
	private static final String ORDER_DETAILS_CMS_PAGE = "asmOrderDetailsPage";
	private static final String ORDER = "order";
	private static final String ORDER_CODE = "orderCode";

	@Resource(name = "orderFacade")
	private BcfOrderFacade bcfOrderFacade;

	@RequestMapping(method = RequestMethod.GET)
	public String getPage(@RequestParam(value = "page", defaultValue = "0") final int page,
			final Model model) throws CMSItemNotFoundException
	{
		// Handle paged search results
		final int pageSize = getConfigurationService().getConfiguration()
				.getInt(BcfstorefrontaddonConstants.AGENT_PENDING_ORDERS_PAGE_SIZE);
		final PageableData pageableData = createPageableData(page, pageSize, "", ShowMode.Page);
		final SearchPageData searchPageData = bcfOrderFacade.getPagedPendingOrdersForInventoryApproval(pageableData);
		populateModel(model, searchPageData, ShowMode.Page);

		storeCmsPageInModel(model, getContentPageForLabelOrId(PENDING_ORDER_DETAILS_CMS_PAGE));

		setUpMetaDataForContentPage(model, getContentPageForLabelOrId(PENDING_ORDER_DETAILS_CMS_PAGE));
		model.addAttribute("searchPageData", searchPageData);
		return getViewForPage(model);
	}

	@RequestMapping(value = "/orderdetail/{orderCode}", method = RequestMethod.GET)
	public String getOrderDetail(@PathVariable final String orderCode, final Model model) throws CMSItemNotFoundException
	{
		OnHoldOrderData onHoldOrderData = bcfOrderFacade.getOrderForCode(orderCode);

		model.addAttribute(ORDER, onHoldOrderData);
		model.addAttribute(ORDER_CODE, orderCode);
		model.addAttribute(BcfstorefrontaddonWebConstants.AGENT_PENDING_ORDER_DECISION_FORM, new AgentPendingOrderDecisionForm());
		model.addAttribute(BcfstorefrontaddonWebConstants.AGENT_PENDING_ORDER_APPROVAL_STATUS_FORM,
				new AgentPendingOrderApprovalStatusForm());
		storeCmsPageInModel(model, getContentPageForLabelOrId(ORDER_DETAILS_CMS_PAGE));
		setUpMetaDataForContentPage(model, getContentPageForLabelOrId(ORDER_DETAILS_CMS_PAGE));

		return getViewForPage(model);
	}

	@RequestMapping(value = "/updateentry", method = RequestMethod.POST)
	public String updateOrderEntry(
			@ModelAttribute(BcfstorefrontaddonWebConstants.AGENT_PENDING_ORDER_DECISION_FORM) final AgentPendingOrderDecisionForm agentPendingOrderDecisionForm,
			@ModelAttribute(BcfstorefrontaddonWebConstants.AGENT_PENDING_ORDER_APPROVAL_STATUS_FORM) final AgentPendingOrderApprovalStatusForm agentPendingOrderApprovalStatusForm,
			final Model model) throws CMSItemNotFoundException
	{
		final UpdatePendingOrderEntryRequestData updatePendingOrderEntryRequest = createUpdateOrderEntryRequest(
				agentPendingOrderDecisionForm);

		model.addAttribute(ORDER_CODE, agentPendingOrderDecisionForm.getOrderCode());
		model.addAttribute("agentPendingOrderApprovalStatusForm", agentPendingOrderApprovalStatusForm);
		model.addAttribute("agentPendingOrderDecisionForm", agentPendingOrderDecisionForm);

		if (bcfOrderFacade.updateOrderEntry(updatePendingOrderEntryRequest))
		{
			addGlobalMessage(model, PENDING_ORDER_ENTRY_UPDATE_SUCCESSFUL);
		}
		else
		{
			addGlobalMessage(model, PENDING_ORDERS_ENTRY_UPDATE_FAILED);
		}

		// OnHoldOrderData needs to be created only after the selected entry has been updated with the selected action by an agent
		final OnHoldOrderData onHoldOrderData = bcfOrderFacade.getOrderForCode(agentPendingOrderDecisionForm.getOrderCode());
		model.addAttribute(ORDER, onHoldOrderData);

		storeCmsPageInModel(model, getContentPageForLabelOrId(ORDER_DETAILS_CMS_PAGE));
		setUpMetaDataForContentPage(model, getContentPageForLabelOrId(ORDER_DETAILS_CMS_PAGE));
		return getViewForPage(model);
	}

	protected UpdatePendingOrderEntryRequestData createUpdateOrderEntryRequest(
			final AgentPendingOrderDecisionForm agentPendingOrderDecisionForm)
	{
		final UpdatePendingOrderEntryRequestData updatePendingOrderEntryRequest = new UpdatePendingOrderEntryRequestData();
		updatePendingOrderEntryRequest.setOrderCode(agentPendingOrderDecisionForm.getOrderCode());
		updatePendingOrderEntryRequest.setEntryNumber(agentPendingOrderDecisionForm.getEntryNumber());
		updatePendingOrderEntryRequest.setAgentComments(agentPendingOrderDecisionForm.getComments());
		updatePendingOrderEntryRequest.setSelectedAction(agentPendingOrderDecisionForm.getButtonClicked());
		return updatePendingOrderEntryRequest;
	}

	@Override
	protected void populateModel(final Model model, final SearchPageData<?> searchPageData, final ShowMode showMode)
	{
		super.populateModel(model, searchPageData, showMode);
		final PaginationData paginationData = searchPageData.getPagination();
		final int next = (paginationData.getCurrentPage() + 1 < paginationData.getNumberOfPages()) ?
				paginationData.getCurrentPage() + 1 :
				paginationData.getCurrentPage();
		final int previous = (paginationData.getCurrentPage() - 1 > 0) ? paginationData.getCurrentPage() - 1 : 0;
		model.addAttribute("naxtPage", next);
		model.addAttribute("previousPage", previous);
	}

	@RequestMapping(value = "/updateorder", method = RequestMethod.POST)
	public String updateOrderEntry(
			@ModelAttribute(BcfstorefrontaddonWebConstants.AGENT_PENDING_ORDER_APPROVAL_STATUS_FORM) final AgentPendingOrderApprovalStatusForm agentPendingOrderApprovalStatusForm,
			@ModelAttribute(BcfstorefrontaddonWebConstants.AGENT_PENDING_ORDER_DECISION_FORM) final AgentPendingOrderDecisionForm agentPendingOrderDecisionForm,
			final Model model) throws CMSItemNotFoundException
	{
		final String orderCode = agentPendingOrderApprovalStatusForm.getOrderNumber();
		LOG.debug(agentPendingOrderApprovalStatusForm.getOrderAction() + " ORDER: " + orderCode);
		try
		{
			switch (agentPendingOrderApprovalStatusForm.getOrderAction())
			{
				case "LOCKED":
					String lockOrderResult = bcfOrderFacade.lockOrder(orderCode);
					if (Boolean.valueOf(lockOrderResult))
					{
						addGlobalMessage(model, ORDER_LOCK_SUCCESSFUL);
					}
					else if (StringUtils.isNotBlank(lockOrderResult) && !StringUtils.equals(lockOrderResult, FALSE))
					{
						addGlobalMessage(model, lockOrderResult);
					}
					break;
				case "UNLOCKED":
					String unlockOrderResult = bcfOrderFacade.unlockOrder(orderCode);
					if (Boolean.valueOf(unlockOrderResult))
					{
						addGlobalMessage(model, ORDER_UNLOCK_SUCCESSFUL);
					}
					else if (StringUtils.isNotBlank(unlockOrderResult) && !StringUtils.equals(unlockOrderResult, FALSE))
					{
						addGlobalMessage(model, unlockOrderResult);
					}
					break;
				case "APPROVED":
					String approvalResult = bcfOrderFacade
							.acceptOrder(orderCode, agentPendingOrderApprovalStatusForm.getOrderComments());
					if (Boolean.parseBoolean(approvalResult))
					{
						addGlobalMessage(model, PENDING_ORDERS_APPROVE_SUCCESSFUL);
					}
					break;
				case "REJECTED":
					String rejectionResult = bcfOrderFacade
							.rejectOrder(orderCode, agentPendingOrderApprovalStatusForm.getOrderComments());
					if (Boolean.valueOf(rejectionResult))
					{
						addGlobalMessage(model, PENDING_ORDERS_REJECT_SUCCESSFUL);
					}
					break;
				default: // Do nothing
			}
		}
		catch (final BcfOrderUpdateException e)
		{
			//Order update specific exception, not need to log
			addGlobalMessage(model, e.getMessage());
		}
		catch (final Exception ex)
		{
			LOG.error(ex);
			addGlobalMessage(model, PENDING_ORDERS_UPDATE_FAILED);
		}
		OnHoldOrderData onHoldOrderData = bcfOrderFacade.getOrderForCode(orderCode);

		model.addAttribute(ORDER, onHoldOrderData);
		model.addAttribute(ORDER_CODE, orderCode);
		model.addAttribute("agentPendingOrderDecisionForm", agentPendingOrderDecisionForm);
		model.addAttribute("agentPendingOrderApprovalStatusForm", agentPendingOrderApprovalStatusForm);
		storeCmsPageInModel(model, getContentPageForLabelOrId(ORDER_DETAILS_CMS_PAGE));
		setUpMetaDataForContentPage(model, getContentPageForLabelOrId(ORDER_DETAILS_CMS_PAGE));

		return getViewForPage(model);
	}

	private void addGlobalMessage(final Model model, final String property)
	{
		GlobalMessages.addInfoMessage(model, property);
	}
}

