/*
 * Copyright (c) 2019 British Columbia Ferry Services Inc
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of British Columbia Ferry Services Inc("Confidential Information").
 * You shall not disclose such Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with British Columbia Ferry Services Inc.
 *
 * Last Modified: 02/07/19 14:01
 */

package com.bcf.bcfstorefrontaddon.controllers.cms;

import de.hybris.platform.commercefacades.accommodation.PropertyData;
import de.hybris.platform.commercefacades.search.data.SearchStateData;
import de.hybris.platform.commerceservices.search.facetdata.FacetData;
import de.hybris.platform.enumeration.EnumerationService;
import de.hybris.platform.servicelayer.config.ConfigurationService;
import de.hybris.platform.travelfacades.facades.accommodation.search.handlers.PropertyHandler;
import de.hybris.platform.travelservices.enums.LocationType;
import de.hybris.platform.travelservices.search.facetdata.AccommodationOfferingSearchPageData;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Set;
import java.util.stream.Collectors;
import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import com.bcf.bcfstorefrontaddon.constants.BcfstorefrontaddonControllerConstants;
import com.bcf.bcfstorefrontaddon.enums.PropertyCategoryTypeEnum;
import com.bcf.bcfstorefrontaddon.enums.VacationPackageViewType;
import com.bcf.bcfstorefrontaddon.model.components.BCFVacationPackageComponentModel;
import com.bcf.core.util.StreamUtil;
import com.bcf.facades.accommodationoffering.search.AccommodationOfferingSolrSearchFacade;
import com.bcf.facades.bcffacades.BcfAccommodationOfferingFacade;
import com.bcf.facades.location.BcfTravelLocationFacade;
import com.bcf.facades.packagedeal.search.PackageSolrSearchFacade;
import com.bcf.facades.packagedeal.search.response.data.PackageResponseData;
import com.bcf.facades.search.facetdata.PackageSearchPageData;
import com.bcf.facades.travel.data.BcfLocationData;
import com.google.common.collect.Sets;


@Controller("BCFVacationPackageComponentController")
@RequestMapping(value = BcfstorefrontaddonControllerConstants.Actions.Cms.BCFVacationPackageComponent)
public class BCFVacationPackageComponentController
		extends SubstitutingCMSAddOnComponentController<BCFVacationPackageComponentModel>
{
	private static final String STATIC_DEALS = "staticDeals";
	private static final String FEATURED_PACKAGES = "featuredPackages";
	private static final String PROPERTY_CATEGORIES = "propertyCategories";
	private static final String DISPLAY_HOTELS_FACET = "displayHotelsFacet";
	private static final String DISPLAY_PROPERTY_CATEGORY_FACET = "displayPropertyCategoryFacet";
	private static final String DELIMITER = ",";
	private static final String HEADING_TEXT = "headingText";
	private static final String ALL = "all";
	private static final String DESTINATION_FILTER = "destinationFilter";
	private static final String PROPERTY_CATEGORY_FILTER = "propertyCategoryFilter";
	private static final String REGION_CODE = "regionCode";
	private static final String CITY_CODE = "cityCode";

	@Resource(name = "vacationPackageViewTypeMap")
	private Map<String, String> vacationPackageViewTypeMap;

	@Resource(name = "accommodationOfferingFacade")
	private BcfAccommodationOfferingFacade accommodationOfferingFacade;

	@Resource(name = "bcfPropertyDataBasicHandler")
	private PropertyHandler propertyDataBasicHandler;

	@Resource(name = "packageSolrSearchFacade")
	private PackageSolrSearchFacade packageSolrSearchFacade;

	@Resource(name = "bcfAccommodationOfferingSolrSearchFacade")
	private AccommodationOfferingSolrSearchFacade accommodationOfferingSolrSearchFacade;

	@Resource(name = "bcfTravelLocationFacade")
	private BcfTravelLocationFacade bcfTravelLocationFacade;

	@Resource(name = "configurationService")
	private ConfigurationService configurationService;

	@Resource(name = "enumerationService")
	private EnumerationService enumerationService;

	@Override
	protected void fillModel(final HttpServletRequest request, final Model model,
			final BCFVacationPackageComponentModel component)
	{
		model.addAttribute(HEADING_TEXT, component.getHeadingText());
		if (VacationPackageViewType.STATICDEALS.equals(component.getContentViewType()))
		{
			model.addAttribute(STATIC_DEALS, component.getStaticDeals());
		}
		if (VacationPackageViewType.HOTELS.equals(component.getContentViewType()))
		{
			setHotels(model, component, request);
		}
		if (VacationPackageViewType.FEATUREDPACKAGES.equals(component.getContentViewType()))
		{
			setFeaturedPackages(model, component);
		}
	}

	private void setFeaturedPackages(final Model model, final BCFVacationPackageComponentModel component)
	{
		SearchStateData searchStateData = null;
		PackageSearchPageData<SearchStateData, PackageResponseData> packageSearchPageData = new PackageSearchPageData<>();
		if (StringUtils.isNotEmpty(component.getLocationCode()))
		{
			packageSearchPageData = packageSolrSearchFacade.searchForLocation(component.getLocationCode());
		}
		else if (CollectionUtils.isNotEmpty(component.getPropertyCategories()))
		{
			final Set<String> propertyCategories = component.getPropertyCategories().stream()
					.map(PropertyCategoryTypeEnum::getCode)
					.collect(Collectors.toSet());
			searchStateData = packageSolrSearchFacade.buildSearchStateData(PROPERTY_CATEGORIES, propertyCategories);
			packageSearchPageData = packageSolrSearchFacade.searchDeals(searchStateData);
		}
		final List<PropertyData> featuredPackages = StreamUtil.safeStream(packageSearchPageData.getResults())
				.map(this::getPropertyDataList)
				.flatMap(Collection::stream)
				.filter(propertyData -> !excludePropertiesWithNonMatchingCategories(propertyData, component.getPropertyCategories()))
				.collect(Collectors.toList());
		model.addAttribute(FEATURED_PACKAGES, featuredPackages);
	}

	private boolean excludePropertiesWithNonMatchingCategories(final PropertyData propertyData,
			final List<PropertyCategoryTypeEnum> filterPropertyCategories)
	{
		return StreamUtil.safeStream(filterPropertyCategories)
				.anyMatch(filterPropertyCategoryTypeEnum -> !(propertyData.getPropertyCategoryTypes()
						.contains(filterPropertyCategoryTypeEnum.getCode())));
	}

	private List<PropertyData> getPropertyDataList(final PackageResponseData packageResponseData)
	{
		final String roomRateProductCodes = StreamUtil.safeStream(packageResponseData.getRoomRateProductCodes())
				.collect(Collectors.joining(DELIMITER));
		final String[] hotelCodes = StreamUtil.safeStream(packageResponseData.getHotelLocationNames())
				.collect(Collectors.toList())
				.toArray(new String[0]);
		return accommodationOfferingFacade.getPackageData(hotelCodes, packageResponseData.getCode(), roomRateProductCodes);
	}

	private void setHotels(final Model model, final BCFVacationPackageComponentModel component, final HttpServletRequest request)
	{
		final Map<String, Set<String>> filterTerms = createFilterTerms(component, request.getParameter(DESTINATION_FILTER),
				request.getParameterValues(PROPERTY_CATEGORY_FILTER));
		final AccommodationOfferingSearchPageData accommodationOfferingSearchPageData = accommodationOfferingSolrSearchFacade
				.searchHotels(filterTerms, buildGlobalPaginationData(request, BCFVacationPackageComponentModel._TYPECODE));
		storePaginationResults(model, accommodationOfferingSearchPageData);
		model.addAttribute(DISPLAY_HOTELS_FACET, displayFacet(accommodationOfferingSearchPageData.getFacets(), CITY_CODE));
		model.addAttribute(DISPLAY_PROPERTY_CATEGORY_FACET,
				displayFacet(accommodationOfferingSearchPageData.getFacets(), PROPERTY_CATEGORIES));
		model.addAttribute(PROPERTY_CATEGORIES,
				component.getPropertyCategories().stream().map(propCategory -> enumerationService.getEnumerationName(propCategory))
						.collect(Collectors.toList()));
	}

	private boolean displayFacet(final List<FacetData> facets, final String facetCode)
	{
		return StreamUtil.safeStream(facets)
				.filter(facetData -> facetCode.equals(facetData.getCode()))
				.map(facetData -> facetData.getValues())
				.flatMap(Collection::stream)
				.collect(Collectors.toList())
				.size() > 1;
	}

	private Map<String, Set<String>> createFilterTerms(final BCFVacationPackageComponentModel component,
			final String destinationFilter, final String[] propertyCategoryFilters)
	{
		final Map<String, Set<String>> filterTerms = new HashMap<>();
		if (Objects.nonNull(destinationFilter) && !ALL.equals(destinationFilter))
		{
			final String locationSolrSearchField = getLocationField(destinationFilter);
			if (StringUtils.isNotEmpty(locationSolrSearchField))
			{
				filterTerms.put(locationSolrSearchField, Sets.newHashSet(destinationFilter));
			}
		}
		else if (Objects.nonNull(component.getLocationCode()))
		{
			final String locationSolrSearchField = getLocationField(component.getLocationCode());
			if (StringUtils.isNotEmpty(locationSolrSearchField))
			{
				filterTerms.put(locationSolrSearchField, Sets.newHashSet(component.getLocationCode()));
			}
		}
		if (Objects.nonNull(propertyCategoryFilters) && !ALL.equals(propertyCategoryFilters))
		{
			filterTerms.put(PROPERTY_CATEGORIES, Sets.newHashSet(propertyCategoryFilters));
		}
		else if (CollectionUtils.isNotEmpty(component.getPropertyCategories()))
		{
			for (final PropertyCategoryTypeEnum propertyCategoryTypeEnum : component.getPropertyCategories())
			{
				filterTerms
						.put(PROPERTY_CATEGORIES, Sets.newHashSet(enumerationService.getEnumerationName(propertyCategoryTypeEnum)));
			}
		}
		return filterTerms;
	}

	private String getLocationField(final String locationCode)
	{
		if (StringUtils.isEmpty(locationCode))
		{
			return StringUtils.EMPTY;
		}
		final BcfLocationData location = bcfTravelLocationFacade.getLocation(locationCode);
		if (Objects.isNull(location))
		{
			return StringUtils.EMPTY;
		}
		if (LocationType.GEOGRAPHICAL_AREA.getCode().equals(location.getLocationType()))
		{
			return REGION_CODE;
		}
		return CITY_CODE;
	}

	@Override
	protected String getView(final BCFVacationPackageComponentModel component)
	{
		return vacationPackageViewTypeMap.get(component.getContentViewType().getCode());
	}
}
