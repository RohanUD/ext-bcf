/*
 * Copyright (c) 2019 British Columbia Ferry Services Inc
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of British Columbia Ferry Services Inc("Confidential Information").
 * You shall not disclose such Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with British Columbia Ferry Services Inc.
 *
 * Last Modified: 02/07/19 14:01
 */

package com.bcf.bcfstorefrontaddon.controllers.pages;

import de.hybris.platform.cms2.exceptions.CMSItemNotFoundException;
import de.hybris.platform.travelservices.enums.LocationType;
import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import com.bcf.facades.location.BcfTravelLocationFacade;
import com.bcf.facades.travel.data.BcfLocationPaginationData;


/**
 * Vacations Region Landing Page Controller
 */
@Controller
@RequestMapping(value = "/regions-landing")
public class VacationRegionsPageController extends BcfAbstractPageController
{
	private static final String REGIONS_LANDING_PAGE_ID = "vacationRegionsLandingPage";

	@Resource(name = "bcfTravelLocationFacade")
	private BcfTravelLocationFacade bcfTravelLocationFacade;

	@RequestMapping(method = RequestMethod.GET)
	public String getRegions(final Model model, final HttpServletRequest request) throws CMSItemNotFoundException
	{
		final BcfLocationPaginationData bcfLocationPaginationData = bcfTravelLocationFacade
				.getPaginatedLocationsByType(LocationType.GEOGRAPHICAL_AREA.getCode(), getPageSize(REGIONS_LANDING_PAGE_ID),
						getCurrentPage(request));
		storePaginationResults(model, bcfLocationPaginationData.getResults(),
				bcfLocationPaginationData.getPaginationData());
		storeCmsPageInModel(model, getContentPageForLabelOrId(REGIONS_LANDING_PAGE_ID));
		setUpMetaDataForContentPage(model, getContentPageForLabelOrId(REGIONS_LANDING_PAGE_ID));
		return getViewForPage(model);
	}
}
