/*
 * Copyright (c) 2019 British Columbia Ferry Services Inc
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of British Columbia Ferry Services Inc("Confidential Information").
 * You shall not disclose such Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with British Columbia Ferry Services Inc.
 *
 * Last Modified: 06/06/19 20:21
 */

package com.bcf.bcfstorefrontaddon.controllers.cms;

import de.hybris.platform.commercefacades.product.data.PriceData;
import de.hybris.platform.servicelayer.session.SessionService;
import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import com.bcf.bcfstorefrontaddon.constants.BcfstorefrontaddonControllerConstants;
import com.bcf.bcfstorefrontaddon.constants.BcfstorefrontaddonWebConstants;
import com.bcf.bcfstorefrontaddon.model.components.BookingTotalComponentModel;
import com.bcf.facades.bcffacades.BcfBookingFacade;
import com.bcf.facades.constants.BcfFacadesConstants;
import com.bcf.facades.order.BcfTravelCartFacade;


@Controller("BookingTotalComponentController")
@RequestMapping(value = BcfstorefrontaddonControllerConstants.Actions.Cms.BookingTotalComponent)
public class BookingTotalComponentController extends SubstitutingCMSAddOnComponentController<BookingTotalComponentModel>
{
	@Resource(name = "sessionService")
	private SessionService sessionService;

	@Resource(name = "bookingFacade")
	private BcfBookingFacade bookingFacade;

	@Resource(name = "bcfCartFacade")
	private BcfTravelCartFacade travelCartFacade;

	@Override
	protected void fillModel(final HttpServletRequest request, final Model model, final BookingTotalComponentModel component)
	{
		final String bookingReference = sessionService.getAttribute(BcfFacadesConstants.BOOKING_CONFIRMATION_REFERENCE);
		final PriceData totalPrice = bookingFacade.getBookingTotal(bookingReference);
		final PriceData amountPaid = bookingFacade.getOrderTotalPaid(bookingReference);
		model.addAttribute(BcfstorefrontaddonWebConstants.PAYMENT_PAID, amountPaid);
		model.addAttribute(BcfstorefrontaddonWebConstants.PAYMENT_DUE,
				travelCartFacade.getBookingDueAmount(totalPrice, amountPaid));
		model.addAttribute(BcfstorefrontaddonWebConstants.TOTAL, totalPrice);
		model.addAttribute(BcfstorefrontaddonWebConstants.AMOUNT_TO_PAY, bookingFacade.getAmountPaid(bookingReference));
	}
}
