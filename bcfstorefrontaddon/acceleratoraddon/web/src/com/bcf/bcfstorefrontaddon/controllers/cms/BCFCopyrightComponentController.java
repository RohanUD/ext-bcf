/*
 * Copyright (c) 2019 British Columbia Ferry Services Inc
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of British Columbia Ferry Services Inc("Confidential Information").
 * You shall not disclose such Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with British Columbia Ferry Services Inc.
 *
 * Last Modified: 06/06/19 20:21
 */

package com.bcf.bcfstorefrontaddon.controllers.cms;

import de.hybris.platform.commerceservices.i18n.CommerceCommonI18NService;
import java.time.Year;
import java.util.Objects;
import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import com.bcf.bcfstorefrontaddon.constants.BcfstorefrontaddonControllerConstants;
import com.bcf.bcfstorefrontaddon.model.components.BCFCopyrightComponentModel;


/**
 * Controller for CMS BCFCopyrightComponentController
 */
@Controller("BCFCopyrightComponentController")
@RequestMapping(value = BcfstorefrontaddonControllerConstants.Actions.Cms.BCFCopyrightComponent)
public class BCFCopyrightComponentController extends SubstitutingCMSAddOnComponentController<BCFCopyrightComponentModel>
{
	private static final String COPYRIGHT_CONTENT = "copyrightContent";
	private static final String YEAR_PLACEHOLDER = "{year}";

	@Resource(name = "commerceCommonI18NService")
	private CommerceCommonI18NService commerceCommonI18NService;

	@Override
	protected void fillModel(final HttpServletRequest request, final Model model, final BCFCopyrightComponentModel component)
	{
		final String copyrightContent = component.getCopyrightContent(commerceCommonI18NService.getCurrentLocale());
		if (Objects.nonNull(copyrightContent))
		{
			model.addAttribute(COPYRIGHT_CONTENT,
					copyrightContent.replace(YEAR_PLACEHOLDER, String.valueOf(Year.now().getValue())));
		}
	}
}
