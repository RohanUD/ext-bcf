/*
 * Copyright (c) 2019 British Columbia Ferry Services Inc
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of British Columbia Ferry Services Inc("Confidential Information").
 * You shall not disclose such Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with British Columbia Ferry Services Inc.
 *
 * Last Modified: 02/07/19 14:01
 */

package com.bcf.bcfstorefrontaddon.controllers.misc;

import static com.bcf.bcfstorefrontaddon.constants.BcfstorefrontaddonWebConstants.CANCELLATION_RESULT;
import static com.bcf.bcfstorefrontaddon.constants.BcfstorefrontaddonWebConstants.ONLY_TRANSPORT_BOOKING_CANCELLATION;

import de.hybris.platform.acceleratorstorefrontcommons.annotations.RequireHardLogIn;
import de.hybris.platform.acceleratorstorefrontcommons.controllers.AbstractController;
import de.hybris.platform.assistedservicefacades.AssistedServiceFacade;
import de.hybris.platform.commercefacades.order.OrderFacade;
import de.hybris.platform.commercefacades.product.data.PriceData;
import de.hybris.platform.commerceservices.enums.SalesApplication;
import de.hybris.platform.core.model.order.OrderModel;
import de.hybris.platform.servicelayer.session.SessionService;
import de.hybris.platform.servicelayer.user.UserService;
import de.hybris.platform.travelfacades.facades.TravellerFacade;
import de.hybris.platform.travelfacades.order.TravelCartFacade;
import java.math.BigDecimal;
import java.util.Objects;
import javax.annotation.Resource;
import org.apache.commons.lang3.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;
import com.bcf.bcfstorefrontaddon.constants.BcfstorefrontaddonConstants;
import com.bcf.bcfstorefrontaddon.constants.BcfstorefrontaddonControllerConstants;
import com.bcf.bcfstorefrontaddon.constants.BcfstorefrontaddonWebConstants;
import com.bcf.bcfvoyageaddon.enums.BookingsViewType;
import com.bcf.core.accommodation.data.BcfChangeCancellationVacationFeeData;
import com.bcf.core.bcfproperties.BcfConfigurablePropertiesService;
import com.bcf.core.constants.BcfCoreConstants;
import com.bcf.core.service.BcfBookingService;
import com.bcf.core.services.vacation.strategies.CancellationFeeStrategy;
import com.bcf.facades.bcffacades.BCFReservationFacade;
import com.bcf.facades.bcffacades.BcfBookingFacade;
import com.bcf.facades.bcffacades.BcfSalesApplicationResolverFacade;
import com.bcf.facades.constants.BcfFacadesConstants;
import com.bcf.facades.ebooking.CancelBookingIntegrationFacade;
import com.bcf.facades.helper.BcfTravelCartFacadeHelper;
import com.bcf.facades.order.cancel.CancelBookingResponseData;
import com.bcf.facades.order.cancel.enums.CancelStatus;
import com.bcf.facades.strategies.impl.PackageBookingCancellationStrategy;
import com.bcf.integrations.cancelbooking.order.response.CancelBookingResponseForOrder;
import com.bcf.integrations.cancelbooking.response.CancellationResult;
import com.bcf.integrations.core.exception.IntegrationException;


/**
 * Controller for Manage Booking pages
 */
@Controller
@RequestMapping("/manage-booking")
public class ManageBookingController extends AbstractController
{
	private static final String REFUND_ORDER_RESULT = "text.page.managemybooking.refund.order.result";
	private static final String CANCEL_ORDER_SUCCESSFUL = "text.page.managemybooking.cancel.order.successful";
	private static final String ACCOUNT_BOOKINGS_UNLINK_ERROR = "account.bookings.unlink.error";
	private static final String BOOKING_DETAILS_PAGE = "/manage-booking/booking-details/";
	private static final String MY_ACCOUNT_BOOKING_DETAILS = "/my-account/my-bookings";
	private static final String MY_BUSINESS_ACCOUNT_BOOKING_DETAILS = "/my-business-account/my-business-bookings";
	private static final String DEPARTURE_TIME_WARNING = "departureTimeWarning";
	private static final String DEPARTURE_TIME_WARNING_CODE = "text.page.managemybooking.cancel.booking.modal.timerestriction.warning";
	private static final String CANCEL_PARTIAL_ORDER_SUCCESSFUL = "text.page.managemybooking.cancel.partial.order.successful";
	private static final String CANCEL_PARTIAL_ORDER_FAILED = "text.page.managemybooking.cancel.partial.order.failed";
	private static final String FAILURE_CANCEL_EBOOKINGS = "failureCancelEBookings";
	private static final String NOT_TRANSPORT_ONLY_BOOKING = "notTransportOnlyBooking";
	private static final String EXCEPTION_CANCEL_EBOOKINGS = "exceptionCancelEBookings";
	private static final String CANCEL_ORDER_FAILED = "text.page.managemybooking.cancel.order.failed";
	private static final String SUCCESSFULL_CANCEL_ORDER_CODES = "successfulCancelOrderCodes";
	private static final String FAILED_CANCELLED_ORDER_CODES = "failedCancelOrderCodes";
	private static final String NOT_TRANSPORT_ONLY_ORDER_CODES = "notTransportBookingOrderCodes";
	private static final String NO_SELECTION_FOR_CANCEL_ORDER_REQUEST = "text.page.multi.cancelbooking.no.selection";
	public static final String SUCCESS = "SUCCESS";
	public static final String FAILURE = "FAILURE";
	private static final Logger LOG = Logger.getLogger(ManageBookingController.class);

	@Resource(name = "travellerFacade")
	private TravellerFacade travellerFacade;

	@Resource(name = "bookingFacade")
	private BcfBookingFacade bcfTravelBookingFacade;

	@Resource(name = "reservationFacade")
	private BCFReservationFacade bcfReservationFacade;

	@Resource(name = "bcfSalesApplicationResolverFacade")
	private BcfSalesApplicationResolverFacade bcfSalesApplicationResolverFacade;

	@Resource(name = "bcfConfigurablePropertiesService")
	private BcfConfigurablePropertiesService bcfConfigurablePropertiesService;

	@Resource(name = "cartFacade")
	private TravelCartFacade travelCartFacade;

	@Resource(name = "sessionService")
	private SessionService sessionService;

	@Resource(name = "packageBookingCancellationStrategy")
	private PackageBookingCancellationStrategy packageBookingCancellationStrategy;

	@Resource(name = "cancelBookingIntegrationFacade")
	private CancelBookingIntegrationFacade cancelBookingIntegrationFacade;

	@Resource(name = "bcfBookingService")
	private BcfBookingService bcfBookingService;

	@Resource(name = "bcfCancellationFeeStrategy")
	private CancellationFeeStrategy cancellationFeeStrategy;

	@Resource(name = "bcfTravelCartFacadeHelper")
	private BcfTravelCartFacadeHelper bcfTravelCartFacadeHelper;

	@Resource(name = "assistedServiceFacade")
	private AssistedServiceFacade assistedServiceFacade;

	@Resource(name = "userService")
	private UserService userService;

	/**
	 * Method to get the total amount to be refunded in case of cancel order
	 *
	 * @param model
	 * @param orderCode Code as the code of the order to be cancelled
	 * @return the string representing the formatted value of the refund
	 */
	@RequestMapping(value = "/cancel-booking-request/{orderCode}", method = RequestMethod.GET)
	@RequireHardLogIn
	public String getRefundConfirmation(final Model model, @PathVariable final String orderCode)
	{
		final boolean isCancelPossible = bcfTravelBookingFacade.isCancelPossible(orderCode);
		if (isCancelPossible)
		{
			model.addAttribute(BcfstorefrontaddonWebConstants.IS_CANCEL_POSSIBLE, true);

			final OrderModel order = bcfBookingService.getOrder(orderCode);
			final BigDecimal totalPaid = bcfBookingService.calculateTotalToRefund(order);
			final BcfChangeCancellationVacationFeeData bcfChangeCancellationVacationFeeData = packageBookingCancellationStrategy
					.adjustCancellationFee(totalPaid, cancellationFeeStrategy.calculateCancellationFee(order));
			BigDecimal totalCancelFee=packageBookingCancellationStrategy.getTotalCancellationFee(bcfChangeCancellationVacationFeeData);
			final PriceData totalToRefund = packageBookingCancellationStrategy
					.getRefundTotal(bcfBookingService.calculateTotalToRefund(order), order.getCurrency().getDigits(),
							order.getCurrency().getIsocode(), totalCancelFee);
			if (bcfSalesApplicationResolverFacade.getCurrentSalesChannel()
					.equals(SalesApplication.CALLCENTER.getCode()))
			{
				final int thresholdTimeGap = Integer.parseInt(bcfConfigurablePropertiesService
						.getBcfPropertyValue(BcfstorefrontaddonConstants.CANCEL_BOOKING_RESTRICTION_TIMEGAP));
				final boolean enabled = bcfReservationFacade.isFirstTOMeetsDepartureTimeRestrictionGap(
						bcfTravelBookingFacade.getBookingByBookingReference(orderCode), thresholdTimeGap);
				if (!enabled)
				{
					model.addAttribute(DEPARTURE_TIME_WARNING, DEPARTURE_TIME_WARNING_CODE);
				}
			}
			model.addAttribute(BcfstorefrontaddonWebConstants.TOTAL_TO_REFUND, totalToRefund.getFormattedValue());
			model.addAttribute(BcfstorefrontaddonWebConstants.CANCEL_FEE, packageBookingCancellationStrategy.getTotalCancellationFeePriceData(totalCancelFee,order.getCurrency().getIsocode()).getFormattedValue());

		}
		else
		{
			model.addAttribute(BcfstorefrontaddonWebConstants.IS_CANCEL_POSSIBLE, false);
		}
		return BcfstorefrontaddonControllerConstants.Views.Pages.BookingDetails.cancelOrderConfirmResponse;
	}

	/**
	 * Method to cancel an order
	 *
	 * @param redirectModel
	 * @param orderCode     as the code of the order to be cancelled
	 * @return the redirect to the Cancel Confirmation page
	 */
	@RequestMapping(value = "/cancel-booking/{orderCode}", method = RequestMethod.POST)
	@RequireHardLogIn
	public String cancelOrder(final RedirectAttributes redirectModel, @PathVariable final String orderCode)
	{
		final OrderModel order = bcfBookingService.getOrder(orderCode);
		final BigDecimal totalPaid = bcfBookingService.calculateTotalToRefund(order);
		final BcfChangeCancellationVacationFeeData bcfChangeCancellationVacationFeeData = packageBookingCancellationStrategy
				.adjustCancellationFee(totalPaid, cancellationFeeStrategy.calculateCancellationFee(order));
		BigDecimal totalCancelFee=packageBookingCancellationStrategy.getTotalCancellationFee(bcfChangeCancellationVacationFeeData);
		final PriceData totalToRefund = packageBookingCancellationStrategy
				.getRefundTotal(bcfBookingService.calculateTotalToRefund(order), order.getCurrency().getDigits(),
						order.getCurrency().getIsocode(), totalCancelFee);
		 sessionService.setAttribute(BcfFacadesConstants.BOOKING_CONFIRMATION_REFERENCE,orderCode);

		if (assistedServiceFacade.isAssistedServiceAgentLoggedIn() && totalToRefund.getValue().doubleValue() > 0d)
		{

			return REDIRECT_PREFIX + "/manage-booking/refund/refund-selection/" + orderCode;
		}


		final String bookingJourney = bcfTravelBookingFacade.getBookingJourneyType(orderCode);

		sessionService.setAttribute(BcfCoreConstants.SESSION_BOOKING_JOURNEY, bookingJourney);
		CancelBookingResponseData cancelBookingResponseData = null;
		try
		{
			cancelBookingResponseData = packageBookingCancellationStrategy.cancelCompleteOrder(orderCode, null);

		}
		catch (final Exception ex)
		{
			LOG.error("Problem occured while cancelling the order", ex);
			redirectModel.addFlashAttribute(CANCELLATION_RESULT, CANCEL_ORDER_FAILED);
			return REDIRECT_PREFIX + BOOKING_DETAILS_PAGE + orderCode;
		}

		if (Objects.equals(CancelStatus.COMPLETE_SUCCESS, cancelBookingResponseData.getCancelStatus()))
		{
			populateCancellationResultInfo(redirectModel, orderCode, false, CANCEL_ORDER_SUCCESSFUL);
			return REDIRECT_PREFIX + BOOKING_DETAILS_PAGE + orderCode;
		}

		redirectModel.addFlashAttribute(FAILURE_CANCEL_EBOOKINGS,
				cancelBookingResponseData.getFailureCancelEBookings());
		if (Objects.equals(CancelStatus.COMPLETE_FAIL, cancelBookingResponseData.getCancelStatus()))
		{
			redirectModel.addFlashAttribute(EXCEPTION_CANCEL_EBOOKINGS, cancelBookingResponseData.getException());
		}
		else if (Objects.equals(CancelStatus.PARTIAL_SUCCESS, cancelBookingResponseData.getCancelStatus()))
		{
			populateCancellationResultInfo(redirectModel, orderCode, false, CANCEL_PARTIAL_ORDER_SUCCESSFUL);
		}
		else if (Objects.equals(CancelStatus.PARTIAL_FAIL, cancelBookingResponseData.getCancelStatus()))
		{
			populateCancellationResultInfo(redirectModel, orderCode, false, CANCEL_PARTIAL_ORDER_FAILED);
		}

		return REDIRECT_PREFIX + BOOKING_DETAILS_PAGE + orderCode;
	}

	protected void populateCancellationResultInfo(final RedirectAttributes redirectModel, final String orderCode,
			final boolean populateRefundInfo, final String cancellationResult)
	{
		redirectModel.addFlashAttribute(CANCELLATION_RESULT, cancellationResult);
		if (populateRefundInfo)
		{
			redirectModel.addFlashAttribute(BcfstorefrontaddonWebConstants.REFUND_RESULT, REFUND_ORDER_RESULT);
			final PriceData refundedAmount = bcfTravelBookingFacade.getRefundTotal(orderCode);
			redirectModel.addFlashAttribute(BcfstorefrontaddonWebConstants.REFUNDED_AMOUNT, refundedAmount);
		}
	}

	@RequestMapping(value = "/accept-booking/{orderCode}")
	@RequireHardLogIn
	public String acceptOrder(@PathVariable final String orderCode)
	{
		final boolean isStatusChanged = bcfTravelBookingFacade.acceptOrder(orderCode);
		if (!isStatusChanged)
		{
			return REDIRECT_PREFIX + ROOT;
		}
		return REDIRECT_PREFIX + BOOKING_DETAILS_PAGE + orderCode;
	}

	@RequestMapping(value = "/unlink-booking/{orderCode}")
	@RequireHardLogIn
	public String deleteBooking(@PathVariable final String orderCode, final RedirectAttributes redirectModel)
	{
		final boolean isBookingDeleted = bcfTravelBookingFacade.unlinkBooking(orderCode);
		if (!isBookingDeleted)
		{
			redirectModel.addFlashAttribute(BcfstorefrontaddonWebConstants.UNLINK_RESULT,
					ACCOUNT_BOOKINGS_UNLINK_ERROR);
		}
		return REDIRECT_PREFIX + MY_ACCOUNT_BOOKING_DETAILS;
	}

	/**
	 * Checks if amendment cart exists in session
	 *
	 * @return
	 */
	@RequestMapping(value = "/check-amendmentcart-in-session", method = RequestMethod.GET, produces = "application/json")
	public String isAmendmentCartInSession(final Model model)
	{
		model.addAttribute(BcfstorefrontaddonWebConstants.IS_AMENDMENTCART, travelCartFacade.isAmendmentCart());
		return BcfstorefrontaddonControllerConstants.Views.Pages.BookingDetails.isAmendedCartResponse;
	}

	@RequestMapping(value = "/delete-amendment-cart", method = RequestMethod.POST, produces = "application/json")
	public @ResponseBody
	boolean deleteAmendmentCartAjax()
	{
		travelCartFacade.deleteCurrentCart();
		return true;
	}

	/**
	 * Method to cancel an order
	 *
	 * @param redirectModel
	 * @param cancelBookingRefs as the code of the orders to be cancelled
	 * @return the redirect to the Cancel Confirmation page
	 */
	@RequestMapping(value = "/multi-cancel-bookings", method = RequestMethod.POST)
	@RequireHardLogIn
	public String cancelOrders(@RequestParam("cancelBookingRefs") final String cancelBookingRefs, final RedirectAttributes
			redirectModel)
	{
		if (cancelBookingRefs == null || cancelBookingRefs.isEmpty())
		{
			redirectModel.addFlashAttribute(BcfstorefrontaddonWebConstants.EMPTY_CANCEL_ORDER_REQUEST,
					NO_SELECTION_FOR_CANCEL_ORDER_REQUEST);
			return REDIRECT_PREFIX + MY_ACCOUNT_BOOKING_DETAILS;
		}

		final String[] orderCodes = cancelBookingRefs.split(",");
		final StringBuilder successfulCancelOrderCodes = new StringBuilder();
		final StringBuilder failedCancelOrderCodes = new StringBuilder();
		final StringBuilder notTransportBookingOrderCodes = new StringBuilder();
		for (final String orderCode : orderCodes)
		{
			final String bookingJourney = bcfTravelBookingFacade.getBookingJourneyType(orderCode);
			sessionService.setAttribute(BcfCoreConstants.SESSION_BOOKING_JOURNEY, bookingJourney);
			if (com.bcf.core.constants.BcfCoreConstants.BOOKING_TRANSPORT_ONLY.equalsIgnoreCase(bookingJourney))
			{
				CancelBookingResponseData cancelBookingResponseData = null;
				try
				{
					cancelBookingResponseData = packageBookingCancellationStrategy.cancelCompleteOrder(orderCode, null);
				}
				catch (final Exception ex)
				{
					redirectModel.addFlashAttribute(FAILURE_CANCEL_EBOOKINGS,
							CANCEL_ORDER_FAILED);
				}
				if (cancelBookingResponseData != null && Objects
						.equals(CancelStatus.COMPLETE_SUCCESS, cancelBookingResponseData.getCancelStatus()))
				{
					successfulCancelOrderCodes.append(" ").append(orderCode).append(" ");
					redirectModel.addFlashAttribute(SUCCESSFULL_CANCEL_ORDER_CODES,
							successfulCancelOrderCodes);
					redirectModel.addFlashAttribute(CANCELLATION_RESULT,
							CANCEL_ORDER_SUCCESSFUL);
				}
				else
				{
					failedCancelOrderCodes.append(orderCode).append(" ");
					redirectModel.addFlashAttribute(FAILED_CANCELLED_ORDER_CODES,
							failedCancelOrderCodes);
					redirectModel.addFlashAttribute(FAILURE_CANCEL_EBOOKINGS, FAILURE_CANCEL_EBOOKINGS);
				}
			}
			else
			{
				notTransportBookingOrderCodes.append(orderCode).append(" ");
				redirectModel.addFlashAttribute(NOT_TRANSPORT_ONLY_ORDER_CODES,
						notTransportBookingOrderCodes);
				redirectModel.addFlashAttribute(NOT_TRANSPORT_ONLY_BOOKING, ONLY_TRANSPORT_BOOKING_CANCELLATION);
			}
		}
		return REDIRECT_PREFIX + MY_ACCOUNT_BOOKING_DETAILS;
	}

	/**
	 * Method to cancel bookings from ebooking system
	 *
	 * @param cancelBookingRefs as the booking references to be cancelled
	 * @param redirectModel
	 * @return the redirect to the booking details page
	 */
	@RequestMapping(value = "/{account-type}/confirm-cancellation", method = RequestMethod.POST)
	public String ebookingCancellation(@PathVariable("account-type") final String accountType,
			@RequestParam("cancelBookingRefs") final String cancelBookingRefs, final RedirectAttributes redirectModel)
			throws Exception
	{
		if (cancelBookingRefs == null || cancelBookingRefs.isEmpty())
		{
			redirectModel.addFlashAttribute(BcfstorefrontaddonWebConstants.EMPTY_CANCEL_ORDER_REQUEST,
					NO_SELECTION_FOR_CANCEL_ORDER_REQUEST);
			return redirectToBookingsDetails(accountType);
		}
		CancelBookingResponseForOrder cancelBookingResponseForOrder = null;
		try
		{
			cancelBookingResponseForOrder = cancelBookingIntegrationFacade.cancelMultipleBookings(cancelBookingRefs);
		}
		catch (final IntegrationException ie)
		{
			redirectModel.addFlashAttribute(FAILURE_CANCEL_EBOOKINGS, CANCEL_ORDER_FAILED);
		}

		String bookingReference = StringUtils.EMPTY;

		if (cancelBookingResponseForOrder != null)
		{
			final StringBuilder successfulCancelOrderCodes = new StringBuilder();
			final StringBuilder failureCancelEBookings = new StringBuilder();

			for (CancellationResult cancellationResult : cancelBookingResponseForOrder.getResult())
			{
				if ((SUCCESS).equals(cancellationResult.getResult()))
				{
					bookingReference = cancellationResult.getBookingReference().getBookingReference();
					successfulCancelOrderCodes.append(" ").append(bookingReference)
							.append(" ");

				}
				else if ((FAILURE).equals(cancellationResult.getResult()))
				{
					failureCancelEBookings.append(" ").append(cancellationResult.getBookingReference().getBookingReference())
							.append(" ");
				}
			}

			if (successfulCancelOrderCodes.length() > 0)
			{
				redirectModel.addFlashAttribute(SUCCESSFULL_CANCEL_ORDER_CODES,
						successfulCancelOrderCodes);
				redirectModel.addFlashAttribute(CANCELLATION_RESULT,
						CANCEL_ORDER_SUCCESSFUL);
			}
			if (failureCancelEBookings.length() > 0)
			{
				redirectModel.addFlashAttribute(FAILED_CANCELLED_ORDER_CODES,
						failureCancelEBookings);
				redirectModel.addFlashAttribute(FAILURE_CANCEL_EBOOKINGS, CANCEL_ORDER_FAILED);
			}
		}

		if (userService.isAnonymousUser(userService.getCurrentUser()))
		{
			final String guestBookingIdentifier = bcfTravelBookingFacade.getOrderCodeForEBookingCode(bookingReference);
			if (StringUtils.isNotBlank(guestBookingIdentifier))
			{
				return REDIRECT_PREFIX + BcfCoreConstants.GUEST_BOOKING_DETAILS_PAGE + guestBookingIdentifier;
			}
		}

		return redirectToBookingsDetails(accountType);
	}

	private String redirectToBookingsDetails(final String bookingType)
	{
		if (StringUtils.equalsIgnoreCase(BookingsViewType.BUSINESS.getCode(), bookingType))
		{
			return REDIRECT_PREFIX + MY_BUSINESS_ACCOUNT_BOOKING_DETAILS;
		}
		return REDIRECT_PREFIX + MY_ACCOUNT_BOOKING_DETAILS;
	}

}
