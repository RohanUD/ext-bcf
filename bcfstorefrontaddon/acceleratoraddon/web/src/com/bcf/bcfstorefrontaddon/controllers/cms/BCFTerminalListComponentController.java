/*
 * Copyright (c) 2019 British Columbia Ferry Services Inc
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of British Columbia Ferry Services Inc("Confidential Information").
 * You shall not disclose such Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with British Columbia Ferry Services Inc.
 *
 * Last Modified: 02/07/19 14:01
 */

package com.bcf.bcfstorefrontaddon.controllers.cms;

import de.hybris.platform.commerceservices.i18n.CommerceCommonI18NService;
import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import com.bcf.bcfstorefrontaddon.constants.BcfstorefrontaddonControllerConstants;
import com.bcf.bcfstorefrontaddon.model.components.BCFTerminalListComponentModel;
import com.bcf.core.strategies.responsive.BCFResponsiveMediaStrategy;


@Controller("BCFTerminalListComponentController")
@RequestMapping(value = BcfstorefrontaddonControllerConstants.Actions.Cms.BCFTerminalListComponent)
public class BCFTerminalListComponentController
		extends SubstitutingCMSAddOnComponentController<BCFTerminalListComponentModel>
{
	private static final String DATA_MEDIA = "dataMedia";
	@Resource(name = "commerceCommonI18NService")
	private CommerceCommonI18NService commerceCommonI18NService;

	@Resource(name = "bcfResponsiveMediaStrategy")
	private BCFResponsiveMediaStrategy bcfResponsiveMediaStrategy;

	@Override
	protected void fillModel(final HttpServletRequest request, final Model model, final BCFTerminalListComponentModel component)
	{
		model.addAttribute(DATA_MEDIA,
				bcfResponsiveMediaStrategy.getResponsiveJson(component.getImage(commerceCommonI18NService.getCurrentLocale())));
	}
}
