/*
 * Copyright (c) 2019 British Columbia Ferry Services Inc
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of British Columbia Ferry Services Inc("Confidential Information").
 * You shall not disclose such Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with British Columbia Ferry Services Inc.
 *
 * Last Modified: 26/06/19 20:25
 */

package com.bcf.bcfstorefrontaddon.controllers.pages;

import de.hybris.platform.acceleratorfacades.ordergridform.OrderGridFormFacade;
import de.hybris.platform.acceleratorfacades.product.data.ReadOnlyOrderGridData;
import de.hybris.platform.acceleratorstorefrontcommons.annotations.RequireHardLogIn;
import de.hybris.platform.acceleratorstorefrontcommons.controllers.util.GlobalMessages;
import de.hybris.platform.assistedservicefacades.AssistedServiceFacade;
import de.hybris.platform.cms2.exceptions.CMSItemNotFoundException;
import de.hybris.platform.cms2.model.pages.ContentPageModel;
import de.hybris.platform.commercefacades.comment.data.CommentData;
import de.hybris.platform.commercefacades.order.data.AbstractOrderData;
import de.hybris.platform.commercefacades.order.data.OrderEntryData;
import de.hybris.platform.commercefacades.product.ProductOption;
import de.hybris.platform.commercefacades.quote.data.QuoteData;
import de.hybris.platform.commerceservices.enums.QuoteAction;
import de.hybris.platform.core.model.order.CartModel;
import de.hybris.platform.order.exceptions.CalculationException;
import java.util.Arrays;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Set;
import java.util.stream.Collectors;
import javax.annotation.Resource;
import org.apache.commons.collections4.CollectionUtils;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;
import com.bcf.bcfstorefrontaddon.constants.BcfstorefrontaddonControllerConstants;
import com.bcf.bcfstorefrontaddon.controllers.imported.AcceleratorControllerConstants;
import com.bcf.core.bcfproperties.BcfConfigurablePropertiesService;
import com.bcf.facades.cart.QuoteCartData;
import com.bcf.facades.constants.BcfFacadesConstants;
import com.bcf.facades.quote.BcfQuoteFacade;
import com.bcf.facades.reservation.data.BcfGlobalReservationData;


@Controller
@RequestMapping(value = "/my-account/my-quotes")
public class MyQuotesController extends BcfAbstractPageController
{
	private static final String MY_QUOTES_CMS_PAGE = "my-quotes";
	private static final String QUOTES_DETAILS_PAGE = "quoteDetailsPage";
	private static final String PAGINATION_NUMBER_OF_COMMENTS = "quote.pagination.numberofcomments";
	private static final String ALLOWED_ACTIONS = "allowedActions";
	private static final String HOME_PAGE_PATH = "/";
	private static final String STARTING_NUMBER_OF_RESULTS = "startingNumberOfResults";
	private static final String TOTAL_NUMBER_OF_RESULTS = "totalNumberOfResults";
	private static final String TOTAL_SHOWN_RESULTS = "totalShownResults";
	private static final String PAGE_NUM = "pageNum";
	private static final String HAS_MORE_RESULTS = "hasMoreResults";
	private static final String QUOTES_CANCEL_SUCCESS_MESSAGE = "quote.cancel.success";
	private static final String QUOTES_SELCT_TO_CANCEL_MESSAGE = "quote.select.to.cancel";
	private static final String SESSION_CART_PARAMETER_NAME = "cart";

	@Resource(name = "bcfConfigurablePropertiesService")
	private BcfConfigurablePropertiesService bcfConfigurablePropertiesService;

	@Resource(name = "bcfQuoteFacade")
	private BcfQuoteFacade quoteFacade;

	@Resource(name = "orderGridFormFacade")
	private OrderGridFormFacade orderGridFormFacade;

	@Resource(name = "assistedServiceFacade")
	private AssistedServiceFacade assistedServiceFacade;

	@RequestMapping(method = RequestMethod.GET)
	@RequireHardLogIn
	public String quotes(@RequestParam(value = "pageNum", required = false, defaultValue = "1") final Integer pageNumber,
			final Model model) throws CMSItemNotFoundException
	{
		// Handle paged search results

		populateCommonModelAttributes(0, 1, quoteFacade.getPagedQuote(), model);
		final ContentPageModel contentPageModel = getContentPageForLabelOrId(MY_QUOTES_CMS_PAGE);
		storeCmsPageInModel(model, contentPageModel);
		setUpMetaDataForContentPage(model, contentPageModel);
		return getViewForPage(model);
	}

	@RequestMapping(value = "/{cartCode}", method = RequestMethod.GET)
	@RequireHardLogIn
	public String quote(final Model model, final RedirectAttributes redirectModel,
			@PathVariable("cartCode") final String cartCode) throws CMSItemNotFoundException
	{
		try
		{
			final BcfGlobalReservationData bcfGlobalReservationData = getQuoteFacade().retrieveCart(cartCode);
			if (Objects.isNull(bcfGlobalReservationData))
			{
				return REDIRECT_PREFIX + HOME_PAGE_PATH;
			}
			model.addAttribute("bcfGlobalReservationData", bcfGlobalReservationData);

			final Boolean isASMSession = assistedServiceFacade.getAsmSession() != null;

			final Date quoteExpiryDate = quoteFacade.getQuoteExpiryDate(cartCode);
			model.addAttribute("quoteExpiryDate", quoteExpiryDate);
			model.addAttribute("quoteID", cartCode);
			if (!isASMSession)
			{
				model.addAttribute("isQuoteExpired", quoteFacade.isQuoteExpired(quoteExpiryDate));
			}
			model.addAttribute("isASMSession", isASMSession);

			final CartModel cart = getSessionService().getAttribute(SESSION_CART_PARAMETER_NAME);
			if (Objects.nonNull(cart))
			{
				model.addAttribute("sessionCartCode", cart.getCode());
			}
		}
		catch (final CalculationException e)
		{
			return REDIRECT_PREFIX + HOME_PAGE_PATH;
		}
		storeCmsPageInModel(model, getContentPageForLabelOrId(QUOTES_DETAILS_PAGE));
		setUpMetaDataForContentPage(model, getContentPageForLabelOrId(QUOTES_DETAILS_PAGE));

		return getViewForPage(model);
	}

	/**
	 * Show more method to return lazy loaded properties.
	 *
	 * @param pageNumber the page number
	 * @param model      the model
	 * @return the string
	 */
	@RequestMapping(value = "/show-more", method = RequestMethod.POST)
	public String showMore(@RequestParam(value = "pageNumber", required = false) final int pageNumber, final Model model)
	{
		populateCommonModelAttributes(pageNumber - 1, pageNumber, quoteFacade.getPagedQuote(), model);
		return BcfstorefrontaddonControllerConstants.Views.Pages.QuoteListing.QuoteListing;
	}

	/**
	 * Cancel the selected quotes.
	 *
	 * @param codes the Quotes code
	 * @param model the model
	 * @return the string
	 */
	@RequestMapping(value = "/cancel-quotes", method = RequestMethod.POST)
	public String cancelQuotes(@RequestParam(value = "codes", required = false) final List<String> codes, final Model model)
	{
		if (Objects.nonNull(codes))
		{
			quoteFacade.cancelQuotes(codes);
			GlobalMessages.addInfoMessage(model, QUOTES_CANCEL_SUCCESS_MESSAGE);
		}
		else
		{
			GlobalMessages.addInfoMessage(model, QUOTES_SELCT_TO_CANCEL_MESSAGE);
		}
		return getViewForPage(model);
	}

	@RequestMapping(value = "/{quoteCode}/getReadOnlyProductVariantMatrix", method = RequestMethod.GET)
	@RequireHardLogIn
	public String getProductVariantMatrixForResponsive(@PathVariable("quoteCode") final String quoteCode,
			@RequestParam("productCode") final String productCode, final Model model)
	{
		final QuoteData quoteData = getQuoteFacade().getQuoteForCode(quoteCode);
		final Map<String, ReadOnlyOrderGridData> readOnlyMultiDMap = orderGridFormFacade.getReadOnlyOrderGridForProductInOrder(
				productCode, Arrays.asList(ProductOption.BASIC, ProductOption.CATEGORIES), quoteData);
		model.addAttribute("readOnlyMultiDMap", readOnlyMultiDMap);

		return AcceleratorControllerConstants.Views.Fragments.Checkout.ReadOnlyExpandedOrderForm;
	}


	protected void sortComments(final AbstractOrderData orderData)
	{
		if (orderData != null)
		{
			setOrderDataComments(orderData);

			if (CollectionUtils.isNotEmpty(orderData.getEntries()))
			{
				for (final OrderEntryData orderEntry : orderData.getEntries())
				{
					setOrderEntryComments(orderEntry);
				}
			}
		}
	}

	private void setOrderEntryComments(final OrderEntryData orderEntry)
	{
		if (CollectionUtils.isNotEmpty(orderEntry.getComments()))
		{
			final List<CommentData> sortedEntryComments = orderEntry.getComments().stream()
					.sorted((comment1, comment2) -> comment2.getCreationDate().compareTo(comment1.getCreationDate()))
					.collect(Collectors.toList());

			orderEntry.setComments(sortedEntryComments);
		}
		else if (orderEntry.getProduct() != null && orderEntry.getProduct().getMultidimensional() != null
				&& orderEntry.getProduct().getMultidimensional() && CollectionUtils.isNotEmpty(orderEntry.getEntries()))
		{
			for (final OrderEntryData multiDOrderEntry : orderEntry.getEntries())
			{
				if (CollectionUtils.isNotEmpty(multiDOrderEntry.getComments()))
				{
					final List<CommentData> sortedMultiDOrderEntryComments = multiDOrderEntry.getComments().stream()
							.sorted((comment1, comment2) -> comment2.getCreationDate().compareTo(comment1.getCreationDate()))
							.collect(Collectors.toList());

					multiDOrderEntry.setComments(sortedMultiDOrderEntryComments);
				}
			}
		}
	}

	private void setOrderDataComments(final AbstractOrderData orderData)
	{
		if (CollectionUtils.isNotEmpty(orderData.getComments()))
		{
			final List<CommentData> sortedComments = orderData.getComments().stream()
					.sorted((comment1, comment2) -> comment2.getCreationDate().compareTo(comment1.getCreationDate()))
					.collect(Collectors.toList());
			orderData.setComments(sortedComments);
		}
	}

	protected void loadCommentsShown(final Model model)
	{
		final int commentsShown = getSiteConfigService().getInt(PAGINATION_NUMBER_OF_COMMENTS, 5);
		model.addAttribute("commentsShown", Integer.valueOf(commentsShown));
	}


	/**
	 * Populate common model attributes.
	 *
	 * @param pageNumber    the page number
	 * @param quoteCartData the cart data
	 * @param model         the model
	 */
	protected void populateCommonModelAttributes(final int startIndex, final int pageNumber,
			final List<QuoteCartData> quoteCartData, final Model model)
	{
		final int quoteSearchPageSize = Integer.parseInt(
				bcfConfigurablePropertiesService.getBcfPropertyValue(BcfFacadesConstants.QUOTE_LISTING_PAGE_RECORDS));
		model.addAttribute("quotes",
				quoteCartData.subList(startIndex * quoteSearchPageSize,
						Math.min(pageNumber * quoteSearchPageSize, quoteCartData.size())));
		model.addAttribute(PAGE_NUM, pageNumber);
		model.addAttribute(TOTAL_NUMBER_OF_RESULTS, quoteCartData.size());
		model.addAttribute(STARTING_NUMBER_OF_RESULTS, (pageNumber - 1) * quoteSearchPageSize + 1);
		final Boolean hasMoreResults = pageNumber * quoteSearchPageSize < quoteCartData.size();
		model.addAttribute(HAS_MORE_RESULTS, hasMoreResults);
		model.addAttribute(TOTAL_SHOWN_RESULTS,
				hasMoreResults ? pageNumber * quoteSearchPageSize : quoteCartData.size());
	}

	/**
	 * Set allowed actions for a given quote on model.
	 *
	 * @param model     the MVC model
	 * @param quoteCode the quote to be checked.
	 */
	protected void setAllowedActions(final Model model, final String quoteCode)
	{
		final Set<QuoteAction> actionSet = getQuoteFacade().getAllowedActions(quoteCode);

		if (actionSet != null)
		{
			final Map<String, Boolean> actionsMap = actionSet.stream().collect(
					Collectors.toMap(QuoteAction::getCode, v -> Boolean.TRUE));
			model.addAttribute(ALLOWED_ACTIONS, actionsMap);
		}
	}

	protected BcfQuoteFacade getQuoteFacade()
	{
		return quoteFacade;
	}

}
