/*
 * Copyright (c) 2019 British Columbia Ferry Services Inc
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of British Columbia Ferry Services Inc("Confidential Information").
 * You shall not disclose such Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with British Columbia Ferry Services Inc.
 *
 * Last Modified: 02/07/19 12:36
 */

package com.bcf.bcfstorefrontaddon.controllers.pages;

import static com.bcf.bcfstorefrontaddon.constants.BcfstorefrontaddonWebConstants.CANCELLATION_RESULT;

import de.hybris.platform.acceleratorstorefrontcommons.annotations.RequireHardLogIn;
import de.hybris.platform.acceleratorstorefrontcommons.controllers.util.GlobalMessages;
import de.hybris.platform.assistedservicefacades.AssistedServiceFacade;
import de.hybris.platform.cms2.exceptions.CMSItemNotFoundException;
import de.hybris.platform.cms2.model.pages.ContentPageModel;
import de.hybris.platform.commercefacades.order.data.OrderData;
import de.hybris.platform.core.model.order.AbstractOrderModel;
import de.hybris.platform.core.model.order.OrderModel;
import de.hybris.platform.order.InvalidCartException;
import de.hybris.platform.servicelayer.session.SessionService;
import de.hybris.platform.stock.exception.InsufficientStockLevelException;
import de.hybris.platform.travelservices.enums.BookingJourneyType;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;
import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.tuple.Pair;
import org.apache.log4j.Logger;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;
import com.bcf.bcfstorefrontaddon.constants.BcfstorefrontaddonControllerConstants;
import com.bcf.bcfstorefrontaddon.form.validators.GoodWillRefundOptionValidator;
import com.bcf.bcfstorefrontaddon.form.validators.RefundOptionValidator;
import com.bcf.bcfstorefrontaddon.forms.RefundOptionForm;
import com.bcf.bcfstorefrontaddon.forms.RefundSelectionForm;
import com.bcf.bcfstorefrontaddon.util.BcfControllerUtil;
import com.bcf.core.accommodation.data.BcfChangeCancellationVacationFeeData;
import com.bcf.core.constants.BcfCoreConstants;
import com.bcf.core.payment.data.RefundTransactionInfoData;
import com.bcf.core.service.BCFTravelCartService;
import com.bcf.core.service.BcfBookingService;
import com.bcf.core.services.vacation.strategies.CancellationFeeStrategy;
import com.bcf.facades.bcffacades.BCFGoodWillRefundFacade;
import com.bcf.facades.bcffacades.BcfBookingFacade;
import com.bcf.facades.bcffacades.BcfSalesApplicationResolverFacade;
import com.bcf.facades.constants.BcfFacadesConstants;
import com.bcf.facades.exception.CartValidationException;
import com.bcf.facades.exception.OrderProcessingException;
import com.bcf.facades.helper.BcfTravelCartFacadeHelper;
import com.bcf.facades.order.BcfTravelCartFacade;
import com.bcf.facades.order.BcfTravelCheckoutFacade;
import com.bcf.facades.order.refund.GoodWillRefundData;
import com.bcf.facades.orderdecision.data.PlaceOrderResponseData;
import com.bcf.facades.payment.BcfRefundIntegrationFacade;
import com.bcf.facades.strategies.impl.PackageBookingCancellationStrategy;
import com.bcf.facades.vacations.ActivityGuestData;
import com.bcf.facades.vacations.RoomGuestData;
import com.bcf.fulfilmentprocess.order.util.NotificationUtils;
import com.bcf.integration.payment.exceptions.BcfRefundException;
import com.bcf.integrations.core.exception.IntegrationException;


@Controller
@RequestMapping({ "/checkout/refund", "/manage-booking/refund" })
public class RefundOptionController extends BcfAbstractPageController
{
	private static final Logger LOG = Logger.getLogger(RefundOptionController.class);
	@Resource(name = "bcfTravelCartFacadeHelper")
	private BcfTravelCartFacadeHelper bcfTravelCartFacadeHelper;

	@Resource(name = "refundOptionValidator")
	private RefundOptionValidator refundOptionValidator;

	@Resource(name = "goodWillRefundOptionValidator")
	private GoodWillRefundOptionValidator goodWillRefundOptionValidator;

	@Resource(name = "bcfRefundIntegrationFacade")
	private BcfRefundIntegrationFacade bcfRefundIntegrationFacade;

	@Resource(name = "bookingFacade")
	private BcfBookingFacade bookingFacade;

	@Resource(name = "cartService")
	private BCFTravelCartService bcfTravelCartService;

	@Resource(name = "notificationUtils")
	private NotificationUtils notificationUtils;

	@Resource(name = "bcfSalesApplicationResolverFacade")
	private BcfSalesApplicationResolverFacade bcfSalesApplicationResolverFacade;

	@Resource(name = "bcfTravelCheckoutFacade")
	private BcfTravelCheckoutFacade bcfTravelCheckoutFacade;

	@Resource(name = "bcfControllerUtil")
	private BcfControllerUtil bcfControllerUtil;

	@Resource(name = "packageBookingCancellationStrategy")
	private PackageBookingCancellationStrategy packageBookingCancellationStrategy;

	@Resource(name = "bcfBookingService")
	private BcfBookingService bcfBookingService;

	@Resource(name = "assistedServiceFacade")
	private AssistedServiceFacade assistedServiceFacade;

	@Resource(name = "sessionService")
	private SessionService sessionService;

	@Resource(name = "bcfGoodWillRefundFacade")
	private BCFGoodWillRefundFacade bcfGoodWillRefundFacade;

	@Resource(name="bcfCartFacade")
	private BcfTravelCartFacade bcfTravelCartFacade;

	@Resource(name = "bcfCancellationFeeStrategy")
	private CancellationFeeStrategy cancellationFeeStrategy;


	private static final String REFUND_OPTION_CMS_PAGE_LABEL = "refundOptionPage";
	private static final String ERROR_PLACE_ORDER = "checkout.error.placeorder";
	private static final String REFUND_INITIATED = "checkout.refund.initiated";
	private static final String CANCEL_ORDER_FAILED = "text.page.managemybooking.cancel.order.failed";
	private static final String GOOD_WILL_REFUND_ORDER_SUCCESS= "text.page.managemybooking.goodwill.refund.order.success";
	private static final String GOOD_WILL_REFUND_EMPTY_REFUND_CODE= "text.page.managemybooking.goodwill.refund.empty.refund.code";
	private static final String GOOD_WILL_REFUND_ORDER_FAILED = "text.page.managemybooking.goodwill.refund.order.failed";
	private static final String BOOKING_DETAILS_PAGE = "/manage-booking/booking-details/";
	private static final String REFUND_OPTION_FORM = "refundOptionForm";
	private static final String CANCEL_FEE = "cancelFee";
	private static final String ADD_REFUND_URL = "addRefundUrl";
	private static final String CANCEL_URL = "cancelUrl";
	private static final String VACATION_POLICY_TERMS_AND_CONDITIIONS = "vacationPolicyTermsAndConditions";
	private static final String NON_REFUNDABLE_COST_TERMS_CONDTIONS = "bcfNonRefundableCostTermsAndConditions";
	private static final String REFUND_REASON_CODE_DATA = "refundReasonCodeData";

	@RequestMapping(value = "/add-refund", method = RequestMethod.POST)
	@RequireHardLogIn
	public String submitRefund(final RefundOptionForm refundOptionForm,
			final BindingResult bindingResult, final HttpServletRequest request, final RedirectAttributes redirectModel,
			final Model model) throws CMSItemNotFoundException
	{

		refundOptionValidator.validate(refundOptionForm, bindingResult);
		final boolean hasErrorFlag = bindingResult.hasErrors();

		if (hasErrorFlag)
		{
			updateModel(model, refundOptionForm, request, bindingResult);
			return BcfstorefrontaddonControllerConstants.Views.Pages.Refund.multiRefundOptionPage;
		}
		else
		{

			AbstractOrderModel abstractOrderModel = null;
			final List<RefundTransactionInfoData> refundTransactionInfoDatas = new ArrayList<>();
			final String bookingReference = refundOptionForm.getBookingReference();
			updateRefundTransactionInfoDatas(refundTransactionInfoDatas, refundOptionForm);


			if (StringUtils.isEmpty(bookingReference))
			{
				final List<RoomGuestData> roomGuestdatas = refundOptionForm.getRoomGuestData();
				final List<ActivityGuestData> activityGuestDatas = refundOptionForm.getActivityLeadPaxInfo();
				if(CollectionUtils.isNotEmpty(roomGuestdatas)){

					bookingFacade.updateAccommodationOrderEntryGroup(refundOptionForm.getRoomGuestData());
					bookingFacade.updateActivityOrderEntryInfoFromAccomodationLeadPaxDetails(roomGuestdatas.get(0).getFirstName(),roomGuestdatas.get(0).getLastName());
				}else if(CollectionUtils.isNotEmpty(activityGuestDatas)){

					bookingFacade.updateActivityOrderEntryInfo(activityGuestDatas);
				}


				abstractOrderModel = bcfTravelCartService.getSessionCart();
				bcfRefundIntegrationFacade.refundOrder(abstractOrderModel, refundTransactionInfoDatas);
				try
				{
					final String channel = bcfSalesApplicationResolverFacade.getCurrentSalesChannel();
					PlaceOrderResponseData decisionData = new PlaceOrderResponseData();
					decisionData = bcfTravelCheckoutFacade.placeOrderAfterRefund(channel, decisionData);
					return redirectToNextPageURL(decisionData, redirectModel);
				}
				catch (final IntegrationException e)
				{
					LOG.error("IntegrationException encountered while performing refund and placing order");
					GlobalMessages.addFlashMessage(redirectModel, GlobalMessages.ERROR_MESSAGES_HOLDER, ERROR_PLACE_ORDER);
				}
				catch (final InvalidCartException | CartValidationException | OrderProcessingException | InsufficientStockLevelException e)
				{
					LOG.error("Exception encountered for while performing refund and placing order", e);
					GlobalMessages.addFlashMessage(redirectModel, GlobalMessages.ERROR_MESSAGES_HOLDER, ERROR_PLACE_ORDER);
				}

				return REDIRECT_PREFIX + bcfControllerUtil.getNextPageURL();

			}
			else
			{
				try
				{
					packageBookingCancellationStrategy.cancelCompleteOrder(bookingReference, refundTransactionInfoDatas);
					return REDIRECT_PREFIX + BOOKING_DETAILS_PAGE + bookingReference;

				}
				catch (final Exception ex)
				{
					LOG.error("Problem occured while cancelling the order", ex);
					redirectModel.addFlashAttribute(CANCELLATION_RESULT, CANCEL_ORDER_FAILED);
					return REDIRECT_PREFIX + BOOKING_DETAILS_PAGE + bookingReference;
				}

			}


		}
	}

	@RequestMapping(value = "/add-goodwill-refund", method = RequestMethod.POST)
	@RequireHardLogIn
	public String submitGoodWillRefund(final RefundOptionForm refundOptionForm,
			final BindingResult bindingResult, final HttpServletRequest request, final RedirectAttributes redirectModel,
			final Model model) throws CMSItemNotFoundException, Exception
	{

		goodWillRefundOptionValidator.validate(refundOptionForm, bindingResult);
		final boolean hasErrorFlag = bindingResult.hasErrors();

		if (hasErrorFlag)
		{
			updateModel(model, refundOptionForm, request, bindingResult);
			model.addAttribute(ADD_REFUND_URL, "/manage-booking/refund/add-goodwill-refund");
			model.addAttribute(REFUND_REASON_CODE_DATA, bcfGoodWillRefundFacade.getAllRefundReasonCodes());
			return BcfstorefrontaddonControllerConstants.Views.Pages.Refund.multiRefundOptionPage;
		}
		else
		{
			final List<RefundTransactionInfoData> refundTransactionInfoDatas = new ArrayList<>();
			final String bookingReference = refundOptionForm.getBookingReference();
			updateRefundTransactionInfoDatas(refundTransactionInfoDatas, refundOptionForm);

			try
			{
				bcfGoodWillRefundFacade
						.processGoodWillRefund(bookingReference, refundOptionForm.getGoodWillRefundCode(), refundOptionForm.getGoodWillRefundComment(),refundTransactionInfoDatas);
				GlobalMessages.addFlashMessage(redirectModel, GlobalMessages.CONF_MESSAGES_HOLDER,
						GOOD_WILL_REFUND_ORDER_SUCCESS);
				return REDIRECT_PREFIX + BOOKING_DETAILS_PAGE + bookingReference;

			}
			catch (final BcfRefundException ex)
			{
				LOG.error("Problem occured while refunding the order", ex);
				GlobalMessages.addFlashMessage(redirectModel, GlobalMessages.ERROR_MESSAGES_HOLDER,
						GOOD_WILL_REFUND_ORDER_FAILED);
				return REDIRECT_PREFIX + BOOKING_DETAILS_PAGE + bookingReference;
			}
		}
	}

	protected String redirectToOrderConfirmationPage(final OrderData orderData)
	{
		final StringBuilder orderCodeToAppend = new StringBuilder();
		if (org.apache.commons.lang.StringUtils.isNotBlank(orderData.getOriginalOrderCode()))
		{
			orderCodeToAppend.append(orderData.getOriginalOrderCode());
		}
		else
		{
			orderCodeToAppend.append(orderData.getCode());
		}
		return REDIRECT_PREFIX + "/checkout/bookingConfirmation/" + orderCodeToAppend.toString();
	}


	@RequestMapping(value = "/refund-selection", method = RequestMethod.GET)
	@RequireHardLogIn
	public String refundSelection(final Model model, final HttpServletRequest request) throws CMSItemNotFoundException
	{
		if (assistedServiceFacade.isAssistedServiceAgentLoggedIn())
		{
			final AbstractOrderModel abstractOrderModel = bcfTravelCartService.getSessionCart();
			final Pair<Double, List<RefundTransactionInfoData>> refundTransactionInfosPair = bcfTravelCartFacadeHelper
					.getRefundTransactionInfo(null);
			model.addAttribute(ADD_REFUND_URL, "/checkout/refund/add-refund");

			if (bcfTravelCartFacadeHelper.isAlacateFlow())
			{
				model.addAttribute(CANCEL_URL, "/alacarte-review");
			}
			else
			{
				model.addAttribute(CANCEL_URL, "/cart/summary");
			}


			if (BookingJourneyType.BOOKING_PACKAGE.getCode()
					.equals(sessionService.getAttribute(BcfCoreConstants.SESSION_BOOKING_JOURNEY)) || BookingJourneyType.BOOKING_ALACARTE.getCode()
					.equals(sessionService.getAttribute(BcfCoreConstants.SESSION_BOOKING_JOURNEY)))
			{
				final List<RoomGuestData> roomGuestDatas = getBcfTravelCartFacade()
						.getRoomDatas(abstractOrderModel.getEntries().stream().filter(entry -> entry.getActive()).collect(
						Collectors.toList()));
				model.addAttribute(BcfFacadesConstants.ROOM_GUEST_DATAS,roomGuestDatas);
			}
			else if (BookingJourneyType.BOOKING_ACTIVITY_ONLY.getCode()
					.equals(getSessionService().getAttribute(BcfCoreConstants.SESSION_BOOKING_JOURNEY)))
			{
				final List<ActivityGuestData> activityGuestDatas = getBcfTravelCartFacade().getUniqueActivityDatas(abstractOrderModel.getEntries().stream().filter(entry->entry.getActive()).collect(
						Collectors.toList()));
				if (CollectionUtils.isNotEmpty(activityGuestDatas))
				{
					model.addAttribute(BcfFacadesConstants.ACTIVITY_DATAS, activityGuestDatas);
				}

			}

			final RefundOptionForm refundOptionForm = populateRefundSelectionForm(abstractOrderModel, refundTransactionInfosPair);
			model.addAttribute(REFUND_OPTION_FORM, refundOptionForm);
			model.addAttribute(VACATION_POLICY_TERMS_AND_CONDITIIONS,
					bcfTravelCartFacadeHelper.getVacationPolicyTermsAndConditions(null));
			model.addAttribute(NON_REFUNDABLE_COST_TERMS_CONDTIONS,
					bcfTravelCartFacadeHelper.getBCFNonRefundableCostTermsAndConditions(null));
			model.addAttribute(BcfFacadesConstants.SALES_CHANNEL, bcfSalesApplicationResolverFacade.getCurrentSalesChannel());
			model.addAttribute("bookingJourney", bcfTravelCheckoutFacade.getBookingJourney());
			final ContentPageModel contentPage = getContentPageForLabelOrId(REFUND_OPTION_CMS_PAGE_LABEL);
			storeCmsPageInModel(model, contentPage);

			return BcfstorefrontaddonControllerConstants.Views.Pages.Refund.multiRefundOptionPage;
		}
		final String referer = request.getHeader("referer");
		if (referer != null)
		{
			return REDIRECT_PREFIX + referer;
		}
		else
		{
			return REDIRECT_PREFIX + "/";
		}
	}


	@RequestMapping(value = "/goodwill-refund-selection/{orderCode}", method = RequestMethod.GET)
	@RequireHardLogIn
	public String goodWillrefundSelection(final Model model, @PathVariable final String orderCode, final RedirectAttributes redirectModel) throws CMSItemNotFoundException
	{

		final   List<RefundTransactionInfoData> refundTransactionInfos = bcfTravelCartFacadeHelper
				.getGoodWillRefundTransactionInfo(orderCode);

		final List<GoodWillRefundData> goodWillRefundDatas = bcfGoodWillRefundFacade.getAllRefundReasonCodes();

		if(CollectionUtils.isEmpty(goodWillRefundDatas)){

			GlobalMessages.addFlashMessage(redirectModel, GlobalMessages.ERROR_MESSAGES_HOLDER,
					GOOD_WILL_REFUND_EMPTY_REFUND_CODE);
			return REDIRECT_PREFIX + BOOKING_DETAILS_PAGE + orderCode;
		}

		model.addAttribute(REFUND_REASON_CODE_DATA, goodWillRefundDatas);

		final AbstractOrderModel abstractOrderModel = bcfBookingService.getOrderModelFromStore(orderCode);

		model.addAttribute(ADD_REFUND_URL, "/manage-booking/refund/add-goodwill-refund");

		model.addAttribute(CANCEL_URL, BOOKING_DETAILS_PAGE + orderCode);

		final RefundOptionForm refundOptionForm = populateGoodWillRefundSelectionForm(refundTransactionInfos);
		refundOptionForm.setBookingReference(orderCode);
		model.addAttribute(REFUND_OPTION_FORM, refundOptionForm);
		model.addAttribute(VACATION_POLICY_TERMS_AND_CONDITIIONS,
				bcfTravelCartFacadeHelper.getVacationPolicyTermsAndConditions(orderCode));
		model.addAttribute(NON_REFUNDABLE_COST_TERMS_CONDTIONS,
				bcfTravelCartFacadeHelper.getBCFNonRefundableCostTermsAndConditions(orderCode));
		model.addAttribute(BcfFacadesConstants.SALES_CHANNEL, bcfSalesApplicationResolverFacade.getCurrentSalesChannel());

		final ContentPageModel contentPage = getContentPageForLabelOrId(REFUND_OPTION_CMS_PAGE_LABEL);
		storeCmsPageInModel(model, contentPage);

		return BcfstorefrontaddonControllerConstants.Views.Pages.Refund.multiRefundOptionPage;
	}


	private RefundOptionForm populateRefundSelectionForm(final AbstractOrderModel abstractOrderModel,
			final Pair<Double, List<RefundTransactionInfoData>> refundTransactionInfosPair)
	{
		final RefundOptionForm refundOptionForm = new RefundOptionForm();
		if (refundTransactionInfosPair != null)
		{
			final List<RefundTransactionInfoData> refundTransactionInfos = refundTransactionInfosPair.getRight();
			if (CollectionUtils.isNotEmpty(refundTransactionInfos))
			{
				final List<RefundSelectionForm> refundSelectionForms = new ArrayList<>();
				for (final RefundTransactionInfoData refundTransactionInfoData : refundTransactionInfos)
				{
					final RefundSelectionForm refundSelectionForm = new RefundSelectionForm();
					refundSelectionForm.setMaxAmount(refundTransactionInfoData.getMaxFormattedAmount());
					refundSelectionForm.setTransactionCode(refundTransactionInfoData.getCode());
					refundSelectionForm.setRefundType(refundTransactionInfoData.getRefundType());
					refundSelectionForms.add(refundSelectionForm);
				}



				refundOptionForm.setRefundSelectionForms(refundSelectionForms);
				refundOptionForm.setMaxRefundAmount(
						notificationUtils.formatPrice(refundTransactionInfosPair.getLeft(), abstractOrderModel.getCurrency()));
			}
		}

		return refundOptionForm;
	}

	private RefundOptionForm populateGoodWillRefundSelectionForm(
			final  List<RefundTransactionInfoData> refundTransactionInfos)
	{
		final RefundOptionForm refundOptionForm = new RefundOptionForm();

			if (CollectionUtils.isNotEmpty(refundTransactionInfos))
			{
				final List<RefundSelectionForm> refundSelectionForms = new ArrayList<>();
				for (final RefundTransactionInfoData refundTransactionInfoData : refundTransactionInfos)
				{
					final RefundSelectionForm refundSelectionForm = new RefundSelectionForm();
					refundSelectionForm.setTransactionCode(refundTransactionInfoData.getCode());
					refundSelectionForm.setRefundType(refundTransactionInfoData.getRefundType());
					refundSelectionForm.setMaxAmount(refundTransactionInfoData.getMaxFormattedAmount());

					refundSelectionForms.add(refundSelectionForm);
				}

				refundOptionForm.setRefundSelectionForms(refundSelectionForms);
		   }

		return refundOptionForm;
	}


	@RequestMapping(value = "/refund-selection/{orderCode}", method = RequestMethod.GET)
	@RequireHardLogIn
	public String refundSelection(final Model model, @PathVariable final String orderCode) throws CMSItemNotFoundException
	{

		final Pair<Double, List<RefundTransactionInfoData>> refundTransactionInfosPair = bcfTravelCartFacadeHelper
				.getRefundTransactionInfo(orderCode);

		final AbstractOrderModel abstractOrderModel = bcfBookingService.getOrderModelFromStore(orderCode);

		model.addAttribute(ADD_REFUND_URL, "/manage-booking/refund/add-refund");
		model.addAttribute(CANCEL_URL, BOOKING_DETAILS_PAGE + orderCode);

		final RefundOptionForm refundOptionForm = populateRefundSelectionForm(abstractOrderModel, refundTransactionInfosPair);
		refundOptionForm.setBookingReference(orderCode);
		model.addAttribute(REFUND_OPTION_FORM, refundOptionForm);
		model.addAttribute(VACATION_POLICY_TERMS_AND_CONDITIIONS,
				bcfTravelCartFacadeHelper.getVacationPolicyTermsAndConditions(orderCode));
		model.addAttribute(NON_REFUNDABLE_COST_TERMS_CONDTIONS,
				bcfTravelCartFacadeHelper.getBCFNonRefundableCostTermsAndConditions(orderCode));
		model.addAttribute(BcfFacadesConstants.SALES_CHANNEL, bcfSalesApplicationResolverFacade.getCurrentSalesChannel());

		addCancelFeeToModel(model, abstractOrderModel);

		final ContentPageModel contentPage = getContentPageForLabelOrId(REFUND_OPTION_CMS_PAGE_LABEL);
		storeCmsPageInModel(model, contentPage);

		return BcfstorefrontaddonControllerConstants.Views.Pages.Refund.multiRefundOptionPage;
	}

	private void addCancelFeeToModel(final Model model, final AbstractOrderModel abstractOrderModel)
	{
		final BigDecimal totalPaid = bcfBookingService.calculateTotalToRefund((OrderModel)abstractOrderModel);
		final BcfChangeCancellationVacationFeeData bcfChangeCancellationVacationFeeData = packageBookingCancellationStrategy
				.adjustCancellationFee(totalPaid, cancellationFeeStrategy.calculateCancellationFee(abstractOrderModel));
		final BigDecimal totalCancelFee = packageBookingCancellationStrategy
				.getTotalCancellationFee(bcfChangeCancellationVacationFeeData);

		packageBookingCancellationStrategy.getTotalCancellationFeePriceData(totalCancelFee,abstractOrderModel.getCurrency().getIsocode());

		model.addAttribute(CANCEL_FEE, packageBookingCancellationStrategy.getTotalCancellationFeePriceData(totalCancelFee,abstractOrderModel.getCurrency().getIsocode()).getFormattedValue());
	}


	private void updateRefundTransactionInfoDatas(final List<RefundTransactionInfoData> refundTransactionInfoDatas,
			final RefundOptionForm refundOptionForm)
	{

		for (final RefundSelectionForm refundSelectionForm : refundOptionForm.getRefundSelectionForms())
		{
			if (RefundOptionValidator.isDouble(refundSelectionForm.getAmount()))
			{
				final RefundTransactionInfoData refundTransactionInfoData = new RefundTransactionInfoData();
				refundTransactionInfoData.setAmount(Double.valueOf(refundSelectionForm.getAmount()));
				refundTransactionInfoData.setCode(refundSelectionForm.getTransactionCode());
				refundTransactionInfoData.setRefundReason(refundSelectionForm.getRefundReason());
				refundTransactionInfoDatas.add(refundTransactionInfoData);
			}

		}
	}

	private void updateModel(final Model model, final RefundOptionForm refundOptionForm,
			final HttpServletRequest request, final BindingResult bindingResult) throws CMSItemNotFoundException
	{
		final ContentPageModel contentPage = getContentPageForLabelOrId(REFUND_OPTION_CMS_PAGE_LABEL);
		storeCmsPageInModel(model, contentPage);
		model.addAttribute("org.springframework.validation.BindingResult.refundOptionForm", bindingResult);
		request.setAttribute("org.springframework.validation.BindingResult.refundOptionForm", bindingResult);
		model.addAttribute(REFUND_OPTION_FORM, refundOptionForm);
		final String bookingReference = refundOptionForm.getBookingReference();

		if (StringUtils.isEmpty(bookingReference))
		{
			model.addAttribute(ADD_REFUND_URL, "/checkout/refund/add-refund");
			if (bcfTravelCartFacadeHelper.isAlacateFlow())
			{
				model.addAttribute(CANCEL_URL, "/alacarte-review");
			}
			else
			{
				model.addAttribute(CANCEL_URL, "/cart/summary");
			}
			final AbstractOrderModel abstractOrderModel = bcfTravelCartService.getSessionCart();
			if (BookingJourneyType.BOOKING_PACKAGE.getCode()
					.equals(sessionService.getAttribute(BcfCoreConstants.SESSION_BOOKING_JOURNEY)) || BookingJourneyType.BOOKING_ALACARTE.getCode()
					.equals(sessionService.getAttribute(BcfCoreConstants.SESSION_BOOKING_JOURNEY)))
			{

				model.addAttribute(BcfFacadesConstants.ROOM_GUEST_DATAS,refundOptionForm.getRoomGuestData());
			}
			else if (BookingJourneyType.BOOKING_ACTIVITY_ONLY.getCode()
					.equals(getSessionService().getAttribute(BcfCoreConstants.SESSION_BOOKING_JOURNEY)))
			{
				model.addAttribute(BcfFacadesConstants.ACTIVITY_DATAS,refundOptionForm.getActivityLeadPaxInfo());


			}
			model.addAttribute(VACATION_POLICY_TERMS_AND_CONDITIIONS,
					bcfTravelCartFacadeHelper.getVacationPolicyTermsAndConditions(null));
			model.addAttribute(NON_REFUNDABLE_COST_TERMS_CONDTIONS,
					bcfTravelCartFacadeHelper.getBCFNonRefundableCostTermsAndConditions(null));

		}

		else
		{
			model.addAttribute(ADD_REFUND_URL, "/manage-booking/refund/add-refund");
			model.addAttribute(CANCEL_URL, BOOKING_DETAILS_PAGE + bookingReference);
			addCancelFeeToModel(model,	  bcfBookingService.getOrderModelFromStore(bookingReference));
			model.addAttribute(VACATION_POLICY_TERMS_AND_CONDITIIONS,
					bcfTravelCartFacadeHelper.getVacationPolicyTermsAndConditions(bookingReference));
			model.addAttribute(NON_REFUNDABLE_COST_TERMS_CONDTIONS,
					bcfTravelCartFacadeHelper.getBCFNonRefundableCostTermsAndConditions(bookingReference));
		}

		model.addAttribute(BcfFacadesConstants.SALES_CHANNEL, bcfSalesApplicationResolverFacade.getCurrentSalesChannel());
		model.addAttribute("bookingJourney", bcfTravelCheckoutFacade.getBookingJourney());



	}

	protected String redirectToNextPageURL(final PlaceOrderResponseData decisionData, final RedirectAttributes redirectModel)
	{
		if (Objects.nonNull(decisionData) && org.apache.commons.lang.StringUtils.isEmpty(decisionData.getError()))

		{
			GlobalMessages.addFlashMessage(redirectModel, GlobalMessages.INFO_MESSAGES_HOLDER, REFUND_INITIATED);
			return redirectToOrderConfirmationPage(decisionData.getOrderData());
		}
		LOG.error("Error encountered while performing refund and placing order");
		GlobalMessages.addFlashMessage(redirectModel, GlobalMessages.ERROR_MESSAGES_HOLDER, ERROR_PLACE_ORDER);
		return REDIRECT_PREFIX + bcfControllerUtil.getNextPageURL();
	}

	protected BcfTravelCartFacade getBcfTravelCartFacade()
	{
		return bcfTravelCartFacade;
	}



}

