/*
 * Copyright (c) 2019 British Columbia Ferry Services Inc
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of British Columbia Ferry Services Inc("Confidential Information").
 * You shall not disclose such Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with British Columbia Ferry Services Inc.
 *
 * Last Modified: 02/07/19 12:36
 */

package com.bcf.bcfstorefrontaddon.controllers.cms;

import de.hybris.platform.acceleratorstorefrontcommons.controllers.ThirdPartyConstants;
import de.hybris.platform.addonsupport.controllers.cms.AbstractCMSAddOnComponentController;
import de.hybris.platform.commercefacades.accommodation.search.RoomStayCandidateData;
import de.hybris.platform.commercefacades.travel.CabinClassData;
import de.hybris.platform.commercefacades.travel.PassengerTypeData;
import de.hybris.platform.commercefacades.travel.PassengerTypeQuantityData;
import de.hybris.platform.servicelayer.session.SessionService;
import de.hybris.platform.travelfacades.constants.TravelfacadesConstants;
import de.hybris.platform.travelfacades.facades.CabinClassFacade;
import de.hybris.platform.travelfacades.strategies.TravellerSortStrategy;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import com.bcf.bcfstorefrontaddon.constants.BcfstorefrontaddonWebConstants;
import com.bcf.bcfstorefrontaddon.model.components.AbstractFinderComponentModel;
import com.bcf.core.bcfproperties.BcfConfigurablePropertiesService;
import com.bcf.facades.bcffacades.BCFPassengerTypeFacade;
import com.bcf.facades.constants.BcfFacadesConstants;


public abstract class AbstractFinderComponentController
		extends SubstitutingCMSAddOnComponentController<AbstractFinderComponentModel>
{

	private static final Logger LOG = Logger.getLogger(AbstractFinderComponentController.class);

	protected static final String HIDE_FINDER_TITLE = "hideFinderTitle";

	private static final String GUEST_QUANTITY = "guestQuantity";

	private static final String ACCOMMODATION_QUANTITY = "accommodationsQuantity";

	private static final String CABIN_CLASSES = "cabinClasses";

	protected static final String SHOW_COMPONENT = "showComponent";

	@Resource(name = "passengerTypeFacade")
	private BCFPassengerTypeFacade passengerTypeFacade;

	@Resource(name = "travellerSortStrategy")
	private TravellerSortStrategy travellerSortStrategy;

	@Resource(name = "cabinClassFacade")
	private CabinClassFacade cabinClassFacade;

	@Resource(name = "sessionService")
	private SessionService sessionService;

	@Resource(name = "bcfConfigurablePropertiesService")
	private BcfConfigurablePropertiesService bcfConfigurablePropertiesService;

	protected RoomStayCandidateData createRoomStayCandidatesData()
	{
		final RoomStayCandidateData roomStayCandidateData = new RoomStayCandidateData();
		roomStayCandidateData.setPassengerTypeQuantityList(getPassengerTypeQuantityList());
		for (final PassengerTypeQuantityData passengeTypeQuantityData : roomStayCandidateData.getPassengerTypeQuantityList())
		{
			if (passengeTypeQuantityData.getPassengerType().getCode().equals(TravelfacadesConstants.PASSENGER_TYPE_CODE_ADULT))
			{
				passengeTypeQuantityData.setQuantity(TravelfacadesConstants.DEFAULT_ADULT_QUANTITY);
			}
		}
		return roomStayCandidateData;
	}

	protected List<PassengerTypeQuantityData> getPassengerTypeQuantityList()
	{
		final List<PassengerTypeQuantityData> passengerTypeQuantityList = new ArrayList<>();
		final List<PassengerTypeData> sortedPassengerTypes = getTravellerSortStrategy()
				.sortPassengerTypes(getPassengerTypeFacade().getPassengerTypes());
		for (final PassengerTypeData passengerTypeData : sortedPassengerTypes)
		{
			final PassengerTypeQuantityData passengerTypeQuantityData = new PassengerTypeQuantityData();
			passengerTypeQuantityData.setPassengerType(passengerTypeData);
			passengerTypeQuantityData.setQuantity(TravelfacadesConstants.DEFAULT_GUEST_QUANTITY);
			passengerTypeQuantityList.add(passengerTypeQuantityData);
		}
		return passengerTypeQuantityList;
	}

	@ModelAttribute(GUEST_QUANTITY)
	public List<String> populatePassengersQuantity()
	{
		final List<String> guestsQuantity = new ArrayList<>();
		final String maxPersonalAllowed = bcfConfigurablePropertiesService
				.getBcfPropertyValue(BcfFacadesConstants.MAX_PERSONS_ALLOWED);
		final int maxGuestQuantity = StringUtils.isNotEmpty(maxPersonalAllowed) ? Integer.parseInt(maxPersonalAllowed) : 9;
		for (int i = 0; i <= maxGuestQuantity; i++)
		{
			guestsQuantity.add(String.valueOf(i));
		}
		return guestsQuantity;
	}

	@ModelAttribute(ACCOMMODATION_QUANTITY)
	public List<String> populateAccommodationsQuantity()
	{
		final List<String> accommodationQuantity = new ArrayList<>();
		final int maxAccommodationsAllowed = getConfigurationService().getConfiguration()
				.getInt(TravelfacadesConstants.MAX_ACCOMMODATION_QUANTITY);
		for (int i = 1; i <= maxAccommodationsAllowed; i++)
		{
			accommodationQuantity.add(i + " ");

		}
		return accommodationQuantity;
	}

	/**
	 * Method responsible for retrieving and setting a list of cabin classes onto the model
	 */
	@ModelAttribute(CABIN_CLASSES)
	public List<CabinClassData> setCabinClassesOnModel()
	{
		return getCabinClassFacade().getCabinClasses();
	}

	/**
	 * Method responsible to set a flag which will decide if a component is to be collapsed or expanded on the selection
	 * page
	 */
	@ModelAttribute(SHOW_COMPONENT)
	public boolean setCollapseExpandFlag()
	{
		return getSessionService().getAttribute(BcfstorefrontaddonWebConstants.SESSION_FARE_SELECTION_DATA) == null
				&& getSessionService()
				.getAttribute(BcfstorefrontaddonWebConstants.ACCOMMODATION_SEARCH_RESPONSE_PROPERTIES) == null
				&& getSessionService()
				.getAttribute(BcfstorefrontaddonWebConstants.PACKAGE_SEARCH_RESPONSE_PROPERTIES) == null;
	}

	/**
	 * method responsible for reloading the component
	 *
	 * @param componentUid
	 * @param request
	 * @param response
	 * @param model
	 * @return
	 */
	@RequestMapping(value = "/load", method = RequestMethod.GET)
	protected String getComponent(@RequestParam final String componentUid, final HttpServletRequest request,
			final HttpServletResponse response, final Model model)
	{
		request.setAttribute(AbstractCMSAddOnComponentController.COMPONENT_UID, componentUid);
		request.setAttribute(HIDE_FINDER_TITLE, true);
		request.setAttribute(BcfstorefrontaddonWebConstants.ACCOMMODATION_FINDER_FORM,
				getSessionService().getAttribute(BcfstorefrontaddonWebConstants.ACCOMMODATION_FINDER_FORM));
		request.setAttribute(BcfstorefrontaddonWebConstants.SESSION_FARE_FINDER_FORM,
				getSessionService().getAttribute(BcfstorefrontaddonWebConstants.SESSION_FARE_FINDER_FORM));
		try
		{
			return handleGet(request, response, model);
		}
		catch (final Exception e)
		{
			LOG.error("Exception loading the component" + e);
		}
		return StringUtils.EMPTY;
	}

	protected void setUpMetaDataRobotsForContentPage(final Model model, final Boolean transactional)
	{
		if (Objects.nonNull(transactional) && !transactional)
		{
			model.addAttribute(ThirdPartyConstants.SeoRobots.META_ROBOTS, ThirdPartyConstants.SeoRobots.NOINDEX_NOFOLLOW);
		}
		else
		{
			model.addAttribute(ThirdPartyConstants.SeoRobots.META_ROBOTS, ThirdPartyConstants.SeoRobots.INDEX_FOLLOW);
		}

	}

	/**
	 * @return the sessionService
	 */
	protected SessionService getSessionService()
	{
		return sessionService;
	}

	/**
	 * @return the passengerTypeFacade
	 */
	public BCFPassengerTypeFacade getPassengerTypeFacade()
	{
		return passengerTypeFacade;
	}

	/**
	 * @return the travellerSortStrategy
	 */
	protected TravellerSortStrategy getTravellerSortStrategy()
	{
		return travellerSortStrategy;
	}

	/**
	 * @return the cabinClassFacade
	 */
	protected CabinClassFacade getCabinClassFacade()
	{
		return cabinClassFacade;
	}
}
