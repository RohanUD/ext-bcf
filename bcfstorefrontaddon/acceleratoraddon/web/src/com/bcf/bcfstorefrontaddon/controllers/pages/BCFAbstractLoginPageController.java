/*
 * Copyright (c) 2019 British Columbia Ferry Services Inc
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of British Columbia Ferry Services Inc("Confidential Information").
 * You shall not disclose such Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with British Columbia Ferry Services Inc.
 *
 * Last Modified: 02/07/19 14:01
 */

package com.bcf.bcfstorefrontaddon.controllers.pages;

import static com.bcf.bcfstorefrontaddon.constants.BcfstorefrontaddonWebConstants.COMMA;

import de.hybris.platform.acceleratorstorefrontcommons.controllers.ThirdPartyConstants;
import de.hybris.platform.acceleratorstorefrontcommons.controllers.pages.AbstractLoginPageController;
import de.hybris.platform.acceleratorstorefrontcommons.controllers.util.GlobalMessages;
import de.hybris.platform.acceleratorstorefrontcommons.forms.GuestForm;
import de.hybris.platform.acceleratorstorefrontcommons.forms.LoginForm;
import de.hybris.platform.cms2.exceptions.CMSItemNotFoundException;
import de.hybris.platform.cms2.model.pages.AbstractPageModel;
import de.hybris.platform.cms2.model.pages.ContentPageModel;
import de.hybris.platform.cmsfacades.data.UserGroupData;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;
import javax.annotation.Resource;
import javax.servlet.http.HttpSession;
import org.apache.commons.lang3.StringUtils;
import org.springframework.ui.Model;
import com.bcf.bcfstorefrontaddon.forms.BCFSignupForm;
import com.bcf.bcfstorefrontaddon.util.BcfPageUtil;
import com.bcf.core.bcfproperties.BcfConfigurablePropertiesService;
import com.bcf.facades.BcfUserGroupFacade;


public abstract class BCFAbstractLoginPageController extends AbstractLoginPageController
{
	protected static final String USER_GROUPS = "userGroups";
	protected static final String LOCALE = "locale";

	@Resource(name = "bcfUserGroupFacade")
	private BcfUserGroupFacade bcfUserGroupFacade;

	@Resource(name = "bcfConfigurablePropertiesService")
	private BcfConfigurablePropertiesService bcfConfigurablePropertiesService;

	@Resource(name = "bcfPageUtil")
	private BcfPageUtil bcfPageUtil;

	@Override
	protected String getDefaultLoginPage(final boolean loginError, final HttpSession session, final Model model)
			throws CMSItemNotFoundException
	{
		final LoginForm loginForm = new LoginForm();
		final BCFSignupForm bcfSignupForm = new BCFSignupForm();

		addUserGroupsInContext(model);
		model.addAttribute(loginForm);
		model.addAttribute("BCFSignupForm", bcfSignupForm);
		model.addAttribute(new GuestForm());

		final String username = (String) session.getAttribute(SPRING_SECURITY_LAST_USERNAME);
		if (username != null)
		{
			session.removeAttribute(SPRING_SECURITY_LAST_USERNAME);
		}

		loginForm.setJ_username(username);
		storeCmsPageInModel(model, getCmsPage());
		setUpMetaDataForContentPage(model, (ContentPageModel) getCmsPage());
		model.addAttribute(ThirdPartyConstants.SeoRobots.META_ROBOTS, ThirdPartyConstants.SeoRobots.INDEX_NOFOLLOW);

		if (loginError)
		{
			model.addAttribute("loginError", Boolean.valueOf(loginError));
			GlobalMessages.addErrorMessage(model, "login.error.account.not.found.title");
		}

		return getView();
	}

	protected void addUserGroupsInContext(final Model model) throws CMSItemNotFoundException
	{
		final String propertyValue = bcfConfigurablePropertiesService.getBcfPropertyValue(USER_GROUPS);
		if (StringUtils.isNotEmpty(propertyValue))
		{
			final List<String> userGroups = Stream.of(propertyValue.split(COMMA)).collect(Collectors.toList());
			final List<UserGroupData> userGroupDataList = new ArrayList<>();
			userGroupDataList.addAll(bcfUserGroupFacade.getUserListForIds(userGroups));
			model.addAttribute(USER_GROUPS, userGroupDataList);
			model.addAttribute(LOCALE, getI18nService().getCurrentLocale().getLanguage());
		}
	}

	@Override
	protected void storeCmsPageInModel(final Model model, final AbstractPageModel cmsPage)
	{
		bcfPageUtil.storeCmsPageInModel(model, cmsPage);
	}

	@Override
	protected void setUpMetaData(final Model model, final String metaKeywords, final String metaDescription)
	{
		bcfPageUtil.setUpMetaData(model, metaKeywords, metaDescription);
	}

	protected void setUpMetaDataForContentPage(final Model model, final ContentPageModel contentPage,
			final String... arguments)
	{
		bcfPageUtil.setUpMetaDataForContentPage(model, contentPage, arguments);
	}

	protected void setUpMetaDataRobotsForContentPage(final Model model, final Boolean indexable, final Boolean metaFollow)
	{
		bcfPageUtil.setUpMetaDataRobotsForContentPage(model, indexable, metaFollow);
	}
}
