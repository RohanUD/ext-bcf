/*
 * Copyright (c) 2019 British Columbia Ferry Services Inc
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of British Columbia Ferry Services Inc("Confidential Information").
 * You shall not disclose such Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with British Columbia Ferry Services Inc.
 *
 * Last Modified: 06/06/19 20:21
 */

package com.bcf.bcfstorefrontaddon.controllers.pages;

import de.hybris.platform.acceleratorstorefrontcommons.controllers.pages.AbstractSearchPageController;
import de.hybris.platform.cms2.model.pages.AbstractPageModel;
import de.hybris.platform.cms2.model.pages.ContentPageModel;
import javax.annotation.Resource;
import org.springframework.ui.Model;
import com.bcf.bcfstorefrontaddon.util.BcfPageUtil;


public class BcfAbstractSearchPageController extends AbstractSearchPageController
{

	@Resource(name = "bcfPageUtil")
	private BcfPageUtil bcfPageUtil;

	@Override
	protected void storeCmsPageInModel(final Model model, final AbstractPageModel cmsPage)
	{
		bcfPageUtil.storeCmsPageInModel(model, cmsPage);
	}

	@Override
	protected void setUpMetaData(final Model model, final String metaKeywords, final String metaDescription)
	{
		bcfPageUtil.setUpMetaData(model, metaKeywords, metaDescription);
	}

	protected void setUpMetaDataForContentPage(final Model model, final ContentPageModel contentPage,
			final String... arguments)
	{
		bcfPageUtil.setUpMetaDataForContentPage(model, contentPage, arguments);
	}

	protected void setUpMetaDataRobotsForContentPage(final Model model, final Boolean indexable, final Boolean metaFollow)
	{
		bcfPageUtil.setUpMetaDataRobotsForContentPage(model, indexable, metaFollow);
	}

}
