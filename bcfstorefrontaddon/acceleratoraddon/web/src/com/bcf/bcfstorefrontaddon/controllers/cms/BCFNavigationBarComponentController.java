/*
 * Copyright (c) 2019 British Columbia Ferry Services Inc
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of British Columbia Ferry Services Inc("Confidential Information").
 * You shall not disclose such Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with British Columbia Ferry Services Inc.
 *
 * Last Modified: 06/06/19 20:21
 */

package com.bcf.bcfstorefrontaddon.controllers.cms;

import java.util.Map;
import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import com.bcf.bcfstorefrontaddon.constants.BcfstorefrontaddonControllerConstants;
import com.bcf.bcfstorefrontaddon.model.components.BCFNavigationBarComponentModel;


@Controller("BCFNavigationBarComponentController")
@RequestMapping(value = BcfstorefrontaddonControllerConstants.Actions.Cms.BCFNavigationBarComponent)
public class BCFNavigationBarComponentController
		extends SubstitutingCMSAddOnComponentController<BCFNavigationBarComponentModel>
{
	@Resource(name = "navigationContentViewMap")
	private Map<String, String> navigationContentViewMap;


	@Override
	protected void fillModel(final HttpServletRequest request, final Model model,
			final BCFNavigationBarComponentModel component)
	{
		model.addAttribute("title", component.getTitle());
		model.addAttribute("links", component.getLinks());
	}

	@Override
	protected String getView(final BCFNavigationBarComponentModel component)
	{
		return navigationContentViewMap.get(component.getContentViewType().getCode());
	}
}
