/*
 * Copyright (c) 2019 British Columbia Ferry Services Inc
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of British Columbia Ferry Services Inc("Confidential Information").
 * You shall not disclose such Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with British Columbia Ferry Services Inc.
 *
 * Last Modified: 02/07/19 14:01
 */

package com.bcf.bcfstorefrontaddon.controllers.pages;

import static com.bcf.bcfstorefrontaddon.constants.BcfstorefrontaddonWebConstants.HYPHEN;

import de.hybris.platform.acceleratorstorefrontcommons.annotations.RequireHardLogIn;
import de.hybris.platform.acceleratorstorefrontcommons.breadcrumb.ResourceBreadcrumbBuilder;
import de.hybris.platform.acceleratorstorefrontcommons.controllers.ThirdPartyConstants;
import de.hybris.platform.acceleratorstorefrontcommons.controllers.util.GlobalMessages;
import de.hybris.platform.acceleratorstorefrontcommons.security.GUIDCookieStrategy;
import de.hybris.platform.cms2.exceptions.CMSItemNotFoundException;
import de.hybris.platform.commercefacades.customer.CustomerFacade;
import de.hybris.platform.commerceservices.customer.DuplicateUidException;
import de.hybris.platform.core.model.order.AbstractOrderEntryModel;
import de.hybris.platform.core.model.order.OrderModel;
import de.hybris.platform.servicelayer.i18n.I18NService;
import de.hybris.platform.servicelayer.session.SessionService;
import de.hybris.platform.travelfacades.facades.ActionFacade;
import de.hybris.platform.travelfacades.facades.customer.TravelCustomerFacade;
import java.util.List;
import java.util.Objects;
import java.util.Optional;
import java.util.stream.Collectors;
import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.context.MessageSource;
import org.springframework.context.NoSuchMessageException;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;
import com.bcf.bcfstorefrontaddon.constants.BcfstorefrontaddonWebConstants;
import com.bcf.bcfstorefrontaddon.forms.ManageMyBookingForm;
import com.bcf.core.service.BcfBookingService;
import com.bcf.core.util.StreamUtil;
import com.bcf.facades.bcffacades.BcfBookingFacade;
import com.bcf.facades.constants.BcfFacadesConstants;
import com.bcf.facades.order.BcfOrderFacade;


/**
 * Controller class to handle Booking details page. This page can be accessed from MyProfilePage or BookingConfirmation
 * page in which case the access url is "/manage-booking/**" and also from ManageMyBooking component, in which case the
 * url is "/checkout/manage-booking/**"
 */
@Controller
@RequestMapping("/manage-booking")
public class BookingDetailsPageController extends BcfAbstractPageController
{
	private static final Logger LOG = Logger.getLogger(BookingDetailsPageController.class);

	private static final String BOOKING_DETAILS_CMS_PAGE = "bookingDetailsPage";
	public static final String BOOKING_DETAILS_USER_VALIDATION_ERROR = "booking.details.user.validation.error";
	public static final String GUEST_BOOKING_DETAILS_NOT_FOUND_ERROR = "guest.booking.details.not.found.error ";
	private static final String IS_EBOOKING_MOCKED = "ebooking.search.bookings.service.url.mock.enable";

	@Resource(name = "bookingFacade")
	private BcfBookingFacade bcfBookingFacade;

	@Resource(name = "sessionService")
	private SessionService sessionService;

	@Resource(name = "guidCookieStrategy")
	private GUIDCookieStrategy guidCookieStrategy;

	@Resource(name = "customerFacade")
	private CustomerFacade customerFacade;

	@Resource(name = "actionFacade")
	private ActionFacade actionFacade;

	@Resource(name = "messageSource")
	private MessageSource messageSource;

	@Resource(name = "i18nService")
	private I18NService i18nService;

	@Resource(name = "travelCustomerFacade")
	private TravelCustomerFacade travelCustomerFacade;

	@Resource(name = "accountBreadcrumbBuilder")
	private ResourceBreadcrumbBuilder accountBreadcrumbBuilder;

	@Resource(name = "bcfBookingService")
	private BcfBookingService bcfBookingService;

	@Resource(name = "orderFacade")
	private BcfOrderFacade bcfOrderFacade;

	/**
	 * This method enables to retrieve a booking using the booking reference number.
	 *
	 * @param bookingReference a string representing bookingReferenceNumber
	 * @param model            a Model Object
	 * @param redirectModel    a RedirectAttributes object
	 * @return a string for a redirected page.
	 * @throws CMSItemNotFoundException
	 */
	@RequestMapping(value = { "/booking-details/{bookingReference}", "/booking-details" }, method = RequestMethod.GET)
	@RequireHardLogIn
	public String getManageBookingsPage(@PathVariable(required = false) String bookingReference, final Model model,
			final RedirectAttributes redirectModel,
			@RequestParam(value = "bcfBookingReferences", required = false) final String bcfBookingReferences,
			@RequestParam(value = "eBookingCode", required = false) final String eBookingCode)
			throws CMSItemNotFoundException
	{
		String bookingReferenceValue = bcfBookingReferences;

		if (StringUtils.isNotBlank(eBookingCode) && getConfigurationService().getConfiguration()
				.getBoolean(BcfFacadesConstants.SEARCH_FROM_EBOOKING))
		{
			final String orderCode = bcfBookingFacade.getOrderCodeForEBookingCode(eBookingCode.split("-")[0]);
			if (StringUtils.isNotBlank(orderCode))
			{
				bookingReference = orderCode;
			}
			bookingReferenceValue = eBookingCode;

		}
		else
		{
			final OrderModel order = bcfBookingService.getOrder(bookingReference);

			final List<AbstractOrderEntryModel> entries = StreamUtil.safeStream(order.getEntries())
					.filter(e -> e.getBookingReference() != null).collect(Collectors.toList());
			if (CollectionUtils.isNotEmpty(entries))
			{
				bookingReferenceValue = entries.stream()
						.map(e -> e.getBookingReference()).distinct()
						.collect(Collectors.joining(HYPHEN));
			}


		}
		return this.manageBooking(bookingReference, bookingReferenceValue, model, redirectModel);
	}

	/**
	 * This method enables to retrieve a booking using the orderCode and VersionId.
	 *
	 * @param orderId       a string representing bookingReferenceNumber
	 * @param versionId     versionId
	 * @param redirectModel a RedirectAttributes object
	 * @return a string for a redirected page.
	 * @throws CMSItemNotFoundException
	 */
	@RequestMapping(value = { "/booking-details/orders/{orderId}" }, method = RequestMethod.GET)
	@RequireHardLogIn
	public String getManageBookingsByVersionPage(@PathVariable(required = true) final String orderId, final Model model,
			final RedirectAttributes redirectModel, @RequestParam(value = "versionId", required = true) final String versionId)
			throws CMSItemNotFoundException
	{
		final OrderModel order = bcfOrderFacade.getOrderByCode(orderId, versionId);
		String bookingReferenceValue = null;

		final List<AbstractOrderEntryModel> entries = StreamUtil.safeStream(order.getEntries())
				.filter(e -> e.getBookingReference() != null).collect(Collectors.toList());
		if (CollectionUtils.isNotEmpty(entries))
		{
			bookingReferenceValue = entries.stream()
					.map(e -> e.getBookingReference()).distinct()
					.collect(Collectors.joining(HYPHEN));
		}
		return this.manageBooking(orderId, bookingReferenceValue, model, redirectModel);
	}

	/**
	 * This method enables to retrieve a guest booking using the guest order identifier. <br>
	 * First, this method tries to find the booking based on GUID, this is the case when Booking is created in Hybris.If found then get booking refernces and send to further method for processing.
	 * <br>
	 * If the booking is not found, this is the case when the booking is done from EBooking - in this case <code> guestBookingIdentifier</code> will contain bookingReference,
	 * the <code> guestBookingIdentifier</code> is copied to bookingReferences and send to further method for processing.
	 *
	 * @param guestBookingIdentifier a string representing guest bookingReferenceNumber
	 * @param model                  a Model Object
	 * @param redirectModel          a RedirectAttributes object
	 * @return a string for a redirected page.
	 * @throws CMSItemNotFoundException
	 */
	@RequestMapping(value = "/guest-booking-details", method = RequestMethod.GET)
	public String getGuestManageBookingsPage(@RequestParam(required = true) final String guestBookingIdentifier,
			final Model model, final RedirectAttributes redirectModel)
			throws CMSItemNotFoundException
	{
		String bookingReferences = null;
		String orderCode = null;

		final OrderModel order = bcfBookingFacade.getGuestOrderForGUID(guestBookingIdentifier);
		if (Objects.nonNull(order))
		{
			orderCode = order.getCode();
			final List<AbstractOrderEntryModel> entries = StreamUtil.safeStream(order.getEntries())
					.filter(e -> e.getBookingReference() != null).collect(Collectors.toList());
			if (CollectionUtils.isNotEmpty(entries))
			{
				bookingReferences = entries.stream()
						.map(e -> e.getBookingReference()).distinct()
						.collect(Collectors.joining(HYPHEN));
			}
		}
		else
		{
			bookingReferences = guestBookingIdentifier;
		}

		return manageBooking(orderCode, bookingReferences, model, redirectModel);
	}

	private String manageBooking(final String bookingReference, final String bcfBookingReferences, final Model model,
			final RedirectAttributes redirectModel) throws CMSItemNotFoundException
	{
		sessionService.setAttribute(BcfstorefrontaddonWebConstants.BOOKING_REFERENCE, bookingReference);
		sessionService.setAttribute(BcfstorefrontaddonWebConstants.BCF_BOOKING_REFERENCES, bcfBookingReferences);
		model.addAttribute(BcfstorefrontaddonWebConstants.BOOKING_REFERENCE, bookingReference);

		storeCmsPageInModel(model, getContentPageForLabelOrId(BOOKING_DETAILS_CMS_PAGE));
		setUpMetaDataForContentPage(model, getContentPageForLabelOrId(BOOKING_DETAILS_CMS_PAGE));
		model.addAttribute(ThirdPartyConstants.SeoRobots.META_ROBOTS, ThirdPartyConstants.SeoRobots.NOINDEX_NOFOLLOW);
		model.addAttribute(BcfFacadesConstants.BREADCRUMBS, accountBreadcrumbBuilder.getBreadcrumbs(null));
		return getViewForPage(model);
	}

	/**
	 * This method allows a traveller to login to bookingDetails page. Validates BookingReference and Last name, if
	 * success full retrieves booking and redirects to bookingDetails page, else redirects to home page with error
	 * message.
	 *
	 * @param mmbForm       ManageMyBookingForm object.
	 * @param request       HttpServletRequest
	 * @param response      HttpServletResponse
	 * @param redirectModel RedirectAttributes
	 * @return a string for a redirected page.
	 */
	@RequestMapping(value = "/login", method = RequestMethod.POST)
	public String loginManageMyBooking(final ManageMyBookingForm mmbForm, final HttpServletRequest request,
			final HttpServletResponse response, final RedirectAttributes redirectModel)
	{
		String emailId = null;
		emailId = bcfBookingFacade.validateAndReturnBookerEmailId(mmbForm.getBookingReference(), mmbForm.getLastName());

		if (emailId == null)
		{
			GlobalMessages.addFlashMessage(redirectModel, GlobalMessages.ERROR_MESSAGES_HOLDER,
					"text.error.manage.booking.invalid.booking.reference.or.last.name");
			return REDIRECT_PREFIX + "/";
		}

		bcfBookingFacade.mapOrderToUserAccount(mmbForm.getBookingReference());

		if (!Optional.ofNullable(emailId).isPresent())
		{
			LOG.error("Invalid Passenger Last name " + mmbForm.getLastName() + " for the booking: " + mmbForm.getBookingReference());
			GlobalMessages.addFlashMessage(redirectModel, GlobalMessages.ERROR_MESSAGES_HOLDER,
					"text.error.manage.booking.invalid.booking.reference.or.last.name");
			return REDIRECT_PREFIX + "/";
		}
		try
		{
			final String guid = travelCustomerFacade.createGuestCustomer(emailId, mmbForm.getLastName());
			getGuidCookieStrategy().setCookie(request, response);
			getSessionService().setAttribute(BcfstorefrontaddonWebConstants.MANAGE_MY_BOOKING_GUEST_UID, guid);
			getSessionService().setAttribute(BcfstorefrontaddonWebConstants.MANAGE_MY_BOOKING_AUTHENTICATION, Boolean.TRUE);
			getSessionService().setAttribute(BcfstorefrontaddonWebConstants.MANAGE_MY_BOOKING_BOOKING_REFERENCE,
					mmbForm.getBookingReference());
		}
		catch (final NoSuchMessageException | DuplicateUidException e)
		{
			LOG.error("Error creating Anonymous customer for Booking Reference number: " + mmbForm.getBookingReference());
			GlobalMessages.addFlashMessage(redirectModel, GlobalMessages.ERROR_MESSAGES_HOLDER,
					"text.error.manage.booking.error.creating.anonymous.user");
			return REDIRECT_PREFIX + "/";
		}
		return REDIRECT_PREFIX + "/manage-booking/booking-details/" + mmbForm.getBookingReference();
	}

	@ModelAttribute("disableCurrencySelector")
	public Boolean getDisableCurrencySelector()
	{
		return Boolean.TRUE;
	}


	@Override
	public SessionService getSessionService()
	{
		return sessionService;
	}

	public void setSessionService(final SessionService sessionService)
	{
		this.sessionService = sessionService;
	}

	protected GUIDCookieStrategy getGuidCookieStrategy()
	{
		return guidCookieStrategy;
	}

	public void setGuidCookieStrategy(final GUIDCookieStrategy guidCookieStrategy)
	{
		this.guidCookieStrategy = guidCookieStrategy;
	}


	@Override
	protected CustomerFacade getCustomerFacade()
	{
		return customerFacade;
	}

	public void setCustomerFacade(final CustomerFacade customerFacade)
	{
		this.customerFacade = customerFacade;
	}


	@Override
	protected MessageSource getMessageSource()
	{
		return messageSource;
	}

	public void setMessageSource(final MessageSource messageSource)
	{
		this.messageSource = messageSource;
	}


	@Override
	protected I18NService getI18nService()
	{
		return i18nService;
	}

	public void setI18nService(final I18NService i18nService)
	{
		this.i18nService = i18nService;
	}

}
