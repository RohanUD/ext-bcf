/*
 * Copyright (c) 2019 British Columbia Ferry Services Inc
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of British Columbia Ferry Services Inc("Confidential Information").
 * You shall not disclose such Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with British Columbia Ferry Services Inc.
 *
 * Last Modified: 02/07/19 14:01
 */

package com.bcf.bcfstorefrontaddon.controllers.pages;


import de.hybris.platform.acceleratorstorefrontcommons.controllers.pages.AbstractPageController;
import de.hybris.platform.cms2.model.pages.AbstractPageModel;
import de.hybris.platform.cms2.model.pages.ContentPageModel;
import de.hybris.platform.cms2.model.site.CMSSiteModel;
import de.hybris.platform.cms2.servicelayer.services.CMSSiteService;
import de.hybris.platform.commercefacades.customer.CustomerFacade;
import de.hybris.platform.commercefacades.storesession.StoreSessionFacade;
import de.hybris.platform.commercefacades.storesession.data.CurrencyData;
import de.hybris.platform.commercefacades.storesession.data.LanguageData;
import de.hybris.platform.commercefacades.user.data.CustomerData;
import de.hybris.platform.commerceservices.search.pagedata.PageableData;
import de.hybris.platform.core.servicelayer.data.PaginationData;
import de.hybris.platform.servicelayer.config.ConfigurationService;
import java.io.Serializable;
import java.util.Collection;
import java.util.List;
import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import com.bcf.bcfstorefrontaddon.util.BcfPageUtil;
import com.bcf.bcfstorefrontaddon.util.GlobalPaginationUtil;


/**
 * Base controller for all travel page controllers. Provides common functionality for all page controllers.
 */
public class BcfAbstractPageController extends AbstractPageController
{
	public static final String CMS_PAGE_TITLE = "pageTitle";
	public static final String START_PAGE_NUMBER = "startPageNumber";
	public static final String END_PAGE_NUMBER = "endPageNumber";

	@Resource(name = "cmsSiteService")
	private CMSSiteService cmsSiteService;

	@Resource(name = "storeSessionFacade")
	private StoreSessionFacade storeSessionFacade;

	@Resource(name = "customerFacade")
	private CustomerFacade customerFacade;

	@Override
	@ModelAttribute("languages")
	public Collection<LanguageData> getLanguages()
	{
		return storeSessionFacade.getAllLanguages();
	}

	@Override
	@ModelAttribute("currencies")
	public Collection<CurrencyData> getCurrencies()
	{
		return storeSessionFacade.getAllCurrencies();
	}

	@Override
	@ModelAttribute("currentLanguage")
	public LanguageData getCurrentLanguage()
	{
		return storeSessionFacade.getCurrentLanguage();
	}

	@Override
	@ModelAttribute("currentCurrency")
	public CurrencyData getCurrentCurrency()
	{
		return storeSessionFacade.getCurrentCurrency();
	}

	@Resource(name = "bcfPageUtil")
	private BcfPageUtil bcfPageUtil;

	@Resource(name = "globalPaginationUtil")
	private GlobalPaginationUtil globalPaginationUtil;

	@Resource
	private ConfigurationService configurationService;

	@Override
	@ModelAttribute("siteName")
	public String getSiteName()
	{
		final CMSSiteModel site = cmsSiteService.getCurrentSite();
		return site != null ? site.getName() : "";
	}

	@Override
	@ModelAttribute("siteUid")
	public String getSiteUid()
	{
		final CMSSiteModel site = cmsSiteService.getCurrentSite();
		return site != null ? site.getUid() : "";
	}

	@Override
	@ModelAttribute("user")
	public CustomerData getUser()
	{
		return customerFacade.getCurrentCustomer();
	}

	@ModelAttribute("google_api_key")
	public String getGoogleAPIKey()
	{
		return configurationService.getConfiguration().getString("google.api.key");
	}

	@Override
	protected void storeCmsPageInModel(final Model model, final AbstractPageModel cmsPage)
	{
		bcfPageUtil.storeCmsPageInModel(model, cmsPage);
	}

	protected PageableData buildGlobalPaginationData(final HttpServletRequest request, final String cmsPageLabel)
	{
		return globalPaginationUtil.buildPaginationData(request, cmsPageLabel);
	}

	protected void storePaginationResults(final Model model, final Object facetSearchPageData)
	{
		globalPaginationUtil.storePaginationResults(model, facetSearchPageData);
	}

	protected void storePaginationResults(final Model model, final List<? extends Serializable> results,
			final PaginationData paginationMetaData)
	{
		globalPaginationUtil.storePaginationResults(model, results, paginationMetaData);
	}

	protected int getPageSize(final String cmsPageLabel)
	{
		return globalPaginationUtil.getPageSize(cmsPageLabel);
	}

	protected int getCurrentPage(final HttpServletRequest httpServletRequest)
	{
		return globalPaginationUtil.getCurrentPage(httpServletRequest);
	}

	public int getJsonPaginationStartPageNumber(final int totalRecords, final int pageSize, final int page)
	{
		return globalPaginationUtil.getJsonPaginationIndexes(totalRecords, pageSize, page).get(GlobalPaginationUtil.START_PAGE);
	}

	public int getJsonPaginationEndPageNumber(final int totalRecords, final int pageSize, final int page)
	{
		return globalPaginationUtil.getJsonPaginationIndexes(totalRecords, pageSize, page).get(GlobalPaginationUtil.END_PAGE);
	}

	@Override
	protected void setUpMetaData(final Model model, final String metaKeywords, final String metaDescription)
	{
		bcfPageUtil.setUpMetaData(model, metaKeywords, metaDescription);
	}

	protected void setUpMetaDataForContentPage(final Model model, final ContentPageModel contentPage,
			final String... arguments)
	{
		bcfPageUtil.setUpMetaDataForContentPage(model, contentPage, arguments);
	}

	protected void setUpMetaDataRobotsForContentPage(final Model model, final Boolean indexable, final Boolean metaFollow)
	{
		bcfPageUtil.setUpMetaDataRobotsForContentPage(model, indexable, metaFollow);
	}

	protected void setUpDynamicOGGraphData(final Model model, final ContentPageModel contentPage,
			final HttpServletRequest request, final String... arguments)
	{
		bcfPageUtil.setUpDynamicOGGraphData(model, contentPage, request, arguments);
	}
}
