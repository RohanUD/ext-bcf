/*
 * Copyright (c) 2019 British Columbia Ferry Services Inc
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of British Columbia Ferry Services Inc("Confidential Information").
 * You shall not disclose such Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with British Columbia Ferry Services Inc.
 *
 * Last Modified: 06/06/19 20:21
 */

package com.bcf.bcfstorefrontaddon.controllers.pages;


import de.hybris.platform.cms2.exceptions.CMSItemNotFoundException;
import de.hybris.platform.commercefacades.customer.CustomerFacade;
import java.util.Objects;
import javax.annotation.Resource;
import org.apache.log4j.Logger;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;
import com.bcf.facades.droptrailer.SearchMovementsIntegrationFacade;
import com.bcf.integration.droptrailer.trailermovement.data.SearchMovementsRequestData;
import com.bcf.integrations.core.exception.IntegrationException;


@Controller
@RequestMapping("/drop-trailer")
public class DropTrailerIntegrationPageController extends BcfAbstractPageController
{
	private static final String DROP_TRAILER_INTEGRATION_CMS_PAGE = "dropTrailerIntegrationPage";
	private static final String NO_MOVEMENTS_ERROR = "noMovementsError";
	private static final String RESULT = "result";
	private static final Logger LOG = Logger.getLogger(DropTrailerIntegrationPageController.class);



	@Resource
	private SearchMovementsIntegrationFacade searchMovementsIntegrationFacade;

	@Resource
	private CustomerFacade customerFacade;


	@RequestMapping(method = RequestMethod.GET)
	public String getIntegrationsPage(final Model model) throws CMSItemNotFoundException
	{
		storeCmsPageInModel(model, getContentPageForLabelOrId(DROP_TRAILER_INTEGRATION_CMS_PAGE));
		setUpMetaDataForContentPage(model, getContentPageForLabelOrId(DROP_TRAILER_INTEGRATION_CMS_PAGE));
		return getViewForPage(model);
	}

	@RequestMapping(value = "/searchMovements", method = RequestMethod.GET)
	String searchTrailerMovements(final RedirectAttributes redirectAttributes)
	{
		final SearchMovementsRequestData requestData = new SearchMovementsRequestData();
		requestData.setCustomerId(customerFacade.getCurrentCustomer().getCustomerId());
		requestData.setIncludeOldShipments(Boolean.TRUE);
		if (Objects.nonNull(requestData.getCustomerId()))
		{
			try
			{
				redirectAttributes.addFlashAttribute(RESULT, searchMovementsIntegrationFacade.searchMovements(requestData));
			}
			catch (final IntegrationException e)
			{
				LOG.error(e);
			}
			return REDIRECT_PREFIX + "/drop-trailer";
		}
		redirectAttributes.addFlashAttribute(RESULT, NO_MOVEMENTS_ERROR);
		return REDIRECT_PREFIX + "/drop-trailer";
	}

}
