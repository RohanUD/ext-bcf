/*
 * Copyright (c) 2019 British Columbia Ferry Services Inc
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of British Columbia Ferry Services Inc("Confidential Information").
 * You shall not disclose such Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with British Columbia Ferry Services Inc.
 *
 * Last Modified: 02/07/19 14:01
 */

package com.bcf.bcfstorefrontaddon.controllers.cms;

import de.hybris.platform.commerceservices.i18n.CommerceCommonI18NService;
import de.hybris.platform.core.model.media.MediaModel;
import de.hybris.platform.promotionengineservices.model.PromotionSourceRuleModel;
import de.hybris.platform.ruleengineservices.enums.RuleStatus;
import de.hybris.platform.servicelayer.exceptions.ModelNotFoundException;
import de.hybris.platform.servicelayer.search.FlexibleSearchService;
import java.util.Locale;
import java.util.Map;
import java.util.Optional;
import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import org.apache.commons.lang.StringUtils;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import com.bcf.bcfstorefrontaddon.constants.BcfstorefrontaddonControllerConstants;
import com.bcf.bcfstorefrontaddon.model.components.BCFVacationDealComponentModel;
import com.bcf.core.strategies.responsive.BCFResponsiveMediaStrategy;


@Controller("BCFVacationDealComponentController")
@RequestMapping(value = BcfstorefrontaddonControllerConstants.Actions.Cms.BCFVacationsDealComponent)
public class BCFVacationDealComponentController extends SubstitutingCMSAddOnComponentController<BCFVacationDealComponentModel>
{
	@Resource(name = "flexibleSearchService")
	private FlexibleSearchService flexibleSearchService;

	@Resource(name = "commerceCommonI18NService")
	private CommerceCommonI18NService commerceCommonI18NService;

	@Resource(name = "bcfResponsiveMediaStrategy")
	private BCFResponsiveMediaStrategy bcfResponsiveMediaStrategy;

	@Resource(name = "vacationDealContentViewMap")
	private Map<String, String> vacationDealContentViewMap;

	@Override
	protected void fillModel(final HttpServletRequest request, final Model model, final BCFVacationDealComponentModel component)
	{
		final Locale currentLocale = commerceCommonI18NService.getCurrentLocale();
		final Optional<PromotionSourceRuleModel> promotionSourceRuleOptional = getPromotionSourceRule(component);
		if (promotionSourceRuleOptional.isPresent())
		{
			setPromotionFieldsInModel(model, currentLocale, promotionSourceRuleOptional.get());
		}
		else
		{
			setComponentFieldsInModel(model, currentLocale, component);
		}
		model.addAttribute("link", component.getLink());
		model.addAttribute("topText", component.getTopText());
		model.addAttribute("dealNameImage",
				bcfResponsiveMediaStrategy.getResponsiveJson(component.getDealNameImage(currentLocale)));
		if (StringUtils.isNotEmpty(component.getBackgroundColour()))
		{
			model.addAttribute("backgroundColour", component.getBackgroundColour());
		}
	}

	private void setComponentFieldsInModel(final Model model, final Locale currentLocale,
			final BCFVacationDealComponentModel component)
	{
		setPromotionAttributes(model, component.getDealName(currentLocale),
				component.getDealDescription(currentLocale), component.getDealText(currentLocale));
		setMediaAttributes(model, Optional.ofNullable(component.getImage()), currentLocale);
	}

	private void setPromotionFieldsInModel(final Model model, final Locale currentLocale,
			final PromotionSourceRuleModel promotionSourceRule)
	{
		setPromotionAttributes(model, promotionSourceRule.getName(currentLocale), promotionSourceRule.getDescription(currentLocale),
				promotionSourceRule.getPromoText(currentLocale));
		setMediaAttributes(model, Optional.ofNullable(promotionSourceRule.getImage()), currentLocale);
		setVideoAttributes(model, promotionSourceRule.getVideoUrl(currentLocale),
				promotionSourceRule.getOverLayText(currentLocale));
	}

	private Optional<PromotionSourceRuleModel> getPromotionSourceRule(final BCFVacationDealComponentModel component)
	{
		if (StringUtils.isEmpty(component.getPromotionCode()))
		{
			return Optional.empty();
		}
		final PromotionSourceRuleModel promotionSourceRuleModel = new PromotionSourceRuleModel();
		promotionSourceRuleModel.setCode(component.getPromotionCode());
		promotionSourceRuleModel.setStatus(RuleStatus.PUBLISHED);
		try
		{
			return Optional.ofNullable(flexibleSearchService.getModelByExample(promotionSourceRuleModel));
		}
		catch (final ModelNotFoundException e)
		{
			return Optional.empty();
		}
	}

	private void setPromotionAttributes(final Model model, final String promoName, final String promotionDesc,
			final String promoText)
	{
		model.addAttribute("promotionName", promoName);
		model.addAttribute("promotionDesc", promotionDesc);
		model.addAttribute("promotionText", promoText);
	}

	private void setVideoAttributes(final Model model, final String videoUrl, final String overlayText)
	{
		model.addAttribute("videoUrl", videoUrl);
		model.addAttribute("overlayText", overlayText);
	}

	private void setMediaAttributes(final Model model, final Optional<MediaModel> mediaModel, final Locale currentLocale)
	{
		mediaModel.ifPresent(media ->
		{
			model.addAttribute("dataMedia", bcfResponsiveMediaStrategy.getResponsiveJson(media));
			model.addAttribute("location", media.getLocationTag(currentLocale));
			model.addAttribute("caption", media.getCaption(currentLocale));
			model.addAttribute("location", media.getLocationTag(currentLocale));
			model.addAttribute("caption", media.getCaption(currentLocale));
			model.addAttribute("featuredText", media.getFeaturedText(currentLocale));
			model.addAttribute("altText", media.getAltText());
		});
	}

	@Override
	protected String getView(final BCFVacationDealComponentModel component)
	{
		return vacationDealContentViewMap.get(component.getViewType().getCode());
	}
}
