/*
 * Copyright (c) 2019 British Columbia Ferry Services Inc
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of British Columbia Ferry Services Inc("Confidential Information").
 * You shall not disclose such Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with British Columbia Ferry Services Inc.
 *
 * Last Modified: 02/07/19 14:01
 */

package com.bcf.bcfstorefrontaddon.controllers.pages;

import de.hybris.platform.acceleratorstorefrontcommons.annotations.RequireHardLogIn;
import de.hybris.platform.acceleratorstorefrontcommons.breadcrumb.Breadcrumb;
import de.hybris.platform.acceleratorstorefrontcommons.breadcrumb.ResourceBreadcrumbBuilder;
import de.hybris.platform.acceleratorstorefrontcommons.constants.WebConstants;
import de.hybris.platform.acceleratorstorefrontcommons.controllers.ThirdPartyConstants;
import de.hybris.platform.acceleratorstorefrontcommons.controllers.pages.AbstractSearchPageController;
import de.hybris.platform.b2b.model.B2BUnitModel;
import de.hybris.platform.b2bcommercefacades.company.data.B2BUnitData;
import de.hybris.platform.cms2.exceptions.CMSItemNotFoundException;
import de.hybris.platform.cms2.model.pages.ContentPageModel;
import de.hybris.platform.converters.Populator;
import java.util.List;
import javax.annotation.Resource;
import org.apache.log4j.Logger;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;
import com.bcf.bcfstorefrontaddon.breadcrumb.BcfContentPageBreadCrumbBuilder;
import com.bcf.core.customer.service.CustomerUpdateService;
import com.bcf.core.util.BcfB2bUtil;
import com.bcf.integration.crm.service.CRMUpdateCustomerService;
import com.bcf.integration.data.CRMGetCustomerAccountResponseDTO;
import com.bcf.integrations.core.exception.IntegrationException;
import com.bcf.travelb2bfacades.checkout.BcfTravelb2bCheckoutFacade;
import com.bcf.travelb2bfacades.checkout.users.UserB2BUnitData;


/**
 * Controller for home page
 */
@Controller
@RequestMapping("/my-business-account")
public class BusinessAccountPageController extends AbstractSearchPageController
{

	@Resource(name = "businessAccountBreadcrumbBuilder")
	private ResourceBreadcrumbBuilder accountBreadcrumbBuilder;

	@Resource(name = "contentPageBreadcrumbBuilder")
	protected BcfContentPageBreadCrumbBuilder contentPageBreadcrumbBuilder;

	@Resource(name = "crmUpdateCustomerService")
	private CRMUpdateCustomerService crmUpdateCustomerService;

	@Resource(name = "customerUpdateService")
	private CustomerUpdateService customerUpdateService;

	@Resource(name = "b2bUnitPopulator")
	private Populator<B2BUnitModel, B2BUnitData> b2bUnitPopulator;

	@Resource(name = "bcfTravelb2bCheckoutFacade")
	private BcfTravelb2bCheckoutFacade bcfTravelb2bCheckoutFacade;

	@Resource(name = "bcfB2bUtil")
	private BcfB2bUtil bcfB2bUtil;


	private static final String PROFILE_CMS_PAGE = "viewBusinessAccountDetails";
	private static final String ACCOUNT_CMS_PAGE = "MyBusinessAccount";
	protected static final String BOOKINGS_CMS_PAGE = "my-business-bookings";
	private static final String TEXT_ACCOUNT_PROFILE = "text.business.account.profile";
	private static final String MY_BUSINESS_ACCOUNT_URL = "/my-business-account";
	private static final Logger LOG = Logger.getLogger(BusinessAccountPageController.class);

	@RequestMapping(method = RequestMethod.GET)
	@RequireHardLogIn
	public String account(final Model model, final RedirectAttributes redirectModel) throws CMSItemNotFoundException
	{
		final List<UserB2BUnitData> userB2BUnits = bcfTravelb2bCheckoutFacade.getUserB2BUnitsData();
		model.addAttribute("userB2BUnits", userB2BUnits);

		final B2BUnitModel b2BUnitModel = bcfB2bUtil.getB2bUnitInContext();
		model.addAttribute("b2bUnitName", b2BUnitModel.getName());

		final ContentPageModel contentPageModel = getContentPageForLabelOrId(ACCOUNT_CMS_PAGE);
		storeCmsPageInModel(model, contentPageModel);
		setUpMetaDataForContentPage(model, contentPageModel);
		model.addAttribute(WebConstants.BREADCRUMBS_KEY, contentPageBreadcrumbBuilder.getBreadcrumbs(contentPageModel));
		model.addAttribute(ThirdPartyConstants.SeoRobots.META_ROBOTS, ThirdPartyConstants.SeoRobots.NOINDEX_NOFOLLOW);
		return getViewForPage(model);
	}

	@RequestMapping(value = "/profile", method = RequestMethod.GET)
	@RequireHardLogIn
	public String profile(final Model model) throws CMSItemNotFoundException
	{

		final B2BUnitModel b2BUnitModel = bcfB2bUtil.getB2bUnitInContext();
		CRMGetCustomerAccountResponseDTO response = null;
		try
		{
			response = crmUpdateCustomerService.getCustomerAccount(b2BUnitModel);
		}
		catch (final IntegrationException intEx)
		{
			LOG.error(intEx.getMessage(), intEx);
		}
		final B2BUnitData b2BUnitData = new B2BUnitData();

		if (response != null)
		{
			b2bUnitPopulator
					.populate(customerUpdateService.updateCustomerAccount(response.getBCF_Account(), b2BUnitModel), b2BUnitData);
		}
		model.addAttribute("b2BUnitData", b2BUnitData);

		storeCmsPageInModel(model, getContentPageForLabelOrId(PROFILE_CMS_PAGE));
		setUpMetaDataForContentPage(model, getContentPageForLabelOrId(PROFILE_CMS_PAGE));

		final List<Breadcrumb> breadcrumbs = accountBreadcrumbBuilder.getBreadcrumbs(null);
		breadcrumbs.add(new Breadcrumb(MY_BUSINESS_ACCOUNT_URL, b2BUnitModel != null ? b2BUnitModel.getName() : null, null));

		model.addAttribute(WebConstants.BREADCRUMBS_KEY, breadcrumbs);
		model.addAttribute(ThirdPartyConstants.SeoRobots.META_ROBOTS, ThirdPartyConstants.SeoRobots.NOINDEX_NOFOLLOW);
		return getViewForPage(model);
	}

	@RequestMapping(value = "/my-business-bookings", method = RequestMethod.GET)
	@RequireHardLogIn
	public String getMyBookingsPage(final Model model) throws CMSItemNotFoundException
	{
		storeCmsPageInModel(model, getContentPageForLabelOrId(BOOKINGS_CMS_PAGE));
		setUpMetaDataForContentPage(model, getContentPageForLabelOrId(BOOKINGS_CMS_PAGE));

		final B2BUnitModel b2BUnitModel = bcfB2bUtil.getB2bUnitInContext();
		final List<Breadcrumb> breadcrumbs = accountBreadcrumbBuilder.getBreadcrumbs(null);
		breadcrumbs.add(new Breadcrumb(MY_BUSINESS_ACCOUNT_URL, b2BUnitModel != null ? b2BUnitModel.getName() : null, null));
		model.addAttribute(WebConstants.BREADCRUMBS_KEY, breadcrumbs);

		model.addAttribute(ThirdPartyConstants.SeoRobots.META_ROBOTS, ThirdPartyConstants.SeoRobots.NOINDEX_NOFOLLOW);
		return getViewForPage(model);
	}

}
