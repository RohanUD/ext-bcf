/*
 * Copyright (c) 2019 British Columbia Ferry Services Inc
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of British Columbia Ferry Services Inc("Confidential Information").
 * You shall not disclose such Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with British Columbia Ferry Services Inc.
 *
 * Last Modified: 06/06/19 20:21
 */

package com.bcf.bcfstorefrontaddon.controllers.pages;

import de.hybris.platform.acceleratorstorefrontcommons.controllers.util.GlobalMessages;
import de.hybris.platform.cms2.exceptions.CMSItemNotFoundException;
import de.hybris.platform.commercefacades.accommodation.search.RoomStayCandidateData;
import de.hybris.platform.commercefacades.travel.PassengerTypeData;
import de.hybris.platform.commercefacades.travel.PassengerTypeQuantityData;
import de.hybris.platform.travelfacades.constants.TravelfacadesConstants;
import de.hybris.platform.travelfacades.strategies.TravellerSortStrategy;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Objects;
import javax.annotation.Resource;
import org.apache.commons.lang.StringUtils;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;
import com.bcf.bcfstorefrontaddon.constants.BcfstorefrontaddonWebConstants;
import com.bcf.bcfstorefrontaddon.forms.GetAQuoteForm;
import com.bcf.bcfstorefrontaddon.validators.AbstractTravelValidator;
import com.bcf.core.bcfproperties.BcfConfigurablePropertiesService;
import com.bcf.facades.bcffacades.BCFPassengerTypeFacade;
import com.bcf.facades.bcffacades.GetAQuoteFacade;
import com.bcf.facades.constants.BcfFacadesConstants;
import com.bcf.facades.getaquote.data.GetAQuoteData;


/**
 * Get A Quote Page Controller
 */
@Controller
@RequestMapping(value = "/get-quote")
public class GetAQuotePageController extends BcfAbstractPageController
{
	private static final String GET_A_QUOTE_PAGE_ID = "getAQuotePage";
	private static final String GUEST_QUANTITY = "guestQuantity";
	public static final String GET_A_QUOTE_PAGE = "/get-quote";
	private static final int DEFAULT_ACCOMMODATION_QUANTITY = 1;

	@Resource(name = "travellerSortStrategy")
	private TravellerSortStrategy travellerSortStrategy;

	@Resource(name = "passengerTypeFacade")
	private BCFPassengerTypeFacade passengerTypeFacade;

	@Resource(name = "getAQuoteFacade")
	private GetAQuoteFacade getAQuoteFacade;

	@Resource(name = "getAQuoteValidator")
	private AbstractTravelValidator getAQuoteValidator;

	@Resource(name = "bcfConfigurablePropertiesService")
	private BcfConfigurablePropertiesService bcfConfigurablePropertiesService;

	@RequestMapping(method = RequestMethod.GET)
	public String getQuoteData(@RequestParam(value = "name", required = false) final String productName, final Model model) throws CMSItemNotFoundException
	{
		final GetAQuoteForm getAQuoteForm = new GetAQuoteForm();
		model.addAttribute(BcfstorefrontaddonWebConstants.GET_A_QUOTE_FORM, getAQuoteForm);
		model.addAttribute(GUEST_QUANTITY, populatePassengersQuantity());

		model.addAttribute(BcfstorefrontaddonWebConstants.GET_A_QUOTE_FORM, initializeGetAQuoteForm(getAQuoteForm));
		model.addAttribute(BcfstorefrontaddonWebConstants.PRODUCT_NAME,productName);
		storeCmsPageInModel(model, getContentPageForLabelOrId(GET_A_QUOTE_PAGE_ID));
		setUpMetaDataForContentPage(model, getContentPageForLabelOrId(GET_A_QUOTE_PAGE_ID));
		return getViewForPage(model);
	}

	protected GetAQuoteForm initializeGetAQuoteForm(GetAQuoteForm getAQuoteForm)
	{
		if (Objects.isNull(getAQuoteForm))
		{
			getAQuoteForm = new GetAQuoteForm();
		}
		final List<RoomStayCandidateData> roomStayCandidates = new ArrayList<>();
		final int maxAccommodationsQuantity = getConfigurationService().getConfiguration()
				.getInt(TravelfacadesConstants.MAX_ACCOMMODATION_QUANTITY);
		for (int i = 0; i < maxAccommodationsQuantity; i++)
		{
			final RoomStayCandidateData roomStayCandidateData = createRoomStayCandidatesData();
			roomStayCandidateData.setRoomStayCandidateRefNumber(i);
			roomStayCandidates.add(roomStayCandidateData);
		}
		getAQuoteForm.setRoomStayCandidates(roomStayCandidates);
		getAQuoteForm.setNumberOfRooms(DEFAULT_ACCOMMODATION_QUANTITY);
		return getAQuoteForm;
	}

	protected RoomStayCandidateData createRoomStayCandidatesData()
	{
		final RoomStayCandidateData roomStayCandidateData = new RoomStayCandidateData();
		roomStayCandidateData.setPassengerTypeQuantityList(getPassengerTypeQuantityList());
		for (final PassengerTypeQuantityData passengeTypeQuantityData : roomStayCandidateData
				.getPassengerTypeQuantityList())
		{
			if (passengeTypeQuantityData.getPassengerType().getCode()
					.equals(TravelfacadesConstants.PASSENGER_TYPE_CODE_ADULT))
			{
				passengeTypeQuantityData.setQuantity(TravelfacadesConstants.DEFAULT_ADULT_QUANTITY);
			}
		}
		return roomStayCandidateData;
	}

	protected List<PassengerTypeQuantityData> getPassengerTypeQuantityList()
	{

		final List<PassengerTypeQuantityData> passengerTypeQuantityList = new ArrayList<>();
		final List<String> codes = Arrays.asList(BcfstorefrontaddonWebConstants.PASSENGER_TYPE_CODE_ADULT,
				BcfstorefrontaddonWebConstants.PASSENGER_TYPE_CODE_INFANT,
				BcfstorefrontaddonWebConstants.PASSENGER_TYPE_CODE_CHILD);
		final List<PassengerTypeData> sortedPassengerTypes = getTravellerSortStrategy()
				.sortPassengerTypes(getPassengerTypeFacade().getPassengerTypesForCodes(codes));
		for (final PassengerTypeData passengerTypeData : sortedPassengerTypes)
		{
			final PassengerTypeQuantityData passengerTypeQuantityData = new PassengerTypeQuantityData();
			passengerTypeQuantityData.setPassengerType(passengerTypeData);
			passengerTypeQuantityData.setQuantity(TravelfacadesConstants.DEFAULT_GUEST_QUANTITY);
			passengerTypeQuantityList.add(passengerTypeQuantityData);
		}
		return passengerTypeQuantityList;
	}

	@ModelAttribute(GUEST_QUANTITY)
	public List<String> populatePassengersQuantity()
	{
		final List<String> guestsQuantity = new ArrayList<>();
		final String maxPersonalAllowed = bcfConfigurablePropertiesService
				.getBcfPropertyValue(BcfFacadesConstants.MAX_PERSONS_ALLOWED);
		final int maxGuestQuantity = StringUtils.isNotEmpty(maxPersonalAllowed) ?
				Integer.parseInt(maxPersonalAllowed) :
				9;
		for (int i = 0; i <= maxGuestQuantity; i++)
		{
			guestsQuantity.add(String.valueOf(i));
		}
		return guestsQuantity;
	}

	@RequestMapping(value = "/save-quote", method = RequestMethod.POST)
	public String validateGetAQuoteForm(final GetAQuoteForm getAQuoteForm, final BindingResult bindingResult,
			final RedirectAttributes redirectModel, final Model model)
	{
		getAQuoteValidator.validate(getAQuoteForm, bindingResult);

		final boolean hasErrorFlag = bindingResult.hasErrors();
		redirectModel.addFlashAttribute(BcfstorefrontaddonWebConstants.GET_A_QUOTE_ERROR_FLAG, hasErrorFlag);

		if (hasErrorFlag)
		{
			redirectModel.addFlashAttribute(BcfstorefrontaddonWebConstants.GET_A_QUOTE_BINDING_RESULT, bindingResult);
			redirectModel.addFlashAttribute(BcfstorefrontaddonWebConstants.GET_A_QUOTE_FORM, getAQuoteForm);
		}
		else
		{
			final GetAQuoteData getAQuoteData = new GetAQuoteData();
			getAQuoteData.setFirstName(getAQuoteForm.getFirstName());
			getAQuoteData.setLastName(getAQuoteForm.getLastName());
			getAQuoteData.setEmail(getAQuoteForm.getEmail());
			getAQuoteData.setConfirmEmail(getAQuoteForm.getConfirmEmail());
			getAQuoteData.setPhoneNumber(getAQuoteForm.getPhoneNumber());
			getAQuoteData.setComments(getAQuoteForm.getComments());
			getAQuoteData.setCheckInDateTime(getAQuoteForm.getCheckInDateTime());
			getAQuoteData.setMoreInfo(getAQuoteForm.isMoreInfo());
			getAQuoteData.setNumberOfRooms(getAQuoteForm.getNumberOfRooms());
			getAQuoteData.setPostalCode(getAQuoteForm.getPostalCode());
			getAQuoteData.setRoomStayCandidates(getAQuoteForm.getRoomStayCandidates());
			getAQuoteData.setProductName(getAQuoteForm.getProductName());

			getAQuoteFacade.createTicketForGetQuote(getAQuoteData, BcfstorefrontaddonWebConstants.GET_A_QUOTE_TITLE);
			GlobalMessages.addFlashMessage(redirectModel, GlobalMessages.CONF_MESSAGES_HOLDER,
					BcfstorefrontaddonWebConstants.GET_A_QUOTE_SUCCESS_MESSAGE);
		}
		return REDIRECT_PREFIX + GET_A_QUOTE_PAGE;
	}

	public TravellerSortStrategy getTravellerSortStrategy()
	{
		return travellerSortStrategy;
	}

	public BCFPassengerTypeFacade getPassengerTypeFacade()
	{
		return passengerTypeFacade;
	}
}
