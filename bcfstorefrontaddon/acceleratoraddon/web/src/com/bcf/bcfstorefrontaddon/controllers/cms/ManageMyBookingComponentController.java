/*
 * Copyright (c) 2019 British Columbia Ferry Services Inc
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of British Columbia Ferry Services Inc("Confidential Information").
 * You shall not disclose such Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with British Columbia Ferry Services Inc.
 *
 * Last Modified: 02/07/19 14:01
 */

package com.bcf.bcfstorefrontaddon.controllers.cms;

import javax.servlet.http.HttpServletRequest;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import com.bcf.bcfstorefrontaddon.constants.BcfstorefrontaddonControllerConstants;
import com.bcf.bcfstorefrontaddon.forms.ManageMyBookingForm;
import com.bcf.bcfstorefrontaddon.model.components.ManageMyBookingComponentModel;


/**
 * Controller for Manage My Booking Component
 */
@Controller("ManageMyBookingComponentController")
@RequestMapping(value = BcfstorefrontaddonControllerConstants.Actions.Cms.ManageMyBookingComponent)
public class ManageMyBookingComponentController extends SubstitutingCMSAddOnComponentController<ManageMyBookingComponentModel>
{
	/**
	 * This method fills ManageMyBookingComponent model with ManageMyBookingForm.
	 */
	@Override
	protected void fillModel(final HttpServletRequest request, final Model model, final ManageMyBookingComponentModel component)
	{
		model.addAttribute(new ManageMyBookingForm());

	}

}
