/*
 * Copyright (c) 2019 British Columbia Ferry Services Inc
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of British Columbia Ferry Services Inc("Confidential Information").
 * You shall not disclose such Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with British Columbia Ferry Services Inc.
 *
 * Last Modified: 02/07/19 14:01
 */

package com.bcf.bcfstorefrontaddon.controllers.pages;

import de.hybris.platform.acceleratorstorefrontcommons.annotations.RequireHardLogIn;
import de.hybris.platform.acceleratorstorefrontcommons.controllers.util.GlobalMessages;
import de.hybris.platform.cms2.exceptions.CMSItemNotFoundException;
import de.hybris.platform.commercefacades.i18n.I18NFacade;
import de.hybris.platform.commercefacades.user.data.AccountTypeData;
import de.hybris.platform.commercefacades.user.data.AddressData;
import de.hybris.platform.commercefacades.user.data.CountryData;
import de.hybris.platform.commercefacades.user.data.CustomerData;
import de.hybris.platform.commercefacades.user.data.RegionData;
import de.hybris.platform.commercefacades.user.data.RegisterData;
import de.hybris.platform.travelfacades.facades.TravelI18NFacade;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.Objects;
import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang3.ArrayUtils;
import org.apache.log4j.Logger;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.validation.support.BindingAwareModelMap;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;
import com.bcf.bcfstorefrontaddon.form.validators.BCFRegistrationValidator;
import com.bcf.bcfstorefrontaddon.forms.BCFRegisterForm;
import com.bcf.core.enums.AccountType;
import com.bcf.core.util.CountryDropDownUtil;
import com.bcf.facades.customer.BCFCustomerFacade;
import com.bcf.facades.customer.data.CustomerSubscriptionData;
import com.bcf.facades.subscription.CustomerSubscriptionsFacade;
import com.bcf.integrations.core.exception.IntegrationException;


@Controller
@RequestMapping("/activate-profile")
public class ActivateProfilePageController extends BcfAbstractPageController
{
	private static final Logger LOG = Logger.getLogger(ActivateProfilePageController.class);

	public static final String FORM_BINDING_RESULT = "org.springframework.validation.BindingResult.activateProfileForm";
	private static final String ACTIVATE_PROFILE_CMS_PAGE = "activate-profile";
	private static final String ACTIVATE_PROFILE_FORM = "activateProfileForm";
	private static final String DEFAULT_CUSTOMER_REDIRECT = "/my-account";

	@Resource(name = "bcfRegistrationValidator")
	private BCFRegistrationValidator bcfRegistrationValidator;

	@Resource(name = "bcfCustomerFacade")
	private BCFCustomerFacade bcfCustomerFacade;

	@Resource(name = "travelI18NFacade")
	private TravelI18NFacade travelI18NFacade;

	@Resource(name = "i18NFacade")
	private I18NFacade i18NFacade;

	@Resource(name = "customerSubscriptionsFacade")
	private CustomerSubscriptionsFacade customerSubscriptionsFacade;

	@RequireHardLogIn
	@RequestMapping(method = RequestMethod.GET)
	public String viewPage(final HttpServletRequest request, final Model model) throws CMSItemNotFoundException
	{
		BCFRegisterForm bcfRegisterForm = null;
		if (model.asMap().containsKey(ACTIVATE_PROFILE_FORM))
		{
			bcfRegisterForm = (BCFRegisterForm) model.asMap().get(ACTIVATE_PROFILE_FORM);
		}
		else
		{
			bcfRegisterForm = this.populateFormData();
		}

		model.addAttribute("subscriptions", customerSubscriptionsFacade.getCustomerSubscriptionsForActivateProfile());
		model.addAttribute("regions", getRegions(bcfRegisterForm.getCountry()));
		model.addAttribute(ACTIVATE_PROFILE_FORM, bcfRegisterForm);
		storeCmsPageInModel(model, getContentPageForLabelOrId(ACTIVATE_PROFILE_CMS_PAGE));
		setUpMetaDataForContentPage(model, getContentPageForLabelOrId(ACTIVATE_PROFILE_CMS_PAGE));
		return getViewForPage(model);
	}

	@RequireHardLogIn
	@RequestMapping(method = RequestMethod.POST)
	public String createOrUpdateProfile(final BCFRegisterForm bcfRegisterForm, final BindingResult bindingResult,
			final Model model,
			final HttpServletRequest request, final HttpServletResponse response, final RedirectAttributes redirectModel)
	{

		bcfRegistrationValidator.validate(bcfRegisterForm, bindingResult);

		if (bindingResult.hasErrors())
		{
			redirectModel.addFlashAttribute(ACTIVATE_PROFILE_FORM, bcfRegisterForm);
			redirectModel.addFlashAttribute(FORM_BINDING_RESULT, bindingResult);
			GlobalMessages.addFlashMessage(redirectModel, GlobalMessages.ERROR_MESSAGES_HOLDER, "form.global.error");
			return REDIRECT_PREFIX + ACTIVATE_PROFILE_CMS_PAGE;
		}

		final RegisterData registerData = populateRegisterData(bcfRegisterForm);

		if (((CustomerData)((BindingAwareModelMap) model).get("user")).getUid().equals(bcfRegisterForm.getEmail()))
		{
			try
			{
				bcfCustomerFacade.activateProfile(registerData);
			}
			catch (final IntegrationException e)
			{
				LOG.error(e.getMessage(), e);
				redirectModel.addFlashAttribute(ACTIVATE_PROFILE_FORM, bcfRegisterForm);
				GlobalMessages.addFlashMessage(redirectModel, GlobalMessages.ERROR_MESSAGES_HOLDER, "text.service.notAvailable");
				return REDIRECT_PREFIX + ACTIVATE_PROFILE_CMS_PAGE;
			}
		}
		else {
			GlobalMessages.addFlashMessage(redirectModel, GlobalMessages.ERROR_MESSAGES_HOLDER, "text.email.id.not.correct");
			return REDIRECT_PREFIX + ACTIVATE_PROFILE_CMS_PAGE;
		}
		try
		{
			if(ArrayUtils.isNotEmpty(bcfRegisterForm.getOptInSubscriptions()))
			{
				customerSubscriptionsFacade.createServiceNoticesSubscriptions(Arrays.asList(bcfRegisterForm.getOptInSubscriptions()));
			}
		}
		catch (final IntegrationException e)
		{
			LOG.error(e.getMessage(), e);
		}

		GlobalMessages.addFlashMessage(redirectModel, GlobalMessages.CONF_MESSAGES_HOLDER, "text.profile.activation.success");
		return REDIRECT_PREFIX + DEFAULT_CUSTOMER_REDIRECT;
	}

	private List<String> populateSubscriptions(final List<CustomerSubscriptionData> customerSubscriptionList)
	{
		if (CollectionUtils.isEmpty(customerSubscriptionList))
		{
			return Collections.emptyList();
		}

		final List<String> optinSubs = new ArrayList<>();
		customerSubscriptionList.forEach(customerSubscriptionData -> {
			optinSubs.add(customerSubscriptionData.getCode());
		});

		return optinSubs;
	}

	private RegisterData populateRegisterData(final BCFRegisterForm bcfRegisterForm)
	{
		final RegisterData registerData = new RegisterData();
		registerData.setLogin(bcfRegisterForm.getEmail());
		registerData.setAccountType(bcfRegisterForm.getAccountType());
		registerData.setFirstName(bcfRegisterForm.getFirstName());
		registerData.setLastName(bcfRegisterForm.getLastName());
		registerData.setConsumer(true);
		registerData.setWebEnabled(true);
		if (StringUtils.equals(bcfRegisterForm.getAccountType(), AccountType.FULL_ACCOUNT.getCode()))
		{
			registerData.setConsumer(false);
			registerData.setPhoneNo(bcfRegisterForm.getMobileNumber());
			registerData.setAddressLine1(bcfRegisterForm.getAddressLine1());
			registerData.setAddressLine2(bcfRegisterForm.getAddressLine2());
			registerData.setCity(bcfRegisterForm.getCity());
			registerData.setCountry(bcfRegisterForm.getCountry());
			if (StringUtils.equals("CA", bcfRegisterForm.getCountry()) || StringUtils.equals("US", bcfRegisterForm.getCountry()))
			{
				registerData.setProvince(bcfRegisterForm.getProvince());
			}
			registerData.setZipCode(bcfRegisterForm.getZipCode());
		}
		return registerData;
	}

	private BCFRegisterForm populateFormData()
	{
		final BCFRegisterForm bcfRegisterForm = new BCFRegisterForm();
		final CustomerData currentCustomer = bcfCustomerFacade.getCurrentCustomer();
		bcfRegisterForm.setEmail(currentCustomer.getUid());
		if (!currentCustomer.getFirstName().equals(currentCustomer.getUid()))
		{
			bcfRegisterForm.setFirstName(currentCustomer.getFirstName());
			bcfRegisterForm.setLastName(currentCustomer.getLastName());
		}

		bcfRegisterForm.setMobileNumber(currentCustomer.getContactNumber());
		bcfRegisterForm.setAccountType(currentCustomer.getAccountType());


		final AddressData defaultAddress = currentCustomer.getDefaultAddress();
		if (Objects.nonNull(defaultAddress))
		{
			bcfRegisterForm.setAddressLine1(defaultAddress.getLine1());
			bcfRegisterForm.setAddressLine2(defaultAddress.getLine2());
			bcfRegisterForm.setCity(defaultAddress.getTown());
			if (Objects.nonNull(defaultAddress.getCountry()))
			{
				bcfRegisterForm.setCountry(defaultAddress.getCountry().getIsocode());
			}

			if (Objects.nonNull(defaultAddress.getRegion()))
			{
				bcfRegisterForm.setProvince(defaultAddress.getRegion().getIsocode());
			}
			bcfRegisterForm.setZipCode(defaultAddress.getPostalCode());
		}

		final List<String> optinSubscriptions = new ArrayList<>();
		currentCustomer.getSubscriptionList()
				.forEach(customerSubscriptionData -> {
					if (customerSubscriptionData.isOptIn())
					{
						optinSubscriptions.add(customerSubscriptionData.getName());
					}
				});
		bcfRegisterForm.setOptInSubscriptions(optinSubscriptions.toArray(new String[optinSubscriptions.size()]));

		return bcfRegisterForm;

	}

	@RequestMapping(value = "/getRegions")
	public @ResponseBody
	List<RegionData> getRegions(@RequestParam final String countryIsoCode)
	{
		if (StringUtils.isEmpty(countryIsoCode))
		{
			return Collections.emptyList();
		}
		return i18NFacade.getRegionsForCountryIso(countryIsoCode);
	}

	@ModelAttribute("accountTypes")
	protected List<AccountTypeData> getAccountTypes()
	{
		return bcfCustomerFacade.getCustomerAccountTypes();
	}

	@ModelAttribute("countries")
	protected List<CountryData> getCountries()
	{
		final List<CountryData> allCountry = travelI18NFacade.getAllCountries();
		return new CountryDropDownUtil().getAllCountry(allCountry);
	}
}
