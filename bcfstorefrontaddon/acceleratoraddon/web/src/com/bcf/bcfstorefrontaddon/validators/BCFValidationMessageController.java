/*
 * Copyright (c) 2019 British Columbia Ferry Services Inc
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of British Columbia Ferry Services Inc("Confidential Information").
 * You shall not disclose such Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with British Columbia Ferry Services Inc.
 *
 * Last Modified: 06/06/19 20:21
 */

package com.bcf.bcfstorefrontaddon.validators;

import de.hybris.platform.enumeration.EnumerationService;
import de.hybris.platform.servicelayer.exceptions.UnknownIdentifierException;
import java.util.Collections;
import java.util.Map;
import java.util.stream.Collectors;
import javax.annotation.Resource;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import com.bcf.custom.propertysource.data.PropertySourceData;
import com.bcf.custom.propertysource.enums.MessageCategory;
import com.bcf.custom.propertysource.facade.PropertySourceFacade;


@Controller
public class BCFValidationMessageController
{
	@Resource(name = "propertySourceFacade")
	private PropertySourceFacade propertySourceFacade;

	@Resource(name = "enumerationService")
	private EnumerationService enumerationService;

	@ResponseBody
	@RequestMapping(value = "/validation/message", method = RequestMethod.GET, produces = "application/json")
	public Map<String, String> getValidationMessages(@RequestParam(value = "formName") final String formName,
			@RequestParam(value = "messageCategory") final String messageCategory)
	{
		try
		{
			return propertySourceFacade.getPropertySourceByFormNameAndMessageCategory(formName,
					enumerationService.getEnumerationValue(MessageCategory.class, messageCategory)).stream().collect(
					Collectors.toMap(PropertySourceData::getCode, PropertySourceData::getValue));
		}
		catch (final UnknownIdentifierException e)
		{
			return Collections.emptyMap();
		}
	}

	@ResponseBody
	@RequestMapping(value = "/validation/message-for-code", method = RequestMethod.GET)
	public String getValidationMessage(@RequestParam(value = "code") final String code)
	{
		return propertySourceFacade.getPropertySourceValue(code);
	}
}
