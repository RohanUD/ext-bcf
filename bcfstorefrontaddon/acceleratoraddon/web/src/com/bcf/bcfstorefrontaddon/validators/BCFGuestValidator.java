/*
 * [y] hybris Platform
 *
 * Copyright (c) 2000-2016 SAP SE
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of SAP
 * Hybris ("Confidential Information"). You shall not disclose such
 * Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with SAP Hybris.
 *
 *
 */

package com.bcf.bcfstorefrontaddon.validators;

import java.util.regex.Matcher;
import java.util.regex.Pattern;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Required;
import org.springframework.validation.Errors;
import org.springframework.validation.Validator;
import com.bcf.bcfstorefrontaddon.forms.BCFGuestForm;
import com.bcf.core.bcfproperties.BcfConfigurablePropertiesService;
import com.bcf.facades.constants.BcfFacadesConstants;


public class BCFGuestValidator implements Validator
{
	private BcfConfigurablePropertiesService bcfConfigurablePropertiesService;

	public static final Pattern PHONENO_REGEX = Pattern.compile("(\\+?\\d+\\s?\\-?\\(?\\d+\\)?\\s?\\-?\\d+\\s?-?\\d+)");

	@Override
	public boolean supports(final Class<?> aClass)
	{
		return BCFGuestForm.class.equals(aClass);
	}

	@Override
	public void validate(final Object object, final Errors errors)
	{
		final BCFGuestForm guestForm = (BCFGuestForm) object;
		final String email = guestForm.getEmail();
		final String firstName = guestForm.getFirstName();
		final String lastName = guestForm.getLastName();
		final String phoneNo = guestForm.getPhoneNo();

		final int emailValidationLength = Integer
				.parseInt(getBcfConfigurablePropertiesService().getBcfPropertyValue("emailValidationLength"));
		final int nameValidationLength = Integer
				.parseInt(getBcfConfigurablePropertiesService().getBcfPropertyValue("nameValidationLength"));
		final int minPhoneValidationLength = Integer
				.parseInt(getBcfConfigurablePropertiesService().getBcfPropertyValue("minPhoneValidationLength"));
		final int maxPhoneValidationLength = Integer
				.parseInt(getBcfConfigurablePropertiesService().getBcfPropertyValue("maxPhoneValidationLength"));

		if (StringUtils.isEmpty(email) || StringUtils.length(email) > emailValidationLength
				|| !BcfFacadesConstants.EMAIL_REGEX.matcher(email).matches())
		{
			errors.rejectValue("email", "profile.email.invalid");
		}

		validateName(errors, firstName, "firstName", "guest.firstName.invalid", nameValidationLength);
		validateName(errors, lastName, "lastName", "guest.lastName.invalid", nameValidationLength);

		if (StringUtils.length(firstName) + StringUtils.length(lastName) > nameValidationLength)
		{
			errors.rejectValue("lastName", "profile.name.invalid");
			errors.rejectValue("firstName", "profile.name.invalid");
		}
		validatePhoneNo(errors, phoneNo, minPhoneValidationLength, maxPhoneValidationLength);
	}

	protected void validateName(final Errors errors, final String name, final String propertyName, final String property,
			final int nameValidationLength)
	{
		if (StringUtils.isBlank(name) || StringUtils.length(name) > nameValidationLength)
		{
			errors.rejectValue(propertyName, property);
		}
	}

	protected void validatePhoneNo(final Errors errors, final String phoneNo, final int minPhoneValidationLength, final int maxPhoneValidationLength)
	{
		if (StringUtils.isEmpty(phoneNo) || StringUtils.length(phoneNo) < minPhoneValidationLength || StringUtils.length(phoneNo) > maxPhoneValidationLength || !validatePhoneNo(phoneNo))
		{
			errors.rejectValue("phoneNo", "guest.phoneNo.invalid");
		}
	}

	public boolean validatePhoneNo(final String phoneNo)
	{
		final Matcher matcher = PHONENO_REGEX.matcher(phoneNo);
		return matcher.matches();
	}

	protected BcfConfigurablePropertiesService getBcfConfigurablePropertiesService()
	{
		return bcfConfigurablePropertiesService;
	}

	@Required
	public void setBcfConfigurablePropertiesService(final BcfConfigurablePropertiesService bcfConfigurablePropertiesService)
	{
		this.bcfConfigurablePropertiesService = bcfConfigurablePropertiesService;
	}
}
