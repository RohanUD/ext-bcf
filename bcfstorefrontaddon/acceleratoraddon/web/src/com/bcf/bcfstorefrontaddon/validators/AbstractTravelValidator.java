/*
 * Copyright (c) 2019 British Columbia Ferry Services Inc
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of British Columbia Ferry Services Inc("Confidential Information").
 * You shall not disclose such Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with British Columbia Ferry Services Inc.
 *
 * Last Modified: 21/06/19 00:21
 */

package com.bcf.bcfstorefrontaddon.validators;

import de.hybris.platform.commercefacades.accommodation.search.RoomStayCandidateData;
import de.hybris.platform.servicelayer.config.ConfigurationService;
import de.hybris.platform.servicelayer.time.TimeService;
import de.hybris.platform.travelservices.constants.TravelacceleratorstorefrontValidationConstants;
import de.hybris.platform.travelservices.utils.TravelDateUtils;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Collection;
import java.util.Date;
import java.util.List;
import javax.annotation.Resource;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.validation.Errors;
import org.springframework.validation.Validator;
import com.bcf.bcfstorefrontaddon.constants.BcfstorefrontaddonWebConstants;
import com.bcf.facades.constants.BcfFacadesConstants;


public abstract class AbstractTravelValidator implements Validator
{

	private static final Logger LOGGER = Logger.getLogger(AbstractTravelValidator.class);

	private static final String FIELD_INVALID_NUMBER_OF_PASSENGERS = "InvalidNumberOfPassengers";
	private static final String FIELD_ROOMSTAYCANDIDATES = "roomStayCandidates";
	public static final String REGEX_LETTERS_NUMBERS_SPACE = "^[a-zA-Z0-9 ]+$";
	private static final String INVALID_QUANTITY = "InvalidQuantity";

	@Resource(name = "timeService")
	private TimeService timeService;

	@Resource(name = "configurationService")
	private ConfigurationService configurationService;


	private String targetForm;

	private String attributePrefix;

	/**
	 * Method to check if the value of the selected field either empty, null or contains only white spaces and if so it
	 * will set a EmptyField error against the field
	 *
	 * @param fieldName
	 * @param fieldValue
	 * @param errors
	 */
	protected Boolean validateBlankField(final String fieldName, final String fieldValue, final Errors errors)
	{
		if (StringUtils.isBlank(fieldValue))
		{
			rejectValue(errors, fieldName, TravelacceleratorstorefrontValidationConstants.ERROR_TYPE_EMPTY_FIELD);
			return Boolean.FALSE;
		}
		return Boolean.TRUE;
	}

	/**
	 * Validate field pattern boolean.
	 *
	 * @param attribute the attribute
	 * @param pattern   the pattern
	 * @return the boolean
	 */
	public Boolean validateFieldPattern(final String attribute, final String pattern)
	{
		if (!attribute.matches(pattern))
		{
			return Boolean.FALSE;
		}
		return Boolean.TRUE;
	}

	/**
	 * Method responsible for validating the date format
	 *
	 * @param date
	 * @param errors
	 * @param field
	 * @return
	 */
	protected Boolean validateDateFormatFareFinder(final String date, final Errors errors, final String field)
	{

		if (StringUtils.isNotEmpty(date))
		{


			final DateFormat dateFormat = new SimpleDateFormat(BcfFacadesConstants.DATE_PATTERN_MM_DD_YYYY);
				dateFormat.setLenient(false);
				try
				{
					dateFormat.parse(date);
					return true;
				}
				catch (final ParseException e)
				{
					rejectValue(errors, field, TravelacceleratorstorefrontValidationConstants.ERROR_TYPE_INVALID_DATE);
				}
		}
		return false;
	}

	protected Boolean validateDateFormat(final String date, final Errors errors, final String field)
	{

		if (StringUtils.isNotEmpty(date))
		{

			if (!date.matches(TravelacceleratorstorefrontValidationConstants.REG_EX_DD_MM_YYY_WITH_FORWARD_SLASH))
			{
				rejectValue(errors, field, TravelacceleratorstorefrontValidationConstants.ERROR_TYPE_INVALID_DATE_FORMAT);
			}
			else
			{
				final DateFormat dateFormat = new SimpleDateFormat(BcfFacadesConstants.DATE_PATTERN_MM_DD_YYYY);
				dateFormat.setLenient(false);
				try
				{
					dateFormat.parse(date);
					return true;
				}
				catch (final ParseException e)
				{
					rejectValue(errors, field, TravelacceleratorstorefrontValidationConstants.ERROR_TYPE_INVALID_DATE);
				}
			}
		}
		return false;
	}

	/**
	 * Method check to see if date is a past date and if so sets a PastDate errors for the field
	 *
	 * @param date
	 * @param errors
	 * @param field
	 */
	protected void validateForPastDateFareFinder(final String date, final Errors errors, final String field)
	{
		final Date selectedDate = TravelDateUtils.convertStringDateToDate(date, BcfFacadesConstants.DATE_PATTERN_MM_DD_YYYY);
		final Date currentDate = TravelDateUtils.convertStringDateToDate(
				TravelDateUtils.convertDateToStringDate(timeService.getCurrentTime(), BcfFacadesConstants.DATE_PATTERN_MM_DD_YYYY),
				BcfFacadesConstants.DATE_PATTERN_MM_DD_YYYY);

		final boolean pastDate = selectedDate.before(currentDate);

		if (pastDate)
		{
			rejectValue(errors, field, TravelacceleratorstorefrontValidationConstants.ERROR_TYPE_PAST_DATE);
		}
	}

	protected void validateForPastDate(final String date, final Errors errors, final String field)
	{
		final Date selectedDate = TravelDateUtils.convertStringDateToDate(date, BcfFacadesConstants.DATE_PATTERN_MM_DD_YYYY);
		final Date currentDate = TravelDateUtils.convertStringDateToDate(
				TravelDateUtils.convertDateToStringDate(timeService.getCurrentTime(), BcfFacadesConstants.DATE_PATTERN_MM_DD_YYYY),
				BcfFacadesConstants.DATE_PATTERN_MM_DD_YYYY);

		final boolean pastDate = selectedDate.before(currentDate);

		if (pastDate)
		{
			rejectValue(errors, field, TravelacceleratorstorefrontValidationConstants.ERROR_TYPE_PAST_DATE);
		}
	}

	protected void validateDates(final String startDate, final String startDateFieldName, final String endDate,
			final String endDateFieldName, final Errors errors)
	{
		boolean isNotBlank = validateBlankField(startDateFieldName, startDate, errors);

		boolean isCheckInDateCorrectlyFormatted = false;
		boolean isCheckOutDateCorrectlyFormatted = false;

		if (isNotBlank)
		{
			isCheckInDateCorrectlyFormatted = validateDateFormat(startDate, errors, startDateFieldName);

			if (isCheckInDateCorrectlyFormatted)
			{
				validateForPastDate(startDate, errors, startDateFieldName);
			}
		}

		isNotBlank = validateBlankField(endDateFieldName, endDate, errors);
		if (isNotBlank)
		{
			isCheckOutDateCorrectlyFormatted = validateDateFormat(endDate, errors, endDateFieldName);
		}

		if (isCheckInDateCorrectlyFormatted && isCheckOutDateCorrectlyFormatted)
		{
			validateCheckInAndCheckOutDateTimeRange(startDate, startDateFieldName, endDate, endDateFieldName, errors);
		}


	}

	/**
	 * Method used to validate Check In and Check Out dates
	 *
	 * @param startDate
	 * @param startDateFieldName
	 * @param endDate
	 * @param endDateFieldName
	 * @param errors
	 */
	protected void validateCheckInAndCheckOutDateTimeRange(final String startDate, final String startDateFieldName,
			final String endDate, final String endDateFieldName, final Errors errors)
	{
		try
		{
			final SimpleDateFormat formatter = new SimpleDateFormat(BcfFacadesConstants.DATE_PATTERN_MM_DD_YYYY);

			final Date checkInDate = formatter.parse(startDate);
			final Date checkOutDate = formatter.parse(endDate);
			final long dateDifference = TravelDateUtils.getDaysBetweenDates(checkInDate, checkOutDate);
			final long maxAllowedDateDifference = configurationService.getConfiguration()
					.getInt(BcfstorefrontaddonWebConstants.MAX_ALLOWED_CHECKIN_CHECKOUT_DATE_DIFFERENCE);
			if (checkInDate.after(checkOutDate) || checkInDate.equals(checkOutDate) || dateDifference > maxAllowedDateDifference)
			{
				rejectValue(errors, startDateFieldName, TravelacceleratorstorefrontValidationConstants.ERROR_TYPE_OUT_OF_RANGE);
			}
		}
		catch (final ParseException e)
		{
			LOGGER.error("Unable to parse String data to Date object:", e);
		}
	}

	/**
	 * @param numberOfRooms
	 * @param roomStayCandidates
	 * @param errors
	 */
	public void validateGuestsQuantity(final int numberOfRooms, final List<RoomStayCandidateData> roomStayCandidates,
			final Errors errors)
	{
		int roomNumber = 0;
		try
		{
			//first validate number of accommodations
			final int selectedAccommodationQuantity = numberOfRooms;
			if (selectedAccommodationQuantity > configurationService.getConfiguration()
					.getInt(BcfFacadesConstants.ALACARTE_MAX_ROOMS))
			{
				rejectValue(errors, TravelacceleratorstorefrontValidationConstants.NUMBER_OF_ROOMS,
						TravelacceleratorstorefrontValidationConstants.ERROR_TYPE_INVALID_QUANTITY);
				return;
			}

			final int maxGuestQuantity = configurationService.getConfiguration().getInt(BcfFacadesConstants.MAX_GUEST_QUANTITY);
			final int collectiveNumberOfGuests = 0;
			for (int i = 0; i < selectedAccommodationQuantity; i++)
			{
				roomNumber = i;
				validateAccommodationQuantity(roomStayCandidates, i, maxGuestQuantity, errors, collectiveNumberOfGuests);
			}
		}
		catch (final IndexOutOfBoundsException | NumberFormatException e)
		{
			LOGGER.debug("No Guest List Found For Room Number " + roomNumber);
			rejectValue(errors, TravelacceleratorstorefrontValidationConstants.NUMBER_OF_ROOMS,
					TravelacceleratorstorefrontValidationConstants.ERROR_TYPE_INVALID_QUANTITY);
		}
	}

	private void validateAccommodationQuantity(final List<RoomStayCandidateData> roomStayCandidates, final int i,
			final int maxGuestQuantity, final Errors errors, int collectiveNumberOfGuests)
	{
		if (CollectionUtils.isEmpty(roomStayCandidates))
		{
			return;
		}
		int adultPassengers = 0;
		int nonAdultPassengers = 0;
		String attributeName = null;
		for (int j = 0; j < roomStayCandidates.get(i).getPassengerTypeQuantityList().size(); j++)
		{
			attributeName = "accommodationFinderForm.roomStayCandidates[" + i + "].passengerTypeQuantityList[" + j + "].quantity";
			if (StringUtils.isNotEmpty(getAttributePrefix()))
			{
				attributeName = getAttributePrefix() + "." + attributeName;
			}
			final int selectedGuestCount = roomStayCandidates.get(i).getPassengerTypeQuantityList().get(j).getQuantity();
			final String selectedGuestCode = roomStayCandidates.get(i).getPassengerTypeQuantityList().get(j).getPassengerType()
					.getCode();

			if (selectedGuestCount > maxGuestQuantity)
			{
				errors.rejectValue(attributeName, INVALID_QUANTITY + "." + getTargetForm() + "." + selectedGuestCode);
			}

			if (TravelacceleratorstorefrontValidationConstants.PASSENGER_TYPE_ADULT.equals(selectedGuestCode))
			{
				validateNegativeGuestCount(selectedGuestCount, errors, attributeName, selectedGuestCode);
				adultPassengers = selectedGuestCount;
			}
			else
			{
				validateNegativeGuestCount(selectedGuestCount, errors, attributeName, selectedGuestCode);
				nonAdultPassengers += selectedGuestCount;
			}
			collectiveNumberOfGuests = collectiveNumberOfGuests + selectedGuestCount;
		}

		attributeName = "roomStayCandidates[" + i + "].passengerTypeQuantityList[" + 0 + "].quantity";
		if (StringUtils.isNotEmpty(getAttributePrefix()))
		{
			attributeName = getAttributePrefix() + "." + attributeName;
		}
		if (nonAdultPassengers >= 0 && adultPassengers >= 0)
		{
			validatePassengerCount(nonAdultPassengers, adultPassengers, errors, attributeName, collectiveNumberOfGuests,
					maxGuestQuantity);
		}
	}

	private void validateNegativeGuestCount(final int selectedGuestCount, final Errors errors, final String attributeName,
			final String selectedGuestCode)
	{
		if (selectedGuestCount < 0)
		{
			errors.rejectValue(attributeName, INVALID_QUANTITY + "."
					+ TravelacceleratorstorefrontValidationConstants.ACCOMMODATION_FORM + "." + selectedGuestCode);
		}
	}

	private void validatePassengerCount(final int nonAdultPassengers, final int adultPassengers, final Errors errors,
			final String attributeName, final int collectiveNumberOfGuests, final int maxGuestQuantity)
	{
		if (nonAdultPassengers > 0 && adultPassengers == 0)
		{
			errors.rejectValue(attributeName, TravelacceleratorstorefrontValidationConstants.ERROR_TYPE_INVALID_NUMBER_OF_ADULTS);
			return;
		}

		if (nonAdultPassengers + adultPassengers == 0)
		{
			errors.rejectValue(attributeName, TravelacceleratorstorefrontValidationConstants.ERROR_TYPE_NO_PASSENGER);
			return;
		}

		if (collectiveNumberOfGuests > maxGuestQuantity)
		{
			rejectValue(errors, FIELD_ROOMSTAYCANDIDATES, FIELD_INVALID_NUMBER_OF_PASSENGERS);
		}
	}

	/**
	 * Method to check if the value of the selected field either empty, null and if so it will set a EmptyField error
	 * against the field
	 *
	 * @param fieldName
	 * @param listObject
	 * @param errors
	 */
	protected Boolean validateEmptyField(final String fieldName, final Collection listObject, final Errors errors)
	{
		if (CollectionUtils.isEmpty(listObject))
		{
			rejectValue(errors, fieldName, TravelacceleratorstorefrontValidationConstants.ERROR_TYPE_EMPTY_FIELD);
			return Boolean.FALSE;
		}
		return Boolean.TRUE;
	}


	/**
	 * Method to add errors
	 *
	 * @param errors
	 * @param attributeName
	 * @param errorCode
	 */
	protected void rejectValue(final Errors errors, String attributeName, final String errorCode)
	{
		final String errorMessageCode = errorCode + "." + getTargetForm() + "." + attributeName;
		if (StringUtils.isNotEmpty(getAttributePrefix()))
		{
			attributeName = getAttributePrefix() + "." + attributeName;
		}
		errors.rejectValue(attributeName, errorMessageCode);
	}


    protected void rejectValue(final Errors errors, String attributeName, final String errorCode, final Object[] params)
    {
        final String errorMessageCode = errorCode + "." + getTargetForm() + "." + attributeName;
        if (StringUtils.isNotEmpty(getAttributePrefix()))
        {
            attributeName = getAttributePrefix() + "." + attributeName;
        }
        errors.rejectValue(attributeName, errorMessageCode, params, null);
    }

	protected String getTargetForm()
	{
		return targetForm;
	}

	public void setTargetForm(final String targetForm)
	{
		this.targetForm = targetForm;
	}

	/**
	 * @return the attributePrefix
	 */
	public String getAttributePrefix()
	{
		return attributePrefix;
	}

	/**
	 * @param attributePrefix the attributePrefix to set
	 */
	public void setAttributePrefix(final String attributePrefix)
	{
		this.attributePrefix = attributePrefix;
	}

	/**
	 * @return the configurationService
	 */
	protected ConfigurationService getConfigurationService()
	{
		return configurationService;
	}

	/**
	 * @param configurationService the configurationService to set
	 */
	public void setConfigurationService(final ConfigurationService configurationService)
	{
		this.configurationService = configurationService;
	}

}
