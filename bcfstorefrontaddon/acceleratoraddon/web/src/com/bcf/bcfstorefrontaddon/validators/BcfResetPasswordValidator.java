/*
 * Copyright (c) 2019 British Columbia Ferry Services Inc
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of British Columbia Ferry Services Inc("Confidential Information").
 * You shall not disclose such Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with British Columbia Ferry Services Inc.
 *
 * Last Modified: 06/06/19 20:21
 */

package com.bcf.bcfstorefrontaddon.validators;

import de.hybris.platform.core.model.user.CustomerModel;
import de.hybris.platform.servicelayer.user.UserService;
import javax.annotation.Resource;
import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.Validator;
import com.bcf.bcfstorefrontaddon.form.validators.PasswordValidatorUtil;
import com.bcf.bcfstorefrontaddon.forms.BcfUpdatePasswordForm;


@Component("bcfresetPasswordValidator")
public class BcfResetPasswordValidator implements Validator
{

	@Resource(name = "userService")
	private UserService userService;

	@Override
	public boolean supports(final Class<?> aClass)
	{
		return false;
	}

	@Override
	public void validate(final Object object, final Errors errors)
	{
		final BcfUpdatePasswordForm bcFormUpdatePwdForm = (BcfUpdatePasswordForm) object;
		final String newPassword = bcFormUpdatePwdForm.getPwd();
		final String checkPassword = bcFormUpdatePwdForm.getCheckPwd();

		PasswordValidatorUtil.validatePassword(errors, newPassword);
		PasswordValidatorUtil.comparePasswords(errors, newPassword, checkPassword);

		final CustomerModel currentUser = (CustomerModel) userService.getCurrentUser();
		final String firstName = currentUser.getFirstName();
		final String lastName = currentUser.getLastName();
		PasswordValidatorUtil.checkContainsName(errors, firstName, lastName, newPassword);
	}
}
