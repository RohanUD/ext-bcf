/*
 * Copyright (c) 2019 British Columbia Ferry Services Inc
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of British Columbia Ferry Services Inc("Confidential Information").
 * You shall not disclose such Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with British Columbia Ferry Services Inc.
 *
 * Last Modified: 06/06/19 20:21
 */

package com.bcf.bcfstorefrontaddon.forms;

/**
 * Pojo for 'order approval decision' form.
 */
public class OrderApprovalDecisionForm
{
	private String workFlowActionCode;
	private String approverSelectedDecision;
	private String comments;

	public String getWorkFlowActionCode()
	{
		return workFlowActionCode;
	}

	public void setWorkFlowActionCode(final String workFlowActionCode)
	{
		this.workFlowActionCode = workFlowActionCode;
	}

	public String getApproverSelectedDecision()
	{
		return approverSelectedDecision;
	}

	public void setApproverSelectedDecision(final String approverSelectedDecision)
	{
		this.approverSelectedDecision = approverSelectedDecision;
	}

	public String getComments()
	{
		return comments;
	}

	public void setComments(final String comments)
	{
		this.comments = comments;
	}
}
