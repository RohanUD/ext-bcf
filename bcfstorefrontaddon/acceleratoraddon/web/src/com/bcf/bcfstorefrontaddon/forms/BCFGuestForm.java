/*
 * [y] hybris Platform
 *
 * Copyright (c) 2000-2016 SAP SE
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of SAP
 * Hybris ("Confidential Information"). You shall not disclose such
 * Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with SAP Hybris.
 *
 *
 */

package com.bcf.bcfstorefrontaddon.forms;

public class BCFGuestForm
{
	private String email;
	private String firstName;
	private String lastName;
	private String phoneNo;
	private String phoneType;

	public String getPhoneNo()
	{
		return phoneNo;
	}

	public void setPhoneNo(final String phoneNo)
	{
		this.phoneNo = phoneNo;
	}

	public String getFirstName()
	{
		return firstName;
	}

	public void setFirstName(final String firstName)
	{
		this.firstName = firstName;
	}

	public String getLastName()
	{
		return lastName;
	}

	public void setLastName(final String lastName)
	{
		this.lastName = lastName;
	}

	public String getEmail()
	{
		return email;
	}

	public void setEmail(final String email)
	{
		this.email = email;
	}

	public String getPhoneType()
	{
		return phoneType;
	}

	public void setPhoneType(final String phoneType)
	{
		this.phoneType = phoneType;
	}
}
