/*
 * Copyright (c) 2019 British Columbia Ferry Services Inc
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of British Columbia Ferry Services Inc("Confidential Information").
 * You shall not disclose such Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with British Columbia Ferry Services Inc.
 *
 * Last Modified: 06/06/19 20:21
 */

package com.bcf.bcfstorefrontaddon.forms;

public class RefundSelectionForm
{
	private String transactionCode;
	private String amount;
	private String refundType;
	private String maxAmount;
	private String refundReason;


	public String getTransactionCode()
	{
		return transactionCode;
	}

	public void setTransactionCode(final String transactionCode)
	{
		this.transactionCode = transactionCode;
	}

	public String getAmount()
	{
		return amount;
	}

	public void setAmount(final String amount)
	{
		this.amount = amount;
	}

	public String getRefundType()
	{
		return refundType;
	}

	public void setRefundType(final String refundType)
	{
		this.refundType = refundType;
	}

	public String getMaxAmount()
	{
		return maxAmount;
	}

	public void setMaxAmount(final String maxAmount)
	{
		this.maxAmount = maxAmount;
	}

	public String getRefundReason()
	{
		return refundReason;
	}

	public void setRefundReason(final String refundReason)
	{
		this.refundReason = refundReason;
	}
}
