/*
 * Copyright (c) 2019 British Columbia Ferry Services Inc
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of British Columbia Ferry Services Inc("Confidential Information").
 * You shall not disclose such Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with British Columbia Ferry Services Inc.
 *
 * Last Modified: 21/06/19 00:21
 */

package com.bcf.bcfstorefrontaddon.forms;

import java.util.List;
import com.bcf.facades.vacations.ActivityGuestData;
import com.bcf.facades.vacations.RoomGuestData;


public class RefundOptionForm
{

	private String bookingReference;

	public String getBookingReference()
	{
		return bookingReference;
	}

	public void setBookingReference(final String bookingReference)
	{
		this.bookingReference = bookingReference;
	}


	private String goodWillRefundComment;

	public String getGoodWillRefundComment()
	{
		return goodWillRefundComment;
	}

	public void setGoodWillRefundComment(final String goodWillRefundComment)
	{
		this.goodWillRefundComment = goodWillRefundComment;
	}

	private String goodWillRefundCode;

	public String getGoodWillRefundCode()
	{
		return goodWillRefundCode;
	}

	public void setGoodWillRefundCode(final String goodWillRefundCode)
	{
		this.goodWillRefundCode = goodWillRefundCode;
	}

	private String maxRefundAmount;

	public String getMaxRefundAmount()
	{
		return maxRefundAmount;
	}

	public void setMaxRefundAmount(final String maxRefundAmount)
	{
		this.maxRefundAmount = maxRefundAmount;
	}

	private boolean termsCheck;

	public boolean isTermsCheck()
	{
		return termsCheck;
	}

	public void setTermsCheck(final boolean termsCheck)
	{
		this.termsCheck = termsCheck;
	}

	private List<RoomGuestData> roomGuestData;

	private List<ActivityGuestData> activityLeadPaxInfo;

	public List<ActivityGuestData> getActivityLeadPaxInfo()
	{
		return activityLeadPaxInfo;
	}

	public void setActivityLeadPaxInfo(final List<ActivityGuestData> activityLeadPaxInfo)
	{
		this.activityLeadPaxInfo = activityLeadPaxInfo;
	}

	public List<RoomGuestData> getRoomGuestData()
	{
		return roomGuestData;
	}

	public void setRoomGuestData(final List<RoomGuestData> roomGuestData)
	{
		this.roomGuestData = roomGuestData;
	}

	private List<RefundSelectionForm> refundSelectionForms;


	public List<RefundSelectionForm> getRefundSelectionForms()
	{
		return refundSelectionForms;
	}

	public void setRefundSelectionForms(final List<RefundSelectionForm> refundSelectionForms)
	{
		this.refundSelectionForms = refundSelectionForms;
	}
}
