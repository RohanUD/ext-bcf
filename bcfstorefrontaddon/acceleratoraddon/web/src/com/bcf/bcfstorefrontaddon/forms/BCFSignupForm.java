/*
 * Copyright (c) 2019 British Columbia Ferry Services Inc
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of British Columbia Ferry Services Inc("Confidential Information").
 * You shall not disclose such Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with British Columbia Ferry Services Inc.
 *
 * Last Modified: 06/06/19 20:21
 */

package com.bcf.bcfstorefrontaddon.forms;

public class BCFSignupForm
{
	private String email;
	private String checkEmail;
	private String pwd;
	private String checkPwd;
	private String captcha;
	private String country;
	private String province;
	private String zipCode;
	private String accountType;
	private String firstName;
	private String lastName;
	private String phoneNo;
	private String userGroup;

	public String getEmail()
	{
		return email;
	}

	public void setEmail(final String email)
	{
		this.email = email;
	}

	public String getCheckEmail()
	{
		return checkEmail;
	}

	public void setCheckEmail(final String checkEmail)
	{
		this.checkEmail = checkEmail;
	}

	public String getPwd()
	{
		return pwd;
	}

	public void setPwd(final String pwd)
	{
		this.pwd = pwd;
	}

	public String getCheckPwd()
	{
		return checkPwd;
	}

	public void setCheckPwd(final String checkPwd)
	{
		this.checkPwd = checkPwd;
	}

	public String getCaptcha()
	{
		return captcha;
	}

	public void setCaptcha(final String captcha)
	{
		this.captcha = captcha;
	}

	public String getCountry()
	{
		return country;
	}

	public void setCountry(final String country)
	{
		this.country = country;
	}

	public String getProvince()
	{
		return province;
	}

	public void setProvince(final String province)
	{
		this.province = province;
	}

	public String getZipCode()
	{
		return zipCode;
	}

	public void setZipCode(final String zipCode)
	{
		this.zipCode = zipCode;
	}

	public String getAccountType()
	{
		return accountType;
	}

	public void setAccountType(final String accountType)
	{
		this.accountType = accountType;
	}

	public String getFirstName()
	{
		return firstName;
	}

	public void setFirstName(final String firstName)
	{
		this.firstName = firstName;
	}

	public String getLastName()
	{
		return lastName;
	}

	public void setLastName(final String lastName)
	{
		this.lastName = lastName;
	}

	public String getPhoneNo()
	{
		return phoneNo;
	}

	public void setPhoneNo(final String phoneNo)
	{
		this.phoneNo = phoneNo;
	}

	public String getUserGroup()
	{
		return userGroup;
	}

	public void setUserGroup(final String userGroup)
	{
		this.userGroup = userGroup;
	}
}
