package com.bcf.bcfstorefrontaddon.forms;

public class BookingsAdvanceSearchForm
{
	private String bookingReference;
	private String departurePortCode;
	private String arrivalPortCode;
	private String departureDateTimeFrom;
	private String departureDateTimeTo;
	private String bookingType;
	private String pageNumber;

	public String getBookingReference()
	{
		return bookingReference;
	}

	public void setBookingReference(final String bookingReference)
	{
		this.bookingReference = bookingReference;
	}

	public String getDeparturePortCode()
	{
		return departurePortCode;
	}

	public void setDeparturePortCode(final String departurePortCode)
	{
		this.departurePortCode = departurePortCode;
	}

	public String getArrivalPortCode()
	{
		return arrivalPortCode;
	}

	public void setArrivalPortCode(final String arrivalPortCode)
	{
		this.arrivalPortCode = arrivalPortCode;
	}

	public String getDepartureDateTimeFrom()
	{
		return departureDateTimeFrom;
	}

	public void setDepartureDateTimeFrom(final String departureDateTimeFrom)
	{
		this.departureDateTimeFrom = departureDateTimeFrom;
	}

	public String getDepartureDateTimeTo()
	{
		return departureDateTimeTo;
	}

	public void setDepartureDateTimeTo(final String departureDateTimeTo)
	{
		this.departureDateTimeTo = departureDateTimeTo;
	}

	public String getBookingType()
	{
		return bookingType;
	}

	public void setBookingType(final String bookingType)
	{
		this.bookingType = bookingType;
	}

	public String getPageNumber()
	{
		return pageNumber;
	}

	public void setPageNumber(final String pageNumber)
	{
		this.pageNumber = pageNumber;
	}
}
