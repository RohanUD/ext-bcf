/*
 * Copyright (c) 2019 British Columbia Ferry Services Inc
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of British Columbia Ferry Services Inc("Confidential Information").
 * You shall not disclose such Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with British Columbia Ferry Services Inc.
 *
 * Last Modified: 06/06/19 20:21
 */

package com.bcf.bcfstorefrontaddon.forms;

import java.util.List;


public class PaymentTransactionForm
{
	private List<String> entryNumbers;
	private String amount;

	public List<String> getEntryNumbers()
	{
		return entryNumbers;
	}

	public void setEntryNumbers(final List<String> entryNumbers)
	{
		this.entryNumbers = entryNumbers;
	}

	public String getAmount()
	{
		return amount;
	}

	public void setAmount(final String amount)
	{
		this.amount = amount;
	}


}
