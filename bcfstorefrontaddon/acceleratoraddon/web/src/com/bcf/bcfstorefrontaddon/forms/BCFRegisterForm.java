/*
 * Copyright (c) 2019 British Columbia Ferry Services Inc
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of British Columbia Ferry Services Inc("Confidential Information").
 * You shall not disclose such Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with British Columbia Ferry Services Inc.
 *
 * Last Modified: 02/07/19 14:01
 */

package com.bcf.bcfstorefrontaddon.forms;

import de.hybris.platform.acceleratorstorefrontcommons.forms.RegisterForm;


public class BCFRegisterForm extends RegisterForm
{

	private String addressLine1;
	private String addressLine2;
	private String city;
	private String country;
	private String province;
	private String zipCode;
	private String accountType;
	private String userGroup;
	private String mobileNumber;
	private String[] optInSubscriptions;

	public String getAddressLine1()
	{
		return addressLine1;
	}

	public void setAddressLine1(final String addressLine1)
	{
		this.addressLine1 = addressLine1;
	}

	public String getAddressLine2()
	{
		return addressLine2;
	}

	public void setAddressLine2(final String addressLine2)
	{
		this.addressLine2 = addressLine2;
	}

	public String getProvince()
	{
		return province;
	}

	public void setProvince(final String province)
	{
		this.province = province;
	}

	public String getZipCode()
	{
		return zipCode;
	}

	public void setZipCode(final String zipCode)
	{
		this.zipCode = zipCode;
	}

	public String getCity()
	{
		return city;
	}

	public void setCity(final String city)
	{
		this.city = city;
	}

	public String getUserGroup()
	{
		return userGroup;
	}

	public void setUserGroup(final String userGroup)
	{
		this.userGroup = userGroup;
	}

	public String getCountry() { return country;	}

	public void setCountry(final String country)	{ this.country = country; }

	public String getAccountType()
	{
		return accountType;
	}

	public void setAccountType(final String accountType)
	{
		this.accountType = accountType;
	}

	@Override
	public String getMobileNumber()
	{
		return mobileNumber;
	}

	@Override
	public void setMobileNumber(final String mobileNumber)
	{
		this.mobileNumber = mobileNumber;
	}

	public String[] getOptInSubscriptions()
	{
		return optInSubscriptions;
	}

	public void setOptInSubscriptions(final String[] optInSubscriptions)
	{
		this.optInSubscriptions = optInSubscriptions;
	}
}
