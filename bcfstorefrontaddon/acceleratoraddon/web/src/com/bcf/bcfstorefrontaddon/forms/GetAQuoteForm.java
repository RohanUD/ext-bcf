/*
 * Copyright (c) 2019 British Columbia Ferry Services Inc
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of British Columbia Ferry Services Inc("Confidential Information").
 * You shall not disclose such Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with British Columbia Ferry Services Inc.
 *
 * Last Modified: 06/06/19 20:21
 */

package com.bcf.bcfstorefrontaddon.forms;

import de.hybris.platform.commercefacades.accommodation.search.RoomStayCandidateData;
import java.util.List;


public class GetAQuoteForm
{
	private String firstName;
	private String lastName;
	private String email;
	private String confirmEmail;
	private String phoneNumber;
	private String postalCode;
	private boolean moreInfo;
	private String comments;
	private int numberOfRooms;
	private List<RoomStayCandidateData> roomStayCandidates;
	private String checkInDateTime;
	private String productName;

	public String getFirstName()
	{
		return firstName;
	}

	public void setFirstName(final String firstName)
	{
		this.firstName = firstName;
	}

	public String getLastName()
	{
		return lastName;
	}

	public void setLastName(final String lastName)
	{
		this.lastName = lastName;
	}

	public String getEmail()
	{
		return email;
	}

	public void setEmail(final String email)
	{
		this.email = email;
	}

	public String getConfirmEmail()
	{
		return confirmEmail;
	}

	public void setConfirmEmail(final String confirmEmail)
	{
		this.confirmEmail = confirmEmail;
	}

	public String getPhoneNumber()
	{
		return phoneNumber;
	}

	public void setPhoneNumber(final String phoneNumber)
	{
		this.phoneNumber = phoneNumber;
	}

	public String getPostalCode()
	{
		return postalCode;
	}

	public void setPostalCode(final String postalCode)
	{
		this.postalCode = postalCode;
	}

	public boolean isMoreInfo()
	{
		return moreInfo;
	}

	public void setMoreInfo(final boolean moreInfo)
	{
		this.moreInfo = moreInfo;
	}

	public String getComments()
	{
		return comments;
	}

	public void setComments(final String comments)
	{
		this.comments = comments;
	}

	public int getNumberOfRooms()
	{
		return numberOfRooms;
	}

	public void setNumberOfRooms(final int numberOfRooms)
	{
		this.numberOfRooms = numberOfRooms;
	}

	public String getCheckInDateTime()
	{
		return checkInDateTime;
	}

	public void setCheckInDateTime(final String checkInDateTime)
	{
		this.checkInDateTime = checkInDateTime;
	}

	public List<RoomStayCandidateData> getRoomStayCandidates()
	{
		return roomStayCandidates;
	}

	public void setRoomStayCandidates(
			final List<RoomStayCandidateData> roomStayCandidates)
	{
		this.roomStayCandidates = roomStayCandidates;
	}

	public String getProductName()
	{
		return productName;
	}

	public void setProductName(final String productName)
	{
		this.productName = productName;
	}
}
