package com.bcf.bcfstorefrontaddon.forms;

import java.util.List;


public class BusinessOpportunitiesSubscriptionsForm
{
	private String companyName;
	private String jobTitle;
	private List<java.lang.String> serviceSupplies;
	private boolean businessOpportunitiesOptedIn;

	public String getCompanyName()
	{
		return companyName;
	}

	public void setCompanyName(final String companyName)
	{
		this.companyName = companyName;
	}

	public String getJobTitle()
	{
		return jobTitle;
	}

	public void setJobTitle(final String jobTitle)
	{
		this.jobTitle = jobTitle;
	}

	public List<String> getServiceSupplies()
	{
		return serviceSupplies;
	}

	public void setServiceSupplies(final List<String> serviceSupplies)
	{
		this.serviceSupplies = serviceSupplies;
	}

	public boolean isBusinessOpportunitiesOptedIn()
	{
		return businessOpportunitiesOptedIn;
	}

	public void setBusinessOpportunitiesOptedIn(final boolean businessOpportunitiesOptedIn)
	{
		this.businessOpportunitiesOptedIn = businessOpportunitiesOptedIn;
	}
}
