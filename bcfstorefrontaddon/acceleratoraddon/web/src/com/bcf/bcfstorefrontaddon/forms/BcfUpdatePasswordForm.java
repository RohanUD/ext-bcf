/*
 * Copyright (c) 2019 British Columbia Ferry Services Inc
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of British Columbia Ferry Services Inc("Confidential Information").
 * You shall not disclose such Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with British Columbia Ferry Services Inc.
 *
 * Last Modified: 06/06/19 20:21
 */

package com.bcf.bcfstorefrontaddon.forms;

public class BcfUpdatePasswordForm
{

	private String uid;
	private String pwd;
	private String checkPwd;
	private String currentPassword;
	private String key;

	public void setUid(final String uid)
	{
		this.uid = uid;
	}

	public void setPwd(final String pwd)
	{
		this.pwd = pwd;
	}

	public void setCheckPwd(final String checkPwd)
	{
		this.checkPwd = checkPwd;
	}

	public void setKey(final String key)
	{
		this.key = key;
	}

	public String getUid()
	{
		return uid;
	}

	public String getPwd()
	{
		return pwd;
	}

	public String getCheckPwd()
	{
		return checkPwd;
	}

	public String getCurrentPassword()
	{
		return currentPassword;
	}

	public void setCurrentPassword(final String currentPassword)
	{
		this.currentPassword = currentPassword;
	}

	public String getKey()
	{
		return key;
	}


}
