/*
 * Copyright (c) 2019 British Columbia Ferry Services Inc
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of British Columbia Ferry Services Inc("Confidential Information").
 * You shall not disclose such Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with British Columbia Ferry Services Inc.
 *
 * Last Modified: 06/06/19 20:21
 */

package com.bcf.bcfstorefrontaddon.forms;

import java.util.List;
import com.bcf.core.paymnet.BcfPaymentOptionsData;


public class BcfPaymentOptionsForm
{
	private List<BcfPaymentOptionsData>  paymentOptions;

	public List<BcfPaymentOptionsData> getPaymentOptions()
	{
		return paymentOptions;
	}

	public void setPaymentOptions(final List<BcfPaymentOptionsData> paymentOptions)
	{
		this.paymentOptions = paymentOptions;
	}
}
