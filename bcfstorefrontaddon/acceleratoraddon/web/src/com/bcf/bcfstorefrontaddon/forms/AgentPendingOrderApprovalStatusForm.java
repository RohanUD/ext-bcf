/*
 * Copyright (c) 2019 British Columbia Ferry Services Inc
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of British Columbia Ferry Services Inc("Confidential Information").
 * You shall not disclose such Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with British Columbia Ferry Services Inc.
 *
 * Last Modified: 02/07/19 14:01
 */

package com.bcf.bcfstorefrontaddon.forms;

public class AgentPendingOrderApprovalStatusForm
{
	private String orderNumber;
	private String orderComments;
	private String orderAction;

	public String getOrderNumber()
	{
		return orderNumber;
	}

	public void setOrderNumber(final String orderNumber)
	{
		this.orderNumber = orderNumber;
	}

	public String getOrderComments()
	{
		return orderComments;
	}

	public void setOrderComments(final String orderComments)
	{
		this.orderComments = orderComments;
	}

	public String getOrderAction()
	{
		return orderAction;
	}

	public void setOrderAction(final String orderAction)
	{
		this.orderAction = orderAction;
	}
}
