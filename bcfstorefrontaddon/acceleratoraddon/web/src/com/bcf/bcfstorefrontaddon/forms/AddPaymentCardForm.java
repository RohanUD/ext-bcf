/*
 * Copyright (c) 2019 British Columbia Ferry Services Inc
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of British Columbia Ferry Services Inc("Confidential Information").
 * You shall not disclose such Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with British Columbia Ferry Services Inc.
 *
 * Last Modified: 06/06/19 20:21
 */

package com.bcf.bcfstorefrontaddon.forms;

import de.hybris.platform.acceleratorstorefrontcommons.forms.AddressForm;


public class AddPaymentCardForm
{
	private String cardType;
	private String cardNumberSuffix;
	private int expiryMonth;
	private int expiryYear;
	private String cardHolderFirstName;
	private String cardHolderLastName;
	private AddressForm addressForm;

	public String getCardType()
	{
		return cardType;
	}

	public void setCardType(final String cardType)
	{
		this.cardType = cardType;
	}

	public String getCardNumberSuffix()
	{
		return cardNumberSuffix;
	}

	public void setCardNumberSuffix(final String cardNumberSuffix)
	{
		this.cardNumberSuffix = cardNumberSuffix;
	}

	public int getExpiryMonth()
	{
		return expiryMonth;
	}

	public void setExpiryMonth(final int expiryMonth)
	{
		this.expiryMonth = expiryMonth;
	}

	public int getExpiryYear()
	{
		return expiryYear;
	}

	public void setExpiryYear(final int expiryYear)
	{
		this.expiryYear = expiryYear;
	}

	public String getCardHolderFirstName()
	{
		return cardHolderFirstName;
	}

	public void setCardHolderFirstName(final String cardHolderFirstName)
	{
		this.cardHolderFirstName = cardHolderFirstName;
	}

	public String getCardHolderLastName()
	{
		return cardHolderLastName;
	}

	public void setCardHolderLastName(final String cardHolderLastName)
	{
		this.cardHolderLastName = cardHolderLastName;
	}

	public AddressForm getAddressForm()
	{
		return addressForm;
	}

	public void setAddressForm(final AddressForm addressForm)
	{
		this.addressForm = addressForm;
	}
}
