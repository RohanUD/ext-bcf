/*
 * Copyright (c) 2019 British Columbia Ferry Services Inc
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of British Columbia Ferry Services Inc("Confidential Information").
 * You shall not disclose such Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with British Columbia Ferry Services Inc.
 *
 * Last Modified: 06/06/19 20:21
 */

package com.bcf.bcfstorefrontaddon.forms;

public class AgentPendingOrderDecisionForm
{
	private String orderCode;
	private String comments;
	private String buttonClicked;
	private String redirectEntry;
	private int entryNumber;

	public String getOrderCode()
	{
		return orderCode;
	}

	public void setOrderCode(final String orderCode)
	{
		this.orderCode = orderCode;
	}

	public String getComments()
	{
		return comments;
	}

	public void setComments(final String comments)
	{
		this.comments = comments;
	}

	public String getButtonClicked()
	{
		return buttonClicked;
	}

	public void setButtonClicked(final String buttonClicked)
	{
		this.buttonClicked = buttonClicked;
	}

	public String getRedirectEntry()
	{
		return redirectEntry;
	}

	public void setRedirectEntry(final String redirectEntry)
	{
		this.redirectEntry = redirectEntry;
	}

	public int getEntryNumber()
	{
		return entryNumber;
	}

	public void setEntryNumber(final int entryNumber)
	{
		this.entryNumber = entryNumber;
	}
}
