/*
 * Copyright (c) 2019 British Columbia Ferry Services Inc
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of British Columbia Ferry Services Inc("Confidential Information").
 * You shall not disclose such Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with British Columbia Ferry Services Inc.
 *
 * Last Modified: 06/06/19 20:21
 */

package com.bcf.bcfstorefrontaddon.forms;

import java.util.List;


public class ServiceNoticesSubscriptionsForm
{
	private List<String> routeCodes;
	private boolean serviceNoticesOptedIn;

	public List<String> getRouteCodes()
	{
		return routeCodes;
	}

	public void setRouteCodes(final List<String> routeCodes)
	{
		this.routeCodes = routeCodes;
	}

	public boolean isServiceNoticesOptedIn()
	{
		return serviceNoticesOptedIn;
	}

	public void setServiceNoticesOptedIn(final boolean serviceNoticesOptedIn)
	{
		this.serviceNoticesOptedIn = serviceNoticesOptedIn;
	}
}
