/*
 * Copyright (c) 2019 British Columbia Ferry Services Inc
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of British Columbia Ferry Services Inc("Confidential Information").
 * You shall not disclose such Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with British Columbia Ferry Services Inc.
 *
 * Last Modified: 06/06/19 20:21
 */

package com.bcf.bcfstorefrontaddon.interceptors.beforecontroller;

import de.hybris.platform.servicelayer.session.SessionService;
import java.util.Objects;
import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.springframework.web.servlet.handler.HandlerInterceptorAdapter;
import com.bcf.bcfstorefrontaddon.constants.BcfstorefrontaddonWebConstants;


/**
 * This interceptor is created only to remove payment transactions from the session as we are using them to display the
 * partial amount in reservation totals and reservation overlay totals components. Since reservation totals and
 * reservation overlay totals components are mapped to almost every page so it wouldn't be appropriate to display the
 * partial payment on pages where it doesn't make any sense
 */
public class RemoveTransactionsBeforeControllerInterceptor extends HandlerInterceptorAdapter
{

	@Resource(name = "sessionService")
	private SessionService sessionService;

	@Override
	public boolean preHandle(final HttpServletRequest request, final HttpServletResponse response, final Object handler)
	{
		if (Objects.nonNull(sessionService.getAttribute(BcfstorefrontaddonWebConstants.PAYMENT_TRANSACTIONS)))
		{
			sessionService.removeAttribute(BcfstorefrontaddonWebConstants.PAYMENT_TRANSACTIONS);
		}
		return true;
	}
}
