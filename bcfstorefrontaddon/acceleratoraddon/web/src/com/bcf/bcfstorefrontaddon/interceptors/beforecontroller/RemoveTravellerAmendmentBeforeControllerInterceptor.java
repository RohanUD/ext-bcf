/*
 * Copyright (c) 2019 British Columbia Ferry Services Inc
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of British Columbia Ferry Services Inc("Confidential Information").
 * You shall not disclose such Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with British Columbia Ferry Services Inc.
 *
 * Last Modified: 06/06/19 20:21
 */

package com.bcf.bcfstorefrontaddon.interceptors.beforecontroller;

import de.hybris.platform.servicelayer.session.SessionService;
import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.springframework.web.servlet.handler.HandlerInterceptorAdapter;
import com.bcf.bcfstorefrontaddon.constants.BcfstorefrontaddonWebConstants;


public class RemoveTravellerAmendmentBeforeControllerInterceptor extends HandlerInterceptorAdapter
{

	@Resource(name = "sessionService")
	private SessionService sessionService;

	@Override
	public boolean preHandle(final HttpServletRequest request, final HttpServletResponse response, final Object handler)
	{
		sessionService.removeAttribute(BcfstorefrontaddonWebConstants.AMEND_TRAVELLERS_JOURNEY_REF_NUM);
		sessionService.removeAttribute(BcfstorefrontaddonWebConstants.AMEND_TRAVELLERS_ORIGIN_DESTINATION_REF_NUM);
		return true;
	}
}
