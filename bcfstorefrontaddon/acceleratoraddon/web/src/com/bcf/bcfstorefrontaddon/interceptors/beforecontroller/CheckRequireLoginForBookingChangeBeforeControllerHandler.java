/*
 * Copyright (c) 2019 British Columbia Ferry Services Inc
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of British Columbia Ferry Services Inc("Confidential Information").
 * You shall not disclose such Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with British Columbia Ferry Services Inc.
 *
 * Last Modified: 06/06/19 20:21
 */

package com.bcf.bcfstorefrontaddon.interceptors.beforecontroller;

import de.hybris.platform.commerceservices.enums.CustomerType;
import de.hybris.platform.core.model.order.OrderModel;
import de.hybris.platform.core.model.user.CustomerModel;
import java.io.IOException;
import java.util.Objects;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Required;
import org.springframework.web.method.HandlerMethod;
import com.bcf.bcfcore.service.customer.BcfCustomerAccountService;
import com.bcf.bcfstorefrontaddon.annotations.CheckRequireLoginForBookingChange;
import com.bcf.core.service.BcfBookingService;


public class CheckRequireLoginForBookingChangeBeforeControllerHandler extends RequireHardLoginBeforeControllerHandler
{

	protected BcfBookingService bcfBookingService;
	private BcfCustomerAccountService customerAccountService;

	private static final Logger LOG = Logger.getLogger(CheckRequireLoginForBookingChangeBeforeControllerHandler.class);

	@Override
	public boolean beforeController(final HttpServletRequest request, final HttpServletResponse response,
			final HandlerMethod handler) throws Exception
	{
		// Check if the handler has our annotation
		final CheckRequireLoginForBookingChange annotation = findAnnotation(handler, CheckRequireLoginForBookingChange.class);
		if (annotation != null)
		{
			if (getUserService().isAnonymousUser(getUserService().getCurrentUser()) && isGuestOrderManagement(request))
			{
				return true;
			}
			else
			{
					return this.redirect(request, response);
			}
		}
		
		return true;
	}

	private boolean redirect(final HttpServletRequest request, final HttpServletResponse response) throws IOException
	{
		final String guid = (String) request.getSession().getAttribute(SECURE_GUID_SESSION_KEY);
		boolean redirect = true;

		if (((!getUserService().isAnonymousUser(getUserService().getCurrentUser()) || checkForAnonymousCheckout()
				|| manageMyBookingLogin()) && checkForGUIDCookie(request, response, guid)))
		{
			redirect = false;
		}

		if (redirect)
		{
			LOG.warn((guid == null ? "missing secure token in session" : "no matching guid cookie") + ", redirecting");
			getRedirectStrategy().sendRedirect(request, response, getRedirectUrl(request));
			return false;
		}

		return true;
	}

	private boolean isGuestOrderManagement(final HttpServletRequest request)
	{
		final String requestURI = request.getRequestURI();
		final String guestBookingIdentifier = request.getParameter("guestBookingIdentifier");
		try
		{
			// For order details page
			if (StringUtils.isNotEmpty(guestBookingIdentifier))
			{
				final OrderModel guestOrderModel = customerAccountService.getGuestOrderForGUID(guestBookingIdentifier);
				if (Objects.nonNull(guestOrderModel) && Objects
						.equals(CustomerType.GUEST, ((CustomerModel) guestOrderModel.getUser()).getType()))
				{
					return true;
				}
			}

			// For editing the guest orders
			final String orderCode = requestURI.split("/", 6)[4];
			if (StringUtils.isNotEmpty(orderCode))
			{
				final OrderModel order = getBcfBookingService().getOrder(orderCode);
				if (Objects.nonNull(order) && Objects.equals(CustomerType.GUEST, ((CustomerModel) order.getUser()).getType()))
				{
					return true;
				}
			}
		}
		catch (final ArrayIndexOutOfBoundsException aobEx)
		{
			LOG.error("Can not get the order code from request path " + requestURI);
			return true;
		}

		return false;
	}

	protected BcfBookingService getBcfBookingService()
	{
		return bcfBookingService;
	}

	@Required
	public void setBcfBookingService(final BcfBookingService bcfBookingService)
	{
		this.bcfBookingService = bcfBookingService;
	}

	protected BcfCustomerAccountService getCustomerAccountService()
	{
		return customerAccountService;
	}

	@Required
	public void setCustomerAccountService(final BcfCustomerAccountService customerAccountService)
	{
		this.customerAccountService = customerAccountService;
	}
}
