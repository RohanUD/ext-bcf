/*
 * Copyright (c) 2019 British Columbia Ferry Services Inc
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of British Columbia Ferry Services Inc("Confidential Information").
 * You shall not disclose such Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with British Columbia Ferry Services Inc.
 *
 * Last Modified: 06/06/19 20:21
 */

package com.bcf.bcfstorefrontaddon.interceptors.beforecontroller;

import de.hybris.platform.commercefacades.storesession.StoreSessionFacade;
import de.hybris.platform.servicelayer.session.SessionService;
import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.springframework.web.servlet.handler.HandlerInterceptorAdapter;
import com.bcf.bcfstorefrontaddon.constants.BcfstorefrontaddonWebConstants;


/**
 * Below Interceptor is created to store the current currency as a previous currency in a session immediately before it
 * gets updated to the new one so that it can be used to clear the facets for search on currency change event
 */

public class CurrencyUpdateBeforeControllerHandler extends HandlerInterceptorAdapter
{

	@Resource(name = "sessionService")
	private SessionService sessionService;

	@Resource(name = "storeSessionFacade")
	private StoreSessionFacade storeSessionFacade;

	@Override
	public boolean preHandle(final HttpServletRequest request, final HttpServletResponse response, final Object handler)
	{
		sessionService.setAttribute(BcfstorefrontaddonWebConstants.SESSION_PREVIOUS_CURRENCY,
				storeSessionFacade.getCurrentCurrency().getIsocode());
		return true;
	}

}
