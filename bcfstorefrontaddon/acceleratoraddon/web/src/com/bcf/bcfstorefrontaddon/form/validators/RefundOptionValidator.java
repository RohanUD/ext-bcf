/*
 * [y] hybris Platform
 *
 * Copyright (c) 2000-2016 SAP SE
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of SAP
 * Hybris ("Confidential Information"). You shall not disclose such
 * Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with SAP Hybris.
 *
 *
 */

package com.bcf.bcfstorefrontaddon.form.validators;

import java.math.BigDecimal;
import java.util.List;
import javax.annotation.Resource;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang3.tuple.Pair;
import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.Validator;
import com.bcf.bcfstorefrontaddon.forms.RefundOptionForm;
import com.bcf.bcfstorefrontaddon.forms.RefundSelectionForm;
import com.bcf.core.payment.data.RefundTransactionInfoData;
import com.bcf.facades.helper.BcfTravelCartFacadeHelper;


@Component("refundOptionValidator")
public class RefundOptionValidator implements Validator
{
	@Resource(name = "bcfTravelCartFacadeHelper")
	private BcfTravelCartFacadeHelper bcfTravelCartFacadeHelper;

	protected static final String REFUND_COST_AMOUNT_ERROR = "refund.amount.invalid";
	protected static final String REFUND_COST_MAX_AMOUNT_ERROR = "refund.amount.maximum";
	protected static final String REFUND_COST_TOTAL_AMOUNT_ERROR = "refund.total.amount.error";
	protected static final String REFUND_COST_TOTAL_AMOUNT_ZERO_ERROR = "refund.total.amount.zero.error";

	@Override
	public boolean supports(final Class<?> aClass)
	{
		return RefundOptionForm.class.equals(aClass);
	}

	@Override
	public void validate(final Object object, final Errors errors)
	{

		final RefundOptionForm refundOptionForm = (RefundOptionForm) object;

		Pair<Double,List<RefundTransactionInfoData>> refundTransactionInfosPair = bcfTravelCartFacadeHelper.getRefundTransactionInfo(refundOptionForm.getBookingReference());

		if (refundTransactionInfosPair!=null)
		{

			List<RefundTransactionInfoData> refundTransactionInfos=refundTransactionInfosPair.getRight();
			Double maxRefundAmount=refundTransactionInfosPair.getLeft();
			List<RefundSelectionForm> refundSelections = refundOptionForm.getRefundSelectionForms();

			if (CollectionUtils.isNotEmpty(refundSelections))
			{
				validateRefundSelections(refundSelections, refundTransactionInfos,errors);
				if(!errors.hasErrors()){
					double totalRequestedRefund=	refundSelections.stream().filter(refundSelection->StringUtils.isNotEmpty(refundSelection.getAmount())).mapToDouble(refundSelection->Double.valueOf(refundSelection.getAmount())).sum();
					if(BigDecimal.valueOf(totalRequestedRefund).compareTo(BigDecimal.valueOf(maxRefundAmount))>0){
						errors.reject(REFUND_COST_TOTAL_AMOUNT_ERROR);
					}else if (BigDecimal.valueOf(totalRequestedRefund).compareTo(BigDecimal.ZERO)==0){
						errors.reject(REFUND_COST_TOTAL_AMOUNT_ZERO_ERROR);
					}
				}
			}
		}
	}

	public static boolean isDouble(String value) {
		try {
			Double.parseDouble(value);
			return true;
		} catch (NumberFormatException e) {
			return false;
		}
	}
	private void validateRefundSelections(final List<RefundSelectionForm> refundSelections, final List<RefundTransactionInfoData> refundTransactionInfos,final Errors errors)
	{
		for (int i = 0; i < refundSelections.size(); i++)
		{
			RefundSelectionForm refundSelectionForm = refundSelections.get(i);
			if (StringUtils.isNotEmpty(refundSelectionForm.getAmount()))
			{
				if (!isDouble(refundSelectionForm.getAmount())){

					errors.reject(REFUND_COST_AMOUNT_ERROR, new String[]
							{ refundSelectionForm.getRefundType()}, REFUND_COST_AMOUNT_ERROR);

				}else{
					RefundTransactionInfoData refundTransactionInfoData=	refundTransactionInfos.stream().filter(refundTransactionInfo->refundTransactionInfo.getCode().equals(refundSelectionForm.getTransactionCode())).findAny().orElse(null);

					if(refundTransactionInfoData!=null && BigDecimal.valueOf(Double.valueOf(refundSelectionForm.getAmount())).compareTo(BigDecimal.valueOf(refundTransactionInfoData.getMaxAmount()))>0){

						errors.reject(REFUND_COST_MAX_AMOUNT_ERROR, new String[]
								{ refundSelectionForm.getRefundType()}, REFUND_COST_MAX_AMOUNT_ERROR);
					}
				}
			}
		}
	}


}
