/*
 * Copyright (c) 2019 British Columbia Ferry Services Inc
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of British Columbia Ferry Services Inc("Confidential Information").
 * You shall not disclose such Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with British Columbia Ferry Services Inc.
 *
 * Last Modified: 21/06/19 00:21
 */

package com.bcf.bcfstorefrontaddon.form.validators;

import java.util.Arrays;
import java.util.List;
import java.util.Objects;
import javax.annotation.Resource;
import org.apache.commons.lang.StringUtils;
import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import com.bcf.bcfstorefrontaddon.forms.BCFUpdateProfileForm;
import com.bcf.core.bcfproperties.BcfConfigurablePropertiesService;
import com.bcf.core.constants.BcfCoreConstants;
import com.bcf.core.enums.AccountType;


@Component("bcfProfileValidator")
public class BcfProfileValidator extends BaseValidator
{
	@Resource(name = "bcfRegistrationValidator")
	private BCFRegistrationValidator bcfRegistrationValidator;

	@Resource(name = "bcfConfigurablePropertiesService")
	private BcfConfigurablePropertiesService bcfConfigurablePropertiesService;

	@Override
	public boolean supports(final Class<?> aClass)
	{
		return BcfProfileValidator.class.equals(aClass);
	}

	@Override
	public void validate(final Object object, final Errors errors)
	{
		if(object instanceof BCFUpdateProfileForm){
		final BCFUpdateProfileForm profileForm = (BCFUpdateProfileForm) object;

		validateField("firstName", profileForm.getFirstName(), errors);
		validateField("lastName", profileForm.getLastName(), errors);

		validateAddressFieldsForAccountType(profileForm, errors);}
	}

	private void validateAddressFieldsForAccountType(final BCFUpdateProfileForm registerForm, final Errors errors)
	{
		if (StringUtils.isNotEmpty(registerForm.getAccountType()) && registerForm.getAccountType()
				.equals(AccountType.FULL_ACCOUNT.getCode()))
		{
			final String phoneNumber = registerForm.getPhoneNumber();
			final String zipCode = registerForm.getZipCode();
			final String country = registerForm.getCountry();
			final String province = registerForm.getRegion();

			validateField("phoneNumber", phoneNumber, errors);
			if (Objects.nonNull(phoneNumber) && phoneNumber.length() < 10)
			{
				errors.rejectValue("phoneNumber", "register.phoneNumber.invalid.length");
			}
			validateField("country", country, errors);
			validateField("zipCode", zipCode, errors);

			final String regionsRequiredForCountries = bcfConfigurablePropertiesService
					.getBcfPropertyValue(BcfCoreConstants.REGIONS_REQUIRED_FOR_COUNTRIES);
			if (StringUtils.isNotEmpty(regionsRequiredForCountries))
			{
				final List<String> regionCountries = Arrays.asList(regionsRequiredForCountries.split(","));
				if (regionCountries.contains(country))
				{
					if (Objects.isNull(province) || StringUtils.equals(province, "-1"))
					{
						errors.rejectValue("region", "profile.region.invalid");
					}
				}
			}

			final String address = registerForm.getAddress();
			final String city = registerForm.getCity();

			if (StringUtils.isNotEmpty(address))
			{
				validateField("city", city, errors);
			}

			if (StringUtils.isNotEmpty(city))
			{
				validateField("address", address, errors);
			}
		}
	}
}
