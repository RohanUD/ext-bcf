/*
 * Copyright (c) 2019 British Columbia Ferry Services Inc
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of British Columbia Ferry Services Inc("Confidential Information").
 * You shall not disclose such Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with British Columbia Ferry Services Inc.
 *
 * Last Modified: 06/06/19 20:21
 */

package com.bcf.bcfstorefrontaddon.form.validators;

import de.hybris.platform.acceleratorstorefrontcommons.forms.validation.RegistrationValidator;
import java.util.Arrays;
import java.util.List;
import java.util.Objects;
import javax.annotation.Resource;
import org.apache.commons.lang.StringUtils;
import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import com.bcf.bcfstorefrontaddon.forms.UpgradeAccountForm;
import com.bcf.core.bcfproperties.BcfConfigurablePropertiesService;
import com.bcf.core.constants.BcfCoreConstants;


@Component("upgradeAccountValidator")
public class UpgradeAccountValidator extends RegistrationValidator
{

	@Resource(name = "bcfConfigurablePropertiesService")
	private BcfConfigurablePropertiesService bcfConfigurablePropertiesService;

	@Override
	public boolean supports(final Class<?> aClass)
	{
		return UpgradeAccountForm.class.equals(aClass);
	}

	@Override
	public void validate(final Object object, final Errors errors)
	{
		final UpgradeAccountForm registerForm = (UpgradeAccountForm) object;

		final String phoneNumber = registerForm.getMobileNumber();
		final String zipCode = registerForm.getZipCode();
		final String country = registerForm.getCountry();
		final String province = registerForm.getProvince();
		final String city = registerForm.getCity();

		validateName(errors, phoneNumber, "mobileNumber", "register.mobileNumber.invalid");
		validateName(errors, country, "country", "register.country.invalid");
		validateName(errors, zipCode, "zipCode", "register.zipCode.invalid");
		validateName(errors, city, "city", "register.city.invalid");

		final String regionsRequiredForCountries = bcfConfigurablePropertiesService
				.getBcfPropertyValue(BcfCoreConstants.REGIONS_REQUIRED_FOR_COUNTRIES);
		if (StringUtils.isNotEmpty(regionsRequiredForCountries))
		{
			final List<String> regionCountries = Arrays.asList(regionsRequiredForCountries.split(","));
			if (regionCountries.contains(country))
			{
				if (Objects.isNull(province) || StringUtils.equals(province, "-1"))
				{
					errors.rejectValue("province", "register.province.invalid");
				}
			}
		}
	}
}
