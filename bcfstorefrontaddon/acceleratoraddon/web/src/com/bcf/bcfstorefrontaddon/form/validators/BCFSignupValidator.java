/*
 * Copyright (c) 2019 British Columbia Ferry Services Inc
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of British Columbia Ferry Services Inc("Confidential Information").
 * You shall not disclose such Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with British Columbia Ferry Services Inc.
 *
 * Last Modified: 21/06/19 00:21
 */

package com.bcf.bcfstorefrontaddon.form.validators;

import de.hybris.platform.acceleratorstorefrontcommons.forms.validation.RegistrationValidator;
import java.util.Arrays;
import java.util.List;
import java.util.Objects;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Required;
import org.springframework.validation.Errors;
import com.bcf.bcfstorefrontaddon.forms.BCFSignupForm;
import com.bcf.core.bcfproperties.BcfConfigurablePropertiesService;
import com.bcf.core.constants.BcfCoreConstants;


public class BCFSignupValidator extends RegistrationValidator
{
	public static final Pattern ATLEAST_ONE_NUMBER_REGEX = Pattern.compile("[0-9]");
	public static final Pattern ATLEAST_ONE_UPPERCASE_LETTER_REGEX = Pattern.compile("[A-Z]");
	public static final Pattern ATLEAST_ONE_LOWERCASE_LETTER_REGEX = Pattern.compile("[a-z]");
	public static final Pattern ATLEAST_ONE_SPECIAL_SYMBOL_REGEX = Pattern.compile("[^A-Za-z0-9\\s]");
	private static final String REGISTER_PWD_INVALID = "register.pwd.invalid";

	private BcfConfigurablePropertiesService bcfConfigurablePropertiesService;

	@Override
	public boolean supports(final Class<?> aClass)
	{
		return BCFSignupForm.class.equals(aClass);
	}

	@Override
	public void validate(final Object object, final Errors errors)
	{
		final BCFSignupForm signupForm = (BCFSignupForm) object;
		final String email = signupForm.getEmail();
		final String checkEmail = signupForm.getCheckEmail();
		final String pwd = signupForm.getPwd();
		final String checkPwd = signupForm.getCheckPwd();
		final String firstName = signupForm.getFirstName();
		final String lastName = signupForm.getLastName();
		final String accountType = signupForm.getAccountType();
		final String country = signupForm.getCountry();
		final String userGroup = signupForm.getUserGroup();

		validateEmail(errors, email);
		compareEmails(errors, email, checkEmail);

		if (BcfCoreConstants.FULL_ACCOUNT.equals(accountType))
		{
			validateName(firstName, lastName, errors);
			validateName(errors, accountType, "accountType", "register.accountType.invalid");
			validateName(errors, country, "country", "register.country.invalid");
			validateName(errors, userGroup, "userGroup", "register.usergroup.invalid");

			final String phoneNumber = signupForm.getPhoneNo();
			validateName(errors, phoneNumber, "phoneNo", "register.phoneNo.invalid");
			if (Objects.nonNull(phoneNumber) && phoneNumber.length() < 10)
			{
				errors.rejectValue("phoneNo", "register.phoneNo.invalid.length");
			}

			final String regionsRequiredForCountries = getBcfConfigurablePropertiesService()
					.getBcfPropertyValue(BcfCoreConstants.REGIONS_REQUIRED_FOR_COUNTRIES);
			if (StringUtils.isNotEmpty(regionsRequiredForCountries))
			{
				final List<String> regionCountries = Arrays.asList(regionsRequiredForCountries.split(","));
				if (regionCountries.contains(country))
				{
					final String province = signupForm.getProvince();
					validateName(errors, province, "province", "register.province.invalid");
					final String zipCode = signupForm.getZipCode();
					validateName(errors, zipCode, "zipCode", "register.zipCode.invalid");
				}
			}
		}
		else if (BcfCoreConstants.SUBSCRIPTION_ONLY.equals(accountType))
		{
			validateName(firstName, lastName, errors);
			validateName(errors, userGroup, "userGroup", "register.usergroup.invalid");
		}
		else
		{
			validateSpecialCharacters(errors, email);
			PasswordValidatorUtil.validatePassword(errors, pwd);
			PasswordValidatorUtil.comparePasswords(errors, pwd, checkPwd);
		}
	}

	private void validateSpecialCharacters(final Errors errors, final String email)
	{
		final String regex = "^[A-Za-z0-9+_.-]+@(.+)$";
		final Pattern pattern = Pattern.compile(regex);
		final Matcher matcher = pattern.matcher(email);
		if (StringUtils.isNotEmpty(email) && !matcher.find())
		{
			errors.rejectValue("email", "register.email.invalid");
		}
	}

	private void validateName(final String firstName, final String lastName, final Errors errors)
	{
		validateName(errors, firstName, "firstName", "register.firstName.invalid");
		validateName(errors, lastName, "lastName", "register.lastName.invalid");

		if (StringUtils.length(firstName) + StringUtils.length(lastName) > Integer
				.parseInt(getBcfConfigurablePropertiesService().getBcfPropertyValue("nameValidationLength")))
		{
			errors.rejectValue("lastName", "register.name.invalid");
			errors.rejectValue("firstName", "register.name.invalid");
		}
	}

	public BcfConfigurablePropertiesService getBcfConfigurablePropertiesService()
	{
		return bcfConfigurablePropertiesService;
	}

	private void compareEmails(final Errors errors, final String email, final String checkEmail)
	{
		if (StringUtils.isNotEmpty(email) && StringUtils.isNotEmpty(checkEmail) && !StringUtils.equals(email, checkEmail))
		{
			errors.rejectValue("checkEmail", "validation.checkEmail.equals");
		}
		else
		{
			if (StringUtils.isEmpty(checkEmail))
			{
				errors.rejectValue("checkEmail", "register.checkEmail.invalid");
			}
		}
	}

	@Required
	public void setBcfConfigurablePropertiesService(
			final BcfConfigurablePropertiesService bcfConfigurablePropertiesService)
	{
		this.bcfConfigurablePropertiesService = bcfConfigurablePropertiesService;
	}
}
