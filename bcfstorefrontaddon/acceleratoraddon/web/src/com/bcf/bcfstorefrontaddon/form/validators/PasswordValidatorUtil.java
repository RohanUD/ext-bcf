package com.bcf.bcfstorefrontaddon.form.validators;

import java.util.Objects;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import org.apache.commons.lang.StringUtils;
import org.springframework.validation.Errors;


public class PasswordValidatorUtil
{
	public static final Pattern ATLEAST_ONE_NUMBER_REGEX = Pattern.compile("[0-9]");
	public static final Pattern ATLEAST_ONE_UPPERCASE_LETTER_REGEX = Pattern.compile("[A-Z]");
	public static final Pattern ATLEAST_ONE_LOWERCASE_LETTER_REGEX = Pattern.compile("[a-z]");
	public static final Pattern ATLEAST_ONE_SPECIAL_SYMBOL_REGEX = Pattern.compile("[^A-Za-z0-9\\s]");

	private static final String REGISTER_PWD_INVALID = "register.pwd.invalid";

	public static void validatePassword(final Errors errors, final String pwd)
	{

		if (StringUtils.isEmpty(pwd) || StringUtils.length(pwd) < 8 || StringUtils.length(pwd) > 32)
		{
			errors.rejectValue("pwd", REGISTER_PWD_INVALID);
		}
		else
		{
			int count = 4;
			if (!upperCaseFound(pwd))
			{
				count--;
			}
			if (!lowerCaseFound(pwd))
			{
				count--;
			}
			if (count < 3)
			{
				errors.rejectValue("pwd", REGISTER_PWD_INVALID);
			}
			else if (!numberFound(pwd))
			{
				count--;
			}
			if (count < 3)
			{
				errors.rejectValue("pwd", REGISTER_PWD_INVALID);
			}
			else if (!specialSymbolFound(pwd))
			{
				count--;
			}

			if (count < 3)
			{
				errors.rejectValue("pwd", REGISTER_PWD_INVALID);
			}
		}
	}

	public static void comparePasswords(final Errors errors, final String pwd, final String checkPwd)
	{
		if (StringUtils.isNotEmpty(pwd) && StringUtils.isNotEmpty(checkPwd) && !StringUtils.equals(pwd, checkPwd))
		{
			errors.rejectValue("checkPwd", "validation.checkPwd.equals");
		}
		else
		{
			if (StringUtils.isEmpty(checkPwd))
			{
				errors.rejectValue("checkPwd", "register.checkPwd.invalid");
			}
		}
	}

	private static boolean specialSymbolFound(final String pwd)
	{
		final Matcher matcher = ATLEAST_ONE_SPECIAL_SYMBOL_REGEX.matcher(pwd);
		return matcher.find();
	}

	private static boolean numberFound(final String pwd)
	{
		final Matcher matcher = ATLEAST_ONE_NUMBER_REGEX.matcher(pwd);
		return matcher.find();
	}

	private static boolean lowerCaseFound(final String pwd)
	{
		final Matcher matcher = ATLEAST_ONE_LOWERCASE_LETTER_REGEX.matcher(pwd);
		return matcher.find();
	}

	private static boolean upperCaseFound(final String pwd)
	{
		final Matcher matcher = ATLEAST_ONE_UPPERCASE_LETTER_REGEX.matcher(pwd);
		return matcher.find();
	}


	public static int checkString(final String input) {
		int matchNumber=0;
		final String specialChars = "~`!@#$%^&*()-_=+\\|[{]};:'\",<.>/?";
		char currentCharacter;
		boolean numberPresent = false;
		boolean upperCasePresent = false;
		boolean lowerCasePresent = false;
		boolean specialCharacterPresent = false;

		for (int i = 0; i < input.length(); i++) {
			currentCharacter = input.charAt(i);
			if (Character.isDigit(currentCharacter)) {
				numberPresent = true;
			} else if (Character.isUpperCase(currentCharacter)) {
				upperCasePresent = true;
			} else if (Character.isLowerCase(currentCharacter)) {
				lowerCasePresent = true;
			} else if (specialChars.contains(String.valueOf(currentCharacter))) {
				specialCharacterPresent = true;
			}
		}

		if(numberPresent) {
			matchNumber++;
		}
		if(upperCasePresent) {
			matchNumber++;
		}

		if(lowerCasePresent) {
			matchNumber++;
		}
		if(specialCharacterPresent) {
			matchNumber++;
		}

		return matchNumber;
	}

	public static void checkContainsName(final Errors errors, final String firstName,
			final String lastName, final String newPasswd)
	{
		if(Objects.nonNull(firstName) && Objects.nonNull (lastName) && Objects.nonNull(newPasswd) && (newPasswd.toLowerCase().contains(firstName.toLowerCase()) || newPasswd.toLowerCase().contains(lastName.toLowerCase()))){
			errors.rejectValue("pwd", "password.contains.name");
		}
	}
}
