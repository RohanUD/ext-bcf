/*
 * [y] hybris Platform
 *
 * Copyright (c) 2017 SAP SE or an SAP affiliate company.  All rights reserved.
 *
 * This software is the confidential and proprietary information of SAP
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with SAP.
 */
package com.bcf.bcfstorefrontaddon.form.validators;

import de.hybris.platform.core.model.user.CustomerModel;
import de.hybris.platform.servicelayer.user.UserService;
import de.hybris.platform.util.Config;
import javax.annotation.Resource;
import org.apache.commons.lang.StringUtils;
import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.Validator;
import com.bcf.bcfstorefrontaddon.forms.BcfUpdatePasswordForm;


/**
 * Validator for password forms.
 */
@Component("bcfPasswordValidator")
public class BcfPasswordValidator implements Validator
{
	private static final String UPDATE_PWD_INVALID = "updatePwd.pwd.invalid";

	@Resource(name = "userService")
	private UserService userService;

	@Override
	public boolean supports(final Class<?> aClass)
	{
		return BcfPasswordValidator.class.equals(aClass);
	}

	@Override
	public void validate(final Object object, final Errors errors)
	{
		final int minLength = Config.getInt("password.strength.minchar", 8);
		final int maxLength = Config.getInt("password.strength.maxchar", 32);

		final BcfUpdatePasswordForm passwordForm = (BcfUpdatePasswordForm) object;
		final String currPasswd = passwordForm.getCurrentPassword();
		final String newPasswd = passwordForm.getPwd();
		final String checkPasswd = passwordForm.getCheckPwd();

		if (StringUtils.isEmpty(currPasswd))
		{
			errors.rejectValue("currentPassword", "profile.currentPassword.invalid");
		}

		final CustomerModel currentUser = (CustomerModel) userService.getCurrentUser();
		final String firstName = currentUser.getFirstName();
		final String lastName = currentUser.getLastName();

		PasswordValidatorUtil.validatePassword(errors, newPasswd);
		PasswordValidatorUtil.comparePasswords(errors, newPasswd, checkPasswd);
		PasswordValidatorUtil.checkContainsName(errors, firstName, lastName, newPasswd);
	}
}
