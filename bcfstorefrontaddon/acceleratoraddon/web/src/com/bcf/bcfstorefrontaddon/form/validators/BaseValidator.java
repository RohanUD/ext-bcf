/*
 * Copyright (c) 2019 British Columbia Ferry Services Inc
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of British Columbia Ferry Services Inc("Confidential Information").
 * You shall not disclose such Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with British Columbia Ferry Services Inc.
 *
 * Last Modified: 06/06/19 20:21
 */

package com.bcf.bcfstorefrontaddon.form.validators;

import de.hybris.platform.acceleratorstorefrontcommons.forms.validation.EmailValidator;
import java.util.regex.Pattern;
import org.apache.commons.lang.StringUtils;
import org.springframework.validation.Errors;
import org.springframework.validation.Validator;


public abstract class BaseValidator implements Validator
{

	private static final Pattern PHONE_NUMBER_REGEX = Pattern.compile("(^$|[0-9]{10})");
	private static final String PROFILE = "profile.";
	private static final String INVALID = ".invalid";

	protected void validateField(final String fieldName, final String fieldValue, final Errors errors)
	{
		if (StringUtils.isEmpty(fieldValue) || StringUtils.length(fieldValue) > 255)
		{
			errors.rejectValue(fieldName, PROFILE + fieldName + INVALID);
		}
	}

	protected void validatePhoneNumber(final String fieldName, final String fieldValue, final Errors errors)
	{
		if (StringUtils.isEmpty(fieldValue) || StringUtils.length(fieldValue) > 255 || !BaseValidator.PHONE_NUMBER_REGEX.matcher(fieldValue).matches())
		{
			errors.rejectValue(fieldName, PROFILE+fieldName+INVALID);
		}
	}

	protected void validateEmail(final String fieldName, final String fieldValue, final Errors errors) {
		if (StringUtils.isEmpty(fieldValue) || StringUtils.length(fieldValue) > 255 || !EmailValidator.EMAIL_REGEX.matcher(fieldValue).matches())
		{
			errors.rejectValue(fieldName, PROFILE+fieldName+INVALID);
		}
	}
}
