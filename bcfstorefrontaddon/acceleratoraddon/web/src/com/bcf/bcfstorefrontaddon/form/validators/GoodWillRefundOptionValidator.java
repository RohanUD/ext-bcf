/*
 * [y] hybris Platform
 *
 * Copyright (c) 2000-2016 SAP SE
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of SAP
 * Hybris ("Confidential Information"). You shall not disclose such
 * Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with SAP Hybris.
 *
 *
 */

package com.bcf.bcfstorefrontaddon.form.validators;

import java.math.BigDecimal;
import java.util.Arrays;
import java.util.List;
import javax.annotation.Resource;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang.StringUtils;
import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.Validator;
import com.bcf.bcfstorefrontaddon.forms.RefundOptionForm;
import com.bcf.bcfstorefrontaddon.forms.RefundSelectionForm;
import com.bcf.core.payment.data.RefundTransactionInfoData;
import com.bcf.core.service.BCFGoodWillRefundService;
import com.bcf.facades.constants.BcfFacadesConstants;
import com.bcf.facades.helper.BcfTravelCartFacadeHelper;


@Component("goodWillRefundOptionValidator")
public class GoodWillRefundOptionValidator implements Validator
{
	@Resource(name = "bcfTravelCartFacadeHelper")
	private BcfTravelCartFacadeHelper bcfTravelCartFacadeHelper;

	@Resource(name = "bcfGoodWillRefundService")
	private BCFGoodWillRefundService bcfGoodWillRefundService;

	protected static final String REFUND_COST_AMOUNT_ERROR = "refund.amount.invalid";
	protected static final String REFUND_COST_MAX_AMOUNT_ERROR = "refund.amount.maximum";
	protected static final String REFUND_COST_TOTAL_AMOUNT_ERROR = "refund.total.amount.error";
	protected static final String REFUND_COST_TOTAL_AMOUNT_ZERO_ERROR = "refund.total.amount.zero.error";
	protected static final String GOODWILL_REFUND_THRESHOLD_ERROR = "text.goodwill.refund.threshold.error";

	@Override
	public boolean supports(final Class<?> aClass)
	{
		return RefundOptionForm.class.equals(aClass);
	}

	@Override
	public void validate(final Object object, final Errors errors)
	{

		final RefundOptionForm refundOptionForm = (RefundOptionForm) object;

		 List<RefundTransactionInfoData> refundTransactionInfos = bcfTravelCartFacadeHelper.getGoodWillRefundTransactionInfo(refundOptionForm.getBookingReference());

		List<RefundSelectionForm> refundSelections = refundOptionForm.getRefundSelectionForms();

		if (refundTransactionInfos!=null)
		{

			if (CollectionUtils.isNotEmpty(refundSelections))
			{
				validateRefundSelections(refundSelections, refundTransactionInfos,errors);
				if(!errors.hasErrors()){
					double totalRequestedRefund=	refundSelections.stream().filter(refundSelection->StringUtils.isNotEmpty(refundSelection.getAmount())).mapToDouble(refundSelection->Double.valueOf(refundSelection.getAmount())).sum();
					 if (BigDecimal.valueOf(totalRequestedRefund).compareTo(BigDecimal.ZERO)==0){
						errors.reject(REFUND_COST_TOTAL_AMOUNT_ZERO_ERROR);
					}
				}
			}
		}

		if(!errors.hasErrors() && StringUtils.isNotBlank(refundOptionForm.getGoodWillRefundCode())){

			Double  thresholdValue = bcfGoodWillRefundService.getGoodWillRefund(refundOptionForm.getGoodWillRefundCode()).getThresholdValue();

			if(thresholdValue!=null){

				Double refundAmount=refundSelections.stream().filter(refundTransactionInfoData->StringUtils.isNotBlank(refundTransactionInfoData.getAmount())).mapToDouble(refundTransactionInfoData->Double.valueOf(refundTransactionInfoData.getAmount())).sum();

				if (refundAmount.compareTo(thresholdValue) > 0)
				{
					errors.reject(GOODWILL_REFUND_THRESHOLD_ERROR, new String[]
							{ thresholdValue.toString()}, GOODWILL_REFUND_THRESHOLD_ERROR);
				}
			}

		}

	}

	public static boolean isDouble(String value) {
		try {
			Double.parseDouble(value);
			return true;
		} catch (NumberFormatException e) {
			return false;
		}
	}
	private void validateRefundSelections(final List<RefundSelectionForm> refundSelections, final List<RefundTransactionInfoData> refundTransactionInfos,final Errors errors)
	{
		for (int i = 0; i < refundSelections.size(); i++)
		{
			RefundSelectionForm refundSelectionForm = refundSelections.get(i);
			if (StringUtils.isNotEmpty(refundSelectionForm.getAmount()))
			{
				if (!isDouble(refundSelectionForm.getAmount())){

					errors.reject(REFUND_COST_AMOUNT_ERROR, new String[]
							{ refundSelectionForm.getRefundType()}, REFUND_COST_AMOUNT_ERROR);
				}
				else{
					RefundTransactionInfoData refundTransactionInfoData=	refundTransactionInfos.stream().filter(refundTransactionInfo->refundTransactionInfo.getCode().equals(refundSelectionForm.getTransactionCode())).findAny().orElse(null);

					if(refundTransactionInfoData!=null && Arrays.asList(BcfFacadesConstants.CREDIT_CARD,BcfFacadesConstants.DEBIT_CARD,BcfFacadesConstants.CARD_WTH_PINPAD).contains(refundTransactionInfoData.getRefundType()) && BigDecimal.valueOf(Double.valueOf(refundSelectionForm.getAmount())).compareTo(BigDecimal.valueOf(refundTransactionInfoData.getMaxAmount()))>0){

						errors.reject(REFUND_COST_MAX_AMOUNT_ERROR, new String[]
								{ refundSelectionForm.getRefundType()}, REFUND_COST_MAX_AMOUNT_ERROR);
					}
				}
			}
		}
	}


}
