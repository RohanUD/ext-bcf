/*
 * Copyright (c) 2019 British Columbia Ferry Services Inc
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of British Columbia Ferry Services Inc("Confidential Information").
 * You shall not disclose such Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with British Columbia Ferry Services Inc.
 *
 * Last Modified: 02/07/19 14:01
 */

package com.bcf.bcfstorefrontaddon.form.validators;

import de.hybris.platform.acceleratorstorefrontcommons.forms.RegisterForm;
import de.hybris.platform.acceleratorstorefrontcommons.forms.validation.RegistrationValidator;
import java.util.Arrays;
import java.util.List;
import java.util.Objects;
import javax.annotation.Resource;
import org.apache.commons.lang.StringUtils;
import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import com.bcf.bcfstorefrontaddon.forms.BCFRegisterForm;
import com.bcf.core.bcfproperties.BcfConfigurablePropertiesService;
import com.bcf.core.constants.BcfCoreConstants;
import com.bcf.core.enums.AccountType;


@Component("bcfRegistrationValidator")
public class BCFRegistrationValidator extends RegistrationValidator
{
	private static final String ADDRESS_LINE1_INVALID = "register.addressLine1.invalid";

	@Resource(name = "bcfConfigurablePropertiesService")
	private BcfConfigurablePropertiesService bcfConfigurablePropertiesService;

	@Override
	public boolean supports(final Class<?> aClass)
	{
		return RegisterForm.class.equals(aClass);
	}

	@Override
	public void validate(final Object object, final Errors errors)
	{
		final BCFRegisterForm registerForm = (BCFRegisterForm) object;
		final String accountType = registerForm.getAccountType();
		final String firstName = registerForm.getFirstName();
		final String lastName = registerForm.getLastName();

		validateName(errors, firstName, "firstName", "register.firstName.invalid");
		validateName(errors, lastName, "lastName", "register.lastName.invalid");
		if (StringUtils.length(firstName) + StringUtils.length(lastName) > 255)
		{
			errors.rejectValue("lastName", "register.name.invalid");
			errors.rejectValue("firstName", "register.name.invalid");
		}

		validateName(errors, accountType, "accountType", "register.accountType.invalid");
		if (StringUtils.isNotEmpty(accountType) && accountType.equals(AccountType.FULL_ACCOUNT.getCode()))
		{
			final String phoneNumber = registerForm.getMobileNumber();
			final String zipCode = registerForm.getZipCode();
			final String country = registerForm.getCountry();
			final String province = registerForm.getProvince();

			validateName(errors, phoneNumber, "mobileNumber", "register.mobileNumber.invalid");
			if (Objects.nonNull(phoneNumber) && phoneNumber.length() < 10)
			{
				errors.rejectValue("mobileNumber", "register.mobileNumber.invalid.length");
			}
			validateName(errors, country, "country", "register.country.invalid");
			validateName(errors, zipCode, "zipCode", "register.zipCode.invalid");

			final String regionsRequiredForCountries = bcfConfigurablePropertiesService
					.getBcfPropertyValue(BcfCoreConstants.REGIONS_REQUIRED_FOR_COUNTRIES);
			if (StringUtils.isNotEmpty(regionsRequiredForCountries))
			{
				final List<String> regionCountries = Arrays.asList(regionsRequiredForCountries.split(","));
				final String addressLine1 = registerForm.getAddressLine1();
				final String city = registerForm.getCity();
				if (regionCountries.contains(country))
				{
					if (StringUtils.isNotEmpty(zipCode) && (!StringUtils.equals(province, "-1")
							|| StringUtils.isNotEmpty(city) || StringUtils.isNotEmpty(addressLine1)))
					{
						if(Objects.isNull(province) || StringUtils.equals(province, "-1"))
						{
							errors.rejectValue("province", "register.province.invalid");
						}
						validateName(errors, city, "city", "register.city.invalid");
						validateName(errors, addressLine1, "addressLine1", ADDRESS_LINE1_INVALID);
					}
				}
			}
		}
	}
}
