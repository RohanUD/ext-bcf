/*
 * Copyright (c) 2019 British Columbia Ferry Services Inc
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of British Columbia Ferry Services Inc("Confidential Information").
 * You shall not disclose such Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with British Columbia Ferry Services Inc.
 *
 * Last Modified: 06/06/19 20:21
 */

package com.bcf.bcfstorefrontaddon.strategies.asm;

import de.hybris.platform.commercefacades.accommodation.GlobalSuggestionData;
import de.hybris.platform.commercefacades.travel.CabinClassData;
import de.hybris.platform.commercefacades.travel.PassengerInformationData;
import de.hybris.platform.commercefacades.travel.PassengerTypeData;
import de.hybris.platform.commercefacades.travel.TravellerData;
import de.hybris.platform.commercefacades.travel.enums.SuggestionType;
import de.hybris.platform.commercefacades.travel.enums.TripType;
import de.hybris.platform.core.model.order.AbstractOrderEntryModel;
import de.hybris.platform.servicelayer.session.SessionService;
import de.hybris.platform.travelfacades.constants.TravelfacadesConstants;
import de.hybris.platform.travelfacades.facades.CabinClassFacade;
import de.hybris.platform.travelfacades.facades.PassengerTypeFacade;
import de.hybris.platform.travelfacades.facades.TransportOfferingFacade;
import de.hybris.platform.travelfacades.facades.TravellerFacade;
import de.hybris.platform.travelfacades.strategies.TravellerSortStrategy;
import de.hybris.platform.travelservices.enums.LocationType;
import de.hybris.platform.travelservices.enums.TravellerType;
import de.hybris.platform.travelservices.model.order.AccommodationOrderEntryGroupModel;
import de.hybris.platform.travelservices.model.order.TravelOrderEntryInfoModel;
import de.hybris.platform.travelservices.model.travel.LocationModel;
import de.hybris.platform.travelservices.model.travel.TransportFacilityModel;
import de.hybris.platform.travelservices.model.warehouse.TransportOfferingModel;
import de.hybris.platform.travelservices.utils.TravelDateUtils;
import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.util.Comparator;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.function.Function;
import java.util.stream.Collectors;
import org.apache.commons.collections.MapUtils;
import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Required;
import com.bcf.bcfstorefrontaddon.constants.BcfstorefrontaddonWebConstants;


/**
 * Abstract redirect strategy that exposes the common methods for the ASM redirect strategies.
 */
public abstract class AbstractASRedirectStrategy
{
	private TravellerSortStrategy travellerSortStrategy;
	private PassengerTypeFacade passengerTypeFacade;
	private TransportOfferingFacade transportOfferingFacade;
	private SessionService sessionService;
	private TravellerFacade travellerFacade;
	private CabinClassFacade cabinClassFacade;

	/**
	 * Returns the map of the passenger type quantities.
	 *
	 * @return a map with key corresponding to the passenger type and value correponding to its quantity
	 */
	protected Map<String, Integer> getPassengerTypeQuantityList()
	{
		final List<PassengerTypeData> sortedPassengerTypes = getTravellerSortStrategy()
				.sortPassengerTypes(getPassengerTypeFacade().getPassengerTypes());

		return sortedPassengerTypes.stream().collect(
				Collectors.toMap(PassengerTypeData::getCode, passengerTypeData -> TravelfacadesConstants.DEFAULT_GUEST_QUANTITY));
	}

	/**
	 * Gets the location name.
	 *
	 * @param locationCode the location code
	 * @param locationType the location type
	 * @return the location name
	 */
	protected String getLocationName(final String locationCode, final String locationType)
	{

		final Map<GlobalSuggestionData, List<GlobalSuggestionData>> suggestionResults = getTransportOfferingFacade()
				.getOriginSuggestionData(locationCode);

		if (StringUtils.isEmpty(locationType) || MapUtils.isEmpty(suggestionResults))
		{
			return StringUtils.EMPTY;
		}
		String locationName = StringUtils.EMPTY;
		final GlobalSuggestionData firstSuggestion = suggestionResults.keySet().iterator().next();
		if (StringUtils.equalsIgnoreCase(SuggestionType.AIRPORTGROUP.toString(), locationType))
		{
			locationName = suggestionResults.get(firstSuggestion).stream().findFirst().get().getName();
		}
		else if (StringUtils.equalsIgnoreCase(SuggestionType.CITY.toString(), locationType))
		{
			locationName = firstSuggestion.getName();
		}
		return locationName.replaceAll("\\|", "%7C").replaceAll(", ", "%2C%20").replaceAll(" ", "%20");
	}

	/**
	 * Return the UTC departure time for the given transportOffering
	 *
	 * @param transportOfferingModel as the tranportOffering
	 * @return the zoned date time
	 */
	protected ZonedDateTime getUTCDepartureTime(final TransportOfferingModel transportOfferingModel)
	{
		final String zoneId = transportOfferingModel.getTravelSector().getOrigin().getPointOfService().get(0).getTimeZoneId();
		return TravelDateUtils.getUtcZonedDateTime(transportOfferingModel.getDepartureTime(), ZoneId.of(zoneId));
	}

	/**
	 * Returns the string with the guest occupancies.
	 *
	 * @param transportationEntries as the list of abstract order entry models of type transport
	 * @param accommodationEntries  as the list of abstract order entry models of type accommodation
	 * @return the string with the guest occupancies
	 */
	public String getGuestOccupancy(final List<AbstractOrderEntryModel> accommodationEntries)
	{
		final List<AccommodationOrderEntryGroupModel> accommodationEntryGroups = accommodationEntries.stream()
				.map(AbstractOrderEntryModel::getEntryGroup).filter(AccommodationOrderEntryGroupModel.class::isInstance)
				.map(AccommodationOrderEntryGroupModel.class::cast).distinct().collect(Collectors.toList());

		final List<TravellerData> travellers = getTravellerFacade().getTravellersForCartEntries();
		final Map<String, Long> travellersMap = travellers.stream()
				.filter(traveller -> TravellerType.PASSENGER.getCode().equals(traveller.getTravellerType()))
				.collect(Collectors.groupingBy(
						traveller -> ((PassengerInformationData) traveller.getTravellerInfo()).getPassengerType().getCode(),
						Collectors.counting()));

		final Map<AccommodationOrderEntryGroupModel, Map<String, Integer>> roomPassengerTypeQuantityMap = accommodationEntryGroups
				.stream().collect(Collectors.toMap(Function.identity(), entryGroup -> getPassengerTypeQuantityList()));

		roomPassengerTypeQuantityMap.forEach((accommodationOrderEntryGroup, passengerMap) -> accommodationOrderEntryGroup
				.getAccommodation().getGuestOccupancies().forEach(guestOccupancy -> {
					final long quantityInMap = travellersMap.get(guestOccupancy.getPassengerType().getCode()) != null
							? travellersMap.get(guestOccupancy.getPassengerType().getCode())
							: TravelfacadesConstants.DEFAULT_GUEST_QUANTITY;
					final long quantityMax = guestOccupancy.getQuantityMax();

					final long quantity = Math.min(quantityInMap, quantityMax);
					passengerMap.put(guestOccupancy.getPassengerType().getCode(), (int) quantity);
					travellersMap.put(guestOccupancy.getPassengerType().getCode(), quantityInMap - quantity);
				}));

		final Map<String, Long> remainingTravellersMap = travellersMap.entrySet().stream()
				.filter(entry -> entry.getValue() > TravelfacadesConstants.DEFAULT_GUEST_QUANTITY)
				.collect(Collectors.toMap(Map.Entry::getKey, Map.Entry::getValue));

		if (MapUtils.isNotEmpty(remainingTravellersMap))
		{
			remainingTravellersMap.forEach((passengerType, value) -> {
				final Map<String, Integer> firstRoomMap = roomPassengerTypeQuantityMap.entrySet().stream().findFirst().get()
						.getValue();
				final long newQuantity = firstRoomMap.get(passengerType) + value;
				firstRoomMap.put(passengerType, (int) newQuantity);
			});
		}

		final StringBuilder queryStringBuilder = new StringBuilder();
		queryStringBuilder.append(BcfstorefrontaddonWebConstants.AMPERSAND);

		roomPassengerTypeQuantityMap.forEach((accommodationEntryGroup, passengerTypeQuantityMap) -> {
			queryStringBuilder.append(BcfstorefrontaddonWebConstants.ROOM_QUERY_STRING_INDICATOR)
					.append(accommodationEntryGroup.getRoomStayRefNumber());
			queryStringBuilder.append(BcfstorefrontaddonWebConstants.EQUALS);
			passengerTypeQuantityMap.forEach((passengerType, quantity) -> {
				queryStringBuilder.append(quantity);
				queryStringBuilder.append(BcfstorefrontaddonWebConstants.HYPHEN);
				queryStringBuilder.append(passengerType);
				queryStringBuilder.append(BcfstorefrontaddonWebConstants.COMMA);
			});
			// remove the last comma
			queryStringBuilder.deleteCharAt(queryStringBuilder.length() - 1);
			queryStringBuilder.append(BcfstorefrontaddonWebConstants.AMPERSAND);
		});

		return queryStringBuilder.substring(0, queryStringBuilder.length() - 1);
	}

	public void setOriginDetails(final List<AbstractOrderEntryModel> transportationEntries,
			final Map<String, String> urlParameters)
	{
		final List<TransportFacilityModel> originList = transportationEntries.stream()
				.map(entry -> entry.getTravelOrderEntryInfo().getOriginDestinationRefNumber() == 0
						? entry.getTravelOrderEntryInfo().getTravelRoute().getOrigin()
						: entry.getTravelOrderEntryInfo().getTravelRoute().getDestination())
				.distinct().collect(Collectors.toList());
		if (CollectionUtils.size(originList) > 1)
		{
			final LocationModel originLocation = originList.get(0).getLocation();
			urlParameters.put(BcfstorefrontaddonWebConstants.DEPARTURE_LOCATION, originLocation.getCode());
			urlParameters.put(BcfstorefrontaddonWebConstants.DEPARTURE_LOCATION_NAME,
					getLocationName(originLocation.getCode(), LocationType.CITY.getCode()));
			urlParameters.put(BcfstorefrontaddonWebConstants.DEPARTURE_LOCATION_SUGGESTION_TYPE, LocationType.CITY.getCode());
		}
		else
		{
			urlParameters.put(BcfstorefrontaddonWebConstants.DEPARTURE_LOCATION, originList.get(0).getCode());
			urlParameters.put(BcfstorefrontaddonWebConstants.DEPARTURE_LOCATION_NAME,
					getLocationName(originList.get(0).getCode(), LocationType.AIRPORTGROUP.getCode()));
			urlParameters.put(BcfstorefrontaddonWebConstants.DEPARTURE_LOCATION_SUGGESTION_TYPE,
					LocationType.AIRPORTGROUP.getCode());
		}
	}

	public void setDestinationDetails(final List<AbstractOrderEntryModel> transportationEntries,
			final Map<String, String> urlParameters)
	{
		final List<TransportFacilityModel> destinationList = transportationEntries.stream()
				.map(entry -> entry.getTravelOrderEntryInfo().getOriginDestinationRefNumber() == 0
						? entry.getTravelOrderEntryInfo().getTravelRoute().getDestination()
						: entry.getTravelOrderEntryInfo().getTravelRoute().getOrigin())
				.distinct().collect(Collectors.toList());

		if (CollectionUtils.size(destinationList) > 1)
		{
			final LocationModel destinationLocation = destinationList.get(0).getLocation();
			urlParameters.put(BcfstorefrontaddonWebConstants.ARRIVAL_LOCATION, destinationLocation.getCode());
			urlParameters.put(BcfstorefrontaddonWebConstants.ARRIVAL_LOCATION_NAME,
					getLocationName(destinationLocation.getCode(), LocationType.CITY.getCode()));
			urlParameters.put(BcfstorefrontaddonWebConstants.ARRIVAL_LOCATION_SUGGESTION_TYPE, LocationType.CITY.getCode());
		}
		else
		{
			urlParameters.put(BcfstorefrontaddonWebConstants.ARRIVAL_LOCATION, destinationList.get(0).getCode());
			urlParameters.put(BcfstorefrontaddonWebConstants.ARRIVAL_LOCATION_NAME,
					getLocationName(destinationList.get(0).getCode(), LocationType.AIRPORTGROUP.getCode()));
			urlParameters.put(BcfstorefrontaddonWebConstants.ARRIVAL_LOCATION_SUGGESTION_TYPE, LocationType.AIRPORTGROUP.getCode());
		}
	}

	public Map<String, String> populateUrlParameters(final List<AbstractOrderEntryModel> transportationEntries,
			final Map<String, String> urlParameters, final long legCount)
	{
		final Optional<AbstractOrderEntryModel> firstLegEntry = transportationEntries.stream()
				.filter(entry -> entry.getTravelOrderEntryInfo().getOriginDestinationRefNumber() == 0).findFirst();

		final TransportOfferingModel firstTransportOffering = firstLegEntry.get().getTravelOrderEntryInfo().getTransportOfferings()
				.stream().sorted(Comparator.comparing(this::getUTCDepartureTime)).findFirst().get();
		urlParameters.put(BcfstorefrontaddonWebConstants.DEPARTING_DATE_TIME, TravelDateUtils
				.convertDateToStringDate(firstTransportOffering.getDepartureTime(), BcfstorefrontaddonWebConstants.DATE_FORMAT));

		if (legCount > 1)
		{
			urlParameters.put(BcfstorefrontaddonWebConstants.TRIP_TYPE, TripType.RETURN.toString());

			// Return Date Time
			final TravelOrderEntryInfoModel returnOrderEntryInfo = transportationEntries.stream()
					.map(AbstractOrderEntryModel::getTravelOrderEntryInfo)
					.filter(entryInfo -> entryInfo.getOriginDestinationRefNumber() == 1).findFirst().get();

			final TransportOfferingModel firstReturnTransportOffering = returnOrderEntryInfo.getTransportOfferings().stream()
					.sorted(Comparator.comparing(this::getUTCDepartureTime)).findFirst().get();
			urlParameters.put(BcfstorefrontaddonWebConstants.RETURN_DATE_TIME, TravelDateUtils.convertDateToStringDate(
					firstReturnTransportOffering.getDepartureTime(), BcfstorefrontaddonWebConstants.DATE_FORMAT));
		}
		else
		{
			urlParameters.put(BcfstorefrontaddonWebConstants.TRIP_TYPE, TripType.SINGLE.toString());
		}

		final Optional<CabinClassData> cabinClassOptional = transportationEntries.stream()
				.map(entry -> entry.getBundleTemplate().getParentTemplate().getId()).distinct()
				.map(bundleTemplateId -> getCabinClassFacade().findCabinClassFromBundleTemplate(bundleTemplateId))
				.sorted(Comparator.comparing(CabinClassData::getIndex)).findFirst();

		cabinClassOptional
				.ifPresent(cabinClassData -> urlParameters.put(BcfstorefrontaddonWebConstants.CABIN_CLASS, cabinClassData.getCode()));

		// Passenger Type Quantities
		final List<TravellerData> travellers = getTravellerFacade().getTravellersForCartEntries();
		final Map<String, Long> travellersMap = travellers.stream()
				.filter(traveller -> TravellerType.PASSENGER.getCode().equals(traveller.getTravellerType()))
				.collect(Collectors.groupingBy(
						traveller -> ((PassengerInformationData) traveller.getTravellerInfo()).getPassengerType().getCode(),
						Collectors.counting()));
		travellersMap.forEach((key, value) -> urlParameters.put(key, String.valueOf(value)));
		return urlParameters;
	}

	/**
	 * @return the travellerSortStrategy
	 */
	protected TravellerSortStrategy getTravellerSortStrategy()
	{
		return travellerSortStrategy;
	}

	/**
	 * @param travellerSortStrategy the travellerSortStrategy to set
	 */
	@Required
	public void setTravellerSortStrategy(final TravellerSortStrategy travellerSortStrategy)
	{
		this.travellerSortStrategy = travellerSortStrategy;
	}

	/**
	 * @return the passengerTypeFacade
	 */
	protected PassengerTypeFacade getPassengerTypeFacade()
	{
		return passengerTypeFacade;
	}

	/**
	 * @param passengerTypeFacade the passengerTypeFacade to set
	 */
	@Required
	public void setPassengerTypeFacade(final PassengerTypeFacade passengerTypeFacade)
	{
		this.passengerTypeFacade = passengerTypeFacade;
	}

	/**
	 * @return the transportOfferingFacade
	 */
	protected TransportOfferingFacade getTransportOfferingFacade()
	{
		return transportOfferingFacade;
	}

	/**
	 * @param transportOfferingFacade the transportOfferingFacade to set
	 */
	@Required
	public void setTransportOfferingFacade(final TransportOfferingFacade transportOfferingFacade)
	{
		this.transportOfferingFacade = transportOfferingFacade;
	}

	/**
	 * @return the sessionService
	 */
	protected SessionService getSessionService()
	{
		return sessionService;
	}

	/**
	 * @param sessionService the sessionService to set
	 */
	@Required
	public void setSessionService(final SessionService sessionService)
	{
		this.sessionService = sessionService;
	}

	protected TravellerFacade getTravellerFacade()
	{
		return travellerFacade;
	}

	@Required
	public void setTravellerFacade(final TravellerFacade travellerFacade)
	{
		this.travellerFacade = travellerFacade;
	}

	protected CabinClassFacade getCabinClassFacade()
	{
		return cabinClassFacade;
	}

	@Required
	public void setCabinClassFacade(final CabinClassFacade cabinClassFacade)
	{
		this.cabinClassFacade = cabinClassFacade;
	}
}
