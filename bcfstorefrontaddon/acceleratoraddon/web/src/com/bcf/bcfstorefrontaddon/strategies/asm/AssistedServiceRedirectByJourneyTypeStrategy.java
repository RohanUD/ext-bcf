/*
 * Copyright (c) 2019 British Columbia Ferry Services Inc
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of British Columbia Ferry Services Inc("Confidential Information").
 * You shall not disclose such Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with British Columbia Ferry Services Inc.
 *
 * Last Modified: 06/06/19 20:21
 */

package com.bcf.bcfstorefrontaddon.strategies.asm;

import de.hybris.platform.core.model.order.CartModel;


/**
 * Interface for the ASM redirect strategies based on the journey type.
 */
public interface AssistedServiceRedirectByJourneyTypeStrategy
{
	/**
	 * Returns the redirect path.
	 *
	 * @param cartModel as the cart model
	 * @return a string corresponding to the redirect path
	 */
	String getRedirectPath(final CartModel cartModel);
}
