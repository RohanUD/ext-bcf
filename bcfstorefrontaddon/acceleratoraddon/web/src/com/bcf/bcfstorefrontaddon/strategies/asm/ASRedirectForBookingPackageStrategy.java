/*
 * Copyright (c) 2019 British Columbia Ferry Services Inc
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of British Columbia Ferry Services Inc("Confidential Information").
 * You shall not disclose such Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with British Columbia Ferry Services Inc.
 *
 * Last Modified: 06/06/19 20:21
 */

package com.bcf.bcfstorefrontaddon.strategies.asm;

import de.hybris.platform.commercefacades.travel.CabinClassData;
import de.hybris.platform.commercefacades.travel.enums.TripType;
import de.hybris.platform.core.model.order.AbstractOrderEntryModel;
import de.hybris.platform.core.model.order.CartModel;
import de.hybris.platform.travelfacades.facades.CabinClassFacade;
import de.hybris.platform.travelfacades.facades.packages.DealBundleTemplateFacade;
import de.hybris.platform.travelfacades.facades.packages.DealCartFacade;
import de.hybris.platform.travelservices.enums.LocationType;
import de.hybris.platform.travelservices.enums.OrderEntryType;
import de.hybris.platform.travelservices.model.order.AccommodationOrderEntryGroupModel;
import de.hybris.platform.travelservices.model.order.TravelOrderEntryInfoModel;
import de.hybris.platform.travelservices.model.product.FareProductModel;
import de.hybris.platform.travelservices.model.travel.LocationModel;
import de.hybris.platform.travelservices.model.travel.TransportFacilityModel;
import de.hybris.platform.travelservices.model.warehouse.TransportOfferingModel;
import de.hybris.platform.travelservices.utils.TravelDateUtils;
import java.util.Comparator;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.stream.Collectors;
import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Required;
import com.bcf.bcfstorefrontaddon.constants.BcfstorefrontaddonWebConstants;
import com.bcf.core.constants.BcfCoreConstants;


/**
 * Redirect strategy for package journey responsible for building the required url parameters and redirect to the package details
 * page.
 */
public class ASRedirectForBookingPackageStrategy extends AbstractASRedirectStrategy
		implements AssistedServiceRedirectByJourneyTypeStrategy
{
	private static final String DEFAULT_CART_REDIRECT = "/";
	private static final String PACKAGE_DETAILS_ROOT_URL = "/package-details";
	private static final String DEAL_DETAILS_ROOT_URL = "/deal-details";

	private DealCartFacade dealCartFacade;
	private DealBundleTemplateFacade dealBundleTemplateFacade;
	private CabinClassFacade cabinClassFacade;

	@Override
	public String getRedirectPath(final CartModel cartModel)
	{
		getSessionService().setAttribute(BcfCoreConstants.SESSION_BOOKING_JOURNEY,
				BcfstorefrontaddonWebConstants.BOOKING_PACKAGE);

		final String urlParameters;
		final String redirectUrl;
		if (getDealCartFacade().isDealInCart())
		{
			urlParameters = buildUrlParametersForDeal(cartModel);
			redirectUrl = DEAL_DETAILS_ROOT_URL;
		}
		else
		{
			urlParameters = buildUrlParametersForPackage(cartModel);
			redirectUrl = PACKAGE_DETAILS_ROOT_URL;
		}

		return StringUtils.isBlank(urlParameters) ? DEFAULT_CART_REDIRECT : redirectUrl + urlParameters;
	}

	/**
	 * Returns the string with the url parameters required for the redirect to the deal-details page.
	 *
	 * @param cartModel as the cart model
	 * @return the string of the url parameters
	 */
	protected String buildUrlParametersForDeal(final CartModel cartModel)
	{
		final Map<String, String> urlParameters = new HashMap<>();

		urlParameters.put(BcfstorefrontaddonWebConstants.DEAL_BUNDLE_TEMPLATE_ID,
				getDealBundleTemplateFacade().getDealBundleTemplateIdFromAbstractOrder(cartModel));

		final Optional<AbstractOrderEntryModel> firstLegEntry = cartModel.getEntries().stream()
				.filter(entry -> entry.getTravelOrderEntryInfo().getOriginDestinationRefNumber() == 0).findFirst();
		final TransportOfferingModel firstTransportOffering = firstLegEntry.get().getTravelOrderEntryInfo().getTransportOfferings()
				.stream().sorted(Comparator.comparing(this::getUTCDepartureTime)).findFirst().get();
		urlParameters.put(BcfstorefrontaddonWebConstants.DEAL_SELECTED_DEPARTURE_DATE, TravelDateUtils
				.convertDateToStringDate(firstTransportOffering.getDepartureTime(),
						BcfstorefrontaddonWebConstants.DATE_FORMAT));

		return "?" + urlParameters.toString().replace(", ", "&").replace("{", "").replace("}", "");
	}

	/**
	 * Returns the string with the url parameters required for the redirect to the package-details page.
	 *
	 * @param cartModel as the cart model
	 * @return the string of the url parameters
	 */
	protected String buildUrlParametersForPackage(final CartModel cartModel)
	{
		final List<AbstractOrderEntryModel> transportationEntries = cartModel.getEntries().stream()
				.filter(entry -> OrderEntryType.TRANSPORT.equals(entry.getType())
						&& FareProductModel._TYPECODE.equals(entry.getProduct().getItemtype()))
				.collect(Collectors.toList());

		final List<AbstractOrderEntryModel> accommodationEntries = cartModel.getEntries().stream()
				.filter(entry -> OrderEntryType.ACCOMMODATION.equals(entry.getType()))
				.collect(Collectors.toList());

		if (CollectionUtils.isEmpty(transportationEntries) || CollectionUtils.isEmpty(accommodationEntries))
		{
			return null;
		}

		final Map<String, String> urlParameters = new HashMap<>();
		populateUrlParametersForAccommodation(accommodationEntries, urlParameters);
		populateUrlParametersForTransport(transportationEntries, urlParameters);
		populatePartHotelStay(urlParameters);

		String redirectUrl = "/" + urlParameters.get(BcfstorefrontaddonWebConstants.ACCOMMODATION_OFFERING_CODE);
		urlParameters.remove(BcfstorefrontaddonWebConstants.ACCOMMODATION_OFFERING_CODE);
		redirectUrl = redirectUrl + "?" + urlParameters.toString().replace(", ", "&").replace("{", "").replace("}", "");

		redirectUrl = redirectUrl + getGuestOccupancy(accommodationEntries);
		return redirectUrl;
	}

	/**
	 * Populates the url parameters map with the attributes for the transport part.
	 *
	 * @param transportationEntries as the list of abstract order entry models of type transport
	 * @param urlParameters         as the url parameters map
	 */
	protected void populateUrlParametersForTransport(final List<AbstractOrderEntryModel> transportationEntries,
			final Map<String, String> urlParameters)
	{
		// ORIGIN
		final List<TransportFacilityModel> originList = transportationEntries.stream()
				.map(entry -> entry.getTravelOrderEntryInfo().getOriginDestinationRefNumber() == 0 ?
						entry.getTravelOrderEntryInfo().getTravelRoute().getOrigin() :
						entry.getTravelOrderEntryInfo().getTravelRoute().getDestination()).distinct().collect(Collectors.toList());
		if (CollectionUtils.size(originList) > 1)
		{
			final LocationModel originLocation = originList.get(0).getLocation();
			urlParameters.put(BcfstorefrontaddonWebConstants.DEPARTURE_LOCATION, originLocation.getCode());
			urlParameters
					.put(BcfstorefrontaddonWebConstants.DEPARTURE_LOCATION_SUGGESTION_TYPE, LocationType.CITY.getCode());
		}
		else
		{
			urlParameters.put(BcfstorefrontaddonWebConstants.DEPARTURE_LOCATION, originList.get(0).getCode());
			urlParameters.put(BcfstorefrontaddonWebConstants.DEPARTURE_LOCATION_SUGGESTION_TYPE,
					LocationType.AIRPORTGROUP.getCode());
		}

		// DESTINATION
		final List<TransportFacilityModel> destinationList = transportationEntries.stream()
				.map(entry -> entry.getTravelOrderEntryInfo().getOriginDestinationRefNumber() == 0 ?
						entry.getTravelOrderEntryInfo().getTravelRoute().getDestination() :
						entry.getTravelOrderEntryInfo().getTravelRoute().getOrigin()).distinct().collect(Collectors.toList());

		if (CollectionUtils.size(destinationList) > 1)
		{
			final LocationModel destinationLocation = destinationList.get(0).getLocation();
			urlParameters.put(BcfstorefrontaddonWebConstants.ARRIVAL_LOCATION, destinationLocation.getCode());
			urlParameters.put(BcfstorefrontaddonWebConstants.ARRIVAL_LOCATION_SUGGESTION_TYPE, LocationType.CITY.getCode
					());
		}
		else
		{
			urlParameters.put(BcfstorefrontaddonWebConstants.ARRIVAL_LOCATION, destinationList.get(0).getCode());
			urlParameters.put(BcfstorefrontaddonWebConstants.ARRIVAL_LOCATION_SUGGESTION_TYPE,
					LocationType.AIRPORTGROUP.getCode());
		}

		final Optional<AbstractOrderEntryModel> firstLegEntry = transportationEntries.stream()
				.filter(entry -> entry.getTravelOrderEntryInfo().getOriginDestinationRefNumber() == 0).findFirst();

		final TransportOfferingModel firstTransportOffering = firstLegEntry.get().getTravelOrderEntryInfo()
				.getTransportOfferings()
				.stream().sorted(Comparator.comparing(this::getUTCDepartureTime)).findFirst().get();
		urlParameters.put(BcfstorefrontaddonWebConstants.DEPARTING_DATE_TIME, TravelDateUtils
				.convertDateToStringDate(firstTransportOffering.getDepartureTime(),
						BcfstorefrontaddonWebConstants.DATE_FORMAT));

		final long legCount = transportationEntries.stream()
				.map(entry -> entry.getTravelOrderEntryInfo().getOriginDestinationRefNumber())
				.distinct().count();
		if (legCount > 1)
		{
			urlParameters.put(BcfstorefrontaddonWebConstants.TRIP_TYPE, TripType.RETURN.toString());

			// Return Date Time
			final TravelOrderEntryInfoModel returnOrderEntryInfo = transportationEntries.stream()
					.map(AbstractOrderEntryModel::getTravelOrderEntryInfo)
					.filter(entryInfo -> entryInfo.getOriginDestinationRefNumber() == 1).findFirst().get();

			final TransportOfferingModel firstReturnTransportOffering = returnOrderEntryInfo.getTransportOfferings().stream()
					.sorted(Comparator.comparing(this::getUTCDepartureTime)).findFirst().get();
			urlParameters.put(BcfstorefrontaddonWebConstants.RETURN_DATE_TIME, TravelDateUtils
					.convertDateToStringDate(firstReturnTransportOffering.getDepartureTime(),
							BcfstorefrontaddonWebConstants.DATE_FORMAT));
		}
		else
		{
			urlParameters.put(BcfstorefrontaddonWebConstants.TRIP_TYPE, TripType.SINGLE.toString());
		}

		final Optional<CabinClassData> cabinClassOptional = transportationEntries.stream()
				.map(entry -> entry.getBundleTemplate().getParentTemplate().getId()).distinct()
				.map(bundleTemplateId -> getCabinClassFacade().findCabinClassFromBundleTemplate(bundleTemplateId))
				.sorted(Comparator.comparing(CabinClassData::getIndex)).findFirst();

		cabinClassOptional.ifPresent(cabinClassData ->
				urlParameters.put(BcfstorefrontaddonWebConstants.CABIN_CLASS, cabinClassData.getCode()));
	}

	/**
	 * Populates the url parameters map with the attributes for the accommodation part.
	 *
	 * @param accommodationEntries as the list of abstract order entry models of type accommodation
	 * @param urlParameters        as the url parameters map
	 */
	protected void populateUrlParametersForAccommodation(final List<AbstractOrderEntryModel> accommodationEntries,
			final Map<String, String> urlParameters)
	{
		final List<AccommodationOrderEntryGroupModel> accommodationEntryGroups = accommodationEntries.stream()
				.map(AbstractOrderEntryModel::getEntryGroup).filter(AccommodationOrderEntryGroupModel.class::isInstance)
				.map(AccommodationOrderEntryGroupModel.class::cast).distinct().collect(Collectors.toList());

		final String accommodationOfferingCode = accommodationEntryGroups.get(0).getAccommodationOffering().getCode();
		urlParameters.put(BcfstorefrontaddonWebConstants.ACCOMMODATION_OFFERING_CODE, accommodationOfferingCode);

		urlParameters.put(BcfstorefrontaddonWebConstants.CHECKIN_DATE, TravelDateUtils
				.convertDateToStringDate(accommodationEntryGroups.get(0).getStartingDate(),
						BcfstorefrontaddonWebConstants.DATE_FORMAT));
		urlParameters.put(BcfstorefrontaddonWebConstants.CHECKOUT_DATE, TravelDateUtils
				.convertDateToStringDate(accommodationEntryGroups.get(0).getEndingDate(),
						BcfstorefrontaddonWebConstants.DATE_FORMAT));
		final long numberOfRooms = accommodationEntryGroups.stream().map(AccommodationOrderEntryGroupModel::getRoomStayRefNumber)
				.distinct().count();
		urlParameters.put(BcfstorefrontaddonWebConstants.NUMBER_OF_ROOMS, String.valueOf(numberOfRooms));
	}

	/**
	 * Populates the partHotelStay attribute in the urlParameters map
	 *
	 * @param urlParameters as the map of the url parameters.
	 */
	protected void populatePartHotelStay(final Map<String, String> urlParameters)
	{
		final Date checkInDate = TravelDateUtils
				.convertStringDateToDate(urlParameters.get(BcfstorefrontaddonWebConstants.CHECKIN_DATE),
						BcfstorefrontaddonWebConstants.DATE_FORMAT);
		final Date checkOutDate = TravelDateUtils
				.convertStringDateToDate(urlParameters.get(BcfstorefrontaddonWebConstants.CHECKOUT_DATE),
						BcfstorefrontaddonWebConstants.DATE_FORMAT);

		final Date departingDate = TravelDateUtils
				.convertStringDateToDate(urlParameters.get(BcfstorefrontaddonWebConstants.DEPARTING_DATE_TIME),
						BcfstorefrontaddonWebConstants.DATE_FORMAT);

		boolean partHotelStay = true;
		final String returnDateString = urlParameters.get(BcfstorefrontaddonWebConstants.RETURN_DATE_TIME);
		if (StringUtils.isNotBlank(returnDateString))
		{
			final Date returnDate = TravelDateUtils
					.convertStringDateToDate(returnDateString, BcfstorefrontaddonWebConstants.DATE_FORMAT);
			partHotelStay = !(TravelDateUtils.isSameDate(checkInDate, departingDate) && TravelDateUtils
					.isSameDate(checkOutDate, returnDate));
		}

		urlParameters.put(BcfstorefrontaddonWebConstants.PART_HOTEL_STAY, String.valueOf(partHotelStay));
	}

	/**
	 * @return the dealCartFacade
	 */
	protected DealCartFacade getDealCartFacade()
	{
		return dealCartFacade;
	}

	/**
	 * @param dealCartFacade the dealCartFacade to set
	 */
	@Required
	public void setDealCartFacade(final DealCartFacade dealCartFacade)
	{
		this.dealCartFacade = dealCartFacade;
	}

	/**
	 * @return the dealBundleTemplateFacade
	 */
	protected DealBundleTemplateFacade getDealBundleTemplateFacade()
	{
		return dealBundleTemplateFacade;
	}

	/**
	 * @param dealBundleTemplateFacade the dealBundleTemplateFacade to set
	 */
	@Required
	public void setDealBundleTemplateFacade(
			final DealBundleTemplateFacade dealBundleTemplateFacade)
	{
		this.dealBundleTemplateFacade = dealBundleTemplateFacade;
	}

	/**
	 * @return the cabinClassFacade
	 */
	@Override
	protected CabinClassFacade getCabinClassFacade()
	{
		return cabinClassFacade;
	}

	/**
	 * @param cabinClassFacade the cabinClassFacade to set
	 */
	@Override
	@Required
	public void setCabinClassFacade(final CabinClassFacade cabinClassFacade)
	{
		this.cabinClassFacade = cabinClassFacade;
	}
}
