/*
 * Copyright (c) 2019 British Columbia Ferry Services Inc
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of British Columbia Ferry Services Inc("Confidential Information").
 * You shall not disclose such Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with British Columbia Ferry Services Inc.
 *
 * Last Modified: 06/06/19 20:21
 */

package com.bcf.bcfstorefrontaddon.strategies.asm;


import de.hybris.platform.servicelayer.config.ConfigurationService;
import de.hybris.platform.travelfacades.order.TravelCartFacade;
import java.util.Map;
import javax.annotation.Resource;
import org.springframework.beans.factory.annotation.Required;
import com.bcf.bcfassistedservicesstorefront.constants.BcfassistedservicesstorefrontConstants;
import com.bcf.bcfassistedservicesstorefront.redirect.AssistedServiceRedirectStrategy;
import com.bcf.bcfassistedservicesstorefront.redirect.impl.DefaultAssistedServiceRedirectStrategy;


/**
 * Default BCF implementation for {@link AssistedServiceRedirectStrategy}. If the cart is amendment cart, relevant order
 * page is returned else cart page is returned
 */
public class DefaultTravelAssistedServiceRedirectStrategy extends DefaultAssistedServiceRedirectStrategy
{
	private static final String DEFAULT_CART_REDIRECT = "/";
	private final static String DEFAULT_ORDER_REDIRECT = "/manage-booking/booking-details/%s";

	private TravelCartFacade travelCartFacade;
	private Map<String, AssistedServiceRedirectByJourneyTypeStrategy> redirectStrategyMap;

	@Resource(name = "configurationService")
	private ConfigurationService configurationService;

	@Override
	protected String getPathWithCart()
	{
		if (getTravelCartFacade().isAmendmentCart())
		{
			return String
					.format(
							getConfigurationService().getConfiguration()
									.getString(BcfassistedservicesstorefrontConstants.REDIRECT_WITH_ORDER, DEFAULT_ORDER_REDIRECT),
							getTravelCartFacade().getOriginalOrderCode());
		}

		return getConfigurationService().getConfiguration().getString(BcfassistedservicesstorefrontConstants.REDIRECT_WITH_CART,
				DEFAULT_CART_REDIRECT);
	}

	/**
	 * @return the redirectStrategyMap
	 */
	protected Map<String, AssistedServiceRedirectByJourneyTypeStrategy> getRedirectStrategyMap()
	{
		return redirectStrategyMap;
	}

	/**
	 * @param redirectStrategyMap the redirectStrategyMap to set
	 */
	@Required
	public void setRedirectStrategyMap(final Map<String, AssistedServiceRedirectByJourneyTypeStrategy> redirectStrategyMap)
	{
		this.redirectStrategyMap = redirectStrategyMap;
	}

	/**
	 * @return the travelCartFacade
	 */
	protected TravelCartFacade getTravelCartFacade()
	{
		return travelCartFacade;
	}

	/**
	 * @param travelCartFacade the travelCartFacade to set
	 */
	@Required
	public void setTravelCartFacade(final TravelCartFacade travelCartFacade)
	{
		this.travelCartFacade = travelCartFacade;
	}

	/**
	 * @return the configurationService
	 */
	public ConfigurationService getConfigurationService()
	{
		return configurationService;
	}

	/**
	 * @param configurationService the configurationService to set
	 */
	public void setConfigurationService(final ConfigurationService configurationService)
	{
		this.configurationService = configurationService;
	}
}
