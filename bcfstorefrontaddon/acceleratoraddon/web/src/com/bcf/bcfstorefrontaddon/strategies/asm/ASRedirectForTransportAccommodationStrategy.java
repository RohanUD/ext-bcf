/*
 * Copyright (c) 2019 British Columbia Ferry Services Inc
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of British Columbia Ferry Services Inc("Confidential Information").
 * You shall not disclose such Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with British Columbia Ferry Services Inc.
 *
 * Last Modified: 06/06/19 20:21
 */

package com.bcf.bcfstorefrontaddon.strategies.asm;

import de.hybris.platform.commercefacades.accommodation.GlobalSuggestionData;
import de.hybris.platform.commercefacades.travel.LocationData;
import de.hybris.platform.commercefacades.travel.PassengerInformationData;
import de.hybris.platform.commercefacades.travel.TravellerData;
import de.hybris.platform.commercefacades.travel.enums.SuggestionType;
import de.hybris.platform.commercefacades.travel.enums.TripType;
import de.hybris.platform.core.model.order.AbstractOrderEntryModel;
import de.hybris.platform.core.model.order.CartModel;
import de.hybris.platform.travelfacades.facades.TransportFacilityFacade;
import de.hybris.platform.travelfacades.facades.TravelLocationFacade;
import de.hybris.platform.travelfacades.facades.TravellerFacade;
import de.hybris.platform.travelfacades.facades.accommodation.AccommodationSuggestionFacade;
import de.hybris.platform.travelservices.enums.OrderEntryType;
import de.hybris.platform.travelservices.enums.TravellerType;
import de.hybris.platform.travelservices.model.order.AccommodationOrderEntryGroupModel;
import de.hybris.platform.travelservices.model.product.FareProductModel;
import de.hybris.platform.travelservices.utils.TravelDateUtils;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.stream.Collectors;
import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.collections4.MapUtils;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Required;
import com.bcf.bcfstorefrontaddon.constants.BcfstorefrontaddonWebConstants;
import com.bcf.core.constants.BcfCoreConstants;


/**
 * Redirect strategy for transport + accommodation journey responsible for building the required url parameters for transportation
 * and redirect to the fare selection page and for building the required url parameters for accommodation and set it in the
 * session.
 */
public class ASRedirectForTransportAccommodationStrategy extends AbstractASRedirectStrategy
		implements AssistedServiceRedirectByJourneyTypeStrategy
{
	private static final String FARE_SELECTION_ROOT_URL = "/fare-selection";
	private static final String DEFAULT_CART_REDIRECT = "/";
	private static final String DEFAULT_NUMBER_OF_ROOM = "1";
	private static final String FIRST_ROOM_NUMBER = "0";

	private TravellerFacade travellerFacade;
	private TransportFacilityFacade transportFacilityFacade;
	private TravelLocationFacade travelLocationFacade;
	private AccommodationSuggestionFacade accommodationSuggestionFacade;

	@Override
	public String getRedirectPath(final CartModel cartModel)
	{
		getSessionService().setAttribute(BcfCoreConstants.SESSION_BOOKING_JOURNEY,
				BcfstorefrontaddonWebConstants.BOOKING_TRANSPORT_ACCOMMODATION);

		final Map<String, String> urlParameters = getUrlParametersForTransportation(cartModel);
		if (MapUtils.isEmpty(urlParameters))
		{
			return DEFAULT_CART_REDIRECT;
		}

		final String accommodationQueryString = getUrlParametersForAccommodation(cartModel, urlParameters);
		getSessionService()
				.setAttribute(BcfstorefrontaddonWebConstants.ACCOMMODATION_QUERY_STRING, accommodationQueryString);

		final String urlParametersString = "?" + urlParameters.toString().replace(", ", "&").replace("{", "").replace("}", "");
		return StringUtils.isBlank(urlParametersString) ? DEFAULT_CART_REDIRECT : FARE_SELECTION_ROOT_URL + urlParametersString;
	}

	/**
	 * Returns the url parameters map with the attributes for the transport part.
	 *
	 * @param cartModel as the cart model
	 */
	protected Map<String, String> getUrlParametersForTransportation(final CartModel cartModel)
	{
		final Map<String, String> urlParameters = new HashMap<>();

		final List<AbstractOrderEntryModel> transportationEntries = cartModel.getEntries().stream()
				.filter(entry -> OrderEntryType.TRANSPORT.equals(entry.getType())
						&& FareProductModel._TYPECODE.equals(entry.getProduct().getItemtype()))
				.collect(Collectors.toList());

		final List<AbstractOrderEntryModel> accommodationEntries = cartModel.getEntries().stream()
				.filter(entry -> OrderEntryType.ACCOMMODATION.equals(entry.getType()))
				.collect(Collectors.toList());

		if (CollectionUtils.isEmpty(transportationEntries) || Objects.isNull(cartModel.getTripType()) ||
				(Objects.nonNull(cartModel.getTripType()) && TripType.SINGLE.toString().equals(cartModel.getTripType().getCode())
						&& CollectionUtils.isEmpty(accommodationEntries)))
		{
			return null;
		}

		final long legCount = transportationEntries.stream()
				.map(entry -> entry.getTravelOrderEntryInfo().getOriginDestinationRefNumber()).distinct().count();
		if (legCount < 2 && TripType.RETURN.toString().equals(cartModel.getTripType().getCode()))
		{
			return null;
		}

		// ORIGIN
		setOriginDetails(transportationEntries, urlParameters);

		// DESTINATION
		setDestinationDetails(transportationEntries, urlParameters);

		return populateUrlParameters(transportationEntries, urlParameters, legCount);
	}

	/**
	 * Returns the url parameters map with the attributes for the accommodation part.
	 *
	 * @param cartModel              as the cart model
	 * @param transportUrlParameters as the map with the url parameters for the transport part
	 */
	protected String getUrlParametersForAccommodation(final CartModel cartModel, final Map<String, String> transportUrlParameters)
	{
		final Map<String, String> accommodationUrlParameters = new HashMap<>();

		LocationData location = null;
		final String arrivalLocationSuggestionType = transportUrlParameters
				.get(BcfstorefrontaddonWebConstants.ARRIVAL_LOCATION_SUGGESTION_TYPE);
		final String arrivalLocation = transportUrlParameters.get(BcfstorefrontaddonWebConstants.ARRIVAL_LOCATION);
		if (StringUtils.isNotBlank(arrivalLocationSuggestionType))
		{
			if (StringUtils.equalsIgnoreCase(SuggestionType.AIRPORTGROUP.toString(), arrivalLocationSuggestionType))
			{
				location = getTransportFacilityFacade().getLocation(arrivalLocation);
			}
			else if (StringUtils.equalsIgnoreCase(SuggestionType.CITY.toString(), arrivalLocationSuggestionType))
			{
				location = getTravelLocationFacade().getLocation(arrivalLocation);
			}
		}

		final List<GlobalSuggestionData> suggestionResults = location != null
				?
				getAccommodationSuggestionFacade().getLocationSuggestions(location.getName()) :
				new ArrayList<>();
		if (CollectionUtils.isNotEmpty(suggestionResults))
		{
			final GlobalSuggestionData firstValidResult = suggestionResults.stream().findFirst().get();
			accommodationUrlParameters
					.put(BcfstorefrontaddonWebConstants.SUGGESTION_TYPE, SuggestionType.LOCATION.toString());
			final String encodedDestinationLocation = firstValidResult.getCode().replaceAll("\\|", "%7C");
			accommodationUrlParameters.put(BcfstorefrontaddonWebConstants.DESTINATION_LOCATION, encodedDestinationLocation);
			final String encodedDestinationLocationName = firstValidResult.getName().replaceAll("\\|", "%7C")
					.replaceAll(", ", "%2C%20").replaceAll(" ", "%20");
			accommodationUrlParameters
					.put(BcfstorefrontaddonWebConstants.DESTINATION_LOCATION_NAME, encodedDestinationLocationName);
		}

		final List<AbstractOrderEntryModel> accommodationEntries = cartModel.getEntries().stream()
				.filter(entry -> OrderEntryType.ACCOMMODATION.equals(entry.getType()))
				.collect(Collectors.toList());

		final String guestOccupancyString;
		if (CollectionUtils.isNotEmpty(accommodationEntries))
		{
			// Populate urlParameters from cart
			final List<AccommodationOrderEntryGroupModel> accommodationEntryGroups = accommodationEntries.stream()
					.map(AbstractOrderEntryModel::getEntryGroup).filter(AccommodationOrderEntryGroupModel.class::isInstance)
					.map(AccommodationOrderEntryGroupModel.class::cast).distinct().collect(Collectors.toList());

			accommodationUrlParameters.put(BcfstorefrontaddonWebConstants.CHECKIN_DATE, TravelDateUtils
					.convertDateToStringDate(accommodationEntryGroups.get(0).getStartingDate(),
							BcfstorefrontaddonWebConstants.DATE_FORMAT));
			accommodationUrlParameters.put(BcfstorefrontaddonWebConstants.CHECKOUT_DATE, TravelDateUtils
					.convertDateToStringDate(accommodationEntryGroups.get(0).getEndingDate(),
							BcfstorefrontaddonWebConstants.DATE_FORMAT));

			final long numberOfRooms = accommodationEntryGroups.stream().map
					(AccommodationOrderEntryGroupModel::getRoomStayRefNumber)
					.distinct().count();
			accommodationUrlParameters.put(BcfstorefrontaddonWebConstants.NUMBER_OF_ROOMS, String.valueOf(numberOfRooms));
			guestOccupancyString = getGuestOccupancy(cartModel);
		}
		else
		{
			// Populate urlParameters from transport info
			accommodationUrlParameters.put(BcfstorefrontaddonWebConstants.CHECKIN_DATE,
					transportUrlParameters.get(BcfstorefrontaddonWebConstants.DEPARTING_DATE_TIME));
			accommodationUrlParameters.put(BcfstorefrontaddonWebConstants.CHECKOUT_DATE,
					transportUrlParameters.get(BcfstorefrontaddonWebConstants.RETURN_DATE_TIME));
			accommodationUrlParameters.put(BcfstorefrontaddonWebConstants.NUMBER_OF_ROOMS, DEFAULT_NUMBER_OF_ROOM);

			guestOccupancyString = getGuestOccupancyFromTransport();
		}

		return "?" + accommodationUrlParameters.toString().replace(", ", "&").replace("{", "").replace("}", "") +
				guestOccupancyString;
	}

	/**
	 * Returns the string with the guest occupancies build from the cart.
	 *
	 * @param cartModel as the cart model
	 * @return the string with the guest occupancies
	 */
	protected String getGuestOccupancy(final CartModel cartModel)
	{
		final List<AbstractOrderEntryModel> accommodationEntries = cartModel.getEntries().stream()
				.filter(entry -> OrderEntryType.ACCOMMODATION.equals(entry.getType()))
				.collect(Collectors.toList());
		return getGuestOccupancy(accommodationEntries);
	}

	/**
	 * Returns the string with the guest occupancies build from the transport travellers.
	 *
	 * @return the string with the guest occupancies
	 */
	protected String getGuestOccupancyFromTransport()
	{
		final List<TravellerData> travellers = getTravellerFacade().getTravellersForCartEntries();
		final Map<String, Long> travellersMap = travellers.stream()
				.filter(traveller -> TravellerType.PASSENGER.getCode().equals(traveller.getTravellerType())).collect(Collectors
						.groupingBy(traveller -> ((PassengerInformationData) traveller.getTravellerInfo()).getPassengerType()
								.getCode(), Collectors.counting()));

		final String guestOccupancyString = travellersMap.entrySet().stream()
				.map(entry -> entry.getValue() + BcfstorefrontaddonWebConstants.HYPHEN + entry.getKey())
				.collect(Collectors.joining(","));

		return BcfstorefrontaddonWebConstants.AMPERSAND
				+ BcfstorefrontaddonWebConstants.ROOM_QUERY_STRING_INDICATOR + FIRST_ROOM_NUMBER
				+ BcfstorefrontaddonWebConstants.EQUALS + guestOccupancyString;
	}

	/**
	 * @return the travellerFacade
	 */
	@Override
	protected TravellerFacade getTravellerFacade()
	{
		return travellerFacade;
	}

	/**
	 * @param travellerFacade the travellerFacade to set
	 */
	@Override
	@Required
	public void setTravellerFacade(final TravellerFacade travellerFacade)
	{
		this.travellerFacade = travellerFacade;
	}

	/**
	 * @return the transportFacilityFacade
	 */
	protected TransportFacilityFacade getTransportFacilityFacade()
	{
		return transportFacilityFacade;
	}

	/**
	 * @param transportFacilityFacade the transportFacilityFacade to set
	 */
	@Required
	public void setTransportFacilityFacade(final TransportFacilityFacade transportFacilityFacade)
	{
		this.transportFacilityFacade = transportFacilityFacade;
	}

	/**
	 * @return the travelLocationFacade
	 */
	protected TravelLocationFacade getTravelLocationFacade()
	{
		return travelLocationFacade;
	}

	/**
	 * @param travelLocationFacade the travelLocationFacade to set
	 */
	@Required
	public void setTravelLocationFacade(final TravelLocationFacade travelLocationFacade)
	{
		this.travelLocationFacade = travelLocationFacade;
	}

	/**
	 * @return the accommodationSuggestionFacade
	 */
	protected AccommodationSuggestionFacade getAccommodationSuggestionFacade()
	{
		return accommodationSuggestionFacade;
	}

	/**
	 * @param accommodationSuggestionFacade the accommodationSuggestionFacade to set
	 */
	@Required
	public void setAccommodationSuggestionFacade(
			final AccommodationSuggestionFacade accommodationSuggestionFacade)
	{
		this.accommodationSuggestionFacade = accommodationSuggestionFacade;
	}
}
