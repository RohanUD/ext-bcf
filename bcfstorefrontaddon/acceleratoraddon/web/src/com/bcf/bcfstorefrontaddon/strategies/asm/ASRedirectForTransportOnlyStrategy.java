/*
 * Copyright (c) 2019 British Columbia Ferry Services Inc
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of British Columbia Ferry Services Inc("Confidential Information").
 * You shall not disclose such Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with British Columbia Ferry Services Inc.
 *
 * Last Modified: 06/06/19 20:21
 */

package com.bcf.bcfstorefrontaddon.strategies.asm;

import de.hybris.platform.commercefacades.travel.enums.TripType;
import de.hybris.platform.core.model.order.AbstractOrderEntryModel;
import de.hybris.platform.core.model.order.CartModel;
import de.hybris.platform.travelservices.enums.OrderEntryType;
import de.hybris.platform.travelservices.model.product.FareProductModel;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.stream.Collectors;
import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.lang.StringUtils;
import com.bcf.bcfstorefrontaddon.constants.BcfstorefrontaddonWebConstants;
import com.bcf.core.constants.BcfCoreConstants;


/**
 * Redirect strategy for transport only journey responsible for building the required url parameters and redirect to the
 * fare selection page.
 */
public class ASRedirectForTransportOnlyStrategy extends AbstractASRedirectStrategy
		implements AssistedServiceRedirectByJourneyTypeStrategy
{
	private static final String FARE_SELECTION_ROOT_URL = "/fare-selection";
	private static final String DEFAULT_CART_REDIRECT = "/";

	@Override
	public String getRedirectPath(final CartModel cartModel)
	{
		getSessionService().setAttribute(BcfCoreConstants.SESSION_BOOKING_JOURNEY,
				BcfstorefrontaddonWebConstants.BOOKING_TRANSPORT_ONLY);

		final String urlParameters = buildUrlParameters(cartModel);
		return StringUtils.isBlank(urlParameters) ? DEFAULT_CART_REDIRECT : FARE_SELECTION_ROOT_URL + urlParameters;
	}

	/**
	 * Returns the string with the url parameters required for the redirect to the fare-selection page.
	 *
	 * @param cartModel as the cart model
	 * @return the string of the url parameters
	 */
	protected String buildUrlParameters(final CartModel cartModel)
	{
		final Map<String, String> urlParameters = new HashMap<>();

		final List<AbstractOrderEntryModel> transportationEntries = cartModel.getEntries().stream()
				.filter(entry -> OrderEntryType.TRANSPORT.equals(entry.getType())
						&& FareProductModel._TYPECODE.equals(entry.getProduct().getItemtype()))
				.collect(Collectors.toList());

		if (CollectionUtils.isEmpty(transportationEntries) || Objects.isNull(cartModel.getTripType()))
		{
			return null;
		}

		final long legCount = transportationEntries.stream()
				.map(entry -> entry.getTravelOrderEntryInfo().getOriginDestinationRefNumber()).distinct().count();
		if (legCount < 2 && TripType.RETURN.toString().equals(cartModel.getTripType().getCode()))
		{
			return null;
		}

		// ORIGIN
		setOriginDetails(transportationEntries, urlParameters);

		// DESTINATION
		setDestinationDetails(transportationEntries, urlParameters);

		populateUrlParameters(transportationEntries, urlParameters, legCount);

		return "?" + urlParameters.toString().replace(", ", "&").replace("{", "").replace("}", "");
	}
}
