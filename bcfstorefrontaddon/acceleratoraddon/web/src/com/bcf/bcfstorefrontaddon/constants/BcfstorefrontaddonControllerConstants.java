/*
 * Copyright (c) 2019 British Columbia Ferry Services Inc
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of British Columbia Ferry Services Inc("Confidential Information").
 * You shall not disclose such Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with British Columbia Ferry Services Inc.
 *
 * Last Modified: 01/07/19 15:16
 */

package com.bcf.bcfstorefrontaddon.constants;


import com.bcf.bcfstorefrontaddon.model.components.ActivityProductCarouselComponentModel;
import com.bcf.bcfstorefrontaddon.model.components.BCFCenterBodyContentComponentModel;
import com.bcf.bcfstorefrontaddon.model.components.BCFContactUsCarouselComponentModel;
import com.bcf.bcfstorefrontaddon.model.components.BCFCopyrightComponentModel;
import com.bcf.bcfstorefrontaddon.model.components.BCFHeaderComponentModel;
import com.bcf.bcfstorefrontaddon.model.components.BCFHeaderContentComponentModel;
import com.bcf.bcfstorefrontaddon.model.components.BCFImageCarouselComponentModel;
import com.bcf.bcfstorefrontaddon.model.components.BCFLeftBodyContentComponentModel;
import com.bcf.bcfstorefrontaddon.model.components.BCFLocationCarouselComponentModel;
import com.bcf.bcfstorefrontaddon.model.components.BCFLocationComponentModel;
import com.bcf.bcfstorefrontaddon.model.components.BCFMapItineraryComponentModel;
import com.bcf.bcfstorefrontaddon.model.components.BCFMultimediaTextComponentModel;
import com.bcf.bcfstorefrontaddon.model.components.BCFNavigationBarComponentModel;
import com.bcf.bcfstorefrontaddon.model.components.BCFNewsCarouselComponentModel;
import com.bcf.bcfstorefrontaddon.model.components.BCFOffersCarouselComponentModel;
import com.bcf.bcfstorefrontaddon.model.components.BCFPromotionContentComponentModel;
import com.bcf.bcfstorefrontaddon.model.components.BCFResponsiveMediaComponentModel;
import com.bcf.bcfstorefrontaddon.model.components.BCFRightBodyCarouselComponentModel;
import com.bcf.bcfstorefrontaddon.model.components.BCFRightBodyContentComponentModel;
import com.bcf.bcfstorefrontaddon.model.components.BCFTerminalListComponentModel;
import com.bcf.bcfstorefrontaddon.model.components.BCFVacationDealComponentModel;
import com.bcf.bcfstorefrontaddon.model.components.BCFVacationPackageComponentModel;
import com.bcf.bcfstorefrontaddon.model.components.BookingTotalComponentModel;
import com.bcf.bcfstorefrontaddon.model.components.ManageMyBookingComponentModel;
import com.bcf.bcfstorefrontaddon.model.components.ReservationTotalsComponentModel;
import com.bcf.core.model.component.ShipInfoRendererComponentModel;
import com.bcf.core.model.components.CMSAlertParagraphComponentModel;


/**
 *
 */
public interface BcfstorefrontaddonControllerConstants
{

	String ADDON_PREFIX = "addon:/bcfstorefrontaddon/";

	/**
	 * Class with view name constants
	 */
	interface Views
	{

		interface Pages
		{

			interface Account
			{
				String AccountPage = ADDON_PREFIX + "pages/account/accountLayoutPage";

				String ProfilePage = ADDON_PREFIX + "pages/account/myProfile";
			}

			interface QuoteListing
			{
				String QuoteListing = ADDON_PREFIX + "pages/quotelisting/quotelListing";
			}

			interface Checkout
			{
				String CheckoutLoginPage = ADDON_PREFIX + "pages/checkout/checkoutLoginPage";
				String SelectPaymentResponse = ADDON_PREFIX + "pages/checkout/selectPaymentResponse";
			}

			interface Order
			{
				String OrderConfirmationPage = ADDON_PREFIX + "pages/order/bookingConfirmationPage";
				String ASMOrderActionResponse = ADDON_PREFIX + "pages/order/asmOrderActionResponse";
			}

			interface CurrentConditions
			{
				String CurrentConditionsPage = ADDON_PREFIX + "pages/currentconditions/getCurrentConditions";
			}

			interface BookingDetails
			{
				String cancelOrderResponse = ADDON_PREFIX + "pages/managebooking/cancelOrderResponse";
				String cancelOrderConfirmResponse = ADDON_PREFIX + "pages/managebooking/cancelOrderConfirmResponse";
				String confirmEbookingCancellation = ADDON_PREFIX + "pages/managebooking/confirmEbookingCancellation";
				String isAmendedCartResponse = ADDON_PREFIX + "pages/managebooking/checkAmendedCartInSessionResponse";
			}

			interface TransportBookings
			{
				String getTransportBookingsResponse = ADDON_PREFIX + "pages/managebooking/getTransportBookingsResponse";
				String getTransportBusinessBookingsResponse =
						ADDON_PREFIX + "pages/managebooking/getTransportBusinessBookingsResponse";
				String transportBookingsResponseJson = ADDON_PREFIX + "pages/managebooking/transportBookingsResponseJson";
			}

			interface BookingConfirmation
			{
				String sendItineraryResponse = ADDON_PREFIX + "pages/managebooking/sendItineraryResponse";
			}

			interface Refund
			{
				String multiRefundOptionPage = ADDON_PREFIX + "pages/refund/multiRefundOptionPage";
			}

			interface GetAQuoteListing
			{
				String getAQuoteListing = ADDON_PREFIX + "pages/getaquotelisting/getAQuoteListing";
			}

			interface Content
			{
				String ArticlePage = ADDON_PREFIX + "pages/layout/articleLayoutPage";
			}

			interface Error // NOSONAR
			{
				String ErrorNotFoundPage = "pages/error/errorNotFoundPage"; // NOSONAR
			}
		}

		interface Cms // NOSONAR
		{
			String ComponentPrefix = "cms/"; // NOSONAR
		}
	}

	interface Actions
	{

		interface Cms
		{

			String _Prefix = "/view/";

			String _Suffix = "Controller";

			/**
			 * CMS components that have specific handlers
			 */
			String ManageMyBookingComponent = _Prefix + ManageMyBookingComponentModel._TYPECODE + _Suffix;
			String ReservationTotalsComponent = _Prefix + ReservationTotalsComponentModel._TYPECODE + _Suffix;
			String BookingTotalComponent = _Prefix + BookingTotalComponentModel._TYPECODE + _Suffix;
			String BCFMultimediaTextComponent = _Prefix + BCFMultimediaTextComponentModel._TYPECODE + _Suffix;
			String BCFCopyrightComponent = _Prefix + BCFCopyrightComponentModel._TYPECODE + _Suffix;
			String BCFResponsiveMediaComponent = _Prefix + BCFResponsiveMediaComponentModel._TYPECODE + _Suffix;
			String BCFImageCarouselComponent = _Prefix + BCFImageCarouselComponentModel._TYPECODE + _Suffix;
			String BCFTerminalListComponent = _Prefix + BCFTerminalListComponentModel._TYPECODE + _Suffix;
			String BCFLocationCarouselComponent = _Prefix + BCFLocationCarouselComponentModel._TYPECODE + _Suffix;
			String BCFOffersCarouselComponent = _Prefix + BCFOffersCarouselComponentModel._TYPECODE + _Suffix;
			String BCFNewsCarouselComponent = _Prefix + BCFNewsCarouselComponentModel._TYPECODE + _Suffix;
			String BCFVacationPackageComponent = _Prefix + BCFVacationPackageComponentModel._TYPECODE + _Suffix;
			String BCFHeaderComponent = _Prefix + BCFHeaderComponentModel._TYPECODE + _Suffix;
			String BCFLeftBodyContentComponent = _Prefix + BCFLeftBodyContentComponentModel._TYPECODE + _Suffix;
			String BCFRightBodyCarouselComponent = _Prefix + BCFRightBodyCarouselComponentModel._TYPECODE + _Suffix;
			String BCFMapItineraryComponent = _Prefix + BCFMapItineraryComponentModel._TYPECODE + _Suffix;
			String BCFRightBodyContentComponent = _Prefix + BCFRightBodyContentComponentModel._TYPECODE + _Suffix;
			String BCFCenterBodyContentComponent = _Prefix + BCFCenterBodyContentComponentModel._TYPECODE + _Suffix;
			String BCFHeaderContentComponent = _Prefix + BCFHeaderContentComponentModel._TYPECODE + _Suffix;
			String ActivityProductCarouselComponent =
					_Prefix + ActivityProductCarouselComponentModel._TYPECODE + _Suffix;
			String BCFNavigationBarComponent = _Prefix + BCFNavigationBarComponentModel._TYPECODE + _Suffix;
			String BCFContactUsCarouselComponent = _Prefix + BCFContactUsCarouselComponentModel._TYPECODE + _Suffix;
			String BCFPromotionContentComponent = _Prefix + BCFPromotionContentComponentModel._TYPECODE + _Suffix;
			String BCFLocationComponent = _Prefix + BCFLocationComponentModel._TYPECODE + _Suffix;
			String BCFVacationsDealComponent = _Prefix + BCFVacationDealComponentModel._TYPECODE + _Suffix;
			String CMSAlertParagraphComponent = _Prefix + CMSAlertParagraphComponentModel._TYPECODE + _Suffix;
			String ShipInfoRendererComponent = _Prefix + ShipInfoRendererComponentModel._TYPECODE + _Suffix;
		}
	}


	interface ModelAttributes
	{
		interface AccountPages
		{
			String TITLE = "accountPageTitle";
		}

		interface ActivityPages
		{
			String IS_CHANGE_ACTIVITY = "changeActivity";
			String ACTIVITY_TO_CHANGE_NAME_KEY = "changeActivityCode";
			String ACTIVITY_TO_CHANGE_DATE_KEY = "changeActivityDate";
			String ACTIVITY_TO_CHANGE_TIME_KEY = "changeActivityTime";
			String ACTIVITY_TO_CHANGE_REF = "changeActivityRef";

		}
	}
}
