/*
 * Copyright (c) 2019 British Columbia Ferry Services Inc
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of British Columbia Ferry Services Inc("Confidential Information").
 * You shall not disclose such Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with British Columbia Ferry Services Inc.
 *
 * Last Modified: 01/07/19 15:16
 */

package com.bcf.bcfstorefrontaddon.constants;

/**
 * Global class for all Bcfstorefrontaddon web constants. You can add global constants for your extension into this
 * class.
 */
public final class BcfstorefrontaddonWebConstants
{
	public static final String TRAVELLER_DETAILS_PATH = "/traveller-details";
	public static final String PERSONAL_DETAILS_PATH = "/checkout/personal-details";
	public static final String PAYMENT_DETAILS_PATH = "/checkout/multi/payment-method/select-flow?pci=";
	public static final String SUMMARY_DETAILS_PATH = "/checkout/multi/summary/view";
	public static final String PAYMENT_TYPE_PATH = "/checkout/multi/payment-type/choose";
	public static final String PAYMENT_FLOW = "ht";
	public static final String GUEST_DETAILS_PATH = "/checkout/guest-details";
	public static final String MANAGE_BOOKING_URL = "manage-booking";
	public static final String BOOKING_DETAILS_URL = "/manage-booking/booking-details/";
	public static final String CHECKOUT_LOGIN = "/login/checkout";
	public static final String HOME_PAGE_PATH = "/";
	public static final String LOGIN_PAGE_PATH = "/login";
	public static final String LOGIN_PAGE_PATH_ONLY = "/login?loginToCheckout=true";
	public static final String LOGIN_TO_CHECKOUT_URL_KEY = "loginToCheckoutUrl";
	public static final String ANCILLARY_EXTRAS_AMENDMENT_PATH = "/manage-booking/ancillary-extras";
	public static final String ANCILLARY_EXTRAS_PATH = "/ancillary-extras";
	public static final String ANCILLARY_AMENDMENT_PAGE = "/manage-booking/ancillary/amendment";
	public static final String PACKAGE_DETAILS_AMENDMENT_PAGE = "/manage-booking/amend-package-details/";
	public static final String ADD_ROOM_PACKAGE_DETAILS_PAGE = "/cart/accommodation/package-add";
	public static final String ADD_ROOM_PACKAGE_DETAILS_AMENDMENT_PAGE = "/cart/accommodation/amend-package-add";
	public static final String ADD_ACCOMMODATION_DETAILS_PAGE = "/cart/accommodation/add";
	public static final String BasketSummaryPage = "/cart/summary";
	public static final String PACKAGE_FERRY_INFO_PAGE_PATH = "/package-ferry-passenger-info";
	public static final String PACKAGE_FERRY_SELECTION_PAGE_PATH = "/package-select-ferry";
	public static final String PACKAGE_FERRY_REVIEW_PAGE_PATH = "/package-review-ferries";
	public static final String FARE_SELECTION_REVIEW_PAGE_PATH = "/fare-selection-review";
	public static final String ALACARTE_REVIEW_PAGE_PATH = "/alacarte-review";
	public static final String PAYMENT_PAGE_PATH = "/checkout/multi/payment-method/select-flow";

	// MMB constants

	public static final String MANAGE_MY_BOOKING_AUTHENTICATION = "manage_my_booking_authentication";
	public static final String MANAGE_MY_BOOKING_GUEST_UID = "manage_my_booking_guest_uid";
	public static final String MANAGE_MY_BOOKING_BOOKING_REFERENCE = "manage_my_booking_booking_reference";

	public static final String PROMOTION_CODE = "promotionCode";
	public static final String DESTINATION_LOCATION_IMAGE = "destinationLocationImage";
	public static final String ACCOMMODATION_OFFERING_CODE = "accommodationOfferingCode";
	public static final String RESERVATION_TOTAL = "reservationTotal";
	public static final String SELECTED_PAYMENT = "paying";

	public static final String ACCOMMODATION_MAX_STOCK_LEVEL = "accommodationMaxStockLevel";
	public static final String BOOKING_REFERENCE = "bookingReference";
	public static final String BCF_BOOKING_REFERENCES = "bcfBookingReferences";
	public static final String RESERVATION_CODE = "reservationCode";

	public static final String IS_CANCEL_POSSIBLE = "isCancelPossible";
	public static final String IS_AMENDMENTCART = "isAmendmentCart";
	public static final String TOTAL_TO_REFUND = "totalToRefund";
	public static final String CANCEL_FEE = "cancelFee";
	public static final String REFUND_RESULT = "refundResult";
	public static final String REFUNDED_AMOUNT = "refundedAmount";
	public static final String CANCELLATION_RESULT = "cancellationResult";
	public static final String GOOD_WILL_REFUND_RESULT = "goodWillRefundResult";
	public static final String CANCELLATION_PARAMETER = "cancellationParameter";
	public static final String CANCELLED_TRAVELLER = "cancelledTraveller";
	public static final String TOTAL_TO_PAY = "totalToPay";
	public static final String BOOKING_ACTION_RESPONSE = "bookingActionResponse";
	public static final String EMPTY_CANCEL_ORDER_REQUEST = "emptyCancelOrderRequest";
	public static final String ONLY_TRANSPORT_BOOKING_CANCELLATION = "text.page.multi.cancelbooking.not.transport"
			+ ".only.booking";

	public static final String PAYMENT_TRANSACTIONS = "paymentTransactions";

	public static final String ACCOMMODATION = "ACCOMMODATION";
	public static final String TRANSPORT = "TRANSPORT";

	public static final String SESSION_TRIP_TYPE = "tripType";
	public static final String BOOKING_JOURNEY = "bookingJourney";
	public static final String BOOKING_TRANSPORT_ONLY = "BOOKING_TRANSPORT_ONLY";
	public static final String BOOKING_ACCOMMODATION_ONLY = "BOOKING_ACCOMMODATION_ONLY";
	public static final String BOOKING_TRANSPORT_ACCOMMODATION = "BOOKING_TRANSPORT_ACCOMMODATION";
	public static final String BOOKING_ALACARTE = "BOOKING_ALACARTE";
	public static final String BOOKING_PACKAGE = "BOOKING_PACKAGE";
	public static final String BOOKING_ACTIVITY_ONLY = "BOOKING_ACTIVITY_ONLY";
	public static final String ACCOMMODATION_QUERY_STRING = "accommodationQueryString";

	public static final String SELECT_PAYMENT_OPTION_RESPONSE_DATA = "selectPaymentOptionResponseData";
	public static final String SELECT_PAYMENT_JS_ERROR = "payment.option.select.js.error";
	public static final String ACCOMMODATION_FINDER_FORM = "accommodationFinderForm";
	public static final String GET_A_QUOTE_FORM = "getAQuoteForm";
	public static final String PRODUCT_NAME = "productName";
	public static final String SESSION_FARE_FINDER_FORM = "fareFinderForm";
	public static final String SESSION_FARE_SELECTION_DATA = "sessionFareSelection";
	public static final String ACCOMMODATION_SEARCH_RESPONSE_PROPERTIES = "accommodationSearchResponseProperties";
	public static final String PACKAGE_SEARCH_RESPONSE_PROPERTIES = "packageSearchResponseProperties";
	public static final String ACCOMMODATION_SEARCH_RESPONSE = "accommodationSearchResponse";
	public static final String ACCOMMODATION_LISTING_PAGE_RESULT_VIEW_TYPE = "resultsViewType";
	public static final String ACCOMMODATION_LISTING_PAGE_RESULT_VIEW_TYPE_DEFAULT = "listView";
	public static final String ACCOMMODATION_LISTING_PAGE_RESULT_VIEW_TYPE_GRID = "GridView";
	public static final String ACCOMMODATION_LISTING_PAGE_RESULT_VIEW_TYPE_MAP = "MapView";
	public static final String MAX_ALLOWED_CHECKIN_CHECKOUT_DATE_DIFFERENCE = "accommodation.calender.checkin.checkout.difference.max";
	public static final String AGENT_DECLARE_PERIOD = "agent.declare.max.period";
	public static final String AMEND_BOOKING_RESULT = "amendBookingResult";
	public static final String AMEND_BOOKING_REFUNDED_AMOUNT = "amendBookingRefundAmount";
	public static final String AMEND = "amend";
	public static final String UNLINK_RESULT = "unlinkResult";

	public static final String SESSION_PAY_NOW = "sessionPayNow";
	public static final String SESSION_CHANGE_DATES = "sessionChangeDates";

	public static final String PAGE_NOT_AVAILABLE = "page.not.available";
	public static final String PAGE_TEMPORARY_NOT_AVAILABLE = "page.temporary.not.available";
	public static final String GOOGLE_API_KEY = "googleAPIKey";

	public static final String PAYMENT_PAID = "partialPaymentPaid";
	public static final String PAYMENT_DUE = "partialPaymentDue";
	public static final String NOT_REFUNDABLE = "notRefundable";
	public static final String PAY_AT_TERMINAL = "payAtTerminal";

	public static final String CART_TOTAL = "cartTotal";

	public static final String ADD_BUNDLE_TO_CART_URL = "addBundleToCartUrl";

	public static final String TOTAL = "total";
	public static final String EQUALS = "=";
	public static final String HYPHEN = "-";
	public static final String AMPERSAND = "&";
	public static final String COMMA = ",";
	public static final String QUESTION_MARK = "?";
	public static final String BACK_SLASH = "/";

	public static final String CHECKIN_DATE = "checkInDateTime";
	public static final String CHECKOUT_DATE = "checkOutDateTime";
	public static final String NUMBER_OF_ROOMS = "numberOfRooms";
	public static final String PART_HOTEL_STAY = "partHotelStay";

	public static final String DEPARTURE_LOCATION = "departureLocation";
	public static final String DEPARTURE_LOCATION_SUGGESTION_TYPE = "departureLocationSuggestionType";
	public static final String DEPARTURE_LOCATION_NAME = "departureLocationName";
	public static final String ARRIVAL_LOCATION_NAME = "arrivalLocationName";
	public static final String ARRIVAL_LOCATION_SUGGESTION_TYPE = "arrivalLocationSuggestionType";
	public static final String ARRIVAL_LOCATION = "arrivalLocation";
	public static final String DEPARTING_DATE_TIME = "departingDateTime";
	public static final String RETURN_DATE_TIME = "returnDateTime";
	public static final String CABIN_CLASS = "cabinClass";
	public static final String TRIP_TYPE = "tripType";

	public static final String DESTINATION_LOCATION_NAME = "destinationLocationName";
	public static final String DESTINATION_LOCATION = "destinationLocation";
	public static final String SUGGESTION_TYPE = "suggestionType";
	public static final String ROOM_QUERY_STRING_INDICATOR = "r";

	public static final String SESSION_PREVIOUS_CURRENCY = "previousCurrency";

	public static final String MY_ACCOUNT_BOOKING_ACCOMMODATION_ROOM_MAPPING = "accommodationRoomMapping";
	public static final String DATE_PATTERN = "datePattern";
	public static final String DATE_FORMAT = "dd/MM/yyyy";

	public final static String SEASONAL_SCHEDULE_DATE_FORMAT_MMM_DD_YYYY = "MMMM dd yyyy";
	public final static String SEASONAL_SCHEDULE_DATE_FORMAT_YYYY_MM_DD = "yyyyMMdd";
	public static final String DEFAULT_ROOM_BED_PREFERENCE_CODE = "defaultRoomBedPreferenceCode";

	public static final String DEAL_BUNDLE_TEMPLATE_ID = "dealBundleTemplateId";
	public static final String DEAL_SELECTED_DEPARTURE_DATE = "dealSelectedDepartureDate";

	public static final String QUOTE_EXPIRATION_TIME = "quoteExpirationTime";
	public static final String AMOUNT_TO_PAY = "amountToPay";
	public static final String REMOVED_SAILINGS_FROM_CART = "removedSailingFromCart";
	public static final String CANCEL_BOOKING_FAILURE_SAILINGS_FROM_CART = "cancelBookingFailureSailings";

	public static final String AMEND_TRAVELLERS_JOURNEY_REF_NUM = "amendTravellersJourneyRefNum";
	public static final String AMEND_TRAVELLERS_ORIGIN_DESTINATION_REF_NUM = "amendTravellersOriginDestinationRefNum";

	public static final String ALLOW_AMEND_BOOKING_FEES = "allowAmendBookingFees";
	public static final String PAYMENT_FLOW_PROPERTY = "payment.flow";
	public static final String BCF_TRAVELLER_DATA = "bcfTravellersData";
	public static final String PERSONAL_DETAILS_FORM = "personalDetailsForms";
	public static final String CURRENT_PAGE = "currentPage";
	public static final String IS_CHECK_IN_JOURNEY = "isCheckInJourney";

	public static final String BCF_CALANDER_DATE_FORMAT = "MMM dd yyyy";
	public static final String GLOBAL_RESERVATION_DATA = "globalReservationDataList";
	public static final String GLOBAL_RESERVATION_TOTAL_DATA = "totalFare";
	public static final String RESERVATION_ITINERARY_DATE_FORMAT = "reservationItineraryDateFormat";
	public static final String ACCOUNT_LOCKED = "ACCOUNT_LOCKED";
	public static final String NEXT_URL = "nextUrl";
	public static final String TRAVEL_FINDER_FORM = "travelFinderForm";
	public static final String ACTIVITY_URL = "activityUrl";
	public static final String PACKAGE_AVAILABILITY_STATUS = "availabilityStatus";
	public static final String GET_A_QUOTE_ERROR_FLAG = "getAQuoteErrorFlag";
	public static final String GET_A_QUOTE_BINDING_RESULT = "org.springframework.validation.BindingResult.getAQuoteForm";
	public static final String BCF_UPDATE_EMAIL_FORM_BINDING_RESULT = "org.springframework.validation.BindingResult.updateEmailForm";
	public static final String GET_A_QUOTE_TITLE = "Create Quote";
	public static final String GET_A_QUOTE_SUCCESS_MESSAGE = "getaquote.form.success";
	public static final String PASSENGER_TYPE_CODE_INFANT = "infant";
	public static final String PASSENGER_TYPE_CODE_CHILD = "child";
	public static final String PASSENGER_TYPE_CODE_ADULT = "adult";
	public static final String PAGE_LABEL = "pageLabel";
	public static final String AGENT_PENDING_ORDER_DECISION_FORM = "agentPendingOrderDecisionForm";
	public static final String AGENT_PENDING_ORDER_APPROVAL_STATUS_FORM = "agentPendingOrderApprovalStatusForm";

	public static final String ACCOUNT_ACTIVATION_KEY = "accountActivationKey";
	public static final String ACCOUNT_UPDATE_OLD_EMAIL = "oldEmailId";

	public static final String TERMS_AND_CONDITIONS = "termsAndConditions";

	public static final String TRANSPORT_BOOKINGS = "transport-bookings";

	public static final String ACTIVITY_REMOVED = "activityRemoved";
	public static final String ACTIVITIES_PER_PAGE = "activitiesPerPage";
	public static final String MODIFY = "modify";

	public static final String SERVICE_NOT_AVAILABLE = "text.service.notAvailable";
	public static final String SERVICE_NOT_ACTIVATED = "text.service.notActivated";
	public static final String INVALID_EMAIL_ID = "invalid.email.id";

	public static final String REGION_CODE = "regionCode";
	public static final String REGIONS = "regions";
	public static final String CITY_CODE = "cityCode";
	public static final String IS_REGION_DETAILS_PAGE = "isRegionDetailsPage";
	public static final String ACTIVITIES = "activities";
	public static final String CITY = "City";
	public static final String DESTINATION_DETAILS = "destinationDetails";
	public static final String DESTINATION_PACKAGES = "destinationPackages";
	public static final String DESTINATION_ACCOMMODATIONS = "destinationAccommodations";
	public static final String FALSE = "false";

	private BcfstorefrontaddonWebConstants()
	{
		// empty to avoid instantiating this constant class
	}

}
