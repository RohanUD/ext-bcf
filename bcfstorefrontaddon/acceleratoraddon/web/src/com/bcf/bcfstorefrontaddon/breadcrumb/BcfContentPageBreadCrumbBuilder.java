/*
 * Copyright (c) 2019 British Columbia Ferry Services Inc
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of British Columbia Ferry Services Inc("Confidential Information").
 * You shall not disclose such Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with British Columbia Ferry Services Inc.
 *
 * Last Modified: 21/06/19 00:21
 */

package com.bcf.bcfstorefrontaddon.breadcrumb;

import de.hybris.platform.acceleratorstorefrontcommons.breadcrumb.Breadcrumb;
import de.hybris.platform.acceleratorstorefrontcommons.breadcrumb.impl.ContentPageBreadcrumbBuilder;
import de.hybris.platform.cms2.model.pages.ContentPageModel;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Objects;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Required;
import com.bcf.core.util.StreamUtil;
import com.bcf.custom.propertysource.facade.PropertySourceFacade;


public class BcfContentPageBreadCrumbBuilder extends ContentPageBreadcrumbBuilder
{
	private static final String HOME_URL = "/";
	private static final String HOME_LABEL = "text.link.home.label";
	private PropertySourceFacade propertySourceFacade;

	@Override
	public List<Breadcrumb> getBreadcrumbs(final ContentPageModel page)
	{
		if (Objects.isNull(page) || page.isHomepage() || page.isHideBreadcrumb())
		{
			return Collections.emptyList();
		}
		final List<Breadcrumb> breadcrumbs = new ArrayList<>();
		ContentPageModel parentPage = page.getParentPage();
		while (Objects.nonNull(parentPage) && !parentPage.isHomepage())
		{
			if (!addUniqueBreadcrumb(breadcrumbs, parentPage))
			{
				break;
			}
			parentPage = parentPage.getParentPage();
		}
		addBreadcrumb(breadcrumbs, getPropertySourceFacade().getPropertySourceValue(HOME_LABEL), HOME_URL);
		Collections.reverse(breadcrumbs);
		return breadcrumbs;
	}

	private boolean addUniqueBreadcrumb(final List<Breadcrumb> breadcrumbs, final ContentPageModel parentPage)
	{
		if (isBreadCrumbAdded(breadcrumbs, parentPage.getLabel()))
		{
			return false;
		}
		addBreadcrumb(breadcrumbs, parentPage.getName(), parentPage.getLabel());
		return true;
	}

	private boolean isBreadCrumbAdded(final List<Breadcrumb> breadcrumbs, final String breadCrumbUrl)
	{
		return StreamUtil.safeStream(breadcrumbs)
				.map(Breadcrumb::getUrl)
				.filter(url -> Objects.equals(url, breadCrumbUrl))
				.findAny()
				.isPresent();
	}

	private void addBreadcrumb(final List<Breadcrumb> breadcrumbs, final String parentPageName, final String parentPageLabel)
	{
		breadcrumbs.add(new Breadcrumb(parentPageLabel, parentPageName, StringUtils.EMPTY));
	}

	public PropertySourceFacade getPropertySourceFacade()
	{
		return propertySourceFacade;
	}

	@Required
	public void setPropertySourceFacade(final PropertySourceFacade propertySourceFacade)
	{
		this.propertySourceFacade = propertySourceFacade;
	}
}
