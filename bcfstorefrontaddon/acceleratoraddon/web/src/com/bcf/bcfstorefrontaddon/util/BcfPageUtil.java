/*
 * Copyright (c) 2019 British Columbia Ferry Services Inc
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of British Columbia Ferry Services Inc("Confidential Information").
 * You shall not disclose such Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with British Columbia Ferry Services Inc.
 *
 * Last Modified: 02/07/19 12:41
 */

package com.bcf.bcfstorefrontaddon.util;

import de.hybris.platform.acceleratorservices.storefront.data.MetaElementData;
import de.hybris.platform.acceleratorservices.storefront.util.PageTitleResolver;
import de.hybris.platform.acceleratorstorefrontcommons.breadcrumb.impl.ContentPageBreadcrumbBuilder;
import de.hybris.platform.acceleratorstorefrontcommons.constants.WebConstants;
import de.hybris.platform.acceleratorstorefrontcommons.controllers.ThirdPartyConstants;
import de.hybris.platform.cms2.model.pages.AbstractPageModel;
import de.hybris.platform.cms2.model.pages.ContentPageModel;
import de.hybris.platform.commercefacades.travel.TravelAdvisoryData;
import de.hybris.platform.commerceservices.i18n.CommerceCommonI18NService;
import de.hybris.platform.servicelayer.config.ConfigurationService;
import de.hybris.platform.servicelayer.session.SessionService;
import java.lang.reflect.InvocationTargetException;
import java.text.MessageFormat;
import java.util.LinkedList;
import java.util.List;
import java.util.Objects;
import java.util.Optional;
import java.util.function.Predicate;
import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import org.apache.commons.lang.StringEscapeUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.stereotype.Component;
import org.springframework.ui.Model;
import org.springframework.validation.support.BindingAwareModelMap;
import com.bcf.bcfstorefrontaddon.model.components.BCFOGGraphTagsModel;
import com.bcf.core.constants.BcfCoreConstants;
import com.bcf.core.enums.TravelAdvisoryDisplayType;
import com.bcf.core.model.MatomoModel;
import com.bcf.core.services.matomo.MatomoService;
import com.bcf.core.util.StreamUtil;
import com.bcf.facades.constants.BcfFacadesConstants;
import com.bcf.facades.metatags.BCFOGGraphTagData;
import com.bcf.facades.populators.BCFOGGraphTagsPopulator;
import com.bcf.facades.traveladvisory.TravelAdvisoryFacade;


@Component("bcfPageUtil")
public class BcfPageUtil
{
	private static final Logger LOG = Logger.getLogger(BcfPageUtil.class);

	public static final String CMS_PAGE_TITLE = "pageTitle";
	public static final String CMS_PAGE_MODEL = "cmsPage";
	public static final String CURRENT_PAGE_URL = "currentPageUrl";
	public static final String PREVIOUS_PAGE_URL = "previousPageUrl";
	private static final String GET_ROUTE_INFO_FORM = "getRouteInfoForm";
	private static final String GET_DEPARTURE_LOCATION = "getDepartureLocation";
	private static final String GET_FARE_FINDER_FORM = "getFareFinderForm";
	private static final String GET_ARRIVAL_LOCATION = "getArrivalLocation";
	private static final String REQUEST = "request";
	private static final String HOME_PAGE_TAKE_OVER_ADVISORY = "homePageTakeOverAdvisory";
	private static final String QUERY_PARAMS = "?";

	@Resource(name = "bcfOGGraphTagsPopulator")
	private BCFOGGraphTagsPopulator bcfOGGraphTagsPopulator;

	@Resource(name = "pageTitleResolver")
	private PageTitleResolver pageTitleResolver;

	@Resource(name = "travelAdvisoryFacade")
	private TravelAdvisoryFacade travelAdvisoryFacade;

	@Resource(name = "configurationService")
	private ConfigurationService configurationService;

	@Resource(name = "sessionService")
	private SessionService sessionService;

	@Resource(name = "contentPageBreadcrumbBuilder")
	private ContentPageBreadcrumbBuilder contentPageBreadcrumbBuilder;

	@Resource(name = "matomoService")
	private MatomoService matomoService;

	@Resource(name = "commerceCommonI18NService")
	private CommerceCommonI18NService commerceCommonI18NService;

	public void storeCmsPageInModel(final Model model, final AbstractPageModel cmsPage)
	{
		if (model != null && cmsPage != null)
		{
			model.addAttribute(CMS_PAGE_MODEL, cmsPage);
			if (cmsPage instanceof ContentPageModel)
			{
				final ContentPageModel contentPageModel = (ContentPageModel) cmsPage;
				model.addAttribute(WebConstants.BREADCRUMBS_KEY, contentPageBreadcrumbBuilder.getBreadcrumbs(contentPageModel));
				if (Objects.nonNull(contentPageModel.getOgGraphData()))
				{
					final BCFOGGraphTagData bcfogGraphTagData = new BCFOGGraphTagData();
					bcfOGGraphTagsPopulator.populate(contentPageModel.getOgGraphData(), bcfogGraphTagData);
					model.addAttribute(BcfCoreConstants.OG_GRAPH_DATA, bcfogGraphTagData);
				}

				final HttpServletRequest request = HttpServletRequest.class.cast(((BindingAwareModelMap) model).get("request"));
				if (StringUtils.isEmpty(contentPageModel.getCanonicalUrl()))
				{
					model.addAttribute(BcfCoreConstants.CANONICAL_URL, request.getRequestURL().toString());
				}
				else
				{
					model.addAttribute(BcfCoreConstants.CANONICAL_URL, (contentPageModel.getCanonicalUrl()));
				}
				storeTravelAdvisories(model, contentPageModel);
				addMatomoScripts(model);
				final HttpSession session = request.getSession();
				if (!Objects.equals(request.getRequestURL().toString(), session.getAttribute(CURRENT_PAGE_URL)))
				{
					session.setAttribute(PREVIOUS_PAGE_URL, session.getAttribute(CURRENT_PAGE_URL));
				}
				session.setAttribute(CURRENT_PAGE_URL, getCurrentPageUrl(request));
				sessionService.setAttribute(CURRENT_PAGE_URL, request.getServletPath());
				model.addAttribute(BcfCoreConstants.DISPLAY_ORDER_ID, contentPageModel.isDisplayOrderId());
			}
		}
	}

	public void setUpMetaDataForContentPage(final Model model, final ContentPageModel contentPage,
			final String... arguments)
	{
		// For description
		String metaDescription = contentPage.getDescription();
		if ((Objects.nonNull(arguments) && arguments.length > 0) && (checkPlaceHoldersExists(metaDescription)))
		{
			metaDescription = MessageFormat.format(metaDescription, arguments);
			setUpMetaData(model, contentPage.getKeywords(), metaDescription);
		}


		// For page title
		String contentPageTitle = pageTitleResolver.resolveContentPageTitle(contentPage.getTitle());
		if (checkPlaceHoldersExists(contentPageTitle))
		{
			contentPageTitle = MessageFormat.format(contentPageTitle, arguments);
			storeContentPageTitleInModel(model, contentPageTitle);
		}
	}

	public void setUpDynamicOGGraphData(final Model model, final ContentPageModel contentPage, final HttpServletRequest request,
			final String... arguments)
	{
		final BCFOGGraphTagsModel ogGraphDataModel = contentPage.getOgGraphData();
		if (Objects.isNull(ogGraphDataModel))
		{
			return;
		}

		final BCFOGGraphTagData bcfogGraphTagData;
		if (model.containsAttribute(BcfCoreConstants.OG_GRAPH_DATA))
		{
			bcfogGraphTagData = (BCFOGGraphTagData) model.asMap().get(BcfCoreConstants.OG_GRAPH_DATA);
		}
		else
		{
			bcfogGraphTagData = new BCFOGGraphTagData();
		}

		final String contentPageTitle = ogGraphDataModel.getTitle();
		if (checkPlaceHoldersExists(contentPageTitle))
		{
			bcfogGraphTagData.setTitle(MessageFormat.format(contentPageTitle, arguments));
		}

		// For description
		String description = ogGraphDataModel.getDescription();
		if ((Objects.nonNull(arguments) && arguments.length > 0) && (checkPlaceHoldersExists(description)))
		{
			description = MessageFormat.format(description, arguments);
			bcfogGraphTagData.setDescription(description);
		}

		bcfogGraphTagData.setUrl(request.getRequestURL().toString());
		if (Objects.nonNull(ogGraphDataModel.getUrl()))
		{
			bcfogGraphTagData.setUrl(ogGraphDataModel.getUrl());
		}


		model.addAttribute(BcfCoreConstants.OG_GRAPH_DATA, bcfogGraphTagData);
	}

	public void setUpMetaData(final Model model, final String metaKeywords, final String metaDescription)
	{
		final List<MetaElementData> metadata = new LinkedList<>();
		metadata.add(createMetaElement("description", metaDescription));
		model.addAttribute("metatags", metadata);
	}

	public void setUpMetaDataRobotsForContentPage(final Model model, Boolean indexable, Boolean metaFollow)
	{
		if (Objects.isNull(indexable))
		{
			indexable = Boolean.FALSE;
		}

		if (Objects.isNull(metaFollow))
		{
			metaFollow = Boolean.FALSE;
		}

		if (indexable && metaFollow)
		{
			model.addAttribute(ThirdPartyConstants.SeoRobots.META_ROBOTS, ThirdPartyConstants.SeoRobots.INDEX_FOLLOW);
		}
		else if (!indexable && metaFollow)
		{
			model.addAttribute(ThirdPartyConstants.SeoRobots.META_ROBOTS, ThirdPartyConstants.SeoRobots.NOINDEX_FOLLOW);
		}
		else if (indexable && !metaFollow)
		{
			model.addAttribute(ThirdPartyConstants.SeoRobots.META_ROBOTS, ThirdPartyConstants.SeoRobots.INDEX_NOFOLLOW);
		}
		else
		{
			model.addAttribute(ThirdPartyConstants.SeoRobots.META_ROBOTS, ThirdPartyConstants.SeoRobots.NOINDEX_NOFOLLOW);
		}
	}

	private String getCurrentPageUrl(final HttpServletRequest request)
	{
		final StringBuilder currentPageUrlBuilder = new StringBuilder();
		currentPageUrlBuilder.append(request.getRequestURL().toString());
		if (StringUtils.isNotEmpty(request.getQueryString()))
		{
			currentPageUrlBuilder.append(QUERY_PARAMS);
			currentPageUrlBuilder.append(request.getQueryString());
		}
		return currentPageUrlBuilder.toString();
	}

	private void storeTravelAdvisories(final Model model, final ContentPageModel contentPageModel)
	{
		if (contentPageModel.isShowDefaultAdvisories())
		{
			showDefaultTravelAdvisoriesChecked(model, contentPageModel);
		}
		else
		{
			showDefaultTravelAdvisoriesNotChecked(model, contentPageModel);
		}
	}

	private void addMatomoScripts(final Model model)
	{
		final MatomoModel matomo = matomoService.getMatomoScript();
		if (matomo != null)
		{
			model.addAttribute("matomoHeaderScript",
					StringEscapeUtils.unescapeXml(matomo.getHeaderScript(commerceCommonI18NService.getCurrentLocale())));
			model.addAttribute("matomoBodyScript",
					StringEscapeUtils.unescapeXml(matomo.getBodyScript(commerceCommonI18NService.getCurrentLocale())));
		}
	}

	private void showDefaultTravelAdvisoriesChecked(final Model model, final ContentPageModel contentPageModel)
	{
		Optional<String> departureLocation = Optional
				.ofNullable(sessionService.getAttribute(BcfFacadesConstants.DEPARTURE_LOCATION));
		Optional<String> arrivalLocation = Optional.ofNullable(sessionService.getAttribute(BcfFacadesConstants.ARRIVAL_LOCATION));

		final HttpServletRequest httpServletRequest = (HttpServletRequest) model.asMap().get(REQUEST);
		if (httpServletRequest != null && httpServletRequest.getSession() != null)
		{
			final Object bcfFareFinderForm = httpServletRequest.getSession().getAttribute(BcfFacadesConstants.BCF_FARE_FINDER_FORM);
			final Object travelFinderForm = httpServletRequest.getSession().getAttribute(BcfFacadesConstants.TRAVEL_FINDER_FORM);

			if (!departureLocation.isPresent() && !arrivalLocation.isPresent() && Objects.nonNull(bcfFareFinderForm) && Objects
					.nonNull(getStoredFieldValue(bcfFareFinderForm, GET_ROUTE_INFO_FORM)))
			{
				final Object routeInfoForm = getStoredFieldValue(bcfFareFinderForm, GET_ROUTE_INFO_FORM);
				departureLocation = Optional.ofNullable((String) getStoredFieldValue(routeInfoForm, GET_DEPARTURE_LOCATION));
				arrivalLocation = Optional.ofNullable((String) getStoredFieldValue(routeInfoForm, GET_ARRIVAL_LOCATION));
			}
			if (!departureLocation.isPresent() && !arrivalLocation.isPresent() && Objects.nonNull(travelFinderForm) && Objects
					.nonNull(getStoredFieldValue(travelFinderForm, GET_FARE_FINDER_FORM)))
			{
				final Object fareFinderForm = getStoredFieldValue(travelFinderForm, GET_FARE_FINDER_FORM);
				departureLocation = Optional.ofNullable((String) getStoredFieldValue(fareFinderForm, GET_DEPARTURE_LOCATION));
				arrivalLocation = Optional.ofNullable((String) getStoredFieldValue(fareFinderForm, GET_ARRIVAL_LOCATION));
			}
		}
		if (contentPageModel.isHomepage())
		{
			setHomePageTravelAdvisories(model,
					travelAdvisoryFacade.findActiveTravelAdvisoriesForHomePageByRouteRegion(departureLocation, arrivalLocation));
		}
		else
		{
			model.addAttribute(BcfFacadesConstants.TRAVEL_ADVISORIES,
					travelAdvisoryFacade.findActiveTravelAdvisoriesForOtherPagesByRouteRegion(departureLocation, arrivalLocation));
		}
	}

	private Object getStoredFieldValue(final Object form, final String attributeGetter)
	{
		if (Objects.isNull(form) || Objects.isNull(attributeGetter))
		{
			return null;
		}
		try
		{
			return form.getClass().getDeclaredMethod(attributeGetter).invoke(form);
		}
		catch (final NoSuchMethodException | IllegalAccessException | InvocationTargetException e)
		{
			LOG.error(e);
			return null;
		}
	}

	private void showDefaultTravelAdvisoriesNotChecked(final Model model, final ContentPageModel contentPageModel)
	{
		if (contentPageModel.isHomepage())
		{
			setHomePageTravelAdvisories(model, travelAdvisoryFacade
					.findActiveTravelAdvisoriesForHomePageWithoutRouteRegion());
		}
		else
		{
			model.addAttribute(BcfFacadesConstants.TRAVEL_ADVISORIES,
					travelAdvisoryFacade.findActiveTravelAdvisoriesForOtherPagesWithoutRouteRegion());
		}
	}

	private void setHomePageTravelAdvisories(final Model model, final List<TravelAdvisoryData> travelAdvisoriesForHomePage)
	{
		model.addAttribute(BcfFacadesConstants.TRAVEL_ADVISORIES,
				travelAdvisoriesForHomePage);
		StreamUtil.safeStream(travelAdvisoriesForHomePage)
				.filter(filterHomePageTakeOverAdvisory())
				.findAny()
				.ifPresent(travelAdvisory -> model.addAttribute(HOME_PAGE_TAKE_OVER_ADVISORY, travelAdvisory));
	}

	private Predicate<TravelAdvisoryData> filterHomePageTakeOverAdvisory()
	{
		return travelAdvisoryData -> TravelAdvisoryDisplayType.HOME_PAGE_TAKEOVER.getCode()
				.equals(travelAdvisoryData.getDisplayType());
	}

	protected boolean checkPlaceHoldersExists(final String description)
	{
		return Objects.nonNull(description) && description.contains("{");
	}

	protected void storeContentPageTitleInModel(final Model model, final String title)
	{
		model.addAttribute(CMS_PAGE_TITLE, title);
	}

	protected MetaElementData createMetaElement(final String name, final String content)
	{
		final MetaElementData element = new MetaElementData();
		element.setName(name);
		element.setContent(content);
		return element;
	}
}
