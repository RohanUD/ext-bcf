/*
 * Copyright (c) 2019 British Columbia Ferry Services Inc
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of British Columbia Ferry Services Inc("Confidential Information").
 * You shall not disclose such Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with British Columbia Ferry Services Inc.
 *
 * Last Modified: 21/06/19 00:21
 */

package com.bcf.bcfstorefrontaddon.util;

import de.hybris.platform.commercefacades.accommodation.search.RoomStayCandidateData;
import de.hybris.platform.commercefacades.product.data.PriceData;
import de.hybris.platform.commercefacades.travel.PassengerTypeQuantityData;
import de.hybris.platform.servicelayer.session.SessionService;
import de.hybris.platform.travelfacades.facades.TravelCommercePriceFacade;
import de.hybris.platform.travelservices.enums.BookingJourneyType;
import de.hybris.platform.travelservices.enums.OrderEntryType;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;
import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang.StringUtils;
import org.springframework.stereotype.Component;
import org.springframework.web.util.WebUtils;
import com.bcf.bcfstorefrontaddon.constants.BcfstorefrontaddonWebConstants;
import com.bcf.core.bcfproperties.BcfConfigurablePropertiesService;
import com.bcf.core.constants.BcfCoreConstants;
import com.bcf.core.service.BCFTravelCartService;
import com.bcf.facades.bcffacades.BcfBookingFacade;
import com.bcf.facades.bcffacades.BcfSalesApplicationResolverFacade;
import com.bcf.facades.constants.BcfFacadesConstants;
import com.bcf.facades.order.BcfTravelCartFacade;


@Component("bcfControllerUtil")
public class BcfControllerUtil
{

	@Resource(name = "bcfSalesApplicationResolverFacade")
	private BcfSalesApplicationResolverFacade bcfSalesApplicationResolverFacade;

	@Resource(name = "sessionService")
	private SessionService sessionService;

	@Resource(name = "salesChannelBookingJourneySummaryUrlsMap")
	private Map<String, String> salesChannelBookingJourneySummaryUrlsMap;

	@Resource(name = "salesChannelAmendBookingJourneySummaryUrlsMap")
	private Map<String, String> salesChannelAmendBookingJourneySummaryUrlsMap;


	@Resource(name = "bcfConfigurablePropertiesService")
	private BcfConfigurablePropertiesService bcfConfigurablePropertiesService;

	@Resource(name = "bookingFacade")
	private BcfBookingFacade bookingFacade;

	@Resource(name = "bcfTravelCartFacade")
	private BcfTravelCartFacade bcfTravelCartFacade;

	@Resource(name = "travelCartService")
	private BCFTravelCartService bcfTravelCartService;

	@Resource(name = "travelCommercePriceFacade")
	private TravelCommercePriceFacade travelCommercePriceFacade;

	public String getNextPageURL()
	{
		final String salesChannel=bcfSalesApplicationResolverFacade.getCurrentSalesChannel();
		final Object sessionJourney=sessionService.getAttribute(BcfCoreConstants.SESSION_BOOKING_JOURNEY);

		if (salesChannel != null && sessionJourney != null)
		{

			if (bcfTravelCartFacade.isAmendmentCart())
			{
				return salesChannelAmendBookingJourneySummaryUrlsMap
						.get(salesChannel + BcfstorefrontaddonWebConstants.HYPHEN + sessionJourney);
			}
			else
			{
				return salesChannelBookingJourneySummaryUrlsMap
						.get(salesChannel + BcfstorefrontaddonWebConstants.HYPHEN + sessionJourney);
			}
		}
		return BcfstorefrontaddonWebConstants.BasketSummaryPage;
	}

	public boolean isNonTransportBooking( final String bookingReference)
	{
		final String bookingJourney = getBookingJourney(bookingReference);
		return !StringUtils.equals(BookingJourneyType.BOOKING_TRANSPORT_ONLY.getCode(), bookingJourney);
	}


	private String getBookingJourney(final String bookingReference)
	{
		if (StringUtils.isNotEmpty(bookingReference))
		{
			return bookingFacade.getBookingJourneyType(bookingReference);
		}
		else if (bcfTravelCartService.hasSessionCart() && bcfTravelCartService.getSessionCart().getBookingJourneyType() != null)
		{
			return bcfTravelCartService.getSessionCart().getBookingJourneyType().getCode();
		}
		return null;
	}

	public static List<RoomStayCandidateData> cleanUpChildRoomStayCandidates(final int numberOfRooms,
			final List<RoomStayCandidateData> roomStayCandidates)
	{

		if(CollectionUtils.isNotEmpty(roomStayCandidates))
		{
			for (int i = 0; i < roomStayCandidates.size(); i++)
			{
				final RoomStayCandidateData roomStayCandidateData = roomStayCandidates.get(i);
				final List<PassengerTypeQuantityData> passengerTypeQuantityList = roomStayCandidateData.getPassengerTypeQuantityList();
				if (CollectionUtils.isNotEmpty(passengerTypeQuantityList))
				{
					for (final PassengerTypeQuantityData passengerTypeQuantityData : passengerTypeQuantityList)
					{

						final List<Integer> childAges = passengerTypeQuantityData.getChildAges();
						if (CollectionUtils.isNotEmpty(childAges))
						{
							passengerTypeQuantityData.setChildAgesArray(
									childAges.stream().map(integer -> integer.toString()).collect(Collectors.joining(",")));
						}
					}

				}
			}
		}

		if (CollectionUtils.size(roomStayCandidates) <= numberOfRooms)
		{
			return roomStayCandidates;
		}

		for(int i = numberOfRooms; i < roomStayCandidates.size(); i++){

			final RoomStayCandidateData roomStayCandidateData=roomStayCandidates.get(i);
			final List<PassengerTypeQuantityData> passengerTypeQuantityList=roomStayCandidateData.getPassengerTypeQuantityList();
			if(CollectionUtils.isNotEmpty(passengerTypeQuantityList)){

				for(final PassengerTypeQuantityData passengerTypeQuantityData:passengerTypeQuantityList){
					if(passengerTypeQuantityData.getPassengerType().getCode().equals("child")){
						passengerTypeQuantityData.setQuantity(0);
						passengerTypeQuantityData.setChildAges(null);
					}

				}
			}
		}

		return roomStayCandidates;
	}

	public PriceData getTotalToPay()
	{
		final PriceData totalToPay;
		final String originalOrderCode = bcfTravelCartFacade.getOriginalOrderCode();
		if (StringUtils
				.equals(sessionService.getAttribute(BcfstorefrontaddonWebConstants.SESSION_PAY_NOW), originalOrderCode)
				&& !bcfTravelCartFacade.hasCartBeenAmended())
		{
			totalToPay = travelCommercePriceFacade.createPriceData(bookingFacade
					.getOrderTotalToPayForOrderEntryType(originalOrderCode, OrderEntryType.ACCOMMODATION).doubleValue());
		}
		else if (StringUtils.equals(sessionService.getAttribute(BcfstorefrontaddonWebConstants.SESSION_CHANGE_DATES),
				originalOrderCode))
		{
			totalToPay = travelCommercePriceFacade
					.createPriceData(bookingFacade.getOrderTotalToPayForChangeDates().doubleValue());
		}
		else
		{
			/*
			 * Assuming getAmountToPay() will return positive, negative or zero amount which is final value for this
			 * transaction. If value is positive, then show payment page for the amount If value is zero, confirm order
			 * directly. And if value is negative, confirm order and initiate refund for the negative amount.
			 *
			 * Thus, using getBcfTravelCartFacade().getAmountToPay() instead of getBcfTravelCartFacade().getTotalToPayPrice() for
			 * now. In case of accommodations or deals, we might need to calculate totalToPay.
			 */

			totalToPay = bcfTravelCartFacade.getAmountToPay();
		}
		return totalToPay;
	}


	public static void cleanUpSession(final HttpServletRequest request, final SessionService sessionService)
	{
		WebUtils.setSessionAttribute(request, BcfFacadesConstants.BCF_FARE_FINDER_FORM, null);
		WebUtils.setSessionAttribute(request, BcfstorefrontaddonWebConstants.BCF_TRAVELLER_DATA, null);
		WebUtils.setSessionAttribute(request, BcfstorefrontaddonWebConstants.PERSONAL_DETAILS_FORM, null);
		WebUtils.setSessionAttribute(request, BcfFacadesConstants.TRAVEL_FINDER_FORM, null);
		WebUtils.setSessionAttribute(request, BcfstorefrontaddonWebConstants.CURRENT_PAGE, null);
		sessionService.removeAttribute(BcfstorefrontaddonWebConstants.SESSION_PAY_NOW);
		sessionService.removeAttribute(BcfstorefrontaddonWebConstants.SESSION_CHANGE_DATES);
		sessionService.removeAttribute(BcfCoreConstants.SESSION_BOOKING_JOURNEY);
		sessionService.removeAttribute(BcfstorefrontaddonWebConstants.IS_CHECK_IN_JOURNEY);
		sessionService.removeAttribute(BcfstorefrontaddonWebConstants.ACCOMMODATION_FINDER_FORM);
		sessionService.removeAttribute((BcfFacadesConstants.BOOKING_CONFIRMATION_REFERENCE));
		sessionService.removeAttribute((BcfFacadesConstants.SAILINGCODE_PRODUCTAVAILABILITY_MAP));
		sessionService.removeAttribute(BcfFacadesConstants.ORIGINAL_BOOKING_REFERENCE);
		sessionService.removeAttribute(BcfFacadesConstants.AMENDING_BOOKING_REFERENCES);
		sessionService.removeAttribute(BcfFacadesConstants.BCF_FARE_FINDER_FORM);
		sessionService.removeAttribute(BcfFacadesConstants.JOURNEY_REF_NUMBER);
		sessionService.removeAttribute(BcfFacadesConstants.DEPARTURE_LOCATION);
		sessionService.removeAttribute(BcfFacadesConstants.ARRIVAL_LOCATION);
		sessionService.removeAttribute(BcfstorefrontaddonWebConstants.BOOKING_REFERENCE);
	}

}
