package com.bcf.bcfstorefrontaddon.util;

import de.hybris.platform.commerceservices.search.pagedata.PageableData;
import de.hybris.platform.core.servicelayer.data.PaginationData;
import java.io.Serializable;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Component;
import org.springframework.ui.Model;
import com.bcf.core.bcfproperties.BcfConfigurablePropertiesService;


@Component("globalPaginationUtil")
public class GlobalPaginationUtil
{
	private static final String PAGE = "page";
	private static final String SORT_BY = "sortBy";
	private static final String PAGE_SIZE = ".pagesize";
	private static final String FACET_SEARCH_PAGE_DATA = "facetSearchPageData";
	private static final String TOTAL_DISPLAY_PAGE_NUMBERS = "totalDisplayPageNumbers";
	private static final int TOTAL_PAGE_NUMBERS = 6;
	private static final String GLOBAL_PAGE_SIZE_CONFIG = "global.page.size";
	private static final int GLOBAL_PAGE_SIZE = 20;
	private static final String PAGINATION_META_DATA = "paginationMetaData";
	private static final String RESULTS = "results";
	public static final String START_PAGE = "startPage";
	public static final String END_PAGE = "endPage";

	@Resource(name = "bcfConfigurablePropertiesService")
	private BcfConfigurablePropertiesService bcfConfigurablePropertiesService;

	public PageableData buildPaginationData(final HttpServletRequest request, final String cmsPageLabel)
	{
		final PageableData pageableData = new PageableData();
		pageableData.setCurrentPage(getCurrentPage(request));
		pageableData.setPageSize(getPageSize(cmsPageLabel));
		pageableData.setSort(request.getParameter(SORT_BY));
		return pageableData;
	}

	public int getPageSize(final String cmsPageLabel)
	{
		return Optional.ofNullable(bcfConfigurablePropertiesService.getBcfPropertyValue(cmsPageLabel + PAGE_SIZE))
				.map(Integer::parseInt)
				.orElse(getFallbackPageSize());
	}

	private int getFallbackPageSize()
	{
		return Optional.ofNullable(bcfConfigurablePropertiesService.getBcfPropertyValue(GLOBAL_PAGE_SIZE_CONFIG))
				.map(Integer::parseInt)
				.orElse(GLOBAL_PAGE_SIZE);
	}

	public void storePaginationResults(final Model model, final Object facetSearchPageData)
	{
		model.addAttribute(FACET_SEARCH_PAGE_DATA, facetSearchPageData);
		model.addAttribute(TOTAL_DISPLAY_PAGE_NUMBERS, TOTAL_PAGE_NUMBERS);
	}

	public void storePaginationResults(final Model model, final List<? extends Serializable> results,
			final PaginationData paginationMetaData)
	{
		model.addAttribute(RESULTS, results);
		model.addAttribute(PAGINATION_META_DATA, paginationMetaData);
		model.addAttribute(TOTAL_DISPLAY_PAGE_NUMBERS, TOTAL_PAGE_NUMBERS);
	}

	public int getCurrentPage(final HttpServletRequest request)
	{
		final String currentPage = request.getParameter(PAGE);
		return StringUtils.isEmpty(currentPage) ? 0 : Integer.parseInt(currentPage);
	}

	public Map<String, Integer> getJsonPaginationIndexes(final int totalRecords, final int pageSize, final int page)
	{
		final Map<String, Integer> jsonPageNumbersMetaData = new HashMap<>();
		int totalPages = totalRecords / pageSize;
		if (totalRecords % pageSize > 0)
		{
			totalPages++;
		}
		int startPageNumber = page - (TOTAL_PAGE_NUMBERS / 2);
		int endPageNumber = page + (TOTAL_PAGE_NUMBERS / 2);

		if (startPageNumber < 0)
		{
			endPageNumber = endPageNumber + (0 - startPageNumber);
			startPageNumber = 0;
		}
		endPageNumber = Math.min(endPageNumber, totalPages - 1);
		if (endPageNumber - startPageNumber < TOTAL_PAGE_NUMBERS)
		{
			startPageNumber = endPageNumber - (TOTAL_PAGE_NUMBERS - 1);
		}
		startPageNumber = Math.max(startPageNumber, 0);

		jsonPageNumbersMetaData.put(START_PAGE, startPageNumber);
		jsonPageNumbersMetaData.put(END_PAGE, endPageNumber);
		return jsonPageNumbersMetaData;
	}

}
