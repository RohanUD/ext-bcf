/*
 * Copyright (c) 2019 British Columbia Ferry Services Inc
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of British Columbia Ferry Services Inc("Confidential Information").
 * You shall not disclose such Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with British Columbia Ferry Services Inc.
 *
 * Last Modified: 02/07/19 14:01
 */

package com.bcf.bcfstorefrontaddon.util;

import de.hybris.platform.servicelayer.session.SessionService;
import javax.annotation.Resource;
import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Component;
import org.springframework.ui.Model;
import com.bcf.facades.constants.BcfFacadesConstants;
import com.bcf.facades.order.BcfTravelCartFacade;


@Component("bcfOriginalBookingUtil")
public class BcfOriginalBookingUtil
{
	@Resource(name = "bcfCartFacade")
	private BcfTravelCartFacade bcfTravelCartFacade;

	@Resource(name = "sessionService")
	private SessionService sessionService;


	public void setAdditionalAttributes(final Model model)
	{
		final String originalBookingReference = sessionService.getAttribute(BcfFacadesConstants.ORIGINAL_BOOKING_REFERENCE);
		final boolean isAmendingSailing = bcfTravelCartFacade.isAmendingSailing();
		if (isAmendingSailing && StringUtils.isNotBlank(originalBookingReference))
		{
			model.addAttribute(BcfFacadesConstants.IS_MODIFYING_TRANSPORT_BOOKING, isAmendingSailing);
			model.addAttribute(BcfFacadesConstants.ORIGINAL_BOOKING_REFERENCE, originalBookingReference);
			model.addAttribute("abortModificationURL", "/manage-booking/abort-modification");
		}
	}
}
