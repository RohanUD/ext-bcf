package de.hybris.platform.cms2.jalo.restrictions;

import de.hybris.platform.jalo.SessionContext;


public class TravelAdvisoryHomePageTakeOverRestriction extends GeneratedTravelAdvisoryHomePageTakeOverRestriction
{
	@Override
	public String getDescription(final SessionContext sessionContext)
	{
		return "TravelAdvisoryHomePageTakeOverRestriction";
	}
}
