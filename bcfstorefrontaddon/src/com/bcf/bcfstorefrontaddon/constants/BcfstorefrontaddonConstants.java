/*
 * Copyright (c) 2019 British Columbia Ferry Services Inc
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of British Columbia Ferry Services Inc("Confidential Information").
 * You shall not disclose such Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with British Columbia Ferry Services Inc.
 *
 * Last Modified: 06/06/19 20:21
 */

package com.bcf.bcfstorefrontaddon.constants;

/**
 * Global class for all Bcfstorefrontaddon constants. You can add global constants for your extension into this class.
 */
public final class BcfstorefrontaddonConstants extends GeneratedBcfstorefrontaddonConstants
{

	public static final String EXTENSIONNAME = "bcfstorefrontaddon";

	public static final String DEFAULT_CONTENTPAGE_SEO_METACONTENT_ID = "contentpagemetadata";
	public static final String DEFAULT_HOMEPAGE_SEO_METACONTENT_ID = "homepagemetadata";
	public static final String CANCEL_BOOKING_RESTRICTION_TIMEGAP = "cancelBookingRestrictionTimeGap";
	public static final String AGENT_PENDING_ORDERS_PAGE_SIZE = "agent.pending.orders.page.size";

	private BcfstorefrontaddonConstants()
	{
		// empty to avoid instantiating this constant class
	}
}
