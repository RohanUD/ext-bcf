/*
 * Copyright (c) 2019 British Columbia Ferry Services Inc
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of British Columbia Ferry Services Inc("Confidential Information").
 * You shall not disclose such Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with British Columbia Ferry Services Inc.
 *
 * Last Modified: 06/06/19 20:21
 */

package com.bcf.bcfstorefrontaddon.jalo;

import de.hybris.platform.jalo.JaloSession;
import de.hybris.platform.jalo.extension.ExtensionManager;
import org.apache.log4j.Logger;
import com.bcf.bcfstorefrontaddon.constants.BcfstorefrontaddonConstants;


@SuppressWarnings("PMD")
public class BcfstorefrontaddonManager extends GeneratedBcfstorefrontaddonManager
{
	@SuppressWarnings("unused")
	private static Logger log = Logger.getLogger(BcfstorefrontaddonManager.class.getName());

	public static final BcfstorefrontaddonManager getInstance()
	{
		ExtensionManager em = JaloSession.getCurrentSession().getExtensionManager();
		return (BcfstorefrontaddonManager) em.getExtension(BcfstorefrontaddonConstants.EXTENSIONNAME);
	}

}
