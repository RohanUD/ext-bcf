/*
 * Copyright (c) 2019 British Columbia Ferry Services Inc
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of British Columbia Ferry Services Inc("Confidential Information").
 * You shall not disclose such Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with British Columbia Ferry Services Inc.
 *
 * Last Modified: 06/06/19 20:21
 */

package com.bcf.bcfstorefrontaddon.component.handler;

import de.hybris.platform.servicelayer.model.attribute.DynamicAttributeHandler;
import com.bcf.bcfstorefrontaddon.model.components.BCFGridColumnModel;


public class BCFLGGridColumnHandler implements DynamicAttributeHandler<Integer, BCFGridColumnModel>
{
	private static final double SINGLE_COLUMN = 8.33d;

	@Override
	public Integer get(final BCFGridColumnModel model)
	{
		return (int) Math.round(model.getRatio() / SINGLE_COLUMN);
	}

	@Override
	public void set(final BCFGridColumnModel model, final Integer integer)
	{
		// DO NOTHING
	}
}
