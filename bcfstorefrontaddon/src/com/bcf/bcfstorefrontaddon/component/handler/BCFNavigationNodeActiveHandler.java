/*
 * Copyright (c) 2019 British Columbia Ferry Services Inc
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of British Columbia Ferry Services Inc("Confidential Information").
 * You shall not disclose such Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with British Columbia Ferry Services Inc.
 *
 * Last Modified: 06/06/19 20:21
 */

package com.bcf.bcfstorefrontaddon.component.handler;

import de.hybris.platform.cms2.model.contents.components.CMSLinkComponentModel;
import de.hybris.platform.servicelayer.model.attribute.DynamicAttributeHandler;
import de.hybris.platform.servicelayer.session.SessionService;
import java.util.Objects;
import javax.annotation.Resource;
import com.bcf.bcfstorefrontaddon.model.components.BCFNavigationNodeComponentModel;
import com.bcf.core.util.StreamUtil;


public class BCFNavigationNodeActiveHandler implements DynamicAttributeHandler<Boolean, BCFNavigationNodeComponentModel>
{
	private static final String CURRENT_PAGE_URL = "currentPageUrl";

	@Resource(name = "sessionService")
	private SessionService sessionService;

	@Override
	public Boolean get(final BCFNavigationNodeComponentModel navigationNodeComponentModel)
	{
		if (StreamUtil.safeStream(navigationNodeComponentModel.getChildLinks()).anyMatch(this::isChildPage))
		{
			return true;
		}
		if (isChildPage(navigationNodeComponentModel.getLink()) || isChildPage(navigationNodeComponentModel.getPromotionalLink()))
		{
			return true;
		}
		return false;
	}

	private boolean isChildPage(final CMSLinkComponentModel link)
	{
		final String currentPageUrl = sessionService.getAttribute(CURRENT_PAGE_URL);
		return Objects.nonNull(link) && currentPageUrl.equals(link.getUrl());
	}

	@Override
	public void set(final BCFNavigationNodeComponentModel navigationNodeComponentModel, final Boolean b)
	{
		// DO NOTHING
	}
}
