/*
 * Copyright (c) 2019 British Columbia Ferry Services Inc
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of British Columbia Ferry Services Inc("Confidential Information").
 * You shall not disclose such Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with British Columbia Ferry Services Inc.
 *
 * Last Modified: 06/06/19 20:21
 */

package com.bcf.bcfstorefrontaddon.annotations;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;


/**
 * This annotation is used in controller methods for which the following conditions will be evaluated to be true in
 * order for the request to proceeded:
 * <p>
 * This can skip the login page redirection for the guest booking management.
 */

@Retention(RetentionPolicy.RUNTIME)
@Target(
		{ ElementType.METHOD, ElementType.TYPE })

public @interface CheckRequireLoginForBookingChange
{

}
